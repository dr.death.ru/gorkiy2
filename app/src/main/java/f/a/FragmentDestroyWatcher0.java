package f.a;

import android.app.Activity;
import f.ObjectWatcher;
import f.b;
import f.d;
import java.lang.reflect.Constructor;
import kotlin.TypeCastException;
import n.g;
import n.n.b.Functions;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.n.c.TypeIntrinsics;

/* compiled from: FragmentDestroyWatcher.kt */
public final class FragmentDestroyWatcher0 {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.reflect.Constructor<?>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final Functions0<Activity, g> a(String str, String str2, d dVar, Functions<b.a> functions) {
        boolean z;
        boolean z2;
        try {
            Class.forName(str);
            z = true;
        } catch (ClassNotFoundException unused) {
            z = false;
        }
        if (z) {
            try {
                Class.forName(str2);
                z2 = true;
            } catch (ClassNotFoundException unused2) {
                z2 = false;
            }
            if (z2) {
                Constructor<?> declaredConstructor = Class.forName(str2).getDeclaredConstructor(ObjectWatcher.class, Functions.class);
                Intrinsics.a((Object) declaredConstructor, "Class.forName(watcherCla…a, Function0::class.java)");
                Object newInstance = declaredConstructor.newInstance(dVar, functions);
                if (newInstance != null) {
                    TypeIntrinsics.a(newInstance, 1);
                    return (Functions0) newInstance;
                }
                throw new TypeCastException("null cannot be cast to non-null type (android.app.Activity) -> kotlin.Unit");
            }
        }
        return null;
    }
}
