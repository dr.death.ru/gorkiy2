package f;

import f.a.InternalAppWatcher;
import j.a.a.a.outline;
import java.util.concurrent.TimeUnit;
import n.Lazy;
import n.p.KProperty;

/* compiled from: AppWatcher.kt */
public final class AppWatcher {
    public static volatile a a = (InternalAppWatcher.d != null ? new a(false, false, false, false, 0, 31) : new a(false, false, false, false, 0, 30));
    public static final AppWatcher b = null;

    /* compiled from: AppWatcher.kt */
    public static final class a {
        public final boolean a;
        public final boolean b;
        public final boolean c;
        public final boolean d;

        /* renamed from: e  reason: collision with root package name */
        public final long f727e;

        public a() {
            this(false, false, false, false, 0, 31);
        }

        public /* synthetic */ a(boolean z, boolean z2, boolean z3, boolean z4, long j2, int i2) {
            if ((i2 & 1) != 0) {
                if (InternalAppWatcher.f732i != null) {
                    Lazy lazy = InternalAppWatcher.c;
                    KProperty kProperty = InternalAppWatcher.a[0];
                    z = ((Boolean) lazy.getValue()).booleanValue();
                } else {
                    throw null;
                }
            }
            z2 = (i2 & 2) != 0 ? true : z2;
            z3 = (i2 & 4) != 0 ? true : z3;
            z4 = (i2 & 8) != 0 ? true : z4;
            j2 = (i2 & 16) != 0 ? TimeUnit.SECONDS.toMillis(5) : j2;
            this.a = z;
            this.b = z2;
            this.c = z3;
            this.d = z4;
            this.f727e = j2;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (this.a == aVar.a) {
                        if (this.b == aVar.b) {
                            if (this.c == aVar.c) {
                                if (this.d == aVar.d) {
                                    if (this.f727e == aVar.f727e) {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
                return false;
            }
            return true;
        }

        public int hashCode() {
            boolean z = this.a;
            boolean z2 = true;
            if (z) {
                z = true;
            }
            int i2 = (z ? 1 : 0) * true;
            boolean z3 = this.b;
            if (z3) {
                z3 = true;
            }
            int i3 = (i2 + (z3 ? 1 : 0)) * 31;
            boolean z4 = this.c;
            if (z4) {
                z4 = true;
            }
            int i4 = (i3 + (z4 ? 1 : 0)) * 31;
            boolean z5 = this.d;
            if (!z5) {
                z2 = z5;
            }
            long j2 = this.f727e;
            return ((i4 + (z2 ? 1 : 0)) * 31) + ((int) (j2 ^ (j2 >>> 32)));
        }

        public String toString() {
            StringBuilder a2 = outline.a("Config(enabled=");
            a2.append(this.a);
            a2.append(", watchActivities=");
            a2.append(this.b);
            a2.append(", watchFragments=");
            a2.append(this.c);
            a2.append(", watchFragmentViews=");
            a2.append(this.d);
            a2.append(", watchDurationMillis=");
            a2.append(this.f727e);
            a2.append(")");
            return a2.toString();
        }
    }

    static {
        if (InternalAppWatcher.f732i != null) {
            return;
        }
        throw null;
    }
}
