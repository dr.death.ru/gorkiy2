package r;

import java.util.concurrent.CompletableFuture;
import r.CompletableFutureCallAdapterFactory2;

/* compiled from: CompletableFutureCallAdapterFactory */
public class CompletableFutureCallAdapterFactory0 extends CompletableFuture<Response<R>> {
    public final /* synthetic */ Call b;

    public CompletableFutureCallAdapterFactory0(CompletableFutureCallAdapterFactory2.b bVar, Call call) {
        this.b = call;
    }

    public boolean cancel(boolean z) {
        if (z) {
            this.b.cancel();
        }
        return super.cancel(z);
    }
}
