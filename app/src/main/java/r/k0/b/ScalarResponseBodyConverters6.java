package r.k0.b;

import o.ResponseBody;
import o.i0;
import r.Converter;

/* compiled from: ScalarResponseBodyConverters */
public final class ScalarResponseBodyConverters6 implements Converter<i0, Short> {
    public static final ScalarResponseBodyConverters6 a = new ScalarResponseBodyConverters6();

    public Object a(Object obj) {
        return Short.valueOf(((ResponseBody) obj).h());
    }
}
