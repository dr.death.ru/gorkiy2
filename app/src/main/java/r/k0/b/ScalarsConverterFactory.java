package r.k0.b;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import javax.annotation.Nullable;
import o.g0;
import o.i0;
import r.Converter;
import r.e0;

public final class ScalarsConverterFactory extends Converter.a {
    @Nullable
    public Converter<?, g0> a(Type type, Annotation[] annotationArr, Annotation[] annotationArr2, e0 e0Var) {
        if (type == String.class || type == Boolean.TYPE || type == Boolean.class || type == Byte.TYPE || type == Byte.class || type == Character.TYPE || type == Character.class || type == Double.TYPE || type == Double.class || type == Float.TYPE || type == Float.class || type == Integer.TYPE || type == Integer.class || type == Long.TYPE || type == Long.class || type == Short.TYPE || type == Short.class) {
            return ScalarRequestBodyConverter.a;
        }
        return null;
    }

    @Nullable
    public Converter<i0, ?> a(Type type, Annotation[] annotationArr, e0 e0Var) {
        if (type == String.class) {
            return ScalarResponseBodyConverters7.a;
        }
        if (type == Boolean.class || type == Boolean.TYPE) {
            return ScalarResponseBodyConverters.a;
        }
        if (type == Byte.class || type == Byte.TYPE) {
            return ScalarResponseBodyConverters0.a;
        }
        if (type == Character.class || type == Character.TYPE) {
            return ScalarResponseBodyConverters1.a;
        }
        if (type == Double.class || type == Double.TYPE) {
            return ScalarResponseBodyConverters2.a;
        }
        if (type == Float.class || type == Float.TYPE) {
            return ScalarResponseBodyConverters3.a;
        }
        if (type == Integer.class || type == Integer.TYPE) {
            return ScalarResponseBodyConverters4.a;
        }
        if (type == Long.class || type == Long.TYPE) {
            return ScalarResponseBodyConverters5.a;
        }
        if (type == Short.class || type == Short.TYPE) {
            return ScalarResponseBodyConverters6.a;
        }
        return null;
    }
}
