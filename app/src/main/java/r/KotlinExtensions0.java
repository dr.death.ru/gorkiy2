package r;

import j.c.a.a.c.n.c;
import kotlinx.coroutines.CancellableContinuation;
import n.n.c.Intrinsics;
import retrofit2.HttpException;

/* compiled from: KotlinExtensions.kt */
public final class KotlinExtensions0 implements Callback<T> {
    public final /* synthetic */ CancellableContinuation b;

    public KotlinExtensions0(CancellableContinuation cancellableContinuation) {
        this.b = cancellableContinuation;
    }

    public void a(Call<T> call, Response<T> response) {
        if (call == null) {
            Intrinsics.a("call");
            throw null;
        } else if (response == null) {
            Intrinsics.a("response");
            throw null;
        } else if (response.a()) {
            this.b.a(response.b);
        } else {
            this.b.a(c.a((Throwable) new HttpException(response)));
        }
    }

    public void a(Call<T> call, Throwable th) {
        if (call == null) {
            Intrinsics.a("call");
            throw null;
        } else if (th != null) {
            this.b.a(c.a(th));
        } else {
            Intrinsics.a("t");
            throw null;
        }
    }
}
