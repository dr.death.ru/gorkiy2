package r.j0.a;

import io.reactivex.exceptions.CompositeException;
import j.c.a.a.c.n.c;
import l.b.Observable;
import l.b.Observer;
import l.b.s.Disposable;
import l.b.s.b;
import r.Call;
import r.Callback;
import r.Response;

public final class CallEnqueueObservable<T> extends Observable<Response<T>> {
    public final Call<T> b;

    public CallEnqueueObservable(Call<T> call) {
        this.b = call;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [r.j0.a.CallEnqueueObservable$a, l.b.s.Disposable, r.Callback] */
    public void b(Observer<? super Response<T>> observer) {
        Call<T> clone = this.b.clone();
        ? aVar = new a(clone, observer);
        observer.a((Disposable) aVar);
        if (!aVar.d) {
            clone.a(aVar);
        }
    }

    public static final class a<T> implements b, Callback<T> {
        public final Call<?> b;
        public final Observer<? super Response<T>> c;
        public volatile boolean d;

        /* renamed from: e  reason: collision with root package name */
        public boolean f3204e = false;

        public a(Call<?> call, Observer<? super Response<T>> observer) {
            this.b = call;
            this.c = observer;
        }

        public void a(Call<T> call, Response<T> response) {
            if (!this.d) {
                try {
                    this.c.b(response);
                    if (!this.d) {
                        this.f3204e = true;
                        this.c.a();
                    }
                } catch (Throwable th) {
                    c.c(th);
                    c.b((Throwable) new CompositeException(th, th));
                }
            }
        }

        public void f() {
            this.d = true;
            this.b.cancel();
        }

        public boolean g() {
            return this.d;
        }

        public void a(Call<T> call, Throwable th) {
            if (!call.h()) {
                try {
                    this.c.a(th);
                } catch (Throwable th2) {
                    c.c(th2);
                    c.b((Throwable) new CompositeException(th, th2));
                }
            }
        }
    }
}
