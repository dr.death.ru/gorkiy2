package r;

import j.c.a.a.c.n.c;
import kotlinx.coroutines.CancellableContinuation;
import n.n.c.Intrinsics;

/* compiled from: KotlinExtensions.kt */
public final class KotlinExtensions1 implements Callback<T> {
    public final /* synthetic */ CancellableContinuation b;

    public KotlinExtensions1(CancellableContinuation cancellableContinuation) {
        this.b = cancellableContinuation;
    }

    public void a(Call<T> call, Response<T> response) {
        if (call == null) {
            Intrinsics.a("call");
            throw null;
        } else if (response != null) {
            this.b.a(response);
        } else {
            Intrinsics.a("response");
            throw null;
        }
    }

    public void a(Call<T> call, Throwable th) {
        if (call == null) {
            Intrinsics.a("call");
            throw null;
        } else if (th != null) {
            this.b.a(c.a(th));
        } else {
            Intrinsics.a("t");
            throw null;
        }
    }
}
