package t.a;

import j.a.a.a.outline;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import t.a.a;

public final class Timber {
    public static final b[] a = new b[0];
    public static final List<a.b> b = new ArrayList();
    public static volatile b[] c = a;
    public static final b d = new a();

    public static abstract class b {
        public final ThreadLocal<String> a = new ThreadLocal<>();

        public abstract void a(int i2, String str, String str2, Throwable th);

        public final void a(int i2, Throwable th, String str, Object... objArr) {
            String str2 = this.a.get();
            if (str2 != null) {
                this.a.remove();
            }
            if (str != null && str.length() == 0) {
                str = null;
            }
            if (str != null) {
                if (objArr != null && objArr.length > 0) {
                    str = String.format(str, objArr);
                }
                if (th != null) {
                    StringBuilder b = outline.b(str, "\n");
                    b.append(b(th));
                    str = b.toString();
                }
            } else if (th != null) {
                str = b(th);
            } else {
                return;
            }
            a(i2, str2, str, th);
        }

        public void b(String str, Object... objArr) {
            a(4, (Throwable) null, str, objArr);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.io.PrintWriter.<init>(java.io.Writer, boolean):void}
         arg types: [java.io.StringWriter, int]
         candidates:
          ClspMth{java.io.PrintWriter.<init>(java.io.File, java.lang.String):void throws java.io.FileNotFoundException, java.io.UnsupportedEncodingException}
          ClspMth{java.io.PrintWriter.<init>(java.io.OutputStream, boolean):void}
          ClspMth{java.io.PrintWriter.<init>(java.lang.String, java.lang.String):void throws java.io.FileNotFoundException, java.io.UnsupportedEncodingException}
          ClspMth{java.io.PrintWriter.<init>(java.io.Writer, boolean):void} */
        public final String b(Throwable th) {
            StringWriter stringWriter = new StringWriter(256);
            PrintWriter printWriter = new PrintWriter((Writer) stringWriter, false);
            th.printStackTrace(printWriter);
            printWriter.flush();
            return stringWriter.toString();
        }

        public void a(String str, Object... objArr) {
            a(6, (Throwable) null, str, objArr);
        }

        public void a(Throwable th, String str, Object... objArr) {
            a(6, th, str, objArr);
        }

        public void a(Throwable th) {
            a(6, th, (String) null, new Object[0]);
        }
    }

    public static void a(b bVar) {
        if (bVar == null) {
            throw new NullPointerException("tree == null");
        } else if (bVar != d) {
            synchronized (b) {
                b.add(bVar);
                c = (b[]) b.toArray(new b[b.size()]);
            }
        } else {
            throw new IllegalArgumentException("Cannot plant Timber into itself.");
        }
    }

    public static class a extends b {
        public void a(String str, Object... objArr) {
            for (b a : Timber.c) {
                a.a(str, objArr);
            }
        }

        public void b(String str, Object... objArr) {
            for (b b : Timber.c) {
                b.b(str, objArr);
            }
        }

        public void a(Throwable th, String str, Object... objArr) {
            for (b a : Timber.c) {
                a.a(th, str, objArr);
            }
        }

        public void a(Throwable th) {
            for (b a : Timber.c) {
                a.a(th);
            }
        }

        public void a(int i2, String str, String str2, Throwable th) {
            throw new AssertionError("Missing override for log method.");
        }
    }
}
