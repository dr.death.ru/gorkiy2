package j.b.a.m.n;

import j.b.a.f;
import j.b.a.m.DataSource;
import j.b.a.m.Options;
import j.b.a.m.l.DataFetcher;
import j.b.a.m.n.ModelLoader;
import j.b.a.r.ObjectKey;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;

public class ByteArrayLoader<Data> implements ModelLoader<byte[], Data> {
    public final b<Data> a;

    public static class a implements ModelLoaderFactory<byte[], ByteBuffer> {

        /* renamed from: j.b.a.m.n.ByteArrayLoader$a$a  reason: collision with other inner class name */
        public class C0014a implements b<ByteBuffer> {
            public C0014a(a aVar) {
            }

            public Object a(byte[] bArr) {
                return ByteBuffer.wrap(bArr);
            }

            public Class<ByteBuffer> a() {
                return ByteBuffer.class;
            }
        }

        public ModelLoader<byte[], ByteBuffer> a(r rVar) {
            return new ByteArrayLoader(new C0014a(this));
        }
    }

    public interface b<Data> {
        Class<Data> a();

        Data a(byte[] bArr);
    }

    public static class d implements ModelLoaderFactory<byte[], InputStream> {

        public class a implements b<InputStream> {
            public a(d dVar) {
            }

            public Object a(byte[] bArr) {
                return new ByteArrayInputStream(bArr);
            }

            public Class<InputStream> a() {
                return InputStream.class;
            }
        }

        public ModelLoader<byte[], InputStream> a(r rVar) {
            return new ByteArrayLoader(new a(this));
        }
    }

    public ByteArrayLoader(b<Data> bVar) {
        this.a = bVar;
    }

    public ModelLoader.a a(Object obj, int i2, int i3, Options options) {
        byte[] bArr = (byte[]) obj;
        return new ModelLoader.a(new ObjectKey(bArr), new c(bArr, this.a));
    }

    public static class c<Data> implements DataFetcher<Data> {
        public final byte[] b;
        public final b<Data> c;

        public c(byte[] bArr, b<Data> bVar) {
            this.b = bArr;
            this.c = bVar;
        }

        public void a(f fVar, DataFetcher.a<? super Data> aVar) {
            aVar.a((Object) this.c.a(this.b));
        }

        public void b() {
        }

        public DataSource c() {
            return DataSource.LOCAL;
        }

        public void cancel() {
        }

        public Class<Data> a() {
            return this.c.a();
        }
    }

    public boolean a(Object obj) {
        byte[] bArr = (byte[]) obj;
        return true;
    }
}
