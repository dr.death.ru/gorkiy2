package j.b.a.m.l;

import android.content.res.AssetManager;
import java.io.InputStream;

public class StreamAssetPathFetcher extends AssetPathFetcher<InputStream> {
    public StreamAssetPathFetcher(AssetManager assetManager, String str) {
        super(assetManager, str);
    }

    public void a(Object obj) {
        ((InputStream) obj).close();
    }

    public Object a(AssetManager assetManager, String str) {
        return assetManager.open(str);
    }

    public Class<InputStream> a() {
        return InputStream.class;
    }
}
