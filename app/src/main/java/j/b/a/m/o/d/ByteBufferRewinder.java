package j.b.a.m.o.d;

import j.b.a.m.l.DataRewinder;
import java.nio.ByteBuffer;

public class ByteBufferRewinder implements DataRewinder<ByteBuffer> {
    public final ByteBuffer a;

    public ByteBufferRewinder(ByteBuffer byteBuffer) {
        this.a = byteBuffer;
    }

    public Object a() {
        this.a.position(0);
        return this.a;
    }

    public void b() {
    }

    public static class a implements DataRewinder.a<ByteBuffer> {
        public DataRewinder a(Object obj) {
            return new ByteBufferRewinder((ByteBuffer) obj);
        }

        public Class<ByteBuffer> a() {
            return ByteBuffer.class;
        }
    }
}
