package j.b.a.m.m;

import android.os.Build;
import android.util.Log;
import com.bumptech.glide.load.engine.GlideException;
import i.h.k.Pools;
import j.a.a.a.outline;
import j.b.a.GlideContext;
import j.b.a.Priority;
import j.b.a.m.DataSource;
import j.b.a.m.Key;
import j.b.a.m.Options;
import j.b.a.m.ResourceEncoder;
import j.b.a.m.l.DataFetcher;
import j.b.a.m.l.DataRewinder;
import j.b.a.m.m.DecodePath;
import j.b.a.m.m.g;
import j.b.a.m.m.i;
import j.b.a.m.o.c.Downsampler;
import j.b.a.s.LogTime;
import j.b.a.s.k.StateVerifier;
import j.b.a.s.k.a;
import java.util.ArrayList;
import java.util.List;

public class DecodeJob<R> implements g.a, Runnable, Comparable<DecodeJob<?>>, a.d {
    public Object A;
    public DataSource B;
    public DataFetcher<?> C;
    public volatile DataFetcherGenerator D;
    public volatile boolean E;
    public volatile boolean F;
    public final DecodeHelper<R> b = new DecodeHelper<>();
    public final List<Throwable> c = new ArrayList();
    public final StateVerifier d = new StateVerifier.b();

    /* renamed from: e  reason: collision with root package name */
    public final d f1622e;

    /* renamed from: f  reason: collision with root package name */
    public final Pools<DecodeJob<?>> f1623f;
    public final c<?> g = new c<>();
    public final e h = new e();

    /* renamed from: i  reason: collision with root package name */
    public GlideContext f1624i;

    /* renamed from: j  reason: collision with root package name */
    public Key f1625j;

    /* renamed from: k  reason: collision with root package name */
    public Priority f1626k;

    /* renamed from: l  reason: collision with root package name */
    public EngineKey f1627l;

    /* renamed from: m  reason: collision with root package name */
    public int f1628m;

    /* renamed from: n  reason: collision with root package name */
    public int f1629n;

    /* renamed from: o  reason: collision with root package name */
    public DiskCacheStrategy f1630o;

    /* renamed from: p  reason: collision with root package name */
    public Options f1631p;

    /* renamed from: q  reason: collision with root package name */
    public a<R> f1632q;

    /* renamed from: r  reason: collision with root package name */
    public int f1633r;

    /* renamed from: s  reason: collision with root package name */
    public g f1634s;

    /* renamed from: t  reason: collision with root package name */
    public f f1635t;
    public long u;
    public boolean v;
    public Object w;
    public Thread x;
    public Key y;
    public Key z;

    public interface a<R> {
    }

    public final class b<Z> implements DecodePath.a<Z> {
        public final DataSource a;

        public b(DataSource dataSource) {
            this.a = dataSource;
        }
    }

    public static class c<Z> {
        public Key a;
        public ResourceEncoder<Z> b;
        public LockedResource<Z> c;
    }

    public interface d {
    }

    public enum f {
        INITIALIZE,
        SWITCH_TO_SOURCE_SERVICE,
        DECODE_DATA
    }

    public enum g {
        INITIALIZE,
        RESOURCE_CACHE,
        DATA_CACHE,
        SOURCE,
        ENCODE,
        FINISHED
    }

    public DecodeJob(i.d dVar, Pools<DecodeJob<?>> pools) {
        this.f1622e = dVar;
        this.f1623f = pools;
    }

    public final g a(g gVar) {
        int ordinal = gVar.ordinal();
        if (ordinal != 0) {
            if (ordinal != 1) {
                if (ordinal == 2) {
                    return this.v ? g.FINISHED : g.SOURCE;
                }
                if (ordinal == 3 || ordinal == 5) {
                    return g.FINISHED;
                }
                throw new IllegalArgumentException("Unrecognized stage: " + gVar);
            } else if (this.f1630o.a()) {
                return g.DATA_CACHE;
            } else {
                return a(g.DATA_CACHE);
            }
        } else if (this.f1630o.b()) {
            return g.RESOURCE_CACHE;
        } else {
            return a(g.RESOURCE_CACHE);
        }
    }

    public int compareTo(Object obj) {
        DecodeJob decodeJob = (DecodeJob) obj;
        int ordinal = this.f1626k.ordinal() - decodeJob.f1626k.ordinal();
        return ordinal == 0 ? this.f1633r - decodeJob.f1633r : ordinal;
    }

    public void f() {
        this.f1635t = f.SWITCH_TO_SOURCE_SERVICE;
        ((EngineJob) this.f1632q).a(this);
    }

    public StateVerifier g() {
        return this.d;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r5v2, types: [j.b.a.m.m.c0.DiskCache$b, j.b.a.m.m.DataCacheWriter] */
    /* JADX WARN: Type inference failed for: r1v12, types: [j.b.a.m.m.Resource] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.b.a.m.m.DecodeJob.a(j.b.a.m.l.DataFetcher<?>, java.lang.Object, j.b.a.m.a):j.b.a.m.m.Resource<R>
     arg types: [j.b.a.m.l.DataFetcher<?>, java.lang.Object, j.b.a.m.DataSource]
     candidates:
      j.b.a.m.m.DecodeJob.a(java.lang.String, long, java.lang.String):void
      j.b.a.m.m.DecodeJob.a(j.b.a.m.l.DataFetcher<?>, java.lang.Object, j.b.a.m.a):j.b.a.m.m.Resource<R> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.b.a.m.m.EngineJob.a(j.b.a.m.m.Resource, j.b.a.m.a):void
     arg types: [j.b.a.m.m.LockedResource, j.b.a.m.DataSource]
     candidates:
      j.b.a.m.m.EngineJob.a(j.b.a.q.ResourceCallback, java.util.concurrent.Executor):void
      j.b.a.m.m.EngineJob.a(j.b.a.m.m.Resource, j.b.a.m.a):void */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void h() {
        /*
            r8 = this;
            java.lang.String r0 = "DecodeJob"
            r1 = 2
            boolean r0 = android.util.Log.isLoggable(r0, r1)
            if (r0 == 0) goto L_0x0033
            long r0 = r8.u
            java.lang.String r2 = "data: "
            java.lang.StringBuilder r2 = j.a.a.a.outline.a(r2)
            java.lang.Object r3 = r8.A
            r2.append(r3)
            java.lang.String r3 = ", cache key: "
            r2.append(r3)
            j.b.a.m.Key r3 = r8.y
            r2.append(r3)
            java.lang.String r3 = ", fetcher: "
            r2.append(r3)
            j.b.a.m.l.DataFetcher<?> r3 = r8.C
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            java.lang.String r3 = "Retrieved data"
            r8.a(r3, r0, r2)
        L_0x0033:
            r0 = 0
            j.b.a.m.l.DataFetcher<?> r1 = r8.C     // Catch:{ GlideException -> 0x003f }
            java.lang.Object r2 = r8.A     // Catch:{ GlideException -> 0x003f }
            j.b.a.m.DataSource r3 = r8.B     // Catch:{ GlideException -> 0x003f }
            j.b.a.m.m.Resource r1 = r8.a(r1, r2, r3)     // Catch:{ GlideException -> 0x003f }
            goto L_0x0050
        L_0x003f:
            r1 = move-exception
            j.b.a.m.Key r2 = r8.z
            j.b.a.m.DataSource r3 = r8.B
            r1.c = r2
            r1.d = r3
            r1.f372e = r0
            java.util.List<java.lang.Throwable> r2 = r8.c
            r2.add(r1)
            r1 = r0
        L_0x0050:
            if (r1 == 0) goto L_0x00cb
            j.b.a.m.DataSource r2 = r8.B
            boolean r3 = r1 instanceof j.b.a.m.m.Initializable
            if (r3 == 0) goto L_0x005e
            r3 = r1
            j.b.a.m.m.Initializable r3 = (j.b.a.m.m.Initializable) r3
            r3.a()
        L_0x005e:
            j.b.a.m.m.DecodeJob$c<?> r3 = r8.g
            j.b.a.m.m.LockedResource<Z> r3 = r3.c
            r4 = 1
            if (r3 == 0) goto L_0x0067
            r3 = 1
            goto L_0x0068
        L_0x0067:
            r3 = 0
        L_0x0068:
            if (r3 == 0) goto L_0x0070
            j.b.a.m.m.LockedResource r1 = j.b.a.m.m.LockedResource.a(r1)
            r3 = r1
            goto L_0x0071
        L_0x0070:
            r3 = r0
        L_0x0071:
            r8.q()
            j.b.a.m.m.DecodeJob$a<R> r5 = r8.f1632q
            j.b.a.m.m.EngineJob r5 = (j.b.a.m.m.EngineJob) r5
            r5.a(r1, r2)
            j.b.a.m.m.DecodeJob$g r1 = j.b.a.m.m.DecodeJob.g.ENCODE
            r8.f1634s = r1
            j.b.a.m.m.DecodeJob$c<?> r1 = r8.g     // Catch:{ all -> 0x00c4 }
            j.b.a.m.m.LockedResource<Z> r1 = r1.c     // Catch:{ all -> 0x00c4 }
            if (r1 == 0) goto L_0x0086
            goto L_0x0087
        L_0x0086:
            r4 = 0
        L_0x0087:
            if (r4 == 0) goto L_0x00b3
            j.b.a.m.m.DecodeJob$c<?> r1 = r8.g     // Catch:{ all -> 0x00c4 }
            j.b.a.m.m.DecodeJob$d r2 = r8.f1622e     // Catch:{ all -> 0x00c4 }
            j.b.a.m.Options r4 = r8.f1631p     // Catch:{ all -> 0x00c4 }
            if (r1 == 0) goto L_0x00b2
            j.b.a.m.m.Engine$c r2 = (j.b.a.m.m.Engine.c) r2     // Catch:{ all -> 0x00c4 }
            j.b.a.m.m.c0.DiskCache r0 = r2.a()     // Catch:{ all -> 0x00ab }
            j.b.a.m.Key r2 = r1.a     // Catch:{ all -> 0x00ab }
            j.b.a.m.m.DataCacheWriter r5 = new j.b.a.m.m.DataCacheWriter     // Catch:{ all -> 0x00ab }
            j.b.a.m.ResourceEncoder<Z> r6 = r1.b     // Catch:{ all -> 0x00ab }
            j.b.a.m.m.LockedResource<Z> r7 = r1.c     // Catch:{ all -> 0x00ab }
            r5.<init>(r6, r7, r4)     // Catch:{ all -> 0x00ab }
            r0.a(r2, r5)     // Catch:{ all -> 0x00ab }
            j.b.a.m.m.LockedResource<Z> r0 = r1.c     // Catch:{ all -> 0x00c4 }
            r0.a()     // Catch:{ all -> 0x00c4 }
            goto L_0x00b3
        L_0x00ab:
            r0 = move-exception
            j.b.a.m.m.LockedResource<Z> r1 = r1.c     // Catch:{ all -> 0x00c4 }
            r1.a()     // Catch:{ all -> 0x00c4 }
            throw r0     // Catch:{ all -> 0x00c4 }
        L_0x00b2:
            throw r0     // Catch:{ all -> 0x00c4 }
        L_0x00b3:
            if (r3 == 0) goto L_0x00b8
            r3.a()
        L_0x00b8:
            j.b.a.m.m.DecodeJob$e r0 = r8.h
            boolean r0 = r0.a()
            if (r0 == 0) goto L_0x00ce
            r8.l()
            goto L_0x00ce
        L_0x00c4:
            r0 = move-exception
            if (r3 == 0) goto L_0x00ca
            r3.a()
        L_0x00ca:
            throw r0
        L_0x00cb:
            r8.o()
        L_0x00ce:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: j.b.a.m.m.DecodeJob.h():void");
    }

    /* JADX WARN: Type inference failed for: r0v2, types: [j.b.a.m.m.DataFetcherGenerator, j.b.a.m.m.ResourceCacheGenerator] */
    /* JADX WARN: Type inference failed for: r0v3, types: [j.b.a.m.m.DataCacheGenerator, j.b.a.m.m.DataFetcherGenerator] */
    public final DataFetcherGenerator i() {
        int ordinal = this.f1634s.ordinal();
        if (ordinal == 1) {
            return new ResourceCacheGenerator(this.b, this);
        }
        if (ordinal == 2) {
            return new DataCacheGenerator(this.b, this);
        }
        if (ordinal == 3) {
            return new SourceGenerator(this.b, this);
        }
        if (ordinal == 5) {
            return null;
        }
        StringBuilder a2 = outline.a("Unrecognized stage: ");
        a2.append(this.f1634s);
        throw new IllegalStateException(a2.toString());
    }

    public final void j() {
        q();
        ((EngineJob) this.f1632q).a(new GlideException("Failed to load resource", new ArrayList(this.c)));
        if (this.h.b()) {
            l();
        }
    }

    public final void l() {
        this.h.c();
        c<?> cVar = this.g;
        cVar.a = null;
        cVar.b = null;
        cVar.c = null;
        DecodeHelper<R> decodeHelper = this.b;
        decodeHelper.c = null;
        decodeHelper.d = null;
        decodeHelper.f1617n = null;
        decodeHelper.g = null;
        decodeHelper.f1614k = null;
        decodeHelper.f1612i = null;
        decodeHelper.f1618o = null;
        decodeHelper.f1613j = null;
        decodeHelper.f1619p = null;
        decodeHelper.a.clear();
        decodeHelper.f1615l = false;
        decodeHelper.b.clear();
        decodeHelper.f1616m = false;
        this.E = false;
        this.f1624i = null;
        this.f1625j = null;
        this.f1631p = null;
        this.f1626k = null;
        this.f1627l = null;
        this.f1632q = null;
        this.f1634s = null;
        this.D = null;
        this.x = null;
        this.y = null;
        this.A = null;
        this.B = null;
        this.C = null;
        this.u = 0;
        this.F = false;
        this.w = null;
        this.c.clear();
        this.f1623f.a(this);
    }

    public final void o() {
        this.x = Thread.currentThread();
        this.u = LogTime.a();
        boolean z2 = false;
        while (!this.F && this.D != null && !(z2 = this.D.a())) {
            this.f1634s = a(this.f1634s);
            this.D = i();
            if (this.f1634s == g.SOURCE) {
                this.f1635t = f.SWITCH_TO_SOURCE_SERVICE;
                ((EngineJob) this.f1632q).a(this);
                return;
            }
        }
        if ((this.f1634s == g.FINISHED || this.F) && !z2) {
            j();
        }
    }

    public final void p() {
        int ordinal = this.f1635t.ordinal();
        if (ordinal == 0) {
            this.f1634s = a(g.INITIALIZE);
            this.D = i();
            o();
        } else if (ordinal == 1) {
            o();
        } else if (ordinal == 2) {
            h();
        } else {
            StringBuilder a2 = outline.a("Unrecognized run reason: ");
            a2.append(this.f1635t);
            throw new IllegalStateException(a2.toString());
        }
    }

    public final void q() {
        Throwable th;
        this.d.a();
        if (this.E) {
            if (this.c.isEmpty()) {
                th = null;
            } else {
                List<Throwable> list = this.c;
                th = list.get(list.size() - 1);
            }
            throw new IllegalStateException("Already notified", th);
        }
        this.E = true;
    }

    public void run() {
        DataFetcher<?> dataFetcher = this.C;
        try {
            if (this.F) {
                j();
                if (dataFetcher != null) {
                    dataFetcher.b();
                    return;
                }
                return;
            }
            p();
            if (dataFetcher != null) {
                dataFetcher.b();
            }
        } catch (CallbackException e2) {
            throw e2;
        } catch (Throwable th) {
            if (dataFetcher != null) {
                dataFetcher.b();
            }
            throw th;
        }
    }

    public static class e {
        public boolean a;
        public boolean b;
        public boolean c;

        public synchronized boolean a() {
            this.b = true;
            return a(false);
        }

        public synchronized boolean b(boolean z) {
            this.a = true;
            return a(z);
        }

        public synchronized void c() {
            this.b = false;
            this.a = false;
            this.c = false;
        }

        public final boolean a(boolean z) {
            return (this.c || z || this.b) && this.a;
        }

        public synchronized boolean b() {
            this.c = true;
            return a(false);
        }
    }

    public void a(j.b.a.m.e eVar, Object obj, DataFetcher<?> dataFetcher, j.b.a.m.a aVar, j.b.a.m.e eVar2) {
        this.y = eVar;
        this.A = obj;
        this.C = dataFetcher;
        this.B = aVar;
        this.z = eVar2;
        if (Thread.currentThread() != this.x) {
            this.f1635t = f.DECODE_DATA;
            ((EngineJob) this.f1632q).a(this);
            return;
        }
        h();
    }

    public void a(j.b.a.m.e eVar, Exception exc, DataFetcher<?> dataFetcher, j.b.a.m.a aVar) {
        dataFetcher.b();
        GlideException glideException = new GlideException("Fetching data failed", exc);
        Class<?> a2 = dataFetcher.a();
        glideException.c = eVar;
        glideException.d = aVar;
        glideException.f372e = a2;
        this.c.add(glideException);
        if (Thread.currentThread() != this.x) {
            this.f1635t = f.SWITCH_TO_SOURCE_SERVICE;
            ((EngineJob) this.f1632q).a(this);
            return;
        }
        o();
    }

    public final <Data> Resource<R> a(DataFetcher<?> dataFetcher, Data data, j.b.a.m.a aVar) {
        if (data == null) {
            dataFetcher.b();
            return null;
        }
        try {
            long a2 = LogTime.a();
            Resource<R> a3 = a(data, aVar);
            if (Log.isLoggable("DecodeJob", 2)) {
                a("Decoded result " + a3, a2, (String) null);
            }
            return a3;
        } finally {
            dataFetcher.b();
        }
    }

    public final <Data> Resource<R> a(Data data, j.b.a.m.a aVar) {
        LoadPath<Data, ?, R> a2 = this.b.a(data.getClass());
        Options options = this.f1631p;
        if (Build.VERSION.SDK_INT >= 26) {
            boolean z2 = aVar == DataSource.RESOURCE_DISK_CACHE || this.b.f1621r;
            Boolean bool = (Boolean) options.a(Downsampler.f1706i);
            if (bool == null || (bool.booleanValue() && !z2)) {
                options = new Options();
                options.a(this.f1631p);
                options.b.put(Downsampler.f1706i, Boolean.valueOf(z2));
            }
        }
        Options options2 = options;
        DataRewinder a3 = this.f1624i.b.f368e.a((Object) data);
        try {
            return a2.a(a3, options2, this.f1628m, this.f1629n, new b(aVar));
        } finally {
            a3.b();
        }
    }

    public final void a(String str, long j2, String str2) {
        StringBuilder b2 = outline.b(str, " in ");
        b2.append(LogTime.a(j2));
        b2.append(", load key: ");
        b2.append(this.f1627l);
        b2.append(str2 != null ? outline.a(", ", str2) : "");
        b2.append(", thread: ");
        b2.append(Thread.currentThread().getName());
        Log.v("DecodeJob", b2.toString());
    }
}
