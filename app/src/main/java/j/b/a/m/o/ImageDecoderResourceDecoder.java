package j.b.a.m.o;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.ColorSpace;
import android.graphics.ImageDecoder;
import android.os.Build;
import android.util.Log;
import android.util.Size;
import j.a.a.a.outline;
import j.b.a.m.DecodeFormat;
import j.b.a.m.Options;
import j.b.a.m.PreferredColorSpace;
import j.b.a.m.ResourceDecoder;
import j.b.a.m.g;
import j.b.a.m.m.Resource;
import j.b.a.m.o.c.BitmapImageDecoderResourceDecoder;
import j.b.a.m.o.c.BitmapResource;
import j.b.a.m.o.c.DownsampleStrategy;
import j.b.a.m.o.c.Downsampler;
import j.b.a.m.o.c.HardwareConfigState;

public abstract class ImageDecoderResourceDecoder<T> implements ResourceDecoder<ImageDecoder.Source, T> {
    public final HardwareConfigState a = HardwareConfigState.b();

    public class a implements ImageDecoder.OnHeaderDecodedListener {
        public final /* synthetic */ int a;
        public final /* synthetic */ int b;
        public final /* synthetic */ boolean c;
        public final /* synthetic */ DecodeFormat d;

        /* renamed from: e  reason: collision with root package name */
        public final /* synthetic */ DownsampleStrategy f1701e;

        /* renamed from: f  reason: collision with root package name */
        public final /* synthetic */ PreferredColorSpace f1702f;

        /* renamed from: j.b.a.m.o.ImageDecoderResourceDecoder$a$a  reason: collision with other inner class name */
        public class C0016a implements ImageDecoder.OnPartialImageListener {
            public C0016a(a aVar) {
            }

            public boolean onPartialImage(ImageDecoder.DecodeException decodeException) {
                return false;
            }
        }

        public a(int i2, int i3, boolean z, DecodeFormat decodeFormat, DownsampleStrategy downsampleStrategy, PreferredColorSpace preferredColorSpace) {
            this.a = i2;
            this.b = i3;
            this.c = z;
            this.d = decodeFormat;
            this.f1701e = downsampleStrategy;
            this.f1702f = preferredColorSpace;
        }

        @SuppressLint({"Override"})
        public void onHeaderDecoded(ImageDecoder imageDecoder, ImageDecoder.ImageInfo imageInfo, ImageDecoder.Source source) {
            boolean z = false;
            if (ImageDecoderResourceDecoder.this.a.a(this.a, this.b, this.c, false)) {
                imageDecoder.setAllocator(3);
            } else {
                imageDecoder.setAllocator(1);
            }
            if (this.d == DecodeFormat.PREFER_RGB_565) {
                imageDecoder.setMemorySizePolicy(0);
            }
            imageDecoder.setOnPartialImageListener(new C0016a(this));
            Size size = imageInfo.getSize();
            int i2 = this.a;
            if (i2 == Integer.MIN_VALUE) {
                i2 = size.getWidth();
            }
            int i3 = this.b;
            if (i3 == Integer.MIN_VALUE) {
                i3 = size.getHeight();
            }
            float b2 = this.f1701e.b(size.getWidth(), size.getHeight(), i2, i3);
            int round = Math.round(((float) size.getWidth()) * b2);
            int round2 = Math.round(((float) size.getHeight()) * b2);
            if (Log.isLoggable("ImageDecoder", 2)) {
                StringBuilder a2 = outline.a("Resizing from [");
                a2.append(size.getWidth());
                a2.append("x");
                a2.append(size.getHeight());
                a2.append("] to [");
                a2.append(round);
                a2.append("x");
                a2.append(round2);
                a2.append("] scaleFactor: ");
                a2.append(b2);
                Log.v("ImageDecoder", a2.toString());
            }
            imageDecoder.setTargetSize(round, round2);
            int i4 = Build.VERSION.SDK_INT;
            if (i4 >= 28) {
                if (this.f1702f == PreferredColorSpace.DISPLAY_P3 && imageInfo.getColorSpace() != null && imageInfo.getColorSpace().isWideGamut()) {
                    z = true;
                }
                imageDecoder.setTargetColorSpace(ColorSpace.get(z ? ColorSpace.Named.DISPLAY_P3 : ColorSpace.Named.SRGB));
            } else if (i4 >= 26) {
                imageDecoder.setTargetColorSpace(ColorSpace.get(ColorSpace.Named.SRGB));
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.b.a.m.o.ImageDecoderResourceDecoder.a(android.graphics.ImageDecoder$Source, int, int, j.b.a.m.g):j.b.a.m.m.Resource<T>
     arg types: [android.graphics.ImageDecoder$Source, int, int, j.b.a.m.Options]
     candidates:
      j.b.a.m.o.ImageDecoderResourceDecoder.a(java.lang.Object, int, int, j.b.a.m.Options):j.b.a.m.m.Resource
      j.b.a.m.ResourceDecoder.a(java.lang.Object, int, int, j.b.a.m.g):j.b.a.m.m.Resource<Z>
      j.b.a.m.o.ImageDecoderResourceDecoder.a(android.graphics.ImageDecoder$Source, int, int, j.b.a.m.g):j.b.a.m.m.Resource<T> */
    public /* bridge */ /* synthetic */ Resource a(Object obj, int i2, int i3, Options options) {
        return a((ImageDecoder.Source) obj, i2, i3, (g) options);
    }

    public boolean a(Object obj, Options options) {
        ImageDecoder.Source source = (ImageDecoder.Source) obj;
        return true;
    }

    public final Resource<T> a(ImageDecoder.Source source, int i2, int i3, g gVar) {
        BitmapImageDecoderResourceDecoder bitmapImageDecoderResourceDecoder = (BitmapImageDecoderResourceDecoder) this;
        Bitmap decodeBitmap = ImageDecoder.decodeBitmap(source, new a(i2, i3, gVar.a(Downsampler.f1706i) != null && ((Boolean) gVar.a(Downsampler.f1706i)).booleanValue(), (DecodeFormat) gVar.a(Downsampler.f1705f), (DownsampleStrategy) gVar.a(DownsampleStrategy.f1704f), (PreferredColorSpace) gVar.a(Downsampler.g)));
        if (Log.isLoggable("BitmapImageDecoder", 2)) {
            StringBuilder a2 = outline.a("Decoded [");
            a2.append(decodeBitmap.getWidth());
            a2.append("x");
            a2.append(decodeBitmap.getHeight());
            a2.append("] for [");
            a2.append(i2);
            a2.append("x");
            a2.append(i3);
            a2.append("]");
            Log.v("BitmapImageDecoder", a2.toString());
        }
        return new BitmapResource(decodeBitmap, bitmapImageDecoderResourceDecoder.b);
    }
}
