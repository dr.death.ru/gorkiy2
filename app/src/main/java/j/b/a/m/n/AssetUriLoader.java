package j.b.a.m.n;

import android.content.res.AssetManager;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import j.b.a.m.Options;
import j.b.a.m.l.DataFetcher;
import j.b.a.m.l.FileDescriptorAssetPathFetcher;
import j.b.a.m.l.StreamAssetPathFetcher;
import j.b.a.m.n.ModelLoader;
import j.b.a.r.ObjectKey;
import java.io.InputStream;

public class AssetUriLoader<Data> implements ModelLoader<Uri, Data> {
    public final AssetManager a;
    public final a<Data> b;

    public interface a<Data> {
        DataFetcher<Data> a(AssetManager assetManager, String str);
    }

    public static class b implements ModelLoaderFactory<Uri, ParcelFileDescriptor>, a<ParcelFileDescriptor> {
        public final AssetManager a;

        public b(AssetManager assetManager) {
            this.a = assetManager;
        }

        public ModelLoader<Uri, ParcelFileDescriptor> a(r rVar) {
            return new AssetUriLoader(this.a, this);
        }

        public DataFetcher<ParcelFileDescriptor> a(AssetManager assetManager, String str) {
            return new FileDescriptorAssetPathFetcher(assetManager, str);
        }
    }

    public static class c implements ModelLoaderFactory<Uri, InputStream>, a<InputStream> {
        public final AssetManager a;

        public c(AssetManager assetManager) {
            this.a = assetManager;
        }

        public ModelLoader<Uri, InputStream> a(r rVar) {
            return new AssetUriLoader(this.a, this);
        }

        public DataFetcher<InputStream> a(AssetManager assetManager, String str) {
            return new StreamAssetPathFetcher(assetManager, str);
        }
    }

    public AssetUriLoader(AssetManager assetManager, a<Data> aVar) {
        this.a = assetManager;
        this.b = aVar;
    }

    public ModelLoader.a a(Object obj, int i2, int i3, Options options) {
        Uri uri = (Uri) obj;
        return new ModelLoader.a(new ObjectKey(uri), this.b.a(this.a, uri.toString().substring(22)));
    }

    public boolean a(Object obj) {
        Uri uri = (Uri) obj;
        if (!"file".equals(uri.getScheme()) || uri.getPathSegments().isEmpty() || !"android_asset".equals(uri.getPathSegments().get(0))) {
            return false;
        }
        return true;
    }
}
