package j.b.a.m.o.g;

import android.util.Log;
import j.b.a.m.EncodeStrategy;
import j.b.a.m.Options;
import j.b.a.m.ResourceEncoder;
import j.b.a.m.m.Resource;
import j.b.a.s.ByteBufferUtil;
import java.io.File;
import java.io.IOException;

public class GifDrawableEncoder implements ResourceEncoder<c> {
    public boolean a(Object obj, File file, Options options) {
        try {
            ByteBufferUtil.a(((GifDrawable) ((Resource) obj).get()).b.a.a.g().asReadOnlyBuffer(), file);
            return true;
        } catch (IOException e2) {
            if (Log.isLoggable("GifEncoder", 5)) {
                Log.w("GifEncoder", "Failed to encode GIF drawable data", e2);
            }
            return false;
        }
    }

    public EncodeStrategy a(Options options) {
        return EncodeStrategy.SOURCE;
    }
}
