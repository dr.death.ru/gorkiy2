package j.b.a.m.o.e;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import j.a.a.a.outline;
import j.b.a.m.Options;
import j.b.a.m.ResourceDecoder;
import j.b.a.m.m.Resource;
import java.util.List;
import l.a.a.a.o.b.AbstractSpiCall;

public class ResourceDrawableDecoder implements ResourceDecoder<Uri, Drawable> {
    public final Context a;

    public ResourceDrawableDecoder(Context context) {
        this.a = context.getApplicationContext();
    }

    public /* bridge */ /* synthetic */ Resource a(Object obj, int i2, int i3, Options options) {
        return a((Uri) obj);
    }

    public boolean a(Object obj, Options options) {
        return ((Uri) obj).getScheme().equals("android.resource");
    }

    public Resource a(Uri uri) {
        Context context;
        int i2;
        String authority = uri.getAuthority();
        if (authority.equals(this.a.getPackageName())) {
            context = this.a;
        } else {
            try {
                context = this.a.createPackageContext(authority, 0);
            } catch (PackageManager.NameNotFoundException e2) {
                if (authority.contains(this.a.getPackageName())) {
                    context = this.a;
                } else {
                    throw new IllegalArgumentException(outline.a("Failed to obtain context or unrecognized Uri format for: ", uri), e2);
                }
            }
        }
        List<String> pathSegments = uri.getPathSegments();
        if (pathSegments.size() == 2) {
            List<String> pathSegments2 = uri.getPathSegments();
            String authority2 = uri.getAuthority();
            String str = pathSegments2.get(0);
            String str2 = pathSegments2.get(1);
            i2 = context.getResources().getIdentifier(str2, str, authority2);
            if (i2 == 0) {
                i2 = Resources.getSystem().getIdentifier(str2, str, AbstractSpiCall.ANDROID_CLIENT_TYPE);
            }
            if (i2 == 0) {
                throw new IllegalArgumentException(outline.a("Failed to find resource id for: ", uri));
            }
        } else if (pathSegments.size() == 1) {
            try {
                i2 = Integer.parseInt(uri.getPathSegments().get(0));
            } catch (NumberFormatException e3) {
                throw new IllegalArgumentException(outline.a("Unrecognized Uri format: ", uri), e3);
            }
        } else {
            throw new IllegalArgumentException(outline.a("Unrecognized Uri format: ", uri));
        }
        Drawable a2 = DrawableDecoderCompat.a(this.a, context, i2, null);
        if (a2 != null) {
            return new NonOwnedDrawableResource(a2);
        }
        return null;
    }
}
