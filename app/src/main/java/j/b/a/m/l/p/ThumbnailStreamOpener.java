package j.b.a.m.l.p;

import android.content.ContentResolver;
import com.bumptech.glide.load.ImageHeaderParser;
import j.b.a.m.m.b0.ArrayPool;
import j.b.a.m.m.b0.b;
import java.util.List;

public class ThumbnailStreamOpener {

    /* renamed from: f  reason: collision with root package name */
    public static final FileService f1604f = new FileService();
    public final FileService a = f1604f;
    public final ThumbnailQuery b;
    public final ArrayPool c;
    public final ContentResolver d;

    /* renamed from: e  reason: collision with root package name */
    public final List<ImageHeaderParser> f1605e;

    public ThumbnailStreamOpener(List<ImageHeaderParser> list, c cVar, b bVar, ContentResolver contentResolver) {
        this.b = cVar;
        this.c = bVar;
        this.d = contentResolver;
        this.f1605e = list;
    }
}
