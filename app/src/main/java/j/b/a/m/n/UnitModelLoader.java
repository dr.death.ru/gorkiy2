package j.b.a.m.n;

import j.b.a.f;
import j.b.a.m.DataSource;
import j.b.a.m.g;
import j.b.a.m.l.DataFetcher;
import j.b.a.m.n.ModelLoader;
import j.b.a.r.ObjectKey;

public class UnitModelLoader<Model> implements ModelLoader<Model, Model> {
    public static final UnitModelLoader<?> a = new UnitModelLoader<>();

    public static class a<Model> implements ModelLoaderFactory<Model, Model> {
        public static final a<?> a = new a<>();

        public ModelLoader<Model, Model> a(r rVar) {
            return UnitModelLoader.a;
        }
    }

    public static class b<Model> implements DataFetcher<Model> {
        public final Model b;

        public b(Model model) {
            this.b = model;
        }

        public void a(f fVar, DataFetcher.a<? super Model> aVar) {
            aVar.a((Object) this.b);
        }

        public void b() {
        }

        public DataSource c() {
            return DataSource.LOCAL;
        }

        public void cancel() {
        }

        public Class<Model> a() {
            return this.b.getClass();
        }
    }

    public ModelLoader.a<Model> a(Model model, int i2, int i3, g gVar) {
        return new ModelLoader.a<>(new ObjectKey(model), new b(model));
    }

    public boolean a(Model model) {
        return true;
    }
}
