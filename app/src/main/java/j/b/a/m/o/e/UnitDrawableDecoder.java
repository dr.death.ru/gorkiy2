package j.b.a.m.o.e;

import android.graphics.drawable.Drawable;
import j.b.a.m.Options;
import j.b.a.m.ResourceDecoder;
import j.b.a.m.m.Resource;

public class UnitDrawableDecoder implements ResourceDecoder<Drawable, Drawable> {
    public Resource a(Object obj, int i2, int i3, Options options) {
        Drawable drawable = (Drawable) obj;
        if (drawable != null) {
            return new NonOwnedDrawableResource(drawable);
        }
        return null;
    }

    public boolean a(Object obj, Options options) {
        Drawable drawable = (Drawable) obj;
        return true;
    }
}
