package j.b.a.m;

import i.e.ArrayMap;
import i.e.SimpleArrayMap;
import j.a.a.a.outline;
import j.b.a.m.Option;
import j.b.a.s.CachedHashCodeArrayMap;
import java.security.MessageDigest;

public final class Options implements Key {
    public final ArrayMap<Option<?>, Object> b = new CachedHashCodeArrayMap();

    public void a(Options options) {
        this.b.a((SimpleArrayMap) options.b);
    }

    public boolean equals(Object obj) {
        if (obj instanceof Options) {
            return this.b.equals(((Options) obj).b);
        }
        return false;
    }

    public int hashCode() {
        return this.b.hashCode();
    }

    public String toString() {
        StringBuilder a = outline.a("Options{values=");
        a.append(this.b);
        a.append('}');
        return a.toString();
    }

    public <T> T a(Option option) {
        if (this.b.a(option) >= 0) {
            return this.b.getOrDefault(option, null);
        }
        return option.a;
    }

    public void a(MessageDigest messageDigest) {
        int i2 = 0;
        while (true) {
            ArrayMap<Option<?>, Object> arrayMap = this.b;
            if (i2 < arrayMap.d) {
                Option c = arrayMap.c(i2);
                Object e2 = this.b.e(i2);
                Option.b<T> bVar = c.b;
                if (c.d == null) {
                    c.d = c.c.getBytes(Key.a);
                }
                bVar.a(c.d, e2, messageDigest);
                i2++;
            } else {
                return;
            }
        }
    }
}
