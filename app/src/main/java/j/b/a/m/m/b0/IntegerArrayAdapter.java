package j.b.a.m.m.b0;

public final class IntegerArrayAdapter implements ArrayAdapterInterface<int[]> {
    public int a(Object obj) {
        return ((int[]) obj).length;
    }

    public String a() {
        return "IntegerArrayPool";
    }

    public int b() {
        return 4;
    }

    public Object newArray(int i2) {
        return new int[i2];
    }
}
