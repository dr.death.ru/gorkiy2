package j.b.a.m.l;

import android.content.res.AssetManager;
import android.util.Log;
import j.b.a.f;
import j.b.a.m.DataSource;
import j.b.a.m.l.DataFetcher;
import java.io.IOException;

public abstract class AssetPathFetcher<T> implements DataFetcher<T> {
    public final String b;
    public final AssetManager c;
    public T d;

    public AssetPathFetcher(AssetManager assetManager, String str) {
        this.c = assetManager;
        this.b = str;
    }

    public abstract T a(AssetManager assetManager, String str);

    public void a(f fVar, DataFetcher.a<? super T> aVar) {
        try {
            T a = a(this.c, this.b);
            this.d = a;
            aVar.a((Object) a);
        } catch (IOException e2) {
            if (Log.isLoggable("AssetPathFetcher", 3)) {
                Log.d("AssetPathFetcher", "Failed to load data from asset manager", e2);
            }
            aVar.a((Exception) e2);
        }
    }

    public abstract void a(T t2);

    public void b() {
        T t2 = this.d;
        if (t2 != null) {
            try {
                a(t2);
            } catch (IOException unused) {
            }
        }
    }

    public DataSource c() {
        return DataSource.LOCAL;
    }

    public void cancel() {
    }
}
