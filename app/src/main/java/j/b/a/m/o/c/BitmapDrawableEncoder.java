package j.b.a.m.o.c;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import j.b.a.m.EncodeStrategy;
import j.b.a.m.Options;
import j.b.a.m.ResourceEncoder;
import j.b.a.m.m.Resource;
import j.b.a.m.m.b0.BitmapPool;
import j.b.a.m.m.b0.d;
import java.io.File;

public class BitmapDrawableEncoder implements ResourceEncoder<BitmapDrawable> {
    public final BitmapPool a;
    public final ResourceEncoder<Bitmap> b;

    public BitmapDrawableEncoder(d dVar, ResourceEncoder<Bitmap> resourceEncoder) {
        this.a = dVar;
        this.b = resourceEncoder;
    }

    public boolean a(Object obj, File file, Options options) {
        return this.b.a(new BitmapResource(((BitmapDrawable) ((Resource) obj).get()).getBitmap(), this.a), file, options);
    }

    public EncodeStrategy a(Options options) {
        return this.b.a(options);
    }
}
