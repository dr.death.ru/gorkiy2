package j.b.a.m.n;

import j.b.a.s.LruCache;
import j.b.a.s.Util;
import java.util.Queue;

public class ModelCache<A, B> {
    public final LruCache<b<A>, B> a;

    public class a extends LruCache<b<A>, B> {
        public a(ModelCache modelCache, long j2) {
            super(j2);
        }

        public void a(Object obj, Object obj2) {
            ((b) obj).a();
        }
    }

    public ModelCache(long j2) {
        this.a = new a(this, j2);
    }

    public static final class b<A> {
        public static final Queue<b<?>> d = Util.a(0);
        public int a;
        public int b;
        public A c;

        public static <A> b<A> a(A a2, int i2, int i3) {
            b<A> poll;
            synchronized (d) {
                poll = d.poll();
            }
            if (poll == null) {
                poll = new b<>();
            }
            poll.c = a2;
            poll.b = i2;
            poll.a = i3;
            return poll;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            if (this.b == bVar.b && this.a == bVar.a && this.c.equals(bVar.c)) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return this.c.hashCode() + (((this.a * 31) + this.b) * 31);
        }

        public void a() {
            synchronized (d) {
                d.offer(this);
            }
        }
    }
}
