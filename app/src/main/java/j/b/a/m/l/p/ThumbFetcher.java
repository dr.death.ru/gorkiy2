package j.b.a.m.l.p;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import j.b.a.Glide;
import j.b.a.f;
import j.b.a.m.DataSource;
import j.b.a.m.l.DataFetcher;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class ThumbFetcher implements DataFetcher<InputStream> {
    public final Uri b;
    public final ThumbnailStreamOpener c;
    public InputStream d;

    public static class a implements ThumbnailQuery {
        public static final String[] b = {"_data"};
        public final ContentResolver a;

        public a(ContentResolver contentResolver) {
            this.a = contentResolver;
        }

        public Cursor a(Uri uri) {
            String lastPathSegment = uri.getLastPathSegment();
            return this.a.query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, b, "kind = 1 AND image_id = ?", new String[]{lastPathSegment}, null);
        }
    }

    public static class b implements ThumbnailQuery {
        public static final String[] b = {"_data"};
        public final ContentResolver a;

        public b(ContentResolver contentResolver) {
            this.a = contentResolver;
        }

        public Cursor a(Uri uri) {
            String lastPathSegment = uri.getLastPathSegment();
            return this.a.query(MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI, b, "kind = 1 AND video_id = ?", new String[]{lastPathSegment}, null);
        }
    }

    public ThumbFetcher(Uri uri, ThumbnailStreamOpener thumbnailStreamOpener) {
        this.b = uri;
        this.c = thumbnailStreamOpener;
    }

    public static ThumbFetcher a(Context context, Uri uri, ThumbnailQuery thumbnailQuery) {
        return new ThumbFetcher(uri, new ThumbnailStreamOpener(Glide.a(context).f1534e.a(), thumbnailQuery, Glide.a(context).f1535f, context.getContentResolver()));
    }

    public void b() {
        InputStream inputStream = this.d;
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException unused) {
            }
        }
    }

    public DataSource c() {
        return DataSource.LOCAL;
    }

    public void cancel() {
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [java.lang.Throwable, java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r3v1, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r3v4 */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(java.util.List<com.bumptech.glide.load.ImageHeaderParser>, java.io.InputStream, j.b.a.m.m.b0.b):int
     arg types: [java.util.List<com.bumptech.glide.load.ImageHeaderParser>, java.io.InputStream, j.b.a.m.m.b0.ArrayPool]
     candidates:
      i.b.k.ResourcesFlusher.a(int, int, int):int
      i.b.k.ResourcesFlusher.a(android.content.Context, int, int):int
      i.b.k.ResourcesFlusher.a(java.lang.Object, android.util.Property, android.graphics.Path):android.animation.ObjectAnimator
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.content.res.Resources$Theme):android.content.res.ColorStateList
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Parcelable$Creator):T
      i.b.k.ResourcesFlusher.a(android.view.inputmethod.InputConnection, android.view.inputmethod.EditorInfo, android.view.View):android.view.inputmethod.InputConnection
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int):java.lang.String
      i.b.k.ResourcesFlusher.a(android.content.Context, android.os.CancellationSignal, android.net.Uri):java.nio.ByteBuffer
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, int):void
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, long):void
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, boolean):void
      i.b.k.ResourcesFlusher.a(android.os.Parcel, android.os.Parcelable, int):void
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetContainer, i.f.a.LinearSystem, int):void
      i.b.k.ResourcesFlusher.a(java.util.List<i.f.a.h.f>, int, int):void
      i.b.k.ResourcesFlusher.a(int, android.graphics.Rect, android.graphics.Rect):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.Rect, android.graphics.Rect, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, android.content.res.Resources, int):boolean
      i.b.k.ResourcesFlusher.a(float[], int, int):float[]
      i.b.k.ResourcesFlusher.a(java.util.List<com.bumptech.glide.load.ImageHeaderParser>, java.io.InputStream, j.b.a.m.m.b0.b):int */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0027, code lost:
        if (r6 != null) goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x004a, code lost:
        if (r6 == null) goto L_0x004f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x004c, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x004f, code lost:
        r7 = r3;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0036 A[Catch:{ all -> 0x0021 }] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x00d7  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0108  */
    /* JADX WARNING: Removed duplicated region for block: B:83:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.io.InputStream d() {
        /*
            r12 = this;
            java.lang.String r0 = "ThumbStreamOpener"
            j.b.a.m.l.p.ThumbnailStreamOpener r1 = r12.c
            android.net.Uri r2 = r12.b
            r3 = 0
            if (r1 == 0) goto L_0x010c
            r4 = 0
            r5 = 3
            j.b.a.m.l.p.ThumbnailQuery r6 = r1.b     // Catch:{ SecurityException -> 0x002d, all -> 0x002a }
            android.database.Cursor r6 = r6.a(r2)     // Catch:{ SecurityException -> 0x002d, all -> 0x002a }
            if (r6 == 0) goto L_0x0027
            boolean r7 = r6.moveToFirst()     // Catch:{ SecurityException -> 0x0025 }
            if (r7 == 0) goto L_0x0027
            java.lang.String r7 = r6.getString(r4)     // Catch:{ SecurityException -> 0x0025 }
            r6.close()
            goto L_0x0050
        L_0x0021:
            r0 = move-exception
            r3 = r6
            goto L_0x0106
        L_0x0025:
            r7 = move-exception
            goto L_0x0030
        L_0x0027:
            if (r6 == 0) goto L_0x004f
            goto L_0x004c
        L_0x002a:
            r0 = move-exception
            goto L_0x0106
        L_0x002d:
            r6 = move-exception
            r7 = r6
            r6 = r3
        L_0x0030:
            boolean r8 = android.util.Log.isLoggable(r0, r5)     // Catch:{ all -> 0x0021 }
            if (r8 == 0) goto L_0x004a
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x0021 }
            r8.<init>()     // Catch:{ all -> 0x0021 }
            java.lang.String r9 = "Failed to query for thumbnail for Uri: "
            r8.append(r9)     // Catch:{ all -> 0x0021 }
            r8.append(r2)     // Catch:{ all -> 0x0021 }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x0021 }
            android.util.Log.d(r0, r8, r7)     // Catch:{ all -> 0x0021 }
        L_0x004a:
            if (r6 == 0) goto L_0x004f
        L_0x004c:
            r6.close()
        L_0x004f:
            r7 = r3
        L_0x0050:
            boolean r6 = android.text.TextUtils.isEmpty(r7)
            if (r6 == 0) goto L_0x0058
        L_0x0056:
            r1 = r3
            goto L_0x0089
        L_0x0058:
            j.b.a.m.l.p.FileService r6 = r1.a
            if (r6 == 0) goto L_0x0105
            java.io.File r6 = new java.io.File
            r6.<init>(r7)
            j.b.a.m.l.p.FileService r7 = r1.a
            if (r7 == 0) goto L_0x0104
            boolean r7 = r6.exists()
            if (r7 == 0) goto L_0x007c
            r7 = 0
            j.b.a.m.l.p.FileService r9 = r1.a
            if (r9 == 0) goto L_0x007b
            long r9 = r6.length()
            int r11 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r11 >= 0) goto L_0x007c
            r4 = 1
            goto L_0x007c
        L_0x007b:
            throw r3
        L_0x007c:
            if (r4 != 0) goto L_0x007f
            goto L_0x0056
        L_0x007f:
            android.net.Uri r4 = android.net.Uri.fromFile(r6)
            android.content.ContentResolver r1 = r1.d     // Catch:{ NullPointerException -> 0x00de }
            java.io.InputStream r1 = r1.openInputStream(r4)     // Catch:{ NullPointerException -> 0x00de }
        L_0x0089:
            r2 = -1
            if (r1 == 0) goto L_0x00d4
            j.b.a.m.l.p.ThumbnailStreamOpener r4 = r12.c
            android.net.Uri r6 = r12.b
            if (r4 == 0) goto L_0x00d3
            android.content.ContentResolver r7 = r4.d     // Catch:{ IOException -> 0x00ac, NullPointerException -> 0x00aa }
            java.io.InputStream r3 = r7.openInputStream(r6)     // Catch:{ IOException -> 0x00ac, NullPointerException -> 0x00aa }
            java.util.List<com.bumptech.glide.load.ImageHeaderParser> r7 = r4.f1605e     // Catch:{ IOException -> 0x00ac, NullPointerException -> 0x00aa }
            j.b.a.m.m.b0.ArrayPool r4 = r4.c     // Catch:{ IOException -> 0x00ac, NullPointerException -> 0x00aa }
            int r0 = i.b.k.ResourcesFlusher.a(r7, r3, r4)     // Catch:{ IOException -> 0x00ac, NullPointerException -> 0x00aa }
            if (r3 == 0) goto L_0x00d5
            r3.close()     // Catch:{ IOException -> 0x00a6 }
            goto L_0x00d5
        L_0x00a6:
            goto L_0x00d5
        L_0x00a8:
            r0 = move-exception
            goto L_0x00cd
        L_0x00aa:
            r4 = move-exception
            goto L_0x00ad
        L_0x00ac:
            r4 = move-exception
        L_0x00ad:
            boolean r5 = android.util.Log.isLoggable(r0, r5)     // Catch:{ all -> 0x00a8 }
            if (r5 == 0) goto L_0x00c7
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a8 }
            r5.<init>()     // Catch:{ all -> 0x00a8 }
            java.lang.String r7 = "Failed to open uri: "
            r5.append(r7)     // Catch:{ all -> 0x00a8 }
            r5.append(r6)     // Catch:{ all -> 0x00a8 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x00a8 }
            android.util.Log.d(r0, r5, r4)     // Catch:{ all -> 0x00a8 }
        L_0x00c7:
            if (r3 == 0) goto L_0x00d4
            r3.close()     // Catch:{ IOException -> 0x00d4 }
            goto L_0x00d4
        L_0x00cd:
            if (r3 == 0) goto L_0x00d2
            r3.close()     // Catch:{ IOException -> 0x00d2 }
        L_0x00d2:
            throw r0
        L_0x00d3:
            throw r3
        L_0x00d4:
            r0 = -1
        L_0x00d5:
            if (r0 == r2) goto L_0x00dd
            j.b.a.m.l.ExifOrientationStream r2 = new j.b.a.m.l.ExifOrientationStream
            r2.<init>(r1, r0)
            r1 = r2
        L_0x00dd:
            return r1
        L_0x00de:
            r0 = move-exception
            java.io.FileNotFoundException r1 = new java.io.FileNotFoundException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r5 = "NPE opening uri: "
            r3.append(r5)
            r3.append(r2)
            java.lang.String r2 = " -> "
            r3.append(r2)
            r3.append(r4)
            java.lang.String r2 = r3.toString()
            r1.<init>(r2)
            java.lang.Throwable r0 = r1.initCause(r0)
            java.io.FileNotFoundException r0 = (java.io.FileNotFoundException) r0
            throw r0
        L_0x0104:
            throw r3
        L_0x0105:
            throw r3
        L_0x0106:
            if (r3 == 0) goto L_0x010b
            r3.close()
        L_0x010b:
            throw r0
        L_0x010c:
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: j.b.a.m.l.p.ThumbFetcher.d():java.io.InputStream");
    }

    public void a(f fVar, DataFetcher.a<? super InputStream> aVar) {
        try {
            InputStream d2 = d();
            this.d = d2;
            aVar.a(d2);
        } catch (FileNotFoundException e2) {
            if (Log.isLoggable("MediaStoreThumbFetcher", 3)) {
                Log.d("MediaStoreThumbFetcher", "Failed to find thumbnail file", e2);
            }
            aVar.a((Exception) e2);
        }
    }

    public Class<InputStream> a() {
        return InputStream.class;
    }
}
