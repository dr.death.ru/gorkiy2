package j.b.a.m.o.c;

import android.graphics.Bitmap;
import j.b.a.m.Options;
import j.b.a.m.ResourceDecoder;
import j.b.a.m.g;
import j.b.a.m.m.Resource;
import j.b.a.m.m.b0.ArrayPool;
import j.b.a.m.m.b0.BitmapPool;
import j.b.a.m.o.c.Downsampler;
import j.b.a.m.o.c.l;
import j.b.a.s.ExceptionCatchingInputStream;
import j.b.a.s.MarkEnforcingInputStream;
import java.io.IOException;
import java.io.InputStream;

public class StreamBitmapDecoder implements ResourceDecoder<InputStream, Bitmap> {
    public final Downsampler a;
    public final ArrayPool b;

    public static class a implements Downsampler.b {
        public final RecyclableBufferedInputStream a;
        public final ExceptionCatchingInputStream b;

        public a(RecyclableBufferedInputStream recyclableBufferedInputStream, ExceptionCatchingInputStream exceptionCatchingInputStream) {
            this.a = recyclableBufferedInputStream;
            this.b = exceptionCatchingInputStream;
        }

        public void a() {
            this.a.a();
        }

        public void a(BitmapPool bitmapPool, Bitmap bitmap) {
            IOException iOException = this.b.c;
            if (iOException != null) {
                if (bitmap != null) {
                    bitmapPool.a(bitmap);
                }
                throw iOException;
            }
        }
    }

    public StreamBitmapDecoder(Downsampler downsampler, ArrayPool arrayPool) {
        this.a = downsampler;
        this.b = arrayPool;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.b.a.m.o.c.Downsampler.a(java.io.InputStream, int, int, j.b.a.m.g, j.b.a.m.o.c.l$b):j.b.a.m.m.Resource<android.graphics.Bitmap>
     arg types: [j.b.a.s.MarkEnforcingInputStream, int, int, j.b.a.m.Options, j.b.a.m.o.c.StreamBitmapDecoder$a]
     candidates:
      j.b.a.m.o.c.Downsampler.a(java.lang.IllegalArgumentException, int, int, java.lang.String, android.graphics.BitmapFactory$Options):java.io.IOException
      j.b.a.m.o.c.Downsampler.a(j.b.a.m.o.c.r, int, int, j.b.a.m.g, j.b.a.m.o.c.l$b):j.b.a.m.m.Resource<android.graphics.Bitmap>
      j.b.a.m.o.c.Downsampler.a(java.io.InputStream, int, int, j.b.a.m.g, j.b.a.m.o.c.l$b):j.b.a.m.m.Resource<android.graphics.Bitmap> */
    public Resource a(Object obj, int i2, int i3, Options options) {
        RecyclableBufferedInputStream recyclableBufferedInputStream;
        boolean z;
        InputStream inputStream = (InputStream) obj;
        if (inputStream instanceof RecyclableBufferedInputStream) {
            recyclableBufferedInputStream = (RecyclableBufferedInputStream) inputStream;
            z = false;
        } else {
            recyclableBufferedInputStream = new RecyclableBufferedInputStream(inputStream, this.b);
            z = true;
        }
        ExceptionCatchingInputStream a2 = ExceptionCatchingInputStream.a(recyclableBufferedInputStream);
        try {
            return this.a.a((InputStream) new MarkEnforcingInputStream(a2), i2, i3, (g) options, (l.b) new a(recyclableBufferedInputStream, a2));
        } finally {
            a2.a();
            if (z) {
                recyclableBufferedInputStream.f();
            }
        }
    }

    public boolean a(Object obj, Options options) {
        InputStream inputStream = (InputStream) obj;
        if (this.a != null) {
            return true;
        }
        throw null;
    }
}
