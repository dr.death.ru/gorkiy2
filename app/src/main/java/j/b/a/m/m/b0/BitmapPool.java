package j.b.a.m.m.b0;

import android.graphics.Bitmap;

public interface BitmapPool {
    Bitmap a(int i2, int i3, Bitmap.Config config);

    void a();

    void a(int i2);

    void a(Bitmap bitmap);

    Bitmap b(int i2, int i3, Bitmap.Config config);
}
