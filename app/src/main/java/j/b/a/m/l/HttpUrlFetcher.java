package j.b.a.m.l;

import android.text.TextUtils;
import android.util.Log;
import com.bumptech.glide.load.HttpException;
import j.a.a.a.outline;
import j.b.a.f;
import j.b.a.m.DataSource;
import j.b.a.m.l.DataFetcher;
import j.b.a.m.n.GlideUrl;
import j.b.a.s.ContentLengthInputStream;
import j.b.a.s.LogTime;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;

public class HttpUrlFetcher implements DataFetcher<InputStream> {
    public static final b h = new a();
    public final GlideUrl b;
    public final int c;
    public final b d;

    /* renamed from: e  reason: collision with root package name */
    public HttpURLConnection f1601e;

    /* renamed from: f  reason: collision with root package name */
    public InputStream f1602f;
    public volatile boolean g;

    public static class a implements b {
    }

    public interface b {
    }

    public HttpUrlFetcher(GlideUrl glideUrl, int i2) {
        b bVar = h;
        this.b = glideUrl;
        this.c = i2;
        this.d = bVar;
    }

    public void a(f fVar, DataFetcher.a<? super InputStream> aVar) {
        StringBuilder sb;
        long a2 = LogTime.a();
        try {
            aVar.a(a(this.b.b(), 0, null, this.b.b.a()));
            if (Log.isLoggable("HttpUrlFetcher", 2)) {
                sb = new StringBuilder();
                sb.append("Finished http url fetcher fetch in ");
                sb.append(LogTime.a(a2));
                Log.v("HttpUrlFetcher", sb.toString());
            }
        } catch (IOException e2) {
            if (Log.isLoggable("HttpUrlFetcher", 3)) {
                Log.d("HttpUrlFetcher", "Failed to load data for url", e2);
            }
            aVar.a((Exception) e2);
            if (Log.isLoggable("HttpUrlFetcher", 2)) {
                sb = new StringBuilder();
            }
        } catch (Throwable th) {
            if (Log.isLoggable("HttpUrlFetcher", 2)) {
                StringBuilder a3 = outline.a("Finished http url fetcher fetch in ");
                a3.append(LogTime.a(a2));
                Log.v("HttpUrlFetcher", a3.toString());
            }
            throw th;
        }
    }

    public void b() {
        InputStream inputStream = this.f1602f;
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException unused) {
            }
        }
        HttpURLConnection httpURLConnection = this.f1601e;
        if (httpURLConnection != null) {
            httpURLConnection.disconnect();
        }
        this.f1601e = null;
    }

    public DataSource c() {
        return DataSource.REMOTE;
    }

    public void cancel() {
        this.g = true;
    }

    public final InputStream a(URL url, int i2, URL url2, Map<String, String> map) {
        if (i2 < 5) {
            if (url2 != null) {
                try {
                    if (url.toURI().equals(url2.toURI())) {
                        throw new HttpException("In re-direct loop");
                    }
                } catch (URISyntaxException unused) {
                }
            }
            if (((a) this.d) != null) {
                this.f1601e = (HttpURLConnection) url.openConnection();
                for (Map.Entry next : map.entrySet()) {
                    this.f1601e.addRequestProperty((String) next.getKey(), (String) next.getValue());
                }
                this.f1601e.setConnectTimeout(this.c);
                this.f1601e.setReadTimeout(this.c);
                boolean z = false;
                this.f1601e.setUseCaches(false);
                this.f1601e.setDoInput(true);
                this.f1601e.setInstanceFollowRedirects(false);
                this.f1601e.connect();
                this.f1602f = this.f1601e.getInputStream();
                if (this.g) {
                    return null;
                }
                int responseCode = this.f1601e.getResponseCode();
                int i3 = responseCode / 100;
                if (i3 == 2) {
                    HttpURLConnection httpURLConnection = this.f1601e;
                    if (TextUtils.isEmpty(httpURLConnection.getContentEncoding())) {
                        this.f1602f = new ContentLengthInputStream(httpURLConnection.getInputStream(), (long) httpURLConnection.getContentLength());
                    } else {
                        if (Log.isLoggable("HttpUrlFetcher", 3)) {
                            StringBuilder a2 = outline.a("Got non empty content encoding: ");
                            a2.append(httpURLConnection.getContentEncoding());
                            Log.d("HttpUrlFetcher", a2.toString());
                        }
                        this.f1602f = httpURLConnection.getInputStream();
                    }
                    return this.f1602f;
                }
                if (i3 == 3) {
                    z = true;
                }
                if (z) {
                    String headerField = this.f1601e.getHeaderField("Location");
                    if (!TextUtils.isEmpty(headerField)) {
                        URL url3 = new URL(url, headerField);
                        b();
                        return a(url3, i2 + 1, url, map);
                    }
                    throw new HttpException("Received empty or null redirect url");
                } else if (responseCode == -1) {
                    throw new HttpException(responseCode);
                } else {
                    throw new HttpException(this.f1601e.getResponseMessage(), responseCode);
                }
            } else {
                throw null;
            }
        } else {
            throw new HttpException("Too many (> 5) redirects!");
        }
    }

    public Class<InputStream> a() {
        return InputStream.class;
    }
}
