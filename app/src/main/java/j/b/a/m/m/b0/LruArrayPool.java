package j.b.a.m.m.b0;

import android.util.Log;
import i.b.k.ResourcesFlusher;
import j.a.a.a.outline;
import j.b.a.m.m.b0.i;
import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

public final class LruArrayPool implements ArrayPool {
    public final GroupedLinkedMap<i.a, Object> a = new GroupedLinkedMap<>();
    public final b b = new b();
    public final Map<Class<?>, NavigableMap<Integer, Integer>> c = new HashMap();
    public final Map<Class<?>, ArrayAdapterInterface<?>> d = new HashMap();

    /* renamed from: e  reason: collision with root package name */
    public final int f1675e;

    /* renamed from: f  reason: collision with root package name */
    public int f1676f;

    public static final class a implements Poolable {
        public final b a;
        public int b;
        public Class<?> c;

        public a(b bVar) {
            this.a = bVar;
        }

        public void a() {
            this.a.a(this);
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            if (this.b == aVar.b && this.c == aVar.c) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            int i2 = this.b * 31;
            Class<?> cls = this.c;
            return i2 + (cls != null ? cls.hashCode() : 0);
        }

        public String toString() {
            StringBuilder a2 = outline.a("Key{size=");
            a2.append(this.b);
            a2.append("array=");
            a2.append(this.c);
            a2.append('}');
            return a2.toString();
        }
    }

    public LruArrayPool(int i2) {
        this.f1675e = i2;
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [java.lang.Class<?>, java.lang.Class, java.lang.Class<T>] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.b.a.m.m.b0.LruArrayPool.a(j.b.a.m.m.b0.i$a, java.lang.Class):T
     arg types: [j.b.a.m.m.b0.LruArrayPool$a, ?]
     candidates:
      j.b.a.m.m.b0.LruArrayPool.a(int, java.lang.Class):T
      j.b.a.m.m.b0.ArrayPool.a(int, java.lang.Class):T
      j.b.a.m.m.b0.LruArrayPool.a(j.b.a.m.m.b0.i$a, java.lang.Class):T */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized <T> T a(int r2, java.lang.Class<T> r3) {
        /*
            r1 = this;
            monitor-enter(r1)
            j.b.a.m.m.b0.LruArrayPool$b r0 = r1.b     // Catch:{ all -> 0x0013 }
            j.b.a.m.m.b0.Poolable r0 = r0.b()     // Catch:{ all -> 0x0013 }
            j.b.a.m.m.b0.LruArrayPool$a r0 = (j.b.a.m.m.b0.LruArrayPool.a) r0     // Catch:{ all -> 0x0013 }
            r0.b = r2     // Catch:{ all -> 0x0013 }
            r0.c = r3     // Catch:{ all -> 0x0013 }
            java.lang.Object r2 = r1.a(r0, r3)     // Catch:{ all -> 0x0013 }
            monitor-exit(r1)
            return r2
        L_0x0013:
            r2 = move-exception
            monitor-exit(r1)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: j.b.a.m.m.b0.LruArrayPool.a(int, java.lang.Class):java.lang.Object");
    }

    /* JADX WARN: Type inference failed for: r7v0, types: [java.lang.Class<?>, java.lang.Class, java.lang.Class<T>] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.b.a.m.m.b0.LruArrayPool.a(j.b.a.m.m.b0.i$a, java.lang.Class):T
     arg types: [j.b.a.m.m.b0.LruArrayPool$a, ?]
     candidates:
      j.b.a.m.m.b0.LruArrayPool.a(int, java.lang.Class):T
      j.b.a.m.m.b0.ArrayPool.a(int, java.lang.Class):T
      j.b.a.m.m.b0.LruArrayPool.a(j.b.a.m.m.b0.i$a, java.lang.Class):T */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized <T> T b(int r6, java.lang.Class<T> r7) {
        /*
            r5 = this;
            monitor-enter(r5)
            java.util.NavigableMap r0 = r5.b(r7)     // Catch:{ all -> 0x004c }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x004c }
            java.lang.Object r0 = r0.ceilingKey(r1)     // Catch:{ all -> 0x004c }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ all -> 0x004c }
            r1 = 0
            r2 = 1
            if (r0 == 0) goto L_0x002c
            int r3 = r5.f1676f     // Catch:{ all -> 0x004c }
            if (r3 == 0) goto L_0x0020
            int r4 = r5.f1675e     // Catch:{ all -> 0x004c }
            int r4 = r4 / r3
            r3 = 2
            if (r4 < r3) goto L_0x001e
            goto L_0x0020
        L_0x001e:
            r3 = 0
            goto L_0x0021
        L_0x0020:
            r3 = 1
        L_0x0021:
            if (r3 != 0) goto L_0x002b
            int r3 = r0.intValue()     // Catch:{ all -> 0x004c }
            int r4 = r6 * 8
            if (r3 > r4) goto L_0x002c
        L_0x002b:
            r1 = 1
        L_0x002c:
            if (r1 == 0) goto L_0x0039
            j.b.a.m.m.b0.LruArrayPool$b r6 = r5.b     // Catch:{ all -> 0x004c }
            int r0 = r0.intValue()     // Catch:{ all -> 0x004c }
            j.b.a.m.m.b0.LruArrayPool$a r6 = r6.a(r0, r7)     // Catch:{ all -> 0x004c }
            goto L_0x0046
        L_0x0039:
            j.b.a.m.m.b0.LruArrayPool$b r0 = r5.b     // Catch:{ all -> 0x004c }
            j.b.a.m.m.b0.Poolable r0 = r0.b()     // Catch:{ all -> 0x004c }
            j.b.a.m.m.b0.LruArrayPool$a r0 = (j.b.a.m.m.b0.LruArrayPool.a) r0     // Catch:{ all -> 0x004c }
            r0.b = r6     // Catch:{ all -> 0x004c }
            r0.c = r7     // Catch:{ all -> 0x004c }
            r6 = r0
        L_0x0046:
            java.lang.Object r6 = r5.a(r6, r7)     // Catch:{ all -> 0x004c }
            monitor-exit(r5)
            return r6
        L_0x004c:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: j.b.a.m.m.b0.LruArrayPool.b(int, java.lang.Class):java.lang.Object");
    }

    public final void c(int i2, Class<?> cls) {
        NavigableMap<Integer, Integer> b2 = b(cls);
        Integer num = (Integer) b2.get(Integer.valueOf(i2));
        if (num == null) {
            throw new NullPointerException("Tried to decrement empty size, size: " + i2 + ", this: " + this);
        } else if (num.intValue() == 1) {
            b2.remove(Integer.valueOf(i2));
        } else {
            b2.put(Integer.valueOf(i2), Integer.valueOf(num.intValue() - 1));
        }
    }

    public synchronized <T> void put(T t2) {
        Class<?> cls = t2.getClass();
        ArrayAdapterInterface a2 = a(cls);
        int a3 = a2.a(t2);
        int b2 = a2.b() * a3;
        int i2 = 1;
        if (b2 <= this.f1675e / 2) {
            a a4 = this.b.a(a3, cls);
            this.a.a(a4, t2);
            NavigableMap<Integer, Integer> b3 = b(cls);
            Integer num = (Integer) b3.get(Integer.valueOf(a4.b));
            Integer valueOf = Integer.valueOf(a4.b);
            if (num != null) {
                i2 = 1 + num.intValue();
            }
            b3.put(valueOf, Integer.valueOf(i2));
            this.f1676f += b2;
            b(this.f1675e);
        }
    }

    public static final class b extends BaseKeyPool<i.a> {
        public i.a a(int i2, Class<?> cls) {
            a aVar = (a) b();
            aVar.b = i2;
            aVar.c = cls;
            return aVar;
        }

        public Poolable a() {
            return new a(this);
        }
    }

    public final <T> T a(i.a aVar, Class<T> cls) {
        ArrayAdapterInterface<T> a2 = a(cls);
        T a3 = this.a.a(aVar);
        if (a3 != null) {
            this.f1676f -= a2.b() * a2.a(a3);
            c(a2.a(a3), cls);
        }
        if (a3 != null) {
            return a3;
        }
        if (Log.isLoggable(a2.a(), 2)) {
            String a4 = a2.a();
            StringBuilder a5 = outline.a("Allocated ");
            a5.append(aVar.b);
            a5.append(" bytes");
            Log.v(a4, a5.toString());
        }
        return a2.newArray(aVar.b);
    }

    public final void b(int i2) {
        while (this.f1676f > i2) {
            Object a2 = this.a.a();
            ResourcesFlusher.a(a2, "Argument must not be null");
            ArrayAdapterInterface a3 = a(a2.getClass());
            this.f1676f -= a3.b() * a3.a(a2);
            c(a3.a(a2), a2.getClass());
            if (Log.isLoggable(a3.a(), 2)) {
                String a4 = a3.a();
                StringBuilder a5 = outline.a("evicted: ");
                a5.append(a3.a(a2));
                Log.v(a4, a5.toString());
            }
        }
    }

    public synchronized void a() {
        b(0);
    }

    public synchronized void a(int i2) {
        if (i2 >= 40) {
            try {
                a();
            } catch (Throwable th) {
                throw th;
            }
        } else if (i2 >= 20 || i2 == 15) {
            b(this.f1675e / 2);
        }
    }

    public final <T> ArrayAdapterInterface<T> a(Class<T> cls) {
        ArrayAdapterInterface<T> arrayAdapterInterface = this.d.get(cls);
        if (arrayAdapterInterface == null) {
            if (cls.equals(int[].class)) {
                arrayAdapterInterface = new IntegerArrayAdapter();
            } else if (cls.equals(byte[].class)) {
                arrayAdapterInterface = new ByteArrayAdapter();
            } else {
                StringBuilder a2 = outline.a("No array pool found for: ");
                a2.append(cls.getSimpleName());
                throw new IllegalArgumentException(a2.toString());
            }
            this.d.put(cls, arrayAdapterInterface);
        }
        return arrayAdapterInterface;
    }

    public final NavigableMap<Integer, Integer> b(Class<?> cls) {
        NavigableMap<Integer, Integer> navigableMap = this.c.get(cls);
        if (navigableMap != null) {
            return navigableMap;
        }
        TreeMap treeMap = new TreeMap();
        this.c.put(cls, treeMap);
        return treeMap;
    }
}
