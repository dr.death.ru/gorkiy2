package j.b.a.m.o.c;

import android.graphics.Bitmap;
import j.b.a.m.Key;
import j.b.a.m.m.b0.BitmapPool;
import java.security.MessageDigest;

public class CenterInside extends BitmapTransformation {
    public static final byte[] b = "com.bumptech.glide.load.resource.bitmap.CenterInside".getBytes(Key.a);

    public Bitmap a(BitmapPool bitmapPool, Bitmap bitmap, int i2, int i3) {
        return TransformationUtils.b(bitmapPool, bitmap, i2, i3);
    }

    public boolean equals(Object obj) {
        return obj instanceof CenterInside;
    }

    public int hashCode() {
        return -670243078;
    }

    public void a(MessageDigest messageDigest) {
        messageDigest.update(b);
    }
}
