package j.b.a;

import android.content.Context;
import android.content.ContextWrapper;
import com.bumptech.glide.Registry;
import j.b.a.Glide;
import j.b.a.GlideBuilder;
import j.b.a.b;
import j.b.a.m.m.Engine;
import j.b.a.m.m.b0.ArrayPool;
import j.b.a.m.m.b0.b;
import j.b.a.m.m.l;
import j.b.a.q.RequestListener;
import j.b.a.q.RequestOptions;
import j.b.a.q.h.ImageViewTargetFactory;
import j.b.a.q.h.f;
import java.util.List;
import java.util.Map;

public class GlideContext extends ContextWrapper {

    /* renamed from: k  reason: collision with root package name */
    public static final TransitionOptions<?, ?> f1546k = new GenericTransitionOptions();
    public final ArrayPool a;
    public final Registry b;
    public final ImageViewTargetFactory c;
    public final Glide.a d;

    /* renamed from: e  reason: collision with root package name */
    public final List<RequestListener<Object>> f1547e;

    /* renamed from: f  reason: collision with root package name */
    public final Map<Class<?>, TransitionOptions<?, ?>> f1548f;
    public final Engine g;
    public final boolean h;

    /* renamed from: i  reason: collision with root package name */
    public final int f1549i;

    /* renamed from: j  reason: collision with root package name */
    public RequestOptions f1550j;

    public GlideContext(Context context, b bVar, Registry registry, f fVar, b.a aVar, Map<Class<?>, TransitionOptions<?, ?>> map, List<RequestListener<Object>> list, l lVar, boolean z, int i2) {
        super(context.getApplicationContext());
        this.a = bVar;
        this.b = registry;
        this.c = fVar;
        this.d = aVar;
        this.f1547e = list;
        this.f1548f = map;
        this.g = lVar;
        this.h = z;
        this.f1549i = i2;
    }

    public synchronized RequestOptions a() {
        if (this.f1550j == null) {
            if (((GlideBuilder.a) this.d) != null) {
                RequestOptions requestOptions = new RequestOptions();
                requestOptions.u = true;
                this.f1550j = requestOptions;
            } else {
                throw null;
            }
        }
        return this.f1550j;
    }
}
