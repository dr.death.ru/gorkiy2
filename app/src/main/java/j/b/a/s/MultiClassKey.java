package j.b.a.s;

import j.a.a.a.outline;

public class MultiClassKey {
    public Class<?> a;
    public Class<?> b;
    public Class<?> c;

    public MultiClassKey() {
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || MultiClassKey.class != obj.getClass()) {
            return false;
        }
        MultiClassKey multiClassKey = (MultiClassKey) obj;
        return this.a.equals(multiClassKey.a) && this.b.equals(multiClassKey.b) && Util.b(this.c, multiClassKey.c);
    }

    public int hashCode() {
        int hashCode = (this.b.hashCode() + (this.a.hashCode() * 31)) * 31;
        Class<?> cls = this.c;
        return hashCode + (cls != null ? cls.hashCode() : 0);
    }

    public String toString() {
        StringBuilder a2 = outline.a("MultiClassKey{first=");
        a2.append(this.a);
        a2.append(", second=");
        a2.append(this.b);
        a2.append('}');
        return a2.toString();
    }

    public MultiClassKey(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        this.a = cls;
        this.b = cls2;
        this.c = cls3;
    }
}
