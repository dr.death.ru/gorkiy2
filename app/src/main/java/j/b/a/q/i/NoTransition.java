package j.b.a.q.i;

public class NoTransition<R> implements Transition<R> {
    public static final NoTransition<?> a = new NoTransition<>();
    public static final TransitionFactory<?> b = new a();

    public static class a<R> implements TransitionFactory<R> {
    }
}
