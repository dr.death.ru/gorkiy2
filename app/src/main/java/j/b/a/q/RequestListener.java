package j.b.a.q;

import com.bumptech.glide.load.engine.GlideException;
import j.b.a.m.a;
import j.b.a.q.h.Target;

public interface RequestListener<R> {
    boolean a(GlideException glideException, Object obj, Target<R> target, boolean z);

    boolean a(R r2, Object obj, Target<R> target, a aVar, boolean z);
}
