package j.b.a.q;

public interface RequestCoordinator {
    RequestCoordinator a();

    boolean a(Request request);

    void b(Request request);

    boolean b();

    void c(Request request);

    boolean d(Request request);

    boolean e(Request request);
}
