package j.b.a;

import android.content.ComponentCallbacks2;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import com.bumptech.glide.GeneratedAppGlideModule;
import com.bumptech.glide.Registry;
import com.bumptech.glide.load.ImageHeaderParser;
import com.bumptech.glide.load.resource.bitmap.DefaultImageHeaderParser;
import i.b.k.ResourcesFlusher;
import j.b.a.b;
import j.b.a.l.GifDecoder;
import j.b.a.m.l.DataRewinder;
import j.b.a.m.l.InputStreamRewinder;
import j.b.a.m.l.ParcelFileDescriptorRewinder;
import j.b.a.m.m.b0.ArrayPool;
import j.b.a.m.m.b0.BitmapPool;
import j.b.a.m.m.b0.b;
import j.b.a.m.m.b0.d;
import j.b.a.m.m.c0.LruResourceCache;
import j.b.a.m.m.c0.MemoryCache;
import j.b.a.m.m.c0.i;
import j.b.a.m.m.l;
import j.b.a.m.n.AssetUriLoader;
import j.b.a.m.n.ByteArrayLoader;
import j.b.a.m.n.ByteBufferEncoder;
import j.b.a.m.n.ByteBufferFileLoader;
import j.b.a.m.n.DataUrlLoader;
import j.b.a.m.n.FileLoader;
import j.b.a.m.n.GlideUrl;
import j.b.a.m.n.MediaStoreFileLoader;
import j.b.a.m.n.ResourceLoader;
import j.b.a.m.n.StreamEncoder;
import j.b.a.m.n.StringLoader;
import j.b.a.m.n.UnitModelLoader;
import j.b.a.m.n.UriLoader;
import j.b.a.m.n.UrlUriLoader;
import j.b.a.m.n.y.HttpGlideUrlLoader;
import j.b.a.m.n.y.HttpUriLoader;
import j.b.a.m.n.y.MediaStoreImageThumbLoader;
import j.b.a.m.n.y.MediaStoreVideoThumbLoader;
import j.b.a.m.n.y.QMediaStoreUriLoader;
import j.b.a.m.n.y.UrlLoader;
import j.b.a.m.o.c.BitmapDrawableDecoder;
import j.b.a.m.o.c.BitmapDrawableEncoder;
import j.b.a.m.o.c.BitmapEncoder;
import j.b.a.m.o.c.ByteBufferBitmapDecoder;
import j.b.a.m.o.c.ByteBufferBitmapImageDecoderResourceDecoder;
import j.b.a.m.o.c.Downsampler;
import j.b.a.m.o.c.ExifInterfaceImageHeaderParser;
import j.b.a.m.o.c.InputStreamBitmapImageDecoderResourceDecoder;
import j.b.a.m.o.c.ParcelFileDescriptorBitmapDecoder;
import j.b.a.m.o.c.ResourceBitmapDecoder;
import j.b.a.m.o.c.StreamBitmapDecoder;
import j.b.a.m.o.c.UnitBitmapDecoder;
import j.b.a.m.o.c.VideoDecoder;
import j.b.a.m.o.d.ByteBufferRewinder;
import j.b.a.m.o.e.ResourceDrawableDecoder;
import j.b.a.m.o.e.UnitDrawableDecoder;
import j.b.a.m.o.f.FileDecoder;
import j.b.a.m.o.g.ByteBufferGifDecoder;
import j.b.a.m.o.g.GifDrawable;
import j.b.a.m.o.g.GifDrawableEncoder;
import j.b.a.m.o.g.GifFrameResourceDecoder;
import j.b.a.m.o.g.StreamGifDecoder;
import j.b.a.m.o.h.BitmapBytesTranscoder;
import j.b.a.m.o.h.BitmapDrawableTranscoder;
import j.b.a.m.o.h.DrawableBytesTranscoder;
import j.b.a.m.o.h.GifDrawableBytesTranscoder;
import j.b.a.n.ConnectivityMonitorFactory;
import j.b.a.n.RequestManagerRetriever;
import j.b.a.q.RequestListener;
import j.b.a.q.h.ImageViewTargetFactory;
import j.b.a.q.h.Target;
import j.b.a.s.LruCache;
import j.b.a.s.Util;
import java.io.File;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Glide implements ComponentCallbacks2 {

    /* renamed from: j  reason: collision with root package name */
    public static volatile Glide f1532j;

    /* renamed from: k  reason: collision with root package name */
    public static volatile boolean f1533k;
    public final BitmapPool b;
    public final MemoryCache c;
    public final GlideContext d;

    /* renamed from: e  reason: collision with root package name */
    public final Registry f1534e;

    /* renamed from: f  reason: collision with root package name */
    public final ArrayPool f1535f;
    public final RequestManagerRetriever g;
    public final ConnectivityMonitorFactory h;

    /* renamed from: i  reason: collision with root package name */
    public final List<i> f1536i = new ArrayList();

    public interface a {
    }

    public Glide(Context context, l lVar, i iVar, d dVar, b bVar, j.b.a.n.l lVar2, j.b.a.n.d dVar2, int i2, b.a aVar, Map<Class<?>, TransitionOptions<?, ?>> map, List<RequestListener<Object>> list, boolean z, boolean z2) {
        Object obj;
        Object obj2;
        Context context2 = context;
        d dVar3 = dVar;
        j.b.a.m.m.b0.b bVar2 = bVar;
        Class<GifDecoder> cls = GifDecoder.class;
        Class<String> cls2 = String.class;
        Class<Integer> cls3 = Integer.class;
        Class<byte[]> cls4 = byte[].class;
        MemoryCategory memoryCategory = MemoryCategory.NORMAL;
        this.b = dVar3;
        this.f1535f = bVar2;
        this.c = iVar;
        this.g = lVar2;
        this.h = dVar2;
        Resources resources = context.getResources();
        Registry registry = new Registry();
        this.f1534e = registry;
        registry.g.a(new DefaultImageHeaderParser());
        if (Build.VERSION.SDK_INT >= 27) {
            Registry registry2 = this.f1534e;
            registry2.g.a(new ExifInterfaceImageHeaderParser());
        }
        List<ImageHeaderParser> a2 = this.f1534e.a();
        ByteBufferGifDecoder byteBufferGifDecoder = new ByteBufferGifDecoder(context2, a2, dVar3, bVar2);
        VideoDecoder videoDecoder = new VideoDecoder(dVar3, new VideoDecoder.g());
        Downsampler downsampler = new Downsampler(this.f1534e.a(), resources.getDisplayMetrics(), dVar3, bVar2);
        if (!z2 || Build.VERSION.SDK_INT < 28) {
            obj = new ByteBufferBitmapDecoder(downsampler);
            obj2 = new StreamBitmapDecoder(downsampler, bVar2);
        } else {
            obj2 = new InputStreamBitmapImageDecoderResourceDecoder();
            obj = new ByteBufferBitmapImageDecoderResourceDecoder();
        }
        ResourceDrawableDecoder resourceDrawableDecoder = new ResourceDrawableDecoder(context2);
        Class<byte[]> cls5 = cls4;
        ResourceLoader.c cVar = new ResourceLoader.c(resources);
        ResourceLoader.d dVar4 = new ResourceLoader.d(resources);
        Class<String> cls6 = cls2;
        ResourceLoader.b bVar3 = new ResourceLoader.b(resources);
        ResourceLoader.d dVar5 = dVar4;
        ResourceLoader.a aVar2 = new ResourceLoader.a(resources);
        BitmapEncoder bitmapEncoder = new BitmapEncoder(bVar2);
        Class<Integer> cls7 = cls3;
        BitmapBytesTranscoder bitmapBytesTranscoder = new BitmapBytesTranscoder();
        GifDrawableBytesTranscoder gifDrawableBytesTranscoder = new GifDrawableBytesTranscoder();
        ContentResolver contentResolver = context.getContentResolver();
        Registry registry3 = this.f1534e;
        ResourceLoader.b bVar4 = bVar3;
        ResourceLoader.c cVar2 = cVar;
        ByteBufferEncoder byteBufferEncoder = new ByteBufferEncoder();
        ResourceDrawableDecoder resourceDrawableDecoder2 = resourceDrawableDecoder;
        registry3.b.a(ByteBuffer.class, byteBufferEncoder);
        StreamEncoder streamEncoder = new StreamEncoder(bVar2);
        registry3.b.a(InputStream.class, streamEncoder);
        registry3.c.a("Bitmap", obj, ByteBuffer.class, Bitmap.class);
        registry3.c.a("Bitmap", obj2, InputStream.class, Bitmap.class);
        Registry registry4 = this.f1534e;
        ParcelFileDescriptorBitmapDecoder parcelFileDescriptorBitmapDecoder = new ParcelFileDescriptorBitmapDecoder(downsampler);
        registry4.c.a("Bitmap", parcelFileDescriptorBitmapDecoder, ParcelFileDescriptor.class, Bitmap.class);
        Registry registry5 = this.f1534e;
        registry5.c.a("Bitmap", videoDecoder, ParcelFileDescriptor.class, Bitmap.class);
        VideoDecoder videoDecoder2 = new VideoDecoder(dVar3, new VideoDecoder.c(null));
        registry5.c.a("Bitmap", videoDecoder2, AssetFileDescriptor.class, Bitmap.class);
        UnitModelLoader.a<?> aVar3 = UnitModelLoader.a.a;
        registry5.a.a(Bitmap.class, Bitmap.class, aVar3);
        UnitBitmapDecoder unitBitmapDecoder = new UnitBitmapDecoder();
        registry5.c.a("Bitmap", unitBitmapDecoder, Bitmap.class, Bitmap.class);
        registry5.d.a(Bitmap.class, bitmapEncoder);
        BitmapDrawableDecoder bitmapDrawableDecoder = new BitmapDrawableDecoder(resources, obj);
        registry5.c.a("BitmapDrawable", bitmapDrawableDecoder, ByteBuffer.class, BitmapDrawable.class);
        BitmapDrawableDecoder bitmapDrawableDecoder2 = new BitmapDrawableDecoder(resources, obj2);
        registry5.c.a("BitmapDrawable", bitmapDrawableDecoder2, InputStream.class, BitmapDrawable.class);
        BitmapDrawableDecoder bitmapDrawableDecoder3 = new BitmapDrawableDecoder(resources, videoDecoder);
        registry5.c.a("BitmapDrawable", bitmapDrawableDecoder3, ParcelFileDescriptor.class, BitmapDrawable.class);
        BitmapDrawableEncoder bitmapDrawableEncoder = new BitmapDrawableEncoder(dVar3, bitmapEncoder);
        registry5.d.a(BitmapDrawable.class, bitmapDrawableEncoder);
        StreamGifDecoder streamGifDecoder = new StreamGifDecoder(a2, byteBufferGifDecoder, bVar2);
        registry5.c.a("Gif", streamGifDecoder, InputStream.class, GifDrawable.class);
        registry5.c.a("Gif", byteBufferGifDecoder, ByteBuffer.class, GifDrawable.class);
        GifDrawableEncoder gifDrawableEncoder = new GifDrawableEncoder();
        registry5.d.a(GifDrawable.class, gifDrawableEncoder);
        Class<GifDecoder> cls8 = cls;
        registry5.a.a(cls8, cls8, UnitModelLoader.a.a);
        GifFrameResourceDecoder gifFrameResourceDecoder = new GifFrameResourceDecoder(dVar3);
        registry5.c.a("Bitmap", gifFrameResourceDecoder, cls8, Bitmap.class);
        ResourceDrawableDecoder resourceDrawableDecoder3 = resourceDrawableDecoder2;
        registry5.c.a("legacy_append", resourceDrawableDecoder3, Uri.class, Drawable.class);
        ResourceBitmapDecoder resourceBitmapDecoder = new ResourceBitmapDecoder(resourceDrawableDecoder3, dVar3);
        registry5.c.a("legacy_append", resourceBitmapDecoder, Uri.class, Bitmap.class);
        registry5.f368e.a((DataRewinder.a<?>) new ByteBufferRewinder.a());
        ByteBufferFileLoader.b bVar5 = new ByteBufferFileLoader.b();
        registry5.a.a(File.class, ByteBuffer.class, bVar5);
        FileLoader.e eVar = new FileLoader.e();
        registry5.a.a(File.class, InputStream.class, eVar);
        FileDecoder fileDecoder = new FileDecoder();
        registry5.c.a("legacy_append", fileDecoder, File.class, File.class);
        FileLoader.b bVar6 = new FileLoader.b();
        registry5.a.a(File.class, ParcelFileDescriptor.class, bVar6);
        UnitModelLoader.a<?> aVar4 = UnitModelLoader.a.a;
        registry5.a.a(File.class, File.class, aVar4);
        registry5.f368e.a((DataRewinder.a<?>) new InputStreamRewinder.a(bVar2));
        Registry registry6 = this.f1534e;
        registry6.f368e.a((DataRewinder.a<?>) new ParcelFileDescriptorRewinder.a());
        Registry registry7 = this.f1534e;
        ResourceLoader.c cVar3 = cVar2;
        registry7.a.a(Integer.TYPE, InputStream.class, cVar3);
        ResourceLoader.b bVar7 = bVar4;
        registry7.a.a(Integer.TYPE, ParcelFileDescriptor.class, bVar7);
        Class<Integer> cls9 = cls7;
        registry7.a.a(cls9, InputStream.class, cVar3);
        registry7.a.a(cls9, ParcelFileDescriptor.class, bVar7);
        ResourceLoader.d dVar6 = dVar5;
        registry7.a.a(cls9, Uri.class, dVar6);
        ResourceLoader.a aVar5 = aVar2;
        registry7.a.a(Integer.TYPE, AssetFileDescriptor.class, aVar5);
        registry7.a.a(cls9, AssetFileDescriptor.class, aVar5);
        registry7.a.a(Integer.TYPE, Uri.class, dVar6);
        DataUrlLoader.c cVar4 = new DataUrlLoader.c();
        Class<String> cls10 = cls6;
        registry7.a.a(cls10, InputStream.class, cVar4);
        DataUrlLoader.c cVar5 = new DataUrlLoader.c();
        registry7.a.a(Uri.class, InputStream.class, cVar5);
        StringLoader.c cVar6 = new StringLoader.c();
        registry7.a.a(cls10, InputStream.class, cVar6);
        StringLoader.b bVar8 = new StringLoader.b();
        registry7.a.a(cls10, ParcelFileDescriptor.class, bVar8);
        StringLoader.a aVar6 = new StringLoader.a();
        registry7.a.a(cls10, AssetFileDescriptor.class, aVar6);
        HttpUriLoader.a aVar7 = new HttpUriLoader.a();
        registry7.a.a(Uri.class, InputStream.class, aVar7);
        AssetUriLoader.c cVar7 = new AssetUriLoader.c(context.getAssets());
        registry7.a.a(Uri.class, InputStream.class, cVar7);
        AssetUriLoader.b bVar9 = new AssetUriLoader.b(context.getAssets());
        registry7.a.a(Uri.class, ParcelFileDescriptor.class, bVar9);
        Context context3 = context;
        MediaStoreImageThumbLoader.a aVar8 = new MediaStoreImageThumbLoader.a(context3);
        registry7.a.a(Uri.class, InputStream.class, aVar8);
        MediaStoreVideoThumbLoader.a aVar9 = new MediaStoreVideoThumbLoader.a(context3);
        registry7.a.a(Uri.class, InputStream.class, aVar9);
        if (Build.VERSION.SDK_INT >= 29) {
            Registry registry8 = this.f1534e;
            QMediaStoreUriLoader.c cVar8 = new QMediaStoreUriLoader.c(context3);
            registry8.a.a(Uri.class, InputStream.class, cVar8);
            Registry registry9 = this.f1534e;
            QMediaStoreUriLoader.b bVar10 = new QMediaStoreUriLoader.b(context3);
            registry9.a.a(Uri.class, ParcelFileDescriptor.class, bVar10);
        }
        Registry registry10 = this.f1534e;
        ContentResolver contentResolver2 = contentResolver;
        UriLoader.d dVar7 = new UriLoader.d(contentResolver2);
        registry10.a.a(Uri.class, InputStream.class, dVar7);
        UriLoader.b bVar11 = new UriLoader.b(contentResolver2);
        registry10.a.a(Uri.class, ParcelFileDescriptor.class, bVar11);
        UriLoader.a aVar10 = new UriLoader.a(contentResolver2);
        registry10.a.a(Uri.class, AssetFileDescriptor.class, aVar10);
        UrlUriLoader.a aVar11 = new UrlUriLoader.a();
        registry10.a.a(Uri.class, InputStream.class, aVar11);
        UrlLoader.a aVar12 = new UrlLoader.a();
        registry10.a.a(URL.class, InputStream.class, aVar12);
        MediaStoreFileLoader.a aVar13 = new MediaStoreFileLoader.a(context3);
        registry10.a.a(Uri.class, File.class, aVar13);
        HttpGlideUrlLoader.a aVar14 = new HttpGlideUrlLoader.a();
        registry10.a.a(GlideUrl.class, InputStream.class, aVar14);
        ByteArrayLoader.a aVar15 = new ByteArrayLoader.a();
        Class<byte[]> cls11 = cls5;
        registry10.a.a(cls11, ByteBuffer.class, aVar15);
        ByteArrayLoader.d dVar8 = new ByteArrayLoader.d();
        registry10.a.a(cls11, InputStream.class, dVar8);
        UnitModelLoader.a<?> aVar16 = UnitModelLoader.a.a;
        registry10.a.a(Uri.class, Uri.class, aVar16);
        UnitModelLoader.a<?> aVar17 = UnitModelLoader.a.a;
        registry10.a.a(Drawable.class, Drawable.class, aVar17);
        UnitDrawableDecoder unitDrawableDecoder = new UnitDrawableDecoder();
        registry10.c.a("legacy_append", unitDrawableDecoder, Drawable.class, Drawable.class);
        BitmapDrawableTranscoder bitmapDrawableTranscoder = new BitmapDrawableTranscoder(resources);
        registry10.f369f.a(Bitmap.class, BitmapDrawable.class, bitmapDrawableTranscoder);
        BitmapBytesTranscoder bitmapBytesTranscoder2 = bitmapBytesTranscoder;
        registry10.f369f.a(Bitmap.class, cls11, bitmapBytesTranscoder2);
        GifDrawableBytesTranscoder gifDrawableBytesTranscoder2 = gifDrawableBytesTranscoder;
        DrawableBytesTranscoder drawableBytesTranscoder = new DrawableBytesTranscoder(dVar3, bitmapBytesTranscoder2, gifDrawableBytesTranscoder2);
        registry10.f369f.a(Drawable.class, cls11, drawableBytesTranscoder);
        registry10.f369f.a(GifDrawable.class, cls11, gifDrawableBytesTranscoder2);
        if (Build.VERSION.SDK_INT >= 23) {
            VideoDecoder videoDecoder3 = new VideoDecoder(dVar3, new VideoDecoder.d());
            this.f1534e.c.a("legacy_append", videoDecoder3, ByteBuffer.class, Bitmap.class);
            Registry registry11 = this.f1534e;
            BitmapDrawableDecoder bitmapDrawableDecoder4 = new BitmapDrawableDecoder(resources, videoDecoder3);
            registry11.c.a("legacy_append", bitmapDrawableDecoder4, ByteBuffer.class, BitmapDrawable.class);
        }
        Context context4 = context;
        j.b.a.m.m.b0.b bVar12 = bVar;
        this.d = new GlideContext(context4, bVar12, this.f1534e, new ImageViewTargetFactory(), aVar, map, list, lVar, z, i2);
    }

    public static Glide a(Context context) {
        if (f1532j == null) {
            Context applicationContext = context.getApplicationContext();
            GeneratedAppGlideModule generatedAppGlideModule = null;
            try {
                generatedAppGlideModule = (GeneratedAppGlideModule) Class.forName("com.bumptech.glide.GeneratedAppGlideModuleImpl").getDeclaredConstructor(Context.class).newInstance(applicationContext.getApplicationContext());
            } catch (ClassNotFoundException unused) {
                if (Log.isLoggable("Glide", 5)) {
                    Log.w("Glide", "Failed to find GeneratedAppGlideModule. You should include an annotationProcessor compile dependency on com.github.bumptech.glide:compiler in your application and a @GlideModule annotated AppGlideModule implementation or LibraryGlideModules will be silently ignored");
                }
            } catch (InstantiationException e2) {
                a(e2);
                throw null;
            } catch (IllegalAccessException e3) {
                a(e3);
                throw null;
            } catch (NoSuchMethodException e4) {
                a(e4);
                throw null;
            } catch (InvocationTargetException e5) {
                a(e5);
                throw null;
            }
            synchronized (Glide.class) {
                if (f1532j == null) {
                    a(context, generatedAppGlideModule);
                }
            }
        }
        return f1532j;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
     arg types: [android.content.Context, java.lang.String]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T */
    public static RequestManager b(Context context) {
        ResourcesFlusher.a((Object) context, "You cannot start a load on a not yet attached View or a Fragment where getActivity() returns null (which usually occurs when getActivity() is called before the Fragment is attached or after the Fragment is destroyed).");
        return a(context).g.a(context);
    }

    public void onConfigurationChanged(Configuration configuration) {
    }

    public void onLowMemory() {
        Util.a();
        ((LruCache) this.c).a(0);
        this.b.a();
        this.f1535f.a();
    }

    public void onTrimMemory(int i2) {
        Util.a();
        Iterator<i> it = this.f1536i.iterator();
        while (it.hasNext()) {
            if (it.next() == null) {
                throw null;
            }
        }
        LruResourceCache lruResourceCache = (LruResourceCache) this.c;
        if (lruResourceCache != null) {
            if (i2 >= 40) {
                lruResourceCache.a(0);
            } else if (i2 >= 20 || i2 == 15) {
                lruResourceCache.a(lruResourceCache.a() / 2);
            }
            this.b.a(i2);
            this.f1535f.a(i2);
            return;
        }
        throw null;
    }

    public void b(RequestManager requestManager) {
        synchronized (this.f1536i) {
            if (this.f1536i.contains(requestManager)) {
                this.f1536i.remove(requestManager);
            } else {
                throw new IllegalStateException("Cannot unregister not yet registered manager");
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v1, resolved type: java.util.ArrayList} */
    /* JADX WARN: Type inference failed for: r1v18, types: [j.b.a.m.m.c0.LruResourceCache, j.b.a.m.m.c0.MemoryCache] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(android.content.Context r26, com.bumptech.glide.GeneratedAppGlideModule r27) {
        /*
            r0 = r27
            boolean r1 = j.b.a.Glide.f1533k
            if (r1 != 0) goto L_0x02d5
            r1 = 1
            j.b.a.Glide.f1533k = r1
            j.b.a.GlideBuilder r2 = new j.b.a.GlideBuilder
            r2.<init>()
            android.content.Context r15 = r26.getApplicationContext()
            java.util.List r3 = java.util.Collections.emptyList()
            r4 = 3
            r5 = 2
            if (r0 == 0) goto L_0x0025
            boolean r6 = r27.a()
            if (r6 == 0) goto L_0x0021
            goto L_0x0025
        L_0x0021:
            r17 = r3
            goto L_0x00c4
        L_0x0025:
            java.lang.String r3 = "ManifestParser"
            boolean r6 = android.util.Log.isLoggable(r3, r4)
            if (r6 == 0) goto L_0x0032
            java.lang.String r6 = "Loading Glide modules"
            android.util.Log.d(r3, r6)
        L_0x0032:
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            android.content.pm.PackageManager r7 = r15.getPackageManager()     // Catch:{ NameNotFoundException -> 0x02cc }
            java.lang.String r8 = r15.getPackageName()     // Catch:{ NameNotFoundException -> 0x02cc }
            r9 = 128(0x80, float:1.794E-43)
            android.content.pm.ApplicationInfo r7 = r7.getApplicationInfo(r8, r9)     // Catch:{ NameNotFoundException -> 0x02cc }
            android.os.Bundle r8 = r7.metaData     // Catch:{ NameNotFoundException -> 0x02cc }
            if (r8 != 0) goto L_0x0055
            boolean r5 = android.util.Log.isLoggable(r3, r4)     // Catch:{ NameNotFoundException -> 0x02cc }
            if (r5 == 0) goto L_0x00c2
            java.lang.String r5 = "Got null app info metadata"
            android.util.Log.d(r3, r5)     // Catch:{ NameNotFoundException -> 0x02cc }
            goto L_0x00c2
        L_0x0055:
            boolean r5 = android.util.Log.isLoggable(r3, r5)     // Catch:{ NameNotFoundException -> 0x02cc }
            if (r5 == 0) goto L_0x0071
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ NameNotFoundException -> 0x02cc }
            r5.<init>()     // Catch:{ NameNotFoundException -> 0x02cc }
            java.lang.String r8 = "Got app info metadata: "
            r5.append(r8)     // Catch:{ NameNotFoundException -> 0x02cc }
            android.os.Bundle r8 = r7.metaData     // Catch:{ NameNotFoundException -> 0x02cc }
            r5.append(r8)     // Catch:{ NameNotFoundException -> 0x02cc }
            java.lang.String r5 = r5.toString()     // Catch:{ NameNotFoundException -> 0x02cc }
            android.util.Log.v(r3, r5)     // Catch:{ NameNotFoundException -> 0x02cc }
        L_0x0071:
            android.os.Bundle r5 = r7.metaData     // Catch:{ NameNotFoundException -> 0x02cc }
            java.util.Set r5 = r5.keySet()     // Catch:{ NameNotFoundException -> 0x02cc }
            java.util.Iterator r5 = r5.iterator()     // Catch:{ NameNotFoundException -> 0x02cc }
        L_0x007b:
            boolean r8 = r5.hasNext()     // Catch:{ NameNotFoundException -> 0x02cc }
            if (r8 == 0) goto L_0x00b7
            java.lang.Object r8 = r5.next()     // Catch:{ NameNotFoundException -> 0x02cc }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ NameNotFoundException -> 0x02cc }
            java.lang.String r9 = "GlideModule"
            android.os.Bundle r10 = r7.metaData     // Catch:{ NameNotFoundException -> 0x02cc }
            java.lang.Object r10 = r10.get(r8)     // Catch:{ NameNotFoundException -> 0x02cc }
            boolean r9 = r9.equals(r10)     // Catch:{ NameNotFoundException -> 0x02cc }
            if (r9 == 0) goto L_0x007b
            j.b.a.o.GlideModule r9 = j.b.a.o.ManifestParser.a(r8)     // Catch:{ NameNotFoundException -> 0x02cc }
            r6.add(r9)     // Catch:{ NameNotFoundException -> 0x02cc }
            boolean r9 = android.util.Log.isLoggable(r3, r4)     // Catch:{ NameNotFoundException -> 0x02cc }
            if (r9 == 0) goto L_0x007b
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ NameNotFoundException -> 0x02cc }
            r9.<init>()     // Catch:{ NameNotFoundException -> 0x02cc }
            java.lang.String r10 = "Loaded Glide module: "
            r9.append(r10)     // Catch:{ NameNotFoundException -> 0x02cc }
            r9.append(r8)     // Catch:{ NameNotFoundException -> 0x02cc }
            java.lang.String r8 = r9.toString()     // Catch:{ NameNotFoundException -> 0x02cc }
            android.util.Log.d(r3, r8)     // Catch:{ NameNotFoundException -> 0x02cc }
            goto L_0x007b
        L_0x00b7:
            boolean r5 = android.util.Log.isLoggable(r3, r4)
            if (r5 == 0) goto L_0x00c2
            java.lang.String r5 = "Finished loading Glide modules"
            android.util.Log.d(r3, r5)
        L_0x00c2:
            r17 = r6
        L_0x00c4:
            java.lang.String r3 = "Glide"
            if (r0 == 0) goto L_0x010f
            java.util.Set r5 = r27.b()
            boolean r5 = r5.isEmpty()
            if (r5 != 0) goto L_0x010f
            java.util.Set r5 = r27.b()
            java.util.Iterator r6 = r17.iterator()
        L_0x00da:
            boolean r7 = r6.hasNext()
            if (r7 == 0) goto L_0x010f
            java.lang.Object r7 = r6.next()
            j.b.a.o.GlideModule r7 = (j.b.a.o.GlideModule) r7
            java.lang.Class r8 = r7.getClass()
            boolean r8 = r5.contains(r8)
            if (r8 != 0) goto L_0x00f1
            goto L_0x00da
        L_0x00f1:
            boolean r8 = android.util.Log.isLoggable(r3, r4)
            if (r8 == 0) goto L_0x010b
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "AppGlideModule excludes manifest GlideModule: "
            r8.append(r9)
            r8.append(r7)
            java.lang.String r7 = r8.toString()
            android.util.Log.d(r3, r7)
        L_0x010b:
            r6.remove()
            goto L_0x00da
        L_0x010f:
            boolean r4 = android.util.Log.isLoggable(r3, r4)
            if (r4 == 0) goto L_0x013a
            java.util.Iterator r4 = r17.iterator()
        L_0x0119:
            boolean r5 = r4.hasNext()
            if (r5 == 0) goto L_0x013a
            java.lang.Object r5 = r4.next()
            j.b.a.o.GlideModule r5 = (j.b.a.o.GlideModule) r5
            java.lang.String r6 = "Discovered GlideModule from manifest: "
            java.lang.StringBuilder r6 = j.a.a.a.outline.a(r6)
            java.lang.Class r5 = r5.getClass()
            r6.append(r5)
            java.lang.String r5 = r6.toString()
            android.util.Log.d(r3, r5)
            goto L_0x0119
        L_0x013a:
            if (r0 == 0) goto L_0x0141
            j.b.a.n.RequestManagerRetriever$b r3 = r27.c()
            goto L_0x0142
        L_0x0141:
            r3 = 0
        L_0x0142:
            r2.f1543m = r3
            java.util.Iterator r3 = r17.iterator()
        L_0x0148:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x0158
            java.lang.Object r4 = r3.next()
            j.b.a.o.GlideModule r4 = (j.b.a.o.GlideModule) r4
            r4.a(r15, r2)
            goto L_0x0148
        L_0x0158:
            if (r0 == 0) goto L_0x015d
            r0.a(r15, r2)
        L_0x015d:
            j.b.a.m.m.d0.GlideExecutor r3 = r2.f1538f
            if (r3 != 0) goto L_0x0167
            j.b.a.m.m.d0.GlideExecutor r3 = j.b.a.m.m.d0.GlideExecutor.c()
            r2.f1538f = r3
        L_0x0167:
            j.b.a.m.m.d0.GlideExecutor r3 = r2.g
            if (r3 != 0) goto L_0x0171
            j.b.a.m.m.d0.GlideExecutor r3 = j.b.a.m.m.d0.GlideExecutor.b()
            r2.g = r3
        L_0x0171:
            j.b.a.m.m.d0.GlideExecutor r3 = r2.f1544n
            if (r3 != 0) goto L_0x01b4
            int r3 = j.b.a.m.m.d0.GlideExecutor.a()
            r4 = 4
            if (r3 < r4) goto L_0x017f
            r3 = 2
            r6 = 2
            goto L_0x0181
        L_0x017f:
            r3 = 1
            r6 = 1
        L_0x0181:
            j.b.a.m.m.d0.GlideExecutor$b r3 = j.b.a.m.m.d0.GlideExecutor.b.b
            java.lang.String r4 = "animation"
            boolean r5 = android.text.TextUtils.isEmpty(r4)
            if (r5 != 0) goto L_0x01a8
            java.util.concurrent.ThreadPoolExecutor r12 = new java.util.concurrent.ThreadPoolExecutor
            r7 = 0
            java.util.concurrent.TimeUnit r9 = java.util.concurrent.TimeUnit.MILLISECONDS
            java.util.concurrent.PriorityBlockingQueue r10 = new java.util.concurrent.PriorityBlockingQueue
            r10.<init>()
            j.b.a.m.m.d0.GlideExecutor$a r11 = new j.b.a.m.m.d0.GlideExecutor$a
            r11.<init>(r4, r3, r1)
            r4 = r12
            r5 = r6
            r4.<init>(r5, r6, r7, r9, r10, r11)
            j.b.a.m.m.d0.GlideExecutor r1 = new j.b.a.m.m.d0.GlideExecutor
            r1.<init>(r12)
            r2.f1544n = r1
            goto L_0x01b4
        L_0x01a8:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Name must be non-null and non-empty, but given: "
            java.lang.String r1 = j.a.a.a.outline.a(r1, r4)
            r0.<init>(r1)
            throw r0
        L_0x01b4:
            j.b.a.m.m.c0.MemorySizeCalculator r1 = r2.f1539i
            if (r1 != 0) goto L_0x01c4
            j.b.a.m.m.c0.MemorySizeCalculator$a r1 = new j.b.a.m.m.c0.MemorySizeCalculator$a
            r1.<init>(r15)
            j.b.a.m.m.c0.MemorySizeCalculator r3 = new j.b.a.m.m.c0.MemorySizeCalculator
            r3.<init>(r1)
            r2.f1539i = r3
        L_0x01c4:
            j.b.a.n.ConnectivityMonitorFactory r1 = r2.f1540j
            if (r1 != 0) goto L_0x01cf
            j.b.a.n.DefaultConnectivityMonitorFactory r1 = new j.b.a.n.DefaultConnectivityMonitorFactory
            r1.<init>()
            r2.f1540j = r1
        L_0x01cf:
            j.b.a.m.m.b0.BitmapPool r1 = r2.c
            if (r1 != 0) goto L_0x01e9
            j.b.a.m.m.c0.MemorySizeCalculator r1 = r2.f1539i
            int r1 = r1.a
            if (r1 <= 0) goto L_0x01e2
            j.b.a.m.m.b0.LruBitmapPool r3 = new j.b.a.m.m.b0.LruBitmapPool
            long r4 = (long) r1
            r3.<init>(r4)
            r2.c = r3
            goto L_0x01e9
        L_0x01e2:
            j.b.a.m.m.b0.BitmapPoolAdapter r1 = new j.b.a.m.m.b0.BitmapPoolAdapter
            r1.<init>()
            r2.c = r1
        L_0x01e9:
            j.b.a.m.m.b0.ArrayPool r1 = r2.d
            if (r1 != 0) goto L_0x01f8
            j.b.a.m.m.b0.LruArrayPool r1 = new j.b.a.m.m.b0.LruArrayPool
            j.b.a.m.m.c0.MemorySizeCalculator r3 = r2.f1539i
            int r3 = r3.d
            r1.<init>(r3)
            r2.d = r1
        L_0x01f8:
            j.b.a.m.m.c0.MemoryCache r1 = r2.f1537e
            if (r1 != 0) goto L_0x0208
            j.b.a.m.m.c0.LruResourceCache r1 = new j.b.a.m.m.c0.LruResourceCache
            j.b.a.m.m.c0.MemorySizeCalculator r3 = r2.f1539i
            int r3 = r3.b
            long r3 = (long) r3
            r1.<init>(r3)
            r2.f1537e = r1
        L_0x0208:
            j.b.a.m.m.c0.DiskCache$a r1 = r2.h
            if (r1 != 0) goto L_0x0213
            j.b.a.m.m.c0.InternalCacheDiskCacheFactory0 r1 = new j.b.a.m.m.c0.InternalCacheDiskCacheFactory0
            r1.<init>(r15)
            r2.h = r1
        L_0x0213:
            j.b.a.m.m.Engine r1 = r2.b
            r3 = 0
            if (r1 != 0) goto L_0x0250
            j.b.a.m.m.Engine r1 = new j.b.a.m.m.Engine
            j.b.a.m.m.c0.MemoryCache r5 = r2.f1537e
            j.b.a.m.m.c0.DiskCache$a r6 = r2.h
            j.b.a.m.m.d0.GlideExecutor r7 = r2.g
            j.b.a.m.m.d0.GlideExecutor r8 = r2.f1538f
            j.b.a.m.m.d0.GlideExecutor r9 = new j.b.a.m.m.d0.GlideExecutor
            java.util.concurrent.ThreadPoolExecutor r4 = new java.util.concurrent.ThreadPoolExecutor
            long r21 = j.b.a.m.m.d0.GlideExecutor.b
            java.util.concurrent.TimeUnit r23 = java.util.concurrent.TimeUnit.MILLISECONDS
            java.util.concurrent.SynchronousQueue r24 = new java.util.concurrent.SynchronousQueue
            r24.<init>()
            j.b.a.m.m.d0.GlideExecutor$a r10 = new j.b.a.m.m.d0.GlideExecutor$a
            j.b.a.m.m.d0.GlideExecutor$b r11 = j.b.a.m.m.d0.GlideExecutor.b.b
            java.lang.String r12 = "source-unlimited"
            r10.<init>(r12, r11, r3)
            r20 = 2147483647(0x7fffffff, float:NaN)
            r19 = 0
            r18 = r4
            r25 = r10
            r18.<init>(r19, r20, r21, r23, r24, r25)
            r9.<init>(r4)
            j.b.a.m.m.d0.GlideExecutor r10 = r2.f1544n
            r11 = 0
            r4 = r1
            r4.<init>(r5, r6, r7, r8, r9, r10, r11)
            r2.b = r1
        L_0x0250:
            java.util.List<j.b.a.q.RequestListener<java.lang.Object>> r1 = r2.f1545o
            if (r1 != 0) goto L_0x025b
            java.util.List r1 = java.util.Collections.emptyList()
            r2.f1545o = r1
            goto L_0x0261
        L_0x025b:
            java.util.List r1 = java.util.Collections.unmodifiableList(r1)
            r2.f1545o = r1
        L_0x0261:
            j.b.a.n.RequestManagerRetriever r9 = new j.b.a.n.RequestManagerRetriever
            j.b.a.n.RequestManagerRetriever$b r1 = r2.f1543m
            r9.<init>(r1)
            j.b.a.Glide r1 = new j.b.a.Glide
            j.b.a.m.m.Engine r5 = r2.b
            j.b.a.m.m.c0.MemoryCache r6 = r2.f1537e
            j.b.a.m.m.b0.BitmapPool r7 = r2.c
            j.b.a.m.m.b0.ArrayPool r8 = r2.d
            j.b.a.n.ConnectivityMonitorFactory r10 = r2.f1540j
            int r11 = r2.f1541k
            j.b.a.Glide$a r12 = r2.f1542l
            java.util.Map<java.lang.Class<?>, j.b.a.TransitionOptions<?, ?>> r13 = r2.a
            java.util.List<j.b.a.q.RequestListener<java.lang.Object>> r14 = r2.f1545o
            r2 = 0
            r16 = 0
            r18 = 0
            r3 = r1
            r4 = r15
            r0 = r15
            r15 = r2
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16)
            java.util.Iterator r2 = r17.iterator()
        L_0x028c:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x02ba
            java.lang.Object r3 = r2.next()
            j.b.a.o.GlideModule r3 = (j.b.a.o.GlideModule) r3
            com.bumptech.glide.Registry r4 = r1.f1534e     // Catch:{ AbstractMethodError -> 0x029e }
            r3.a(r0, r1, r4)     // Catch:{ AbstractMethodError -> 0x029e }
            goto L_0x028c
        L_0x029e:
            r0 = move-exception
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "Attempting to register a Glide v3 module. If you see this, you or one of your dependencies may be including Glide v3 even though you're using Glide v4. You'll need to find and remove (or update) the offending dependency. The v3 module name is: "
            java.lang.StringBuilder r2 = j.a.a.a.outline.a(r2)
            java.lang.Class r3 = r3.getClass()
            java.lang.String r3 = r3.getName()
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2, r0)
            throw r1
        L_0x02ba:
            r2 = r0
            r0 = r27
            if (r0 == 0) goto L_0x02c4
            com.bumptech.glide.Registry r3 = r1.f1534e
            r0.a(r2, r1, r3)
        L_0x02c4:
            r2.registerComponentCallbacks(r1)
            j.b.a.Glide.f1532j = r1
            j.b.a.Glide.f1533k = r18
            return
        L_0x02cc:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r2 = "Unable to find metadata to parse GlideModules"
            r1.<init>(r2, r0)
            throw r1
        L_0x02d5:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "You cannot call Glide.get() in registerComponents(), use the provided Glide instance instead"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: j.b.a.Glide.a(android.content.Context, com.bumptech.glide.GeneratedAppGlideModule):void");
    }

    public static void a(Exception exc) {
        throw new IllegalStateException("GeneratedAppGlideModuleImpl is implemented incorrectly. If you've manually implemented this class, remove your implementation. The Annotation processor will generate a correct implementation.", exc);
    }

    public boolean a(Target<?> target) {
        synchronized (this.f1536i) {
            Iterator<i> it = this.f1536i.iterator();
            while (it.hasNext()) {
                if (it.next().b(target)) {
                    return true;
                }
            }
            return false;
        }
    }

    public void a(RequestManager requestManager) {
        synchronized (this.f1536i) {
            if (!this.f1536i.contains(requestManager)) {
                this.f1536i.add(requestManager);
            } else {
                throw new IllegalStateException("Cannot register already registered manager");
            }
        }
    }
}
