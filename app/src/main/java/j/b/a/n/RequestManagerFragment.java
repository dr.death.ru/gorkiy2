package j.b.a.n;

import android.app.Activity;
import android.app.Fragment;
import android.util.Log;
import j.b.a.Glide;
import j.b.a.RequestManager;
import java.util.HashSet;
import java.util.Set;

@Deprecated
public class RequestManagerFragment extends Fragment {
    public final ActivityFragmentLifecycle b;
    public final RequestManagerTreeNode c = new a();
    public final Set<k> d = new HashSet();

    /* renamed from: e  reason: collision with root package name */
    public RequestManager f1740e;

    /* renamed from: f  reason: collision with root package name */
    public RequestManagerFragment f1741f;
    public Fragment g;

    public class a implements RequestManagerTreeNode {
        public a() {
        }

        public String toString() {
            return super.toString() + "{fragment=" + RequestManagerFragment.this + "}";
        }
    }

    public RequestManagerFragment() {
        ActivityFragmentLifecycle activityFragmentLifecycle = new ActivityFragmentLifecycle();
        this.b = activityFragmentLifecycle;
    }

    public final void a(Activity activity) {
        a();
        RequestManagerRetriever requestManagerRetriever = Glide.a(activity).g;
        if (requestManagerRetriever != null) {
            RequestManagerFragment a2 = requestManagerRetriever.a(activity.getFragmentManager(), (Fragment) null, RequestManagerRetriever.d(activity));
            this.f1741f = a2;
            if (!equals(a2)) {
                this.f1741f.d.add(this);
                return;
            }
            return;
        }
        throw null;
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            a(activity);
        } catch (IllegalStateException e2) {
            if (Log.isLoggable("RMFragment", 5)) {
                Log.w("RMFragment", "Unable to register fragment with root", e2);
            }
        }
    }

    public void onDestroy() {
        super.onDestroy();
        this.b.a();
        a();
    }

    public void onDetach() {
        super.onDetach();
        a();
    }

    public void onStart() {
        super.onStart();
        this.b.b();
    }

    public void onStop() {
        super.onStop();
        this.b.c();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("{parent=");
        Fragment parentFragment = getParentFragment();
        if (parentFragment == null) {
            parentFragment = this.g;
        }
        sb.append(parentFragment);
        sb.append("}");
        return sb.toString();
    }

    public final void a() {
        RequestManagerFragment requestManagerFragment = this.f1741f;
        if (requestManagerFragment != null) {
            requestManagerFragment.d.remove(this);
            this.f1741f = null;
        }
    }
}
