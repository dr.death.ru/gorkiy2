package j.c.b.a.v;

import j.c.a.a.c.n.c;
import j.c.b.a.Registry;
import j.c.b.a.z.RegistryConfig;
import java.security.GeneralSecurityException;

public final class DeterministicAeadConfig {
    @Deprecated
    public static final RegistryConfig a;
    public static final RegistryConfig b;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.String, java.lang.String, java.lang.String, int, boolean):j.c.b.a.z.KeyTypeEntry
     arg types: [java.lang.String, java.lang.String, java.lang.String, int, int]
     candidates:
      j.c.a.a.c.n.c.a(int, byte[], int, int, j.c.a.a.f.e.s2):int
      j.c.a.a.c.n.c.a(j.c.a.a.f.e.t5, byte[], int, int, j.c.a.a.f.e.s2):int
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String):java.security.spec.ECParameterSpec
      j.c.a.a.c.n.c.a(byte, byte, byte, char[], int):void
      j.c.a.a.c.n.c.a(byte[], int, byte[], int, int):byte[]
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String, java.lang.String, int, boolean):j.c.b.a.z.KeyTypeEntry */
    static {
        RegistryConfig.b g = RegistryConfig.g();
        g.a(c.a("TinkDeterministicAead", "DeterministicAead", "AesSivKey", 0, true));
        g.m();
        RegistryConfig.a((RegistryConfig) g.c, "TINK_DETERMINISTIC_AEAD_1_1_0");
        a = (RegistryConfig) g.k();
        RegistryConfig.b g2 = RegistryConfig.g();
        g2.a(c.a("TinkDeterministicAead", "DeterministicAead", "AesSivKey", 0, true));
        g2.m();
        RegistryConfig.a((RegistryConfig) g2.c, "TINK_DETERMINISTIC_AEAD");
        b = (RegistryConfig) g2.k();
        try {
            Registry.a("TinkDeterministicAead", new DeterministicAeadCatalogue());
            c.a(b);
        } catch (GeneralSecurityException e2) {
            throw new ExceptionInInitializerError(e2);
        }
    }

    public static void a() {
        Registry.a("TinkDeterministicAead", new DeterministicAeadCatalogue());
        c.a(b);
    }
}
