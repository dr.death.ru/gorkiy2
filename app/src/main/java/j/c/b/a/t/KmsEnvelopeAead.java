package j.c.b.a.t;

import j.c.b.a.Aead;
import j.c.b.a.Registry;
import j.c.b.a.z.KeyTemplate;
import j.c.b.a.z.z1;
import j.c.e.ByteString;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;

public final class KmsEnvelopeAead implements Aead {
    public static final byte[] c = new byte[0];
    public final KeyTemplate a;
    public final Aead b;

    public KmsEnvelopeAead(KeyTemplate keyTemplate, Aead aead) {
        this.a = keyTemplate;
        this.b = aead;
    }

    public byte[] a(byte[] bArr, byte[] bArr2) {
        byte[] b2 = Registry.a((z1) this.a).b();
        byte[] a2 = this.b.a(b2, c);
        String str = this.a.f2481e;
        byte[] a3 = ((Aead) Registry.b(str).c(ByteString.a(b2))).a(bArr, bArr2);
        return ByteBuffer.allocate(a2.length + 4 + a3.length).putInt(a2.length).put(a2).put(a3).array();
    }

    public byte[] b(byte[] bArr, byte[] bArr2) {
        try {
            ByteBuffer wrap = ByteBuffer.wrap(bArr);
            int i2 = wrap.getInt();
            if (i2 <= 0 || i2 > bArr.length - 4) {
                throw new GeneralSecurityException("invalid ciphertext");
            }
            byte[] bArr3 = new byte[i2];
            wrap.get(bArr3, 0, i2);
            byte[] bArr4 = new byte[wrap.remaining()];
            wrap.get(bArr4, 0, wrap.remaining());
            return ((Aead) Registry.a(this.a.f2481e, this.b.b(bArr3, c))).b(bArr4, bArr2);
        } catch (IndexOutOfBoundsException | NegativeArraySizeException | BufferUnderflowException e2) {
            throw new GeneralSecurityException("invalid ciphertext", e2);
        }
    }
}
