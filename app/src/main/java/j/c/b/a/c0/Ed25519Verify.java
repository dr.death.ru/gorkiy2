package j.c.b.a.c0;

import j.c.b.a.PublicKeyVerify;

public final class Ed25519Verify implements PublicKeyVerify {
    public Ed25519Verify(byte[] bArr) {
        if (bArr.length == 32) {
            int length = bArr.length;
            System.arraycopy(bArr, 0, new byte[length], 0, length);
            return;
        }
        throw new IllegalArgumentException(String.format("Given public key's length is not %s.", 32));
    }
}
