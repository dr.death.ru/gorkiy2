package j.c.b.a.z;

import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.InvalidProtocolBufferException;
import j.c.b.a.z.q2;
import j.c.e.AbstractProtobufList;
import j.c.e.CodedInputStream;
import j.c.e.ExtensionRegistryLite;
import j.c.e.GeneratedMessageLite;
import j.c.e.Internal;
import j.c.e.Parser;
import j.c.e.ProtobufArrayList;
import java.io.IOException;

public final class RegistryConfig extends GeneratedMessageLite<q2, q2.b> implements r2 {
    public static final RegistryConfig h;

    /* renamed from: i  reason: collision with root package name */
    public static volatile Parser<q2> f2510i;

    /* renamed from: e  reason: collision with root package name */
    public int f2511e;

    /* renamed from: f  reason: collision with root package name */
    public String f2512f = "";
    public Internal.c<b2> g = ProtobufArrayList.d;

    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        /* JADX WARNING: Can't wrap try/catch for region: R(20:0|(2:1|2)|3|5|6|7|8|9|11|12|13|14|15|(2:17|18)|19|21|22|23|24|26) */
        /* JADX WARNING: Can't wrap try/catch for region: R(22:0|1|2|3|5|6|7|8|9|11|12|13|14|15|17|18|19|21|22|23|24|26) */
        /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0025 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0039 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0017 */
        static {
            /*
                j.c.e.GeneratedMessageLite$j[] r0 = j.c.e.GeneratedMessageLite.j.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                j.c.b.a.z.RegistryConfig.a.a = r0
                r1 = 1
                r2 = 4
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.NEW_MUTABLE_INSTANCE     // Catch:{ NoSuchFieldError -> 0x000f }
                r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x000f }
            L_0x000f:
                r0 = 2
                int[] r3 = j.c.b.a.z.RegistryConfig.a.a     // Catch:{ NoSuchFieldError -> 0x0017 }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.IS_INITIALIZED     // Catch:{ NoSuchFieldError -> 0x0017 }
                r4 = 0
                r3[r4] = r0     // Catch:{ NoSuchFieldError -> 0x0017 }
            L_0x0017:
                int[] r3 = j.c.b.a.z.RegistryConfig.a.a     // Catch:{ NoSuchFieldError -> 0x001e }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.MAKE_IMMUTABLE     // Catch:{ NoSuchFieldError -> 0x001e }
                r4 = 3
                r3[r4] = r4     // Catch:{ NoSuchFieldError -> 0x001e }
            L_0x001e:
                r3 = 5
                int[] r4 = j.c.b.a.z.RegistryConfig.a.a     // Catch:{ NoSuchFieldError -> 0x0025 }
                j.c.e.GeneratedMessageLite$j r5 = j.c.e.GeneratedMessageLite.j.NEW_BUILDER     // Catch:{ NoSuchFieldError -> 0x0025 }
                r4[r3] = r2     // Catch:{ NoSuchFieldError -> 0x0025 }
            L_0x0025:
                int[] r2 = j.c.b.a.z.RegistryConfig.a.a     // Catch:{ NoSuchFieldError -> 0x002b }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.VISIT     // Catch:{ NoSuchFieldError -> 0x002b }
                r2[r1] = r3     // Catch:{ NoSuchFieldError -> 0x002b }
            L_0x002b:
                r1 = 6
                int[] r2 = j.c.b.a.z.RegistryConfig.a.a     // Catch:{ NoSuchFieldError -> 0x0032 }
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.MERGE_FROM_STREAM     // Catch:{ NoSuchFieldError -> 0x0032 }
                r2[r0] = r1     // Catch:{ NoSuchFieldError -> 0x0032 }
            L_0x0032:
                r0 = 7
                int[] r2 = j.c.b.a.z.RegistryConfig.a.a     // Catch:{ NoSuchFieldError -> 0x0039 }
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.GET_DEFAULT_INSTANCE     // Catch:{ NoSuchFieldError -> 0x0039 }
                r2[r1] = r0     // Catch:{ NoSuchFieldError -> 0x0039 }
            L_0x0039:
                int[] r1 = j.c.b.a.z.RegistryConfig.a.a     // Catch:{ NoSuchFieldError -> 0x0041 }
                j.c.e.GeneratedMessageLite$j r2 = j.c.e.GeneratedMessageLite.j.GET_PARSER     // Catch:{ NoSuchFieldError -> 0x0041 }
                r2 = 8
                r1[r0] = r2     // Catch:{ NoSuchFieldError -> 0x0041 }
            L_0x0041:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: j.c.b.a.z.RegistryConfig.a.<clinit>():void");
        }
    }

    static {
        RegistryConfig registryConfig = new RegistryConfig();
        h = registryConfig;
        super.f();
    }

    public static /* synthetic */ void a(RegistryConfig registryConfig, String str) {
        if (str != null) {
            registryConfig.f2512f = str;
            return;
        }
        throw null;
    }

    public static b g() {
        return (b) h.e();
    }

    public int c() {
        int i2 = super.d;
        if (i2 != -1) {
            return i2;
        }
        int b2 = !this.f2512f.isEmpty() ? CodedOutputStream.b(1, this.f2512f) + 0 : 0;
        for (int i3 = 0; i3 < this.g.size(); i3++) {
            b2 += CodedOutputStream.b(2, this.g.get(i3));
        }
        super.d = b2;
        return b2;
    }

    public static final class b extends GeneratedMessageLite.b<q2, q2.b> implements r2 {
        public b() {
            super(RegistryConfig.h);
        }

        public b a(KeyTypeEntry keyTypeEntry) {
            m();
            RegistryConfig.a((RegistryConfig) super.c, keyTypeEntry);
            return this;
        }

        public /* synthetic */ b(a aVar) {
            super(RegistryConfig.h);
        }
    }

    public static /* synthetic */ void a(RegistryConfig registryConfig, KeyTypeEntry keyTypeEntry) {
        if (keyTypeEntry != null) {
            Internal.c<b2> cVar = registryConfig.g;
            if (!((AbstractProtobufList) cVar).b) {
                registryConfig.g = GeneratedMessageLite.a(cVar);
            }
            registryConfig.g.add(keyTypeEntry);
            return;
        }
        throw null;
    }

    public void a(CodedOutputStream codedOutputStream) {
        if (!this.f2512f.isEmpty()) {
            codedOutputStream.a(1, this.f2512f);
        }
        for (int i2 = 0; i2 < this.g.size(); i2++) {
            codedOutputStream.a(2, this.g.get(i2));
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final Object a(GeneratedMessageLite.j jVar, Object obj, Object obj2) {
        boolean z = false;
        switch (jVar.ordinal()) {
            case 0:
                return h;
            case 1:
                GeneratedMessageLite.k kVar = (GeneratedMessageLite.k) obj;
                RegistryConfig registryConfig = (RegistryConfig) obj2;
                this.f2512f = kVar.a(!this.f2512f.isEmpty(), this.f2512f, true ^ registryConfig.f2512f.isEmpty(), registryConfig.f2512f);
                this.g = kVar.a(this.g, registryConfig.g);
                if (kVar == GeneratedMessageLite.i.a) {
                    this.f2511e |= registryConfig.f2511e;
                }
                return this;
            case 2:
                CodedInputStream codedInputStream = (CodedInputStream) obj;
                ExtensionRegistryLite extensionRegistryLite = (ExtensionRegistryLite) obj2;
                while (!z) {
                    try {
                        int g2 = codedInputStream.g();
                        if (g2 != 0) {
                            if (g2 == 10) {
                                this.f2512f = codedInputStream.f();
                            } else if (g2 == 18) {
                                if (!((AbstractProtobufList) this.g).b) {
                                    this.g = GeneratedMessageLite.a(this.g);
                                }
                                this.g.add(codedInputStream.a(KeyTypeEntry.f2483j.i(), extensionRegistryLite));
                            } else if (!codedInputStream.e(g2)) {
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e2) {
                        throw new RuntimeException(e2);
                    } catch (IOException e3) {
                        throw new RuntimeException(new InvalidProtocolBufferException(e3.getMessage()));
                    }
                }
                break;
            case 3:
                ((AbstractProtobufList) this.g).b = false;
                return null;
            case 4:
                return new RegistryConfig();
            case 5:
                return new b(null);
            case 6:
                break;
            case 7:
                if (f2510i == null) {
                    synchronized (RegistryConfig.class) {
                        if (f2510i == null) {
                            f2510i = new GeneratedMessageLite.c(h);
                        }
                    }
                }
                return f2510i;
            default:
                throw new UnsupportedOperationException();
        }
        return h;
    }
}
