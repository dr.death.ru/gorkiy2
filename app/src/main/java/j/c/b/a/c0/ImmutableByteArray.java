package j.c.b.a.c0;

public final class ImmutableByteArray {
    public final byte[] a;

    public ImmutableByteArray(byte[] bArr, int i2, int i3) {
        byte[] bArr2 = new byte[i3];
        this.a = bArr2;
        System.arraycopy(bArr, i2, bArr2, 0, i3);
    }
}
