package j.c.b.a.b0;

import com.google.protobuf.InvalidProtocolBufferException;
import j.c.a.a.c.n.c;
import j.c.b.a.KeyManager;
import j.c.b.a.c0.AesCtrHmacStreaming;
import j.c.b.a.c0.NonceBasedStreamingAead;
import j.c.b.a.c0.Random;
import j.c.b.a.c0.Validators;
import j.c.b.a.c0.f0;
import j.c.b.a.z.AesCtrHmacStreamingKey;
import j.c.b.a.z.AesCtrHmacStreamingKeyFormat;
import j.c.b.a.z.AesCtrHmacStreamingParams;
import j.c.b.a.z.HashType;
import j.c.b.a.z.HmacParams;
import j.c.b.a.z.KeyData;
import j.c.e.ByteString;
import j.c.e.GeneratedMessageLite;
import j.c.e.MessageLite;
import j.c.e.f;
import java.security.GeneralSecurityException;

public class AesCtrHmacStreamingKeyManager implements KeyManager<f0> {
    public String a() {
        return "type.googleapis.com/google.crypto.tink.AesCtrHmacStreamingKey";
    }

    public int b() {
        return 0;
    }

    /* JADX WARN: Type inference failed for: r4v6, types: [j.c.e.MessageLite, j.c.e.GeneratedMessageLite] */
    public MessageLite b(MessageLite messageLite) {
        if (messageLite instanceof AesCtrHmacStreamingKeyFormat) {
            AesCtrHmacStreamingKeyFormat aesCtrHmacStreamingKeyFormat = (AesCtrHmacStreamingKeyFormat) messageLite;
            if (aesCtrHmacStreamingKeyFormat.f2397f >= 16) {
                a(aesCtrHmacStreamingKeyFormat.g());
                AesCtrHmacStreamingKey.b bVar = (AesCtrHmacStreamingKey.b) AesCtrHmacStreamingKey.h.e();
                ByteString a = ByteString.a(Random.a(aesCtrHmacStreamingKeyFormat.f2397f));
                bVar.m();
                bVar.c.g = a;
                AesCtrHmacStreamingParams g = aesCtrHmacStreamingKeyFormat.g();
                bVar.m();
                AesCtrHmacStreamingKey.a(bVar.c, g);
                bVar.m();
                bVar.c.f2394e = 0;
                return bVar.k();
            }
            throw new GeneralSecurityException("key_size must be at least 16 bytes");
        }
        throw new GeneralSecurityException("expected AesCtrHmacStreamingKeyFormat proto");
    }

    /* JADX WARN: Type inference failed for: r3v3, types: [j.c.e.MessageLite, j.c.b.a.z.AesCtrHmacStreamingKey] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
     arg types: [j.c.b.a.z.AesCtrHmacStreamingKey, j.c.e.ByteString]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T */
    public Object c(ByteString byteString) {
        try {
            return a((MessageLite) ((AesCtrHmacStreamingKey) GeneratedMessageLite.a((GeneratedMessageLite) AesCtrHmacStreamingKey.h, (f) byteString)));
        } catch (InvalidProtocolBufferException e2) {
            throw new GeneralSecurityException("expected AesCtrHmacStreamingKey proto", e2);
        }
    }

    public final void a(AesCtrHmacStreamingParams aesCtrHmacStreamingParams) {
        Validators.a(aesCtrHmacStreamingParams.f2401f);
        HashType a = HashType.a(aesCtrHmacStreamingParams.g);
        if (a == null) {
            a = HashType.UNRECOGNIZED;
        }
        if (a == HashType.UNKNOWN_HASH) {
            throw new GeneralSecurityException("unknown HKDF hash type");
        } else if (aesCtrHmacStreamingParams.g().g() != HashType.UNKNOWN_HASH) {
            HmacParams g = aesCtrHmacStreamingParams.g();
            if (g.f2476f >= 10) {
                int ordinal = g.g().ordinal();
                if (ordinal != 1) {
                    if (ordinal != 2) {
                        if (ordinal != 3) {
                            throw new GeneralSecurityException("unknown hash type");
                        } else if (g.f2476f > 64) {
                            throw new GeneralSecurityException("tag size too big");
                        }
                    } else if (g.f2476f > 32) {
                        throw new GeneralSecurityException("tag size too big");
                    }
                } else if (g.f2476f > 20) {
                    throw new GeneralSecurityException("tag size too big");
                }
                if (aesCtrHmacStreamingParams.f2400e < aesCtrHmacStreamingParams.f2401f + aesCtrHmacStreamingParams.g().f2476f + 8) {
                    throw new GeneralSecurityException("ciphertext_segment_size must be at least (derived_key_size + tag_size + 8)");
                }
                return;
            }
            throw new GeneralSecurityException("tag size too small");
        } else {
            throw new GeneralSecurityException("unknown HMAC hash type");
        }
    }

    public KeyData b(ByteString byteString) {
        KeyData.b g = KeyData.g();
        g.m();
        KeyData.a((KeyData) g.c, "type.googleapis.com/google.crypto.tink.AesCtrHmacStreamingKey");
        ByteString a = ((AesCtrHmacStreamingKey) a(byteString)).a();
        g.m();
        KeyData.a((KeyData) g.c, a);
        g.a(KeyData.c.SYMMETRIC);
        return (KeyData) g.k();
    }

    public NonceBasedStreamingAead a(MessageLite messageLite) {
        if (messageLite instanceof AesCtrHmacStreamingKey) {
            AesCtrHmacStreamingKey aesCtrHmacStreamingKey = (AesCtrHmacStreamingKey) messageLite;
            Validators.a(aesCtrHmacStreamingKey.f2394e, 0);
            if (aesCtrHmacStreamingKey.g.size() < 16) {
                throw new GeneralSecurityException("key_value must have at least 16 bytes");
            } else if (aesCtrHmacStreamingKey.g.size() >= aesCtrHmacStreamingKey.g().f2401f) {
                a(aesCtrHmacStreamingKey.g());
                byte[] d = aesCtrHmacStreamingKey.g.d();
                HashType a = HashType.a(aesCtrHmacStreamingKey.g().g);
                if (a == null) {
                    a = HashType.UNRECOGNIZED;
                }
                return new AesCtrHmacStreaming(d, c.c(a), aesCtrHmacStreamingKey.g().f2401f, c.c(aesCtrHmacStreamingKey.g().g().g()), aesCtrHmacStreamingKey.g().g().f2476f, aesCtrHmacStreamingKey.g().f2400e, 0);
            } else {
                throw new GeneralSecurityException("key_value must have at least as many bits as derived keys");
            }
        } else {
            throw new GeneralSecurityException("expected AesCtrHmacStreamingKey proto");
        }
    }

    public boolean a(String str) {
        return str.equals("type.googleapis.com/google.crypto.tink.AesCtrHmacStreamingKey");
    }

    /* JADX WARN: Type inference failed for: r3v3, types: [j.c.e.MessageLite, j.c.b.a.z.AesCtrHmacStreamingKeyFormat] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
     arg types: [j.c.b.a.z.AesCtrHmacStreamingKeyFormat, j.c.e.ByteString]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T */
    public MessageLite a(ByteString byteString) {
        try {
            return b((MessageLite) ((AesCtrHmacStreamingKeyFormat) GeneratedMessageLite.a((GeneratedMessageLite) AesCtrHmacStreamingKeyFormat.g, (f) byteString)));
        } catch (InvalidProtocolBufferException e2) {
            throw new GeneralSecurityException("expected serialized AesCtrHmacStreamingKeyFormat proto", e2);
        }
    }
}
