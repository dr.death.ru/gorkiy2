package j.c.b.a.w;

import j.c.a.a.c.n.c;
import j.c.b.a.Registry;
import j.c.b.a.t.AeadConfig;
import j.c.b.a.z.RegistryConfig;
import java.security.GeneralSecurityException;

public final class HybridConfig {
    @Deprecated
    public static final RegistryConfig a;
    @Deprecated
    public static final RegistryConfig b;
    public static final RegistryConfig c;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.String, java.lang.String, java.lang.String, int, boolean):j.c.b.a.z.KeyTypeEntry
     arg types: [java.lang.String, java.lang.String, java.lang.String, int, int]
     candidates:
      j.c.a.a.c.n.c.a(int, byte[], int, int, j.c.a.a.f.e.s2):int
      j.c.a.a.c.n.c.a(j.c.a.a.f.e.t5, byte[], int, int, j.c.a.a.f.e.s2):int
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String):java.security.spec.ECParameterSpec
      j.c.a.a.c.n.c.a(byte, byte, byte, char[], int):void
      j.c.a.a.c.n.c.a(byte[], int, byte[], int, int):byte[]
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String, java.lang.String, int, boolean):j.c.b.a.z.KeyTypeEntry */
    static {
        RegistryConfig.b g = RegistryConfig.g();
        g.a(AeadConfig.a);
        RegistryConfig.b bVar = g;
        bVar.a(c.a("TinkHybridDecrypt", "HybridDecrypt", "EciesAeadHkdfPrivateKey", 0, true));
        bVar.a(c.a("TinkHybridEncrypt", "HybridEncrypt", "EciesAeadHkdfPublicKey", 0, true));
        bVar.m();
        RegistryConfig.a((RegistryConfig) bVar.c, "TINK_HYBRID_1_0_0");
        a = (RegistryConfig) bVar.k();
        RegistryConfig.b g2 = RegistryConfig.g();
        g2.a(a);
        RegistryConfig.b bVar2 = g2;
        bVar2.m();
        RegistryConfig.a((RegistryConfig) bVar2.c, "TINK_HYBRID_1_1_0");
        b = (RegistryConfig) bVar2.k();
        RegistryConfig.b g3 = RegistryConfig.g();
        g3.a(AeadConfig.b);
        RegistryConfig.b bVar3 = g3;
        bVar3.a(c.a("TinkHybridDecrypt", "HybridDecrypt", "EciesAeadHkdfPrivateKey", 0, true));
        bVar3.a(c.a("TinkHybridEncrypt", "HybridEncrypt", "EciesAeadHkdfPublicKey", 0, true));
        bVar3.m();
        RegistryConfig.a((RegistryConfig) bVar3.c, "TINK_HYBRID");
        c = (RegistryConfig) bVar3.k();
        try {
            a();
        } catch (GeneralSecurityException e2) {
            throw new ExceptionInInitializerError(e2);
        }
    }

    public static void a() {
        AeadConfig.a();
        Registry.a("TinkHybridEncrypt", new HybridEncryptCatalogue());
        Registry.a("TinkHybridDecrypt", new HybridDecryptCatalogue());
        c.a(c);
    }
}
