package j.c.b.a.c0;

import j.c.a.a.c.n.c;
import j.c.b.a.Aead;
import j.c.b.a.Mac;
import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.util.Arrays;

public final class EncryptThenAuthenticate implements Aead {
    public final IndCpaCipher a;
    public final Mac b;
    public final int c;

    public EncryptThenAuthenticate(IndCpaCipher indCpaCipher, Mac mac, int i2) {
        this.a = indCpaCipher;
        this.b = mac;
        this.c = i2;
    }

    public byte[] a(byte[] bArr, byte[] bArr2) {
        byte[] a2 = this.a.a(bArr);
        if (bArr2 == null) {
            bArr2 = new byte[0];
        }
        byte[] copyOf = Arrays.copyOf(ByteBuffer.allocate(8).putLong(((long) bArr2.length) * 8).array(), 8);
        return c.a(a2, this.b.a(c.a(bArr2, a2, copyOf)));
    }

    public byte[] b(byte[] bArr, byte[] bArr2) {
        int length = bArr.length;
        int i2 = this.c;
        if (length >= i2) {
            byte[] copyOfRange = Arrays.copyOfRange(bArr, 0, bArr.length - i2);
            byte[] copyOfRange2 = Arrays.copyOfRange(bArr, bArr.length - this.c, bArr.length);
            if (bArr2 == null) {
                bArr2 = new byte[0];
            }
            byte[] copyOf = Arrays.copyOf(ByteBuffer.allocate(8).putLong(((long) bArr2.length) * 8).array(), 8);
            this.b.a(copyOfRange2, c.a(bArr2, copyOfRange, copyOf));
            return this.a.b(copyOfRange);
        }
        throw new GeneralSecurityException("ciphertext too short");
    }
}
