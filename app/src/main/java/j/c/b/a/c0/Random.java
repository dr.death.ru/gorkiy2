package j.c.b.a.c0;

import java.security.SecureRandom;

public final class Random {
    public static final ThreadLocal<SecureRandom> a = new a();

    public class a extends ThreadLocal<SecureRandom> {
        public Object initialValue() {
            SecureRandom secureRandom = new SecureRandom();
            secureRandom.nextLong();
            return secureRandom;
        }
    }

    public static byte[] a(int i2) {
        byte[] bArr = new byte[i2];
        a.get().nextBytes(bArr);
        return bArr;
    }
}
