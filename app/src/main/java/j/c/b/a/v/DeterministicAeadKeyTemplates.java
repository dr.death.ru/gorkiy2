package j.c.b.a.v;

import j.c.b.a.z.AesSivKeyFormat;
import j.c.b.a.z.KeyTemplate;
import j.c.b.a.z.OutputPrefixType;
import j.c.e.ByteString;

public final class DeterministicAeadKeyTemplates {
    public static final KeyTemplate a;

    static {
        AesSivKeyFormat.b bVar = (AesSivKeyFormat.b) AesSivKeyFormat.f2430f.e();
        bVar.m();
        ((AesSivKeyFormat) bVar.c).f2431e = 64;
        KeyTemplate.b g = KeyTemplate.g();
        ByteString a2 = ((AesSivKeyFormat) bVar.k()).a();
        g.m();
        KeyTemplate.a((KeyTemplate) g.c, a2);
        g.m();
        KeyTemplate.a((KeyTemplate) g.c, "type.googleapis.com/google.crypto.tink.AesSivKey");
        g.a(OutputPrefixType.TINK);
        a = (KeyTemplate) g.k();
    }
}
