package j.c.b.a;

import j.c.b.a.z.KeyData;
import j.c.e.ByteString;
import j.c.e.MessageLite;
import j.c.e.f;
import j.c.e.o;

public interface KeyManager<P> {
    MessageLite a(ByteString byteString);

    P a(o oVar);

    String a();

    boolean a(String str);

    int b();

    KeyData b(ByteString byteString);

    MessageLite b(MessageLite messageLite);

    P c(f fVar);
}
