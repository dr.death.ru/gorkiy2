package j.c.b.a.t;

import com.google.protobuf.InvalidProtocolBufferException;
import j.c.b.a.Aead;
import j.c.b.a.KeyManager;
import j.c.b.a.KmsClients;
import j.c.b.a.a;
import j.c.b.a.c0.Validators;
import j.c.b.a.z.KeyData;
import j.c.b.a.z.KmsEnvelopeAeadKey;
import j.c.b.a.z.KmsEnvelopeAeadKeyFormat;
import j.c.e.ByteString;
import j.c.e.GeneratedMessageLite;
import j.c.e.MessageLite;
import j.c.e.f;
import java.security.GeneralSecurityException;

public class KmsEnvelopeAeadKeyManager implements KeyManager<a> {
    public String a() {
        return "type.googleapis.com/google.crypto.tink.KmsEnvelopeAeadKey";
    }

    public int b() {
        return 0;
    }

    /* JADX WARN: Type inference failed for: r3v4, types: [j.c.e.MessageLite, j.c.e.GeneratedMessageLite] */
    public MessageLite b(MessageLite messageLite) {
        if (messageLite instanceof KmsEnvelopeAeadKeyFormat) {
            KmsEnvelopeAeadKey.b bVar = (KmsEnvelopeAeadKey.b) KmsEnvelopeAeadKey.g.e();
            bVar.m();
            KmsEnvelopeAeadKey.a(bVar.c, (KmsEnvelopeAeadKeyFormat) messageLite);
            bVar.m();
            bVar.c.f2506e = 0;
            return bVar.k();
        }
        throw new GeneralSecurityException("expected KmsEnvelopeAeadKeyFormat proto");
    }

    /* JADX WARN: Type inference failed for: r3v3, types: [j.c.b.a.z.KmsEnvelopeAeadKey, j.c.e.MessageLite] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
     arg types: [j.c.b.a.z.KmsEnvelopeAeadKey, j.c.e.ByteString]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T */
    public Object c(ByteString byteString) {
        try {
            return a((MessageLite) ((KmsEnvelopeAeadKey) GeneratedMessageLite.a((GeneratedMessageLite) KmsEnvelopeAeadKey.g, (f) byteString)));
        } catch (InvalidProtocolBufferException e2) {
            throw new GeneralSecurityException("expected serialized KmSEnvelopeAeadKey proto", e2);
        }
    }

    public Aead a(MessageLite messageLite) {
        if (messageLite instanceof KmsEnvelopeAeadKey) {
            KmsEnvelopeAeadKey kmsEnvelopeAeadKey = (KmsEnvelopeAeadKey) messageLite;
            Validators.a(kmsEnvelopeAeadKey.f2506e, 0);
            String str = kmsEnvelopeAeadKey.g().f2508e;
            return new KmsEnvelopeAead(kmsEnvelopeAeadKey.g().g(), KmsClients.a(str).b(str));
        }
        throw new GeneralSecurityException("expected KmsEnvelopeAeadKey proto");
    }

    public KeyData b(ByteString byteString) {
        KeyData.b g = KeyData.g();
        g.m();
        KeyData.a((KeyData) g.c, "type.googleapis.com/google.crypto.tink.KmsEnvelopeAeadKey");
        ByteString a = ((KmsEnvelopeAeadKey) a(byteString)).a();
        g.m();
        KeyData.a((KeyData) g.c, a);
        g.a(KeyData.c.REMOTE);
        return (KeyData) g.k();
    }

    public boolean a(String str) {
        return str.equals("type.googleapis.com/google.crypto.tink.KmsEnvelopeAeadKey");
    }

    /* JADX WARN: Type inference failed for: r3v3, types: [j.c.e.MessageLite, j.c.b.a.z.KmsEnvelopeAeadKeyFormat] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
     arg types: [j.c.b.a.z.KmsEnvelopeAeadKeyFormat, j.c.e.ByteString]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T */
    public MessageLite a(ByteString byteString) {
        try {
            return b((MessageLite) ((KmsEnvelopeAeadKeyFormat) GeneratedMessageLite.a((GeneratedMessageLite) KmsEnvelopeAeadKeyFormat.g, (f) byteString)));
        } catch (InvalidProtocolBufferException e2) {
            throw new GeneralSecurityException("expected serialized KmsEnvelopeAeadKeyFormat proto", e2);
        }
    }
}
