package j.c.b.a.z;

import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.InvalidProtocolBufferException;
import j.c.b.a.z.AesCtrParams;
import j.c.b.a.z.l;
import j.c.e.CodedInputStream;
import j.c.e.ExtensionRegistryLite;
import j.c.e.GeneratedMessageLite;
import j.c.e.MessageLite;
import j.c.e.Parser;
import java.io.IOException;

public final class AesCtrKeyFormat extends GeneratedMessageLite<l, l.b> implements m {
    public static final AesCtrKeyFormat g;
    public static volatile Parser<l> h;

    /* renamed from: e  reason: collision with root package name */
    public AesCtrParams f2405e;

    /* renamed from: f  reason: collision with root package name */
    public int f2406f;

    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        /* JADX WARNING: Can't wrap try/catch for region: R(20:0|(2:1|2)|3|5|6|7|8|9|11|12|13|14|15|(2:17|18)|19|21|22|23|24|26) */
        /* JADX WARNING: Can't wrap try/catch for region: R(22:0|1|2|3|5|6|7|8|9|11|12|13|14|15|17|18|19|21|22|23|24|26) */
        /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0025 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0039 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0017 */
        static {
            /*
                j.c.e.GeneratedMessageLite$j[] r0 = j.c.e.GeneratedMessageLite.j.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                j.c.b.a.z.AesCtrKeyFormat.a.a = r0
                r1 = 1
                r2 = 4
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.NEW_MUTABLE_INSTANCE     // Catch:{ NoSuchFieldError -> 0x000f }
                r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x000f }
            L_0x000f:
                r0 = 2
                int[] r3 = j.c.b.a.z.AesCtrKeyFormat.a.a     // Catch:{ NoSuchFieldError -> 0x0017 }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.IS_INITIALIZED     // Catch:{ NoSuchFieldError -> 0x0017 }
                r4 = 0
                r3[r4] = r0     // Catch:{ NoSuchFieldError -> 0x0017 }
            L_0x0017:
                int[] r3 = j.c.b.a.z.AesCtrKeyFormat.a.a     // Catch:{ NoSuchFieldError -> 0x001e }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.MAKE_IMMUTABLE     // Catch:{ NoSuchFieldError -> 0x001e }
                r4 = 3
                r3[r4] = r4     // Catch:{ NoSuchFieldError -> 0x001e }
            L_0x001e:
                r3 = 5
                int[] r4 = j.c.b.a.z.AesCtrKeyFormat.a.a     // Catch:{ NoSuchFieldError -> 0x0025 }
                j.c.e.GeneratedMessageLite$j r5 = j.c.e.GeneratedMessageLite.j.NEW_BUILDER     // Catch:{ NoSuchFieldError -> 0x0025 }
                r4[r3] = r2     // Catch:{ NoSuchFieldError -> 0x0025 }
            L_0x0025:
                int[] r2 = j.c.b.a.z.AesCtrKeyFormat.a.a     // Catch:{ NoSuchFieldError -> 0x002b }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.VISIT     // Catch:{ NoSuchFieldError -> 0x002b }
                r2[r1] = r3     // Catch:{ NoSuchFieldError -> 0x002b }
            L_0x002b:
                r1 = 6
                int[] r2 = j.c.b.a.z.AesCtrKeyFormat.a.a     // Catch:{ NoSuchFieldError -> 0x0032 }
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.MERGE_FROM_STREAM     // Catch:{ NoSuchFieldError -> 0x0032 }
                r2[r0] = r1     // Catch:{ NoSuchFieldError -> 0x0032 }
            L_0x0032:
                r0 = 7
                int[] r2 = j.c.b.a.z.AesCtrKeyFormat.a.a     // Catch:{ NoSuchFieldError -> 0x0039 }
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.GET_DEFAULT_INSTANCE     // Catch:{ NoSuchFieldError -> 0x0039 }
                r2[r1] = r0     // Catch:{ NoSuchFieldError -> 0x0039 }
            L_0x0039:
                int[] r1 = j.c.b.a.z.AesCtrKeyFormat.a.a     // Catch:{ NoSuchFieldError -> 0x0041 }
                j.c.e.GeneratedMessageLite$j r2 = j.c.e.GeneratedMessageLite.j.GET_PARSER     // Catch:{ NoSuchFieldError -> 0x0041 }
                r2 = 8
                r1[r0] = r2     // Catch:{ NoSuchFieldError -> 0x0041 }
            L_0x0041:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: j.c.b.a.z.AesCtrKeyFormat.a.<clinit>():void");
        }
    }

    static {
        AesCtrKeyFormat aesCtrKeyFormat = new AesCtrKeyFormat();
        g = aesCtrKeyFormat;
        super.f();
    }

    public static /* synthetic */ void a(AesCtrKeyFormat aesCtrKeyFormat, AesCtrParams aesCtrParams) {
        if (aesCtrParams != null) {
            aesCtrKeyFormat.f2405e = aesCtrParams;
            return;
        }
        throw null;
    }

    /* JADX WARN: Type inference failed for: r2v1, types: [j.c.b.a.z.AesCtrParams, j.c.e.MessageLite] */
    public int c() {
        int i2 = super.d;
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        if (this.f2405e != null) {
            i3 = 0 + CodedOutputStream.b(1, (MessageLite) g());
        }
        int i4 = this.f2406f;
        if (i4 != 0) {
            i3 += CodedOutputStream.d(2, i4);
        }
        super.d = i3;
        return i3;
    }

    public AesCtrParams g() {
        AesCtrParams aesCtrParams = this.f2405e;
        return aesCtrParams == null ? AesCtrParams.f2407f : aesCtrParams;
    }

    public static final class b extends GeneratedMessageLite.b<l, l.b> implements m {
        public b() {
            super(AesCtrKeyFormat.g);
        }

        public /* synthetic */ b(a aVar) {
            super(AesCtrKeyFormat.g);
        }
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [j.c.b.a.z.AesCtrParams, j.c.e.MessageLite] */
    public void a(CodedOutputStream codedOutputStream) {
        if (this.f2405e != null) {
            codedOutputStream.a(1, (MessageLite) g());
        }
        int i2 = this.f2406f;
        if (i2 != 0) {
            codedOutputStream.b(2, i2);
        }
    }

    /* JADX WARN: Type inference failed for: r5v3, types: [j.c.b.a.z.AesCtrParams, j.c.e.MessageLite] */
    /* JADX WARN: Type inference failed for: r2v1, types: [j.c.b.a.z.AesCtrParams, j.c.e.MessageLite] */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final Object a(GeneratedMessageLite.j jVar, Object obj, Object obj2) {
        boolean z = false;
        switch (jVar.ordinal()) {
            case 0:
                return g;
            case 1:
                GeneratedMessageLite.k kVar = (GeneratedMessageLite.k) obj;
                AesCtrKeyFormat aesCtrKeyFormat = (AesCtrKeyFormat) obj2;
                this.f2405e = (AesCtrParams) kVar.a((MessageLite) this.f2405e, (MessageLite) aesCtrKeyFormat.f2405e);
                boolean z2 = this.f2406f != 0;
                int i2 = this.f2406f;
                if (aesCtrKeyFormat.f2406f != 0) {
                    z = true;
                }
                this.f2406f = kVar.a(z2, i2, z, aesCtrKeyFormat.f2406f);
                return this;
            case 2:
                CodedInputStream codedInputStream = (CodedInputStream) obj;
                ExtensionRegistryLite extensionRegistryLite = (ExtensionRegistryLite) obj2;
                while (!z) {
                    try {
                        int g2 = codedInputStream.g();
                        if (g2 != 0) {
                            if (g2 == 10) {
                                AesCtrParams.b bVar = this.f2405e != null ? (AesCtrParams.b) this.f2405e.e() : null;
                                AesCtrParams aesCtrParams = (AesCtrParams) codedInputStream.a(AesCtrParams.f2407f.i(), extensionRegistryLite);
                                this.f2405e = aesCtrParams;
                                if (bVar != null) {
                                    bVar.a(super);
                                    this.f2405e = (AesCtrParams) bVar.l();
                                }
                            } else if (g2 == 16) {
                                this.f2406f = codedInputStream.d();
                            } else if (!codedInputStream.e(g2)) {
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e2) {
                        throw new RuntimeException(e2);
                    } catch (IOException e3) {
                        throw new RuntimeException(new InvalidProtocolBufferException(e3.getMessage()));
                    }
                }
                break;
            case 3:
                return null;
            case 4:
                return new AesCtrKeyFormat();
            case 5:
                return new b(null);
            case 6:
                break;
            case 7:
                if (h == null) {
                    synchronized (AesCtrKeyFormat.class) {
                        if (h == null) {
                            h = new GeneratedMessageLite.c(g);
                        }
                    }
                }
                return h;
            default:
                throw new UnsupportedOperationException();
        }
        return g;
    }
}
