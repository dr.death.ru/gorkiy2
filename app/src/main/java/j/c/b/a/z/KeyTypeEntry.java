package j.c.b.a.z;

import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.InvalidProtocolBufferException;
import j.c.b.a.z.b2;
import j.c.e.CodedInputStream;
import j.c.e.ExtensionRegistryLite;
import j.c.e.GeneratedMessageLite;
import j.c.e.Parser;
import java.io.IOException;

public final class KeyTypeEntry extends GeneratedMessageLite<b2, b2.b> implements c2 {

    /* renamed from: j  reason: collision with root package name */
    public static final KeyTypeEntry f2483j;

    /* renamed from: k  reason: collision with root package name */
    public static volatile Parser<b2> f2484k;

    /* renamed from: e  reason: collision with root package name */
    public String f2485e = "";

    /* renamed from: f  reason: collision with root package name */
    public String f2486f = "";
    public int g;
    public boolean h;

    /* renamed from: i  reason: collision with root package name */
    public String f2487i = "";

    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        /* JADX WARNING: Can't wrap try/catch for region: R(20:0|(2:1|2)|3|5|6|7|8|9|11|12|13|14|15|(2:17|18)|19|21|22|23|24|26) */
        /* JADX WARNING: Can't wrap try/catch for region: R(22:0|1|2|3|5|6|7|8|9|11|12|13|14|15|17|18|19|21|22|23|24|26) */
        /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0025 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0039 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0017 */
        static {
            /*
                j.c.e.GeneratedMessageLite$j[] r0 = j.c.e.GeneratedMessageLite.j.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                j.c.b.a.z.KeyTypeEntry.a.a = r0
                r1 = 1
                r2 = 4
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.NEW_MUTABLE_INSTANCE     // Catch:{ NoSuchFieldError -> 0x000f }
                r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x000f }
            L_0x000f:
                r0 = 2
                int[] r3 = j.c.b.a.z.KeyTypeEntry.a.a     // Catch:{ NoSuchFieldError -> 0x0017 }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.IS_INITIALIZED     // Catch:{ NoSuchFieldError -> 0x0017 }
                r4 = 0
                r3[r4] = r0     // Catch:{ NoSuchFieldError -> 0x0017 }
            L_0x0017:
                int[] r3 = j.c.b.a.z.KeyTypeEntry.a.a     // Catch:{ NoSuchFieldError -> 0x001e }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.MAKE_IMMUTABLE     // Catch:{ NoSuchFieldError -> 0x001e }
                r4 = 3
                r3[r4] = r4     // Catch:{ NoSuchFieldError -> 0x001e }
            L_0x001e:
                r3 = 5
                int[] r4 = j.c.b.a.z.KeyTypeEntry.a.a     // Catch:{ NoSuchFieldError -> 0x0025 }
                j.c.e.GeneratedMessageLite$j r5 = j.c.e.GeneratedMessageLite.j.NEW_BUILDER     // Catch:{ NoSuchFieldError -> 0x0025 }
                r4[r3] = r2     // Catch:{ NoSuchFieldError -> 0x0025 }
            L_0x0025:
                int[] r2 = j.c.b.a.z.KeyTypeEntry.a.a     // Catch:{ NoSuchFieldError -> 0x002b }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.VISIT     // Catch:{ NoSuchFieldError -> 0x002b }
                r2[r1] = r3     // Catch:{ NoSuchFieldError -> 0x002b }
            L_0x002b:
                r1 = 6
                int[] r2 = j.c.b.a.z.KeyTypeEntry.a.a     // Catch:{ NoSuchFieldError -> 0x0032 }
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.MERGE_FROM_STREAM     // Catch:{ NoSuchFieldError -> 0x0032 }
                r2[r0] = r1     // Catch:{ NoSuchFieldError -> 0x0032 }
            L_0x0032:
                r0 = 7
                int[] r2 = j.c.b.a.z.KeyTypeEntry.a.a     // Catch:{ NoSuchFieldError -> 0x0039 }
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.GET_DEFAULT_INSTANCE     // Catch:{ NoSuchFieldError -> 0x0039 }
                r2[r1] = r0     // Catch:{ NoSuchFieldError -> 0x0039 }
            L_0x0039:
                int[] r1 = j.c.b.a.z.KeyTypeEntry.a.a     // Catch:{ NoSuchFieldError -> 0x0041 }
                j.c.e.GeneratedMessageLite$j r2 = j.c.e.GeneratedMessageLite.j.GET_PARSER     // Catch:{ NoSuchFieldError -> 0x0041 }
                r2 = 8
                r1[r0] = r2     // Catch:{ NoSuchFieldError -> 0x0041 }
            L_0x0041:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: j.c.b.a.z.KeyTypeEntry.a.<clinit>():void");
        }
    }

    static {
        KeyTypeEntry keyTypeEntry = new KeyTypeEntry();
        f2483j = keyTypeEntry;
        super.f();
    }

    public static /* synthetic */ void a(KeyTypeEntry keyTypeEntry, String str) {
        if (str != null) {
            keyTypeEntry.f2485e = str;
            return;
        }
        throw null;
    }

    public static /* synthetic */ void b(KeyTypeEntry keyTypeEntry, String str) {
        if (str != null) {
            keyTypeEntry.f2487i = str;
            return;
        }
        throw null;
    }

    public static /* synthetic */ void c(KeyTypeEntry keyTypeEntry, String str) {
        if (str != null) {
            keyTypeEntry.f2486f = str;
            return;
        }
        throw null;
    }

    public static final class b extends GeneratedMessageLite.b<b2, b2.b> implements c2 {
        public b() {
            super(KeyTypeEntry.f2483j);
        }

        public /* synthetic */ b(a aVar) {
            super(KeyTypeEntry.f2483j);
        }
    }

    public void a(CodedOutputStream codedOutputStream) {
        if (!this.f2485e.isEmpty()) {
            codedOutputStream.a(1, this.f2485e);
        }
        if (!this.f2486f.isEmpty()) {
            codedOutputStream.a(2, this.f2486f);
        }
        int i2 = this.g;
        if (i2 != 0) {
            codedOutputStream.b(3, i2);
        }
        boolean z = this.h;
        if (z) {
            CodedOutputStream.b bVar = (CodedOutputStream.b) codedOutputStream;
            bVar.a(32);
            byte b2 = z ? (byte) 1 : 0;
            try {
                byte[] bArr = bVar.d;
                int i3 = bVar.f561f;
                bVar.f561f = i3 + 1;
                bArr[i3] = b2;
            } catch (IndexOutOfBoundsException e2) {
                throw new CodedOutputStream.OutOfSpaceException(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(bVar.f561f), Integer.valueOf(bVar.f560e), 1), e2);
            }
        }
        if (!this.f2487i.isEmpty()) {
            codedOutputStream.a(5, this.f2487i);
        }
    }

    public int c() {
        int i2 = super.d;
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        if (!this.f2485e.isEmpty()) {
            i3 = 0 + CodedOutputStream.b(1, this.f2485e);
        }
        if (!this.f2486f.isEmpty()) {
            i3 += CodedOutputStream.b(2, this.f2486f);
        }
        int i4 = this.g;
        if (i4 != 0) {
            i3 += CodedOutputStream.d(3, i4);
        }
        if (this.h) {
            i3 += CodedOutputStream.c(4) + 1;
        }
        if (!this.f2487i.isEmpty()) {
            i3 += CodedOutputStream.b(5, this.f2487i);
        }
        super.d = i3;
        return i3;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final Object a(GeneratedMessageLite.j jVar, Object obj, Object obj2) {
        boolean z = false;
        switch (jVar.ordinal()) {
            case 0:
                return f2483j;
            case 1:
                GeneratedMessageLite.k kVar = (GeneratedMessageLite.k) obj;
                KeyTypeEntry keyTypeEntry = (KeyTypeEntry) obj2;
                this.f2485e = kVar.a(!this.f2485e.isEmpty(), this.f2485e, !keyTypeEntry.f2485e.isEmpty(), keyTypeEntry.f2485e);
                this.f2486f = kVar.a(!this.f2486f.isEmpty(), this.f2486f, !keyTypeEntry.f2486f.isEmpty(), keyTypeEntry.f2486f);
                boolean z2 = this.g != 0;
                int i2 = this.g;
                if (keyTypeEntry.g != 0) {
                    z = true;
                }
                this.g = kVar.a(z2, i2, z, keyTypeEntry.g);
                boolean z3 = this.h;
                boolean z4 = keyTypeEntry.h;
                this.h = kVar.a(z3, z3, z4, z4);
                this.f2487i = kVar.a(!this.f2487i.isEmpty(), this.f2487i, true ^ keyTypeEntry.f2487i.isEmpty(), keyTypeEntry.f2487i);
                return this;
            case 2:
                CodedInputStream codedInputStream = (CodedInputStream) obj;
                ExtensionRegistryLite extensionRegistryLite = (ExtensionRegistryLite) obj2;
                while (!z) {
                    try {
                        int g2 = codedInputStream.g();
                        if (g2 != 0) {
                            if (g2 == 10) {
                                this.f2485e = codedInputStream.f();
                            } else if (g2 == 18) {
                                this.f2486f = codedInputStream.f();
                            } else if (g2 == 24) {
                                this.g = codedInputStream.d();
                            } else if (g2 == 32) {
                                this.h = codedInputStream.a();
                            } else if (g2 == 42) {
                                this.f2487i = codedInputStream.f();
                            } else if (!codedInputStream.e(g2)) {
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e2) {
                        throw new RuntimeException(e2);
                    } catch (IOException e3) {
                        throw new RuntimeException(new InvalidProtocolBufferException(e3.getMessage()));
                    }
                }
                break;
            case 3:
                return null;
            case 4:
                return new KeyTypeEntry();
            case 5:
                return new b(null);
            case 6:
                break;
            case 7:
                if (f2484k == null) {
                    synchronized (KeyTypeEntry.class) {
                        if (f2484k == null) {
                            f2484k = new GeneratedMessageLite.c(f2483j);
                        }
                    }
                }
                return f2484k;
            default:
                throw new UnsupportedOperationException();
        }
        return f2483j;
    }
}
