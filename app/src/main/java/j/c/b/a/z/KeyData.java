package j.c.b.a.z;

import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.InvalidProtocolBufferException;
import j.c.b.a.z.w1;
import j.c.e.ByteString;
import j.c.e.CodedInputStream;
import j.c.e.ExtensionRegistryLite;
import j.c.e.GeneratedMessageLite;
import j.c.e.Internal;
import j.c.e.Parser;
import j.c.e.l;
import java.io.IOException;

public final class KeyData extends GeneratedMessageLite<w1, w1.b> implements x1 {
    public static final KeyData h;

    /* renamed from: i  reason: collision with root package name */
    public static volatile Parser<w1> f2477i;

    /* renamed from: e  reason: collision with root package name */
    public String f2478e = "";

    /* renamed from: f  reason: collision with root package name */
    public ByteString f2479f = ByteString.c;
    public int g;

    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        /* JADX WARNING: Can't wrap try/catch for region: R(20:0|(2:1|2)|3|5|6|7|8|9|11|12|13|14|15|(2:17|18)|19|21|22|23|24|26) */
        /* JADX WARNING: Can't wrap try/catch for region: R(22:0|1|2|3|5|6|7|8|9|11|12|13|14|15|17|18|19|21|22|23|24|26) */
        /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0025 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0039 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0017 */
        static {
            /*
                j.c.e.GeneratedMessageLite$j[] r0 = j.c.e.GeneratedMessageLite.j.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                j.c.b.a.z.KeyData.a.a = r0
                r1 = 1
                r2 = 4
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.NEW_MUTABLE_INSTANCE     // Catch:{ NoSuchFieldError -> 0x000f }
                r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x000f }
            L_0x000f:
                r0 = 2
                int[] r3 = j.c.b.a.z.KeyData.a.a     // Catch:{ NoSuchFieldError -> 0x0017 }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.IS_INITIALIZED     // Catch:{ NoSuchFieldError -> 0x0017 }
                r4 = 0
                r3[r4] = r0     // Catch:{ NoSuchFieldError -> 0x0017 }
            L_0x0017:
                int[] r3 = j.c.b.a.z.KeyData.a.a     // Catch:{ NoSuchFieldError -> 0x001e }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.MAKE_IMMUTABLE     // Catch:{ NoSuchFieldError -> 0x001e }
                r4 = 3
                r3[r4] = r4     // Catch:{ NoSuchFieldError -> 0x001e }
            L_0x001e:
                r3 = 5
                int[] r4 = j.c.b.a.z.KeyData.a.a     // Catch:{ NoSuchFieldError -> 0x0025 }
                j.c.e.GeneratedMessageLite$j r5 = j.c.e.GeneratedMessageLite.j.NEW_BUILDER     // Catch:{ NoSuchFieldError -> 0x0025 }
                r4[r3] = r2     // Catch:{ NoSuchFieldError -> 0x0025 }
            L_0x0025:
                int[] r2 = j.c.b.a.z.KeyData.a.a     // Catch:{ NoSuchFieldError -> 0x002b }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.VISIT     // Catch:{ NoSuchFieldError -> 0x002b }
                r2[r1] = r3     // Catch:{ NoSuchFieldError -> 0x002b }
            L_0x002b:
                r1 = 6
                int[] r2 = j.c.b.a.z.KeyData.a.a     // Catch:{ NoSuchFieldError -> 0x0032 }
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.MERGE_FROM_STREAM     // Catch:{ NoSuchFieldError -> 0x0032 }
                r2[r0] = r1     // Catch:{ NoSuchFieldError -> 0x0032 }
            L_0x0032:
                r0 = 7
                int[] r2 = j.c.b.a.z.KeyData.a.a     // Catch:{ NoSuchFieldError -> 0x0039 }
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.GET_DEFAULT_INSTANCE     // Catch:{ NoSuchFieldError -> 0x0039 }
                r2[r1] = r0     // Catch:{ NoSuchFieldError -> 0x0039 }
            L_0x0039:
                int[] r1 = j.c.b.a.z.KeyData.a.a     // Catch:{ NoSuchFieldError -> 0x0041 }
                j.c.e.GeneratedMessageLite$j r2 = j.c.e.GeneratedMessageLite.j.GET_PARSER     // Catch:{ NoSuchFieldError -> 0x0041 }
                r2 = 8
                r1[r0] = r2     // Catch:{ NoSuchFieldError -> 0x0041 }
            L_0x0041:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: j.c.b.a.z.KeyData.a.<clinit>():void");
        }
    }

    public enum c implements l.a {
        UNKNOWN_KEYMATERIAL(0),
        SYMMETRIC(1),
        ASYMMETRIC_PRIVATE(2),
        ASYMMETRIC_PUBLIC(3),
        REMOTE(4),
        UNRECOGNIZED(-1);
        
        public static final int ASYMMETRIC_PRIVATE_VALUE = 2;
        public static final int ASYMMETRIC_PUBLIC_VALUE = 3;
        public static final int REMOTE_VALUE = 4;
        public static final int SYMMETRIC_VALUE = 1;
        public static final int UNKNOWN_KEYMATERIAL_VALUE = 0;
        public static final Internal.b<w1.c> internalValueMap = new a();
        public final int value;

        public class a implements Internal.b<w1.c> {
        }

        /* access modifiers changed from: public */
        c(int i2) {
            this.value = i2;
        }

        public static c a(int i2) {
            if (i2 == 0) {
                return UNKNOWN_KEYMATERIAL;
            }
            if (i2 == 1) {
                return SYMMETRIC;
            }
            if (i2 == 2) {
                return ASYMMETRIC_PRIVATE;
            }
            if (i2 == 3) {
                return ASYMMETRIC_PUBLIC;
            }
            if (i2 != 4) {
                return null;
            }
            return REMOTE;
        }
    }

    static {
        KeyData keyData = new KeyData();
        h = keyData;
        super.f();
    }

    public static /* synthetic */ void a(KeyData keyData, c cVar) {
        if (keyData == null) {
            throw null;
        } else if (cVar != null) {
            keyData.g = cVar.value;
        } else {
            throw null;
        }
    }

    public static b g() {
        return (b) h.e();
    }

    public int c() {
        int i2 = super.d;
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        if (!this.f2478e.isEmpty()) {
            i3 = 0 + CodedOutputStream.b(1, this.f2478e);
        }
        if (!this.f2479f.isEmpty()) {
            i3 += CodedOutputStream.b(2, this.f2479f);
        }
        int i4 = this.g;
        if (i4 != c.UNKNOWN_KEYMATERIAL.value) {
            i3 += CodedOutputStream.c(3, i4);
        }
        super.d = i3;
        return i3;
    }

    public static final class b extends GeneratedMessageLite.b<w1, w1.b> implements x1 {
        public b() {
            super(KeyData.h);
        }

        public b a(c cVar) {
            m();
            KeyData.a((KeyData) super.c, cVar);
            return this;
        }

        public /* synthetic */ b(a aVar) {
            super(KeyData.h);
        }
    }

    public static /* synthetic */ void a(KeyData keyData, String str) {
        if (str != null) {
            keyData.f2478e = str;
            return;
        }
        throw null;
    }

    public static /* synthetic */ void a(KeyData keyData, ByteString byteString) {
        if (byteString != null) {
            keyData.f2479f = byteString;
            return;
        }
        throw null;
    }

    public void a(CodedOutputStream codedOutputStream) {
        if (!this.f2478e.isEmpty()) {
            codedOutputStream.a(1, this.f2478e);
        }
        if (!this.f2479f.isEmpty()) {
            codedOutputStream.a(2, this.f2479f);
        }
        int i2 = this.g;
        if (i2 != c.UNKNOWN_KEYMATERIAL.value) {
            codedOutputStream.a(3, i2);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final Object a(GeneratedMessageLite.j jVar, Object obj, Object obj2) {
        boolean z = false;
        switch (jVar.ordinal()) {
            case 0:
                return h;
            case 1:
                GeneratedMessageLite.k kVar = (GeneratedMessageLite.k) obj;
                KeyData keyData = (KeyData) obj2;
                this.f2478e = kVar.a(!this.f2478e.isEmpty(), this.f2478e, !keyData.f2478e.isEmpty(), keyData.f2478e);
                this.f2479f = kVar.a(this.f2479f != ByteString.c, this.f2479f, keyData.f2479f != ByteString.c, keyData.f2479f);
                boolean z2 = this.g != 0;
                int i2 = this.g;
                if (keyData.g != 0) {
                    z = true;
                }
                this.g = kVar.a(z2, i2, z, keyData.g);
                return this;
            case 2:
                CodedInputStream codedInputStream = (CodedInputStream) obj;
                ExtensionRegistryLite extensionRegistryLite = (ExtensionRegistryLite) obj2;
                while (!z) {
                    try {
                        int g2 = codedInputStream.g();
                        if (g2 != 0) {
                            if (g2 == 10) {
                                this.f2478e = codedInputStream.f();
                            } else if (g2 == 18) {
                                this.f2479f = codedInputStream.b();
                            } else if (g2 == 24) {
                                this.g = codedInputStream.d();
                            } else if (!codedInputStream.e(g2)) {
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e2) {
                        throw new RuntimeException(e2);
                    } catch (IOException e3) {
                        throw new RuntimeException(new InvalidProtocolBufferException(e3.getMessage()));
                    }
                }
                break;
            case 3:
                return null;
            case 4:
                return new KeyData();
            case 5:
                return new b(null);
            case 6:
                break;
            case 7:
                if (f2477i == null) {
                    synchronized (KeyData.class) {
                        if (f2477i == null) {
                            f2477i = new GeneratedMessageLite.c(h);
                        }
                    }
                }
                return f2477i;
            default:
                throw new UnsupportedOperationException();
        }
        return h;
    }
}
