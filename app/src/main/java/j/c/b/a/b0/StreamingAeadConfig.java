package j.c.b.a.b0;

import j.c.a.a.c.n.c;
import j.c.b.a.Registry;
import j.c.b.a.z.RegistryConfig;
import java.security.GeneralSecurityException;

public final class StreamingAeadConfig {
    @Deprecated
    public static final RegistryConfig a;
    public static final RegistryConfig b;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.String, java.lang.String, java.lang.String, int, boolean):j.c.b.a.z.KeyTypeEntry
     arg types: [java.lang.String, java.lang.String, java.lang.String, int, int]
     candidates:
      j.c.a.a.c.n.c.a(int, byte[], int, int, j.c.a.a.f.e.s2):int
      j.c.a.a.c.n.c.a(j.c.a.a.f.e.t5, byte[], int, int, j.c.a.a.f.e.s2):int
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String):java.security.spec.ECParameterSpec
      j.c.a.a.c.n.c.a(byte, byte, byte, char[], int):void
      j.c.a.a.c.n.c.a(byte[], int, byte[], int, int):byte[]
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String, java.lang.String, int, boolean):j.c.b.a.z.KeyTypeEntry */
    static {
        RegistryConfig.b g = RegistryConfig.g();
        g.a(c.a("TinkStreamingAead", "StreamingAead", "AesCtrHmacStreamingKey", 0, true));
        g.a(c.a("TinkStreamingAead", "StreamingAead", "AesGcmHkdfStreamingKey", 0, true));
        g.m();
        RegistryConfig.a((RegistryConfig) g.c, "TINK_STREAMINGAEAD_1_1_0");
        a = (RegistryConfig) g.k();
        RegistryConfig.b g2 = RegistryConfig.g();
        g2.a(c.a("TinkStreamingAead", "StreamingAead", "AesCtrHmacStreamingKey", 0, true));
        g2.a(c.a("TinkStreamingAead", "StreamingAead", "AesGcmHkdfStreamingKey", 0, true));
        g2.m();
        RegistryConfig.a((RegistryConfig) g2.c, "TINK_STREAMINGAEAD");
        b = (RegistryConfig) g2.k();
        try {
            Registry.a("TinkStreamingAead", new StreamingAeadCatalogue());
            c.a(b);
        } catch (GeneralSecurityException e2) {
            throw new ExceptionInInitializerError(e2);
        }
    }
}
