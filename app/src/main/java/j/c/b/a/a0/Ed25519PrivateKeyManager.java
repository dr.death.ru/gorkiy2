package j.c.b.a.a0;

import com.google.protobuf.InvalidProtocolBufferException;
import j.c.b.a.PrivateKeyManager;
import j.c.b.a.PublicKeySign;
import j.c.b.a.c0.Ed25519;
import j.c.b.a.c0.Ed25519Sign;
import j.c.b.a.c0.Random;
import j.c.b.a.c0.Validators;
import j.c.b.a.p;
import j.c.b.a.z.Ed25519PrivateKey;
import j.c.b.a.z.Ed25519PublicKey;
import j.c.b.a.z.KeyData;
import j.c.e.ByteString;
import j.c.e.GeneratedMessageLite;
import j.c.e.MessageLite;
import j.c.e.f;
import java.security.GeneralSecurityException;
import java.util.Arrays;

public class Ed25519PrivateKeyManager implements PrivateKeyManager<p> {
    public String a() {
        return "type.googleapis.com/google.crypto.tink.Ed25519PrivateKey";
    }

    public int b() {
        return 0;
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [j.c.b.a.z.Ed25519PrivateKey, j.c.e.MessageLite] */
    public MessageLite b(MessageLite messageLite) {
        return c();
    }

    public final Ed25519PrivateKey c() {
        byte[] a = Random.a(32);
        byte[] b = Ed25519.b(Ed25519.a(a));
        Ed25519PublicKey.b bVar = (Ed25519PublicKey.b) Ed25519PublicKey.g.e();
        bVar.m();
        ((Ed25519PublicKey) bVar.c).f2466e = 0;
        ByteString a2 = ByteString.a(Arrays.copyOf(b, b.length));
        bVar.m();
        Ed25519PublicKey.a((Ed25519PublicKey) bVar.c, a2);
        Ed25519PrivateKey.b bVar2 = (Ed25519PrivateKey.b) Ed25519PrivateKey.h.e();
        bVar2.m();
        ((Ed25519PrivateKey) bVar2.c).f2464e = 0;
        ByteString a3 = ByteString.a(Arrays.copyOf(a, a.length));
        bVar2.m();
        Ed25519PrivateKey.a((Ed25519PrivateKey) bVar2.c, a3);
        bVar2.m();
        Ed25519PrivateKey.a((Ed25519PrivateKey) bVar2.c, (Ed25519PublicKey) bVar.k());
        return (Ed25519PrivateKey) bVar2.k();
    }

    public PublicKeySign a(MessageLite messageLite) {
        if (messageLite instanceof Ed25519PrivateKey) {
            Ed25519PrivateKey ed25519PrivateKey = (Ed25519PrivateKey) messageLite;
            Validators.a(ed25519PrivateKey.f2464e, 0);
            if (ed25519PrivateKey.f2465f.size() == 32) {
                return new Ed25519Sign(ed25519PrivateKey.f2465f.d());
            }
            throw new GeneralSecurityException("invalid Ed25519 private key: incorrect key length");
        }
        throw new GeneralSecurityException("expected Ed25519PrivateKey proto");
    }

    public KeyData b(ByteString byteString) {
        Ed25519PrivateKey c = c();
        KeyData.b g = KeyData.g();
        g.m();
        KeyData.a((KeyData) g.c, "type.googleapis.com/google.crypto.tink.Ed25519PrivateKey");
        ByteString a = c.a();
        g.m();
        KeyData.a((KeyData) g.c, a);
        g.a(KeyData.c.ASYMMETRIC_PRIVATE);
        return (KeyData) g.k();
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [j.c.b.a.z.Ed25519PrivateKey, j.c.e.MessageLite] */
    public MessageLite a(ByteString byteString) {
        return c();
    }

    public boolean a(String str) {
        return "type.googleapis.com/google.crypto.tink.Ed25519PrivateKey".equals(str);
    }

    /* JADX WARN: Type inference failed for: r3v3, types: [j.c.b.a.z.Ed25519PrivateKey, j.c.e.MessageLite] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
     arg types: [j.c.b.a.z.Ed25519PrivateKey, j.c.e.ByteString]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T */
    public Object c(ByteString byteString) {
        try {
            return a((MessageLite) ((Ed25519PrivateKey) GeneratedMessageLite.a((GeneratedMessageLite) Ed25519PrivateKey.h, (f) byteString)));
        } catch (InvalidProtocolBufferException e2) {
            throw new GeneralSecurityException("invalid Ed25519 private key", e2);
        }
    }
}
