package j.c.b.a.x.a;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import j.c.b.a.KeysetWriter;

public final class SharedPrefKeysetWriter implements KeysetWriter {
    public final SharedPreferences.Editor a;
    public final String b;

    public SharedPrefKeysetWriter(Context context, String str, String str2) {
        if (str != null) {
            this.b = str;
            Context applicationContext = context.getApplicationContext();
            if (str2 == null) {
                this.a = PreferenceManager.getDefaultSharedPreferences(applicationContext).edit();
            } else {
                this.a = applicationContext.getSharedPreferences(str2, 0).edit();
            }
        } else {
            throw new IllegalArgumentException("keysetName cannot be null");
        }
    }
}
