package j.c.a.b.w;

import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.widget.FrameLayout;
import j.c.a.b.w.CircularRevealWidget;

public class CircularRevealFrameLayout extends FrameLayout implements CircularRevealWidget {
    public void a() {
        throw null;
    }

    public void b() {
        throw null;
    }

    @SuppressLint({"MissingSuperCall"})
    public void draw(Canvas canvas) {
        super.draw(canvas);
    }

    public Drawable getCircularRevealOverlayDrawable() {
        throw null;
    }

    public int getCircularRevealScrimColor() {
        throw null;
    }

    public CircularRevealWidget.e getRevealInfo() {
        throw null;
    }

    public boolean isOpaque() {
        return super.isOpaque();
    }

    public void setCircularRevealOverlayDrawable(Drawable drawable) {
        throw null;
    }

    public void setCircularRevealScrimColor(int i2) {
        throw null;
    }

    public void setRevealInfo(CircularRevealWidget.e eVar) {
        throw null;
    }
}
