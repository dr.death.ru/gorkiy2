package j.c.a.b.n;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

public class ViewOffsetBehavior<V extends View> extends CoordinatorLayout.c<V> {
    public ViewOffsetHelper a;
    public int b = 0;
    public int c = 0;

    public ViewOffsetBehavior() {
    }

    public boolean a(CoordinatorLayout coordinatorLayout, View view, int i2) {
        b(coordinatorLayout, view, i2);
        if (this.a == null) {
            this.a = new ViewOffsetHelper(view);
        }
        ViewOffsetHelper viewOffsetHelper = this.a;
        viewOffsetHelper.b = viewOffsetHelper.a.getTop();
        viewOffsetHelper.c = viewOffsetHelper.a.getLeft();
        this.a.a();
        int i3 = this.b;
        if (i3 != 0) {
            ViewOffsetHelper viewOffsetHelper2 = this.a;
            if (viewOffsetHelper2.f2304f && viewOffsetHelper2.d != i3) {
                viewOffsetHelper2.d = i3;
                viewOffsetHelper2.a();
            }
            this.b = 0;
        }
        int i4 = this.c;
        if (i4 == 0) {
            return true;
        }
        ViewOffsetHelper viewOffsetHelper3 = this.a;
        if (viewOffsetHelper3.g && viewOffsetHelper3.f2303e != i4) {
            viewOffsetHelper3.f2303e = i4;
            viewOffsetHelper3.a();
        }
        this.c = 0;
        return true;
    }

    public void b(CoordinatorLayout coordinatorLayout, V v, int i2) {
        coordinatorLayout.b(v, i2);
    }

    public int b() {
        ViewOffsetHelper viewOffsetHelper = this.a;
        if (viewOffsetHelper != null) {
            return viewOffsetHelper.d;
        }
        return 0;
    }

    public ViewOffsetBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public boolean a(int i2) {
        ViewOffsetHelper viewOffsetHelper = this.a;
        if (viewOffsetHelper == null) {
            this.b = i2;
            return false;
        } else if (!viewOffsetHelper.f2304f || viewOffsetHelper.d == i2) {
            return false;
        } else {
            viewOffsetHelper.d = i2;
            viewOffsetHelper.a();
            return true;
        }
    }
}
