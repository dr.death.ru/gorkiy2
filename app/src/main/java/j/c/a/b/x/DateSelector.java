package j.c.a.b.x;

import android.os.Parcelable;
import i.h.k.Pair;
import java.util.Collection;

public interface DateSelector<S> extends Parcelable {
    void f(long j2);

    S i();

    Collection<Pair<Long, Long>> l();

    Collection<Long> p();
}
