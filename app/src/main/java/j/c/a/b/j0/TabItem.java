package j.c.a.b.j0;

import android.graphics.drawable.Drawable;
import android.view.View;

public class TabItem extends View {
    public final CharSequence b;
    public final Drawable c;
    public final int d;
}
