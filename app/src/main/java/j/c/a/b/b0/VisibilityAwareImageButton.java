package j.c.a.b.b0;

import android.annotation.SuppressLint;
import android.widget.ImageButton;

@SuppressLint({"AppCompatCustomView"})
public class VisibilityAwareImageButton extends ImageButton {
    public int b;

    public final void a(int i2, boolean z) {
        super.setVisibility(i2);
        if (z) {
            this.b = i2;
        }
    }

    public final int getUserSetVisibility() {
        return this.b;
    }

    public void setVisibility(int i2) {
        super.setVisibility(i2);
        this.b = i2;
    }
}
