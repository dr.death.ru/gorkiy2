package j.c.a.b.r;

import android.animation.ValueAnimator;
import j.c.a.b.g0.MaterialShapeDrawable;

public class BottomSheetBehavior implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ com.google.android.material.bottomsheet.BottomSheetBehavior a;

    public BottomSheetBehavior(com.google.android.material.bottomsheet.BottomSheetBehavior bottomSheetBehavior) {
        this.a = bottomSheetBehavior;
    }

    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        MaterialShapeDrawable materialShapeDrawable = this.a.f412i;
        if (materialShapeDrawable != null) {
            materialShapeDrawable.b(floatValue);
        }
    }
}
