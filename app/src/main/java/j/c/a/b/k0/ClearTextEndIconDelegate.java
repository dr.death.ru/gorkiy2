package j.c.a.b.k0;

import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import com.google.android.material.textfield.TextInputLayout;
import i.b.l.a.AppCompatResources;
import j.c.a.b.e;
import j.c.a.b.j;
import j.c.a.b.m.AnimationUtils;

public class ClearTextEndIconDelegate extends EndIconDelegate {
    public final TextWatcher d = new a();

    /* renamed from: e  reason: collision with root package name */
    public final TextInputLayout.f f2264e = new b();

    /* renamed from: f  reason: collision with root package name */
    public AnimatorSet f2265f;
    public ValueAnimator g;

    public class a implements TextWatcher {
        public a() {
        }

        public void afterTextChanged(Editable editable) {
            if (ClearTextEndIconDelegate.this.a.getSuffixText() == null) {
                ClearTextEndIconDelegate.this.b(editable.length() > 0);
            }
        }

        public void beforeTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
        }

        public void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
        }
    }

    public class b implements TextInputLayout.f {

        public class a implements View.OnFocusChangeListener {
            public a() {
            }

            public void onFocusChange(View view, boolean z) {
                boolean z2 = true;
                boolean z3 = !TextUtils.isEmpty(((EditText) view).getText());
                ClearTextEndIconDelegate clearTextEndIconDelegate = ClearTextEndIconDelegate.this;
                if (!z3 || !z) {
                    z2 = false;
                }
                clearTextEndIconDelegate.b(z2);
            }
        }

        public b() {
        }

        public void a(TextInputLayout textInputLayout) {
            EditText editText = textInputLayout.getEditText();
            textInputLayout.setEndIconVisible(editText.getText().length() > 0);
            textInputLayout.setEndIconCheckable(false);
            editText.setOnFocusChangeListener(new a());
            editText.removeTextChangedListener(ClearTextEndIconDelegate.this.d);
            editText.addTextChangedListener(ClearTextEndIconDelegate.this.d);
        }
    }

    public class c implements View.OnClickListener {
        public c() {
        }

        public void onClick(View view) {
            ClearTextEndIconDelegate.this.a.getEditText().setText((CharSequence) null);
        }
    }

    public ClearTextEndIconDelegate(TextInputLayout textInputLayout) {
        super(textInputLayout);
    }

    public void a() {
        super.a.setEndIconDrawable(AppCompatResources.c(super.b, e.mtrl_ic_cancel));
        TextInputLayout textInputLayout = super.a;
        textInputLayout.setEndIconContentDescription(textInputLayout.getResources().getText(j.clear_text_end_icon_content_description));
        super.a.setEndIconOnClickListener(new c());
        super.a.a(this.f2264e);
        ValueAnimator ofFloat = ValueAnimator.ofFloat(0.8f, 1.0f);
        ofFloat.setInterpolator(AnimationUtils.d);
        ofFloat.setDuration(150L);
        ofFloat.addUpdateListener(new ClearTextEndIconDelegate3(this));
        ValueAnimator ofFloat2 = ValueAnimator.ofFloat(0.0f, 1.0f);
        ofFloat2.setInterpolator(AnimationUtils.a);
        ofFloat2.setDuration(100L);
        ofFloat2.addUpdateListener(new ClearTextEndIconDelegate2(this));
        AnimatorSet animatorSet = new AnimatorSet();
        this.f2265f = animatorSet;
        animatorSet.playTogether(ofFloat, ofFloat2);
        this.f2265f.addListener(new ClearTextEndIconDelegate0(this));
        ValueAnimator ofFloat3 = ValueAnimator.ofFloat(1.0f, 0.0f);
        ofFloat3.setInterpolator(AnimationUtils.a);
        ofFloat3.setDuration(100L);
        ofFloat3.addUpdateListener(new ClearTextEndIconDelegate2(this));
        this.g = ofFloat3;
        ofFloat3.addListener(new ClearTextEndIconDelegate1(this));
    }

    public final void b(boolean z) {
        boolean z2 = super.a.g() == z;
        if (z) {
            this.g.cancel();
            this.f2265f.start();
            if (z2) {
                this.f2265f.end();
                return;
            }
            return;
        }
        this.f2265f.cancel();
        this.g.start();
        if (z2) {
            this.g.end();
        }
    }

    public void a(boolean z) {
        if (super.a.getSuffixText() != null) {
            b(z);
        }
    }
}
