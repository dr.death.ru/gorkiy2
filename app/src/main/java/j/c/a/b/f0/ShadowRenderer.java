package j.c.a.b.f0;

import android.graphics.Paint;
import android.graphics.Path;
import i.h.f.ColorUtils;

public class ShadowRenderer {

    /* renamed from: i  reason: collision with root package name */
    public static final int[] f2208i = new int[3];

    /* renamed from: j  reason: collision with root package name */
    public static final float[] f2209j = {0.0f, 0.5f, 1.0f};

    /* renamed from: k  reason: collision with root package name */
    public static final int[] f2210k = new int[4];

    /* renamed from: l  reason: collision with root package name */
    public static final float[] f2211l = {0.0f, 0.0f, 0.5f, 1.0f};
    public final Paint a = new Paint();
    public final Paint b;
    public final Paint c;
    public int d;

    /* renamed from: e  reason: collision with root package name */
    public int f2212e;

    /* renamed from: f  reason: collision with root package name */
    public int f2213f;
    public final Path g = new Path();
    public Paint h = new Paint();

    public ShadowRenderer() {
        a(-16777216);
        this.h.setColor(0);
        Paint paint = new Paint(4);
        this.b = paint;
        paint.setStyle(Paint.Style.FILL);
        this.c = new Paint(this.b);
    }

    public void a(int i2) {
        this.d = ColorUtils.b(i2, 68);
        this.f2212e = ColorUtils.b(i2, 20);
        this.f2213f = ColorUtils.b(i2, 0);
        this.a.setColor(this.d);
    }
}
