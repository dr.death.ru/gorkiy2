package j.c.a.b.x;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.datepicker.MaterialCalendarGridView;
import i.h.l.ViewCompat;
import j.c.a.b.d;
import j.c.a.b.f;
import j.c.a.b.h;
import j.c.a.b.x.MaterialCalendar;
import j.c.a.b.x.f;
import j.c.a.b.x.q;

/* compiled from: MonthsPagerAdapter */
public class MonthsPagerAdapter0 extends RecyclerView.g<q.a> {
    public final CalendarConstraints c;
    public final DateSelector<?> d;

    /* renamed from: e  reason: collision with root package name */
    public final MaterialCalendar.f f2367e;

    /* renamed from: f  reason: collision with root package name */
    public final int f2368f;

    /* compiled from: MonthsPagerAdapter */
    public static class a extends RecyclerView.d0 {

        /* renamed from: t  reason: collision with root package name */
        public final TextView f2369t;
        public final MaterialCalendarGridView u;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: i.h.l.ViewCompat.a(android.view.View, boolean):void
         arg types: [android.widget.TextView, int]
         candidates:
          i.h.l.ViewCompat.a(android.view.View, i.h.l.WindowInsetsCompat):i.h.l.WindowInsetsCompat
          i.h.l.ViewCompat.a(int, android.view.View):void
          i.h.l.ViewCompat.a(android.view.View, float):void
          i.h.l.ViewCompat.a(android.view.View, int):void
          i.h.l.ViewCompat.a(android.view.View, android.content.res.ColorStateList):void
          i.h.l.ViewCompat.a(android.view.View, android.graphics.PorterDuff$Mode):void
          i.h.l.ViewCompat.a(android.view.View, android.graphics.Rect):void
          i.h.l.ViewCompat.a(android.view.View, android.graphics.drawable.Drawable):void
          i.h.l.ViewCompat.a(android.view.View, i.h.l.AccessibilityDelegateCompat):void
          i.h.l.ViewCompat.a(android.view.View, i.h.l.OnApplyWindowInsetsListener):void
          i.h.l.ViewCompat.a(android.view.View, i.h.l.x.AccessibilityNodeInfoCompat):void
          i.h.l.ViewCompat.a(android.view.View, java.lang.Runnable):void
          i.h.l.ViewCompat.a(android.view.View, java.lang.String):void
          i.h.l.ViewCompat.a(android.view.View, android.view.KeyEvent):boolean
          i.h.l.ViewCompat.a(android.view.View, boolean):void */
        public a(LinearLayout linearLayout, boolean z) {
            super(linearLayout);
            TextView textView = (TextView) linearLayout.findViewById(f.month_title);
            this.f2369t = textView;
            ViewCompat.a((View) textView, true);
            this.u = (MaterialCalendarGridView) linearLayout.findViewById(f.month_grid);
            if (!z) {
                this.f2369t.setVisibility(8);
            }
        }
    }

    public MonthsPagerAdapter0(Context context, DateSelector<?> dateSelector, a aVar, f.f fVar) {
        Month month = aVar.b;
        Month month2 = aVar.c;
        Month month3 = aVar.d;
        if (month.compareTo(month3) > 0) {
            throw new IllegalArgumentException("firstPage cannot be after currentPage");
        } else if (month3.compareTo(month2) <= 0) {
            this.f2368f = (MaterialCalendar.b(context) * MonthAdapter.f2365f) + (MaterialDatePicker.b(context) ? context.getResources().getDimensionPixelSize(d.mtrl_calendar_day_height) : 0);
            this.c = aVar;
            this.d = dateSelector;
            this.f2367e = fVar;
            if (!super.a.a()) {
                super.b = true;
                return;
            }
            throw new IllegalStateException("Cannot change whether this adapter has stable IDs while the adapter has registered observers.");
        } else {
            throw new IllegalArgumentException("currentPage cannot be after lastPage");
        }
    }

    public void a(RecyclerView.d0 d0Var, int i2) {
        a aVar = (a) d0Var;
        Month a2 = this.c.b.a(i2);
        aVar.f2369t.setText(a2.c);
        MaterialCalendarGridView materialCalendarGridView = (MaterialCalendarGridView) aVar.u.findViewById(j.c.a.b.f.month_grid);
        if (materialCalendarGridView.getAdapter() == null || !a2.equals(materialCalendarGridView.getAdapter().b)) {
            MonthAdapter monthAdapter = new MonthAdapter(a2, this.d, this.c);
            materialCalendarGridView.setNumColumns(a2.f2364f);
            materialCalendarGridView.setAdapter((ListAdapter) monthAdapter);
        } else {
            materialCalendarGridView.getAdapter().notifyDataSetChanged();
        }
        materialCalendarGridView.setOnItemClickListener(new MonthsPagerAdapter(this, materialCalendarGridView));
    }

    public Month b(int i2) {
        return this.c.b.a(i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public RecyclerView.d0 a(ViewGroup viewGroup, int i2) {
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(viewGroup.getContext()).inflate(h.mtrl_calendar_month_labeled, viewGroup, false);
        if (!MaterialDatePicker.b(viewGroup.getContext())) {
            return new a(linearLayout, false);
        }
        linearLayout.setLayoutParams(new RecyclerView.p(-1, this.f2368f));
        return new a(linearLayout, true);
    }

    public long a(int i2) {
        return this.c.b.a(i2).b.getTimeInMillis();
    }

    public int a() {
        return this.c.g;
    }

    public int a(Month month) {
        return this.c.b.b(month);
    }
}
