package j.c.a.a.g.a;

import androidx.recyclerview.widget.RecyclerView;
import i.b.k.ResourcesFlusher;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class p4<V> extends FutureTask<V> implements Comparable<p4> {
    public final long b;
    public final boolean c;
    public final String d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ l4 f2066e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public p4(l4 l4Var, Callable<V> callable, boolean z, String str) {
        super(callable);
        this.f2066e = l4Var;
        ResourcesFlusher.b((Object) str);
        long andIncrement = l4.f2027l.getAndIncrement();
        this.b = andIncrement;
        this.d = str;
        this.c = z;
        if (andIncrement == RecyclerView.FOREVER_NS) {
            l4Var.a().f2046f.a("Tasks index overflow");
        }
    }

    public final /* synthetic */ int compareTo(Object obj) {
        p4 p4Var = (p4) obj;
        boolean z = this.c;
        if (z != p4Var.c) {
            return z ? -1 : 1;
        }
        int i2 = (this.b > p4Var.b ? 1 : (this.b == p4Var.b ? 0 : -1));
        if (i2 < 0) {
            return -1;
        }
        if (i2 > 0) {
            return 1;
        }
        this.f2066e.a().g.a("Two tasks share the same index. index", Long.valueOf(this.b));
        return 0;
    }

    public final void setException(Throwable th) {
        this.f2066e.a().f2046f.a(this.d, th);
        super.setException(th);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public p4(l4 l4Var, Runnable runnable, String str) {
        super(runnable, null);
        this.f2066e = l4Var;
        ResourcesFlusher.b((Object) str);
        long andIncrement = l4.f2027l.getAndIncrement();
        this.b = andIncrement;
        this.d = str;
        this.c = false;
        if (andIncrement == RecyclerView.FOREVER_NS) {
            l4Var.a().f2046f.a("Tasks index overflow");
        }
    }
}
