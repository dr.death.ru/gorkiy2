package j.c.a.a.c.k.e;

import i.b.k.ResourcesFlusher;
import j.c.a.a.c.k.a;
import j.c.a.a.c.k.a.c;

public final class z<O extends a.c> {
    public final boolean a;
    public final a<O> b;
    public final O c;

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof z)) {
            return false;
        }
        z zVar = (z) obj;
        return !zVar.a && ResourcesFlusher.c(null, zVar.b) && ResourcesFlusher.c(null, zVar.c);
    }

    public final int hashCode() {
        return 0;
    }
}
