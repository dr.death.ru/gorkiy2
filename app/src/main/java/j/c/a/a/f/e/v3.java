package j.c.a.a.f.e;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class v3 implements c5 {
    public static final v3 a = new v3();

    public final d5 a(Class<?> cls) {
        Class<x3> cls2 = x3.class;
        if (!cls2.isAssignableFrom(cls)) {
            String name = cls.getName();
            throw new IllegalArgumentException(name.length() != 0 ? "Unsupported message type: ".concat(name) : new String("Unsupported message type: "));
        }
        try {
            return (d5) x3.a(cls.asSubclass(cls2)).a(3, (Object) null, (Object) null);
        } catch (Exception e2) {
            String name2 = cls.getName();
            throw new RuntimeException(name2.length() != 0 ? "Unable to get message info for ".concat(name2) : new String("Unable to get message info for "), e2);
        }
    }

    public final boolean b(Class<?> cls) {
        return x3.class.isAssignableFrom(cls);
    }
}
