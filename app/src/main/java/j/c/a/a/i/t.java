package j.c.a.a.i;

import j.c.c.g.m0;
import java.util.concurrent.Executor;

public final class t<TResult, TContinuationResult> implements b, c, d<TContinuationResult>, v<TResult> {
    public final Executor a;
    public final m0<TResult, TContinuationResult> b;
    public final y<TContinuationResult> c;

    public t(Executor executor, m0<TResult, TContinuationResult> m0Var, y<TContinuationResult> yVar) {
        this.a = executor;
        this.b = m0Var;
        this.c = yVar;
    }

    public final void a(e<TResult> eVar) {
        this.a.execute(new u(this, eVar));
    }

    public final void a(TContinuationResult tcontinuationresult) {
        this.c.a((Object) tcontinuationresult);
    }

    public final void a(Exception exc) {
        this.c.a(exc);
    }

    public final void a() {
        this.c.e();
    }
}
