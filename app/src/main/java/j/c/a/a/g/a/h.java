package j.c.a.a.g.a;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import i.b.k.ResourcesFlusher;
import j.c.a.a.c.l.p.a;
import java.util.Iterator;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class h extends a implements Iterable<String> {
    public static final Parcelable.Creator<h> CREATOR = new j();
    public final Bundle b;

    public h(Bundle bundle) {
        this.b = bundle;
    }

    public final Long a(String str) {
        return Long.valueOf(this.b.getLong(str));
    }

    public final Double b(String str) {
        return Double.valueOf(this.b.getDouble(str));
    }

    public final String c(String str) {
        return this.b.getString(str);
    }

    public final Iterator<String> iterator() {
        return new g(this);
    }

    public final String toString() {
        return this.b.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Bundle, boolean):void
     arg types: [android.os.Parcel, int, android.os.Bundle, int]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int, boolean, int):int
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.content.res.Resources$Theme):android.content.res.ColorStateList
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, android.content.res.Resources$Theme, android.util.AttributeSet, int[]):android.content.res.TypedArray
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int):java.lang.String
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, java.lang.String, boolean):void
      i.b.k.ResourcesFlusher.a(int, android.graphics.Rect, android.graphics.Rect, android.graphics.Rect):boolean
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, boolean):boolean
      i.b.k.ResourcesFlusher.a(i.f.a.h.d, i.f.a.h.f, java.util.List<i.f.a.h.f>, boolean):boolean
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Bundle, boolean):void */
    public final void writeToParcel(Parcel parcel, int i2) {
        int a = ResourcesFlusher.a(parcel);
        ResourcesFlusher.a(parcel, 2, b(), false);
        ResourcesFlusher.k(parcel, a);
    }

    public final Bundle b() {
        return new Bundle(this.b);
    }
}
