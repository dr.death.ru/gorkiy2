package j.c.a.a.f.e;

import j.c.a.a.f.e.x3;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class r0 extends x3<r0, a> implements g5 {
    public static final r0 zzf;
    public static volatile m5<r0> zzg;
    public int zzc;
    public String zzd = "";
    public long zze;

    /* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
    public static final class a extends x3.a<r0, a> implements g5 {
        public /* synthetic */ a(z0 z0Var) {
            super(r0.zzf);
        }
    }

    static {
        r0 r0Var = new r0();
        zzf = r0Var;
        x3.zzd.put(r0.class, r0Var);
    }

    public final Object a(int i2, Object obj, Object obj2) {
        switch (z0.a[i2 - 1]) {
            case 1:
                return new r0();
            case 2:
                return new a(null);
            case 3:
                return new r5(zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\b\u0000\u0002\u0002\u0001", new Object[]{"zzc", "zzd", "zze"});
            case 4:
                return zzf;
            case 5:
                m5<r0> m5Var = zzg;
                if (m5Var == null) {
                    synchronized (r0.class) {
                        m5Var = zzg;
                        if (m5Var == null) {
                            m5Var = new x3.c<>(zzf);
                            zzg = m5Var;
                        }
                    }
                }
                return m5Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
