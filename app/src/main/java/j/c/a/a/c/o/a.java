package j.c.a.a.c.o;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Binder;
import android.os.Process;
import j.c.a.a.c.n.c;

public class a {
    public final Context a;

    public a(Context context) {
        this.a = context;
    }

    public ApplicationInfo a(String str, int i2) {
        return this.a.getPackageManager().getApplicationInfo(str, i2);
    }

    public PackageInfo b(String str, int i2) {
        return this.a.getPackageManager().getPackageInfo(str, i2);
    }

    public CharSequence a(String str) {
        return this.a.getPackageManager().getApplicationLabel(this.a.getPackageManager().getApplicationInfo(str, 0));
    }

    public boolean a() {
        String nameForUid;
        if (Binder.getCallingUid() == Process.myUid()) {
            return c.a(this.a);
        }
        if (!c.e() || (nameForUid = this.a.getPackageManager().getNameForUid(Binder.getCallingUid())) == null) {
            return false;
        }
        return this.a.getPackageManager().isInstantApp(nameForUid);
    }
}
