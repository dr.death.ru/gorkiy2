package j.c.a.a.c.k;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.api.Scope;
import j.c.a.a.c.k.a.c;
import j.c.a.a.c.l.b;
import j.c.a.a.c.l.j;
import java.util.Set;

public final class a<O extends c> {

    /* renamed from: j.c.a.a.c.k.a$a  reason: collision with other inner class name */
    public static abstract class C0018a<T extends e, O> extends d<T, O> {
        public abstract T a(Context context, Looper looper, j.c.a.a.c.l.c cVar, O o2, c cVar2, d dVar);
    }

    public static class b<C> {
    }

    public interface c {

        /* renamed from: j.c.a.a.c.k.a$c$a  reason: collision with other inner class name */
        public interface C0019a extends c {
        }

        public interface b extends c {
        }

        /* renamed from: j.c.a.a.c.k.a$c$c  reason: collision with other inner class name */
        public interface C0020c extends C0019a, b {
        }
    }

    public static abstract class d<T, O> {
    }

    public interface e {
        void a(b.c cVar);

        void a(b.e eVar);

        void a(j jVar, Set<Scope> set);

        boolean a();

        j.c.a.a.c.d[] b();

        boolean c();

        String d();

        void e();

        boolean g();

        boolean h();

        int i();
    }

    public static final class f<C extends e> extends b<C> {
    }
}
