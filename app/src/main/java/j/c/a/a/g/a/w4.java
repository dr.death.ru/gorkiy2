package j.c.a.a.g.a;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class w4 implements Runnable {
    public final /* synthetic */ g9 b;
    public final /* synthetic */ s4 c;

    public w4(s4 s4Var, g9 g9Var) {
        this.c = s4Var;
        this.b = g9Var;
    }

    public final void run() {
        this.c.a.o();
        q8 q8Var = this.c.a;
        g9 g9Var = this.b;
        if (q8Var != null) {
            d9 a = q8Var.a(g9Var.b);
            if (a != null) {
                q8Var.b(g9Var, a);
                return;
            }
            return;
        }
        throw null;
    }
}
