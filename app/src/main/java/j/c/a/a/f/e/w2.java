package j.c.a.a.f.e;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.Iterator;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public abstract class w2 implements Serializable, Iterable<Byte> {
    public static final w2 c = new d3(y3.b);
    public static final a3 d = (q2.a() ? new c3(null) : new x2(null));
    public int b = 0;

    public static w2 a(byte[] bArr, int i2, int i3) {
        a(i2, i2 + i3, bArr.length);
        return new d3(d.a(bArr, i2, i3));
    }

    public static b3 c(int i2) {
        return new b3(i2, null);
    }

    public abstract byte a(int i2);

    public abstract int a();

    public abstract byte b(int i2);

    public final String b() {
        Charset charset = y3.a;
        if (a() == 0) {
            return "";
        }
        d3 d3Var = (d3) this;
        return new String(d3Var.f1843e, d3Var.c(), d3Var.a(), charset);
    }

    public abstract boolean equals(Object obj);

    public final int hashCode() {
        int i2 = this.b;
        if (i2 == 0) {
            int a = a();
            d3 d3Var = (d3) this;
            i2 = y3.a(a, d3Var.f1843e, d3Var.c(), a);
            if (i2 == 0) {
                i2 = 1;
            }
            this.b = i2;
        }
        return i2;
    }

    public /* synthetic */ Iterator iterator() {
        return new v2(this);
    }

    public final String toString() {
        return String.format("<ByteString@%s size=%d>", Integer.toHexString(System.identityHashCode(this)), Integer.valueOf(a()));
    }

    public static w2 a(String str) {
        return new d3(str.getBytes(y3.a));
    }

    public static int a(int i2, int i3, int i4) {
        int i5 = i3 - i2;
        if ((i2 | i3 | i5 | (i4 - i3)) >= 0) {
            return i5;
        }
        if (i2 < 0) {
            StringBuilder sb = new StringBuilder(32);
            sb.append("Beginning index: ");
            sb.append(i2);
            sb.append(" < 0");
            throw new IndexOutOfBoundsException(sb.toString());
        } else if (i3 < i2) {
            StringBuilder sb2 = new StringBuilder(66);
            sb2.append("Beginning index larger than ending index: ");
            sb2.append(i2);
            sb2.append(", ");
            sb2.append(i3);
            throw new IndexOutOfBoundsException(sb2.toString());
        } else {
            StringBuilder sb3 = new StringBuilder(37);
            sb3.append("End index: ");
            sb3.append(i3);
            sb3.append(" >= ");
            sb3.append(i4);
            throw new IndexOutOfBoundsException(sb3.toString());
        }
    }
}
