package j.c.a.a.c.l;

import android.accounts.Account;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import j.c.a.a.f.c.b;
import j.c.a.a.f.c.c;

public interface j extends IInterface {

    public static abstract class a extends b implements j {

        /* renamed from: j.c.a.a.c.l.j$a$a  reason: collision with other inner class name */
        public static class C0024a extends j.c.a.a.f.c.a implements j {
            public C0024a(IBinder iBinder) {
                super(iBinder, "com.google.android.gms.common.internal.IAccountAccessor");
            }

            public final Account e() {
                Parcel a = a(2, a());
                Account account = (Account) c.a(a, Account.CREATOR);
                a.recycle();
                return account;
            }
        }

        public static j a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IAccountAccessor");
            if (queryLocalInterface instanceof j) {
                return (j) queryLocalInterface;
            }
            return new C0024a(iBinder);
        }
    }

    Account e();
}
