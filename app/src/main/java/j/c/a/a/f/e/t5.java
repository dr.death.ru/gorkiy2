package j.c.a.a.f.e;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public interface t5<T> {
    int a(T t2);

    T a();

    void a(Object obj, c7 c7Var);

    void a(T t2, byte[] bArr, int i2, int i3, s2 s2Var);

    boolean a(Object obj, Object obj2);

    void b(T t2);

    void b(T t2, T t3);

    int c(T t2);

    boolean d(T t2);
}
