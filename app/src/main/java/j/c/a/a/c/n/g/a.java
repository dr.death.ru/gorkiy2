package j.c.a.a.c.n.g;

import i.b.k.ResourcesFlusher;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

public class a implements ThreadFactory {
    public final String b;
    public final ThreadFactory c = Executors.defaultThreadFactory();

    public a(String str) {
        ResourcesFlusher.b(str, "Name must not be null");
        this.b = str;
    }

    public Thread newThread(Runnable runnable) {
        Thread newThread = this.c.newThread(new b(runnable, 0));
        newThread.setName(this.b);
        return newThread;
    }
}
