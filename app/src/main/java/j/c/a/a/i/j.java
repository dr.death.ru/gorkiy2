package j.c.a.a.i;

import java.util.concurrent.Executor;

public final class j<TResult, TContinuationResult> implements v<TResult> {
    public final Executor a;
    public final a<TResult, TContinuationResult> b;
    public final y<TContinuationResult> c;

    public j(Executor executor, a<TResult, TContinuationResult> aVar, y<TContinuationResult> yVar) {
        this.a = executor;
        this.b = aVar;
        this.c = yVar;
    }

    public final void a(e<TResult> eVar) {
        this.a.execute(new k(this, eVar));
    }
}
