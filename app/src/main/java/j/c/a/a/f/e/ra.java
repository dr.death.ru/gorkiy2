package j.c.a.a.f.e;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class ra implements oa {
    public static final p1<Boolean> a;
    public static final p1<Boolean> b;
    public static final p1<Boolean> c;
    public static final p1<Boolean> d;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.p1.a(j.c.a.a.f.e.v1, java.lang.String, boolean):j.c.a.a.f.e.p1
     arg types: [j.c.a.a.f.e.v1, java.lang.String, int]
     candidates:
      j.c.a.a.f.e.p1.a(j.c.a.a.f.e.v1, java.lang.String, long):j.c.a.a.f.e.p1
      j.c.a.a.f.e.p1.a(j.c.a.a.f.e.v1, java.lang.String, java.lang.String):j.c.a.a.f.e.p1
      j.c.a.a.f.e.p1.a(j.c.a.a.f.e.v1, java.lang.String, boolean):j.c.a.a.f.e.p1 */
    static {
        v1 v1Var = new v1(q1.a("com.google.android.gms.measurement"));
        a = p1.a(v1Var, "measurement.client.sessions.background_sessions_enabled", true);
        b = p1.a(v1Var, "measurement.client.sessions.immediate_start_enabled_foreground", true);
        c = p1.a(v1Var, "measurement.client.sessions.remove_expired_session_properties_enabled", true);
        d = p1.a(v1Var, "measurement.client.sessions.session_id_enabled", true);
        p1.a(v1Var, "measurement.id.sessionization_client", 0);
        p1.a(v1Var, "measurement.id.sessions.immediate_session_start", 0);
    }

    public final boolean a() {
        return a.b().booleanValue();
    }

    public final boolean b() {
        return b.b().booleanValue();
    }

    public final boolean c() {
        return c.b().booleanValue();
    }

    public final boolean d() {
        return d.b().booleanValue();
    }
}
