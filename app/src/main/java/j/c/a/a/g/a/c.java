package j.c.a.a.g.a;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.Context;
import android.content.pm.PackageManager;
import i.h.e.ContextCompat;
import j.a.a.a.outline;
import j.c.a.a.c.n.b;
import java.io.IOException;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class c extends n5 {
    public long c;
    public String d;

    /* renamed from: e  reason: collision with root package name */
    public Boolean f1946e;

    /* renamed from: f  reason: collision with root package name */
    public AccountManager f1947f;
    public Boolean g;
    public long h;

    public c(r4 r4Var) {
        super(r4Var);
    }

    public final boolean a(Context context) {
        if (this.f1946e == null) {
            h9 h9Var = this.a.f2086f;
            this.f1946e = false;
            try {
                PackageManager packageManager = context.getPackageManager();
                if (packageManager != null) {
                    packageManager.getPackageInfo("com.google.android.gms", 128);
                    this.f1946e = true;
                }
            } catch (PackageManager.NameNotFoundException unused) {
            }
        }
        return this.f1946e.booleanValue();
    }

    public final boolean r() {
        Calendar instance = Calendar.getInstance();
        this.c = TimeUnit.MINUTES.convert((long) (instance.get(16) + instance.get(15)), TimeUnit.MILLISECONDS);
        Locale locale = Locale.getDefault();
        String lowerCase = locale.getLanguage().toLowerCase(Locale.ENGLISH);
        String lowerCase2 = locale.getCountry().toLowerCase(Locale.ENGLISH);
        StringBuilder sb = new StringBuilder(outline.a(lowerCase2, outline.a(lowerCase, 1)));
        sb.append(lowerCase);
        sb.append("-");
        sb.append(lowerCase2);
        this.d = sb.toString();
        return false;
    }

    public final long t() {
        o();
        return this.c;
    }

    public final String u() {
        o();
        return this.d;
    }

    public final long v() {
        d();
        return this.h;
    }

    public final boolean w() {
        d();
        if (((b) this.a.f2092n) != null) {
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis - this.h > 86400000) {
                this.g = null;
            }
            Boolean bool = this.g;
            if (bool != null) {
                return bool.booleanValue();
            }
            if (ContextCompat.a(this.a.a, "android.permission.GET_ACCOUNTS") != 0) {
                a().f2048j.a("Permission error checking for dasher/unicorn accounts");
                this.h = currentTimeMillis;
                this.g = false;
                return false;
            }
            if (this.f1947f == null) {
                this.f1947f = AccountManager.get(this.a.a);
            }
            try {
                Account[] result = this.f1947f.getAccountsByTypeAndFeatures("com.google", new String[]{"service_HOSTED"}, null, null).getResult();
                if (result == null || result.length <= 0) {
                    Account[] result2 = this.f1947f.getAccountsByTypeAndFeatures("com.google", new String[]{"service_uca"}, null, null).getResult();
                    if (result2 != null && result2.length > 0) {
                        this.g = true;
                        this.h = currentTimeMillis;
                        return true;
                    }
                    this.h = currentTimeMillis;
                    this.g = false;
                    return false;
                }
                this.g = true;
                this.h = currentTimeMillis;
                return true;
            } catch (AuthenticatorException | OperationCanceledException | IOException e2) {
                a().g.a("Exception checking account types", e2);
            }
        } else {
            throw null;
        }
    }
}
