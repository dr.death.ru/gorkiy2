package j.c.a.a.c.l;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import j.c.a.a.c.o.b;
import javax.annotation.concurrent.GuardedBy;

public final class g0 {
    public static Object a = new Object();
    @GuardedBy("sLock")
    public static boolean b;
    public static String c;
    public static int d;

    public static void a(Context context) {
        synchronized (a) {
            if (!b) {
                b = true;
                try {
                    Bundle bundle = b.b(context).a(context.getPackageName(), 128).metaData;
                    if (bundle != null) {
                        c = bundle.getString("com.google.app.id");
                        d = bundle.getInt("com.google.android.gms.version");
                    }
                } catch (PackageManager.NameNotFoundException e2) {
                    Log.wtf("MetadataValueReader", "This should never happen.", e2);
                }
            }
        }
    }
}
