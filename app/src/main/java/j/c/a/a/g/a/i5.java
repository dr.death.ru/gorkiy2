package j.c.a.a.g.a;

import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class i5 implements Callable<byte[]> {
    public final /* synthetic */ i b;
    public final /* synthetic */ String c;
    public final /* synthetic */ s4 d;

    public i5(s4 s4Var, i iVar, String str) {
        this.d = s4Var;
        this.b = iVar;
        this.c = str;
    }

    public final /* synthetic */ Object call() {
        this.d.a.o();
        q8 q8Var = this.d.a;
        q8.a(q8Var.h);
        u6 u6Var = q8Var.h;
        u6Var.d();
        u6Var.a.d();
        throw null;
    }
}
