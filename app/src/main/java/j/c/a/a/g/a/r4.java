package j.c.a.a.g.a;

import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import i.b.k.ResourcesFlusher;
import j.c.a.a.c.k.e.c;
import j.c.a.a.c.n.a;
import j.c.a.a.c.n.b;
import j.c.a.a.f.e.nb;
import j.c.a.a.f.e.p1;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public class r4 implements p5 {
    public static volatile r4 G;
    public volatile Boolean A;
    public Boolean B;
    public Boolean C;
    public int D;
    public AtomicInteger E = new AtomicInteger(0);
    public final long F;
    public final Context a;
    public final String b;
    public final String c;
    public final String d;

    /* renamed from: e  reason: collision with root package name */
    public final boolean f2085e;

    /* renamed from: f  reason: collision with root package name */
    public final h9 f2086f;
    public final i9 g;
    public final w3 h;

    /* renamed from: i  reason: collision with root package name */
    public final n3 f2087i;

    /* renamed from: j  reason: collision with root package name */
    public final l4 f2088j;

    /* renamed from: k  reason: collision with root package name */
    public final f8 f2089k;

    /* renamed from: l  reason: collision with root package name */
    public final y8 f2090l;

    /* renamed from: m  reason: collision with root package name */
    public final l3 f2091m;

    /* renamed from: n  reason: collision with root package name */
    public final a f2092n;

    /* renamed from: o  reason: collision with root package name */
    public final y6 f2093o;

    /* renamed from: p  reason: collision with root package name */
    public final x5 f2094p;

    /* renamed from: q  reason: collision with root package name */
    public final a f2095q;

    /* renamed from: r  reason: collision with root package name */
    public final r6 f2096r;

    /* renamed from: s  reason: collision with root package name */
    public j3 f2097s;

    /* renamed from: t  reason: collision with root package name */
    public z6 f2098t;
    public c u;
    public g3 v;
    public c4 w;
    public boolean x = false;
    public Boolean y;
    public long z;

    public r4(u5 u5Var) {
        Bundle bundle;
        boolean z2 = false;
        ResourcesFlusher.b(u5Var);
        h9 h9Var = new h9();
        this.f2086f = h9Var;
        k.a = h9Var;
        this.a = u5Var.a;
        this.b = u5Var.b;
        this.c = u5Var.c;
        this.d = u5Var.d;
        this.f2085e = u5Var.h;
        this.A = u5Var.f2105e;
        nb nbVar = u5Var.g;
        if (!(nbVar == null || (bundle = nbVar.h) == null)) {
            Object obj = bundle.get("measurementEnabled");
            if (obj instanceof Boolean) {
                this.B = (Boolean) obj;
            }
            Object obj2 = nbVar.h.get("measurementDeactivated");
            if (obj2 instanceof Boolean) {
                this.C = (Boolean) obj2;
            }
        }
        p1.a(this.a);
        b bVar = b.a;
        this.f2092n = bVar;
        if (bVar != null) {
            this.F = System.currentTimeMillis();
            this.g = new i9(this);
            w3 w3Var = new w3(this);
            w3Var.p();
            this.h = w3Var;
            n3 n3Var = new n3(this);
            n3Var.p();
            this.f2087i = n3Var;
            y8 y8Var = new y8(this);
            y8Var.p();
            this.f2090l = y8Var;
            l3 l3Var = new l3(this);
            l3Var.p();
            this.f2091m = l3Var;
            this.f2095q = new a(this);
            y6 y6Var = new y6(this);
            y6Var.x();
            this.f2093o = y6Var;
            x5 x5Var = new x5(this);
            x5Var.x();
            this.f2094p = x5Var;
            f8 f8Var = new f8(this);
            f8Var.x();
            this.f2089k = f8Var;
            r6 r6Var = new r6(this);
            r6Var.p();
            this.f2096r = r6Var;
            l4 l4Var = new l4(this);
            l4Var.p();
            this.f2088j = l4Var;
            nb nbVar2 = u5Var.g;
            if (!(nbVar2 == null || nbVar2.c == 0)) {
                z2 = true;
            }
            boolean z3 = !z2;
            if (this.a.getApplicationContext() instanceof Application) {
                x5 m2 = m();
                if (m2.a.a.getApplicationContext() instanceof Application) {
                    Application application = (Application) m2.a.a.getApplicationContext();
                    if (m2.c == null) {
                        m2.c = new p6(m2, null);
                    }
                    if (z3) {
                        application.unregisterActivityLifecycleCallbacks(m2.c);
                        application.registerActivityLifecycleCallbacks(m2.c);
                        m2.a().f2052n.a("Registered activity lifecycle callback");
                    }
                }
            } else {
                a().f2047i.a("Application context is not an Application");
            }
            l4 l4Var2 = this.f2088j;
            t4 t4Var = new t4(this, u5Var);
            l4Var2.o();
            ResourcesFlusher.b(t4Var);
            l4Var2.a((p4<?>) new p4(l4Var2, t4Var, "Task exception on worker thread"));
            return;
        }
        throw null;
    }

    public static void a(e5 e5Var) {
        if (e5Var == null) {
            throw new IllegalStateException("Component not created");
        } else if (!e5Var.b) {
            String valueOf = String.valueOf(e5Var.getClass());
            StringBuilder sb = new StringBuilder(valueOf.length() + 27);
            sb.append("Component not initialized: ");
            sb.append(valueOf);
            throw new IllegalStateException(sb.toString());
        }
    }

    public final boolean b() {
        return this.A != null && this.A.booleanValue();
    }

    public final boolean c() {
        boolean z2;
        i().d();
        if (!this.x) {
            throw new IllegalStateException("AppMeasurement is not initialized");
        } else if (this.g.a(k.k0)) {
            if (this.g.q()) {
                return false;
            }
            Boolean bool = this.C;
            if (bool != null && bool.booleanValue()) {
                return false;
            }
            Boolean u2 = l().u();
            if (u2 != null) {
                return u2.booleanValue();
            }
            Boolean r2 = this.g.r();
            if (r2 != null) {
                return r2.booleanValue();
            }
            Boolean bool2 = this.B;
            if (bool2 != null) {
                return bool2.booleanValue();
            }
            if (c.b()) {
                return false;
            }
            if (!this.g.a(k.f0) || this.A == null) {
                return true;
            }
            return this.A.booleanValue();
        } else if (this.g.q()) {
            return false;
        } else {
            Boolean r3 = this.g.r();
            if (r3 != null) {
                z2 = r3.booleanValue();
            } else {
                z2 = !c.b();
                if (z2 && this.A != null && k.f0.a(null).booleanValue()) {
                    z2 = this.A.booleanValue();
                }
            }
            w3 l2 = l();
            l2.d();
            return l2.v().getBoolean("measurement_enabled", z2);
        }
    }

    public final void d() {
        throw new IllegalStateException("Unexpected call on client side");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0033, code lost:
        if (java.lang.Math.abs(android.os.SystemClock.elapsedRealtime() - r7.z) > 1000) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00b9, code lost:
        if (android.text.TextUtils.isEmpty(r0.f1997m) == false) goto L_0x00bb;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean e() {
        /*
            r7 = this;
            boolean r0 = r7.x
            if (r0 == 0) goto L_0x00ca
            j.c.a.a.g.a.l4 r0 = r7.i()
            r0.d()
            java.lang.Boolean r0 = r7.y
            r1 = 0
            if (r0 == 0) goto L_0x0037
            long r2 = r7.z
            r4 = 0
            int r6 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r6 == 0) goto L_0x0037
            boolean r0 = r0.booleanValue()
            if (r0 != 0) goto L_0x00c2
            j.c.a.a.c.n.a r0 = r7.f2092n
            j.c.a.a.c.n.b r0 = (j.c.a.a.c.n.b) r0
            if (r0 == 0) goto L_0x0036
            long r2 = android.os.SystemClock.elapsedRealtime()
            long r4 = r7.z
            long r2 = r2 - r4
            long r2 = java.lang.Math.abs(r2)
            r4 = 1000(0x3e8, double:4.94E-321)
            int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x00c2
            goto L_0x0037
        L_0x0036:
            throw r1
        L_0x0037:
            j.c.a.a.c.n.a r0 = r7.f2092n
            j.c.a.a.c.n.b r0 = (j.c.a.a.c.n.b) r0
            if (r0 == 0) goto L_0x00c9
            long r0 = android.os.SystemClock.elapsedRealtime()
            r7.z = r0
            j.c.a.a.g.a.y8 r0 = r7.n()
            java.lang.String r1 = "android.permission.INTERNET"
            boolean r0 = r0.c(r1)
            r1 = 0
            r2 = 1
            if (r0 == 0) goto L_0x0083
            j.c.a.a.g.a.y8 r0 = r7.n()
            java.lang.String r3 = "android.permission.ACCESS_NETWORK_STATE"
            boolean r0 = r0.c(r3)
            if (r0 == 0) goto L_0x0083
            android.content.Context r0 = r7.a
            j.c.a.a.c.o.a r0 = j.c.a.a.c.o.b.b(r0)
            boolean r0 = r0.a()
            if (r0 != 0) goto L_0x0081
            j.c.a.a.g.a.i9 r0 = r7.g
            boolean r0 = r0.t()
            if (r0 != 0) goto L_0x0081
            android.content.Context r0 = r7.a
            boolean r0 = j.c.a.a.g.a.i4.a(r0)
            if (r0 == 0) goto L_0x0083
            android.content.Context r0 = r7.a
            boolean r0 = j.c.a.a.g.a.y8.a(r0)
            if (r0 == 0) goto L_0x0083
        L_0x0081:
            r0 = 1
            goto L_0x0084
        L_0x0083:
            r0 = 0
        L_0x0084:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r7.y = r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x00c2
            j.c.a.a.g.a.y8 r0 = r7.n()
            j.c.a.a.g.a.g3 r3 = r7.t()
            r3.w()
            java.lang.String r3 = r3.f1996l
            j.c.a.a.g.a.g3 r4 = r7.t()
            r4.w()
            java.lang.String r4 = r4.f1997m
            boolean r0 = r0.c(r3, r4)
            if (r0 != 0) goto L_0x00bb
            j.c.a.a.g.a.g3 r0 = r7.t()
            r0.w()
            java.lang.String r0 = r0.f1997m
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x00bc
        L_0x00bb:
            r1 = 1
        L_0x00bc:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r1)
            r7.y = r0
        L_0x00c2:
            java.lang.Boolean r0 = r7.y
            boolean r0 = r0.booleanValue()
            return r0
        L_0x00c9:
            throw r1
        L_0x00ca:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "AppMeasurement is not initialized"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.r4.e():boolean");
    }

    public final Context f() {
        return this.a;
    }

    public final r6 g() {
        a((n5) this.f2096r);
        return this.f2096r;
    }

    public final h9 h() {
        return this.f2086f;
    }

    public final l4 i() {
        a((n5) this.f2088j);
        return this.f2088j;
    }

    public final a j() {
        return this.f2092n;
    }

    public final i9 k() {
        return this.g;
    }

    public final w3 l() {
        a((o5) this.h);
        return this.h;
    }

    public final x5 m() {
        a((e5) this.f2094p);
        return this.f2094p;
    }

    public final y8 n() {
        a((o5) this.f2090l);
        return this.f2090l;
    }

    public final l3 o() {
        a((o5) this.f2091m);
        return this.f2091m;
    }

    public final boolean p() {
        return TextUtils.isEmpty(this.b);
    }

    public final y6 q() {
        a((e5) this.f2093o);
        return this.f2093o;
    }

    public final z6 r() {
        a((e5) this.f2098t);
        return this.f2098t;
    }

    public final c s() {
        a((n5) this.u);
        return this.u;
    }

    public final g3 t() {
        a((e5) this.v);
        return this.v;
    }

    public final a u() {
        a aVar = this.f2095q;
        if (aVar != null) {
            return aVar;
        }
        throw new IllegalStateException("Component not created");
    }

    public final n3 a() {
        a((n5) this.f2087i);
        return this.f2087i;
    }

    public static r4 a(Context context, Bundle bundle) {
        return a(context, new nb(0, 0, true, null, null, null, bundle));
    }

    public static r4 a(Context context, nb nbVar) {
        Bundle bundle;
        if (nbVar != null && (nbVar.f1883f == null || nbVar.g == null)) {
            nbVar = new nb(nbVar.b, nbVar.c, nbVar.d, nbVar.f1882e, null, null, nbVar.h);
        }
        ResourcesFlusher.b(context);
        ResourcesFlusher.b(context.getApplicationContext());
        if (G == null) {
            synchronized (r4.class) {
                if (G == null) {
                    G = new r4(new u5(context, nbVar));
                }
            }
        } else if (!(nbVar == null || (bundle = nbVar.h) == null || !bundle.containsKey("dataCollectionDefaultEnabled"))) {
            G.A = Boolean.valueOf(nbVar.h.getBoolean("dataCollectionDefaultEnabled"));
        }
        return G;
    }

    public static void a(n5 n5Var) {
        if (n5Var == null) {
            throw new IllegalStateException("Component not created");
        } else if (!n5Var.s()) {
            String valueOf = String.valueOf(n5Var.getClass());
            StringBuilder sb = new StringBuilder(valueOf.length() + 27);
            sb.append("Component not initialized: ");
            sb.append(valueOf);
            throw new IllegalStateException(sb.toString());
        }
    }

    public static void a(o5 o5Var) {
        if (o5Var == null) {
            throw new IllegalStateException("Component not created");
        }
    }
}
