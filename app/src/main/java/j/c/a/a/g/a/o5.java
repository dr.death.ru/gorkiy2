package j.c.a.a.g.a;

import android.content.Context;
import i.b.k.ResourcesFlusher;
import j.c.a.a.c.n.a;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public class o5 implements p5 {
    public final r4 a;

    public o5(r4 r4Var) {
        ResourcesFlusher.b(r4Var);
        this.a = r4Var;
    }

    public n3 a() {
        return this.a.a();
    }

    public void b() {
        if (this.a == null) {
            throw null;
        }
    }

    public void c() {
        this.a.i().c();
    }

    public void d() {
        this.a.i().d();
    }

    public c e() {
        return this.a.s();
    }

    public Context f() {
        return this.a.a;
    }

    public l3 g() {
        return this.a.o();
    }

    public h9 h() {
        return this.a.f2086f;
    }

    public l4 i() {
        return this.a.i();
    }

    public a j() {
        return this.a.f2092n;
    }

    public y8 k() {
        return this.a.n();
    }

    public w3 l() {
        return this.a.l();
    }

    public i9 m() {
        return this.a.g;
    }
}
