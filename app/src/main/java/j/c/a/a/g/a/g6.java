package j.c.a.a.g.a;

import android.os.Bundle;
import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import i.b.k.ResourcesFlusher;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class g6 implements Runnable {
    public final /* synthetic */ Bundle b;
    public final /* synthetic */ x5 c;

    public g6(x5 x5Var, Bundle bundle) {
        this.c = x5Var;
        this.b = bundle;
    }

    public final void run() {
        x5 x5Var = this.c;
        Bundle bundle = this.b;
        x5Var.d();
        x5Var.w();
        ResourcesFlusher.b(bundle);
        ResourcesFlusher.b(bundle.getString(DefaultAppMeasurementEventListenerRegistrar.NAME));
        if (!x5Var.a.c()) {
            x5Var.a().f2051m.a("Conditional property not cleared since collection is disabled");
            return;
        }
        x8 x8Var = new x8(bundle.getString(DefaultAppMeasurementEventListenerRegistrar.NAME), 0, null, null);
        try {
            i a = x5Var.k().a(bundle.getString("app_id"), bundle.getString("expired_event_name"), bundle.getBundle("expired_event_params"), bundle.getString("origin"), bundle.getLong("creation_timestamp"));
            String string = bundle.getString("app_id");
            String string2 = bundle.getString("origin");
            long j2 = bundle.getLong("creation_timestamp");
            boolean z = bundle.getBoolean("active");
            String string3 = bundle.getString("trigger_event_name");
            long j3 = bundle.getLong("trigger_timeout");
            long j4 = bundle.getLong("time_to_live");
            g9 g9Var = r3;
            g9 g9Var2 = new g9(string, string2, x8Var, j2, z, string3, null, j3, null, j4, a);
            x5Var.r().a(g9Var);
        } catch (IllegalArgumentException unused) {
        }
    }
}
