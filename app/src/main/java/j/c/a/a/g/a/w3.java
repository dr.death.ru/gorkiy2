package j.c.a.a.g.a;

import android.content.SharedPreferences;
import android.os.SystemClock;
import android.util.Pair;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import j.c.a.a.c.n.b;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Locale;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class w3 extends n5 {
    public static final Pair<String, Long> B = new Pair<>("", 0L);
    public b4 A = new b4(this, "deep_link_retrieval_attempts", 0);
    public SharedPreferences c;
    public a4 d;

    /* renamed from: e  reason: collision with root package name */
    public final b4 f2111e = new b4(this, "last_upload", 0);

    /* renamed from: f  reason: collision with root package name */
    public final b4 f2112f = new b4(this, "last_upload_attempt", 0);
    public final b4 g = new b4(this, "backoff", 0);
    public final b4 h = new b4(this, "last_delete_stale", 0);

    /* renamed from: i  reason: collision with root package name */
    public final b4 f2113i = new b4(this, "midnight_offset", 0);

    /* renamed from: j  reason: collision with root package name */
    public final b4 f2114j = new b4(this, "first_open_time", 0);

    /* renamed from: k  reason: collision with root package name */
    public final b4 f2115k = new b4(this, "app_install_time", 0);

    /* renamed from: l  reason: collision with root package name */
    public final d4 f2116l = new d4(this, "app_instance_id");

    /* renamed from: m  reason: collision with root package name */
    public String f2117m;

    /* renamed from: n  reason: collision with root package name */
    public boolean f2118n;

    /* renamed from: o  reason: collision with root package name */
    public long f2119o;

    /* renamed from: p  reason: collision with root package name */
    public final b4 f2120p = new b4(this, "time_before_start", 10000);

    /* renamed from: q  reason: collision with root package name */
    public final b4 f2121q = new b4(this, "session_timeout", 1800000);

    /* renamed from: r  reason: collision with root package name */
    public final y3 f2122r = new y3(this, "start_new_session", true);

    /* renamed from: s  reason: collision with root package name */
    public final d4 f2123s = new d4(this, "non_personalized_ads");

    /* renamed from: t  reason: collision with root package name */
    public final y3 f2124t = new y3(this, "use_dynamite_api", false);
    public final y3 u = new y3(this, "allow_remote_dynamite", false);
    public final b4 v = new b4(this, "last_pause_time", 0);
    public final b4 w = new b4(this, "time_active", 0);
    public boolean x;
    public y3 y = new y3(this, "app_backgrounded", false);
    public y3 z = new y3(this, "deep_link_retrieval_complete", false);

    public w3(r4 r4Var) {
        super(r4Var);
    }

    public final Pair<String, Boolean> a(String str) {
        d();
        if (((b) this.a.f2092n) != null) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            if (this.f2117m != null && elapsedRealtime < this.f2119o) {
                return new Pair<>(this.f2117m, Boolean.valueOf(this.f2118n));
            }
            this.f2119o = this.a.g.a(str, k.g) + elapsedRealtime;
            AdvertisingIdClient.setShouldSkipGmsCoreVersionCheck(true);
            try {
                AdvertisingIdClient.Info advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(this.a.a);
                if (advertisingIdInfo != null) {
                    this.f2117m = advertisingIdInfo.getId();
                    this.f2118n = advertisingIdInfo.isLimitAdTrackingEnabled();
                }
                if (this.f2117m == null) {
                    this.f2117m = "";
                }
            } catch (Exception e2) {
                a().f2051m.a("Unable to get advertising id", e2);
                this.f2117m = "";
            }
            AdvertisingIdClient.setShouldSkipGmsCoreVersionCheck(false);
            return new Pair<>(this.f2117m, Boolean.valueOf(this.f2118n));
        }
        throw null;
    }

    public final String b(String str) {
        d();
        String str2 = (String) a(str).first;
        MessageDigest x2 = y8.x();
        if (x2 == null) {
            return null;
        }
        return String.format(Locale.US, "%032X", new BigInteger(1, x2.digest(str2.getBytes())));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public final void n() {
        SharedPreferences sharedPreferences = this.a.a.getSharedPreferences("com.google.android.gms.measurement.prefs", 0);
        this.c = sharedPreferences;
        boolean z2 = sharedPreferences.getBoolean("has_been_opened", false);
        this.x = z2;
        if (!z2) {
            SharedPreferences.Editor edit = this.c.edit();
            edit.putBoolean("has_been_opened", true);
            edit.apply();
        }
        this.d = new a4(this, "health_monitor", Math.max(0L, k.h.a(null).longValue()), null);
    }

    public final boolean r() {
        return true;
    }

    public final Boolean t() {
        d();
        if (!v().contains("use_service")) {
            return null;
        }
        return Boolean.valueOf(v().getBoolean("use_service", false));
    }

    public final Boolean u() {
        d();
        if (v().contains("measurement_enabled")) {
            return Boolean.valueOf(v().getBoolean("measurement_enabled", true));
        }
        return null;
    }

    public final SharedPreferences v() {
        d();
        o();
        return this.c;
    }

    public final void b(boolean z2) {
        d();
        a().f2052n.a("Updating deferred analytics collection", Boolean.valueOf(z2));
        SharedPreferences.Editor edit = v().edit();
        edit.putBoolean("deferred_analytics_collection", z2);
        edit.apply();
    }

    public final void a(boolean z2) {
        d();
        a().f2052n.a("Setting measurementEnabled", Boolean.valueOf(z2));
        SharedPreferences.Editor edit = v().edit();
        edit.putBoolean("measurement_enabled", z2);
        edit.apply();
    }

    public final boolean a(long j2) {
        return j2 - this.f2121q.a() > this.v.a();
    }
}
