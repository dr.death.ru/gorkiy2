package j.c.a.a.f.e;

import com.google.android.gms.internal.measurement.zzek;
import j.c.a.a.f.e.o3;
import j.c.a.a.f.e.x3;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class m3<FieldDescriptorType extends o3<FieldDescriptorType>> {
    public static final m3 d = new m3(true);
    public final x5<FieldDescriptorType, Object> a;
    public boolean b;
    public boolean c;

    public m3() {
        this.c = false;
        this.a = x5.c(16);
    }

    public static int c(Map.Entry<FieldDescriptorType, Object> entry) {
        int c2;
        int f2;
        int c3;
        int h;
        o3 o3Var = (o3) entry.getKey();
        Object value = entry.getValue();
        if (o3Var.c() != z6.MESSAGE || o3Var.d() || o3Var.e()) {
            return b(o3Var, value);
        }
        if (value instanceof g4) {
            c2 = zzek.c(2, ((o3) entry.getKey()).a()) + (zzek.f(1) << 1);
            f2 = zzek.f(3);
            c3 = ((g4) value).a();
            h = zzek.h(c3);
        } else {
            c2 = zzek.c(2, ((o3) entry.getKey()).a()) + (zzek.f(1) << 1);
            f2 = zzek.f(3);
            c3 = ((f5) value).c();
            h = zzek.h(c3);
        }
        return h + c3 + f2 + c2;
    }

    public final Iterator<Map.Entry<FieldDescriptorType, Object>> a() {
        if (this.c) {
            return new l4(this.a.entrySet().iterator());
        }
        return this.a.entrySet().iterator();
    }

    public final boolean b() {
        for (int i2 = 0; i2 < this.a.b(); i2++) {
            if (!b(this.a.a(i2))) {
                return false;
            }
        }
        for (Map.Entry<FieldDescriptorType, Object> b2 : this.a.c()) {
            if (!b(b2)) {
                return false;
            }
        }
        return true;
    }

    public final /* synthetic */ Object clone() {
        m3 m3Var = new m3();
        for (int i2 = 0; i2 < this.a.b(); i2++) {
            Map.Entry<FieldDescriptorType, Object> a2 = this.a.a(i2);
            m3Var.a((o3) a2.getKey(), a2.getValue());
        }
        for (Map.Entry next : this.a.c()) {
            m3Var.a((o3) next.getKey(), next.getValue());
        }
        m3Var.c = this.c;
        return m3Var;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof m3)) {
            return false;
        }
        return this.a.equals(((m3) obj).a);
    }

    public final int hashCode() {
        return this.a.hashCode();
    }

    public m3(boolean z) {
        this.c = false;
        w5 w5Var = new w5(0);
        this.a = w5Var;
        if (!this.b) {
            w5Var.a();
            this.b = true;
        }
    }

    public final void a(FieldDescriptorType fielddescriptortype, Object obj) {
        if (!fielddescriptortype.d()) {
            a(fielddescriptortype.b(), obj);
        } else if (obj instanceof List) {
            ArrayList arrayList = new ArrayList();
            arrayList.addAll((List) obj);
            int size = arrayList.size();
            int i2 = 0;
            while (i2 < size) {
                Object obj2 = arrayList.get(i2);
                i2++;
                a(fielddescriptortype.b(), obj2);
            }
            obj = arrayList;
        } else {
            throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
        }
        if (obj instanceof g4) {
            this.c = true;
        }
        this.a.put(fielddescriptortype, obj);
    }

    public static boolean b(Map.Entry<FieldDescriptorType, Object> entry) {
        o3 o3Var = (o3) entry.getKey();
        if (o3Var.c() == z6.MESSAGE) {
            if (o3Var.d()) {
                for (f5 l2 : (List) entry.getValue()) {
                    if (!l2.l()) {
                        return false;
                    }
                }
            } else {
                Object value = entry.getValue();
                if (value instanceof f5) {
                    if (!((f5) value).l()) {
                        return false;
                    }
                } else if (value instanceof g4) {
                    return true;
                } else {
                    throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
                }
            }
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002a, code lost:
        if ((r2 instanceof byte[]) == false) goto L_0x0041;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0018, code lost:
        if ((r2 instanceof j.c.a.a.f.e.g4) == false) goto L_0x0041;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0021, code lost:
        if ((r2 instanceof j.c.a.a.f.e.b4) == false) goto L_0x0041;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(j.c.a.a.f.e.w6 r1, java.lang.Object r2) {
        /*
            j.c.a.a.f.e.y3.a(r2)
            int[] r0 = j.c.a.a.f.e.p3.a
            j.c.a.a.f.e.z6 r1 = r1.zzs
            int r1 = r1.ordinal()
            r1 = r0[r1]
            r0 = 0
            switch(r1) {
                case 1: goto L_0x003e;
                case 2: goto L_0x003b;
                case 3: goto L_0x0038;
                case 4: goto L_0x0035;
                case 5: goto L_0x0032;
                case 6: goto L_0x002f;
                case 7: goto L_0x0024;
                case 8: goto L_0x001b;
                case 9: goto L_0x0012;
                default: goto L_0x0011;
            }
        L_0x0011:
            goto L_0x0041
        L_0x0012:
            boolean r1 = r2 instanceof j.c.a.a.f.e.f5
            if (r1 != 0) goto L_0x002c
            boolean r1 = r2 instanceof j.c.a.a.f.e.g4
            if (r1 == 0) goto L_0x0041
            goto L_0x002c
        L_0x001b:
            boolean r1 = r2 instanceof java.lang.Integer
            if (r1 != 0) goto L_0x002c
            boolean r1 = r2 instanceof j.c.a.a.f.e.b4
            if (r1 == 0) goto L_0x0041
            goto L_0x002c
        L_0x0024:
            boolean r1 = r2 instanceof j.c.a.a.f.e.w2
            if (r1 != 0) goto L_0x002c
            boolean r1 = r2 instanceof byte[]
            if (r1 == 0) goto L_0x0041
        L_0x002c:
            r1 = 1
            r0 = 1
            goto L_0x0041
        L_0x002f:
            boolean r1 = r2 instanceof java.lang.String
            goto L_0x0040
        L_0x0032:
            boolean r1 = r2 instanceof java.lang.Boolean
            goto L_0x0040
        L_0x0035:
            boolean r1 = r2 instanceof java.lang.Double
            goto L_0x0040
        L_0x0038:
            boolean r1 = r2 instanceof java.lang.Float
            goto L_0x0040
        L_0x003b:
            boolean r1 = r2 instanceof java.lang.Long
            goto L_0x0040
        L_0x003e:
            boolean r1 = r2 instanceof java.lang.Integer
        L_0x0040:
            r0 = r1
        L_0x0041:
            if (r0 == 0) goto L_0x0044
            return
        L_0x0044:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Wrong object type used with protocol message reflection."
            r1.<init>(r2)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.f.e.m3.a(j.c.a.a.f.e.w6, java.lang.Object):void");
    }

    public static int b(w6 w6Var, Object obj) {
        switch (p3.b[w6Var.ordinal()]) {
            case 1:
                ((Double) obj).doubleValue();
                zzek.c();
                return 8;
            case 2:
                ((Float) obj).floatValue();
                zzek.b();
                return 4;
            case 3:
                return zzek.c(((Long) obj).longValue());
            case 4:
                return zzek.c(((Long) obj).longValue());
            case 5:
                return zzek.g(((Integer) obj).intValue());
            case 6:
                ((Long) obj).longValue();
                zzek.e();
                return 8;
            case 7:
                ((Integer) obj).intValue();
                zzek.g();
                return 4;
            case 8:
                ((Boolean) obj).booleanValue();
                zzek.d();
                return 1;
            case 9:
                return zzek.b((f5) obj);
            case 10:
                if (obj instanceof g4) {
                    return zzek.a((g4) obj);
                }
                return zzek.a((f5) obj);
            case 11:
                if (obj instanceof w2) {
                    return zzek.a((w2) obj);
                }
                return zzek.a((String) obj);
            case 12:
                if (obj instanceof w2) {
                    return zzek.a((w2) obj);
                }
                return zzek.b((byte[]) obj);
            case 13:
                return zzek.h(((Integer) obj).intValue());
            case 14:
                ((Integer) obj).intValue();
                zzek.h();
                return 4;
            case 15:
                ((Long) obj).longValue();
                zzek.f();
                return 8;
            case 16:
                return zzek.j(((Integer) obj).intValue());
            case 17:
                return zzek.d(((Long) obj).longValue());
            case 18:
                if (obj instanceof b4) {
                    return zzek.g(((b4) obj).a());
                }
                return zzek.g(((Integer) obj).intValue());
            default:
                throw new RuntimeException("There is no way to get here, but the compiler thinks otherwise.");
        }
    }

    public static Object a(Object obj) {
        if (obj instanceof i5) {
            return ((i5) obj).a();
        }
        if (!(obj instanceof byte[])) {
            return obj;
        }
        byte[] bArr = (byte[]) obj;
        byte[] bArr2 = new byte[bArr.length];
        System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        return bArr2;
    }

    public final void a(Map.Entry<FieldDescriptorType, Object> entry) {
        Object obj;
        o3 o3Var = (o3) entry.getKey();
        Object value = entry.getValue();
        if (value instanceof g4) {
            g4.c();
            throw null;
        } else if (o3Var.d()) {
            Object obj2 = this.a.get(o3Var);
            if (!(obj2 instanceof g4)) {
                if (obj2 == null) {
                    obj2 = new ArrayList();
                }
                for (Object a2 : (List) value) {
                    ((List) obj2).add(a(a2));
                }
                this.a.put(o3Var, obj2);
                return;
            }
            g4.c();
            throw null;
        } else if (o3Var.c() == z6.MESSAGE) {
            Object obj3 = this.a.get(o3Var);
            if (obj3 instanceof g4) {
                g4.c();
                throw null;
            } else if (obj3 == null) {
                this.a.put(o3Var, a(value));
            } else {
                if (obj3 instanceof i5) {
                    obj = o3Var.a((i5) obj3, (i5) value);
                } else {
                    obj = ((x3.a) o3Var.a(((f5) obj3).d(), (f5) value)).m();
                }
                this.a.put(o3Var, obj);
            }
        } else {
            this.a.put(o3Var, a(value));
        }
    }

    public static int b(o3<?> o3Var, Object obj) {
        w6 b2 = o3Var.b();
        int a2 = o3Var.a();
        if (!o3Var.d()) {
            return a(b2, a2, obj);
        }
        int i2 = 0;
        if (o3Var.e()) {
            for (Object b3 : (List) obj) {
                i2 += b(b2, b3);
            }
            return zzek.f(a2) + i2 + zzek.h(i2);
        }
        for (Object a3 : (List) obj) {
            i2 += a(b2, a2, a3);
        }
        return i2;
    }

    public static int a(w6 w6Var, int i2, Object obj) {
        int f2 = zzek.f(i2);
        if (w6Var == w6.zzj) {
            f5 f5Var = (f5) obj;
            f2 <<= 1;
        }
        return b(w6Var, obj) + f2;
    }
}
