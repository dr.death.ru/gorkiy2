package j.c.a.a.c.k.e;

import android.util.Log;
import j.c.a.a.c.b;
import j.c.a.a.c.k.e.b;
import j.c.a.a.c.l.j;
import java.util.Collections;

public final class p implements Runnable {
    public final /* synthetic */ b b;
    public final /* synthetic */ b.c c;

    public p(b.c cVar, j.c.a.a.c.b bVar) {
        this.c = cVar;
        this.b = bVar;
    }

    public final void run() {
        j jVar;
        if (this.b.c()) {
            b.c cVar = this.c;
            cVar.f1795e = true;
            if (cVar.a.g()) {
                b.c cVar2 = this.c;
                if (cVar2.f1795e && (jVar = cVar2.c) != null) {
                    cVar2.a.a(jVar, cVar2.d);
                    return;
                }
                return;
            }
            try {
                this.c.a.a(null, Collections.emptySet());
            } catch (SecurityException e2) {
                Log.e("GoogleApiManager", "Failed to get service from broker. ", e2);
                b.c cVar3 = this.c;
                b.this.h.get(cVar3.b).a(new j.c.a.a.c.b(10));
            }
        } else {
            b.c cVar4 = this.c;
            b.this.h.get(cVar4.b).a(this.b);
        }
    }
}
