package j.c.a.a.g.a;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class q6 implements Runnable {
    public final /* synthetic */ long b;
    public final /* synthetic */ x5 c;

    public q6(x5 x5Var, long j2) {
        this.c = x5Var;
        this.b = j2;
    }

    public final void run() {
        this.c.l().f2121q.a(this.b);
        this.c.a().f2051m.a("Session timeout duration set", Long.valueOf(this.b));
    }
}
