package j.c.a.a.g.a;

import i.b.k.ResourcesFlusher;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class s3 implements Runnable {
    public final t3 b;
    public final int c;
    public final Throwable d;

    /* renamed from: e  reason: collision with root package name */
    public final byte[] f2099e;

    /* renamed from: f  reason: collision with root package name */
    public final String f2100f;
    public final Map<String, List<String>> g;

    public /* synthetic */ s3(String str, t3 t3Var, int i2, Throwable th, byte[] bArr, Map map, q3 q3Var) {
        ResourcesFlusher.b(t3Var);
        this.b = t3Var;
        this.c = i2;
        this.d = th;
        this.f2099e = bArr;
        this.f2100f = str;
        this.g = map;
    }

    public final void run() {
        this.b.a(this.f2100f, this.c, this.d, this.f2099e, this.g);
    }
}
