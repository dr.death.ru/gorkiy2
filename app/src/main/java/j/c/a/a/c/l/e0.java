package j.c.a.a.c.l;

import android.os.IBinder;
import android.os.IInterface;
import j.c.a.a.f.c.b;

public abstract class e0 extends b implements d0 {
    public static d0 a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IGoogleCertificatesApi");
        if (queryLocalInterface instanceof d0) {
            return (d0) queryLocalInterface;
        }
        return new f0(iBinder);
    }
}
