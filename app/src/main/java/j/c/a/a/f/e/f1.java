package j.c.a.a.f.e;

import android.os.Build;
import android.os.UserManager;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public class f1 {
    public static UserManager a;
    public static volatile boolean b = (!a());

    public static boolean a() {
        return Build.VERSION.SDK_INT >= 24;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0036, code lost:
        if (r3.isUserRunning(android.os.Process.myUserHandle()) != false) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0046, code lost:
        r1 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0050, code lost:
        return r1;
     */
    @android.annotation.TargetApi(24)
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(android.content.Context r7) {
        /*
            boolean r0 = j.c.a.a.f.e.f1.b
            r1 = 1
            if (r0 == 0) goto L_0x0006
            return r1
        L_0x0006:
            java.lang.Class<j.c.a.a.f.e.f1> r0 = j.c.a.a.f.e.f1.class
            monitor-enter(r0)
            boolean r2 = j.c.a.a.f.e.f1.b     // Catch:{ all -> 0x0051 }
            if (r2 == 0) goto L_0x000f
            monitor-exit(r0)     // Catch:{ all -> 0x0051 }
            return r1
        L_0x000f:
            r2 = 1
        L_0x0010:
            r3 = 2
            r4 = 0
            r5 = 0
            if (r2 > r3) goto L_0x0046
            android.os.UserManager r3 = j.c.a.a.f.e.f1.a     // Catch:{ all -> 0x0051 }
            if (r3 != 0) goto L_0x0023
            java.lang.Class<android.os.UserManager> r3 = android.os.UserManager.class
            java.lang.Object r3 = r7.getSystemService(r3)     // Catch:{ all -> 0x0051 }
            android.os.UserManager r3 = (android.os.UserManager) r3     // Catch:{ all -> 0x0051 }
            j.c.a.a.f.e.f1.a = r3     // Catch:{ all -> 0x0051 }
        L_0x0023:
            android.os.UserManager r3 = j.c.a.a.f.e.f1.a     // Catch:{ all -> 0x0051 }
            if (r3 != 0) goto L_0x0028
            goto L_0x004b
        L_0x0028:
            boolean r6 = r3.isUserUnlocked()     // Catch:{ NullPointerException -> 0x0039 }
            if (r6 != 0) goto L_0x0047
            android.os.UserHandle r6 = android.os.Process.myUserHandle()     // Catch:{ NullPointerException -> 0x0039 }
            boolean r7 = r3.isUserRunning(r6)     // Catch:{ NullPointerException -> 0x0039 }
            if (r7 != 0) goto L_0x0046
            goto L_0x0047
        L_0x0039:
            r3 = move-exception
            java.lang.String r5 = "DirectBootUtils"
            java.lang.String r6 = "Failed to check if user is unlocked."
            android.util.Log.w(r5, r6, r3)     // Catch:{ all -> 0x0051 }
            j.c.a.a.f.e.f1.a = r4     // Catch:{ all -> 0x0051 }
            int r2 = r2 + 1
            goto L_0x0010
        L_0x0046:
            r1 = 0
        L_0x0047:
            if (r1 == 0) goto L_0x004b
            j.c.a.a.f.e.f1.a = r4     // Catch:{ all -> 0x0051 }
        L_0x004b:
            if (r1 == 0) goto L_0x004f
            j.c.a.a.f.e.f1.b = r1     // Catch:{ all -> 0x0051 }
        L_0x004f:
            monitor-exit(r0)     // Catch:{ all -> 0x0051 }
            return r1
        L_0x0051:
            r7 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0051 }
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.f.e.f1.a(android.content.Context):boolean");
    }
}
