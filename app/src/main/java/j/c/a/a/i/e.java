package j.c.a.a.i;

import java.util.concurrent.Executor;

public abstract class e<TResult> {
    public e<TResult> a(Executor executor, b bVar) {
        throw new UnsupportedOperationException("addOnCanceledListener is not implemented");
    }

    public abstract e<TResult> a(Executor executor, c cVar);

    public abstract e<TResult> a(Executor executor, d dVar);

    public abstract Exception a();

    public abstract <X extends Throwable> TResult a(Class cls);

    public abstract TResult b();

    public abstract boolean c();

    public abstract boolean d();
}
