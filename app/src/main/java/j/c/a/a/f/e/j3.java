package j.c.a.a.f.e;

import j.c.a.a.f.e.x3;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public class j3 {
    public static volatile j3 b;
    public static volatile j3 c;
    public static final j3 d = new j3(true);
    public final Map<a, x3.e<?, ?>> a;

    /* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
    public static final class a {
        public final Object a;
        public final int b;

        public a(Object obj, int i2) {
            this.a = obj;
            this.b = i2;
        }

        public final boolean equals(Object obj) {
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            if (this.a == aVar.a && this.b == aVar.b) {
                return true;
            }
            return false;
        }

        public final int hashCode() {
            return (System.identityHashCode(this.a) * 65535) + this.b;
        }
    }

    static {
        try {
            Class.forName("com.google.protobuf.Extension");
        } catch (ClassNotFoundException unused) {
        }
    }

    public j3() {
        this.a = new HashMap();
    }

    public static j3 a() {
        j3 j3Var = b;
        if (j3Var == null) {
            synchronized (j3.class) {
                j3Var = b;
                if (j3Var == null) {
                    j3Var = d;
                    b = j3Var;
                }
            }
        }
        return j3Var;
    }

    public static j3 b() {
        Class<j3> cls = j3.class;
        j3 j3Var = c;
        if (j3Var == null) {
            synchronized (cls) {
                j3Var = c;
                if (j3Var == null) {
                    j3Var = w3.a(cls);
                    c = j3Var;
                }
            }
        }
        return j3Var;
    }

    public j3(boolean z) {
        this.a = Collections.emptyMap();
    }
}
