package j.c.a.a.f.e;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class s3 extends r2<Float> implements e4<Float>, p5, RandomAccess {
    public float[] c;
    public int d;

    static {
        new s3(new float[0], 0).b = false;
    }

    public s3() {
        this.c = new float[10];
        this.d = 0;
    }

    public final void a(float f2) {
        c();
        int i2 = this.d;
        float[] fArr = this.c;
        if (i2 == fArr.length) {
            float[] fArr2 = new float[(((i2 * 3) / 2) + 1)];
            System.arraycopy(fArr, 0, fArr2, 0, i2);
            this.c = fArr2;
        }
        float[] fArr3 = this.c;
        int i3 = this.d;
        this.d = i3 + 1;
        fArr3[i3] = f2;
    }

    public final /* synthetic */ void add(int i2, Object obj) {
        int i3;
        float floatValue = ((Float) obj).floatValue();
        c();
        if (i2 < 0 || i2 > (i3 = this.d)) {
            throw new IndexOutOfBoundsException(c(i2));
        }
        float[] fArr = this.c;
        if (i3 < fArr.length) {
            System.arraycopy(fArr, i2, fArr, i2 + 1, i3 - i2);
        } else {
            float[] fArr2 = new float[(((i3 * 3) / 2) + 1)];
            System.arraycopy(fArr, 0, fArr2, 0, i2);
            System.arraycopy(this.c, i2, fArr2, i2 + 1, this.d - i2);
            this.c = fArr2;
        }
        this.c[i2] = floatValue;
        this.d++;
        this.modCount++;
    }

    public final boolean addAll(Collection<? extends Float> collection) {
        c();
        y3.a(collection);
        if (!(collection instanceof s3)) {
            return super.addAll(collection);
        }
        s3 s3Var = (s3) collection;
        int i2 = s3Var.d;
        if (i2 == 0) {
            return false;
        }
        int i3 = this.d;
        if (Integer.MAX_VALUE - i3 >= i2) {
            int i4 = i3 + i2;
            float[] fArr = this.c;
            if (i4 > fArr.length) {
                this.c = Arrays.copyOf(fArr, i4);
            }
            System.arraycopy(s3Var.c, 0, this.c, this.d, s3Var.d);
            this.d = i4;
            this.modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    public final void b(int i2) {
        if (i2 < 0 || i2 >= this.d) {
            throw new IndexOutOfBoundsException(c(i2));
        }
    }

    public final String c(int i2) {
        int i3 = this.d;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i2);
        sb.append(", Size:");
        sb.append(i3);
        return sb.toString();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof s3)) {
            return super.equals(obj);
        }
        s3 s3Var = (s3) obj;
        if (this.d != s3Var.d) {
            return false;
        }
        float[] fArr = s3Var.c;
        for (int i2 = 0; i2 < this.d; i2++) {
            if (Float.floatToIntBits(this.c[i2]) != Float.floatToIntBits(fArr[i2])) {
                return false;
            }
        }
        return true;
    }

    public final /* synthetic */ Object get(int i2) {
        b(i2);
        return Float.valueOf(this.c[i2]);
    }

    public final int hashCode() {
        int i2 = 1;
        for (int i3 = 0; i3 < this.d; i3++) {
            i2 = (i2 * 31) + Float.floatToIntBits(this.c[i3]);
        }
        return i2;
    }

    public final boolean remove(Object obj) {
        c();
        for (int i2 = 0; i2 < this.d; i2++) {
            if (obj.equals(Float.valueOf(this.c[i2]))) {
                float[] fArr = this.c;
                System.arraycopy(fArr, i2 + 1, fArr, i2, (this.d - i2) - 1);
                this.d--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    public final void removeRange(int i2, int i3) {
        c();
        if (i3 >= i2) {
            float[] fArr = this.c;
            System.arraycopy(fArr, i3, fArr, i2, this.d - i3);
            this.d -= i3 - i2;
            this.modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    public final /* synthetic */ Object set(int i2, Object obj) {
        float floatValue = ((Float) obj).floatValue();
        c();
        b(i2);
        float[] fArr = this.c;
        float f2 = fArr[i2];
        fArr[i2] = floatValue;
        return Float.valueOf(f2);
    }

    public final int size() {
        return this.d;
    }

    public s3(float[] fArr, int i2) {
        this.c = fArr;
        this.d = i2;
    }

    public final /* synthetic */ Object remove(int i2) {
        c();
        b(i2);
        float[] fArr = this.c;
        float f2 = fArr[i2];
        int i3 = this.d;
        if (i2 < i3 - 1) {
            System.arraycopy(fArr, i2 + 1, fArr, i2, (i3 - i2) - 1);
        }
        this.d--;
        this.modCount++;
        return Float.valueOf(f2);
    }

    public final /* synthetic */ e4 a(int i2) {
        if (i2 >= this.d) {
            return new s3(Arrays.copyOf(this.c, i2), this.d);
        }
        throw new IllegalArgumentException();
    }

    public final /* synthetic */ boolean add(Object obj) {
        a(((Float) obj).floatValue());
        return true;
    }
}
