package j.c.a.a.f.e;

import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public interface y4 {
    int a(int i2, Object obj, Object obj2);

    Object a(Object obj, Object obj2);

    Map<?, ?> a(Object obj);

    boolean b(Object obj);

    Map<?, ?> c(Object obj);

    Object d(Object obj);

    x4<?, ?> e(Object obj);

    Object f(Object obj);
}
