package j.c.a.a.f.e;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public class d3 extends w2 {

    /* renamed from: e  reason: collision with root package name */
    public final byte[] f1843e;

    public d3(byte[] bArr) {
        if (bArr != null) {
            this.f1843e = bArr;
            return;
        }
        throw null;
    }

    public byte a(int i2) {
        return this.f1843e[i2];
    }

    public byte b(int i2) {
        return this.f1843e[i2];
    }

    public int c() {
        return 0;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof w2) || a() != ((w2) obj).a()) {
            return false;
        }
        if (a() == 0) {
            return true;
        }
        if (!(obj instanceof d3)) {
            return obj.equals(this);
        }
        d3 d3Var = (d3) obj;
        int i2 = super.b;
        int i3 = super.b;
        if (i2 != 0 && i3 != 0 && i2 != i3) {
            return false;
        }
        int a = a();
        if (a > super.a()) {
            int a2 = a();
            StringBuilder sb = new StringBuilder(40);
            sb.append("Length too large: ");
            sb.append(a);
            sb.append(a2);
            throw new IllegalArgumentException(sb.toString());
        } else if (a <= super.a()) {
            byte[] bArr = this.f1843e;
            byte[] bArr2 = d3Var.f1843e;
            int c = c() + a;
            int c2 = c();
            int c3 = d3Var.c();
            while (c2 < c) {
                if (bArr[c2] != bArr2[c3]) {
                    return false;
                }
                c2++;
                c3++;
            }
            return true;
        } else {
            int a3 = super.a();
            StringBuilder sb2 = new StringBuilder(59);
            sb2.append("Ran off end of other: 0, ");
            sb2.append(a);
            sb2.append(", ");
            sb2.append(a3);
            throw new IllegalArgumentException(sb2.toString());
        }
    }

    public int a() {
        return this.f1843e.length;
    }
}
