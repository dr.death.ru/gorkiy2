package j.c.a.a.g.a;

import j.c.a.a.c.n.b;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class e8 extends b {

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ f8 f1984e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public e8(f8 f8Var, p5 p5Var) {
        super(p5Var);
        this.f1984e = f8Var;
    }

    public final void a() {
        f8 f8Var = this.f1984e;
        f8Var.d();
        if (((b) f8Var.a.f2092n) != null) {
            f8Var.b(System.currentTimeMillis(), false);
            return;
        }
        throw null;
    }
}
