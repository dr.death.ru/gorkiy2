package j.c.a.a.g.a;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class k5 implements Runnable {
    public final /* synthetic */ x8 b;
    public final /* synthetic */ d9 c;
    public final /* synthetic */ s4 d;

    public k5(s4 s4Var, x8 x8Var, d9 d9Var) {
        this.d = s4Var;
        this.b = x8Var;
        this.c = d9Var;
    }

    public final void run() {
        this.d.a.o();
        this.d.a.a(this.b, this.c);
    }
}
