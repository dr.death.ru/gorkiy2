package j.c.a.a.g.a;

import com.google.android.gms.measurement.internal.AppMeasurementDynamiteService;
import j.c.a.a.f.e.fb;

/* compiled from: com.google.android.gms:play-services-measurement-sdk@@17.0.1 */
public final class a9 implements Runnable {
    public final /* synthetic */ fb b;
    public final /* synthetic */ AppMeasurementDynamiteService c;

    public a9(AppMeasurementDynamiteService appMeasurementDynamiteService, fb fbVar) {
        this.c = appMeasurementDynamiteService;
        this.b = fbVar;
    }

    public final void run() {
        this.c.a.n().a(this.b, this.c.a.b());
    }
}
