package j.c.a.a.g.a;

import i.b.k.ResourcesFlusher;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class a0 implements Runnable {
    public final /* synthetic */ String b;
    public final /* synthetic */ long c;
    public final /* synthetic */ a d;

    public a0(a aVar, String str, long j2) {
        this.d = aVar;
        this.b = str;
        this.c = j2;
    }

    public final void run() {
        a aVar = this.d;
        String str = this.b;
        long j2 = this.c;
        aVar.b();
        aVar.d();
        ResourcesFlusher.b(str);
        Integer num = aVar.c.get(str);
        if (num != null) {
            w6 z = aVar.s().z();
            int intValue = num.intValue() - 1;
            if (intValue == 0) {
                aVar.c.remove(str);
                Long l2 = aVar.b.get(str);
                if (l2 == null) {
                    aVar.a().f2046f.a("First ad unit exposure time was never set");
                } else {
                    aVar.b.remove(str);
                    aVar.a(str, j2 - l2.longValue(), z);
                }
                if (aVar.c.isEmpty()) {
                    long j3 = aVar.d;
                    if (j3 == 0) {
                        aVar.a().f2046f.a("First ad exposure time was never set");
                        return;
                    }
                    aVar.a(j2 - j3, z);
                    aVar.d = 0;
                    return;
                }
                return;
            }
            aVar.c.put(str, Integer.valueOf(intValue));
            return;
        }
        aVar.a().f2046f.a("Call to endAdUnitExposure for unknown ad unit id", str);
    }
}
