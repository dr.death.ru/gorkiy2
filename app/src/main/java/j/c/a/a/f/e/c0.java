package j.c.a.a.f.e;

import j.c.a.a.f.e.x3;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class c0 extends x3<c0, b> implements g5 {
    public static final c0 zzi;
    public static volatile m5<c0> zzj;
    public int zzc;
    public int zzd;
    public boolean zze;
    public String zzf = "";
    public String zzg = "";
    public String zzh = "";

    /* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
    public enum a implements b4 {
        UNKNOWN_COMPARISON_TYPE(0),
        LESS_THAN(1),
        GREATER_THAN(2),
        EQUAL(3),
        BETWEEN(4);
        
        public static final a4<a> zzf = new h0();
        public final int zzg;

        /* access modifiers changed from: public */
        a(int i2) {
            this.zzg = i2;
        }

        public static d4 b() {
            return g0.a;
        }

        public final int a() {
            return this.zzg;
        }

        public final String toString() {
            return "<" + a.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzg + " name=" + name() + '>';
        }

        public static a a(int i2) {
            if (i2 == 0) {
                return UNKNOWN_COMPARISON_TYPE;
            }
            if (i2 == 1) {
                return LESS_THAN;
            }
            if (i2 == 2) {
                return GREATER_THAN;
            }
            if (i2 == 3) {
                return EQUAL;
            }
            if (i2 != 4) {
                return null;
            }
            return BETWEEN;
        }
    }

    /* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
    public static final class b extends x3.a<c0, b> implements g5 {
        public /* synthetic */ b(f0 f0Var) {
            super(c0.zzi);
        }
    }

    static {
        c0 c0Var = new c0();
        zzi = c0Var;
        x3.zzd.put(c0.class, c0Var);
    }

    public final Object a(int i2, Object obj, Object obj2) {
        switch (f0.a[i2 - 1]) {
            case 1:
                return new c0();
            case 2:
                return new b(null);
            case 3:
                return new r5(zzi, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0000\u0000\u0001\f\u0000\u0002\u0007\u0001\u0003\b\u0002\u0004\b\u0003\u0005\b\u0004", new Object[]{"zzc", "zzd", a.b(), "zze", "zzf", "zzg", "zzh"});
            case 4:
                return zzi;
            case 5:
                m5<c0> m5Var = zzj;
                if (m5Var == null) {
                    synchronized (c0.class) {
                        m5Var = zzj;
                        if (m5Var == null) {
                            m5Var = new x3.c<>(zzi);
                            zzj = m5Var;
                        }
                    }
                }
                return m5Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final a i() {
        a a2 = a.a(this.zzd);
        return a2 == null ? a.UNKNOWN_COMPARISON_TYPE : a2;
    }
}
