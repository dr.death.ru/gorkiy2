package j.c.a.a.c.l;

import android.accounts.Account;
import android.os.IInterface;
import com.google.android.gms.common.api.Scope;
import j.c.a.a.c.k.a;
import java.util.Set;

public abstract class g<T extends IInterface> extends b<T> implements a.e {
    public final Set<Scope> w;
    public final Account x;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public g(android.content.Context r10, android.os.Looper r11, int r12, j.c.a.a.c.l.c r13, j.c.a.a.c.k.c r14, j.c.a.a.c.k.d r15) {
        /*
            r9 = this;
            j.c.a.a.c.l.h r3 = j.c.a.a.c.l.h.a(r10)
            j.c.a.a.c.e r4 = j.c.a.a.c.e.d
            i.b.k.ResourcesFlusher.b(r14)
            j.c.a.a.c.k.c r14 = (j.c.a.a.c.k.c) r14
            i.b.k.ResourcesFlusher.b(r15)
            j.c.a.a.c.k.d r15 = (j.c.a.a.c.k.d) r15
            r0 = 0
            if (r14 != 0) goto L_0x0015
            r6 = r0
            goto L_0x001b
        L_0x0015:
            j.c.a.a.c.l.s r1 = new j.c.a.a.c.l.s
            r1.<init>(r14)
            r6 = r1
        L_0x001b:
            if (r15 != 0) goto L_0x001f
            r7 = r0
            goto L_0x0025
        L_0x001f:
            j.c.a.a.c.l.t r14 = new j.c.a.a.c.l.t
            r14.<init>(r15)
            r7 = r14
        L_0x0025:
            java.lang.String r8 = r13.d
            r0 = r9
            r1 = r10
            r2 = r11
            r5 = r12
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
            android.accounts.Account r10 = r13.a
            r9.x = r10
            java.util.Set<com.google.android.gms.common.api.Scope> r10 = r13.b
            java.util.Iterator r11 = r10.iterator()
        L_0x0038:
            boolean r12 = r11.hasNext()
            if (r12 == 0) goto L_0x0053
            java.lang.Object r12 = r11.next()
            com.google.android.gms.common.api.Scope r12 = (com.google.android.gms.common.api.Scope) r12
            boolean r12 = r10.contains(r12)
            if (r12 == 0) goto L_0x004b
            goto L_0x0038
        L_0x004b:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r11 = "Expanding scopes is not permitted, use implied scopes instead"
            r10.<init>(r11)
            throw r10
        L_0x0053:
            r9.w = r10
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.c.l.g.<init>(android.content.Context, android.os.Looper, int, j.c.a.a.c.l.c, j.c.a.a.c.k.c, j.c.a.a.c.k.d):void");
    }

    public final Account k() {
        return this.x;
    }

    public final Set<Scope> m() {
        return this.w;
    }
}
