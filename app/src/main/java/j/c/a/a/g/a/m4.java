package j.c.a.a.g.a;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import com.google.android.gms.internal.measurement.zzfn;
import i.b.k.ResourcesFlusher;
import i.e.ArrayMap;
import j.c.a.a.c.n.c;
import j.c.a.a.f.e.a0;
import j.c.a.a.f.e.b0;
import j.c.a.a.f.e.d0;
import j.c.a.a.f.e.j0;
import j.c.a.a.f.e.j3;
import j.c.a.a.f.e.k0;
import j.c.a.a.f.e.l0;
import j.c.a.a.f.e.x3;
import j.c.a.a.f.e.z;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class m4 extends o8 implements k9 {
    public final Map<String, Map<String, String>> d = new ArrayMap();

    /* renamed from: e  reason: collision with root package name */
    public final Map<String, Map<String, Boolean>> f2039e = new ArrayMap();

    /* renamed from: f  reason: collision with root package name */
    public final Map<String, Map<String, Boolean>> f2040f = new ArrayMap();
    public final Map<String, k0> g = new ArrayMap();
    public final Map<String, Map<String, Integer>> h = new ArrayMap();

    /* renamed from: i  reason: collision with root package name */
    public final Map<String, String> f2041i = new ArrayMap();

    public m4(q8 q8Var) {
        super(q8Var);
    }

    public final k0 a(String str) {
        o();
        d();
        ResourcesFlusher.b(str);
        f(str);
        return this.g.get(str);
    }

    public final boolean b(String str) {
        d();
        k0 a = a(str);
        if (a == null) {
            return false;
        }
        return a.zzk;
    }

    public final boolean c(String str, String str2) {
        Boolean bool;
        d();
        f(str);
        if ("ecommerce_purchase".equals(str2)) {
            return true;
        }
        Map map = this.f2040f.get(str);
        if (map == null || (bool = (Boolean) map.get(str2)) == null) {
            return false;
        }
        return bool.booleanValue();
    }

    public final int d(String str, String str2) {
        Integer num;
        d();
        f(str);
        Map map = this.h.get(str);
        if (map == null || (num = (Integer) map.get(str2)) == null) {
            return 1;
        }
        return num.intValue();
    }

    public final boolean e(String str) {
        return "1".equals(a(str, "measurement.upload.blacklist_public"));
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00d5  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void f(java.lang.String r12) {
        /*
            r11 = this;
            r11.o()
            r11.d()
            i.b.k.ResourcesFlusher.b(r12)
            java.util.Map<java.lang.String, j.c.a.a.f.e.k0> r0 = r11.g
            java.lang.Object r0 = r0.get(r12)
            if (r0 != 0) goto L_0x00da
            j.c.a.a.g.a.n9 r0 = r11.s()
            r1 = 0
            if (r0 == 0) goto L_0x00d9
            i.b.k.ResourcesFlusher.b(r12)
            r0.d()
            r0.o()
            android.database.sqlite.SQLiteDatabase r2 = r0.v()     // Catch:{ SQLiteException -> 0x0067, all -> 0x0065 }
            java.lang.String r3 = "apps"
            java.lang.String r4 = "remote_config"
            java.lang.String[] r4 = new java.lang.String[]{r4}     // Catch:{ SQLiteException -> 0x0067, all -> 0x0065 }
            java.lang.String r5 = "app_id=?"
            r6 = 1
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ SQLiteException -> 0x0067, all -> 0x0065 }
            r10 = 0
            r6[r10] = r12     // Catch:{ SQLiteException -> 0x0067, all -> 0x0065 }
            r7 = 0
            r8 = 0
            r9 = 0
            android.database.Cursor r2 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ SQLiteException -> 0x0067, all -> 0x0065 }
            boolean r3 = r2.moveToFirst()     // Catch:{ SQLiteException -> 0x0063 }
            if (r3 != 0) goto L_0x0046
            r2.close()
            goto L_0x007e
        L_0x0046:
            byte[] r3 = r2.getBlob(r10)     // Catch:{ SQLiteException -> 0x0063 }
            boolean r4 = r2.moveToNext()     // Catch:{ SQLiteException -> 0x0063 }
            if (r4 == 0) goto L_0x005f
            j.c.a.a.g.a.n3 r4 = r0.a()     // Catch:{ SQLiteException -> 0x0063 }
            j.c.a.a.g.a.p3 r4 = r4.f2046f     // Catch:{ SQLiteException -> 0x0063 }
            java.lang.String r5 = "Got multiple records for app config, expected one. appId"
            java.lang.Object r6 = j.c.a.a.g.a.n3.a(r12)     // Catch:{ SQLiteException -> 0x0063 }
            r4.a(r5, r6)     // Catch:{ SQLiteException -> 0x0063 }
        L_0x005f:
            r2.close()
            goto L_0x007f
        L_0x0063:
            r3 = move-exception
            goto L_0x006a
        L_0x0065:
            r12 = move-exception
            goto L_0x00d3
        L_0x0067:
            r2 = move-exception
            r3 = r2
            r2 = r1
        L_0x006a:
            j.c.a.a.g.a.n3 r0 = r0.a()     // Catch:{ all -> 0x00d1 }
            j.c.a.a.g.a.p3 r0 = r0.f2046f     // Catch:{ all -> 0x00d1 }
            java.lang.String r4 = "Error querying remote config. appId"
            java.lang.Object r5 = j.c.a.a.g.a.n3.a(r12)     // Catch:{ all -> 0x00d1 }
            r0.a(r4, r5, r3)     // Catch:{ all -> 0x00d1 }
            if (r2 == 0) goto L_0x007e
            r2.close()
        L_0x007e:
            r3 = r1
        L_0x007f:
            if (r3 != 0) goto L_0x00a0
            java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.String>> r0 = r11.d
            r0.put(r12, r1)
            java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.Boolean>> r0 = r11.f2039e
            r0.put(r12, r1)
            java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.Boolean>> r0 = r11.f2040f
            r0.put(r12, r1)
            java.util.Map<java.lang.String, j.c.a.a.f.e.k0> r0 = r11.g
            r0.put(r12, r1)
            java.util.Map<java.lang.String, java.lang.String> r0 = r11.f2041i
            r0.put(r12, r1)
            java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.Integer>> r0 = r11.h
            r0.put(r12, r1)
            return
        L_0x00a0:
            j.c.a.a.f.e.k0 r0 = r11.a(r12, r3)
            j.c.a.a.f.e.x3$a r0 = r0.h()
            j.c.a.a.f.e.k0$a r0 = (j.c.a.a.f.e.k0.a) r0
            r11.a(r12, r0)
            java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.String>> r2 = r11.d
            j.c.a.a.f.e.f5 r3 = r0.m()
            j.c.a.a.f.e.x3 r3 = (j.c.a.a.f.e.x3) r3
            j.c.a.a.f.e.k0 r3 = (j.c.a.a.f.e.k0) r3
            java.util.Map r3 = a(r3)
            r2.put(r12, r3)
            java.util.Map<java.lang.String, j.c.a.a.f.e.k0> r2 = r11.g
            j.c.a.a.f.e.f5 r0 = r0.m()
            j.c.a.a.f.e.x3 r0 = (j.c.a.a.f.e.x3) r0
            j.c.a.a.f.e.k0 r0 = (j.c.a.a.f.e.k0) r0
            r2.put(r12, r0)
            java.util.Map<java.lang.String, java.lang.String> r0 = r11.f2041i
            r0.put(r12, r1)
            goto L_0x00da
        L_0x00d1:
            r12 = move-exception
            r1 = r2
        L_0x00d3:
            if (r1 == 0) goto L_0x00d8
            r1.close()
        L_0x00d8:
            throw r12
        L_0x00d9:
            throw r1
        L_0x00da:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.m4.f(java.lang.String):void");
    }

    public final boolean q() {
        return false;
    }

    public final boolean b(String str, String str2) {
        Boolean bool;
        d();
        f(str);
        if ("1".equals(a(str, "measurement.upload.blacklist_internal")) && y8.f(str2)) {
            return true;
        }
        if ("1".equals(a(str, "measurement.upload.blacklist_public")) && y8.e(str2)) {
            return true;
        }
        Map map = this.f2039e.get(str);
        if (map == null || (bool = (Boolean) map.get(str2)) == null) {
            return false;
        }
        return bool.booleanValue();
    }

    public final k0 a(String str, byte[] bArr) {
        if (bArr == null) {
            return k0.zzl;
        }
        try {
            k0 k0Var = (k0) x3.a(k0.zzl, bArr, j3.b());
            p3 p3Var = a().f2052n;
            boolean z = true;
            if ((k0Var.zzc & 1) == 0) {
                z = false;
            }
            p3Var.a("Parsed config. version, gmp_app_id", z ? Long.valueOf(k0Var.zzd) : null, k0Var.zze);
            return k0Var;
        } catch (zzfn e2) {
            a().f2047i.a("Unable to merge remote config. appId", n3.a(str), e2);
            return k0.zzl;
        } catch (RuntimeException e3) {
            a().f2047i.a("Unable to merge remote config. appId", n3.a(str), e3);
            return k0.zzl;
        }
    }

    public final boolean d(String str) {
        return "1".equals(a(str, "measurement.upload.blacklist_internal"));
    }

    public final long c(String str) {
        String a = a(str, "measurement.account.time_zone_offset_minutes");
        if (TextUtils.isEmpty(a)) {
            return 0;
        }
        try {
            return Long.parseLong(a);
        } catch (NumberFormatException e2) {
            a().f2047i.a("Unable to parse timezone offset. appId", n3.a(str), e2);
            return 0;
        }
    }

    public final String a(String str, String str2) {
        d();
        f(str);
        Map map = this.d.get(str);
        if (map != null) {
            return (String) map.get(str2);
        }
        return null;
    }

    public static Map<String, String> a(k0 k0Var) {
        ArrayMap arrayMap = new ArrayMap();
        if (k0Var != null) {
            for (l0 next : k0Var.zzg) {
                arrayMap.put(next.zzd, next.zze);
            }
        }
        return arrayMap;
    }

    public final void a(String str, k0.a aVar) {
        ArrayMap arrayMap = new ArrayMap();
        ArrayMap arrayMap2 = new ArrayMap();
        ArrayMap arrayMap3 = new ArrayMap();
        if (aVar != null) {
            for (int i2 = 0; i2 < ((k0) aVar.c).zzh.size(); i2++) {
                j0.a aVar2 = (j0.a) ((k0) aVar.c).zzh.get(i2).h();
                if (TextUtils.isEmpty(aVar2.a())) {
                    a().f2047i.a("EventConfig contained null event name");
                } else {
                    String a = c.a(aVar2.a(), r5.a, r5.b);
                    if (!TextUtils.isEmpty(a)) {
                        aVar2.i();
                        j0.a((j0) aVar2.c, a);
                        aVar.i();
                        k0 k0Var = (k0) aVar.c;
                        if (!k0Var.zzh.a()) {
                            k0Var.zzh = x3.a(k0Var.zzh);
                        }
                        k0Var.zzh.set(i2, (j0) ((x3) aVar2.m()));
                    }
                    arrayMap.put(aVar2.a(), Boolean.valueOf(((j0) aVar2.c).zze));
                    arrayMap2.put(aVar2.a(), Boolean.valueOf(((j0) aVar2.c).zzf));
                    if ((((j0) aVar2.c).zzc & 8) != 0) {
                        if (aVar2.n() < 2 || aVar2.n() > 65535) {
                            a().f2047i.a("Invalid sampling rate. Event name, sample rate", aVar2.a(), Integer.valueOf(aVar2.n()));
                        } else {
                            arrayMap3.put(aVar2.a(), Integer.valueOf(aVar2.n()));
                        }
                    }
                }
            }
        }
        this.f2039e.put(str, arrayMap);
        this.f2040f.put(str, arrayMap2);
        this.h.put(str, arrayMap3);
    }

    public final boolean a(String str, byte[] bArr, String str2) {
        byte[] bArr2;
        boolean z;
        boolean z2;
        String str3 = str;
        o();
        d();
        ResourcesFlusher.b(str);
        k0.a aVar = (k0.a) a(str, bArr).h();
        if (aVar == null) {
            return false;
        }
        a(str3, aVar);
        this.g.put(str3, (k0) ((x3) aVar.m()));
        this.f2041i.put(str3, str2);
        this.d.put(str3, a((k0) ((x3) aVar.m())));
        f9 n2 = n();
        z[] zVarArr = (z[]) Collections.unmodifiableList(((k0) aVar.c).zzi).toArray(new z[0]);
        ResourcesFlusher.b(zVarArr);
        for (int i2 = 0; i2 < zVarArr.length; i2++) {
            z.a aVar2 = (z.a) zVarArr[i2].h();
            if (((z) aVar2.c).zzf.size() != 0) {
                for (int i3 = 0; i3 < ((z) aVar2.c).zzf.size(); i3++) {
                    a0.a aVar3 = (a0.a) ((z) aVar2.c).zzf.get(i3).h();
                    a0.a aVar4 = (a0.a) ((x3.a) aVar3.clone());
                    String a = c.a(((a0) aVar3.c).zze, r5.a, r5.b);
                    if (a != null) {
                        aVar4.i();
                        a0.a((a0) aVar4.c, a);
                        z2 = true;
                    } else {
                        z2 = false;
                    }
                    for (int i4 = 0; i4 < ((a0) aVar3.c).zzf.size(); i4++) {
                        b0 b0Var = ((a0) aVar3.c).zzf.get(i4);
                        String a2 = c.a(b0Var.zzg, q5.a, q5.b);
                        if (a2 != null) {
                            b0.a aVar5 = (b0.a) b0Var.h();
                            aVar5.i();
                            b0.a((b0) aVar5.c, a2);
                            aVar4.i();
                            a0.a((a0) aVar4.c, i4, (b0) ((x3) aVar5.m()));
                            z2 = true;
                        }
                    }
                    if (z2) {
                        aVar2.i();
                        z zVar = (z) aVar2.c;
                        if (!zVar.zzf.a()) {
                            zVar.zzf = x3.a(zVar.zzf);
                        }
                        zVar.zzf.set(i3, (a0) ((x3) aVar4.m()));
                        zVarArr[i2] = (z) ((x3) aVar2.m());
                    }
                }
            }
            if (((z) aVar2.c).zze.size() != 0) {
                for (int i5 = 0; i5 < ((z) aVar2.c).zze.size(); i5++) {
                    d0 d0Var = ((z) aVar2.c).zze.get(i5);
                    String a3 = c.a(d0Var.zze, t5.a, t5.b);
                    if (a3 != null) {
                        d0.a aVar6 = (d0.a) d0Var.h();
                        aVar6.i();
                        d0.a((d0) aVar6.c, a3);
                        aVar2.i();
                        z zVar2 = (z) aVar2.c;
                        if (!zVar2.zze.a()) {
                            zVar2.zze = x3.a(zVar2.zze);
                        }
                        zVar2.zze.set(i5, (d0) ((x3) aVar6.m()));
                        zVarArr[i2] = (z) ((x3) aVar2.m());
                    }
                }
            }
        }
        n9 s2 = super.s();
        super.o();
        s2.d();
        ResourcesFlusher.b(str);
        ResourcesFlusher.b(zVarArr);
        SQLiteDatabase v = s2.v();
        v.beginTransaction();
        try {
            super.o();
            s2.d();
            ResourcesFlusher.b(str);
            SQLiteDatabase v2 = s2.v();
            v2.delete("property_filters", "app_id=?", new String[]{str3});
            v2.delete("event_filters", "app_id=?", new String[]{str3});
            for (z zVar3 : zVarArr) {
                super.o();
                s2.d();
                ResourcesFlusher.b(str);
                ResourcesFlusher.b(zVar3);
                if (!((zVar3.zzc & 1) != 0)) {
                    s2.a().f2047i.a("Audience with no ID. appId", n3.a(str));
                } else {
                    int i6 = zVar3.zzd;
                    Iterator<a0> it = zVar3.zzf.iterator();
                    while (true) {
                        if (it.hasNext()) {
                            if (!it.next().a()) {
                                s2.a().f2047i.a("Event filter with no ID. Audience definition ignored. appId, audienceId", n3.a(str), Integer.valueOf(i6));
                                break;
                            }
                        } else {
                            Iterator<d0> it2 = zVar3.zze.iterator();
                            while (true) {
                                if (it2.hasNext()) {
                                    if (!it2.next().a()) {
                                        s2.a().f2047i.a("Property filter with no ID. Audience definition ignored. appId, audienceId", n3.a(str), Integer.valueOf(i6));
                                        break;
                                    }
                                } else {
                                    Iterator<a0> it3 = zVar3.zzf.iterator();
                                    while (true) {
                                        if (it3.hasNext()) {
                                            if (!s2.a(str3, i6, it3.next())) {
                                                z = false;
                                                break;
                                            }
                                        } else {
                                            z = true;
                                            break;
                                        }
                                    }
                                    if (z) {
                                        Iterator<d0> it4 = zVar3.zze.iterator();
                                        while (true) {
                                            if (it4.hasNext()) {
                                                if (!s2.a(str3, i6, it4.next())) {
                                                    z = false;
                                                    break;
                                                }
                                            } else {
                                                break;
                                            }
                                        }
                                    }
                                    if (!z) {
                                        super.o();
                                        s2.d();
                                        ResourcesFlusher.b(str);
                                        SQLiteDatabase v3 = s2.v();
                                        v3.delete("property_filters", "app_id=? and audience_id=?", new String[]{str3, String.valueOf(i6)});
                                        v3.delete("event_filters", "app_id=? and audience_id=?", new String[]{str3, String.valueOf(i6)});
                                    }
                                }
                            }
                        }
                    }
                }
            }
            ArrayList arrayList = new ArrayList();
            for (z zVar4 : zVarArr) {
                arrayList.add((zVar4.zzc & 1) != 0 ? Integer.valueOf(zVar4.zzd) : null);
            }
            s2.b(str3, arrayList);
            v.setTransactionSuccessful();
            try {
                aVar.i();
                k0.a((k0) aVar.c);
                bArr2 = ((k0) ((x3) aVar.m())).f();
            } catch (RuntimeException e2) {
                a().f2047i.a("Unable to serialize reduced-size config. Storing full config instead. appId", n3.a(str), e2);
                bArr2 = bArr;
            }
            n9 s3 = s();
            ResourcesFlusher.b(str);
            s3.d();
            super.o();
            ContentValues contentValues = new ContentValues();
            contentValues.put("remote_config", bArr2);
            try {
                if (((long) s3.v().update("apps", contentValues, "app_id = ?", new String[]{str3})) == 0) {
                    s3.a().f2046f.a("Failed to update remote config (got 0). appId", n3.a(str));
                }
            } catch (SQLiteException e3) {
                s3.a().f2046f.a("Error storing remote config. appId", n3.a(str), e3);
            }
            this.g.put(str3, (k0) ((x3) aVar.m()));
            return true;
        } finally {
            v.endTransaction();
        }
    }
}
