package j.c.a.a.g.a;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class x6 implements Runnable {
    public final /* synthetic */ boolean b;
    public final /* synthetic */ w6 c;
    public final /* synthetic */ w6 d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ y6 f2127e;

    public x6(y6 y6Var, boolean z, w6 w6Var, w6 w6Var2) {
        this.f2127e = y6Var;
        this.b = z;
        this.c = w6Var;
        this.d = w6Var2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.y6.a(j.c.a.a.g.a.y6, j.c.a.a.g.a.w6, boolean):void
     arg types: [j.c.a.a.g.a.y6, j.c.a.a.g.a.w6, int]
     candidates:
      j.c.a.a.g.a.y6.a(j.c.a.a.g.a.w6, android.os.Bundle, boolean):void
      j.c.a.a.g.a.y6.a(android.app.Activity, j.c.a.a.g.a.w6, boolean):void
      j.c.a.a.g.a.y6.a(android.app.Activity, java.lang.String, java.lang.String):void
      j.c.a.a.g.a.y6.a(j.c.a.a.g.a.y6, j.c.a.a.g.a.w6, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.y6.a(j.c.a.a.g.a.w6, android.os.Bundle, boolean):void
     arg types: [j.c.a.a.g.a.w6, android.os.Bundle, int]
     candidates:
      j.c.a.a.g.a.y6.a(j.c.a.a.g.a.y6, j.c.a.a.g.a.w6, boolean):void
      j.c.a.a.g.a.y6.a(android.app.Activity, j.c.a.a.g.a.w6, boolean):void
      j.c.a.a.g.a.y6.a(android.app.Activity, java.lang.String, java.lang.String):void
      j.c.a.a.g.a.y6.a(j.c.a.a.g.a.w6, android.os.Bundle, boolean):void */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0060, code lost:
        if (j.c.a.a.g.a.y8.d(r10.c.a, r10.d.a) != false) goto L_0x0063;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r10 = this;
            j.c.a.a.g.a.y6 r0 = r10.f2127e
            j.c.a.a.g.a.r4 r1 = r0.a
            j.c.a.a.g.a.i9 r1 = r1.g
            j.c.a.a.g.a.g3 r0 = r0.q()
            r0.w()
            java.lang.String r0 = r0.c
            boolean r0 = r1.h(r0)
            r1 = 1
            r2 = 0
            if (r0 == 0) goto L_0x002e
            boolean r0 = r10.b
            if (r0 == 0) goto L_0x0023
            j.c.a.a.g.a.y6 r0 = r10.f2127e
            j.c.a.a.g.a.w6 r0 = r0.c
            if (r0 == 0) goto L_0x0023
            r0 = 1
            goto L_0x0024
        L_0x0023:
            r0 = 0
        L_0x0024:
            if (r0 == 0) goto L_0x003c
            j.c.a.a.g.a.y6 r3 = r10.f2127e
            j.c.a.a.g.a.w6 r4 = r3.c
            j.c.a.a.g.a.y6.a(r3, r4, r1)
            goto L_0x003c
        L_0x002e:
            boolean r0 = r10.b
            if (r0 == 0) goto L_0x003b
            j.c.a.a.g.a.y6 r0 = r10.f2127e
            j.c.a.a.g.a.w6 r3 = r0.c
            if (r3 == 0) goto L_0x003b
            j.c.a.a.g.a.y6.a(r0, r3, r1)
        L_0x003b:
            r0 = 0
        L_0x003c:
            j.c.a.a.g.a.w6 r3 = r10.c
            if (r3 == 0) goto L_0x0062
            long r4 = r3.c
            j.c.a.a.g.a.w6 r6 = r10.d
            long r7 = r6.c
            int r9 = (r4 > r7 ? 1 : (r4 == r7 ? 0 : -1))
            if (r9 != 0) goto L_0x0062
            java.lang.String r3 = r3.b
            java.lang.String r4 = r6.b
            boolean r3 = j.c.a.a.g.a.y8.d(r3, r4)
            if (r3 == 0) goto L_0x0062
            j.c.a.a.g.a.w6 r3 = r10.c
            java.lang.String r3 = r3.a
            j.c.a.a.g.a.w6 r4 = r10.d
            java.lang.String r4 = r4.a
            boolean r3 = j.c.a.a.g.a.y8.d(r3, r4)
            if (r3 != 0) goto L_0x0063
        L_0x0062:
            r2 = 1
        L_0x0063:
            if (r2 == 0) goto L_0x00cb
            android.os.Bundle r2 = new android.os.Bundle
            r2.<init>()
            j.c.a.a.g.a.w6 r3 = r10.d
            j.c.a.a.g.a.y6.a(r3, r2, r1)
            j.c.a.a.g.a.w6 r1 = r10.c
            if (r1 == 0) goto L_0x008e
            java.lang.String r1 = r1.a
            if (r1 == 0) goto L_0x007c
            java.lang.String r3 = "_pn"
            r2.putString(r3, r1)
        L_0x007c:
            j.c.a.a.g.a.w6 r1 = r10.c
            java.lang.String r1 = r1.b
            java.lang.String r3 = "_pc"
            r2.putString(r3, r1)
            j.c.a.a.g.a.w6 r1 = r10.c
            long r3 = r1.c
            java.lang.String r1 = "_pi"
            r2.putLong(r1, r3)
        L_0x008e:
            j.c.a.a.g.a.y6 r1 = r10.f2127e
            j.c.a.a.g.a.r4 r3 = r1.a
            j.c.a.a.g.a.i9 r3 = r3.g
            j.c.a.a.g.a.g3 r1 = r1.q()
            r1.w()
            java.lang.String r1 = r1.c
            boolean r1 = r3.h(r1)
            if (r1 == 0) goto L_0x00be
            if (r0 == 0) goto L_0x00be
            j.c.a.a.g.a.y6 r0 = r10.f2127e
            j.c.a.a.g.a.f8 r0 = r0.u()
            long r0 = r0.z()
            r3 = 0
            int r5 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r5 <= 0) goto L_0x00be
            j.c.a.a.g.a.y6 r3 = r10.f2127e
            j.c.a.a.g.a.y8 r3 = r3.k()
            r3.a(r2, r0)
        L_0x00be:
            j.c.a.a.g.a.y6 r0 = r10.f2127e
            j.c.a.a.g.a.x5 r0 = r0.p()
            java.lang.String r1 = "auto"
            java.lang.String r3 = "_vs"
            r0.b(r1, r3, r2)
        L_0x00cb:
            j.c.a.a.g.a.y6 r0 = r10.f2127e
            j.c.a.a.g.a.w6 r1 = r10.d
            r0.c = r1
            j.c.a.a.g.a.z6 r0 = r0.r()
            j.c.a.a.g.a.w6 r1 = r10.d
            r0.d()
            r0.w()
            j.c.a.a.g.a.h7 r2 = new j.c.a.a.g.a.h7
            r2.<init>(r0, r1)
            r0.a(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.x6.run():void");
    }
}
