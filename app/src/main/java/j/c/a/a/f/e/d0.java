package j.c.a.a.f.e;

import j.c.a.a.f.e.x3;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class d0 extends x3<d0, a> implements g5 {
    public static final d0 zzj;
    public static volatile m5<d0> zzk;
    public int zzc;
    public int zzd;
    public String zze = "";
    public b0 zzf;
    public boolean zzg;
    public boolean zzh;
    public boolean zzi;

    static {
        d0 d0Var = new d0();
        zzj = d0Var;
        x3.zzd.put(d0.class, d0Var);
    }

    public final boolean a() {
        return (this.zzc & 1) != 0;
    }

    public final int i() {
        return this.zzd;
    }

    public final String j() {
        return this.zze;
    }

    public final boolean m() {
        return this.zzg;
    }

    public final boolean n() {
        return this.zzh;
    }

    public final boolean o() {
        return this.zzi;
    }

    /* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
    public static final class a extends x3.a<d0, a> implements g5 {
        public a() {
            super(d0.zzj);
        }

        public /* synthetic */ a(f0 f0Var) {
            super(d0.zzj);
        }
    }

    public static /* synthetic */ void a(d0 d0Var, String str) {
        if (str != null) {
            d0Var.zzc |= 2;
            d0Var.zze = str;
            return;
        }
        throw null;
    }

    public final Object a(int i2, Object obj, Object obj2) {
        switch (f0.a[i2 - 1]) {
            case 1:
                return new d0();
            case 2:
                return new a(null);
            case 3:
                return new r5(zzj, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001\u0004\u0000\u0002\b\u0001\u0003\t\u0002\u0004\u0007\u0003\u0005\u0007\u0004\u0006\u0007\u0005", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg", "zzh", "zzi"});
            case 4:
                return zzj;
            case 5:
                m5<d0> m5Var = zzk;
                if (m5Var == null) {
                    synchronized (d0.class) {
                        m5Var = zzk;
                        if (m5Var == null) {
                            m5Var = new x3.c<>(zzj);
                            zzk = m5Var;
                        }
                    }
                }
                return m5Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
