package j.c.a.a.f.e;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import i.b.k.ResourcesFlusher;

/* compiled from: com.google.android.gms:play-services-measurement-base@@17.0.1 */
public final class mb implements Parcelable.Creator<nb> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = ResourcesFlusher.b(parcel);
        long j2 = 0;
        long j3 = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        Bundle bundle = null;
        boolean z = false;
        while (parcel.dataPosition() < b) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    j2 = ResourcesFlusher.g(parcel2, readInt);
                    break;
                case 2:
                    j3 = ResourcesFlusher.g(parcel2, readInt);
                    break;
                case 3:
                    z = ResourcesFlusher.d(parcel2, readInt);
                    break;
                case 4:
                    str = ResourcesFlusher.b(parcel2, readInt);
                    break;
                case 5:
                    str2 = ResourcesFlusher.b(parcel2, readInt);
                    break;
                case 6:
                    str3 = ResourcesFlusher.b(parcel2, readInt);
                    break;
                case 7:
                    bundle = ResourcesFlusher.a(parcel2, readInt);
                    break;
                default:
                    ResourcesFlusher.i(parcel2, readInt);
                    break;
            }
        }
        ResourcesFlusher.c(parcel2, b);
        return new nb(j2, j3, z, str, str2, str3, bundle);
    }

    public final /* synthetic */ Object[] newArray(int i2) {
        return new nb[i2];
    }
}
