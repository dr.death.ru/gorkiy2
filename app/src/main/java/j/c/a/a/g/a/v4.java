package j.c.a.a.g.a;

import i.b.k.ResourcesFlusher;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class v4 implements Runnable {
    public final /* synthetic */ d9 b;
    public final /* synthetic */ s4 c;

    public v4(s4 s4Var, d9 d9Var) {
        this.c = s4Var;
        this.b = d9Var;
    }

    public final void run() {
        this.c.a.o();
        q8 q8Var = this.c.a;
        d9 d9Var = this.b;
        q8Var.r();
        q8Var.m();
        ResourcesFlusher.b(d9Var.b);
        q8Var.b(d9Var);
    }
}
