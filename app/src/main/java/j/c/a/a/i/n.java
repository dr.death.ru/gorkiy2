package j.c.a.a.i;

import java.util.concurrent.Executor;
import javax.annotation.concurrent.GuardedBy;

public final class n<TResult> implements v<TResult> {
    public final Executor a;
    public final Object b = new Object();
    @GuardedBy("mLock")
    public b c;

    public n(Executor executor, b bVar) {
        this.a = executor;
        this.c = bVar;
    }

    public final void a(e eVar) {
        if (((y) eVar).d) {
            synchronized (this.b) {
                if (this.c != null) {
                    this.a.execute(new o(this));
                }
            }
        }
    }
}
