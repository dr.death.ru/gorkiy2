package j.c.a.a.f.d;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

public final class g extends WeakReference<Throwable> {
    public final int a;

    public g(Throwable th, ReferenceQueue<Throwable> referenceQueue) {
        super(th, referenceQueue);
        if (th != null) {
            this.a = System.identityHashCode(th);
            return;
        }
        throw new NullPointerException("The referent cannot be null");
    }

    public final boolean equals(Object obj) {
        if (obj != null && obj.getClass() == g.class) {
            if (this == obj) {
                return true;
            }
            g gVar = (g) obj;
            return this.a == gVar.a && get() == super.get();
        }
    }

    public final int hashCode() {
        return this.a;
    }
}
