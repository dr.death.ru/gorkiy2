package j.c.a.a.c.l;

import android.os.IBinder;
import android.os.Parcel;
import j.c.a.a.d.a;
import j.c.a.a.f.c.a;

public final class c0 extends a implements b0 {
    public c0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.ICertData");
    }

    public final j.c.a.a.d.a b() {
        Parcel a = a(1, a());
        j.c.a.a.d.a a2 = a.C0025a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    public final int c() {
        Parcel a = a(2, a());
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }
}
