package j.c.a.a.f.e;

import j.c.a.a.f.e.x3;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class k0 extends x3<k0, a> implements g5 {
    public static final k0 zzl;
    public static volatile m5<k0> zzm;
    public int zzc;
    public long zzd;
    public String zze = "";
    public int zzf;
    public e4<l0> zzg;
    public e4<j0> zzh;
    public e4<z> zzi;
    public String zzj;
    public boolean zzk;

    static {
        k0 k0Var = new k0();
        zzl = k0Var;
        x3.zzd.put(k0.class, k0Var);
    }

    public k0() {
        s5<Object> s5Var = s5.f1905e;
        this.zzg = s5Var;
        this.zzh = s5Var;
        this.zzi = s5Var;
        this.zzj = "";
    }

    public static /* synthetic */ void a(k0 k0Var) {
        if (k0Var != null) {
            k0Var.zzi = s5.f1905e;
            return;
        }
        throw null;
    }

    public final long i() {
        return this.zzd;
    }

    /* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
    public static final class a extends x3.a<k0, a> implements g5 {
        public a() {
            super(k0.zzl);
        }

        public /* synthetic */ a(n0 n0Var) {
            super(k0.zzl);
        }
    }

    public final boolean a() {
        return (this.zzc & 1) != 0;
    }

    public final Object a(int i2, Object obj, Object obj2) {
        switch (n0.a[i2 - 1]) {
            case 1:
                return new k0();
            case 2:
                return new a(null);
            case 3:
                return new r5(zzl, "\u0001\b\u0000\u0001\u0001\b\b\u0000\u0003\u0000\u0001\u0002\u0000\u0002\b\u0001\u0003\u0004\u0002\u0004\u001b\u0005\u001b\u0006\u001b\u0007\b\u0003\b\u0007\u0004", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg", l0.class, "zzh", j0.class, "zzi", z.class, "zzj", "zzk"});
            case 4:
                return zzl;
            case 5:
                m5<k0> m5Var = zzm;
                if (m5Var == null) {
                    synchronized (k0.class) {
                        m5Var = zzm;
                        if (m5Var == null) {
                            m5Var = new x3.c<>(zzl);
                            zzm = m5Var;
                        }
                    }
                }
                return m5Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
