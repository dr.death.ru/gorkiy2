package j.c.a.a.c.k.e;

import android.content.Context;
import android.os.Handler;
import com.google.android.gms.common.api.Scope;
import j.c.a.a.c.k.a;
import j.c.a.a.c.k.c;
import j.c.a.a.c.k.d;
import j.c.a.a.h.b.b;
import j.c.a.a.h.f;
import java.util.Set;

public final class t extends b implements c, d {
    public static a.C0018a<? extends f, j.c.a.a.h.a> h = j.c.a.a.h.c.c;
    public final Context a;
    public final Handler b;
    public final a.C0018a<? extends f, j.c.a.a.h.a> c;
    public Set<Scope> d;

    /* renamed from: e  reason: collision with root package name */
    public j.c.a.a.c.l.c f1798e;

    /* renamed from: f  reason: collision with root package name */
    public f f1799f;
    public v g;
}
