package j.c.e;

import j.c.e.ByteString;
import java.util.NoSuchElementException;

/* compiled from: ByteString */
public class ByteString0 implements ByteString.d {
    public int b = 0;
    public final int c = this.d.size();
    public final /* synthetic */ ByteString d;

    public ByteString0(ByteString byteString) {
        this.d = byteString;
    }

    public boolean hasNext() {
        return this.b < this.c;
    }

    public Object next() {
        try {
            ByteString byteString = this.d;
            int i2 = this.b;
            this.b = i2 + 1;
            return Byte.valueOf(byteString.c(i2));
        } catch (IndexOutOfBoundsException e2) {
            throw new NoSuchElementException(e2.getMessage());
        }
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }
}
