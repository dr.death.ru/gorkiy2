package j.c.e;

public class LazyFieldLite {
    public ByteString a;
    public volatile MessageLite b;

    static {
        ExtensionRegistryLite.a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.Parser.a(j.c.e.f, j.c.e.i):MessageType
     arg types: [j.c.e.ByteString, ?[OBJECT, ARRAY]]
     candidates:
      j.c.e.Parser.a(j.c.e.g, j.c.e.i):MessageType
      j.c.e.Parser.a(j.c.e.f, j.c.e.i):MessageType */
    /* JADX WARNING: Can't wrap try/catch for region: R(2:14|15) */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r3.b = r4;
        r4 = j.c.e.ByteString.c;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:14:0x0025 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(j.c.e.MessageLite r4) {
        /*
            r3 = this;
            j.c.e.MessageLite r0 = r3.b
            if (r0 == 0) goto L_0x0005
            return
        L_0x0005:
            monitor-enter(r3)
            j.c.e.MessageLite r0 = r3.b     // Catch:{ all -> 0x002b }
            if (r0 == 0) goto L_0x000c
            monitor-exit(r3)     // Catch:{ all -> 0x002b }
            return
        L_0x000c:
            j.c.e.ByteString r0 = r3.a     // Catch:{ InvalidProtocolBufferException -> 0x0025 }
            if (r0 == 0) goto L_0x0020
            j.c.e.Parser r0 = r4.i()     // Catch:{ InvalidProtocolBufferException -> 0x0025 }
            j.c.e.ByteString r1 = r3.a     // Catch:{ InvalidProtocolBufferException -> 0x0025 }
            r2 = 0
            java.lang.Object r0 = r0.a(r1, r2)     // Catch:{ InvalidProtocolBufferException -> 0x0025 }
            j.c.e.MessageLite r0 = (j.c.e.MessageLite) r0     // Catch:{ InvalidProtocolBufferException -> 0x0025 }
            r3.b = r0     // Catch:{ InvalidProtocolBufferException -> 0x0025 }
            goto L_0x0029
        L_0x0020:
            r3.b = r4     // Catch:{ InvalidProtocolBufferException -> 0x0025 }
            j.c.e.ByteString r4 = j.c.e.ByteString.c     // Catch:{ InvalidProtocolBufferException -> 0x0025 }
            goto L_0x0029
        L_0x0025:
            r3.b = r4     // Catch:{ all -> 0x002b }
            j.c.e.ByteString r4 = j.c.e.ByteString.c     // Catch:{ all -> 0x002b }
        L_0x0029:
            monitor-exit(r3)     // Catch:{ all -> 0x002b }
            return
        L_0x002b:
            r4 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x002b }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.e.LazyFieldLite.a(j.c.e.MessageLite):void");
    }
}
