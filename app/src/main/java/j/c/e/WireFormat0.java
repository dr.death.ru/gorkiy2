package j.c.e;

/* compiled from: WireFormat */
public enum WireFormat0 {
    DOUBLE(WireFormat1.DOUBLE, 1),
    FLOAT(WireFormat1.FLOAT, 5),
    INT64(WireFormat1.LONG, 0),
    UINT64(WireFormat1.LONG, 0),
    INT32(WireFormat1.INT, 0),
    FIXED64(WireFormat1.LONG, 1),
    FIXED32(WireFormat1.INT, 5),
    BOOL(WireFormat1.BOOLEAN, 0),
    STRING(WireFormat1.STRING, 2) {
    },
    GROUP(WireFormat1.MESSAGE, 3) {
    },
    MESSAGE(WireFormat1.MESSAGE, 2) {
    },
    BYTES(WireFormat1.BYTE_STRING, 2) {
    },
    UINT32(WireFormat1.INT, 0),
    ENUM(WireFormat1.ENUM, 0),
    SFIXED32(WireFormat1.INT, 5),
    SFIXED64(WireFormat1.LONG, 1),
    SINT32(WireFormat1.INT, 0),
    SINT64(WireFormat1.LONG, 0);
    
    public final WireFormat1 javaType;
    public final int wireType;

    /* access modifiers changed from: public */
    WireFormat0(y yVar, int i2) {
        this.javaType = yVar;
        this.wireType = i2;
    }

    /* access modifiers changed from: public */
    /* synthetic */ WireFormat0(WireFormat1 wireFormat1, int i2, WireFormat wireFormat) {
        this.javaType = wireFormat1;
        this.wireType = i2;
    }
}
