package j.c.c.d;

import android.os.Bundle;
import i.b.k.ResourcesFlusher;
import j.c.a.a.f.e.c;
import j.c.a.a.f.e.e;
import j.c.a.a.f.e.f;
import j.c.a.a.f.e.g;
import j.c.a.a.f.e.h;
import j.c.a.a.f.e.i;
import j.c.a.a.f.e.j;
import j.c.a.a.f.e.k;
import j.c.a.a.f.e.m;
import j.c.a.a.f.e.m9;
import j.c.a.a.f.e.n;
import j.c.a.a.f.e.o;
import j.c.a.a.f.e.p;
import j.c.a.a.f.e.pb;
import j.c.a.a.f.e.r;
import j.c.a.a.g.a.s6;
import j.c.a.a.g.a.v5;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/* compiled from: com.google.android.gms:play-services-measurement-api@@17.0.1 */
public final class b implements s6 {
    public final /* synthetic */ pb a;

    public b(pb pbVar) {
        this.a = pbVar;
    }

    public final Map<String, Object> a(String str, String str2, boolean z) {
        pb pbVar = this.a;
        if (pbVar != null) {
            m9 m9Var = new m9();
            pbVar.c.execute(new m(pbVar, str, str2, z, m9Var));
            Bundle b = m9Var.b(5000);
            if (b == null || b.size() == 0) {
                return Collections.emptyMap();
            }
            HashMap hashMap = new HashMap(b.size());
            for (String next : b.keySet()) {
                Object obj = b.get(next);
                if ((obj instanceof Double) || (obj instanceof Long) || (obj instanceof String)) {
                    hashMap.put(next, obj);
                }
            }
            return hashMap;
        }
        throw null;
    }

    public final void b(String str, String str2, Bundle bundle) {
        pb pbVar = this.a;
        if (pbVar != null) {
            pbVar.c.execute(new p(pbVar, null, str, str2, bundle, true, true));
            return;
        }
        throw null;
    }

    public final String c() {
        pb pbVar = this.a;
        if (pbVar != null) {
            m9 m9Var = new m9();
            pbVar.c.execute(new i(pbVar, m9Var));
            return m9Var.a(50);
        }
        throw null;
    }

    public final String d() {
        pb pbVar = this.a;
        if (pbVar != null) {
            m9 m9Var = new m9();
            pbVar.c.execute(new g(pbVar, m9Var));
            return m9Var.a(500);
        }
        throw null;
    }

    public final long e() {
        pb pbVar = this.a;
        if (pbVar != null) {
            m9 m9Var = new m9();
            pbVar.c.execute(new h(pbVar, m9Var));
            Long l2 = (Long) m9.a(m9Var.b(500), Long.class);
            if (l2 != null) {
                return l2.longValue();
            }
            long nanoTime = System.nanoTime();
            if (((j.c.a.a.c.n.b) pbVar.b) != null) {
                long nextLong = new Random(nanoTime ^ System.currentTimeMillis()).nextLong();
                int i2 = pbVar.f1900e + 1;
                pbVar.f1900e = i2;
                return nextLong + ((long) i2);
            }
            throw null;
        }
        throw null;
    }

    public final String b() {
        pb pbVar = this.a;
        if (pbVar != null) {
            m9 m9Var = new m9();
            pbVar.c.execute(new j(pbVar, m9Var));
            return m9Var.a(500);
        }
        throw null;
    }

    public final void c(String str) {
        pb pbVar = this.a;
        if (pbVar != null) {
            pbVar.c.execute(new e(pbVar, str));
            return;
        }
        throw null;
    }

    public final int b(String str) {
        pb pbVar = this.a;
        if (pbVar != null) {
            m9 m9Var = new m9();
            pbVar.c.execute(new n(pbVar, str, m9Var));
            Integer num = (Integer) m9.a(m9Var.b(10000), Integer.class);
            if (num == null) {
                return 25;
            }
            return num.intValue();
        }
        throw null;
    }

    public final void a(v5 v5Var) {
        pb pbVar = this.a;
        if (pbVar != null) {
            ResourcesFlusher.b(v5Var);
            pbVar.c.execute(new o(pbVar, v5Var));
            return;
        }
        throw null;
    }

    public final String a() {
        pb pbVar = this.a;
        if (pbVar != null) {
            m9 m9Var = new m9();
            pbVar.c.execute(new k(pbVar, m9Var));
            return m9Var.a(500);
        }
        throw null;
    }

    public final void a(String str) {
        pb pbVar = this.a;
        if (pbVar != null) {
            pbVar.c.execute(new f(pbVar, str));
            return;
        }
        throw null;
    }

    public final void a(Bundle bundle) {
        pb pbVar = this.a;
        if (pbVar != null) {
            pbVar.c.execute(new r(pbVar, bundle));
            return;
        }
        throw null;
    }

    public final void a(String str, String str2, Bundle bundle) {
        pb pbVar = this.a;
        if (pbVar != null) {
            pbVar.c.execute(new c(pbVar, str, str2, bundle));
            return;
        }
        throw null;
    }

    public final List<Bundle> a(String str, String str2) {
        pb pbVar = this.a;
        if (pbVar != null) {
            m9 m9Var = new m9();
            pbVar.c.execute(new j.c.a.a.f.e.b(pbVar, str, str2, m9Var));
            List<Bundle> list = (List) m9.a(m9Var.b(5000), List.class);
            return list == null ? Collections.emptyList() : list;
        }
        throw null;
    }
}
