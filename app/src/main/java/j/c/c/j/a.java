package j.c.c.j;

import j.a.a.a.outline;

/* compiled from: com.google.firebase:firebase-common@@17.1.0 */
public final class a extends e {
    public final String a;
    public final String b;

    public a(String str, String str2) {
        if (str != null) {
            this.a = str;
            if (str2 != null) {
                this.b = str2;
                return;
            }
            throw new NullPointerException("Null version");
        }
        throw new NullPointerException("Null libraryName");
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof e)) {
            return false;
        }
        a aVar = (a) ((e) obj);
        if (!this.a.equals(aVar.a) || !this.b.equals(aVar.b)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return ((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode();
    }

    public String toString() {
        StringBuilder a2 = outline.a("LibraryVersion{libraryName=");
        a2.append(this.a);
        a2.append(", version=");
        return outline.a(a2, this.b, "}");
    }
}
