package j.c.c.g;

import android.os.Bundle;
import android.util.Log;
import com.google.firebase.iid.zzam;
import j.c.a.a.i.f;

public abstract class m<T> {
    public final int a;
    public final f<T> b = new f<>();
    public final int c;
    public final Bundle d;

    public m(int i2, int i3, Bundle bundle) {
        this.a = i2;
        this.c = i3;
        this.d = bundle;
    }

    public final void a(zzam zzam) {
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            String valueOf = String.valueOf(this);
            String valueOf2 = String.valueOf(zzam);
            StringBuilder sb = new StringBuilder(valueOf2.length() + valueOf.length() + 14);
            sb.append("Failing ");
            sb.append(valueOf);
            sb.append(" with ");
            sb.append(valueOf2);
            Log.d("MessengerIpcClient", sb.toString());
        }
        this.b.a.a((Exception) zzam);
    }

    public String toString() {
        int i2 = this.c;
        int i3 = this.a;
        StringBuilder sb = new StringBuilder(55);
        sb.append("Request { what=");
        sb.append(i2);
        sb.append(" id=");
        sb.append(i3);
        sb.append(" oneWay=");
        sb.append(false);
        sb.append("}");
        return sb.toString();
    }
}
