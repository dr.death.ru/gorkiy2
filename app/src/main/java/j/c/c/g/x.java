package j.c.c.g;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.PowerManager;
import android.util.Log;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdReceiver;
import java.io.IOException;

public final class x implements Runnable {
    public final long b;
    public final PowerManager.WakeLock c;
    public final FirebaseInstanceId d;

    /* renamed from: e  reason: collision with root package name */
    public final z f2533e;

    public x(FirebaseInstanceId firebaseInstanceId, z zVar, long j2) {
        this.d = firebaseInstanceId;
        this.f2533e = zVar;
        this.b = j2;
        PowerManager.WakeLock newWakeLock = ((PowerManager) a().getSystemService("power")).newWakeLock(1, "fiid-sync");
        this.c = newWakeLock;
        newWakeLock.setReferenceCounted(false);
    }

    public final Context a() {
        FirebaseApp firebaseApp = this.d.b;
        firebaseApp.a();
        return firebaseApp.a;
    }

    public final boolean b() {
        y c2 = this.d.c();
        if (!this.d.d.b() && !this.d.a(c2)) {
            return true;
        }
        try {
            String d2 = this.d.d();
            if (d2 == null) {
                Log.e("FirebaseInstanceId", "Token retrieval failed: null");
                return false;
            }
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                Log.d("FirebaseInstanceId", "Token successfully retrieved");
            }
            if (c2 == null || !d2.equals(c2.a)) {
                Intent intent = new Intent("com.google.firebase.messaging.NEW_TOKEN");
                intent.putExtra("token", d2);
                Context a = a();
                Intent intent2 = new Intent(a, FirebaseInstanceIdReceiver.class);
                intent2.setAction("com.google.firebase.MESSAGING_EVENT");
                intent2.putExtra("wrapped_intent", intent);
                a.sendBroadcast(intent2);
            }
            return true;
        } catch (IOException | SecurityException e2) {
            String valueOf = String.valueOf(e2.getMessage());
            Log.e("FirebaseInstanceId", valueOf.length() != 0 ? "Token retrieval failed: ".concat(valueOf) : new String("Token retrieval failed: "));
            return false;
        }
    }

    public final boolean c() {
        ConnectivityManager connectivityManager = (ConnectivityManager) a().getSystemService("connectivity");
        NetworkInfo activeNetworkInfo = connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null;
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @SuppressLint({"Wakelock"})
    public final void run() {
        boolean a;
        try {
            if (w.a().a(a())) {
                this.c.acquire();
            }
            boolean z = true;
            this.d.a(true);
            if (!this.d.d.a()) {
                this.d.a(false);
                if (!a) {
                    return;
                }
                return;
            }
            w a2 = w.a();
            Context a3 = a();
            if (a2.c == null) {
                if (a3.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != 0) {
                    z = false;
                }
                a2.c = Boolean.valueOf(z);
            }
            if (!a2.b.booleanValue()) {
                if (Log.isLoggable("FirebaseInstanceId", 3)) {
                    Log.d("FirebaseInstanceId", "Missing Permission: android.permission.ACCESS_NETWORK_STATE this should normally be included by the manifest merger, but may needed to be manually added to your manifest");
                }
            }
            if (!a2.c.booleanValue() || c()) {
                if (!b() || !this.f2533e.a(this.d)) {
                    this.d.a(this.b);
                } else {
                    this.d.a(false);
                }
                if (w.a().a(a())) {
                    this.c.release();
                    return;
                }
                return;
            }
            a0 a0Var = new a0(this);
            if (FirebaseInstanceId.h()) {
                Log.d("FirebaseInstanceId", "Connectivity change received registered");
            }
            a0Var.a.a().registerReceiver(a0Var, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
            if (w.a().a(a())) {
                this.c.release();
            }
        } finally {
            if (w.a().a(a())) {
                this.c.release();
            }
        }
    }
}
