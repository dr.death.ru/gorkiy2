package j.c.c.g;

import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import j.c.c.e.a;
import j.c.c.e.e;
import j.c.c.f.d;
import j.c.c.j.f;

public final /* synthetic */ class q implements e {
    public static final e a = new q();

    public final Object a(a aVar) {
        FirebaseApp firebaseApp = (FirebaseApp) aVar.a(FirebaseApp.class);
        firebaseApp.a();
        return new FirebaseInstanceId(firebaseApp, new o(firebaseApp.a), e0.a(), e0.a(), (d) aVar.a(d.class), (f) aVar.a(f.class));
    }
}
