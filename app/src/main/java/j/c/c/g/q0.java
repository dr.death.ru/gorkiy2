package j.c.c.g;

import android.os.Bundle;
import android.util.Log;
import com.crashlytics.android.core.CrashlyticsController;
import j.c.a.a.i.a;
import j.c.a.a.i.e;
import java.io.IOException;

public final class q0 implements a<Bundle, String> {
    public final /* synthetic */ p0 a;

    public q0(p0 p0Var) {
        this.a = p0Var;
    }

    public final /* synthetic */ Object a(e eVar) {
        Bundle bundle = (Bundle) eVar.a(IOException.class);
        if (bundle != null) {
            String string = bundle.getString("registration_id");
            if (string != null || (string = bundle.getString("unregistered")) != null) {
                return string;
            }
            String string2 = bundle.getString(CrashlyticsController.EVENT_TYPE_LOGGED);
            if ("RST".equals(string2)) {
                throw new IOException("INSTANCE_ID_RESET");
            } else if (string2 != null) {
                throw new IOException(string2);
            } else {
                String valueOf = String.valueOf(bundle);
                StringBuilder sb = new StringBuilder(valueOf.length() + 21);
                sb.append("Unexpected response: ");
                sb.append(valueOf);
                Log.w("FirebaseInstanceId", sb.toString(), new Throwable());
                throw new IOException("SERVICE_NOT_AVAILABLE");
            }
        } else {
            throw new IOException("SERVICE_NOT_AVAILABLE");
        }
    }
}
