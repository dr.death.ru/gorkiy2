package j.c.d;

import j.c.a.a.c.n.c;
import j.c.d.b0.Excluder;
import j.c.d.b0.a0.TreeTypeAdapter;
import j.c.d.b0.a0.TypeAdapters;
import j.c.d.c0.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class GsonBuilder {
    public Excluder a = Excluder.g;
    public LongSerializationPolicy b = LongSerializationPolicy.DEFAULT;
    public FieldNamingStrategy c = FieldNamingPolicy.IDENTITY;
    public final Map<Type, InstanceCreator<?>> d = new HashMap();

    /* renamed from: e  reason: collision with root package name */
    public final List<z> f2538e = new ArrayList();

    /* renamed from: f  reason: collision with root package name */
    public final List<z> f2539f = new ArrayList();
    public boolean g = false;
    public int h = 2;

    /* renamed from: i  reason: collision with root package name */
    public int f2540i = 2;

    /* renamed from: j  reason: collision with root package name */
    public boolean f2541j = false;

    /* renamed from: k  reason: collision with root package name */
    public boolean f2542k = false;

    /* renamed from: l  reason: collision with root package name */
    public boolean f2543l = true;

    /* renamed from: m  reason: collision with root package name */
    public boolean f2544m = false;

    /* renamed from: n  reason: collision with root package name */
    public boolean f2545n = false;

    /* renamed from: o  reason: collision with root package name */
    public boolean f2546o = false;

    public GsonBuilder a(Type type, Object obj) {
        boolean z = obj instanceof JsonSerializer;
        boolean z2 = false;
        c.a(z || (obj instanceof JsonDeserializer) || (obj instanceof InstanceCreator) || (obj instanceof TypeAdapter));
        if (obj instanceof InstanceCreator) {
            this.d.put(type, (InstanceCreator) obj);
        }
        if (z || (obj instanceof JsonDeserializer)) {
            TypeToken typeToken = new TypeToken(type);
            List<z> list = this.f2538e;
            if (typeToken.b == typeToken.a) {
                z2 = true;
            }
            list.add(new TreeTypeAdapter.c(obj, typeToken, z2, null));
        }
        if (obj instanceof TypeAdapter) {
            this.f2538e.add(TypeAdapters.a(new TypeToken(type), (TypeAdapter) obj));
        }
        return this;
    }
}
