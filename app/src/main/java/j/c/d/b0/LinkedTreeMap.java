package j.c.d.b0;

import java.io.Serializable;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

public final class LinkedTreeMap<K, V> extends AbstractMap<K, V> implements Serializable {

    /* renamed from: i  reason: collision with root package name */
    public static final Comparator<Comparable> f2551i = new a();
    public Comparator<? super K> b;
    public e<K, V> c;
    public int d = 0;

    /* renamed from: e  reason: collision with root package name */
    public int f2552e = 0;

    /* renamed from: f  reason: collision with root package name */
    public final e<K, V> f2553f = new e<>();
    public LinkedTreeMap<K, V>.defpackage.b g;
    public LinkedTreeMap<K, V>.defpackage.c h;

    public static class a implements Comparator<Comparable> {
        public int compare(Object obj, Object obj2) {
            return ((Comparable) obj).compareTo((Comparable) obj2);
        }
    }

    public class b extends AbstractSet<Map.Entry<K, V>> {

        public class a extends LinkedTreeMap<K, V>.defpackage.d<Map.Entry<K, V>> {
            public a(b bVar) {
                super();
            }

            public Object next() {
                return b();
            }
        }

        public b() {
        }

        public void clear() {
            LinkedTreeMap.this.clear();
        }

        public boolean contains(Object obj) {
            return (obj instanceof Map.Entry) && LinkedTreeMap.this.a((Map.Entry) obj) != null;
        }

        public Iterator<Map.Entry<K, V>> iterator() {
            return new a(this);
        }

        public boolean remove(Object obj) {
            e a2;
            if (!(obj instanceof Map.Entry) || (a2 = LinkedTreeMap.this.a((Map.Entry<?, ?>) ((Map.Entry) obj))) == null) {
                return false;
            }
            LinkedTreeMap.this.b(a2, true);
            return true;
        }

        public int size() {
            return LinkedTreeMap.this.d;
        }
    }

    public final class c extends AbstractSet<K> {

        public class a extends LinkedTreeMap<K, V>.defpackage.d<K> {
            public a(c cVar) {
                super();
            }

            public K next() {
                return b().g;
            }
        }

        public c() {
        }

        public void clear() {
            LinkedTreeMap.this.clear();
        }

        public boolean contains(Object obj) {
            return LinkedTreeMap.this.a(obj) != null;
        }

        public Iterator<K> iterator() {
            return new a(this);
        }

        public boolean remove(Object obj) {
            LinkedTreeMap linkedTreeMap = LinkedTreeMap.this;
            e a2 = linkedTreeMap.a(obj);
            if (a2 != null) {
                linkedTreeMap.b(a2, true);
            }
            if (a2 != null) {
                return true;
            }
            return false;
        }

        public int size() {
            return LinkedTreeMap.this.d;
        }
    }

    public abstract class d<T> implements Iterator<T> {
        public e<K, V> b;
        public e<K, V> c = null;
        public int d;

        public d() {
            LinkedTreeMap linkedTreeMap = LinkedTreeMap.this;
            this.b = linkedTreeMap.f2553f.f2555e;
            this.d = linkedTreeMap.f2552e;
        }

        public final e<K, V> b() {
            e<K, V> eVar = this.b;
            LinkedTreeMap linkedTreeMap = LinkedTreeMap.this;
            if (eVar == linkedTreeMap.f2553f) {
                throw new NoSuchElementException();
            } else if (linkedTreeMap.f2552e == this.d) {
                this.b = eVar.f2555e;
                this.c = eVar;
                return eVar;
            } else {
                throw new ConcurrentModificationException();
            }
        }

        public final boolean hasNext() {
            return this.b != LinkedTreeMap.this.f2553f;
        }

        public final void remove() {
            e<K, V> eVar = this.c;
            if (eVar != null) {
                LinkedTreeMap.this.b(eVar, true);
                this.c = null;
                this.d = LinkedTreeMap.this.f2552e;
                return;
            }
            throw new IllegalStateException();
        }
    }

    static {
        Class<LinkedTreeMap> cls = LinkedTreeMap.class;
    }

    public LinkedTreeMap() {
        Comparator<Comparable> comparator = f2551i;
        this.b = comparator == null ? f2551i : comparator;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.d.b0.LinkedTreeMap.a(j.c.d.b0.LinkedTreeMap$e, boolean):void
     arg types: [j.c.d.b0.LinkedTreeMap$e<K, V>, int]
     candidates:
      j.c.d.b0.LinkedTreeMap.a(java.lang.Object, boolean):j.c.d.b0.LinkedTreeMap$e<K, V>
      j.c.d.b0.LinkedTreeMap.a(j.c.d.b0.LinkedTreeMap$e, j.c.d.b0.LinkedTreeMap$e):void
      j.c.d.b0.LinkedTreeMap.a(j.c.d.b0.LinkedTreeMap$e, boolean):void */
    public e<K, V> a(K k2, boolean z) {
        int i2;
        e<K, V> eVar;
        Comparator<? super K> comparator = this.b;
        e<K, V> eVar2 = this.c;
        if (eVar2 != null) {
            Comparable comparable = comparator == f2551i ? (Comparable) k2 : null;
            while (true) {
                if (comparable != null) {
                    i2 = comparable.compareTo(eVar2.g);
                } else {
                    i2 = comparator.compare(k2, eVar2.g);
                }
                if (i2 == 0) {
                    return eVar2;
                }
                e<K, V> eVar3 = i2 < 0 ? eVar2.c : eVar2.d;
                if (eVar3 == null) {
                    break;
                }
                eVar2 = eVar3;
            }
        } else {
            i2 = 0;
        }
        if (!z) {
            return null;
        }
        e<K, V> eVar4 = this.f2553f;
        if (eVar2 != null) {
            eVar = new e<>(eVar2, k2, eVar4, eVar4.f2556f);
            if (i2 < 0) {
                eVar2.c = eVar;
            } else {
                eVar2.d = eVar;
            }
            a((e) eVar2, true);
        } else if (comparator != f2551i || (k2 instanceof Comparable)) {
            eVar = new e<>(eVar2, k2, eVar4, eVar4.f2556f);
            this.c = eVar;
        } else {
            throw new ClassCastException(k2.getClass().getName() + " is not Comparable");
        }
        this.d++;
        this.f2552e++;
        return eVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.d.b0.LinkedTreeMap.a(j.c.d.b0.LinkedTreeMap$e, boolean):void
     arg types: [j.c.d.b0.LinkedTreeMap$e<K, V>, int]
     candidates:
      j.c.d.b0.LinkedTreeMap.a(java.lang.Object, boolean):j.c.d.b0.LinkedTreeMap$e<K, V>
      j.c.d.b0.LinkedTreeMap.a(j.c.d.b0.LinkedTreeMap$e, j.c.d.b0.LinkedTreeMap$e):void
      j.c.d.b0.LinkedTreeMap.a(j.c.d.b0.LinkedTreeMap$e, boolean):void */
    public void b(e<K, V> eVar, boolean z) {
        e<K, V> eVar2;
        int i2;
        e<K, V> eVar3;
        if (z) {
            e<K, V> eVar4 = eVar.f2556f;
            eVar4.f2555e = eVar.f2555e;
            eVar.f2555e.f2556f = eVar4;
        }
        e<K, V> eVar5 = eVar.c;
        e<K, V> eVar6 = eVar.d;
        e<K, V> eVar7 = eVar.b;
        int i3 = 0;
        if (eVar5 == null || eVar6 == null) {
            if (eVar5 != null) {
                a(eVar, eVar5);
                eVar.c = null;
            } else if (eVar6 != null) {
                a(eVar, eVar6);
                eVar.d = null;
            } else {
                a(eVar, (e) null);
            }
            a((e) eVar7, false);
            this.d--;
            this.f2552e++;
            return;
        }
        if (eVar5.f2557i > eVar6.f2557i) {
            e<K, V> eVar8 = eVar5.d;
            while (true) {
                e<K, V> eVar9 = eVar8;
                eVar2 = eVar5;
                eVar5 = eVar9;
                if (eVar5 == null) {
                    break;
                }
                eVar8 = eVar5.d;
            }
        } else {
            e<K, V> eVar10 = eVar6.c;
            while (true) {
                e<K, V> eVar11 = eVar6;
                eVar6 = eVar10;
                eVar3 = eVar11;
                if (eVar6 == null) {
                    break;
                }
                eVar10 = eVar6.c;
            }
            eVar2 = eVar3;
        }
        b(eVar2, false);
        e<K, V> eVar12 = eVar.c;
        if (eVar12 != null) {
            i2 = eVar12.f2557i;
            eVar2.c = eVar12;
            eVar12.b = eVar2;
            eVar.c = null;
        } else {
            i2 = 0;
        }
        e<K, V> eVar13 = eVar.d;
        if (eVar13 != null) {
            i3 = eVar13.f2557i;
            eVar2.d = eVar13;
            eVar13.b = eVar2;
            eVar.d = null;
        }
        eVar2.f2557i = Math.max(i2, i3) + 1;
        a(eVar, eVar2);
    }

    public void clear() {
        this.c = null;
        this.d = 0;
        this.f2552e++;
        e<K, V> eVar = this.f2553f;
        eVar.f2556f = eVar;
        eVar.f2555e = eVar;
    }

    public boolean containsKey(Object obj) {
        return a(obj) != null;
    }

    public Set<Map.Entry<K, V>> entrySet() {
        LinkedTreeMap<K, V>.defpackage.b bVar = this.g;
        if (bVar != null) {
            return bVar;
        }
        LinkedTreeMap<K, V>.defpackage.b bVar2 = new b();
        this.g = bVar2;
        return bVar2;
    }

    public V get(Object obj) {
        e a2 = a(obj);
        if (a2 != null) {
            return a2.h;
        }
        return null;
    }

    public Set<K> keySet() {
        LinkedTreeMap<K, V>.defpackage.c cVar = this.h;
        if (cVar != null) {
            return cVar;
        }
        LinkedTreeMap<K, V>.defpackage.c cVar2 = new c();
        this.h = cVar2;
        return cVar2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.d.b0.LinkedTreeMap.a(java.lang.Object, boolean):j.c.d.b0.LinkedTreeMap$e<K, V>
     arg types: [K, int]
     candidates:
      j.c.d.b0.LinkedTreeMap.a(j.c.d.b0.LinkedTreeMap$e, j.c.d.b0.LinkedTreeMap$e):void
      j.c.d.b0.LinkedTreeMap.a(j.c.d.b0.LinkedTreeMap$e, boolean):void
      j.c.d.b0.LinkedTreeMap.a(java.lang.Object, boolean):j.c.d.b0.LinkedTreeMap$e<K, V> */
    public V put(K k2, V v) {
        if (k2 != null) {
            e a2 = a((Object) k2, true);
            V v2 = a2.h;
            a2.h = v;
            return v2;
        }
        throw new NullPointerException("key == null");
    }

    public V remove(Object obj) {
        e a2 = a(obj);
        if (a2 != null) {
            b(a2, true);
        }
        if (a2 != null) {
            return a2.h;
        }
        return null;
    }

    public int size() {
        return this.d;
    }

    public static final class e<K, V> implements Map.Entry<K, V> {
        public e<K, V> b;
        public e<K, V> c;
        public e<K, V> d;

        /* renamed from: e  reason: collision with root package name */
        public e<K, V> f2555e;

        /* renamed from: f  reason: collision with root package name */
        public e<K, V> f2556f;
        public final K g;
        public V h;

        /* renamed from: i  reason: collision with root package name */
        public int f2557i;

        public e() {
            this.g = null;
            this.f2556f = this;
            this.f2555e = this;
        }

        /* JADX WARNING: Removed duplicated region for block: B:14:0x0031 A[ORIG_RETURN, RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean equals(java.lang.Object r4) {
            /*
                r3 = this;
                boolean r0 = r4 instanceof java.util.Map.Entry
                r1 = 0
                if (r0 == 0) goto L_0x0032
                java.util.Map$Entry r4 = (java.util.Map.Entry) r4
                K r0 = r3.g
                if (r0 != 0) goto L_0x0012
                java.lang.Object r0 = r4.getKey()
                if (r0 != 0) goto L_0x0032
                goto L_0x001c
            L_0x0012:
                java.lang.Object r2 = r4.getKey()
                boolean r0 = r0.equals(r2)
                if (r0 == 0) goto L_0x0032
            L_0x001c:
                V r0 = r3.h
                if (r0 != 0) goto L_0x0027
                java.lang.Object r4 = r4.getValue()
                if (r4 != 0) goto L_0x0032
                goto L_0x0031
            L_0x0027:
                java.lang.Object r4 = r4.getValue()
                boolean r4 = r0.equals(r4)
                if (r4 == 0) goto L_0x0032
            L_0x0031:
                r1 = 1
            L_0x0032:
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: j.c.d.b0.LinkedTreeMap.e.equals(java.lang.Object):boolean");
        }

        public K getKey() {
            return this.g;
        }

        public V getValue() {
            return this.h;
        }

        public int hashCode() {
            K k2 = this.g;
            int i2 = 0;
            int hashCode = k2 == null ? 0 : k2.hashCode();
            V v = this.h;
            if (v != null) {
                i2 = v.hashCode();
            }
            return hashCode ^ i2;
        }

        public V setValue(V v) {
            V v2 = this.h;
            this.h = v;
            return v2;
        }

        public String toString() {
            return ((Object) this.g) + "=" + ((Object) this.h);
        }

        public e(e<K, V> eVar, K k2, e<K, V> eVar2, e<K, V> eVar3) {
            this.b = eVar;
            this.g = k2;
            this.f2557i = 1;
            this.f2555e = eVar2;
            this.f2556f = eVar3;
            eVar3.f2555e = this;
            eVar2.f2556f = this;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.d.b0.LinkedTreeMap.a(java.lang.Object, boolean):j.c.d.b0.LinkedTreeMap$e<K, V>
     arg types: [java.lang.Object, int]
     candidates:
      j.c.d.b0.LinkedTreeMap.a(j.c.d.b0.LinkedTreeMap$e, j.c.d.b0.LinkedTreeMap$e):void
      j.c.d.b0.LinkedTreeMap.a(j.c.d.b0.LinkedTreeMap$e, boolean):void
      j.c.d.b0.LinkedTreeMap.a(java.lang.Object, boolean):j.c.d.b0.LinkedTreeMap$e<K, V> */
    public e<K, V> a(Object obj) {
        if (obj == null) {
            return null;
        }
        try {
            return a(obj, false);
        } catch (ClassCastException unused) {
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0020, code lost:
        if ((r3 == r5 || (r3 != null && r3.equals(r5))) != false) goto L_0x0024;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public j.c.d.b0.LinkedTreeMap.e<K, V> a(java.util.Map.Entry<?, ?> r5) {
        /*
            r4 = this;
            java.lang.Object r0 = r5.getKey()
            j.c.d.b0.LinkedTreeMap$e r0 = r4.a(r0)
            r1 = 1
            r2 = 0
            if (r0 == 0) goto L_0x0023
            V r3 = r0.h
            java.lang.Object r5 = r5.getValue()
            if (r3 == r5) goto L_0x001f
            if (r3 == 0) goto L_0x001d
            boolean r5 = r3.equals(r5)
            if (r5 == 0) goto L_0x001d
            goto L_0x001f
        L_0x001d:
            r5 = 0
            goto L_0x0020
        L_0x001f:
            r5 = 1
        L_0x0020:
            if (r5 == 0) goto L_0x0023
            goto L_0x0024
        L_0x0023:
            r1 = 0
        L_0x0024:
            if (r1 == 0) goto L_0x0027
            goto L_0x0028
        L_0x0027:
            r0 = 0
        L_0x0028:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.d.b0.LinkedTreeMap.a(java.util.Map$Entry):j.c.d.b0.LinkedTreeMap$e");
    }

    public final void a(e<K, V> eVar, e<K, V> eVar2) {
        e<K, V> eVar3 = eVar.b;
        eVar.b = null;
        if (eVar2 != null) {
            eVar2.b = eVar3;
        }
        if (eVar3 == null) {
            this.c = eVar2;
        } else if (eVar3.c == eVar) {
            eVar3.c = eVar2;
        } else {
            eVar3.d = eVar2;
        }
    }

    public final void a(e<K, V> eVar, boolean z) {
        while (eVar != null) {
            e<K, V> eVar2 = eVar.c;
            e<K, V> eVar3 = eVar.d;
            int i2 = 0;
            int i3 = eVar2 != null ? eVar2.f2557i : 0;
            int i4 = eVar3 != null ? eVar3.f2557i : 0;
            int i5 = i3 - i4;
            if (i5 == -2) {
                e<K, V> eVar4 = eVar3.c;
                e<K, V> eVar5 = eVar3.d;
                int i6 = eVar5 != null ? eVar5.f2557i : 0;
                if (eVar4 != null) {
                    i2 = eVar4.f2557i;
                }
                int i7 = i2 - i6;
                if (i7 == -1 || (i7 == 0 && !z)) {
                    a((e) eVar);
                } else {
                    b(eVar3);
                    a((e) eVar);
                }
                if (z) {
                    return;
                }
            } else if (i5 == 2) {
                e<K, V> eVar6 = eVar2.c;
                e<K, V> eVar7 = eVar2.d;
                int i8 = eVar7 != null ? eVar7.f2557i : 0;
                if (eVar6 != null) {
                    i2 = eVar6.f2557i;
                }
                int i9 = i2 - i8;
                if (i9 == 1 || (i9 == 0 && !z)) {
                    b(eVar);
                } else {
                    a((e) eVar2);
                    b(eVar);
                }
                if (z) {
                    return;
                }
            } else if (i5 == 0) {
                eVar.f2557i = i3 + 1;
                if (z) {
                    return;
                }
            } else {
                eVar.f2557i = Math.max(i3, i4) + 1;
                if (!z) {
                    return;
                }
            }
            eVar = eVar.b;
        }
    }

    public final void b(e<K, V> eVar) {
        e<K, V> eVar2 = eVar.c;
        e<K, V> eVar3 = eVar.d;
        e<K, V> eVar4 = eVar2.c;
        e<K, V> eVar5 = eVar2.d;
        eVar.c = eVar5;
        if (eVar5 != null) {
            eVar5.b = eVar;
        }
        a(eVar, eVar2);
        eVar2.d = eVar;
        eVar.b = eVar2;
        int i2 = 0;
        int max = Math.max(eVar3 != null ? eVar3.f2557i : 0, eVar5 != null ? eVar5.f2557i : 0) + 1;
        eVar.f2557i = max;
        if (eVar4 != null) {
            i2 = eVar4.f2557i;
        }
        eVar2.f2557i = Math.max(max, i2) + 1;
    }

    public final void a(e<K, V> eVar) {
        e<K, V> eVar2 = eVar.c;
        e<K, V> eVar3 = eVar.d;
        e<K, V> eVar4 = eVar3.c;
        e<K, V> eVar5 = eVar3.d;
        eVar.d = eVar4;
        if (eVar4 != null) {
            eVar4.b = eVar;
        }
        a(eVar, eVar3);
        eVar3.c = eVar;
        eVar.b = eVar3;
        int i2 = 0;
        int max = Math.max(eVar2 != null ? eVar2.f2557i : 0, eVar4 != null ? eVar4.f2557i : 0) + 1;
        eVar.f2557i = max;
        if (eVar5 != null) {
            i2 = eVar5.f2557i;
        }
        eVar3.f2557i = Math.max(max, i2) + 1;
    }
}
