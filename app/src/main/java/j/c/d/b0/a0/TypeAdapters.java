package j.c.d.b0.a0;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import j.a.a.a.outline;
import j.c.d.JsonArray;
import j.c.d.JsonElement;
import j.c.d.JsonNull;
import j.c.d.JsonObject;
import j.c.d.JsonPrimitive;
import j.c.d.TypeAdapter;
import j.c.d.TypeAdapter0;
import j.c.d.TypeAdapterFactory;
import j.c.d.a0.SerializedName;
import j.c.d.b0.LazilyParsedNumber;
import j.c.d.c0.TypeToken;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;
import l.a.a.a.o.d.EventsFilesManager;

public final class TypeAdapters {
    public static final TypeAdapter<String> A = new g();
    public static final TypeAdapter<BigDecimal> B = new h();
    public static final TypeAdapter<BigInteger> C = new i();
    public static final TypeAdapterFactory D = new y(String.class, A);
    public static final TypeAdapter<StringBuilder> E;
    public static final TypeAdapterFactory F;
    public static final TypeAdapter<StringBuffer> G;
    public static final TypeAdapterFactory H;
    public static final TypeAdapter<URL> I;
    public static final TypeAdapterFactory J;
    public static final TypeAdapter<URI> K;
    public static final TypeAdapterFactory L;
    public static final TypeAdapter<InetAddress> M;
    public static final TypeAdapterFactory N;
    public static final TypeAdapter<UUID> O;
    public static final TypeAdapterFactory P;
    public static final TypeAdapter<Currency> Q;
    public static final TypeAdapterFactory R;
    public static final TypeAdapterFactory S = new r();
    public static final TypeAdapter<Calendar> T;
    public static final TypeAdapterFactory U;
    public static final TypeAdapter<Locale> V;
    public static final TypeAdapterFactory W;
    public static final TypeAdapter<j.c.d.q> X;
    public static final TypeAdapterFactory Y;
    public static final TypeAdapterFactory Z = new w();
    public static final TypeAdapter<Class> a;
    public static final TypeAdapterFactory b;
    public static final TypeAdapter<BitSet> c;
    public static final TypeAdapterFactory d;

    /* renamed from: e  reason: collision with root package name */
    public static final TypeAdapter<Boolean> f2571e = new z();

    /* renamed from: f  reason: collision with root package name */
    public static final TypeAdapter<Boolean> f2572f = new a0();
    public static final TypeAdapterFactory g = new TypeAdapters0(Boolean.TYPE, Boolean.class, f2571e);
    public static final TypeAdapter<Number> h = new b0();

    /* renamed from: i  reason: collision with root package name */
    public static final TypeAdapterFactory f2573i = new TypeAdapters0(Byte.TYPE, Byte.class, h);

    /* renamed from: j  reason: collision with root package name */
    public static final TypeAdapter<Number> f2574j = new c0();

    /* renamed from: k  reason: collision with root package name */
    public static final TypeAdapterFactory f2575k = new TypeAdapters0(Short.TYPE, Short.class, f2574j);

    /* renamed from: l  reason: collision with root package name */
    public static final TypeAdapter<Number> f2576l = new d0();

    /* renamed from: m  reason: collision with root package name */
    public static final TypeAdapterFactory f2577m = new TypeAdapters0(Integer.TYPE, Integer.class, f2576l);

    /* renamed from: n  reason: collision with root package name */
    public static final TypeAdapter<AtomicInteger> f2578n;

    /* renamed from: o  reason: collision with root package name */
    public static final TypeAdapterFactory f2579o;

    /* renamed from: p  reason: collision with root package name */
    public static final TypeAdapter<AtomicBoolean> f2580p;

    /* renamed from: q  reason: collision with root package name */
    public static final TypeAdapterFactory f2581q;

    /* renamed from: r  reason: collision with root package name */
    public static final TypeAdapter<AtomicIntegerArray> f2582r;

    /* renamed from: s  reason: collision with root package name */
    public static final TypeAdapterFactory f2583s;

    /* renamed from: t  reason: collision with root package name */
    public static final TypeAdapter<Number> f2584t = new b();
    public static final TypeAdapter<Number> u = new c();
    public static final TypeAdapter<Number> v = new d();
    public static final TypeAdapter<Number> w;
    public static final TypeAdapterFactory x;
    public static final TypeAdapter<Character> y = new f();
    public static final TypeAdapterFactory z = new TypeAdapters0(Character.TYPE, Character.class, y);

    public static class r implements TypeAdapterFactory {
        public <T> TypeAdapter<T> a(j.c.d.k kVar, TypeToken<T> typeToken) {
            if (typeToken.a != Timestamp.class) {
                return null;
            }
            Class<Date> cls = Date.class;
            if (kVar != null) {
                return new a(this, kVar.a(new TypeToken(cls)));
            }
            throw null;
        }

        public class a extends TypeAdapter<Timestamp> {
            public final /* synthetic */ TypeAdapter a;

            public a(r rVar, TypeAdapter typeAdapter) {
                this.a = super;
            }

            public void a(JsonWriter jsonWriter, Object obj) {
                this.a.a(jsonWriter, (Timestamp) obj);
            }

            public Object a(JsonReader jsonReader) {
                Date date = (Date) this.a.a(jsonReader);
                if (date != null) {
                    return new Timestamp(date.getTime());
                }
                return null;
            }
        }
    }

    public static class w implements TypeAdapterFactory {
        public <T> TypeAdapter<T> a(j.c.d.k kVar, TypeToken<T> typeToken) {
            Class<? super T> cls = typeToken.a;
            if (!Enum.class.isAssignableFrom(cls) || cls == Enum.class) {
                return null;
            }
            if (!cls.isEnum()) {
                cls = cls.getSuperclass();
            }
            return new g0(cls);
        }
    }

    public static class x implements TypeAdapterFactory {
        public final /* synthetic */ TypeToken b;
        public final /* synthetic */ TypeAdapter c;

        public x(TypeToken typeToken, TypeAdapter typeAdapter) {
            this.b = typeToken;
            this.c = typeAdapter;
        }

        public <T> TypeAdapter<T> a(j.c.d.k kVar, TypeToken<T> typeToken) {
            if (typeToken.equals(this.b)) {
                return this.c;
            }
            return null;
        }
    }

    public static class y implements TypeAdapterFactory {
        public final /* synthetic */ Class b;
        public final /* synthetic */ TypeAdapter c;

        public y(Class cls, TypeAdapter typeAdapter) {
            this.b = cls;
            this.c = typeAdapter;
        }

        public <T> TypeAdapter<T> a(j.c.d.k kVar, TypeToken<T> typeToken) {
            if (typeToken.a == this.b) {
                return this.c;
            }
            return null;
        }

        public String toString() {
            StringBuilder a = outline.a("Factory[type=");
            a.append(this.b.getName());
            a.append(",adapter=");
            a.append(this.c);
            a.append("]");
            return a.toString();
        }
    }

    static {
        TypeAdapter0 typeAdapter0 = new TypeAdapter0(new k());
        a = typeAdapter0;
        b = new y(Class.class, typeAdapter0);
        TypeAdapter0 typeAdapter02 = new TypeAdapter0(new v());
        c = typeAdapter02;
        d = new y(BitSet.class, typeAdapter02);
        TypeAdapter0 typeAdapter03 = new TypeAdapter0(new e0());
        f2578n = typeAdapter03;
        f2579o = new y(AtomicInteger.class, typeAdapter03);
        TypeAdapter0 typeAdapter04 = new TypeAdapter0(new f0());
        f2580p = typeAdapter04;
        f2581q = new y(AtomicBoolean.class, typeAdapter04);
        TypeAdapter0 typeAdapter05 = new TypeAdapter0(new a());
        f2582r = typeAdapter05;
        f2583s = new y(AtomicIntegerArray.class, typeAdapter05);
        e eVar = new e();
        w = eVar;
        x = new y(Number.class, eVar);
        j jVar = new j();
        E = jVar;
        F = new y(StringBuilder.class, jVar);
        l lVar = new l();
        G = lVar;
        H = new y(StringBuffer.class, lVar);
        m mVar = new m();
        I = mVar;
        J = new y(URL.class, mVar);
        n nVar = new n();
        K = nVar;
        L = new y(URI.class, nVar);
        o oVar = new o();
        M = oVar;
        N = new TypeAdapters2(InetAddress.class, oVar);
        p pVar = new p();
        O = pVar;
        P = new y(UUID.class, pVar);
        TypeAdapter0 typeAdapter06 = new TypeAdapter0(new q());
        Q = typeAdapter06;
        R = new y(Currency.class, typeAdapter06);
        s sVar = new s();
        T = sVar;
        U = new TypeAdapters1(Calendar.class, GregorianCalendar.class, sVar);
        t tVar = new t();
        V = tVar;
        W = new y(Locale.class, tVar);
        u uVar = new u();
        X = uVar;
        Y = new TypeAdapters2(JsonElement.class, uVar);
    }

    public static <TT> j.c.d.z a(TypeToken typeToken, TypeAdapter typeAdapter) {
        return new x(typeToken, typeAdapter);
    }

    public static class a0 extends TypeAdapter<Boolean> {
        public void a(JsonWriter jsonWriter, Object obj) {
            String str;
            Boolean bool = (Boolean) obj;
            if (bool == null) {
                str = "null";
            } else {
                str = bool.toString();
            }
            jsonWriter.value(str);
        }

        public Object a(JsonReader jsonReader) {
            if (jsonReader.peek() != JsonToken.NULL) {
                return Boolean.valueOf(jsonReader.nextString());
            }
            jsonReader.nextNull();
            return null;
        }
    }

    public static class b extends TypeAdapter<Number> {
        public void a(JsonWriter jsonWriter, Object obj) {
            jsonWriter.value((Number) obj);
        }

        public Object a(JsonReader jsonReader) {
            if (jsonReader.peek() == JsonToken.NULL) {
                jsonReader.nextNull();
                return null;
            }
            try {
                return Long.valueOf(jsonReader.nextLong());
            } catch (NumberFormatException e2) {
                throw new JsonSyntaxException(e2);
            }
        }
    }

    public static class b0 extends TypeAdapter<Number> {
        public void a(JsonWriter jsonWriter, Object obj) {
            jsonWriter.value((Number) obj);
        }

        public Object a(JsonReader jsonReader) {
            if (jsonReader.peek() == JsonToken.NULL) {
                jsonReader.nextNull();
                return null;
            }
            try {
                return Byte.valueOf((byte) jsonReader.nextInt());
            } catch (NumberFormatException e2) {
                throw new JsonSyntaxException(e2);
            }
        }
    }

    public static class c extends TypeAdapter<Number> {
        public void a(JsonWriter jsonWriter, Object obj) {
            jsonWriter.value((Number) obj);
        }

        public Object a(JsonReader jsonReader) {
            if (jsonReader.peek() != JsonToken.NULL) {
                return Float.valueOf((float) jsonReader.nextDouble());
            }
            jsonReader.nextNull();
            return null;
        }
    }

    public static class c0 extends TypeAdapter<Number> {
        public void a(JsonWriter jsonWriter, Object obj) {
            jsonWriter.value((Number) obj);
        }

        public Object a(JsonReader jsonReader) {
            if (jsonReader.peek() == JsonToken.NULL) {
                jsonReader.nextNull();
                return null;
            }
            try {
                return Short.valueOf((short) jsonReader.nextInt());
            } catch (NumberFormatException e2) {
                throw new JsonSyntaxException(e2);
            }
        }
    }

    public static class d extends TypeAdapter<Number> {
        public void a(JsonWriter jsonWriter, Object obj) {
            jsonWriter.value((Number) obj);
        }

        public Object a(JsonReader jsonReader) {
            if (jsonReader.peek() != JsonToken.NULL) {
                return Double.valueOf(jsonReader.nextDouble());
            }
            jsonReader.nextNull();
            return null;
        }
    }

    public static class d0 extends TypeAdapter<Number> {
        public void a(JsonWriter jsonWriter, Object obj) {
            jsonWriter.value((Number) obj);
        }

        public Object a(JsonReader jsonReader) {
            if (jsonReader.peek() == JsonToken.NULL) {
                jsonReader.nextNull();
                return null;
            }
            try {
                return Integer.valueOf(jsonReader.nextInt());
            } catch (NumberFormatException e2) {
                throw new JsonSyntaxException(e2);
            }
        }
    }

    public static class e extends TypeAdapter<Number> {
        public void a(JsonWriter jsonWriter, Object obj) {
            jsonWriter.value((Number) obj);
        }

        public Object a(JsonReader jsonReader) {
            JsonToken peek = jsonReader.peek();
            int ordinal = peek.ordinal();
            if (ordinal == 5 || ordinal == 6) {
                return new LazilyParsedNumber(jsonReader.nextString());
            }
            if (ordinal == 8) {
                jsonReader.nextNull();
                return null;
            }
            throw new JsonSyntaxException("Expecting number, got: " + peek);
        }
    }

    public static class e0 extends TypeAdapter<AtomicInteger> {
        public void a(JsonWriter jsonWriter, Object obj) {
            jsonWriter.value((long) ((AtomicInteger) obj).get());
        }

        public Object a(JsonReader jsonReader) {
            try {
                return new AtomicInteger(jsonReader.nextInt());
            } catch (NumberFormatException e2) {
                throw new JsonSyntaxException(e2);
            }
        }
    }

    public static class f extends TypeAdapter<Character> {
        public void a(JsonWriter jsonWriter, Object obj) {
            String str;
            Character ch = (Character) obj;
            if (ch == null) {
                str = null;
            } else {
                str = String.valueOf(ch);
            }
            jsonWriter.value(str);
        }

        public Object a(JsonReader jsonReader) {
            if (jsonReader.peek() == JsonToken.NULL) {
                jsonReader.nextNull();
                return null;
            }
            String nextString = jsonReader.nextString();
            if (nextString.length() == 1) {
                return Character.valueOf(nextString.charAt(0));
            }
            throw new JsonSyntaxException(outline.a("Expecting character, got: ", nextString));
        }
    }

    public static class f0 extends TypeAdapter<AtomicBoolean> {
        public void a(JsonWriter jsonWriter, Object obj) {
            jsonWriter.value(((AtomicBoolean) obj).get());
        }

        public Object a(JsonReader jsonReader) {
            return new AtomicBoolean(jsonReader.nextBoolean());
        }
    }

    public static class g extends TypeAdapter<String> {
        public void a(JsonWriter jsonWriter, Object obj) {
            jsonWriter.value((String) obj);
        }

        public Object a(JsonReader jsonReader) {
            JsonToken peek = jsonReader.peek();
            if (peek == JsonToken.NULL) {
                jsonReader.nextNull();
                return null;
            } else if (peek == JsonToken.BOOLEAN) {
                return Boolean.toString(jsonReader.nextBoolean());
            } else {
                return jsonReader.nextString();
            }
        }
    }

    public static final class g0<T extends Enum<T>> extends TypeAdapter<T> {
        public final Map<String, T> a = new HashMap();
        public final Map<T, String> b = new HashMap();

        public g0(Class<T> cls) {
            try {
                for (Enum enumR : (Enum[]) cls.getEnumConstants()) {
                    String name = enumR.name();
                    SerializedName serializedName = (SerializedName) cls.getField(name).getAnnotation(SerializedName.class);
                    if (serializedName != null) {
                        name = serializedName.value();
                        for (String put : serializedName.alternate()) {
                            this.a.put(put, enumR);
                        }
                    }
                    this.a.put(name, enumR);
                    this.b.put(enumR, name);
                }
            } catch (NoSuchFieldException e2) {
                throw new AssertionError(e2);
            }
        }

        public void a(JsonWriter jsonWriter, Object obj) {
            String str;
            Enum enumR = (Enum) obj;
            if (enumR == null) {
                str = null;
            } else {
                str = this.b.get(enumR);
            }
            jsonWriter.value(str);
        }

        public Object a(JsonReader jsonReader) {
            if (jsonReader.peek() != JsonToken.NULL) {
                return (Enum) this.a.get(jsonReader.nextString());
            }
            jsonReader.nextNull();
            return null;
        }
    }

    public static class h extends TypeAdapter<BigDecimal> {
        public void a(JsonWriter jsonWriter, Object obj) {
            jsonWriter.value((BigDecimal) obj);
        }

        public Object a(JsonReader jsonReader) {
            if (jsonReader.peek() == JsonToken.NULL) {
                jsonReader.nextNull();
                return null;
            }
            try {
                return new BigDecimal(jsonReader.nextString());
            } catch (NumberFormatException e2) {
                throw new JsonSyntaxException(e2);
            }
        }
    }

    public static class i extends TypeAdapter<BigInteger> {
        public void a(JsonWriter jsonWriter, Object obj) {
            jsonWriter.value((BigInteger) obj);
        }

        public Object a(JsonReader jsonReader) {
            if (jsonReader.peek() == JsonToken.NULL) {
                jsonReader.nextNull();
                return null;
            }
            try {
                return new BigInteger(jsonReader.nextString());
            } catch (NumberFormatException e2) {
                throw new JsonSyntaxException(e2);
            }
        }
    }

    public static class j extends TypeAdapter<StringBuilder> {
        public void a(JsonWriter jsonWriter, Object obj) {
            String str;
            StringBuilder sb = (StringBuilder) obj;
            if (sb == null) {
                str = null;
            } else {
                str = sb.toString();
            }
            jsonWriter.value(str);
        }

        public Object a(JsonReader jsonReader) {
            if (jsonReader.peek() != JsonToken.NULL) {
                return new StringBuilder(jsonReader.nextString());
            }
            jsonReader.nextNull();
            return null;
        }
    }

    public static class l extends TypeAdapter<StringBuffer> {
        public void a(JsonWriter jsonWriter, Object obj) {
            String str;
            StringBuffer stringBuffer = (StringBuffer) obj;
            if (stringBuffer == null) {
                str = null;
            } else {
                str = stringBuffer.toString();
            }
            jsonWriter.value(str);
        }

        public Object a(JsonReader jsonReader) {
            if (jsonReader.peek() != JsonToken.NULL) {
                return new StringBuffer(jsonReader.nextString());
            }
            jsonReader.nextNull();
            return null;
        }
    }

    public static class m extends TypeAdapter<URL> {
        public void a(JsonWriter jsonWriter, Object obj) {
            String str;
            URL url = (URL) obj;
            if (url == null) {
                str = null;
            } else {
                str = url.toExternalForm();
            }
            jsonWriter.value(str);
        }

        public Object a(JsonReader jsonReader) {
            if (jsonReader.peek() == JsonToken.NULL) {
                jsonReader.nextNull();
                return null;
            }
            String nextString = jsonReader.nextString();
            if ("null".equals(nextString)) {
                return null;
            }
            return new URL(nextString);
        }
    }

    public static class n extends TypeAdapter<URI> {
        public void a(JsonWriter jsonWriter, Object obj) {
            String str;
            URI uri = (URI) obj;
            if (uri == null) {
                str = null;
            } else {
                str = uri.toASCIIString();
            }
            jsonWriter.value(str);
        }

        public Object a(JsonReader jsonReader) {
            if (jsonReader.peek() == JsonToken.NULL) {
                jsonReader.nextNull();
                return null;
            }
            try {
                String nextString = jsonReader.nextString();
                if ("null".equals(nextString)) {
                    return null;
                }
                return new URI(nextString);
            } catch (URISyntaxException e2) {
                throw new JsonIOException(e2);
            }
        }
    }

    public static class o extends TypeAdapter<InetAddress> {
        public void a(JsonWriter jsonWriter, Object obj) {
            String str;
            InetAddress inetAddress = (InetAddress) obj;
            if (inetAddress == null) {
                str = null;
            } else {
                str = inetAddress.getHostAddress();
            }
            jsonWriter.value(str);
        }

        public Object a(JsonReader jsonReader) {
            if (jsonReader.peek() != JsonToken.NULL) {
                return InetAddress.getByName(jsonReader.nextString());
            }
            jsonReader.nextNull();
            return null;
        }
    }

    public static class p extends TypeAdapter<UUID> {
        public void a(JsonWriter jsonWriter, Object obj) {
            String str;
            UUID uuid = (UUID) obj;
            if (uuid == null) {
                str = null;
            } else {
                str = uuid.toString();
            }
            jsonWriter.value(str);
        }

        public Object a(JsonReader jsonReader) {
            if (jsonReader.peek() != JsonToken.NULL) {
                return UUID.fromString(jsonReader.nextString());
            }
            jsonReader.nextNull();
            return null;
        }
    }

    public static class q extends TypeAdapter<Currency> {
        public void a(JsonWriter jsonWriter, Object obj) {
            jsonWriter.value(((Currency) obj).getCurrencyCode());
        }

        public Object a(JsonReader jsonReader) {
            return Currency.getInstance(jsonReader.nextString());
        }
    }

    public static class t extends TypeAdapter<Locale> {
        public void a(JsonWriter jsonWriter, Object obj) {
            String str;
            Locale locale = (Locale) obj;
            if (locale == null) {
                str = null;
            } else {
                str = locale.toString();
            }
            jsonWriter.value(str);
        }

        public Object a(JsonReader jsonReader) {
            String str = null;
            if (jsonReader.peek() == JsonToken.NULL) {
                jsonReader.nextNull();
                return null;
            }
            StringTokenizer stringTokenizer = new StringTokenizer(jsonReader.nextString(), EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
            String nextToken = stringTokenizer.hasMoreElements() ? stringTokenizer.nextToken() : null;
            String nextToken2 = stringTokenizer.hasMoreElements() ? stringTokenizer.nextToken() : null;
            if (stringTokenizer.hasMoreElements()) {
                str = stringTokenizer.nextToken();
            }
            if (nextToken2 == null && str == null) {
                return new Locale(nextToken);
            }
            if (str == null) {
                return new Locale(nextToken, nextToken2);
            }
            return new Locale(nextToken, nextToken2, str);
        }
    }

    public static class z extends TypeAdapter<Boolean> {
        public void a(JsonWriter jsonWriter, Object obj) {
            jsonWriter.value((Boolean) obj);
        }

        public Object a(JsonReader jsonReader) {
            JsonToken peek = jsonReader.peek();
            if (peek == JsonToken.NULL) {
                jsonReader.nextNull();
                return null;
            } else if (peek == JsonToken.STRING) {
                return Boolean.valueOf(Boolean.parseBoolean(jsonReader.nextString()));
            } else {
                return Boolean.valueOf(jsonReader.nextBoolean());
            }
        }
    }

    public static <TT> j.c.d.z a(Class cls, TypeAdapter typeAdapter) {
        return new y(cls, typeAdapter);
    }

    public static class k extends TypeAdapter<Class> {
        public void a(JsonWriter jsonWriter, Object obj) {
            StringBuilder a = outline.a("Attempted to serialize java.lang.Class: ");
            a.append(((Class) obj).getName());
            a.append(". Forgot to register a type adapter?");
            throw new UnsupportedOperationException(a.toString());
        }

        public Object a(JsonReader jsonReader) {
            throw new UnsupportedOperationException("Attempted to deserialize a java.lang.Class. Forgot to register a type adapter?");
        }
    }

    public static class a extends TypeAdapter<AtomicIntegerArray> {
        public void a(JsonWriter jsonWriter, Object obj) {
            AtomicIntegerArray atomicIntegerArray = (AtomicIntegerArray) obj;
            jsonWriter.beginArray();
            int length = atomicIntegerArray.length();
            for (int i2 = 0; i2 < length; i2++) {
                jsonWriter.value((long) atomicIntegerArray.get(i2));
            }
            jsonWriter.endArray();
        }

        public Object a(JsonReader jsonReader) {
            ArrayList arrayList = new ArrayList();
            jsonReader.beginArray();
            while (jsonReader.hasNext()) {
                try {
                    arrayList.add(Integer.valueOf(jsonReader.nextInt()));
                } catch (NumberFormatException e2) {
                    throw new JsonSyntaxException(e2);
                }
            }
            jsonReader.endArray();
            int size = arrayList.size();
            AtomicIntegerArray atomicIntegerArray = new AtomicIntegerArray(size);
            for (int i2 = 0; i2 < size; i2++) {
                atomicIntegerArray.set(i2, ((Integer) arrayList.get(i2)).intValue());
            }
            return atomicIntegerArray;
        }
    }

    public static class v extends TypeAdapter<BitSet> {
        public void a(JsonWriter jsonWriter, Object obj) {
            BitSet bitSet = (BitSet) obj;
            jsonWriter.beginArray();
            int length = bitSet.length();
            for (int i2 = 0; i2 < length; i2++) {
                jsonWriter.value(bitSet.get(i2) ? 1 : 0);
            }
            jsonWriter.endArray();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:13:0x003e, code lost:
            if (r6.nextInt() != 0) goto L_0x004b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0049, code lost:
            if (java.lang.Integer.parseInt(r1) != 0) goto L_0x004b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x004d, code lost:
            r1 = false;
         */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x0050  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x0053 A[SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Object a(com.google.gson.stream.JsonReader r6) {
            /*
                r5 = this;
                java.util.BitSet r0 = new java.util.BitSet
                r0.<init>()
                r6.beginArray()
                com.google.gson.stream.JsonToken r1 = r6.peek()
                r2 = 0
            L_0x000d:
                com.google.gson.stream.JsonToken r3 = com.google.gson.stream.JsonToken.END_ARRAY
                if (r1 == r3) goto L_0x0066
                int r3 = r1.ordinal()
                r4 = 5
                if (r3 == r4) goto L_0x0041
                r4 = 6
                if (r3 == r4) goto L_0x003a
                r4 = 7
                if (r3 != r4) goto L_0x0023
                boolean r1 = r6.nextBoolean()
                goto L_0x004e
            L_0x0023:
                com.google.gson.JsonSyntaxException r6 = new com.google.gson.JsonSyntaxException
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r2 = "Invalid bitset value type: "
                r0.append(r2)
                r0.append(r1)
                java.lang.String r0 = r0.toString()
                r6.<init>(r0)
                throw r6
            L_0x003a:
                int r1 = r6.nextInt()
                if (r1 == 0) goto L_0x004d
                goto L_0x004b
            L_0x0041:
                java.lang.String r1 = r6.nextString()
                int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ NumberFormatException -> 0x005a }
                if (r1 == 0) goto L_0x004d
            L_0x004b:
                r1 = 1
                goto L_0x004e
            L_0x004d:
                r1 = 0
            L_0x004e:
                if (r1 == 0) goto L_0x0053
                r0.set(r2)
            L_0x0053:
                int r2 = r2 + 1
                com.google.gson.stream.JsonToken r1 = r6.peek()
                goto L_0x000d
            L_0x005a:
                com.google.gson.JsonSyntaxException r6 = new com.google.gson.JsonSyntaxException
                java.lang.String r0 = "Error: Expecting: bitset number value (1, 0), Found: "
                java.lang.String r0 = j.a.a.a.outline.a(r0, r1)
                r6.<init>(r0)
                throw r6
            L_0x0066:
                r6.endArray()
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: j.c.d.b0.a0.TypeAdapters.v.a(com.google.gson.stream.JsonReader):java.lang.Object");
        }
    }

    public static class s extends TypeAdapter<Calendar> {
        public void a(JsonWriter jsonWriter, Object obj) {
            Calendar calendar = (Calendar) obj;
            if (calendar == null) {
                jsonWriter.nullValue();
                return;
            }
            jsonWriter.beginObject();
            jsonWriter.name("year");
            jsonWriter.value((long) calendar.get(1));
            jsonWriter.name("month");
            jsonWriter.value((long) calendar.get(2));
            jsonWriter.name("dayOfMonth");
            jsonWriter.value((long) calendar.get(5));
            jsonWriter.name("hourOfDay");
            jsonWriter.value((long) calendar.get(11));
            jsonWriter.name("minute");
            jsonWriter.value((long) calendar.get(12));
            jsonWriter.name("second");
            jsonWriter.value((long) calendar.get(13));
            jsonWriter.endObject();
        }

        public Object a(JsonReader jsonReader) {
            if (jsonReader.peek() == JsonToken.NULL) {
                jsonReader.nextNull();
                return null;
            }
            jsonReader.beginObject();
            int i2 = 0;
            int i3 = 0;
            int i4 = 0;
            int i5 = 0;
            int i6 = 0;
            int i7 = 0;
            while (jsonReader.peek() != JsonToken.END_OBJECT) {
                String nextName = jsonReader.nextName();
                int nextInt = jsonReader.nextInt();
                if ("year".equals(nextName)) {
                    i2 = nextInt;
                } else if ("month".equals(nextName)) {
                    i3 = nextInt;
                } else if ("dayOfMonth".equals(nextName)) {
                    i4 = nextInt;
                } else if ("hourOfDay".equals(nextName)) {
                    i5 = nextInt;
                } else if ("minute".equals(nextName)) {
                    i6 = nextInt;
                } else if ("second".equals(nextName)) {
                    i7 = nextInt;
                }
            }
            jsonReader.endObject();
            return new GregorianCalendar(i2, i3, i4, i5, i6, i7);
        }
    }

    public static class u extends TypeAdapter<j.c.d.q> {
        public void a(JsonWriter jsonWriter, JsonElement jsonElement) {
            if (jsonElement == null || (jsonElement instanceof JsonNull)) {
                jsonWriter.nullValue();
            } else if (jsonElement instanceof JsonPrimitive) {
                JsonPrimitive c = jsonElement.c();
                Object obj = c.a;
                if (obj instanceof Number) {
                    jsonWriter.value(c.f());
                } else if (obj instanceof Boolean) {
                    jsonWriter.value(c.e());
                } else {
                    jsonWriter.value(c.d());
                }
            } else {
                boolean z = jsonElement instanceof JsonArray;
                if (z) {
                    jsonWriter.beginArray();
                    if (z) {
                        Iterator<j.c.d.q> it = ((JsonArray) jsonElement).iterator();
                        while (it.hasNext()) {
                            a(jsonWriter, it.next());
                        }
                        jsonWriter.endArray();
                        return;
                    }
                    throw new IllegalStateException("Not a JSON Array: " + jsonElement);
                }
                boolean z2 = jsonElement instanceof JsonObject;
                if (z2) {
                    jsonWriter.beginObject();
                    if (z2) {
                        for (Map.Entry next : ((JsonObject) jsonElement).a.entrySet()) {
                            jsonWriter.name((String) next.getKey());
                            a(jsonWriter, (JsonElement) next.getValue());
                        }
                        jsonWriter.endObject();
                        return;
                    }
                    throw new IllegalStateException("Not a JSON Object: " + jsonElement);
                }
                StringBuilder a = outline.a("Couldn't write ");
                a.append(jsonElement.getClass());
                throw new IllegalArgumentException(a.toString());
            }
        }

        /* JADX WARN: Type inference failed for: r0v2, types: [j.c.d.JsonElement, j.c.d.JsonArray] */
        public JsonElement a(JsonReader jsonReader) {
            int ordinal = jsonReader.peek().ordinal();
            if (ordinal == 0) {
                ? jsonArray = new JsonArray();
                jsonReader.beginArray();
                while (jsonReader.hasNext()) {
                    Object a = a(jsonReader);
                    if (a == null) {
                        a = JsonNull.a;
                    }
                    jsonArray.b.add(a);
                }
                jsonReader.endArray();
                return jsonArray;
            } else if (ordinal == 2) {
                JsonObject jsonObject = new JsonObject();
                jsonReader.beginObject();
                while (jsonReader.hasNext()) {
                    jsonObject.a(jsonReader.nextName(), a(jsonReader));
                }
                jsonReader.endObject();
                return jsonObject;
            } else if (ordinal == 5) {
                return new JsonPrimitive(jsonReader.nextString());
            } else {
                if (ordinal == 6) {
                    return new JsonPrimitive((Number) new LazilyParsedNumber(jsonReader.nextString()));
                }
                if (ordinal == 7) {
                    return new JsonPrimitive(Boolean.valueOf(jsonReader.nextBoolean()));
                }
                if (ordinal == 8) {
                    jsonReader.nextNull();
                    return JsonNull.a;
                }
                throw new IllegalArgumentException();
            }
        }
    }
}
