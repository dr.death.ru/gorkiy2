package j.f.a.b;

import j.f.a.c.CaretString;
import n.n.c.Intrinsics;

/* compiled from: RTLCaretStringIterator.kt */
public final class RTLCaretStringIterator extends CaretStringIterator {
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RTLCaretStringIterator(CaretString caretString) {
        super(caretString, 0, 2);
        if (caretString != null) {
        } else {
            Intrinsics.a("caretString");
            throw null;
        }
    }

    public boolean b() {
        return super.b <= super.a.b;
    }
}
