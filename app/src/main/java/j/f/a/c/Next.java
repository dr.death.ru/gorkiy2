package j.f.a.c;

import n.n.c.Intrinsics;

/* compiled from: Next.kt */
public final class Next {
    public final State a;
    public final Character b;
    public final boolean c;
    public final Character d;

    public Next(State state, Character ch, boolean z, Character ch2) {
        if (state != null) {
            this.a = state;
            this.b = ch;
            this.c = z;
            this.d = ch2;
            return;
        }
        Intrinsics.a("state");
        throw null;
    }
}
