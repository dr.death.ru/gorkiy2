package j.f.a.c.e;

import com.crashlytics.android.answers.SessionEventTransform;
import j.a.a.a.outline;
import j.f.a.c.Next;
import j.f.a.c.State;
import kotlin.NoWhenBranchMatchedException;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;
import n.r.Indent;

/* compiled from: ValueState.kt */
public final class ValueState extends State {
    public final a b;

    /* compiled from: ValueState.kt */
    public static abstract class a {

        /* renamed from: j.f.a.c.e.ValueState$a$a  reason: collision with other inner class name */
        /* compiled from: ValueState.kt */
        public static final class C0032a extends a {
            public C0032a() {
                super(null);
            }
        }

        /* compiled from: ValueState.kt */
        public static final class b extends a {
            public final char a;
            public final String b;
        }

        /* compiled from: ValueState.kt */
        public static final class c extends a {
            public final a a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(a aVar) {
                super(null);
                if (aVar != null) {
                    this.a = super;
                    return;
                }
                Intrinsics.a("inheritedType");
                throw null;
            }
        }

        /* compiled from: ValueState.kt */
        public static final class d extends a {
            public d() {
                super(null);
            }
        }

        /* compiled from: ValueState.kt */
        public static final class e extends a {
            public e() {
                super(null);
            }
        }

        public /* synthetic */ a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ValueState(a aVar) {
        super(null);
        if (aVar != null) {
            this.b = new a.c(aVar);
            return;
        }
        Intrinsics.a("inheritedType");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.CharSequence, char, boolean, int):boolean
     arg types: [java.lang.String, char, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean):int
      n.r.Indent.a(java.lang.CharSequence, char[], int, boolean):int
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, boolean, int):java.util.List<java.lang.String>
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean, int):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, boolean, int):boolean
      n.r.Indent.a(java.lang.CharSequence, char, boolean, int):boolean */
    public Next a(char c) {
        a aVar = this.b;
        boolean z = false;
        if (aVar instanceof a.e) {
            z = Character.isDigit(c);
        } else if (aVar instanceof a.d) {
            z = Character.isLetter(c);
        } else if (aVar instanceof a.C0032a) {
            z = Character.isLetterOrDigit(c);
        } else if (aVar instanceof a.c) {
            a aVar2 = ((a.c) aVar).a;
            if (aVar2 instanceof a.e) {
                z = Character.isDigit(c);
            } else if (aVar2 instanceof a.d) {
                z = Character.isLetter(c);
            } else if (aVar2 instanceof a.C0032a) {
                z = Character.isLetterOrDigit(c);
            } else if (aVar2 instanceof a.b) {
                z = Indent.a((CharSequence) ((a.b) aVar2).b, c, false, 2);
            }
        } else if (aVar instanceof a.b) {
            z = Indent.a((CharSequence) ((a.b) aVar).b, c, false, 2);
        } else {
            throw new NoWhenBranchMatchedException();
        }
        if (!z) {
            return null;
        }
        return new Next(b(), Character.valueOf(c), true, Character.valueOf(c));
    }

    public State b() {
        return this.b instanceof a.c ? super : super.b();
    }

    public String toString() {
        a aVar = this.b;
        String str = "null";
        if (aVar instanceof a.d) {
            StringBuilder a2 = outline.a("[A] -> ");
            State state = super.a;
            if (state != null) {
                str = super.toString();
            }
            a2.append(str);
            return a2.toString();
        } else if (aVar instanceof a.e) {
            StringBuilder a3 = outline.a("[0] -> ");
            State state2 = super.a;
            if (state2 != null) {
                str = super.toString();
            }
            a3.append(str);
            return a3.toString();
        } else if (aVar instanceof a.C0032a) {
            StringBuilder a4 = outline.a("[_] -> ");
            State state3 = super.a;
            if (state3 != null) {
                str = super.toString();
            }
            a4.append(str);
            return a4.toString();
        } else if (aVar instanceof a.c) {
            StringBuilder a5 = outline.a("[…] -> ");
            State state4 = super.a;
            if (state4 != null) {
                str = super.toString();
            }
            a5.append(str);
            return a5.toString();
        } else if (aVar instanceof a.b) {
            StringBuilder a6 = outline.a("[");
            a6.append(((a.b) this.b).a);
            a6.append("] -> ");
            State state5 = super.a;
            if (state5 != null) {
                str = super.toString();
            }
            a6.append(str);
            return a6.toString();
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ValueState(State state, a aVar) {
        super(super);
        if (aVar != null) {
            this.b = aVar;
            return;
        }
        Intrinsics.a(SessionEventTransform.TYPE_KEY);
        throw null;
    }
}
