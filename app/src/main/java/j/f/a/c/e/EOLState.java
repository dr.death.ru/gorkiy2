package j.f.a.c.e;

import j.f.a.c.Next;
import j.f.a.c.State;

/* compiled from: EOLState.kt */
public final class EOLState extends State {
    public EOLState() {
        super(null);
    }

    public Next a(char c) {
        return null;
    }

    public String toString() {
        return "EOL";
    }
}
