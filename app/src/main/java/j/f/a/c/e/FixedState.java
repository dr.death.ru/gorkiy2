package j.f.a.c.e;

import j.a.a.a.outline;
import j.f.a.c.Next;
import j.f.a.c.State;

/* compiled from: FixedState.kt */
public final class FixedState extends State {
    public final char b;

    public FixedState(State state, char c) {
        super(super);
        this.b = c;
    }

    public Next a(char c) {
        if (this.b == c) {
            return new Next(b(), Character.valueOf(c), true, Character.valueOf(c));
        }
        return new Next(b(), Character.valueOf(this.b), false, Character.valueOf(this.b));
    }

    public String toString() {
        String str;
        StringBuilder a = outline.a("{");
        a.append(this.b);
        a.append("} -> ");
        State state = super.a;
        if (state == null) {
            str = "null";
        } else {
            str = super.toString();
        }
        a.append(str);
        return a.toString();
    }

    public Next a() {
        return new Next(b(), Character.valueOf(this.b), false, Character.valueOf(this.b));
    }
}
