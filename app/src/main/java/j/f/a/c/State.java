package j.f.a.c;

import n.n.c.Intrinsics;

/* compiled from: State.kt */
public abstract class State {
    public final State a;

    public State(State state) {
        this.a = state;
    }

    public Next a() {
        return null;
    }

    public abstract Next a(char c);

    public State b() {
        State state = this.a;
        if (state != null) {
            return state;
        }
        Intrinsics.a();
        throw null;
    }

    public abstract String toString();
}
