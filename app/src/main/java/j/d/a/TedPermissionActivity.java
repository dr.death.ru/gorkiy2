package j.d.a;

import android.content.DialogInterface;
import android.content.Intent;

public class TedPermissionActivity implements DialogInterface.OnClickListener {
    public final /* synthetic */ Intent b;
    public final /* synthetic */ com.gun0912.tedpermission.TedPermissionActivity c;

    public TedPermissionActivity(com.gun0912.tedpermission.TedPermissionActivity tedPermissionActivity, Intent intent) {
        this.c = tedPermissionActivity;
        this.b = intent;
    }

    public void onClick(DialogInterface dialogInterface, int i2) {
        this.c.startActivityForResult(this.b, 30);
    }
}
