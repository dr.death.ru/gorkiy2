package j.d.a;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import androidx.recyclerview.widget.RecyclerView;
import com.gun0912.tedpermission.TedPermissionActivity;
import j.a.a.a.outline;

/* compiled from: TedPermissionActivity */
public class TedPermissionActivity2 implements DialogInterface.OnClickListener {
    public final /* synthetic */ TedPermissionActivity b;

    public TedPermissionActivity2(TedPermissionActivity tedPermissionActivity) {
        this.b = tedPermissionActivity;
    }

    public void onClick(DialogInterface dialogInterface, int i2) {
        TedPermissionActivity tedPermissionActivity = this.b;
        Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
        StringBuilder a = outline.a("package:");
        a.append(tedPermissionActivity.getPackageName());
        tedPermissionActivity.startActivityForResult(intent.setData(Uri.parse(a.toString())), RecyclerView.MAX_SCROLL_DURATION);
    }
}
