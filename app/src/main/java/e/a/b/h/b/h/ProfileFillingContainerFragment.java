package e.a.b.h.b.h;

import e.a.b.h.b.h.ProfileFillingContainerFragmentViewState;
import e.c.c.b;
import j.e.b.PublishRelay;
import l.b.t.Consumer;
import n.n.c.Intrinsics;

/* compiled from: ProfileFillingContainerFragment.kt */
public final class ProfileFillingContainerFragment<T> implements Consumer<Boolean> {
    public final /* synthetic */ ru.covid19.droid.presentation.main.profileFilling.ProfileFillingContainerFragment b;

    public ProfileFillingContainerFragment(ru.covid19.droid.presentation.main.profileFilling.ProfileFillingContainerFragment profileFillingContainerFragment) {
        this.b = profileFillingContainerFragment;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.Boolean, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void a(Object obj) {
        Boolean bool = (Boolean) obj;
        PublishRelay<b> publishRelay = this.b.c0;
        Intrinsics.a((Object) bool, "it");
        publishRelay.a((b) new ProfileFillingContainerFragmentViewState.a(bool.booleanValue()));
    }
}
