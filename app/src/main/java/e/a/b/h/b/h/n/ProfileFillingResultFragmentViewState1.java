package e.a.b.h.b.h.n;

import j.a.a.a.outline;
import j.b.a.m.n.GlideUrl;
import n.n.c.Intrinsics;

/* compiled from: ProfileFillingResultFragmentViewState.kt */
public final class ProfileFillingResultFragmentViewState1 extends ProfileFillingResultFragmentViewState {
    public final GlideUrl a;

    public ProfileFillingResultFragmentViewState1(GlideUrl glideUrl) {
        if (glideUrl != null) {
            this.a = glideUrl;
        } else {
            Intrinsics.a("glideUrl");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof ProfileFillingResultFragmentViewState1) && Intrinsics.a(this.a, ((ProfileFillingResultFragmentViewState1) obj).a);
        }
        return true;
    }

    public int hashCode() {
        GlideUrl glideUrl = this.a;
        if (glideUrl != null) {
            return glideUrl.hashCode();
        }
        return 0;
    }

    public String toString() {
        StringBuilder a2 = outline.a("FillingProfileResultImageItem(glideUrl=");
        a2.append(this.a);
        a2.append(")");
        return a2.toString();
    }
}
