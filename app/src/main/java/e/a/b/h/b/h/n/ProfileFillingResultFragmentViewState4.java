package e.a.b.h.b.h.n;

import e.c.c.BaseViewState1;
import j.a.a.a.outline;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;
import ru.covid19.droid.data.model.profileData.ProfileData;

/* compiled from: ProfileFillingResultFragmentViewState.kt */
public abstract class ProfileFillingResultFragmentViewState4 extends BaseViewState1 {

    /* compiled from: ProfileFillingResultFragmentViewState.kt */
    public static final class a extends ProfileFillingResultFragmentViewState4 {
        public final ProfileData a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ProfileData profileData) {
            super(null);
            if (profileData != null) {
                this.a = profileData;
                return;
            }
            Intrinsics.a("profile");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof a) && Intrinsics.a(this.a, ((a) obj).a);
            }
            return true;
        }

        public int hashCode() {
            ProfileData profileData = this.a;
            if (profileData != null) {
                return profileData.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder a2 = outline.a("InitResult(profile=");
            a2.append(this.a);
            a2.append(")");
            return a2.toString();
        }
    }

    public /* synthetic */ ProfileFillingResultFragmentViewState4(DefaultConstructorMarker defaultConstructorMarker) {
    }
}
