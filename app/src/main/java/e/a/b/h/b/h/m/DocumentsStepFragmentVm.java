package e.a.b.h.b.h.m;

import android.net.Uri;
import e.a.a.a.b.BaseDpFragmentVm1;
import e.a.a.a.e.o.ITopLevelCoordinator;
import e.a.a.a.e.p.BottomSheetSelectorNavigationDto0;
import e.a.a.g.b.b.a.IAuthPrefs;
import e.a.b.d.b.IProfileRepository;
import e.a.b.h.b.g.IMainCoordinator;
import e.a.b.h.b.h.BaseStepValidatedFragmentVm;
import e.a.b.h.b.h.m.DocumentsStepFragmentViewState1;
import e.a.b.h.b.h.m.b;
import e.a.b.h.b.i.c.FillingProfileFullNamePageFragmentViewState2;
import e.c.c.BaseMviVm;
import e.c.c.BaseMviVm0;
import java.util.List;
import java.util.Locale;
import l.b.Observable;
import l.b.Single;
import l.b.s.Disposable;
import l.b.t.Action;
import l.b.t.Consumer;
import l.b.t.Function;
import l.b.u.b.Functions;
import l.b.u.b.ObjectHelper;
import l.b.u.e.c.ObservableDoOnLifecycle;
import n.Unit;
import n.i.Collections;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.n.c.Reflection;
import n.p.KDeclarationContainer;
import n.r.Indent;
import o.ResponseBody;
import q.b.a.ZoneId;
import ru.covid19.droid.data.model.profileData.Document;
import ru.covid19.droid.data.model.profileData.Document0;
import ru.covid19.droid.data.model.profileData.ProfileData;
import ru.covid19.droid.data.model.profileData.Travel;

/* compiled from: DocumentsStepFragmentVm.kt */
public final class DocumentsStepFragmentVm extends BaseStepValidatedFragmentVm<c> {

    /* renamed from: n  reason: collision with root package name */
    public final IMainCoordinator f692n;

    /* renamed from: o  reason: collision with root package name */
    public final IProfileRepository f693o;

    /* renamed from: p  reason: collision with root package name */
    public final ITopLevelCoordinator f694p;

    /* renamed from: q  reason: collision with root package name */
    public final IAuthPrefs f695q;

    /* compiled from: DocumentsStepFragmentVm.kt */
    public static final class a extends n.n.c.j implements Functions0<b.i, n.g> {
        public final /* synthetic */ DocumentsStepFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(DocumentsStepFragmentVm documentsStepFragmentVm) {
            super(1);
            this.c = documentsStepFragmentVm;
        }

        public Object a(Object obj) {
            DocumentsStepFragmentViewState1.i iVar = (DocumentsStepFragmentViewState1.i) obj;
            if (iVar != null) {
                this.c.f693o.a(iVar.a);
                return Unit.a;
            }
            Intrinsics.a("event");
            throw null;
        }
    }

    /* compiled from: DocumentsStepFragmentVm.kt */
    public static final class b extends n.n.c.j implements Functions0<b.c, n.g> {
        public final /* synthetic */ DocumentsStepFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(DocumentsStepFragmentVm documentsStepFragmentVm) {
            super(1);
            this.c = documentsStepFragmentVm;
        }

        public Object a(Object obj) {
            DocumentsStepFragmentViewState1.c cVar = (DocumentsStepFragmentViewState1.c) obj;
            if (cVar != null) {
                this.c.f693o.f().getTravel().setCity(cVar.a);
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: DocumentsStepFragmentVm.kt */
    public static final class c extends n.n.c.j implements Functions0<b.k, n.g> {
        public final /* synthetic */ DocumentsStepFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(DocumentsStepFragmentVm documentsStepFragmentVm) {
            super(1);
            this.c = documentsStepFragmentVm;
        }

        public Object a(Object obj) {
            DocumentsStepFragmentViewState1.k kVar = (DocumentsStepFragmentViewState1.k) obj;
            if (kVar != null) {
                this.c.f692n.a(new BottomSheetSelectorNavigationDto0("", kVar.a));
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: DocumentsStepFragmentVm.kt */
    public static final class d extends n.n.c.j implements Functions0<b.b, n.g> {
        public final /* synthetic */ DocumentsStepFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(DocumentsStepFragmentVm documentsStepFragmentVm) {
            super(1);
            this.c = documentsStepFragmentVm;
        }

        public Object a(Object obj) {
            DocumentsStepFragmentViewState1.b bVar = (DocumentsStepFragmentViewState1.b) obj;
            if (bVar != null) {
                ProfileData f2 = this.c.f693o.f();
                f2.setDocument(Document.copy$default(f2.getDocument(), null, null, bVar.a, 3, null));
                if (bVar.a == Document0.foreignCitizenDoc) {
                    f2.setDocument(Document.copy$default(f2.getDocument(), null, "", null, 5, null));
                }
                DocumentsStepFragmentVm documentsStepFragmentVm = this.c;
                documentsStepFragmentVm.d.a(DocumentsStepFragmentVm.b(documentsStepFragmentVm));
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: DocumentsStepFragmentVm.kt */
    public static final /* synthetic */ class e extends n.n.c.h implements Functions0<Observable<b.j>, Observable<BaseMviVm0<? extends e.c.c.k>>> {
        public e(DocumentsStepFragmentVm documentsStepFragmentVm) {
            super(1, documentsStepFragmentVm);
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [e.a.b.h.b.h.m.DocumentsStepFragmentVm$e, n.n.c.CallableReference] */
        public Object a(Object obj) {
            Observable observable = (Observable) obj;
            if (observable != null) {
                return DocumentsStepFragmentVm.a((DocumentsStepFragmentVm) this.c, observable);
            }
            Intrinsics.a("p1");
            throw null;
        }

        public final String f() {
            return "selectCountryEventProcessor";
        }

        /* JADX WARN: Type inference failed for: r0v1, types: [n.p.KClass, n.p.KDeclarationContainer] */
        public final KDeclarationContainer g() {
            return Reflection.a(DocumentsStepFragmentVm.class);
        }

        public final String i() {
            return "selectCountryEventProcessor(Lio/reactivex/Observable;)Lio/reactivex/Observable;";
        }
    }

    /* compiled from: DocumentsStepFragmentVm.kt */
    public static final class f extends n.n.c.j implements Functions0<b.a, n.g> {
        public final /* synthetic */ DocumentsStepFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(DocumentsStepFragmentVm documentsStepFragmentVm) {
            super(1);
            this.c = documentsStepFragmentVm;
        }

        public Object a(Object obj) {
            DocumentsStepFragmentViewState1.a aVar = (DocumentsStepFragmentViewState1.a) obj;
            if (aVar != null) {
                ProfileData f2 = this.c.f693o.f();
                f2.setTravel(Travel.copy$default(f2.getTravel(), 0, aVar.a.getTitle(), null, aVar.a.getCode(), null, 21, null));
                DocumentsStepFragmentVm documentsStepFragmentVm = this.c;
                documentsStepFragmentVm.d.a(DocumentsStepFragmentVm.b(documentsStepFragmentVm));
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: DocumentsStepFragmentVm.kt */
    public static final class g extends n.n.c.j implements Functions0<b.g, n.g> {
        public final /* synthetic */ DocumentsStepFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(DocumentsStepFragmentVm documentsStepFragmentVm) {
            super(1);
            this.c = documentsStepFragmentVm;
        }

        public Object a(Object obj) {
            DocumentsStepFragmentViewState1.g gVar = (DocumentsStepFragmentViewState1.g) obj;
            if (gVar != null) {
                ProfileData f2 = this.c.f693o.f();
                f2.setDocument(Document.copy$default(f2.getDocument(), null, gVar.a, null, 5, null));
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: DocumentsStepFragmentVm.kt */
    public static final class h extends n.n.c.j implements Functions0<b.e, n.g> {
        public final /* synthetic */ DocumentsStepFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(DocumentsStepFragmentVm documentsStepFragmentVm) {
            super(1);
            this.c = documentsStepFragmentVm;
        }

        public Object a(Object obj) {
            DocumentsStepFragmentViewState1.e eVar = (DocumentsStepFragmentViewState1.e) obj;
            if (eVar != null) {
                ProfileData f2 = this.c.f693o.f();
                f2.setDocument(Document.copy$default(f2.getDocument(), eVar.a, null, null, 6, null));
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: DocumentsStepFragmentVm.kt */
    public static final class i extends n.n.c.j implements Functions0<b.d, n.g> {
        public final /* synthetic */ DocumentsStepFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(DocumentsStepFragmentVm documentsStepFragmentVm) {
            super(1);
            this.c = documentsStepFragmentVm;
        }

        public Object a(Object obj) {
            DocumentsStepFragmentViewState1.d dVar = (DocumentsStepFragmentViewState1.d) obj;
            if (dVar != null) {
                ProfileData f2 = this.c.f693o.f();
                f2.setTravel(Travel.copy$default(f2.getTravel(), Integer.parseInt(Collections.a(dVar.a, "yyyyMMdd", (Locale) null, (ZoneId) null, 6)), null, null, null, null, 30, null));
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: DocumentsStepFragmentVm.kt */
    public static final class j extends n.n.c.j implements Functions0<b.f, n.g> {
        public final /* synthetic */ DocumentsStepFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(DocumentsStepFragmentVm documentsStepFragmentVm) {
            super(1);
            this.c = documentsStepFragmentVm;
        }

        public Object a(Object obj) {
            DocumentsStepFragmentViewState1.f fVar = (DocumentsStepFragmentViewState1.f) obj;
            if (fVar != null) {
                ProfileData f2 = this.c.f693o.f();
                f2.setTravel(Travel.copy$default(f2.getTravel(), 0, null, fVar.a, null, null, 27, null));
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: DocumentsStepFragmentVm.kt */
    public static final class k extends n.n.c.j implements Functions0<b.h, n.g> {
        public final /* synthetic */ DocumentsStepFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(DocumentsStepFragmentVm documentsStepFragmentVm) {
            super(1);
            this.c = documentsStepFragmentVm;
        }

        public Object a(Object obj) {
            if (((DocumentsStepFragmentViewState1.h) obj) != null) {
                this.c.f694p.a(false);
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: DocumentsStepFragmentVm.kt */
    public static final class l<T, R> implements Function<T, R> {
        public static final l a = new l();

        public Object a(Object obj) {
            if (((ResponseBody) obj) != null) {
                return new BaseMviVm0.a(FillingProfileFullNamePageFragmentViewState2.a.a);
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: DocumentsStepFragmentVm.kt */
    public static final class m<T> implements Consumer<l.b.s.b> {
        public final /* synthetic */ DocumentsStepFragmentVm b;

        public m(DocumentsStepFragmentVm documentsStepFragmentVm) {
            this.b = documentsStepFragmentVm;
        }

        public void a(Object obj) {
            Disposable disposable = (Disposable) obj;
            this.b.f682m.a((Boolean) true);
        }
    }

    /* compiled from: DocumentsStepFragmentVm.kt */
    public static final class n<T, R> implements Function<T, R> {
        public final /* synthetic */ DocumentsStepFragmentVm a;

        public n(DocumentsStepFragmentVm documentsStepFragmentVm) {
            this.a = documentsStepFragmentVm;
        }

        public Object a(Object obj) {
            BaseMviVm0 baseMviVm0 = (BaseMviVm0) obj;
            if (baseMviVm0 != null) {
                this.a.f682m.a((Boolean) false);
                return Boolean.valueOf(!(baseMviVm0 instanceof BaseMviVm0.b));
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    static {
        j.c.a.a.c.n.c.c((Object) "RUS");
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DocumentsStepFragmentVm(IMainCoordinator iMainCoordinator, IProfileRepository iProfileRepository, ITopLevelCoordinator iTopLevelCoordinator, IAuthPrefs iAuthPrefs) {
        super(iMainCoordinator);
        if (iMainCoordinator == null) {
            Intrinsics.a("mainNavigator");
            throw null;
        } else if (iProfileRepository == null) {
            Intrinsics.a("profileRepository");
            throw null;
        } else if (iTopLevelCoordinator == null) {
            Intrinsics.a("topLevelCoordinator");
            throw null;
        } else if (iAuthPrefs != null) {
            this.f692n = iMainCoordinator;
            this.f693o = iProfileRepository;
            this.f694p = iTopLevelCoordinator;
            this.f695q = iAuthPrefs;
        } else {
            Intrinsics.a("authPrefs");
            throw null;
        }
    }

    public static final /* synthetic */ DocumentsStepFragmentViewState0 b(DocumentsStepFragmentVm documentsStepFragmentVm) {
        return (DocumentsStepFragmentViewState0) documentsStepFragmentVm.e();
    }

    public void a(Object obj) {
        DocumentsStepFragmentViewState0 documentsStepFragmentViewState0 = (DocumentsStepFragmentViewState0) obj;
        if (documentsStepFragmentViewState0 != null) {
            Document document = this.f693o.f().getDocument();
            if (document != null) {
                documentsStepFragmentViewState0.a = document;
                Travel travel = this.f693o.f().getTravel();
                if (travel != null) {
                    documentsStepFragmentViewState0.b = travel;
                    documentsStepFragmentViewState0.c = this.f693o.e();
                    return;
                }
                Intrinsics.a("<set-?>");
                throw null;
            }
            Intrinsics.a("<set-?>");
            throw null;
        }
        Intrinsics.a("vs");
        throw null;
    }

    public Object d() {
        return new DocumentsStepFragmentViewState0(null, null, null, 7);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.b.h.b.h.m.DocumentsStepFragmentVm$m, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.t.Action, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.u.e.c.ObservableDoOnLifecycle, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Single<java.lang.Boolean>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Single<Boolean> g() {
        IProfileRepository iProfileRepository = this.f693o;
        Uri e2 = iProfileRepository.e();
        if (e2 != null) {
            Observable<R> c2 = iProfileRepository.b(e2, this.f695q.b()).c().c((Function) l.a);
            m mVar = new m(this);
            Action action = Functions.c;
            ObjectHelper.a((Object) mVar, "onSubscribe is null");
            ObjectHelper.a((Object) action, "onDispose is null");
            ObservableDoOnLifecycle observableDoOnLifecycle = new ObservableDoOnLifecycle(c2, mVar, action);
            Intrinsics.a((Object) observableDoOnLifecycle, "profileRepository\n      …ccept(true)\n            }");
            Single<Boolean> b2 = BaseDpFragmentVm1.a(this, observableDoOnLifecycle, FillingProfileFullNamePageFragmentViewState2.a.class, null, null, null, null, 30, null).c((Function) new n(this)).b();
            Intrinsics.a((Object) b2, "profileRepository\n      …          .firstOrError()");
            return b2;
        }
        Intrinsics.a();
        throw null;
    }

    public boolean h() {
        Document document = this.f693o.f().getDocument();
        Travel travel = this.f693o.f().getTravel();
        if (document.getType() != null && !Indent.b(document.getNumber()) && ((!Indent.b(document.getSeries()) || document.getType() == Document0.foreignCitizenDoc) && this.f693o.e() != null)) {
            travel.getArrivalDate();
            if (!Indent.b(travel.getArrivalPlace()) && !Indent.b(travel.getCountry())) {
                String city = travel.getCity();
                if (!(city == null || Indent.b(city))) {
                    return true;
                }
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<U>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<e.c.c.BaseMviVm0<? extends e.c.c.k>>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Observable<BaseMviVm0<? extends e.c.c.k>> a(Observable<? extends e.c.c.b> observable) {
        if (observable != null) {
            Observable<U> a2 = observable.a(DocumentsStepFragmentViewState1.k.class);
            Intrinsics.a((Object) a2, "ofType(R::class.java)");
            Observable<U> a3 = observable.a(DocumentsStepFragmentViewState1.b.class);
            Intrinsics.a((Object) a3, "ofType(R::class.java)");
            Observable<U> a4 = observable.a(DocumentsStepFragmentViewState1.j.class);
            Intrinsics.a((Object) a4, "ofType(R::class.java)");
            Observable<U> a5 = observable.a(DocumentsStepFragmentViewState1.a.class);
            Intrinsics.a((Object) a5, "ofType(R::class.java)");
            Observable<U> a6 = observable.a(DocumentsStepFragmentViewState1.g.class);
            Intrinsics.a((Object) a6, "ofType(R::class.java)");
            Observable<U> a7 = observable.a(DocumentsStepFragmentViewState1.e.class);
            Intrinsics.a((Object) a7, "ofType(R::class.java)");
            Observable<U> a8 = observable.a(DocumentsStepFragmentViewState1.d.class);
            Intrinsics.a((Object) a8, "ofType(R::class.java)");
            Observable<U> a9 = observable.a(DocumentsStepFragmentViewState1.f.class);
            Intrinsics.a((Object) a9, "ofType(R::class.java)");
            Observable<U> a10 = observable.a(DocumentsStepFragmentViewState1.h.class);
            Intrinsics.a((Object) a10, "ofType(R::class.java)");
            Observable<U> a11 = observable.a(DocumentsStepFragmentViewState1.i.class);
            Intrinsics.a((Object) a11, "ofType(R::class.java)");
            Observable<U> a12 = observable.a(DocumentsStepFragmentViewState1.c.class);
            Intrinsics.a((Object) a12, "ofType(R::class.java)");
            Observable<BaseMviVm0<? extends e.c.c.k>> a13 = Observable.a(super.a(observable), BaseMviVm.a(a2, new c(this)), BaseMviVm.a(a3, new d(this)), BaseMviVm.b(a4, new e(this)), BaseMviVm.a(a5, new f(this)), BaseMviVm.a(a6, new g(this)), BaseMviVm.a(a7, new h(this)), BaseMviVm.a(a8, new i(this)), BaseMviVm.a(a9, new j(this)), BaseMviVm.a(a10, new k(this)), BaseMviVm.a(a11, new a(this)), BaseMviVm.a(a12, new b(this)));
            Intrinsics.a((Object) a13, "Observable.mergeArray(\n …t\n            }\n        )");
            return a13;
        }
        Intrinsics.a("o");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final /* synthetic */ Observable a(DocumentsStepFragmentVm documentsStepFragmentVm, Observable observable) {
        if (documentsStepFragmentVm != null) {
            Observable c2 = observable.b(new DocumentsStepFragmentVm1(documentsStepFragmentVm)).c((Function) new DocumentsStepFragmentVm2(documentsStepFragmentVm)).c((Function) DocumentsStepFragmentVm3.a);
            Intrinsics.a((Object) c2, "observable.flatMapSingle…      }.map { IgnoreLce }");
            return c2;
        }
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.s.Disposable, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void a(List<? extends Observable<? extends e.c.c.b>> list) {
        if (list != null) {
            super.a(list);
            Disposable a2 = this.f692n.f().a(new DocumentsStepFragmentVm4(this));
            Intrinsics.a((Object) a2, "mainNavigator.addBottomS…)\n            }\n        }");
            j.c.a.a.c.n.c.a(a2, this.f722f);
            Disposable a3 = this.f694p.c().a(new DocumentsStepFragmentVm5(this));
            Intrinsics.a((Object) a3, "topLevelCoordinator.addC…          )\n            }");
            j.c.a.a.c.n.c.a(a3, this.f722f);
            return;
        }
        Intrinsics.a("es");
        throw null;
    }
}
