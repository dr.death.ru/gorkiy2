package e.a.b.h.c;

import android.os.Bundle;
import android.os.Parcelable;
import android.util.SparseArray;
import androidx.fragment.app.Fragment;
import e.a.a.a.e.BaseScreens2;
import e.a.a.a.e.Commands5;
import e.b.a.Navigator;
import e.b.a.Screen;
import e.b.a.h.Command;
import i.l.a.BackStackRecord;
import i.l.a.FragmentManager;
import i.l.a.FragmentManagerImpl;
import i.l.a.j;
import j.a.a.a.outline;
import n.g;
import n.n.b.Functions;
import n.n.b.Functions0;
import n.n.c.Intrinsics;

/* compiled from: StepsNavigator.kt */
public final class StepsNavigator0 implements Navigator {
    public SparseArray<Fragment.d> a;
    public Integer b;
    public final int c;
    public final FragmentManager d;

    /* renamed from: e  reason: collision with root package name */
    public final Functions0<c, g> f710e;

    /* renamed from: f  reason: collision with root package name */
    public final Functions<g> f711f;

    public StepsNavigator0(int i2, j jVar, Functions0<? super c, g> functions0, Functions<g> functions) {
        if (jVar == null) {
            Intrinsics.a("fragmentManager");
            throw null;
        } else if (functions0 == null) {
            Intrinsics.a("onShowNewStepAction");
            throw null;
        } else if (functions != null) {
            this.c = i2;
            this.d = jVar;
            this.f710e = functions0;
            this.f711f = functions;
            this.a = new SparseArray<>();
        } else {
            Intrinsics.a("onInvalidStepAction");
            throw null;
        }
    }

    public final Fragment a() {
        return this.d.a(this.c);
    }

    public void a(Command[] commandArr) {
        Bundle bundle;
        Bundle i2;
        if (commandArr != null) {
            for (Command command : commandArr) {
                if (command instanceof Commands5) {
                    Commands5 commands5 = (Commands5) command;
                    int i3 = commands5.a;
                    Screen screen = commands5.b;
                    if (!(screen instanceof BaseScreens2)) {
                        screen = null;
                    }
                    BaseScreens2 baseScreens2 = (BaseScreens2) screen;
                    if (baseScreens2 != null) {
                        Object obj = commands5.c;
                        if (!(obj instanceof IStepsNestedFragmentCoordinator0)) {
                            obj = null;
                        }
                        IStepsNestedFragmentCoordinator0 iStepsNestedFragmentCoordinator0 = (IStepsNestedFragmentCoordinator0) obj;
                        Integer num = this.b;
                        if (num != null) {
                            int intValue = num.intValue();
                            Fragment a2 = this.d.a(this.c);
                            if (a2 != null) {
                                SparseArray<Fragment.d> sparseArray = this.a;
                                FragmentManagerImpl fragmentManagerImpl = (FragmentManagerImpl) this.d;
                                if (fragmentManagerImpl == null) {
                                    throw null;
                                } else if (a2.f232s == fragmentManagerImpl) {
                                    sparseArray.put(intValue, (a2.b <= 0 || (i2 = fragmentManagerImpl.i(a2)) == null) ? null : new Fragment.d(i2));
                                } else {
                                    fragmentManagerImpl.a(new IllegalStateException(outline.a("Fragment ", a2, " is not currently in the FragmentManager")));
                                    throw null;
                                }
                            }
                        }
                        this.b = Integer.valueOf(i3);
                        Fragment a3 = baseScreens2.a();
                        Parcelable parcelable = baseScreens2.c;
                        if (parcelable != null) {
                            Bundle bundle2 = new Bundle();
                            bundle2.putParcelable("EXTRA_SCREEN_DATA", parcelable);
                            a3.f(bundle2);
                        }
                        Fragment.d dVar = this.a.get(i3);
                        if (a3.f232s == null) {
                            if (dVar == null || (bundle = dVar.b) == null) {
                                bundle = null;
                            }
                            a3.c = bundle;
                            FragmentManagerImpl fragmentManagerImpl2 = (FragmentManagerImpl) this.d;
                            if (fragmentManagerImpl2 != null) {
                                BackStackRecord backStackRecord = new BackStackRecord(fragmentManagerImpl2);
                                int i4 = this.c;
                                String str = baseScreens2.b;
                                if (i4 != 0) {
                                    backStackRecord.a(i4, a3, str, 2);
                                    if (!backStackRecord.h) {
                                        backStackRecord.f1326i = false;
                                        FragmentManagerImpl fragmentManagerImpl3 = backStackRecord.f1275s;
                                        fragmentManagerImpl3.c(false);
                                        if (backStackRecord.a(fragmentManagerImpl3.z, fragmentManagerImpl3.A)) {
                                            fragmentManagerImpl3.f1295e = true;
                                            try {
                                                fragmentManagerImpl3.c(fragmentManagerImpl3.z, fragmentManagerImpl3.A);
                                            } finally {
                                                fragmentManagerImpl3.e();
                                            }
                                        }
                                        fragmentManagerImpl3.r();
                                        fragmentManagerImpl3.i();
                                        fragmentManagerImpl3.c();
                                        if (iStepsNestedFragmentCoordinator0 != null) {
                                            this.f710e.a(iStepsNestedFragmentCoordinator0);
                                        }
                                    } else {
                                        throw new IllegalStateException("This transaction is already being added to the back stack");
                                    }
                                } else {
                                    throw new IllegalArgumentException("Must use non-zero containerViewId");
                                }
                            } else {
                                throw null;
                            }
                        } else {
                            throw new IllegalStateException("Fragment already added");
                        }
                    } else {
                        StringBuilder a4 = outline.a("Screen ");
                        a4.append(commands5.b);
                        a4.append(" must be a FragmentScreen");
                        throw new IllegalStateException(a4.toString());
                    }
                }
            }
        }
    }
}
