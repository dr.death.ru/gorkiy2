package e.a.b.h.b.i.c;

import e.c.c.BaseViewState2;
import j.a.a.a.outline;
import n.n.c.Intrinsics;

/* compiled from: FillingProfileFullNamePageFragmentViewState.kt */
public final class FillingProfileFullNamePageFragmentViewState3 extends BaseViewState2 {
    public final String a;

    public FillingProfileFullNamePageFragmentViewState3(String str) {
        if (str != null) {
            this.a = str;
        } else {
            Intrinsics.a("text");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof FillingProfileFullNamePageFragmentViewState3) && Intrinsics.a(this.a, ((FillingProfileFullNamePageFragmentViewState3) obj).a);
        }
        return true;
    }

    public int hashCode() {
        String str = this.a;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    public String toString() {
        return outline.a(outline.a("SetPlacementAddress(text="), this.a, ")");
    }
}
