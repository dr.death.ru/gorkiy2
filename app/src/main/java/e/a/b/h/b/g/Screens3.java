package e.a.b.h.b.g;

import androidx.fragment.app.Fragment;
import e.a.a.a.e.BaseScreens2;
import ru.covid19.droid.presentation.main.questionnaire.pages.FillingProfileFullNamePageFragment;

/* compiled from: Screens.kt */
public final class Screens3 extends BaseScreens2 {
    public static final Screens3 d = new Screens3();

    public Screens3() {
        super(null, 1);
    }

    public Fragment a() {
        return new FillingProfileFullNamePageFragment();
    }
}
