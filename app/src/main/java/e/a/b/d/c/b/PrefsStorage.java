package e.a.b.d.c.b;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import e.c.a.c.a.a.IPrefsStorage;
import e.c.a.c.a.b.ISecuredPreferencesWrapper;
import j.c.a.a.c.n.c;
import n.n.c.Intrinsics;

/* compiled from: PrefsStorage.kt */
public class PrefsStorage implements IPrefsStorage {
    public final Context a;
    public final ISecuredPreferencesWrapper b;

    public PrefsStorage(Context context, ISecuredPreferencesWrapper iSecuredPreferencesWrapper) {
        if (context == null) {
            Intrinsics.a("context");
            throw null;
        } else if (iSecuredPreferencesWrapper != null) {
            this.a = context;
            this.b = iSecuredPreferencesWrapper;
        } else {
            Intrinsics.a("securedPrefsWrapper");
            throw null;
        }
    }

    public void a() {
        for (String b2 : c.c((Object) "AUTH_PREFS")) {
            b(b2);
        }
    }

    @SuppressLint({"ApplySharedPref"})
    public void b(String str) {
        if (str != null) {
            a(str).edit().clear().commit();
        } else {
            Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.SharedPreferences, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public SharedPreferences a(String str) {
        if (str == null) {
            Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
            throw null;
        } else if (c.c((Object) "AUTH_PREFS").contains(str)) {
            return this.b.a(str);
        } else {
            SharedPreferences sharedPreferences = this.a.getSharedPreferences(str, 0);
            Intrinsics.a((Object) sharedPreferences, "context.getSharedPrefere…me, Context.MODE_PRIVATE)");
            return sharedPreferences;
        }
    }
}
