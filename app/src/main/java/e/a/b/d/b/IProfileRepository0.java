package e.a.b.d.b;

import android.content.Context;
import android.net.Uri;
import com.crashlytics.android.core.MetaDataStore;
import com.crashlytics.android.core.SessionProtobufHelper;
import e.a.a.g.a.f.EsiaProfileApiService;
import e.a.b.d.a.a.ProfileDataApiService;
import j.a.a.a.outline;
import j.c.d.Gson;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.TypeCastException;
import l.b.Completable;
import l.b.Scheduler;
import l.b.Single;
import l.b.r.b.AndroidSchedulers;
import l.b.t.Consumer;
import l.b.t.Function;
import l.b.u.b.ObjectHelper;
import l.b.u.e.d.SingleDoOnSuccess;
import l.b.u.e.d.SingleObserveOn;
import l.b.u.e.d.SingleSubscribeOn;
import l.b.w.Schedulers;
import n.Unit;
import n.i.Collections;
import n.n.c.Intrinsics;
import n.r.Charsets;
import n.r.Indent;
import o.Headers;
import o.MediaType;
import o.MultipartBody;
import o.RequestBody;
import o.i0;
import ru.covid19.core.data.network.model.CitizenshipResponse1;
import ru.covid19.core.data.network.model.CitizenshipsResponse;
import ru.covid19.core.data.network.model.CountriesResponse;
import ru.covid19.core.data.network.model.GetCountriesRequestBody;
import ru.covid19.core.data.network.model.PersonalResponse;
import ru.covid19.droid.data.model.profileData.Children;
import ru.covid19.droid.data.model.profileData.ProfileData;

/* compiled from: IProfileRepository.kt */
public final class IProfileRepository0 implements IProfileRepository {
    public ProfileData a;
    public Uri b;
    public Uri c;
    public final Map<String, PersonalResponse> d;

    /* renamed from: e  reason: collision with root package name */
    public final EsiaProfileApiService f651e;

    /* renamed from: f  reason: collision with root package name */
    public final Context f652f;
    public final ProfileDataApiService g;

    /* compiled from: IProfileRepository.kt */
    public static final class a<T, R> implements Function<Throwable, CitizenshipsResponse> {
        public final /* synthetic */ IProfileRepository0 a;

        public a(IProfileRepository0 iProfileRepository0) {
            this.a = iProfileRepository0;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.io.InputStream, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public Object a(Object obj) {
            if (((Throwable) obj) != null) {
                Gson gson = new Gson();
                InputStream open = this.a.f652f.getAssets().open("data/citizenships.json");
                Intrinsics.a((Object) open, "context.assets\n         …\"data/citizenships.json\")");
                InputStreamReader inputStreamReader = new InputStreamReader(open, Charsets.a);
                return (CitizenshipResponse1) gson.a(Collections.a((Reader) (inputStreamReader instanceof BufferedReader ? (BufferedReader) inputStreamReader : new BufferedReader(inputStreamReader, 8192))), CitizenshipResponse1.class);
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: IProfileRepository.kt */
    public static final class b<T, R> implements Function<Throwable, CountriesResponse> {
        public final /* synthetic */ IProfileRepository0 a;

        public b(IProfileRepository0 iProfileRepository0) {
            this.a = iProfileRepository0;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.io.InputStream, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public Object a(Object obj) {
            if (((Throwable) obj) != null) {
                Gson gson = new Gson();
                InputStream open = this.a.f652f.getAssets().open("data/countries.json");
                Intrinsics.a((Object) open, "context.assets\n         …en(\"data/countries.json\")");
                InputStreamReader inputStreamReader = new InputStreamReader(open, Charsets.a);
                return (CountriesResponse) gson.a(Collections.a((Reader) (inputStreamReader instanceof BufferedReader ? (BufferedReader) inputStreamReader : new BufferedReader(inputStreamReader, 8192))), CountriesResponse.class);
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: IProfileRepository.kt */
    public static final class c<T> implements Consumer<PersonalResponse> {
        public final /* synthetic */ IProfileRepository0 b;
        public final /* synthetic */ String c;

        public c(IProfileRepository0 iProfileRepository0, String str) {
            this.b = iProfileRepository0;
            this.c = str;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [ru.covid19.core.data.network.model.PersonalResponse, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public void a(Object obj) {
            PersonalResponse personalResponse = (PersonalResponse) obj;
            Map<String, PersonalResponse> map = this.b.d;
            String str = this.c;
            Intrinsics.a((Object) personalResponse, "it");
            map.put(str, personalResponse);
        }
    }

    public IProfileRepository0(EsiaProfileApiService esiaProfileApiService, Context context, ProfileDataApiService profileDataApiService) {
        EsiaProfileApiService esiaProfileApiService2 = esiaProfileApiService;
        Context context2 = context;
        ProfileDataApiService profileDataApiService2 = profileDataApiService;
        if (esiaProfileApiService2 == null) {
            Intrinsics.a("esiaProfileApiService");
            throw null;
        } else if (context2 == null) {
            Intrinsics.a("context");
            throw null;
        } else if (profileDataApiService2 != null) {
            this.f651e = esiaProfileApiService2;
            this.f652f = context2;
            this.g = profileDataApiService2;
            this.a = new ProfileData(false, false, false, null, null, null, null, null, null, 511, null);
            this.d = new LinkedHashMap();
        } else {
            Intrinsics.a("profileDataApiService");
            throw null;
        }
    }

    public void a(ProfileData profileData) {
        if (profileData != null) {
            this.a = profileData;
        } else {
            Intrinsics.a("<set-?>");
            throw null;
        }
    }

    public void b(Uri uri) {
        this.b = uri;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Single<ru.covid19.core.data.network.model.CountriesResponse>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Single<CountriesResponse> c() {
        Single<CountriesResponse> b2 = this.g.a(new GetCountriesRequestBody(null, 0, null, null, null, null, 63, null)).b(new b(this));
        Intrinsics.a((Object) b2, "profileDataApiService.ge…class.java)\n            }");
        return b2;
    }

    public Completable d() {
        ProfileDataApiService profileDataApiService = this.g;
        ProfileData profileData = this.a;
        profileData.getPassenger().setCitizenshipName(null);
        profileData.getPassenger().setFirstName(null);
        profileData.getPassenger().setLastName(null);
        profileData.getPassenger().setPatronymic(null);
        profileData.getPassenger().setQuarantineAddressMatchesPlacement(null);
        profileData.getPassenger().setRegistrationAddressMatchesPlacement(null);
        List<Children> children = profileData.getChildren();
        ArrayList arrayList = new ArrayList(Collections.a(children, 10));
        for (Children citizenshipName : children) {
            citizenshipName.setCitizenshipName(null);
            arrayList.add(Unit.a);
        }
        profileData.getTravel().setCountryName(null);
        return profileDataApiService.a(profileData);
    }

    public Uri e() {
        return this.c;
    }

    public ProfileData f() {
        return this.a;
    }

    public Uri g() {
        return this.b;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Single<ru.covid19.core.data.network.model.CitizenshipsResponse>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Single<CitizenshipsResponse> b() {
        Single<CitizenshipsResponse> b2 = this.f651e.b().b(new a(this));
        Intrinsics.a((Object) b2, "esiaProfileApiService.ge…class.java)\n            }");
        return b2;
    }

    public void a(Uri uri) {
        this.c = uri;
    }

    public Single<ProfileData> a() {
        return this.g.a();
    }

    public Single<i0> b(Uri uri, String str) {
        if (uri == null) {
            Intrinsics.a("uri");
            throw null;
        } else if (str != null) {
            return a(uri, str, "document_photo.jpg", "document_photo");
        } else {
            Intrinsics.a("oid");
            throw null;
        }
    }

    public PersonalResponse a(String str) {
        if (str != null) {
            return this.d.get(str);
        }
        Intrinsics.a(MetaDataStore.KEY_USER_ID);
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.b.d.b.IProfileRepository0$c, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.u.e.d.SingleDoOnSuccess, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Single<PersonalResponse> a(String str, String str2) {
        Single<PersonalResponse> single;
        if (str == null) {
            Intrinsics.a(MetaDataStore.KEY_USER_ID);
            throw null;
        } else if (str2 != null) {
            PersonalResponse personalResponse = this.d.get(str);
            if (personalResponse != null) {
                single = Single.a(personalResponse);
            } else {
                single = this.f651e.a(str, str2);
            }
            c cVar = new c(this, str);
            if (single != null) {
                ObjectHelper.a((Object) cVar, "onSuccess is null");
                SingleDoOnSuccess singleDoOnSuccess = new SingleDoOnSuccess(single, cVar);
                Intrinsics.a((Object) singleDoOnSuccess, "getPersonalFormNetOrCach…serId] = it\n            }");
                return singleDoOnSuccess;
            }
            throw null;
        } else {
            Intrinsics.a("embed");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.Scheduler, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.u.e.d.SingleObserveOn, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final Single<i0> a(Uri uri, String str, String str2, String str3) {
        RequestBody.a aVar = RequestBody.a;
        byte[] a2 = Collections.a(this.f652f, uri);
        if (a2 != null) {
            MediaType.a aVar2 = MediaType.f2859f;
            RequestBody a3 = RequestBody.a.a(aVar, a2, MediaType.a.a("image/jpg"), 0, 0, 6);
            if (a3 != null) {
                StringBuilder a4 = outline.a("form-data; name=");
                MultipartBody.f2863l.a(a4, "file");
                if (str2 != null) {
                    a4.append("; filename=");
                    MultipartBody.f2863l.a(a4, str2);
                }
                String sb = a4.toString();
                Intrinsics.a((Object) sb, "StringBuilder().apply(builderAction).toString()");
                ArrayList arrayList = new ArrayList(20);
                Headers.c.a("Content-Disposition");
                arrayList.add("Content-Disposition");
                arrayList.add(Indent.c(sb).toString());
                Object[] array = arrayList.toArray(new String[0]);
                if (array != null) {
                    MultipartBody.c a5 = MultipartBody.c.a(new Headers((String[]) array, null), a3);
                    RequestBody.a aVar3 = RequestBody.a;
                    MediaType.a aVar4 = MediaType.f2859f;
                    RequestBody a6 = aVar3.a("88", MediaType.a.a("multipart/form-data"));
                    RequestBody.a aVar5 = RequestBody.a;
                    MediaType.a aVar6 = MediaType.f2859f;
                    RequestBody a7 = aVar5.a(str, MediaType.a.a("multipart/form-data"));
                    RequestBody.a aVar7 = RequestBody.a;
                    MediaType.a aVar8 = MediaType.f2859f;
                    RequestBody a8 = aVar7.a(str3, MediaType.a.a("multipart/form-data"));
                    RequestBody.a aVar9 = RequestBody.a;
                    MediaType.a aVar10 = MediaType.f2859f;
                    RequestBody a9 = aVar9.a(SessionProtobufHelper.SIGNAL_DEFAULT, MediaType.a.a("multipart/form-data"));
                    RequestBody.a aVar11 = RequestBody.a;
                    MediaType.a aVar12 = MediaType.f2859f;
                    RequestBody a10 = aVar11.a("1", MediaType.a.a("multipart/form-data"));
                    RequestBody.a aVar13 = RequestBody.a;
                    MediaType.a aVar14 = MediaType.f2859f;
                    Single<i0> a11 = this.g.a(a6, a7, a8, a9, a10, aVar13.a(str2, MediaType.a.a("multipart/form-data")), a5);
                    Scheduler scheduler = Schedulers.b;
                    if (a11 != null) {
                        ObjectHelper.a((Object) scheduler, "scheduler is null");
                        SingleSubscribeOn singleSubscribeOn = new SingleSubscribeOn(a11, scheduler);
                        Scheduler a12 = AndroidSchedulers.a();
                        ObjectHelper.a((Object) a12, "scheduler is null");
                        SingleObserveOn singleObserveOn = new SingleObserveOn(singleSubscribeOn, a12);
                        Intrinsics.a((Object) singleObserveOn, "profileDataApiService\n  …dSchedulers.mainThread())");
                        return singleObserveOn;
                    }
                    throw null;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            }
            Intrinsics.a("body");
            throw null;
        }
        Intrinsics.a();
        throw null;
    }

    public Single<i0> a(Uri uri, String str) {
        if (uri == null) {
            Intrinsics.a("uri");
            throw null;
        } else if (str != null) {
            return a(uri, str, "face_photo.jpg", "face_photo");
        } else {
            Intrinsics.a("oid");
            throw null;
        }
    }
}
