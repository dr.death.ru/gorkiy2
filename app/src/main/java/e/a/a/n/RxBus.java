package e.a.a.n;

import j.e.b.PublishRelay;
import n.n.c.Intrinsics;

/* compiled from: RxBus.kt */
public final class RxBus {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [j.e.b.PublishRelay, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public RxBus() {
        Intrinsics.a((Object) new PublishRelay(), "PublishRelay.create()");
    }
}
