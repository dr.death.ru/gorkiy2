package e.a.a.g.a.f;

import l.b.Single;
import r.l0.GET;
import r.l0.Path;
import r.l0.Query;
import ru.covid19.core.data.network.model.CitizenshipsResponse;
import ru.covid19.core.data.network.model.PersonalResponse;

/* compiled from: EsiaProfileApiService.kt */
public interface EsiaProfileApiService {
    @GET("esia-rs/api/public/v1/prns/{oid}")
    Single<PersonalResponse> a(@Path(encoded = true, value = "oid") String str, @Query(encoded = true, value = "embed") String str2);

    @GET("esia-rs/api/public/v1/citizenship")
    Single<CitizenshipsResponse> b();
}
