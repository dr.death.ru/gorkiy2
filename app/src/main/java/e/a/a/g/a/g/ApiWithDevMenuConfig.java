package e.a.a.g.a.g;

import e.a.a.j.b.IDevMenuRepository0;
import n.n.c.Intrinsics;

/* compiled from: ApiWithDevMenuConfig.kt */
public final class ApiWithDevMenuConfig extends IApiConfig {
    public final String a;
    public final String b;
    public final String c;
    public final String d;

    /* renamed from: e  reason: collision with root package name */
    public final long f621e;

    /* renamed from: f  reason: collision with root package name */
    public final String f622f;
    public final String g;
    public final String h;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
    public ApiWithDevMenuConfig(IDevMenuRepository0 iDevMenuRepository0) {
        String str;
        String str2;
        String str3;
        if (iDevMenuRepository0 != null) {
            this.a = "https://esia.gosuslugi.ru/";
            this.b = "https://www.gosuslugi.ru/";
            this.c = "https://lk.gosuslugi.ru/";
            this.d = "https://digipass-backend.wavea.cc/";
            this.f621e = 15;
            if (!Intrinsics.a((Object) "prod", (Object) "prod")) {
                str = iDevMenuRepository0.c();
                if (str == null) {
                    str = this.a;
                }
            } else {
                str = this.a;
            }
            this.f622f = str;
            if (!Intrinsics.a((Object) "prod", (Object) "prod")) {
                str2 = iDevMenuRepository0.f();
                if (str2 == null) {
                    str2 = this.b;
                }
            } else {
                str2 = this.b;
            }
            this.g = str2;
            if (!Intrinsics.a((Object) "prod", (Object) "prod")) {
                str3 = iDevMenuRepository0.d();
                if (str3 == null) {
                    str3 = this.c;
                }
            } else {
                str3 = this.c;
            }
            this.h = str3;
            return;
        }
        Intrinsics.a("devRepository");
        throw null;
    }

    public String b() {
        return this.h;
    }

    public String c() {
        return this.g;
    }

    public String d() {
        return this.f622f;
    }

    public String e() {
        return this.d;
    }

    public long f() {
        return this.f621e;
    }
}
