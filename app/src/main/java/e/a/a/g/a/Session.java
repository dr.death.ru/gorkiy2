package e.a.a.g.a;

import java.util.LinkedHashMap;
import java.util.Map;
import n.n.c.Intrinsics;

/* compiled from: Session.kt */
public final class Session {
    public String a = "";
    public String b = "";
    public Map<String, String> c = new LinkedHashMap();
    public String d = "";

    public final void a(String str) {
        if (str != null) {
            this.a = str;
        } else {
            Intrinsics.a("<set-?>");
            throw null;
        }
    }
}
