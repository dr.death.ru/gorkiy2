package e.a.a.g.a.e;

import j.c.d.JsonElement;
import j.c.d.JsonNull;
import j.c.d.JsonPrimitive;
import j.c.d.JsonSerializationContext;
import j.c.d.JsonSerializer;
import java.lang.reflect.Type;
import n.n.c.Intrinsics;
import q.b.a.LocalDateTime;
import q.b.a.e;
import q.b.a.t.DateTimeFormatter;

/* compiled from: LocalDateTimeSerialization.kt */
public final class LocalDateTimeSerialization0 implements JsonSerializer<e> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [j.c.d.JsonNull, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public JsonElement a(Object obj, Type type, JsonSerializationContext jsonSerializationContext) {
        String a;
        LocalDateTime localDateTime = (LocalDateTime) obj;
        if (localDateTime != null && (a = localDateTime.a(DateTimeFormatter.f3129l)) != null) {
            return new JsonPrimitive(a);
        }
        JsonNull jsonNull = JsonNull.a;
        Intrinsics.a((Object) jsonNull, "JsonNull.INSTANCE");
        return jsonNull;
    }
}
