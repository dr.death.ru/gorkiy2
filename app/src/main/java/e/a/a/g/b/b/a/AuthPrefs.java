package e.a.a.g.b.b.a;

import java.nio.charset.Charset;
import java.util.Arrays;
import kotlin.TypeCastException;
import n.n.c.Intrinsics;
import n.o.d;
import n.r.Charsets;
import q.a.a.i.b;
import q.a.a.i.c;

/* compiled from: AuthPrefs.kt */
public final class AuthPrefs {

    /* compiled from: AuthPrefs.kt */
    public static final class a extends Throwable {
        public final String b;

        public a(String str) {
            super(str);
            this.b = str;
        }

        public String getMessage() {
            return this.b;
        }
    }

    /* JADX WARN: Type inference failed for: r1v3, types: [n.o.Ranges0, n.o.Progressions] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.a.a.i.c.a(boolean, q.a.a.b):void
     arg types: [int, q.a.a.j.a]
     candidates:
      q.a.a.i.c.a(byte[], int):int
      q.a.a.i.c.a(boolean, q.a.a.b):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [byte[], java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final String a(String str, String str2, String str3) {
        byte[] bArr;
        if (str == null) {
            Intrinsics.a("data");
            throw null;
        } else if (str2 == null) {
            Intrinsics.a("password");
            throw null;
        } else if (str3 != null) {
            c cVar = new c(new q.a.a.h.a(new q.a.a.f.a()), new b());
            byte[] a2 = q.a.b.c.a.a(str);
            cVar.a(false, (q.a.a.b) a(str2, str3));
            int a3 = cVar.a(a2.length);
            byte[] bArr2 = new byte[a3];
            int a4 = cVar.a(a2, 0, a2.length, bArr2, 0);
            ? b = d.b(0, cVar.a(bArr2, a4) + a4);
            if (b != 0) {
                if (b.isEmpty()) {
                    bArr = new byte[0];
                } else {
                    int intValue = b.c().intValue();
                    int intValue2 = Integer.valueOf(b.c).intValue() + 1;
                    if (intValue2 <= a3) {
                        bArr = Arrays.copyOfRange(bArr2, intValue, intValue2);
                        Intrinsics.a((Object) bArr, "java.util.Arrays.copyOfR…this, fromIndex, toIndex)");
                    } else {
                        throw new IndexOutOfBoundsException("toIndex (" + intValue2 + ") is greater than size (" + a3 + ").");
                    }
                }
                return new String(bArr, Charsets.a);
            }
            Intrinsics.a("indices");
            throw null;
        } else {
            Intrinsics.a("salt64");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [byte[], java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.a.a.i.c.a(boolean, q.a.a.b):void
     arg types: [int, q.a.a.j.a]
     candidates:
      q.a.a.i.c.a(byte[], int):int
      q.a.a.i.c.a(boolean, q.a.a.b):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final String b(String str, String str2, String str3) {
        if (str == null) {
            Intrinsics.a("data");
            throw null;
        } else if (str2 == null) {
            Intrinsics.a("password");
            throw null;
        } else if (str3 != null) {
            c cVar = new c(new q.a.a.h.a(new q.a.a.f.a()), new b());
            byte[] bytes = str.getBytes(Charsets.a);
            Intrinsics.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            cVar.a(true, (q.a.a.b) a(str2, str3));
            byte[] bArr = new byte[cVar.a(bytes.length)];
            cVar.a(bArr, cVar.a(bytes, 0, bytes.length, bArr, 0));
            String a2 = q.a.b.c.a.a(bArr);
            Intrinsics.a((Object) a2, "Base64.toBase64String(outputBytes)");
            return a2;
        } else {
            Intrinsics.a("salt64");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [byte[], java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final q.a.a.j.a a(String str, String str2) {
        byte[] bArr;
        String str3 = str;
        byte[] a2 = q.a.b.c.a.a(str2);
        q.a.a.g.a aVar = new q.a.a.g.a(new q.a.a.e.b());
        int i2 = aVar.b;
        byte[] bArr2 = new byte[i2];
        Charset charset = Charsets.a;
        if (str3 != null) {
            byte[] bytes = str3.getBytes(charset);
            Intrinsics.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            int i3 = aVar.b;
            byte b = 1;
            int i4 = ((i3 + 32) - 1) / i3;
            byte[] bArr3 = new byte[4];
            byte[] bArr4 = new byte[(i4 * i3)];
            int length = bytes.length;
            byte[] bArr5 = new byte[length];
            System.arraycopy(bytes, 0, bArr5, 0, length);
            aVar.a.a();
            if (length > aVar.c) {
                aVar.a.a(bArr5, 0, length);
                aVar.a.a(aVar.f3084f, 0);
                length = aVar.b;
            } else {
                System.arraycopy(bArr5, 0, aVar.f3084f, 0, length);
            }
            while (true) {
                bArr = aVar.f3084f;
                if (length >= bArr.length) {
                    break;
                }
                bArr[length] = 0;
                length++;
            }
            System.arraycopy(bArr, 0, aVar.g, 0, aVar.c);
            byte[] bArr6 = aVar.f3084f;
            int i5 = aVar.c;
            for (int i6 = 0; i6 < i5; i6++) {
                bArr6[i6] = (byte) (bArr6[i6] ^ 54);
            }
            byte[] bArr7 = aVar.g;
            int i7 = aVar.c;
            for (int i8 = 0; i8 < i7; i8++) {
                bArr7[i8] = (byte) (bArr7[i8] ^ 92);
            }
            q.a.a.c cVar = aVar.a;
            if (cVar instanceof q.a.b.a) {
                q.a.b.a b2 = ((q.a.b.a) cVar).b();
                aVar.f3083e = b2;
                ((q.a.a.c) b2).a(aVar.g, 0, aVar.c);
            }
            q.a.a.c cVar2 = aVar.a;
            byte[] bArr8 = aVar.f3084f;
            cVar2.a(bArr8, 0, bArr8.length);
            q.a.a.c cVar3 = aVar.a;
            if (cVar3 instanceof q.a.b.a) {
                aVar.d = ((q.a.b.a) cVar3).b();
            }
            int i9 = 1;
            int i10 = 0;
            while (i9 <= i4) {
                int i11 = 3;
                while (true) {
                    byte b3 = (byte) (bArr3[i11] + b);
                    bArr3[i11] = b3;
                    if (b3 != 0) {
                        break;
                    }
                    i11--;
                }
                if (a2 != null) {
                    aVar.a.a(a2, 0, a2.length);
                }
                aVar.a.a(bArr3, 0, 4);
                aVar.a(bArr2, 0);
                System.arraycopy(bArr2, 0, bArr4, i10, i2);
                for (int i12 = 1; i12 < 1000; i12++) {
                    aVar.a.a(bArr2, 0, i2);
                    aVar.a(bArr2, 0);
                    for (int i13 = 0; i13 != i2; i13++) {
                        int i14 = i10 + i13;
                        bArr4[i14] = (byte) (bArr2[i13] ^ bArr4[i14]);
                    }
                }
                i10 += i3;
                i9++;
                b = 1;
            }
            return new q.a.a.j.a(bArr4, 0, 32);
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
}
