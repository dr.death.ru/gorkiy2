package e.a.a.i.d;

import i.o.ViewModel;
import i.o.ViewModelProvider;
import i.o.v;
import java.util.Map;
import m.a.Provider;
import n.n.c.Intrinsics;

/* compiled from: VmFactory.kt */
public final class VmFactory implements ViewModelProvider.b {
    public final Map<Class<? extends v>, Provider<v>> a;

    public VmFactory(Map<Class<? extends v>, Provider<v>> map) {
        if (map != null) {
            this.a = map;
        } else {
            Intrinsics.a("creators");
            throw null;
        }
    }

    public <T extends v> T a(Class<T> cls) {
        T t2 = null;
        if (cls != null) {
            Provider provider = this.a.get(cls);
            T t3 = provider != null ? (ViewModel) provider.get() : null;
            if (t3 instanceof ViewModel) {
                t2 = t3;
            }
            if (t2 != null) {
                return t2;
            }
            throw new IllegalArgumentException("Unknown model class " + cls);
        }
        Intrinsics.a("modelClass");
        throw null;
    }
}
