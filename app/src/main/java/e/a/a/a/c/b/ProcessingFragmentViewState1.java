package e.a.a.a.c.b;

import e.a.a.a.e.p.ProcessingNavigationDto;
import e.c.c.BaseViewState1;
import n.n.c.Intrinsics;

/* compiled from: ProcessingFragmentViewState.kt */
public abstract class ProcessingFragmentViewState1 extends BaseViewState1 {

    /* compiled from: ProcessingFragmentViewState.kt */
    public static final class a extends ProcessingFragmentViewState1 {
        public final ProcessingNavigationDto a;

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof a) && Intrinsics.a(null, ((a) obj).a);
            }
            return true;
        }

        public int hashCode() {
            return 0;
        }

        public String toString() {
            return "InitResult(dto=null)";
        }
    }
}
