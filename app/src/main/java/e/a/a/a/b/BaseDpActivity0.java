package e.a.a.a.b;

import android.content.Intent;
import n.Unit;
import n.g;
import n.n.b.Functions1;
import n.n.c.Intrinsics;
import n.n.c.Reflection;
import n.n.c.h;
import n.p.KDeclarationContainer;

/* compiled from: BaseDpActivity.kt */
public final /* synthetic */ class BaseDpActivity0 extends h implements Functions1<Intent, Integer, g> {
    public BaseDpActivity0(BaseDpActivity4 baseDpActivity4) {
        super(2, baseDpActivity4);
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [i.l.a.FragmentActivity, e.a.a.a.b.BaseDpActivity4] */
    public Object a(Object obj, Object obj2) {
        Intent intent = (Intent) obj;
        int intValue = ((Number) obj2).intValue();
        if (intent != null) {
            ((BaseDpActivity4) this.c).startActivityForResult(intent, intValue);
            return Unit.a;
        }
        Intrinsics.a("p1");
        throw null;
    }

    public final String f() {
        return "openActivityForResult";
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [n.p.KClass, n.p.KDeclarationContainer] */
    public final KDeclarationContainer g() {
        return Reflection.a(BaseDpActivity4.class);
    }

    public final String i() {
        return "openActivityForResult(Landroid/content/Intent;I)V";
    }
}
