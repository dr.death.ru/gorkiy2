package e.a.a.a.d;

import e.a.a.a.d.DevActivityViewState1;
import e.c.c.BaseMviVm0;
import l.b.t.Function;
import n.n.c.Intrinsics;

/* compiled from: DevActivityVm.kt */
public final class DevActivityVm8<T, R> implements Function<T, R> {
    public static final DevActivityVm8 a = new DevActivityVm8();

    public Object a(Object obj) {
        Boolean bool = (Boolean) obj;
        if (bool != null) {
            return new BaseMviVm0.a(new DevActivityViewState1.b(bool.booleanValue()));
        }
        Intrinsics.a("it");
        throw null;
    }
}
