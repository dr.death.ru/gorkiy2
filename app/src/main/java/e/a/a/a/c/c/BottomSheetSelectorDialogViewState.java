package e.a.a.a.c.c;

import e.a.a.a.e.p.a;
import j.a.a.a.outline;
import java.util.List;
import n.i.Collections2;
import n.n.c.Intrinsics;

/* compiled from: BottomSheetSelectorDialogViewState.kt */
public final class BottomSheetSelectorDialogViewState {
    public final String a;
    public final List<a> b;

    public BottomSheetSelectorDialogViewState(String str, List<? extends a> list) {
        if (str == null) {
            Intrinsics.a("title");
            throw null;
        } else if (list != null) {
            this.a = str;
            this.b = list;
        } else {
            Intrinsics.a("items");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof BottomSheetSelectorDialogViewState)) {
            return false;
        }
        BottomSheetSelectorDialogViewState bottomSheetSelectorDialogViewState = (BottomSheetSelectorDialogViewState) obj;
        return Intrinsics.a(this.a, bottomSheetSelectorDialogViewState.a) && Intrinsics.a(this.b, bottomSheetSelectorDialogViewState.b);
    }

    public int hashCode() {
        String str = this.a;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        List<a> list = this.b;
        if (list != null) {
            i2 = list.hashCode();
        }
        return hashCode + i2;
    }

    public String toString() {
        StringBuilder a2 = outline.a("BottomSheetSelectorDialogViewState(title=");
        a2.append(this.a);
        a2.append(", items=");
        a2.append(this.b);
        a2.append(")");
        return a2.toString();
    }

    public BottomSheetSelectorDialogViewState() {
        this("", Collections2.b);
    }
}
