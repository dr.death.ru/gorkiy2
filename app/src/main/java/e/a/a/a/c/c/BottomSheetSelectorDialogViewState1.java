package e.a.a.a.c.c;

import e.c.c.BaseViewState1;
import j.a.a.a.outline;
import java.util.List;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;

/* compiled from: BottomSheetSelectorDialogViewState.kt */
public abstract class BottomSheetSelectorDialogViewState1 extends BaseViewState1 {

    /* compiled from: BottomSheetSelectorDialogViewState.kt */
    public static final class a extends BottomSheetSelectorDialogViewState1 {
        public final String a;
        public final List<e.a.a.a.e.p.a> b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(String str, List<? extends e.a.a.a.e.p.a> list) {
            super(null);
            if (str == null) {
                Intrinsics.a("title");
                throw null;
            } else if (list != null) {
                this.a = str;
                this.b = list;
            } else {
                Intrinsics.a("items");
                throw null;
            }
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return Intrinsics.a(this.a, aVar.a) && Intrinsics.a(this.b, aVar.b);
        }

        public int hashCode() {
            String str = this.a;
            int i2 = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            List<e.a.a.a.e.p.a> list = this.b;
            if (list != null) {
                i2 = list.hashCode();
            }
            return hashCode + i2;
        }

        public String toString() {
            StringBuilder a2 = outline.a("InitResult(title=");
            a2.append(this.a);
            a2.append(", items=");
            a2.append(this.b);
            a2.append(")");
            return a2.toString();
        }
    }

    public /* synthetic */ BottomSheetSelectorDialogViewState1(DefaultConstructorMarker defaultConstructorMarker) {
    }
}
