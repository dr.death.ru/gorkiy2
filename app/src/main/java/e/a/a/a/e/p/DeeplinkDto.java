package e.a.a.a.e.p;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import j.a.a.a.outline;
import java.util.LinkedHashMap;
import java.util.Map;
import n.n.c.Intrinsics;

/* compiled from: DeeplinkDto.kt */
public final class DeeplinkDto implements Parcelable {
    public static final Parcelable.Creator CREATOR = new a();
    public final Uri b;
    public final String c;
    public final Map<String, String> d;

    public static class a implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            if (parcel != null) {
                Uri uri = (Uri) parcel.readParcelable(DeeplinkDto.class.getClassLoader());
                String readString = parcel.readString();
                int readInt = parcel.readInt();
                LinkedHashMap linkedHashMap = new LinkedHashMap(readInt);
                while (readInt != 0) {
                    linkedHashMap.put(parcel.readString(), parcel.readString());
                    readInt--;
                }
                return new DeeplinkDto(uri, readString, linkedHashMap);
            }
            Intrinsics.a("in");
            throw null;
        }

        public final Object[] newArray(int i2) {
            return new DeeplinkDto[i2];
        }
    }

    public DeeplinkDto(Uri uri, String str, Map<String, String> map) {
        if (uri == null) {
            Intrinsics.a("uri");
            throw null;
        } else if (str == null) {
            Intrinsics.a("path");
            throw null;
        } else if (map != null) {
            this.b = uri;
            this.c = str;
            this.d = map;
        } else {
            Intrinsics.a("params");
            throw null;
        }
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DeeplinkDto)) {
            return false;
        }
        DeeplinkDto deeplinkDto = (DeeplinkDto) obj;
        return Intrinsics.a(this.b, deeplinkDto.b) && Intrinsics.a(this.c, deeplinkDto.c) && Intrinsics.a(this.d, deeplinkDto.d);
    }

    public int hashCode() {
        Uri uri = this.b;
        int i2 = 0;
        int hashCode = (uri != null ? uri.hashCode() : 0) * 31;
        String str = this.c;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        Map<String, String> map = this.d;
        if (map != null) {
            i2 = map.hashCode();
        }
        return hashCode2 + i2;
    }

    public String toString() {
        StringBuilder a2 = outline.a("DeeplinkDto(uri=");
        a2.append(this.b);
        a2.append(", path=");
        a2.append(this.c);
        a2.append(", params=");
        a2.append(this.d);
        a2.append(")");
        return a2.toString();
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeParcelable(this.b, i2);
            parcel.writeString(this.c);
            Map<String, String> map = this.d;
            parcel.writeInt(map.size());
            for (Map.Entry<String, String> next : map.entrySet()) {
                parcel.writeString(next.getKey());
                parcel.writeString(next.getValue());
            }
            return;
        }
        Intrinsics.a("parcel");
        throw null;
    }
}
