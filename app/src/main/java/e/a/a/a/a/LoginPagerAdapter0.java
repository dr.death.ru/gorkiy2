package e.a.a.a.a;

import android.view.View;
import j.f.a.MaskedTextChangedListener;
import n.n.c.Intrinsics;

/* compiled from: LoginPagerAdapter.kt */
public final class LoginPagerAdapter0 implements MaskedTextChangedListener.a {
    public final /* synthetic */ LoginPagerAdapter1 a;

    public LoginPagerAdapter0(LoginPagerAdapter1 loginPagerAdapter1, View view) {
        this.a = loginPagerAdapter1;
    }

    public void a(boolean z, String str, String str2) {
        if (str == null) {
            Intrinsics.a("extractedValue");
            throw null;
        } else if (str2 != null) {
            this.a.c.a.a(str);
        } else {
            Intrinsics.a("formattedValue");
            throw null;
        }
    }
}
