package e.a.a.a.e;

import android.os.Parcelable;
import androidx.fragment.app.Fragment;
import e.b.a.g.a.SupportAppScreen;

/* compiled from: BaseScreens.kt */
public abstract class BaseScreens2 extends SupportAppScreen {
    public final Parcelable c;

    public BaseScreens2() {
        this(null, 1);
    }

    public BaseScreens2(Parcelable parcelable) {
        this.c = parcelable;
    }

    public abstract Fragment a();

    public /* synthetic */ BaseScreens2(Parcelable parcelable, int i2) {
        this.c = (i2 & 1) != 0 ? null : parcelable;
    }
}
