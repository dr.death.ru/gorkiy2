package e.a.a.a.c.c;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import e.a.a.a.b.BaseBottomSheetMviDialog;
import e.a.a.a.c.c.BottomSheetSelectorDialogViewState0;
import e.a.a.a.e.p.BottomSheetSelectorNavigationDto;
import e.a.a.a.e.p.BottomSheetSelectorNavigationDto0;
import e.a.a.d;
import e.a.a.e;
import e.a.a.i.CoreComponentsHolder;
import e.a.a.i.b.CoreComponent;
import e.c.c.b;
import j.c.a.a.c.n.c;
import j.e.a.b.AnyToUnit;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import l.b.Observable;
import l.b.t.Function;
import n.Unit;
import n.i.Collections;
import n.i._Arrays;
import n.n.c.Intrinsics;

/* compiled from: BottomSheetSelectorDialog.kt */
public final class BottomSheetSelectorDialog0 extends BaseBottomSheetMviDialog<e, f> {
    public HashMap o0;

    /* compiled from: BottomSheetSelectorDialog.kt */
    public static final class a<T, R> implements Function<T, R> {
        public static final a a = new a();

        public Object a(Object obj) {
            if (((Unit) obj) != null) {
                return BottomSheetSelectorDialogViewState0.a.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    public /* synthetic */ void C() {
        super.C();
        N();
    }

    public void N() {
        HashMap hashMap = this.o0;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public Class<f> O() {
        return BottomSheetSelectorDialogVm.class;
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [e.a.a.a.b.BaseBottomSheetMviDialog, androidx.fragment.app.Fragment, e.a.a.a.c.c.BottomSheetSelectorDialog0] */
    public void P() {
        BottomSheetSelectorNavigationDto0 bottomSheetSelectorNavigationDto0 = (BottomSheetSelectorNavigationDto0) Collections.a(this.g);
        if (bottomSheetSelectorNavigationDto0 != null) {
            ((BottomSheetSelectorDialogVm) h()).g.a((b) new BottomSheetSelectorDialogViewState0.b(bottomSheetSelectorNavigationDto0.b, bottomSheetSelectorNavigationDto0.c));
        }
    }

    /* JADX WARN: Type inference failed for: r6v0, types: [androidx.fragment.app.Fragment, e.a.a.a.c.c.BottomSheetSelectorDialog0] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [androidx.appcompat.widget.LinearLayoutCompat, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.Context, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void a(Object obj) {
        BottomSheetSelectorDialogViewState bottomSheetSelectorDialogViewState = (BottomSheetSelectorDialogViewState) obj;
        if (bottomSheetSelectorDialogViewState != null) {
            TextView textView = (TextView) c(d.dlg_bottom_sheet_selector_tv_title);
            Intrinsics.a((Object) textView, "dlg_bottom_sheet_selector_tv_title");
            textView.setText(bottomSheetSelectorDialogViewState.a);
            List<e.a.a.a.e.p.a> list = bottomSheetSelectorDialogViewState.b;
            LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) c(d.dlg_bottom_sheet_selector_ll_items);
            Intrinsics.a((Object) linearLayoutCompat, "dlg_bottom_sheet_selector_ll_items");
            if (linearLayoutCompat.getChildCount() == 0) {
                Iterator<T> it = list.iterator();
                while (it.hasNext()) {
                    BottomSheetSelectorNavigationDto bottomSheetSelectorNavigationDto = (BottomSheetSelectorNavigationDto) it.next();
                    Context J = J();
                    Intrinsics.a((Object) J, "requireContext()");
                    BottomSheetSelectorView bottomSheetSelectorView = new BottomSheetSelectorView(J, null, 0, 6);
                    bottomSheetSelectorView.setItem(bottomSheetSelectorNavigationDto);
                    bottomSheetSelectorView.setOnClickListener(new BottomSheetSelectorDialog(bottomSheetSelectorNavigationDto, this));
                    ((LinearLayoutCompat) c(d.dlg_bottom_sheet_selector_ll_items)).addView(bottomSheetSelectorView);
                }
                return;
            }
            return;
        }
        Intrinsics.a("vs");
        throw null;
    }

    public void b(Bundle bundle) {
        CoreComponent coreComponent = CoreComponentsHolder.b;
        if (coreComponent != null) {
            coreComponent.a(super.m0);
            super.b(bundle);
            return;
        }
        Intrinsics.b("coreComponent");
        throw null;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [androidx.fragment.app.Fragment, e.a.a.a.c.c.BottomSheetSelectorDialog0] */
    public View c(int i2) {
        if (this.o0 == null) {
            this.o0 = new HashMap();
        }
        View view = (View) this.o0.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View view2 = this.H;
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i2);
        this.o0.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<R>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i._Arrays.a(java.util.Collection, java.lang.Iterable):java.util.List<T>
     arg types: [java.util.List, java.util.List<R>]
     candidates:
      n.i._Arrays.a(java.lang.Iterable, java.util.Collection):C
      n.i._Arrays.a(java.util.Collection, java.lang.Iterable):java.util.List<T> */
    public List<Observable<? extends b>> g() {
        List c = c.c(b());
        TextView textView = (TextView) c(d.dlg_bottom_sheet_selector_tv_close);
        Intrinsics.a((Object) textView, "dlg_bottom_sheet_selector_tv_close");
        Observable<R> c2 = c.a((View) textView).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c2, "RxView.clicks(this).map(AnyToUnit)");
        return _Arrays.a((Collection) c, (Iterable) c.c(c2.c((Function) a.a)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        if (layoutInflater != null) {
            return layoutInflater.inflate(e.dlg_bottom_sheet_selector, viewGroup, false);
        }
        Intrinsics.a("inflater");
        throw null;
    }
}
