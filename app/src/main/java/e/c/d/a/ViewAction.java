package e.c.d.a;

import j.e.b.PublishRelay;
import j.e.b.Relay;
import l.b.t.Consumer;
import n.n.c.Intrinsics;

/* compiled from: ViewAction.kt */
public final class ViewAction<T> extends Bind<T> {
    public final Relay<T> a;
    public final Consumer<T> b;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [j.e.b.PublishRelay, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public ViewAction() {
        PublishRelay publishRelay = new PublishRelay();
        Intrinsics.a((Object) publishRelay, "PublishRelay.create<T>()");
        this.a = publishRelay;
        this.b = publishRelay;
    }

    public Relay<T> a() {
        return this.a;
    }
}
