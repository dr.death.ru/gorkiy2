package e.c.c;

import android.os.Parcel;
import android.os.Parcelable;
import n.n.c.Intrinsics;

/* compiled from: BaseViewState.kt */
public final class BaseViewState4 extends BaseViewState0 implements Parcelable {
    public static final Parcelable.Creator CREATOR = new a();
    public static final BaseViewState4 b = new BaseViewState4();

    public static class a implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            if (parcel == null) {
                Intrinsics.a("in");
                throw null;
            } else if (parcel.readInt() != 0) {
                return BaseViewState4.b;
            } else {
                return null;
            }
        }

        public final Object[] newArray(int i2) {
            return new BaseViewState4[i2];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeInt(1);
        } else {
            Intrinsics.a("parcel");
            throw null;
        }
    }
}
