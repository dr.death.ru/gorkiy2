package n.i;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import n.n.c.ArrayIterator;
import n.n.c.CollectionToArray;
import n.n.c.Intrinsics;
import n.n.c.t.a;

/* compiled from: Collections.kt */
public final class Collections0<T> implements Collection<T>, a {
    public final T[] b;
    public final boolean c;

    public Collections0(T[] tArr, boolean z) {
        if (tArr != null) {
            this.b = tArr;
            this.c = z;
            return;
        }
        Intrinsics.a("values");
        throw null;
    }

    public boolean add(T t2) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean addAll(Collection<? extends T> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.Object, T]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0027 A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean contains(java.lang.Object r6) {
        /*
            r5 = this;
            T[] r0 = r5.b
            if (r0 == 0) goto L_0x0029
            r1 = 0
            if (r6 != 0) goto L_0x0013
            int r6 = r0.length
            r2 = 0
        L_0x0009:
            if (r2 >= r6) goto L_0x0024
            r3 = r0[r2]
            if (r3 != 0) goto L_0x0010
            goto L_0x0025
        L_0x0010:
            int r2 = r2 + 1
            goto L_0x0009
        L_0x0013:
            int r2 = r0.length
            r3 = 0
        L_0x0015:
            if (r3 >= r2) goto L_0x0024
            r4 = r0[r3]
            boolean r4 = n.n.c.Intrinsics.a(r6, r4)
            if (r4 == 0) goto L_0x0021
            r2 = r3
            goto L_0x0025
        L_0x0021:
            int r3 = r3 + 1
            goto L_0x0015
        L_0x0024:
            r2 = -1
        L_0x0025:
            if (r2 < 0) goto L_0x0028
            r1 = 1
        L_0x0028:
            return r1
        L_0x0029:
            java.lang.String r6 = "$this$contains"
            n.n.c.Intrinsics.a(r6)
            r6 = 0
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: n.i.Collections0.contains(java.lang.Object):boolean");
    }

    public boolean containsAll(Collection<? extends Object> collection) {
        if (collection == null) {
            Intrinsics.a("elements");
            throw null;
        } else if (collection.isEmpty()) {
            return true;
        } else {
            Iterator<T> it = collection.iterator();
            while (it.hasNext()) {
                if (!contains(it.next())) {
                    return false;
                }
            }
            return true;
        }
    }

    public boolean isEmpty() {
        return this.b.length == 0;
    }

    public Iterator<T> iterator() {
        T[] tArr = this.b;
        if (tArr != null) {
            return new ArrayIterator(tArr);
        }
        Intrinsics.a("array");
        throw null;
    }

    public boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean removeAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean retainAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final int size() {
        return this.b.length;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.Object[], java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final Object[] toArray() {
        T[] tArr = this.b;
        boolean z = this.c;
        Class<Object[]> cls = Object[].class;
        if (tArr == null) {
            Intrinsics.a("$this$copyToArrayOfAny");
            throw null;
        } else if (z && Intrinsics.a(tArr.getClass(), cls)) {
            return tArr;
        } else {
            Object[] copyOf = Arrays.copyOf(tArr, tArr.length, cls);
            Intrinsics.a((Object) copyOf, "java.util.Arrays.copyOf(… Array<Any?>::class.java)");
            return copyOf;
        }
    }

    public <T> T[] toArray(T[] tArr) {
        return CollectionToArray.a(this, tArr);
    }
}
