package n.i;

import java.util.Iterator;
import n.q.Sequence;

/* compiled from: Sequences.kt */
public final class Sequences implements Sequence<T> {
    public final /* synthetic */ Iterable a;

    public Sequences(Iterable iterable) {
        this.a = iterable;
    }

    public Iterator<T> iterator() {
        return this.a.iterator();
    }
}
