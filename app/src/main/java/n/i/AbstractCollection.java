package n.i;

import java.util.Collection;
import java.util.Iterator;
import kotlin.TypeCastException;
import n.n.b.Functions0;
import n.n.c.CollectionToArray;
import n.n.c.Intrinsics;
import n.n.c.j;

/* compiled from: AbstractCollection.kt */
public abstract class AbstractCollection<E> implements Collection<E>, n.n.c.t.a {

    /* compiled from: AbstractCollection.kt */
    public static final class a extends j implements Functions0<E, CharSequence> {
        public final /* synthetic */ AbstractCollection c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(AbstractCollection abstractCollection) {
            super(1);
            this.c = abstractCollection;
        }

        public Object a(Object obj) {
            return obj == this.c ? "(this Collection)" : String.valueOf(obj);
        }
    }

    public boolean add(E e2) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean addAll(Collection<? extends E> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public abstract int c();

    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean contains(Object obj) {
        if (isEmpty()) {
            return false;
        }
        for (Object a2 : this) {
            if (Intrinsics.a(a2, obj)) {
                return true;
            }
        }
        return false;
    }

    public boolean containsAll(Collection<? extends Object> collection) {
        if (collection == null) {
            Intrinsics.a("elements");
            throw null;
        } else if (collection.isEmpty()) {
            return true;
        } else {
            Iterator<T> it = collection.iterator();
            while (it.hasNext()) {
                if (!contains(it.next())) {
                    return false;
                }
            }
            return true;
        }
    }

    public boolean isEmpty() {
        return c() == 0;
    }

    public boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean removeAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean retainAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final /* bridge */ int size() {
        return c();
    }

    public Object[] toArray() {
        return CollectionToArray.a(this);
    }

    public String toString() {
        return _Arrays.a(this, ", ", "[", "]", 0, (CharSequence) null, new a(this), 24);
    }

    public <T> T[] toArray(T[] tArr) {
        if (tArr != null) {
            T[] a2 = CollectionToArray.a(this, tArr);
            if (a2 != null) {
                return a2;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
        Intrinsics.a("array");
        throw null;
    }
}
