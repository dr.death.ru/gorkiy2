package n.l.e.a;

import androidx.recyclerview.widget.RecyclerView;
import j.c.a.a.c.n.c;
import n.Result;
import n.i.Collections;
import n.l.Continuation;
import n.l.ContinuationInterceptor;
import n.l.CoroutineContext;
import n.l.d.Intrinsics;
import r.KotlinExtensions5;

/* compiled from: ContinuationImpl.kt */
public abstract class ContinuationImpl implements Continuation<Object>, Object {
    public final Continuation<Object> b;

    public ContinuationImpl(Continuation<Object> continuation) {
        this.b = continuation;
    }

    public final void a(Object obj) {
        ContinuationImpl continuationImpl = this;
        while (true) {
            Continuation continuation = continuationImpl.b;
            if (continuation != null) {
                try {
                    KotlinExtensions5 kotlinExtensions5 = (KotlinExtensions5) continuationImpl;
                    kotlinExtensions5.f3167e = obj;
                    kotlinExtensions5.f3168f |= RecyclerView.UNDEFINED_DURATION;
                    obj = Collections.a((Exception) null, (Continuation<?>) kotlinExtensions5);
                    if (obj != Intrinsics.COROUTINE_SUSPENDED) {
                        Result.a(obj);
                        ContinuationImpl1 continuationImpl1 = (ContinuationImpl1) continuationImpl;
                        Continuation<Object> continuation2 = continuationImpl1.c;
                        if (!(continuation2 == null || continuation2 == continuationImpl1)) {
                            CoroutineContext.a a = continuationImpl1.c().a(ContinuationInterceptor.a);
                            if (a != null) {
                                ((ContinuationInterceptor) a).a(continuation2);
                            } else {
                                n.n.c.Intrinsics.a();
                                throw null;
                            }
                        }
                        continuationImpl1.c = ContinuationImpl0.b;
                        if (continuation instanceof ContinuationImpl) {
                            continuationImpl = (ContinuationImpl) continuation;
                        } else {
                            continuation.a(obj);
                            return;
                        }
                    } else {
                        return;
                    }
                } catch (Throwable th) {
                    obj = c.a(th);
                }
            } else {
                n.n.c.Intrinsics.a();
                throw null;
            }
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r3v3, types: [java.lang.StackTraceElement] */
    /* JADX WARN: Type inference failed for: r3v8, types: [java.lang.Object] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.reflect.Field, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String toString() {
        /*
            r10 = this;
            java.lang.String r0 = "Continuation at "
            java.lang.StringBuilder r0 = j.a.a.a.outline.a(r0)
            r1 = 0
            java.lang.Class r2 = r10.getClass()
            java.lang.Class<n.l.e.a.DebugMetadata> r3 = n.l.e.a.DebugMetadata.class
            java.lang.annotation.Annotation r2 = r2.getAnnotation(r3)
            n.l.e.a.DebugMetadata r2 = (n.l.e.a.DebugMetadata) r2
            if (r2 == 0) goto L_0x0124
            int r3 = r2.v()
            r4 = 1
            if (r3 > r4) goto L_0x00fc
            r3 = 0
            r5 = -1
            java.lang.Class r6 = r10.getClass()     // Catch:{ Exception -> 0x0045 }
            java.lang.String r7 = "label"
            java.lang.reflect.Field r6 = r6.getDeclaredField(r7)     // Catch:{ Exception -> 0x0045 }
            java.lang.String r7 = "field"
            n.n.c.Intrinsics.a(r6, r7)     // Catch:{ Exception -> 0x0045 }
            r6.setAccessible(r4)     // Catch:{ Exception -> 0x0045 }
            java.lang.Object r6 = r6.get(r10)     // Catch:{ Exception -> 0x0045 }
            boolean r7 = r6 instanceof java.lang.Integer     // Catch:{ Exception -> 0x0045 }
            if (r7 != 0) goto L_0x0039
            r6 = r1
        L_0x0039:
            java.lang.Integer r6 = (java.lang.Integer) r6     // Catch:{ Exception -> 0x0045 }
            if (r6 == 0) goto L_0x0042
            int r6 = r6.intValue()     // Catch:{ Exception -> 0x0045 }
            goto L_0x0043
        L_0x0042:
            r6 = 0
        L_0x0043:
            int r6 = r6 - r4
            goto L_0x0046
        L_0x0045:
            r6 = -1
        L_0x0046:
            if (r6 >= 0) goto L_0x0049
            goto L_0x004f
        L_0x0049:
            int[] r4 = r2.l()
            r5 = r4[r6]
        L_0x004f:
            n.l.e.a.DebugMetadata0 r4 = n.l.e.a.DebugMetadata0.c
            n.l.e.a.DebugMetadata0$a r4 = n.l.e.a.DebugMetadata0.b
            if (r4 == 0) goto L_0x0056
            goto L_0x0099
        L_0x0056:
            java.lang.Class<java.lang.Class> r4 = java.lang.Class.class
            java.lang.String r6 = "getModule"
            java.lang.Class[] r7 = new java.lang.Class[r3]     // Catch:{ Exception -> 0x0095 }
            java.lang.reflect.Method r4 = r4.getDeclaredMethod(r6, r7)     // Catch:{ Exception -> 0x0095 }
            java.lang.Class r6 = r10.getClass()     // Catch:{ Exception -> 0x0095 }
            java.lang.ClassLoader r6 = r6.getClassLoader()     // Catch:{ Exception -> 0x0095 }
            java.lang.String r7 = "java.lang.Module"
            java.lang.Class r6 = r6.loadClass(r7)     // Catch:{ Exception -> 0x0095 }
            java.lang.String r7 = "getDescriptor"
            java.lang.Class[] r8 = new java.lang.Class[r3]     // Catch:{ Exception -> 0x0095 }
            java.lang.reflect.Method r6 = r6.getDeclaredMethod(r7, r8)     // Catch:{ Exception -> 0x0095 }
            java.lang.Class r7 = r10.getClass()     // Catch:{ Exception -> 0x0095 }
            java.lang.ClassLoader r7 = r7.getClassLoader()     // Catch:{ Exception -> 0x0095 }
            java.lang.String r8 = "java.lang.module.ModuleDescriptor"
            java.lang.Class r7 = r7.loadClass(r8)     // Catch:{ Exception -> 0x0095 }
            java.lang.String r8 = "name"
            java.lang.Class[] r9 = new java.lang.Class[r3]     // Catch:{ Exception -> 0x0095 }
            java.lang.reflect.Method r7 = r7.getDeclaredMethod(r8, r9)     // Catch:{ Exception -> 0x0095 }
            n.l.e.a.DebugMetadata0$a r8 = new n.l.e.a.DebugMetadata0$a     // Catch:{ Exception -> 0x0095 }
            r8.<init>(r4, r6, r7)     // Catch:{ Exception -> 0x0095 }
            n.l.e.a.DebugMetadata0.b = r8     // Catch:{ Exception -> 0x0095 }
            r4 = r8
            goto L_0x0099
        L_0x0095:
            n.l.e.a.DebugMetadata0$a r4 = n.l.e.a.DebugMetadata0.a
            n.l.e.a.DebugMetadata0.b = r4
        L_0x0099:
            n.l.e.a.DebugMetadata0$a r6 = n.l.e.a.DebugMetadata0.a
            if (r4 != r6) goto L_0x009e
            goto L_0x00ce
        L_0x009e:
            java.lang.reflect.Method r6 = r4.a
            if (r6 == 0) goto L_0x00ce
            java.lang.Class r7 = r10.getClass()
            java.lang.Object[] r8 = new java.lang.Object[r3]
            java.lang.Object r6 = r6.invoke(r7, r8)
            if (r6 == 0) goto L_0x00ce
            java.lang.reflect.Method r7 = r4.b
            if (r7 == 0) goto L_0x00ce
            java.lang.Object[] r8 = new java.lang.Object[r3]
            java.lang.Object r6 = r7.invoke(r6, r8)
            if (r6 == 0) goto L_0x00ce
            java.lang.reflect.Method r4 = r4.c
            if (r4 == 0) goto L_0x00c5
            java.lang.Object[] r3 = new java.lang.Object[r3]
            java.lang.Object r3 = r4.invoke(r6, r3)
            goto L_0x00c6
        L_0x00c5:
            r3 = r1
        L_0x00c6:
            boolean r4 = r3 instanceof java.lang.String
            if (r4 != 0) goto L_0x00cb
            goto L_0x00cc
        L_0x00cb:
            r1 = r3
        L_0x00cc:
            java.lang.String r1 = (java.lang.String) r1
        L_0x00ce:
            if (r1 != 0) goto L_0x00d5
            java.lang.String r1 = r2.c()
            goto L_0x00ed
        L_0x00d5:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r1)
            r1 = 47
            r3.append(r1)
            java.lang.String r1 = r2.c()
            r3.append(r1)
            java.lang.String r1 = r3.toString()
        L_0x00ed:
            java.lang.StackTraceElement r3 = new java.lang.StackTraceElement
            java.lang.String r4 = r2.m()
            java.lang.String r2 = r2.f()
            r3.<init>(r1, r4, r2, r5)
            r1 = r3
            goto L_0x0124
        L_0x00fc:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Debug metadata version mismatch. Expected: "
            r0.append(r1)
            r0.append(r4)
            java.lang.String r1 = ", got "
            r0.append(r1)
            r0.append(r3)
            java.lang.String r1 = ". Please update the Kotlin standard library."
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        L_0x0124:
            if (r1 == 0) goto L_0x0127
            goto L_0x012f
        L_0x0127:
            java.lang.Class r1 = r10.getClass()
            java.lang.String r1 = r1.getName()
        L_0x012f:
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: n.l.e.a.ContinuationImpl.toString():java.lang.String");
    }
}
