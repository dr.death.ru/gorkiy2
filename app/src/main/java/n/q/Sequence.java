package n.q;

import java.util.Iterator;

/* compiled from: Sequence.kt */
public interface Sequence<T> {
    Iterator<T> iterator();
}
