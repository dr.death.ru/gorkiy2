package n.j;

import java.util.Comparator;
import n.n.c.Intrinsics;

/* compiled from: Comparisons.kt */
public final class Comparisons implements Comparator<Comparable<? super Object>> {
    public static final Comparisons b = new Comparisons();

    public int compare(Object obj, Object obj2) {
        Comparable comparable = (Comparable) obj;
        Comparable comparable2 = (Comparable) obj2;
        if (comparable == null) {
            Intrinsics.a("a");
            throw null;
        } else if (comparable2 != null) {
            return comparable.compareTo(comparable2);
        } else {
            Intrinsics.a("b");
            throw null;
        }
    }

    public final Comparator<Comparable<Object>> reversed() {
        return Comparisons0.b;
    }
}
