package n.n.c;

import j.a.a.a.outline;
import n.p.KCallable;
import n.p.KProperty;

public abstract class PropertyReference extends CallableReference implements KProperty {
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof PropertyReference) {
            PropertyReference propertyReference = (PropertyReference) obj;
            if (!g().equals(super.g()) || !f().equals(super.f()) || !i().equals(super.i()) || !Intrinsics.a(super.c, super.c)) {
                return false;
            }
            return true;
        } else if (obj instanceof KProperty) {
            return obj.equals(d());
        } else {
            return false;
        }
    }

    public int hashCode() {
        int hashCode = f().hashCode();
        return i().hashCode() + ((hashCode + (g().hashCode() * 31)) * 31);
    }

    public String toString() {
        KCallable d = d();
        if (d != this) {
            return d.toString();
        }
        StringBuilder a = outline.a("property ");
        a.append(f());
        a.append(" (Kotlin reflection is not available)");
        return a.toString();
    }

    public KProperty h() {
        return (KProperty) super.h();
    }
}
