package l.b.u.e.c;

import j.c.a.a.c.n.c;
import java.util.concurrent.atomic.AtomicReference;
import l.b.Observable;
import l.b.ObservableSource;
import l.b.Observer;
import l.b.s.Disposable;
import l.b.t.Function;
import l.b.u.a.DisposableHelper;
import l.b.u.a.EmptyDisposable;
import l.b.u.b.ObjectHelper;
import l.b.x.PublishSubject;

public final class ObservablePublishSelector<T, R> extends AbstractObservableWithUpstream<T, R> {
    public final Function<? super Observable<T>, ? extends ObservableSource<R>> c;

    public static final class a<T, R> implements Observer<T> {
        public final PublishSubject<T> b;
        public final AtomicReference<l.b.s.b> c;

        public a(PublishSubject<T> publishSubject, AtomicReference<l.b.s.b> atomicReference) {
            this.b = publishSubject;
            this.c = atomicReference;
        }

        public void a(Disposable disposable) {
            DisposableHelper.b(this.c, disposable);
        }

        public void b(T t2) {
            this.b.b((Object) t2);
        }

        public void a(Throwable th) {
            this.b.a(th);
        }

        public void a() {
            this.b.a();
        }
    }

    public ObservablePublishSelector(ObservableSource<T> observableSource, Function<? super Observable<T>, ? extends ObservableSource<R>> function) {
        super(observableSource);
        this.c = function;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [? extends l.b.ObservableSource<R>, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public void b(Observer<? super R> observer) {
        PublishSubject publishSubject = new PublishSubject();
        try {
            Object a2 = this.c.a(publishSubject);
            ObjectHelper.a((Object) a2, "The selector returned a null ObservableSource");
            ObservableSource observableSource = (ObservableSource) a2;
            b bVar = new b(observer);
            observableSource.a(bVar);
            super.b.a(new a(publishSubject, bVar));
        } catch (Throwable th) {
            c.c(th);
            observer.a((Disposable) EmptyDisposable.INSTANCE);
            observer.a(th);
        }
    }

    public static final class b<T, R> extends AtomicReference<l.b.s.b> implements Observer<R>, l.b.s.b {
        public final Observer<? super R> b;
        public Disposable c;

        public b(Observer<? super R> observer) {
            this.b = observer;
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [l.b.s.Disposable, l.b.u.e.c.ObservablePublishSelector$b] */
        public void a(Disposable disposable) {
            if (DisposableHelper.a(this.c, disposable)) {
                this.c = disposable;
                this.b.a((Disposable) this);
            }
        }

        public void b(R r2) {
            this.b.b(r2);
        }

        public void f() {
            this.c.f();
            DisposableHelper.a(super);
        }

        public boolean g() {
            return this.c.g();
        }

        public void a(Throwable th) {
            DisposableHelper.a(super);
            this.b.a(th);
        }

        public void a() {
            DisposableHelper.a(super);
            this.b.a();
        }
    }
}
