package l.b.u.e.d;

import io.reactivex.exceptions.CompositeException;
import j.c.a.a.c.n.c;
import java.util.concurrent.atomic.AtomicReference;
import l.b.Single;
import l.b.SingleObserver;
import l.b.SingleSource;
import l.b.s.Disposable;
import l.b.s.b;
import l.b.t.Function;
import l.b.u.a.DisposableHelper;
import l.b.u.b.ObjectHelper;
import l.b.u.d.ResumeSingleObserver;

public final class SingleResumeNext<T> extends Single<T> {
    public final SingleSource<? extends T> a;
    public final Function<? super Throwable, ? extends SingleSource<? extends T>> b;

    public SingleResumeNext(SingleSource<? extends T> singleSource, Function<? super Throwable, ? extends SingleSource<? extends T>> function) {
        this.a = singleSource;
        this.b = function;
    }

    public void b(SingleObserver<? super T> singleObserver) {
        this.a.a(new a(singleObserver, this.b));
    }

    public static final class a<T> extends AtomicReference<b> implements SingleObserver<T>, b {
        public final SingleObserver<? super T> b;
        public final Function<? super Throwable, ? extends SingleSource<? extends T>> c;

        public a(SingleObserver<? super T> singleObserver, Function<? super Throwable, ? extends SingleSource<? extends T>> function) {
            this.b = singleObserver;
            this.c = function;
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [l.b.s.Disposable, java.util.concurrent.atomic.AtomicReference, l.b.u.e.d.SingleResumeNext$a] */
        public void a(Disposable disposable) {
            if (DisposableHelper.b(super, disposable)) {
                this.b.a((Disposable) this);
            }
        }

        public void f() {
            DisposableHelper.a(super);
        }

        public boolean g() {
            return DisposableHelper.a((Disposable) get());
        }

        public void a(T t2) {
            this.b.a((Object) t2);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
         arg types: [? extends l.b.SingleSource<? extends T>, java.lang.String]
         candidates:
          l.b.u.b.ObjectHelper.a(int, java.lang.String):int
          l.b.u.b.ObjectHelper.a(long, long):int
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
        public void a(Throwable th) {
            try {
                Object a = this.c.a(th);
                ObjectHelper.a((Object) a, "The nextFunction returned a null SingleSource.");
                ((SingleSource) a).a(new ResumeSingleObserver(super, this.b));
            } catch (Throwable th2) {
                c.c(th2);
                this.b.a((Throwable) new CompositeException(th, th2));
            }
        }
    }
}
