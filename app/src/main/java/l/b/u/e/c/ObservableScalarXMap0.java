package l.b.u.e.c;

import j.c.a.a.c.n.c;
import java.util.concurrent.Callable;
import l.b.Observable;
import l.b.ObservableSource;
import l.b.Observer;
import l.b.s.Disposable;
import l.b.t.Function;
import l.b.u.a.EmptyDisposable;
import l.b.u.b.ObjectHelper;

/* compiled from: ObservableScalarXMap */
public final class ObservableScalarXMap0<T, R> extends Observable<R> {
    public final T b;
    public final Function<? super T, ? extends ObservableSource<? extends R>> c;

    public ObservableScalarXMap0(T t2, Function<? super T, ? extends ObservableSource<? extends R>> function) {
        this.b = t2;
        this.c = function;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
    /* JADX WARN: Type inference failed for: r1v4, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
    /* JADX WARN: Type inference failed for: r1v5, types: [l.b.s.Disposable, l.b.u.e.c.ObservableScalarXMap] */
    /* JADX WARN: Type inference failed for: r0v7, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [? extends l.b.ObservableSource<? extends R>, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public void b(Observer<? super R> observer) {
        try {
            Object a = this.c.a(this.b);
            ObjectHelper.a((Object) a, "The mapper returned a null ObservableSource");
            ObservableSource observableSource = (ObservableSource) a;
            if (observableSource instanceof Callable) {
                try {
                    Object call = ((Callable) observableSource).call();
                    if (call == null) {
                        observer.a((Disposable) EmptyDisposable.INSTANCE);
                        observer.a();
                        return;
                    }
                    ? observableScalarXMap = new ObservableScalarXMap(observer, call);
                    observer.a((Disposable) observableScalarXMap);
                    observableScalarXMap.run();
                } catch (Throwable th) {
                    c.c(th);
                    observer.a((Disposable) EmptyDisposable.INSTANCE);
                    observer.a(th);
                }
            } else {
                observableSource.a(observer);
            }
        } catch (Throwable th2) {
            observer.a((Disposable) EmptyDisposable.INSTANCE);
            observer.a(th2);
        }
    }
}
