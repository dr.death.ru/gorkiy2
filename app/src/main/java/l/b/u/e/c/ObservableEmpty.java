package l.b.u.e.c;

import l.b.Observable;
import l.b.Observer;
import l.b.s.Disposable;
import l.b.u.a.EmptyDisposable;
import l.b.u.c.ScalarCallable;

public final class ObservableEmpty extends Observable<Object> implements ScalarCallable<Object> {
    public static final Observable<Object> b = new ObservableEmpty();

    /* JADX WARN: Type inference failed for: r0v0, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
    public void b(Observer<? super Object> observer) {
        observer.a((Disposable) EmptyDisposable.INSTANCE);
        observer.a();
    }

    public Object call() {
        return null;
    }
}
