package l.b.u.e.c;

import j.c.a.a.c.n.c;
import java.util.NoSuchElementException;
import l.b.Observable;
import l.b.ObservableSource;
import l.b.Observer;
import l.b.Single;
import l.b.SingleObserver;
import l.b.s.Disposable;
import l.b.s.b;
import l.b.u.a.DisposableHelper;
import l.b.u.c.FuseToObservable;

public final class ObservableElementAtSingle<T> extends Single<T> implements FuseToObservable<T> {
    public final ObservableSource<T> a;
    public final long b;
    public final T c;

    public ObservableElementAtSingle(ObservableSource<T> observableSource, long j2, T t2) {
        this.a = observableSource;
        this.b = j2;
        this.c = t2;
    }

    public Observable<T> a() {
        return new ObservableElementAt(this.a, this.b, this.c, true);
    }

    public void b(SingleObserver<? super T> singleObserver) {
        this.a.a(new a(singleObserver, this.b, this.c));
    }

    public static final class a<T> implements Observer<T>, b {
        public final SingleObserver<? super T> b;
        public final long c;
        public final T d;

        /* renamed from: e  reason: collision with root package name */
        public Disposable f2706e;

        /* renamed from: f  reason: collision with root package name */
        public long f2707f;
        public boolean g;

        public a(SingleObserver<? super T> singleObserver, long j2, T t2) {
            this.b = singleObserver;
            this.c = j2;
            this.d = t2;
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [l.b.s.Disposable, l.b.u.e.c.ObservableElementAtSingle$a] */
        public void a(Disposable disposable) {
            if (DisposableHelper.a(this.f2706e, disposable)) {
                this.f2706e = disposable;
                this.b.a((Disposable) this);
            }
        }

        public void b(T t2) {
            if (!this.g) {
                long j2 = this.f2707f;
                if (j2 == this.c) {
                    this.g = true;
                    this.f2706e.f();
                    this.b.a((Object) t2);
                    return;
                }
                this.f2707f = j2 + 1;
            }
        }

        public void f() {
            this.f2706e.f();
        }

        public boolean g() {
            return this.f2706e.g();
        }

        public void a(Throwable th) {
            if (this.g) {
                c.b(th);
                return;
            }
            this.g = true;
            this.b.a(th);
        }

        public void a() {
            if (!this.g) {
                this.g = true;
                T t2 = this.d;
                if (t2 != null) {
                    this.b.a((Object) t2);
                } else {
                    this.b.a((Throwable) new NoSuchElementException());
                }
            }
        }
    }
}
