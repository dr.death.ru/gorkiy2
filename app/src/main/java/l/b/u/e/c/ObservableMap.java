package l.b.u.e.c;

import l.b.ObservableSource;
import l.b.Observer;
import l.b.t.Function;
import l.b.u.b.ObjectHelper;
import l.b.u.d.BasicFuseableObserver;

public final class ObservableMap<T, U> extends AbstractObservableWithUpstream<T, U> {
    public final Function<? super T, ? extends U> c;

    public static final class a<T, U> extends BasicFuseableObserver<T, U> {
        public final Function<? super T, ? extends U> g;

        public a(Observer<? super U> observer, Function<? super T, ? extends U> function) {
            super(observer);
            this.g = function;
        }

        /*  JADX ERROR: StackOverflow in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxOverflowException: 
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:52)
            	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:86)
            */
        public int a(int r1) {
            /*
                r0 = this;
                int r1 = r0.b(r1)
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: l.b.u.e.c.ObservableMap.a.a(int):int");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
         arg types: [? extends U, java.lang.String]
         candidates:
          l.b.u.b.ObjectHelper.a(int, java.lang.String):int
          l.b.u.b.ObjectHelper.a(long, long):int
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
        /*  JADX ERROR: StackOverflow in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxOverflowException: 
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:52)
            	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:86)
            */
        public void b(T r2) {
            /*
                r1 = this;
                boolean r0 = r1.f2677e
                if (r0 == 0) goto L_0x0005
                return
            L_0x0005:
                int r0 = r1.f2678f
                if (r0 == 0) goto L_0x0010
                l.b.Observer<? super R> r2 = r1.b
                r0 = 0
                r2.b(r0)
                return
            L_0x0010:
                l.b.t.Function<? super T, ? extends U> r0 = r1.g     // Catch:{ all -> 0x0021 }
                java.lang.Object r2 = r0.a(r2)     // Catch:{ all -> 0x0021 }
                java.lang.String r0 = "The mapper function returned a null value."
                l.b.u.b.ObjectHelper.a(r2, r0)     // Catch:{ all -> 0x0021 }
                l.b.Observer<? super R> r0 = r1.b
                r0.b(r2)
                return
            L_0x0021:
                r2 = move-exception
                r1.b(r2)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: l.b.u.e.c.ObservableMap.a.b(java.lang.Object):void");
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [l.b.u.c.QueueDisposable<T>, l.b.u.c.SimpleQueue] */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
         arg types: [U, java.lang.String]
         candidates:
          l.b.u.b.ObjectHelper.a(int, java.lang.String):int
          l.b.u.b.ObjectHelper.a(long, long):int
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
        public U poll() {
            Object poll = super.d.poll();
            if (poll == null) {
                return null;
            }
            U a = this.g.a(poll);
            ObjectHelper.a((Object) a, "The mapper function returned a null value.");
            return a;
        }
    }

    public ObservableMap(ObservableSource<T> observableSource, Function<? super T, ? extends U> function) {
        super(observableSource);
        this.c = function;
    }

    public void b(Observer<? super U> observer) {
        super.b.a(new a(observer, this.c));
    }
}
