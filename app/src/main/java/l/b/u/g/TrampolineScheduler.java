package l.b.u.g;

import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import l.b.Scheduler;
import l.b.s.Disposable;
import l.b.s.RunnableDisposable;
import l.b.u.a.EmptyDisposable;
import l.b.u.b.ObjectHelper;
import l.b.u.g.j;

public final class TrampolineScheduler extends Scheduler {
    public static final TrampolineScheduler a = new TrampolineScheduler();

    public static final class a implements Runnable {
        public final Runnable b;
        public final c c;
        public final long d;

        public a(Runnable runnable, c cVar, long j2) {
            this.b = runnable;
            this.c = cVar;
            this.d = j2;
        }

        public void run() {
            if (!this.c.f2771e) {
                long a = this.c.a(TimeUnit.MILLISECONDS);
                long j2 = this.d;
                if (j2 > a) {
                    try {
                        Thread.sleep(j2 - a);
                    } catch (InterruptedException e2) {
                        Thread.currentThread().interrupt();
                        j.c.a.a.c.n.c.b((Throwable) e2);
                        return;
                    }
                }
                if (!this.c.f2771e) {
                    this.b.run();
                }
            }
        }
    }

    public static final class b implements Comparable<j.b> {
        public final Runnable b;
        public final long c;
        public final int d;

        /* renamed from: e  reason: collision with root package name */
        public volatile boolean f2770e;

        public b(Runnable runnable, Long l2, int i2) {
            this.b = runnable;
            this.c = l2.longValue();
            this.d = i2;
        }

        public int compareTo(Object obj) {
            b bVar = (b) obj;
            int a = ObjectHelper.a(this.c, bVar.c);
            if (a != 0) {
                return a;
            }
            int i2 = this.d;
            int i3 = bVar.d;
            if (i2 < i3) {
                return -1;
            }
            return i2 > i3 ? 1 : 0;
        }
    }

    public static final class c extends Scheduler.b implements Disposable {
        public final PriorityBlockingQueue<j.b> b = new PriorityBlockingQueue<>();
        public final AtomicInteger c = new AtomicInteger();
        public final AtomicInteger d = new AtomicInteger();

        /* renamed from: e  reason: collision with root package name */
        public volatile boolean f2771e;

        public final class a implements Runnable {
            public final b b;

            public a(b bVar) {
                this.b = bVar;
            }

            public void run() {
                this.b.f2770e = true;
                c.this.b.remove(this.b);
            }
        }

        public Disposable a(Runnable runnable) {
            return a(runnable, a(TimeUnit.MILLISECONDS));
        }

        public void f() {
            this.f2771e = true;
        }

        public boolean g() {
            return this.f2771e;
        }

        public Disposable a(Runnable runnable, long j2, TimeUnit timeUnit) {
            long millis = timeUnit.toMillis(j2) + a(TimeUnit.MILLISECONDS);
            return a(new a(runnable, this, millis), millis);
        }

        /* JADX WARN: Type inference failed for: r2v8, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
        /* JADX WARN: Type inference failed for: r2v12, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
        /* JADX WARN: Type inference failed for: r2v13, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
         arg types: [l.b.u.g.TrampolineScheduler$c$a, java.lang.String]
         candidates:
          l.b.u.b.ObjectHelper.a(int, java.lang.String):int
          l.b.u.b.ObjectHelper.a(long, long):int
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
        public Disposable a(Runnable runnable, long j2) {
            if (this.f2771e) {
                return EmptyDisposable.INSTANCE;
            }
            b bVar = new b(runnable, Long.valueOf(j2), this.d.incrementAndGet());
            this.b.add(bVar);
            if (this.c.getAndIncrement() == 0) {
                int i2 = 1;
                while (!this.f2771e) {
                    b poll = this.b.poll();
                    if (poll == null) {
                        i2 = this.c.addAndGet(-i2);
                        if (i2 == 0) {
                            return EmptyDisposable.INSTANCE;
                        }
                    } else if (!poll.f2770e) {
                        poll.b.run();
                    }
                }
                this.b.clear();
                return EmptyDisposable.INSTANCE;
            }
            a aVar = new a(bVar);
            ObjectHelper.a((Object) aVar, "run is null");
            return new RunnableDisposable(aVar);
        }
    }

    public Scheduler.b a() {
        return new c();
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.Runnable, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public Disposable a(Runnable runnable, long j2, TimeUnit timeUnit) {
        try {
            timeUnit.sleep(j2);
            ObjectHelper.a((Object) runnable, "run is null");
            runnable.run();
        } catch (InterruptedException e2) {
            Thread.currentThread().interrupt();
            j.c.a.a.c.n.c.b((Throwable) e2);
        }
        return EmptyDisposable.INSTANCE;
    }

    /* JADX WARN: Type inference failed for: r2v1, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.Runnable, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public Disposable a(Runnable runnable) {
        ObjectHelper.a((Object) runnable, "run is null");
        runnable.run();
        return EmptyDisposable.INSTANCE;
    }
}
