package l.b.t;

public interface Action {
    void run();
}
