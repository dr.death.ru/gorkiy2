package l.b;

import j.c.a.a.c.n.c;
import java.util.concurrent.Callable;
import l.b.s.b;
import l.b.t.Action;
import l.b.t.Consumer;
import l.b.t.a;
import l.b.u.b.Functions;
import l.b.u.b.ObjectHelper;
import l.b.u.c.FuseToObservable;
import l.b.u.d.CallbackCompletableObserver;
import l.b.u.e.a.CompletableFromCallable;
import l.b.u.e.a.CompletablePeek;
import l.b.u.e.a.CompletableSubscribeOn;
import l.b.u.e.a.CompletableToObservable;

public abstract class Completable implements CompletableSource {
    public final Completable a(Action action) {
        Consumer<Object> consumer = Functions.d;
        Action action2 = Functions.c;
        return a(consumer, consumer, action, action2, action2, action2);
    }

    public final <T> Observable<T> b() {
        if (this instanceof FuseToObservable) {
            return ((FuseToObservable) this).a();
        }
        return new CompletableToObservable(this);
    }

    public abstract void b(CompletableObserver completableObserver);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [java.util.concurrent.Callable<?>, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public static b a(Callable<?> callable) {
        ObjectHelper.a((Object) callable, "callable is null");
        return new CompletableFromCallable(callable);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.t.Consumer<? super l.b.s.b>, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.t.Consumer<? super java.lang.Throwable>, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.t.a, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public final b a(Consumer<? super b> consumer, Consumer<? super Throwable> consumer2, a aVar, a aVar2, a aVar3, a aVar4) {
        ObjectHelper.a((Object) consumer, "onSubscribe is null");
        ObjectHelper.a((Object) consumer2, "onError is null");
        ObjectHelper.a((Object) aVar, "onComplete is null");
        ObjectHelper.a((Object) aVar2, "onTerminate is null");
        ObjectHelper.a((Object) aVar3, "onAfterTerminate is null");
        ObjectHelper.a((Object) aVar4, "onDispose is null");
        return new CompletablePeek(this, consumer, consumer2, aVar, aVar2, aVar3, aVar4);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.CompletableObserver, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public final void a(CompletableObserver completableObserver) {
        ObjectHelper.a((Object) completableObserver, "observer is null");
        try {
            ObjectHelper.a((Object) completableObserver, "The RxJavaPlugins.onSubscribe hook returned a null CompletableObserver. Please check the handler provided to RxJavaPlugins.setOnCompletableSubscribe for invalid null returns. Further reading: https://github.com/ReactiveX/RxJava/wiki/Plugins");
            b(completableObserver);
        } catch (NullPointerException e2) {
            throw e2;
        } catch (Throwable th) {
            c.c(th);
            c.b(th);
            NullPointerException nullPointerException = new NullPointerException("Actually not, but can't pass out an exception otherwise...");
            nullPointerException.initCause(th);
            throw nullPointerException;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.t.Consumer<? super java.lang.Throwable>, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.t.a, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public final b a(a aVar, Consumer<? super Throwable> consumer) {
        ObjectHelper.a((Object) consumer, "onError is null");
        ObjectHelper.a((Object) aVar, "onComplete is null");
        b callbackCompletableObserver = new CallbackCompletableObserver(consumer, aVar);
        a((CompletableObserver) callbackCompletableObserver);
        return callbackCompletableObserver;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.Scheduler, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public final Completable a(Scheduler scheduler) {
        ObjectHelper.a((Object) scheduler, "scheduler is null");
        return new CompletableSubscribeOn(this, scheduler);
    }
}
