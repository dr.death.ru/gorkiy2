package l.a.a.a;

import android.app.Activity;
import android.os.Bundle;
import l.a.a.a.ActivityLifecycleManager0;

/* compiled from: Fabric */
public class Fabric0 extends ActivityLifecycleManager0.b {
    public final /* synthetic */ Fabric a;

    public Fabric0(Fabric fabric) {
        this.a = fabric;
    }

    public void onActivityCreated(Activity activity, Bundle bundle) {
        this.a.a(activity);
    }

    public void onActivityResumed(Activity activity) {
        this.a.a(activity);
    }

    public void onActivityStarted(Activity activity) {
        this.a.a(activity);
    }
}
