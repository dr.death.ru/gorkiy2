package l.a.a.a;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import io.fabric.sdk.android.services.concurrency.UnmetDependencyException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import l.a.a.a.o.b.DataCollectionArbiter;
import l.a.a.a.o.b.IdManager;
import l.a.a.a.o.b.s;
import l.a.a.a.o.c.DependencyPriorityBlockingQueue;
import l.a.a.a.o.c.DependsOn;
import l.a.a.a.o.c.PriorityThreadPoolExecutor;
import l.a.a.a.o.c.Task;
import l.a.a.a.o.c.k;

public class Fabric {

    /* renamed from: l  reason: collision with root package name */
    public static volatile Fabric f2613l;

    /* renamed from: m  reason: collision with root package name */
    public static final DefaultLogger f2614m = new DefaultLogger();
    public final Context a;
    public final Map<Class<? extends k>, k> b;
    public final ExecutorService c;
    public final InitializationCallback<f> d;

    /* renamed from: e  reason: collision with root package name */
    public final InitializationCallback<?> f2615e;

    /* renamed from: f  reason: collision with root package name */
    public final IdManager f2616f;
    public ActivityLifecycleManager0 g;
    public WeakReference<Activity> h;

    /* renamed from: i  reason: collision with root package name */
    public AtomicBoolean f2617i = new AtomicBoolean(false);

    /* renamed from: j  reason: collision with root package name */
    public final DefaultLogger f2618j;

    /* renamed from: k  reason: collision with root package name */
    public final boolean f2619k;

    public Fabric(Context context, Map<Class<? extends k>, k> map, k kVar, Handler handler, c cVar, boolean z, i iVar, s sVar, Activity activity) {
        this.a = context;
        this.b = map;
        this.c = kVar;
        this.f2618j = cVar;
        this.f2619k = z;
        this.d = iVar;
        this.f2615e = new Fabric1(this, map.size());
        this.f2616f = sVar;
        a(activity);
    }

    public static <T extends k> T a(Class cls) {
        if (f2613l != null) {
            return (Kit) f2613l.b.get(cls);
        }
        throw new IllegalStateException("Must Initialize Fabric before using singleton()");
    }

    public static boolean b() {
        if (f2613l == null) {
            return false;
        }
        return f2613l.f2619k;
    }

    public static Fabric a(Context context, Kit... kitArr) {
        if (f2613l == null) {
            synchronized (Fabric.class) {
                if (f2613l == null) {
                    a aVar = new a(context);
                    aVar.a(kitArr);
                    a(aVar.a());
                }
            }
        }
        return f2613l;
    }

    /* JADX WARN: Type inference failed for: r10v0, types: [l.a.a.a.InitializationTask<Result>, l.a.a.a.o.c.Task] */
    /* JADX WARN: Type inference failed for: r13v3, types: [l.a.a.a.InitializationTask<Result>, l.a.a.a.o.c.Task] */
    /* JADX WARN: Type inference failed for: r15v3, types: [l.a.a.a.InitializationTask<Result>, l.a.a.a.o.c.Task] */
    public static void a(Fabric fabric) {
        StringBuilder sb;
        Fabric fabric2 = fabric;
        f2613l = fabric2;
        ActivityLifecycleManager0 activityLifecycleManager0 = new ActivityLifecycleManager0(fabric2.a);
        fabric2.g = activityLifecycleManager0;
        activityLifecycleManager0.a(new Fabric0(fabric2));
        Context context = fabric2.a;
        Future submit = fabric2.c.submit(new FabricKitsFinder(context.getPackageCodePath()));
        Collection<k> values = fabric2.b.values();
        Onboarding onboarding = new Onboarding(submit, values);
        ArrayList arrayList = new ArrayList(values);
        Collections.sort(arrayList);
        onboarding.injectParameters(context, fabric2, InitializationCallback.a, fabric2.f2616f);
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            ((Kit) it.next()).injectParameters(context, fabric2, fabric2.f2615e, fabric2.f2616f);
        }
        onboarding.initialize();
        if (a().a("Fabric", 3)) {
            sb = new StringBuilder("Initializing ");
            sb.append("io.fabric.sdk.android:fabric");
            sb.append(" [Version: ");
            sb.append("1.4.8.32");
            sb.append("], with the following kits:\n");
        } else {
            sb = null;
        }
        Iterator it2 = arrayList.iterator();
        while (it2.hasNext()) {
            Kit kit = (Kit) it2.next();
            kit.initializationTask.addDependency((Task) onboarding.initializationTask);
            Map<Class<? extends k>, k> map = fabric2.b;
            DependsOn dependsOn = kit.dependsOnAnnotation;
            if (dependsOn != null) {
                for (Class<?> cls : dependsOn.value()) {
                    if (cls.isInterface()) {
                        for (Kit next : map.values()) {
                            if (cls.isAssignableFrom(next.getClass())) {
                                kit.initializationTask.addDependency((Task) next.initializationTask);
                            }
                        }
                    } else if (map.get(cls) != null) {
                        kit.initializationTask.addDependency((Task) map.get(cls).initializationTask);
                    } else {
                        throw new UnmetDependencyException("Referenced Kit was null, does the kit exist?");
                    }
                }
            }
            kit.initialize();
            if (sb != null) {
                sb.append(kit.getIdentifier());
                sb.append(" [Version: ");
                sb.append(kit.getVersion());
                sb.append("]\n");
            }
        }
        if (sb != null) {
            DefaultLogger a2 = a();
            String sb2 = sb.toString();
            if (a2.a("Fabric", 3)) {
                Log.d("Fabric", sb2, null);
            }
        }
    }

    public static class a {
        public final Context a;
        public Kit[] b;
        public PriorityThreadPoolExecutor c;
        public Handler d;

        /* renamed from: e  reason: collision with root package name */
        public DefaultLogger f2620e;

        /* renamed from: f  reason: collision with root package name */
        public String f2621f;
        public InitializationCallback<f> g;

        public a(Context context) {
            if (context != null) {
                this.a = context;
                return;
            }
            throw new IllegalArgumentException("Context must not be null.");
        }

        public a a(Kit... kitArr) {
            if (this.b == null) {
                if (!DataCollectionArbiter.a(this.a).a()) {
                    ArrayList arrayList = new ArrayList();
                    boolean z = false;
                    for (Kit kit : kitArr) {
                        String identifier = kit.getIdentifier();
                        char c2 = 65535;
                        int hashCode = identifier.hashCode();
                        if (hashCode != 607220212) {
                            if (hashCode == 1830452504 && identifier.equals("com.crashlytics.sdk.android:crashlytics")) {
                                c2 = 0;
                            }
                        } else if (identifier.equals("com.crashlytics.sdk.android:answers")) {
                            c2 = 1;
                        }
                        if (c2 == 0 || c2 == 1) {
                            arrayList.add(kit);
                        } else if (!z) {
                            if (Fabric.a().a("Fabric", 5)) {
                                Log.w("Fabric", "Fabric will not initialize any kits when Firebase automatic data collection is disabled; to use Third-party kits with automatic data collection disabled, initialize these kits via non-Fabric means.", null);
                            }
                            z = true;
                        }
                    }
                    kitArr = (Kit[]) arrayList.toArray(new Kit[0]);
                }
                this.b = kitArr;
                return this;
            }
            throw new IllegalStateException("Kits already set.");
        }

        public Fabric a() {
            HashMap hashMap;
            if (this.c == null) {
                this.c = new PriorityThreadPoolExecutor(PriorityThreadPoolExecutor.b, PriorityThreadPoolExecutor.c, 1, TimeUnit.SECONDS, new DependencyPriorityBlockingQueue(), new PriorityThreadPoolExecutor.a(10));
            }
            if (this.d == null) {
                this.d = new Handler(Looper.getMainLooper());
            }
            if (this.f2620e == null) {
                this.f2620e = new DefaultLogger();
            }
            if (this.f2621f == null) {
                this.f2621f = this.a.getPackageName();
            }
            if (this.g == null) {
                this.g = InitializationCallback.a;
            }
            Kit[] kitArr = this.b;
            if (kitArr == null) {
                hashMap = new HashMap();
            } else {
                List asList = Arrays.asList(kitArr);
                HashMap hashMap2 = new HashMap(asList.size());
                Fabric.a(hashMap2, asList);
                hashMap = hashMap2;
            }
            Context applicationContext = this.a.getApplicationContext();
            IdManager idManager = new IdManager(applicationContext, this.f2621f, null, hashMap.values());
            PriorityThreadPoolExecutor priorityThreadPoolExecutor = this.c;
            Handler handler = this.d;
            DefaultLogger defaultLogger = this.f2620e;
            InitializationCallback<f> initializationCallback = this.g;
            Context context = this.a;
            return new Fabric(applicationContext, hashMap, priorityThreadPoolExecutor, handler, defaultLogger, false, initializationCallback, idManager, context instanceof Activity ? (Activity) context : null);
        }
    }

    public Fabric a(Activity activity) {
        this.h = new WeakReference<>(activity);
        return this;
    }

    public static DefaultLogger a() {
        if (f2613l == null) {
            return f2614m;
        }
        return f2613l.f2618j;
    }

    public static void a(Map<Class<? extends k>, k> map, Collection<? extends k> collection) {
        Iterator<? extends k> it = collection.iterator();
        while (it.hasNext()) {
            Kit kit = (Kit) it.next();
            map.put(kit.getClass(), kit);
            if (kit instanceof KitGroup) {
                a(map, ((KitGroup) kit).getKits());
            }
        }
    }
}
