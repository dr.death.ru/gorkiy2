package l.a.a.a.o.e;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.HashMap;

public class SystemKeyStore {
    public final KeyStore a;
    public final HashMap<Principal, X509Certificate> b;

    public SystemKeyStore(InputStream inputStream, String str) {
        BufferedInputStream bufferedInputStream;
        try {
            KeyStore instance = KeyStore.getInstance("BKS");
            bufferedInputStream = new BufferedInputStream(inputStream);
            instance.load(bufferedInputStream, str.toCharArray());
            bufferedInputStream.close();
            try {
                HashMap<Principal, X509Certificate> hashMap = new HashMap<>();
                Enumeration<String> aliases = instance.aliases();
                while (aliases.hasMoreElements()) {
                    X509Certificate x509Certificate = (X509Certificate) instance.getCertificate(aliases.nextElement());
                    if (x509Certificate != null) {
                        hashMap.put(x509Certificate.getSubjectX500Principal(), x509Certificate);
                    }
                }
                this.b = hashMap;
                this.a = instance;
            } catch (KeyStoreException e2) {
                throw new AssertionError(e2);
            }
        } catch (KeyStoreException e3) {
            throw new AssertionError(e3);
        } catch (NoSuchAlgorithmException e4) {
            throw new AssertionError(e4);
        } catch (CertificateException e5) {
            throw new AssertionError(e5);
        } catch (IOException e6) {
            throw new AssertionError(e6);
        } catch (Throwable th) {
            bufferedInputStream.close();
            throw th;
        }
    }

    public boolean a(X509Certificate x509Certificate) {
        X509Certificate x509Certificate2 = this.b.get(x509Certificate.getSubjectX500Principal());
        return x509Certificate2 != null && x509Certificate2.getPublicKey().equals(x509Certificate.getPublicKey());
    }
}
