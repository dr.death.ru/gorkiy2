package l.a.a.a.o.g;

public class AnalyticsSettingsData {
    public final String a;
    public final int b;
    public final int c;
    public final int d;

    /* renamed from: e  reason: collision with root package name */
    public final boolean f2656e;

    /* renamed from: f  reason: collision with root package name */
    public final boolean f2657f;
    public final boolean g;
    public final boolean h;

    /* renamed from: i  reason: collision with root package name */
    public final boolean f2658i;

    /* renamed from: j  reason: collision with root package name */
    public final int f2659j;

    public AnalyticsSettingsData(String str, int i2, int i3, int i4, int i5, boolean z, boolean z2, boolean z3, boolean z4, int i6, boolean z5) {
        this.a = str;
        this.b = i2;
        this.c = i3;
        this.d = i5;
        this.f2656e = z;
        this.f2657f = z2;
        this.g = z3;
        this.h = z4;
        this.f2659j = i6;
        this.f2658i = z5;
    }
}
