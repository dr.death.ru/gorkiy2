package l.a.a.a.o.b;

public class AdvertisingInfo {
    public final String a;
    public final boolean b;

    public AdvertisingInfo(String str, boolean z) {
        this.a = str;
        this.b = z;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || AdvertisingInfo.class != obj.getClass()) {
            return false;
        }
        AdvertisingInfo advertisingInfo = (AdvertisingInfo) obj;
        if (this.b != advertisingInfo.b) {
            return false;
        }
        String str = this.a;
        String str2 = advertisingInfo.a;
        return str == null ? str2 == null : str.equals(str2);
    }

    public int hashCode() {
        String str = this.a;
        return ((str != null ? str.hashCode() : 0) * 31) + (this.b ? 1 : 0);
    }
}
