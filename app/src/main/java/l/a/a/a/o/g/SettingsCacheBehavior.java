package l.a.a.a.o.g;

public enum SettingsCacheBehavior {
    USE_CACHE,
    SKIP_CACHE_LOOKUP,
    IGNORE_CACHE_EXPIRATION
}
