package l.a.a.a.o.c;

import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

public class PriorityFutureTask<V> extends FutureTask<V> implements Dependency<l>, i, l {
    public final Object b;

    public PriorityFutureTask(Callable<V> callable) {
        super(callable);
        Object obj;
        if (PriorityTask.isProperDelegate(callable)) {
            obj = (Dependency) callable;
        } else {
            obj = new PriorityTask();
        }
        this.b = obj;
    }

    public void addDependency(Object obj) {
        ((Dependency) ((PriorityProvider) f())).addDependency((Task) obj);
    }

    public boolean areDependenciesMet() {
        return ((Dependency) ((PriorityProvider) f())).areDependenciesMet();
    }

    public int compareTo(Object obj) {
        return ((PriorityProvider) f()).compareTo(obj);
    }

    public <T extends Dependency<l> & i & l> T f() {
        return (Dependency) this.b;
    }

    public Collection<l> getDependencies() {
        return ((Dependency) ((PriorityProvider) f())).getDependencies();
    }

    public Priority getPriority() {
        return ((PriorityProvider) f()).getPriority();
    }

    public boolean isFinished() {
        return ((Task) ((PriorityProvider) f())).isFinished();
    }

    public void setError(Throwable th) {
        ((Task) ((PriorityProvider) f())).setError(th);
    }

    public void setFinished(boolean z) {
        ((Task) ((PriorityProvider) f())).setFinished(z);
    }

    public PriorityFutureTask(Runnable runnable, V v) {
        super(runnable, v);
        Object obj;
        if (PriorityTask.isProperDelegate(runnable)) {
            obj = (Dependency) runnable;
        } else {
            obj = new PriorityTask();
        }
        this.b = obj;
    }
}
