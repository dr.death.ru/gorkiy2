package l.a.a.a.o.c;

import java.util.Collection;
import java.util.concurrent.Executor;
import l.a.a.a.o.c.AsyncTask;

public abstract class PriorityAsyncTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> implements Dependency<l>, i, l {

    /* renamed from: o  reason: collision with root package name */
    public final PriorityTask f2651o = new PriorityTask();

    public static class a<Result> implements Executor {
        public final Executor a;
        public final PriorityAsyncTask b;

        /* renamed from: l.a.a.a.o.c.PriorityAsyncTask$a$a  reason: collision with other inner class name */
        public class C0033a extends PriorityFutureTask<Result> {
            public C0033a(Runnable runnable, Object obj) {
                super(runnable, obj);
            }

            public <T extends Dependency<l> & i & l> T f() {
                return a.this.b;
            }
        }

        public a(Executor executor, PriorityAsyncTask priorityAsyncTask) {
            this.a = executor;
            this.b = priorityAsyncTask;
        }

        public void execute(Runnable runnable) {
            this.a.execute(new C0033a(runnable, null));
        }
    }

    /* renamed from: a */
    public void addDependency(Task task) {
        if (super.d == AsyncTask.g.PENDING) {
            this.f2651o.addDependency((Object) task);
            return;
        }
        throw new IllegalStateException("Must not add Dependency after task is running");
    }

    public boolean areDependenciesMet() {
        return this.f2651o.areDependenciesMet();
    }

    public int compareTo(Object obj) {
        return Priority.a(this, obj);
    }

    public Collection<l> getDependencies() {
        return this.f2651o.getDependencies();
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [l.a.a.a.o.c.Task, l.a.a.a.o.c.PriorityTask] */
    public boolean isFinished() {
        return this.f2651o.isFinished();
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [l.a.a.a.o.c.Task, l.a.a.a.o.c.PriorityTask] */
    public void setError(Throwable th) {
        this.f2651o.setError(th);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [l.a.a.a.o.c.Task, l.a.a.a.o.c.PriorityTask] */
    public void setFinished(boolean z) {
        this.f2651o.setFinished(z);
    }
}
