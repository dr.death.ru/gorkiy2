package ru.covid19.droid.data.model.profileData;

import e.a.b.h.b.h.o.TransportHealthStepViewState2;
import l.a.a.a.o.d.EventsFilesManager;

/* compiled from: ProfileData.kt */
public final class ProfileData1 {
    public static final ProfileData mockProfileData = new ProfileData(true, true, true, false, new Passenger(19820301, "RUS", "Full name", ProfileData0.male, "pa", "ra", null, null, null, null, null, null, null, EventsFilesManager.MAX_BYTE_SIZE_PER_FILE, null), new Document("1231", "123213", Document0.russianPassport), new Travel(20100102, null, "apl", "co", null, 18, null), new Transport(TransportHealthStepViewState2.plane, "tp"), null, 256, null);

    public static final ProfileData getMockProfileData() {
        return mockProfileData;
    }
}
