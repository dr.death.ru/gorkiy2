package ru.covid19.droid.presentation.main.auth;

import android.view.View;
import e.a.b.h.b.d.AuthFragmentVm;
import e.a.b.h.b.d.a;
import java.util.HashMap;
import ru.covid19.core.presentation.esiaAuth.BaseAuthFragment;

/* compiled from: AuthFragment.kt */
public final class AuthFragment extends BaseAuthFragment<a> {
    public HashMap e0;

    public /* synthetic */ void C() {
        super.C();
        N();
    }

    public void N() {
        HashMap hashMap = this.e0;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public Class<a> P() {
        return AuthFragmentVm.class;
    }

    public View c(int i2) {
        if (this.e0 == null) {
            this.e0 = new HashMap();
        }
        View view = (View) this.e0.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View view2 = this.H;
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i2);
        this.e0.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }
}
