package ru.covid19.droid.presentation.main;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import e.a.a.a.e.o.ActivityNavigator;
import e.a.a.a.e.q.SingleFragmentNavigator;
import e.a.a.i.CoreComponentsHolder;
import e.a.a.i.b.CoreComponent;
import e.a.b.f.a.DaggerMainComponent;
import e.a.b.f.a.MainComponent;
import e.a.b.f.b.AppComponentsHolder;
import e.a.b.f.c.AddProfileStepsNavigationModule;
import e.a.b.f.c.MainModule;
import e.a.b.h.a.BaseDpWithDevMenuActivity;
import e.a.b.h.b.MainActivityVm;
import e.a.b.h.b.b;
import e.b.a.Navigator;
import i.l.a.FragmentManager;
import i.o.ViewModelProvider;
import j.c.a.a.c.n.c;
import kotlin.TypeCastException;
import n.Lazy;
import n.i.Collections;
import n.n.b.Functions;
import n.n.c.Intrinsics;
import n.n.c.PropertyReference1Impl;
import n.n.c.Reflection;
import n.n.c.j;
import n.p.KProperty;

/* compiled from: MainActivity.kt */
public final class MainActivity extends BaseDpWithDevMenuActivity<b> {
    public static final /* synthetic */ KProperty[] C;
    public final String A = "MAIN_FRAG_CHAIN";
    public final Lazy B = c.a((Functions) new a(this));

    /* compiled from: MainActivity.kt */
    public static final class a extends j implements Functions<e.a.a.a.e.q.c> {
        public final /* synthetic */ MainActivity c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(MainActivity mainActivity) {
            super(0);
            this.c = mainActivity;
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [ru.covid19.droid.presentation.main.MainActivity, i.l.a.FragmentActivity] */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [i.l.a.FragmentManager, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public Object b() {
            FragmentManager j2 = this.c.j();
            Intrinsics.a((Object) j2, "supportFragmentManager");
            return new SingleFragmentNavigator(j2, new e.a.b.h.b.MainActivity((MainActivityVm) this.c.h()));
        }
    }

    /* JADX WARN: Type inference failed for: r2v1, types: [n.p.KClass, n.p.KDeclarationContainer] */
    static {
        PropertyReference1Impl propertyReference1Impl = new PropertyReference1Impl(Reflection.a(MainActivity.class), "fragmentNavigator", "getFragmentNavigator()Lru/covid19/core/presentation/navigation/fragementLevel/SingleFragmentNavigator;");
        Reflection.a(propertyReference1Impl);
        C = new KProperty[]{propertyReference1Impl};
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.content.Context, ru.covid19.droid.presentation.main.MainActivity, e.a.a.a.b.BaseDpActivity4] */
    public void onActivityResult(int i2, int i3, Intent intent) {
        boolean z = true;
        if (i2 != 1) {
            super.onActivityResult(i2, i3, intent);
            return;
        }
        try {
            Navigator navigator = this.w;
            if (navigator == null) {
                Intrinsics.b("navigator");
                throw null;
            } else if (navigator != null) {
                Uri uri = ((ActivityNavigator) navigator).a;
                if (Collections.a((Context) this, uri) == null) {
                    z = false;
                }
                if (z) {
                    r().b.a(10, uri);
                }
            } else {
                throw new TypeCastException("null cannot be cast to non-null type ru.covid19.core.presentation.navigation.activityLevel.ActivityNavigator");
            }
        } catch (Exception unused) {
        }
    }

    public void onCreate(Bundle bundle) {
        if (AppComponentsHolder.b == null) {
            CoreComponent coreComponent = CoreComponentsHolder.b;
            if (coreComponent == null) {
                Intrinsics.b("coreComponent");
                throw null;
            } else if (coreComponent != null) {
                MainModule mainModule = new MainModule();
                AddProfileStepsNavigationModule addProfileStepsNavigationModule = new AddProfileStepsNavigationModule();
                c.a(coreComponent, CoreComponent.class);
                AppComponentsHolder.b = new DaggerMainComponent(mainModule, addProfileStepsNavigationModule, coreComponent, null);
            } else {
                throw null;
            }
        }
        MainComponent mainComponent = AppComponentsHolder.b;
        if (mainComponent != null) {
            mainComponent.a(this.u);
            mainComponent.a(this.v);
            mainComponent.a(this.z);
            super.onCreate(bundle);
            return;
        }
        Intrinsics.a();
        throw null;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [ru.covid19.droid.presentation.main.MainActivity, i.b.k.AppCompatActivity] */
    public void onDestroy() {
        AppComponentsHolder.b = null;
        MainActivity.super.onDestroy();
    }

    public Class<b> p() {
        return MainActivityVm.class;
    }

    public ViewModelProvider.b q() {
        return this.u.a();
    }

    public SingleFragmentNavigator t() {
        Lazy lazy = this.B;
        KProperty kProperty = C[0];
        return (SingleFragmentNavigator) lazy.getValue();
    }
}
