package ru.covid19.core.data.network.model;

/* compiled from: PersonalResponse.kt */
public enum PersonalResponse2 {
    PERSONAL("PERSONAL"),
    RF_PASSPORT("RF_PASSPORT"),
    FID_DOC("FID_DOC"),
    SNILS("SNILS"),
    INN("INN"),
    MDCL_PLCY("MDCL_PLCY"),
    RF_DRIVING_LICENSE("RF_DRIVING_LICENSE"),
    CERT_REG_TRANSPORT("CERT_REG_TRANSPORT"),
    FRGN_PASS("FRGN_PASS"),
    MLTR_ID("MLTR_ID"),
    BRTH_CERT("BRTH_CERT"),
    RF_BRTH_CERT("RF_BRTH_CERT"),
    OLD_BRTH_CERT("OLD_BRTH_CERT"),
    FID_BRTH_CERT("FID_BRTH_CERT"),
    BIOMETRIC_DATA("BIOMETRIC_DATA"),
    NONE("NONE");
    
    public final String type;

    /* access modifiers changed from: public */
    PersonalResponse2(String str) {
        this.type = str;
    }

    public final String getType() {
        return this.type;
    }
}
