package ru.covid19.core.data.network.model;

import android.os.Parcel;
import android.os.Parcelable;
import e.a.a.a.e.p.BottomSheetSelectorNavigationDto;
import j.a.a.a.outline;
import n.n.c.Intrinsics;

/* compiled from: CitizenshipResponse.kt */
public final class CitizenshipResponse0 implements BottomSheetSelectorNavigationDto {
    public static final Parcelable.Creator CREATOR = new Creator();
    public final CitizenshipResponse citizenship;
    public final boolean isSelected;
    public final String label;

    public static class Creator implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            if (parcel != null) {
                return new CitizenshipResponse0((CitizenshipResponse) CitizenshipResponse.CREATOR.createFromParcel(parcel), parcel.readString(), parcel.readInt() != 0);
            }
            Intrinsics.a("in");
            throw null;
        }

        public final Object[] newArray(int i2) {
            return new CitizenshipResponse0[i2];
        }
    }

    public CitizenshipResponse0(CitizenshipResponse citizenshipResponse, String str, boolean z) {
        if (citizenshipResponse == null) {
            Intrinsics.a("citizenship");
            throw null;
        } else if (str != null) {
            this.citizenship = citizenshipResponse;
            this.label = str;
            this.isSelected = z;
        } else {
            Intrinsics.a("label");
            throw null;
        }
    }

    public static /* synthetic */ CitizenshipResponse0 copy$default(CitizenshipResponse0 citizenshipResponse0, CitizenshipResponse citizenshipResponse, String str, boolean z, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            citizenshipResponse = citizenshipResponse0.citizenship;
        }
        if ((i2 & 2) != 0) {
            str = citizenshipResponse0.getLabel();
        }
        if ((i2 & 4) != 0) {
            z = citizenshipResponse0.isSelected();
        }
        return citizenshipResponse0.copy(citizenshipResponse, str, z);
    }

    public final CitizenshipResponse component1() {
        return this.citizenship;
    }

    public final String component2() {
        return getLabel();
    }

    public final boolean component3() {
        return isSelected();
    }

    public final CitizenshipResponse0 copy(CitizenshipResponse citizenshipResponse, String str, boolean z) {
        if (citizenshipResponse == null) {
            Intrinsics.a("citizenship");
            throw null;
        } else if (str != null) {
            return new CitizenshipResponse0(citizenshipResponse, str, z);
        } else {
            Intrinsics.a("label");
            throw null;
        }
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CitizenshipResponse0)) {
            return false;
        }
        CitizenshipResponse0 citizenshipResponse0 = (CitizenshipResponse0) obj;
        return Intrinsics.a(this.citizenship, citizenshipResponse0.citizenship) && Intrinsics.a(getLabel(), citizenshipResponse0.getLabel()) && isSelected() == citizenshipResponse0.isSelected();
    }

    public final CitizenshipResponse getCitizenship() {
        return this.citizenship;
    }

    public String getLabel() {
        return this.label;
    }

    public int hashCode() {
        CitizenshipResponse citizenshipResponse = this.citizenship;
        int i2 = 0;
        int hashCode = (citizenshipResponse != null ? citizenshipResponse.hashCode() : 0) * 31;
        String label2 = getLabel();
        if (label2 != null) {
            i2 = label2.hashCode();
        }
        int i3 = (hashCode + i2) * 31;
        boolean isSelected2 = isSelected();
        if (isSelected2) {
            isSelected2 = true;
        }
        return i3 + (isSelected2 ? 1 : 0);
    }

    public boolean isSelected() {
        return this.isSelected;
    }

    public String toString() {
        StringBuilder a = outline.a("CitizenshipSelectorItem(citizenship=");
        a.append(this.citizenship);
        a.append(", label=");
        a.append(getLabel());
        a.append(", isSelected=");
        a.append(isSelected());
        a.append(")");
        return a.toString();
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            this.citizenship.writeToParcel(parcel, 0);
            parcel.writeString(this.label);
            parcel.writeInt(this.isSelected ? 1 : 0);
            return;
        }
        Intrinsics.a("parcel");
        throw null;
    }
}
