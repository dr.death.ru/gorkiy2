package i.w;

import android.annotation.SuppressLint;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import i.h.l.ViewCompat;
import i.l.a.FragmentTransitionImpl0;
import i.w.Transition;
import i.w.TransitionManager;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@SuppressLint({"RestrictedApi"})
public class FragmentTransitionSupport extends FragmentTransitionImpl0 {

    public class a extends Transition.c {
        public a(FragmentTransitionSupport fragmentTransitionSupport, Rect rect) {
        }
    }

    public class b implements Transition.d {
        public final /* synthetic */ View a;
        public final /* synthetic */ ArrayList b;

        public b(FragmentTransitionSupport fragmentTransitionSupport, View view, ArrayList arrayList) {
            this.a = view;
            this.b = arrayList;
        }

        public void a(Transition transition) {
        }

        public void b(Transition transition) {
        }

        public void c(Transition transition) {
        }

        public void d(Transition transition) {
        }

        public void e(Transition transition) {
            transition.b(this);
            this.a.setVisibility(8);
            int size = this.b.size();
            for (int i2 = 0; i2 < size; i2++) {
                ((View) this.b.get(i2)).setVisibility(0);
            }
        }
    }

    public class c extends TransitionListenerAdapter {
        public final /* synthetic */ Object a;
        public final /* synthetic */ ArrayList b;
        public final /* synthetic */ Object c;
        public final /* synthetic */ ArrayList d;

        /* renamed from: e  reason: collision with root package name */
        public final /* synthetic */ Object f1450e;

        /* renamed from: f  reason: collision with root package name */
        public final /* synthetic */ ArrayList f1451f;

        public c(Object obj, ArrayList arrayList, Object obj2, ArrayList arrayList2, Object obj3, ArrayList arrayList3) {
            this.a = obj;
            this.b = arrayList;
            this.c = obj2;
            this.d = arrayList2;
            this.f1450e = obj3;
            this.f1451f = arrayList3;
        }

        public void c(Transition transition) {
            Object obj = this.a;
            if (obj != null) {
                FragmentTransitionSupport.this.a(obj, (ArrayList<View>) this.b, (ArrayList<View>) null);
            }
            Object obj2 = this.c;
            if (obj2 != null) {
                FragmentTransitionSupport.this.a(obj2, (ArrayList<View>) this.d, (ArrayList<View>) null);
            }
            Object obj3 = this.f1450e;
            if (obj3 != null) {
                FragmentTransitionSupport.this.a(obj3, (ArrayList<View>) this.f1451f, (ArrayList<View>) null);
            }
        }

        public void e(Transition transition) {
            transition.b(this);
        }
    }

    public class d extends Transition.c {
        public d(FragmentTransitionSupport fragmentTransitionSupport, Rect rect) {
        }
    }

    public boolean a(Object obj) {
        return obj instanceof Transition;
    }

    public Object b(Object obj) {
        if (obj != null) {
            return ((Transition) obj).clone();
        }
        return null;
    }

    public Object c(Object obj) {
        if (obj == null) {
            return null;
        }
        TransitionSet transitionSet = new TransitionSet();
        transitionSet.a((Transition) obj);
        return transitionSet;
    }

    public void a(Object obj, ArrayList<View> arrayList) {
        Transition transition = (Transition) obj;
        if (transition != null) {
            int i2 = 0;
            if (transition instanceof TransitionSet) {
                TransitionSet transitionSet = (TransitionSet) transition;
                int size = transitionSet.J.size();
                while (i2 < size) {
                    a(transitionSet.a(i2), arrayList);
                    i2++;
                }
            } else if (!a(transition) && FragmentTransitionImpl0.a((List) transition.g)) {
                int size2 = arrayList.size();
                while (i2 < size2) {
                    transition.a(arrayList.get(i2));
                    i2++;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentTransitionImpl0.a(java.util.List<android.view.View>, android.view.View):void
     arg types: [java.util.ArrayList<android.view.View>, android.view.View]
     candidates:
      i.l.a.FragmentTransitionImpl0.a(android.view.View, android.graphics.Rect):void
      i.l.a.FragmentTransitionImpl0.a(android.view.ViewGroup, java.lang.Object):void
      i.l.a.FragmentTransitionImpl0.a(java.lang.Object, android.graphics.Rect):void
      i.l.a.FragmentTransitionImpl0.a(java.lang.Object, android.view.View):void
      i.l.a.FragmentTransitionImpl0.a(java.lang.Object, java.util.ArrayList<android.view.View>):void
      i.l.a.FragmentTransitionImpl0.a(java.util.ArrayList<android.view.View>, android.view.View):void
      i.l.a.FragmentTransitionImpl0.a(java.util.Map<java.lang.String, android.view.View>, android.view.View):void
      i.l.a.FragmentTransitionImpl0.a(java.util.List<android.view.View>, android.view.View):void */
    public void b(Object obj, View view, ArrayList<View> arrayList) {
        TransitionSet transitionSet = (TransitionSet) obj;
        ArrayList<View> arrayList2 = transitionSet.g;
        arrayList2.clear();
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            FragmentTransitionImpl0.a((List<View>) arrayList2, arrayList.get(i2));
        }
        arrayList2.add(view);
        arrayList.add(view);
        a(transitionSet, arrayList);
    }

    public void c(Object obj, View view) {
        if (view != null) {
            Rect rect = new Rect();
            a(view, rect);
            ((Transition) obj).a(new a(this, rect));
        }
    }

    public Object b(Object obj, Object obj2, Object obj3) {
        TransitionSet transitionSet = new TransitionSet();
        if (obj != null) {
            transitionSet.a((Transition) obj);
        }
        if (obj2 != null) {
            transitionSet.a((Transition) obj2);
        }
        if (obj3 != null) {
            transitionSet.a((Transition) obj3);
        }
        return transitionSet;
    }

    public void a(Object obj, View view, ArrayList<View> arrayList) {
        ((Transition) obj).a(new b(this, view, arrayList));
    }

    public Object a(Object obj, Object obj2, Object obj3) {
        Transition transition = (Transition) obj;
        Transition transition2 = (Transition) obj2;
        Transition transition3 = (Transition) obj3;
        if (transition != null && transition2 != null) {
            TransitionSet transitionSet = new TransitionSet();
            transitionSet.a(transition);
            transitionSet.a(transition2);
            transitionSet.b(1);
            transition = transitionSet;
        } else if (transition == null) {
            transition = transition2 != null ? transition2 : null;
        }
        if (transition3 == null) {
            return transition;
        }
        TransitionSet transitionSet2 = new TransitionSet();
        if (transition != null) {
            transitionSet2.a(transition);
        }
        transitionSet2.a(transition3);
        return transitionSet2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.w.FragmentTransitionSupport.a(java.lang.Object, java.util.ArrayList<android.view.View>, java.util.ArrayList<android.view.View>):void
     arg types: [i.w.TransitionSet, java.util.ArrayList<android.view.View>, java.util.ArrayList<android.view.View>]
     candidates:
      i.w.FragmentTransitionSupport.a(java.lang.Object, java.lang.Object, java.lang.Object):java.lang.Object
      i.w.FragmentTransitionSupport.a(java.lang.Object, android.view.View, java.util.ArrayList<android.view.View>):void
      i.l.a.FragmentTransitionImpl0.a(java.util.List<android.view.View>, android.view.View, int):boolean
      i.l.a.FragmentTransitionImpl0.a(java.lang.Object, java.lang.Object, java.lang.Object):java.lang.Object
      i.l.a.FragmentTransitionImpl0.a(java.lang.Object, android.view.View, java.util.ArrayList<android.view.View>):void
      i.w.FragmentTransitionSupport.a(java.lang.Object, java.util.ArrayList<android.view.View>, java.util.ArrayList<android.view.View>):void */
    public void b(Object obj, ArrayList<View> arrayList, ArrayList<View> arrayList2) {
        TransitionSet transitionSet = (TransitionSet) obj;
        if (transitionSet != null) {
            transitionSet.g.clear();
            transitionSet.g.addAll(arrayList2);
            a((Object) transitionSet, arrayList, arrayList2);
        }
    }

    public void b(Object obj, View view) {
        if (obj != null) {
            ((Transition) obj).d(view);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.w.Transition.a(android.view.ViewGroup, boolean):void
     arg types: [android.view.ViewGroup, int]
     candidates:
      i.w.Transition.a(android.view.View, boolean):void
      i.w.Transition.a(i.w.TransitionValues, i.w.TransitionValues):boolean
      i.w.Transition.a(android.view.ViewGroup, boolean):void */
    public void a(ViewGroup viewGroup, Object obj) {
        Transition transition = (Transition) obj;
        if (!TransitionManager.c.contains(viewGroup) && ViewCompat.w(viewGroup)) {
            TransitionManager.c.add(viewGroup);
            if (transition == null) {
                transition = TransitionManager.a;
            }
            Transition clone = transition.clone();
            ArrayList orDefault = TransitionManager.a().getOrDefault(viewGroup, null);
            if (orDefault != null && orDefault.size() > 0) {
                Iterator it = orDefault.iterator();
                while (it.hasNext()) {
                    ((Transition) it.next()).c(viewGroup);
                }
            }
            if (clone != null) {
                clone.a(viewGroup, true);
            }
            if (((Scene) viewGroup.getTag(f.transition_current_scene)) == null) {
                viewGroup.setTag(f.transition_current_scene, null);
                if (clone != null) {
                    TransitionManager.a aVar = new TransitionManager.a(clone, viewGroup);
                    viewGroup.addOnAttachStateChangeListener(aVar);
                    viewGroup.getViewTreeObserver().addOnPreDrawListener(aVar);
                    return;
                }
                return;
            }
            throw null;
        }
    }

    public void a(Object obj, Object obj2, ArrayList<View> arrayList, Object obj3, ArrayList<View> arrayList2, Object obj4, ArrayList<View> arrayList3) {
        ((Transition) obj).a(new c(obj2, arrayList, obj3, arrayList2, obj4, arrayList3));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.w.FragmentTransitionSupport.a(java.lang.Object, java.util.ArrayList<android.view.View>, java.util.ArrayList<android.view.View>):void
     arg types: [i.w.Transition, java.util.ArrayList<android.view.View>, java.util.ArrayList<android.view.View>]
     candidates:
      i.w.FragmentTransitionSupport.a(java.lang.Object, java.lang.Object, java.lang.Object):java.lang.Object
      i.w.FragmentTransitionSupport.a(java.lang.Object, android.view.View, java.util.ArrayList<android.view.View>):void
      i.l.a.FragmentTransitionImpl0.a(java.util.List<android.view.View>, android.view.View, int):boolean
      i.l.a.FragmentTransitionImpl0.a(java.lang.Object, java.lang.Object, java.lang.Object):java.lang.Object
      i.l.a.FragmentTransitionImpl0.a(java.lang.Object, android.view.View, java.util.ArrayList<android.view.View>):void
      i.w.FragmentTransitionSupport.a(java.lang.Object, java.util.ArrayList<android.view.View>, java.util.ArrayList<android.view.View>):void */
    public void a(Object obj, ArrayList<View> arrayList, ArrayList<View> arrayList2) {
        int i2;
        Transition transition = (Transition) obj;
        int i3 = 0;
        if (transition instanceof TransitionSet) {
            TransitionSet transitionSet = (TransitionSet) transition;
            int size = transitionSet.J.size();
            while (i3 < size) {
                a((Object) transitionSet.a(i3), arrayList, arrayList2);
                i3++;
            }
        } else if (!a(transition)) {
            ArrayList<View> arrayList3 = transition.g;
            if (arrayList3.size() == arrayList.size() && arrayList3.containsAll(arrayList)) {
                if (arrayList2 == null) {
                    i2 = 0;
                } else {
                    i2 = arrayList2.size();
                }
                while (i3 < i2) {
                    transition.a(arrayList2.get(i3));
                    i3++;
                }
                int size2 = arrayList.size();
                while (true) {
                    size2--;
                    if (size2 >= 0) {
                        transition.d(arrayList.get(size2));
                    } else {
                        return;
                    }
                }
            }
        }
    }

    public void a(Object obj, View view) {
        if (obj != null) {
            ((Transition) obj).a(view);
        }
    }

    public void a(Object obj, Rect rect) {
        if (obj != null) {
            ((Transition) obj).a(new d(this, rect));
        }
    }

    public static boolean a(Transition transition) {
        return !FragmentTransitionImpl0.a(transition.f1453f) || !FragmentTransitionImpl0.a(transition.h) || !FragmentTransitionImpl0.a(transition.f1454i);
    }
}
