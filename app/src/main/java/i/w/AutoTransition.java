package i.w;

public class AutoTransition extends TransitionSet {
    public AutoTransition() {
        b(1);
        a(new Fade(2));
        a(new ChangeBounds());
        a(new Fade(1));
    }
}
