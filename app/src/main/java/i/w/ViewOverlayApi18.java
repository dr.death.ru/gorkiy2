package i.w;

import android.view.View;
import android.view.ViewOverlay;

public class ViewOverlayApi18 implements ViewOverlayImpl {
    public final ViewOverlay a;

    public ViewOverlayApi18(View view) {
        this.a = view.getOverlay();
    }
}
