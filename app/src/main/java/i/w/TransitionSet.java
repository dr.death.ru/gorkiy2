package i.w;

import android.animation.TimeInterpolator;
import android.util.AndroidRuntimeException;
import android.view.View;
import android.view.ViewGroup;
import i.w.Transition;
import j.a.a.a.outline;
import java.util.ArrayList;
import java.util.Iterator;

public class TransitionSet extends Transition {
    public ArrayList<i> J = new ArrayList<>();
    public boolean K = true;
    public int L;
    public boolean M = false;
    public int N = 0;

    public class a extends TransitionListenerAdapter {
        public final /* synthetic */ Transition a;

        public a(TransitionSet transitionSet, Transition transition) {
            this.a = transition;
        }

        public void e(Transition transition) {
            this.a.d();
            transition.b(this);
        }
    }

    public static class b extends TransitionListenerAdapter {
        public TransitionSet a;

        public b(TransitionSet transitionSet) {
            this.a = transitionSet;
        }

        public void c(Transition transition) {
            TransitionSet transitionSet = this.a;
            if (!transitionSet.M) {
                transitionSet.e();
                this.a.M = true;
            }
        }

        public void e(Transition transition) {
            TransitionSet transitionSet = this.a;
            int i2 = transitionSet.L - 1;
            transitionSet.L = i2;
            if (i2 == 0) {
                transitionSet.M = false;
                transitionSet.b();
            }
            transition.b(this);
        }
    }

    public TransitionSet a(Transition transition) {
        this.J.add(transition);
        super.f1464s = this;
        long j2 = super.d;
        if (j2 >= 0) {
            super.a(j2);
        }
        if ((this.N & 1) != 0) {
            super.a(super.f1452e);
        }
        if ((this.N & 2) != 0) {
            super.a((TransitionPropagation) null);
        }
        if ((this.N & 4) != 0) {
            super.a(super.F);
        }
        if ((this.N & 8) != 0) {
            super.a(super.E);
        }
        return this;
    }

    public TransitionSet b(int i2) {
        if (i2 == 0) {
            this.K = true;
        } else if (i2 == 1) {
            this.K = false;
        } else {
            throw new AndroidRuntimeException(outline.b("Invalid parameter for TransitionSet ordering: ", i2));
        }
        return this;
    }

    public void c(TransitionValues transitionValues) {
        if (b(transitionValues.b)) {
            Iterator<i> it = this.J.iterator();
            while (it.hasNext()) {
                Transition next = it.next();
                if (super.b(transitionValues.b)) {
                    super.c(transitionValues);
                    transitionValues.c.add(next);
                }
            }
        }
    }

    public void cancel() {
        super.cancel();
        int size = this.J.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.J.get(i2).cancel();
        }
    }

    public Transition d(View view) {
        for (int i2 = 0; i2 < this.J.size(); i2++) {
            this.J.get(i2).d(view);
        }
        super.g.remove(view);
        return super;
    }

    public void e(View view) {
        super.e(view);
        int size = this.J.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.J.get(i2).e(view);
        }
    }

    public Transition clone() {
        TransitionSet transitionSet = (TransitionSet) super.clone();
        transitionSet.J = new ArrayList<>();
        int size = this.J.size();
        for (int i2 = 0; i2 < size; i2++) {
            Transition clone = this.J.get(i2).clone();
            transitionSet.J.add(clone);
            super.f1464s = transitionSet;
        }
        return super;
    }

    public Transition b(long j2) {
        super.c = j2;
        return super;
    }

    public void d() {
        if (this.J.isEmpty()) {
            e();
            b();
            return;
        }
        b bVar = new b(this);
        Iterator<i> it = this.J.iterator();
        while (it.hasNext()) {
            it.next().a(bVar);
        }
        this.L = this.J.size();
        if (!this.K) {
            for (int i2 = 1; i2 < this.J.size(); i2++) {
                this.J.get(i2 - 1).a(new a(this, this.J.get(i2)));
            }
            Transition transition = this.J.get(0);
            if (transition != null) {
                super.d();
                return;
            }
            return;
        }
        Iterator<i> it2 = this.J.iterator();
        while (it2.hasNext()) {
            it2.next().d();
        }
    }

    public Transition b(Transition.d dVar) {
        super.b(dVar);
        return super;
    }

    public void b(TransitionValues transitionValues) {
        int size = this.J.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.J.get(i2).b(transitionValues);
        }
    }

    public void c(View view) {
        super.c(view);
        int size = this.J.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.J.get(i2).c(view);
        }
    }

    public Transition a(int i2) {
        if (i2 < 0 || i2 >= this.J.size()) {
            return null;
        }
        return this.J.get(i2);
    }

    public Transition a(TimeInterpolator timeInterpolator) {
        this.N |= 1;
        ArrayList<i> arrayList = this.J;
        if (arrayList != null) {
            int size = arrayList.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.J.get(i2).a(timeInterpolator);
            }
        }
        super.f1452e = timeInterpolator;
        return super;
    }

    public Transition a(View view) {
        for (int i2 = 0; i2 < this.J.size(); i2++) {
            this.J.get(i2).a(view);
        }
        super.g.add(view);
        return super;
    }

    public Transition a(Transition.d dVar) {
        super.a(dVar);
        return super;
    }

    public Transition a(long j2) {
        ArrayList<i> arrayList;
        super.d = j2;
        if (j2 >= 0 && (arrayList = this.J) != null) {
            int size = arrayList.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.J.get(i2).a(j2);
            }
        }
        return super;
    }

    public void a(ViewGroup viewGroup, r rVar, r rVar2, ArrayList<q> arrayList, ArrayList<q> arrayList2) {
        long j2 = super.c;
        int size = this.J.size();
        for (int i2 = 0; i2 < size; i2++) {
            Transition transition = this.J.get(i2);
            if (j2 > 0 && (this.K || i2 == 0)) {
                long j3 = super.c;
                if (j3 > 0) {
                    super.b(j3 + j2);
                } else {
                    super.b(j2);
                }
            }
            super.a(viewGroup, rVar, rVar2, arrayList, arrayList2);
        }
    }

    public void a(TransitionValues transitionValues) {
        if (b(transitionValues.b)) {
            Iterator<i> it = this.J.iterator();
            while (it.hasNext()) {
                Transition next = it.next();
                if (super.b(transitionValues.b)) {
                    super.a(transitionValues);
                    transitionValues.c.add(next);
                }
            }
        }
    }

    public String a(String str) {
        String a2 = super.a(str);
        for (int i2 = 0; i2 < this.J.size(); i2++) {
            StringBuilder b2 = outline.b(a2, "\n");
            b2.append(this.J.get(i2).a(str + "  "));
            a2 = b2.toString();
        }
        return a2;
    }

    public void a(PathMotion pathMotion) {
        if (pathMotion == null) {
            super.F = Transition.H;
        } else {
            super.F = pathMotion;
        }
        this.N |= 4;
        if (this.J != null) {
            for (int i2 = 0; i2 < this.J.size(); i2++) {
                this.J.get(i2).a(pathMotion);
            }
        }
    }

    public void a(Transition.c cVar) {
        super.E = cVar;
        this.N |= 8;
        int size = this.J.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.J.get(i2).a(cVar);
        }
    }

    public void a(TransitionPropagation transitionPropagation) {
        super.D = transitionPropagation;
        this.N |= 2;
        int size = this.J.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.J.get(i2).a(transitionPropagation);
        }
    }
}
