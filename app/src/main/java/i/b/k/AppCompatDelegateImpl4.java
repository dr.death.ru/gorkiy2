package i.b.k;

import android.view.View;
import i.h.l.ViewCompat;
import i.h.l.ViewPropertyAnimatorListener;
import i.h.l.ViewPropertyAnimatorListenerAdapter;

/* compiled from: AppCompatDelegateImpl */
public class AppCompatDelegateImpl4 extends ViewPropertyAnimatorListenerAdapter {
    public final /* synthetic */ AppCompatDelegateImpl a;

    public AppCompatDelegateImpl4(AppCompatDelegateImpl appCompatDelegateImpl) {
        this.a = appCompatDelegateImpl;
    }

    public void a(View view) {
        this.a.f747p.setAlpha(1.0f);
        this.a.f750s.a((ViewPropertyAnimatorListener) null);
        this.a.f750s = null;
    }

    public void b(View view) {
        this.a.f747p.setVisibility(0);
        this.a.f747p.sendAccessibilityEvent(32);
        if (this.a.f747p.getParent() instanceof View) {
            ViewCompat.B((View) this.a.f747p.getParent());
        }
    }
}
