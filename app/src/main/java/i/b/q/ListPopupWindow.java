package i.b.q;

import android.view.View;
import android.widget.AdapterView;

public class ListPopupWindow implements AdapterView.OnItemSelectedListener {
    public final /* synthetic */ ListPopupWindow0 b;

    public ListPopupWindow(ListPopupWindow0 listPopupWindow0) {
        this.b = listPopupWindow0;
    }

    public void onItemSelected(AdapterView<?> adapterView, View view, int i2, long j2) {
        DropDownListView dropDownListView;
        if (i2 != -1 && (dropDownListView = this.b.d) != null) {
            dropDownListView.setListSelectionHidden(false);
        }
    }

    public void onNothingSelected(AdapterView<?> adapterView) {
    }
}
