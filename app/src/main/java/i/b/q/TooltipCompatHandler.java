package i.b.q;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityManager;
import i.b.d;
import i.h.l.ViewCompat;
import i.h.l.ViewConfigurationCompat;
import l.a.a.a.o.b.AbstractSpiCall;

public class TooltipCompatHandler implements View.OnLongClickListener, View.OnHoverListener, View.OnAttachStateChangeListener {

    /* renamed from: k  reason: collision with root package name */
    public static TooltipCompatHandler f1041k;

    /* renamed from: l  reason: collision with root package name */
    public static TooltipCompatHandler f1042l;
    public final View b;
    public final CharSequence c;
    public final int d;

    /* renamed from: e  reason: collision with root package name */
    public final Runnable f1043e = new a();

    /* renamed from: f  reason: collision with root package name */
    public final Runnable f1044f = new b();
    public int g;
    public int h;

    /* renamed from: i  reason: collision with root package name */
    public TooltipPopup f1045i;

    /* renamed from: j  reason: collision with root package name */
    public boolean f1046j;

    public class a implements Runnable {
        public a() {
        }

        public void run() {
            TooltipCompatHandler.this.a(false);
        }
    }

    public class b implements Runnable {
        public b() {
        }

        public void run() {
            TooltipCompatHandler.this.b();
        }
    }

    public TooltipCompatHandler(View view, CharSequence charSequence) {
        this.b = view;
        this.c = charSequence;
        this.d = ViewConfigurationCompat.a(ViewConfiguration.get(view.getContext()));
        a();
        this.b.setOnLongClickListener(this);
        this.b.setOnHoverListener(this);
    }

    public void a(boolean z) {
        int i2;
        int i3;
        long j2;
        int i4;
        long j3;
        if (ViewCompat.v(this.b)) {
            a((TooltipCompatHandler) null);
            TooltipCompatHandler tooltipCompatHandler = f1042l;
            if (tooltipCompatHandler != null) {
                tooltipCompatHandler.b();
            }
            f1042l = this;
            this.f1046j = z;
            TooltipPopup tooltipPopup = new TooltipPopup(this.b.getContext());
            this.f1045i = tooltipPopup;
            View view = this.b;
            int i5 = this.g;
            int i6 = this.h;
            boolean z2 = this.f1046j;
            CharSequence charSequence = this.c;
            if (tooltipPopup.b.getParent() != null) {
                tooltipPopup.a();
            }
            tooltipPopup.c.setText(charSequence);
            WindowManager.LayoutParams layoutParams = tooltipPopup.d;
            layoutParams.token = view.getApplicationWindowToken();
            int dimensionPixelOffset = tooltipPopup.a.getResources().getDimensionPixelOffset(d.tooltip_precise_anchor_threshold);
            if (view.getWidth() < dimensionPixelOffset) {
                i5 = view.getWidth() / 2;
            }
            if (view.getHeight() >= dimensionPixelOffset) {
                int dimensionPixelOffset2 = tooltipPopup.a.getResources().getDimensionPixelOffset(d.tooltip_precise_anchor_extra_offset);
                i2 = i6 + dimensionPixelOffset2;
                i3 = i6 - dimensionPixelOffset2;
            } else {
                i2 = view.getHeight();
                i3 = 0;
            }
            layoutParams.gravity = 49;
            int dimensionPixelOffset3 = tooltipPopup.a.getResources().getDimensionPixelOffset(z2 ? d.tooltip_y_offset_touch : d.tooltip_y_offset_non_touch);
            View rootView = view.getRootView();
            ViewGroup.LayoutParams layoutParams2 = rootView.getLayoutParams();
            if (!(layoutParams2 instanceof WindowManager.LayoutParams) || ((WindowManager.LayoutParams) layoutParams2).type != 2) {
                Context context = view.getContext();
                while (true) {
                    if (!(context instanceof ContextWrapper)) {
                        break;
                    } else if (context instanceof Activity) {
                        rootView = ((Activity) context).getWindow().getDecorView();
                        break;
                    } else {
                        context = ((ContextWrapper) context).getBaseContext();
                    }
                }
            }
            if (rootView == null) {
                Log.e("TooltipPopup", "Cannot find app view");
            } else {
                rootView.getWindowVisibleDisplayFrame(tooltipPopup.f1047e);
                Rect rect = tooltipPopup.f1047e;
                if (rect.left < 0 && rect.top < 0) {
                    Resources resources = tooltipPopup.a.getResources();
                    int identifier = resources.getIdentifier("status_bar_height", "dimen", AbstractSpiCall.ANDROID_CLIENT_TYPE);
                    int dimensionPixelSize = identifier != 0 ? resources.getDimensionPixelSize(identifier) : 0;
                    DisplayMetrics displayMetrics = resources.getDisplayMetrics();
                    tooltipPopup.f1047e.set(0, dimensionPixelSize, displayMetrics.widthPixels, displayMetrics.heightPixels);
                }
                rootView.getLocationOnScreen(tooltipPopup.g);
                view.getLocationOnScreen(tooltipPopup.f1048f);
                int[] iArr = tooltipPopup.f1048f;
                int i7 = iArr[0];
                int[] iArr2 = tooltipPopup.g;
                iArr[0] = i7 - iArr2[0];
                iArr[1] = iArr[1] - iArr2[1];
                layoutParams.x = (iArr[0] + i5) - (rootView.getWidth() / 2);
                int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
                tooltipPopup.b.measure(makeMeasureSpec, makeMeasureSpec);
                int measuredHeight = tooltipPopup.b.getMeasuredHeight();
                int[] iArr3 = tooltipPopup.f1048f;
                int i8 = ((iArr3[1] + i3) - dimensionPixelOffset3) - measuredHeight;
                int i9 = iArr3[1] + i2 + dimensionPixelOffset3;
                if (z2) {
                    if (i8 >= 0) {
                        layoutParams.y = i8;
                    } else {
                        layoutParams.y = i9;
                    }
                } else if (measuredHeight + i9 <= tooltipPopup.f1047e.height()) {
                    layoutParams.y = i9;
                } else {
                    layoutParams.y = i8;
                }
            }
            ((WindowManager) tooltipPopup.a.getSystemService("window")).addView(tooltipPopup.b, tooltipPopup.d);
            this.b.addOnAttachStateChangeListener(this);
            if (this.f1046j) {
                j2 = 2500;
            } else {
                if ((this.b.getWindowSystemUiVisibility() & 1) == 1) {
                    j3 = 3000;
                    i4 = ViewConfiguration.getLongPressTimeout();
                } else {
                    j3 = 15000;
                    i4 = ViewConfiguration.getLongPressTimeout();
                }
                j2 = j3 - ((long) i4);
            }
            this.b.removeCallbacks(this.f1044f);
            this.b.postDelayed(this.f1044f, j2);
        }
    }

    public void b() {
        if (f1042l == this) {
            f1042l = null;
            TooltipPopup tooltipPopup = this.f1045i;
            if (tooltipPopup != null) {
                tooltipPopup.a();
                this.f1045i = null;
                a();
                this.b.removeOnAttachStateChangeListener(this);
            } else {
                Log.e("TooltipCompatHandler", "sActiveHandler.mPopup == null");
            }
        }
        if (f1041k == this) {
            a((TooltipCompatHandler) null);
        }
        this.b.removeCallbacks(this.f1044f);
    }

    public boolean onHover(View view, MotionEvent motionEvent) {
        boolean z;
        if (this.f1045i != null && this.f1046j) {
            return false;
        }
        AccessibilityManager accessibilityManager = (AccessibilityManager) this.b.getContext().getSystemService("accessibility");
        if (accessibilityManager.isEnabled() && accessibilityManager.isTouchExplorationEnabled()) {
            return false;
        }
        int action = motionEvent.getAction();
        if (action != 7) {
            if (action == 10) {
                a();
                b();
            }
        } else if (this.b.isEnabled() && this.f1045i == null) {
            int x = (int) motionEvent.getX();
            int y = (int) motionEvent.getY();
            if (Math.abs(x - this.g) > this.d || Math.abs(y - this.h) > this.d) {
                this.g = x;
                this.h = y;
                z = true;
            } else {
                z = false;
            }
            if (z) {
                a(this);
            }
        }
        return false;
    }

    public boolean onLongClick(View view) {
        this.g = view.getWidth() / 2;
        this.h = view.getHeight() / 2;
        a(true);
        return true;
    }

    public void onViewAttachedToWindow(View view) {
    }

    public void onViewDetachedFromWindow(View view) {
        b();
    }

    public static void a(TooltipCompatHandler tooltipCompatHandler) {
        TooltipCompatHandler tooltipCompatHandler2 = f1041k;
        if (tooltipCompatHandler2 != null) {
            tooltipCompatHandler2.b.removeCallbacks(tooltipCompatHandler2.f1043e);
        }
        f1041k = tooltipCompatHandler;
        if (tooltipCompatHandler != null) {
            tooltipCompatHandler.b.postDelayed(tooltipCompatHandler.f1043e, (long) ViewConfiguration.getLongPressTimeout());
        }
    }

    public final void a() {
        this.g = Integer.MAX_VALUE;
        this.h = Integer.MAX_VALUE;
    }
}
