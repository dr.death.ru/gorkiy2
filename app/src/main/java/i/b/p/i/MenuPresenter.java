package i.b.p.i;

import android.content.Context;

public interface MenuPresenter {

    public interface a {
        void a(MenuBuilder menuBuilder, boolean z);

        boolean a(MenuBuilder menuBuilder);
    }

    void a(Context context, MenuBuilder menuBuilder);

    void a(MenuBuilder menuBuilder, boolean z);

    void a(a aVar);

    void a(boolean z);

    boolean a(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl);

    boolean a(SubMenuBuilder subMenuBuilder);

    boolean b(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl);

    boolean c();
}
