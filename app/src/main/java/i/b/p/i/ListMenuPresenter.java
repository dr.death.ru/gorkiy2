package i.b.p.i;

import android.content.Context;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import androidx.appcompat.app.AlertController;
import androidx.appcompat.view.menu.ExpandedMenuView;
import i.b.g;
import i.b.k.AlertDialog;
import i.b.p.i.MenuPresenter;
import i.b.p.i.MenuView;
import java.util.ArrayList;

public class ListMenuPresenter implements MenuPresenter, AdapterView.OnItemClickListener {
    public Context b;
    public LayoutInflater c;
    public MenuBuilder d;

    /* renamed from: e  reason: collision with root package name */
    public ExpandedMenuView f876e;

    /* renamed from: f  reason: collision with root package name */
    public int f877f;
    public int g = 0;
    public int h;

    /* renamed from: i  reason: collision with root package name */
    public MenuPresenter.a f878i;

    /* renamed from: j  reason: collision with root package name */
    public a f879j;

    public class a extends BaseAdapter {
        public int b = -1;

        public a() {
            a();
        }

        public void a() {
            MenuBuilder menuBuilder = ListMenuPresenter.this.d;
            MenuItemImpl menuItemImpl = menuBuilder.w;
            if (menuItemImpl != null) {
                menuBuilder.a();
                ArrayList<i> arrayList = menuBuilder.f885j;
                int size = arrayList.size();
                for (int i2 = 0; i2 < size; i2++) {
                    if (((MenuItemImpl) arrayList.get(i2)) == menuItemImpl) {
                        this.b = i2;
                        return;
                    }
                }
            }
            this.b = -1;
        }

        public int getCount() {
            MenuBuilder menuBuilder = ListMenuPresenter.this.d;
            menuBuilder.a();
            int size = menuBuilder.f885j.size() - ListMenuPresenter.this.f877f;
            return this.b < 0 ? size : size - 1;
        }

        public long getItemId(int i2) {
            return (long) i2;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i2, View view, ViewGroup viewGroup) {
            if (view == null) {
                ListMenuPresenter listMenuPresenter = ListMenuPresenter.this;
                view = listMenuPresenter.c.inflate(listMenuPresenter.h, viewGroup, false);
            }
            ((MenuView.a) view).a(getItem(i2), 0);
            return view;
        }

        public void notifyDataSetChanged() {
            a();
            super.notifyDataSetChanged();
        }

        public MenuItemImpl getItem(int i2) {
            MenuBuilder menuBuilder = ListMenuPresenter.this.d;
            menuBuilder.a();
            ArrayList<i> arrayList = menuBuilder.f885j;
            int i3 = i2 + ListMenuPresenter.this.f877f;
            int i4 = this.b;
            if (i4 >= 0 && i3 >= i4) {
                i3++;
            }
            return arrayList.get(i3);
        }
    }

    public ListMenuPresenter(Context context, int i2) {
        this.h = i2;
        this.b = context;
        this.c = LayoutInflater.from(context);
    }

    public void a(Context context, MenuBuilder menuBuilder) {
        if (this.g != 0) {
            ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(context, this.g);
            this.b = contextThemeWrapper;
            this.c = LayoutInflater.from(contextThemeWrapper);
        } else if (this.b != null) {
            this.b = context;
            if (this.c == null) {
                this.c = LayoutInflater.from(context);
            }
        }
        this.d = menuBuilder;
        a aVar = this.f879j;
        if (aVar != null) {
            aVar.notifyDataSetChanged();
        }
    }

    public boolean a(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    public boolean b(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    public boolean c() {
        return false;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i2, long j2) {
        this.d.a(this.f879j.getItem(i2), this, 0);
    }

    public ListAdapter a() {
        if (this.f879j == null) {
            this.f879j = new a();
        }
        return this.f879j;
    }

    public void a(boolean z) {
        a aVar = this.f879j;
        if (aVar != null) {
            aVar.notifyDataSetChanged();
        }
    }

    public void a(MenuPresenter.a aVar) {
        this.f878i = aVar;
    }

    public boolean a(SubMenuBuilder subMenuBuilder) {
        if (!subMenuBuilder.hasVisibleItems()) {
            return false;
        }
        MenuDialogHelper menuDialogHelper = new MenuDialogHelper(subMenuBuilder);
        MenuBuilder menuBuilder = menuDialogHelper.b;
        AlertDialog.a aVar = new AlertDialog.a(menuBuilder.a);
        ListMenuPresenter listMenuPresenter = new ListMenuPresenter(aVar.a.a, g.abc_list_menu_item_layout);
        menuDialogHelper.d = listMenuPresenter;
        listMenuPresenter.f878i = menuDialogHelper;
        MenuBuilder menuBuilder2 = menuDialogHelper.b;
        menuBuilder2.a(listMenuPresenter, menuBuilder2.a);
        ListAdapter a2 = menuDialogHelper.d.a();
        AlertController.b bVar = aVar.a;
        bVar.f28q = a2;
        bVar.f29r = menuDialogHelper;
        View view = menuBuilder.f890o;
        if (view != null) {
            bVar.g = view;
        } else {
            bVar.d = menuBuilder.f889n;
            bVar.f19f = menuBuilder.f888m;
        }
        aVar.a.f27p = menuDialogHelper;
        AlertDialog a3 = aVar.a();
        menuDialogHelper.c = a3;
        a3.setOnDismissListener(menuDialogHelper);
        WindowManager.LayoutParams attributes = menuDialogHelper.c.getWindow().getAttributes();
        attributes.type = 1003;
        attributes.flags |= 131072;
        menuDialogHelper.c.show();
        MenuPresenter.a aVar2 = this.f878i;
        if (aVar2 == null) {
            return true;
        }
        aVar2.a(subMenuBuilder);
        return true;
    }

    public void a(MenuBuilder menuBuilder, boolean z) {
        MenuPresenter.a aVar = this.f878i;
        if (aVar != null) {
            aVar.a(menuBuilder, z);
        }
    }
}
