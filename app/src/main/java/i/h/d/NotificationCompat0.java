package i.h.d;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import java.util.ArrayList;

/* compiled from: NotificationCompat */
public class NotificationCompat0 {
    public Context a;
    public ArrayList<e> b = new ArrayList<>();
    public ArrayList<e> c = new ArrayList<>();
    public CharSequence d;

    /* renamed from: e  reason: collision with root package name */
    public CharSequence f1162e;

    /* renamed from: f  reason: collision with root package name */
    public PendingIntent f1163f;
    public int g;
    public boolean h = true;

    /* renamed from: i  reason: collision with root package name */
    public NotificationCompat1 f1164i;

    /* renamed from: j  reason: collision with root package name */
    public boolean f1165j = false;

    /* renamed from: k  reason: collision with root package name */
    public Bundle f1166k;

    /* renamed from: l  reason: collision with root package name */
    public int f1167l = 0;

    /* renamed from: m  reason: collision with root package name */
    public int f1168m = 0;

    /* renamed from: n  reason: collision with root package name */
    public String f1169n;

    /* renamed from: o  reason: collision with root package name */
    public int f1170o = 0;

    /* renamed from: p  reason: collision with root package name */
    public int f1171p = 0;

    /* renamed from: q  reason: collision with root package name */
    public boolean f1172q;

    /* renamed from: r  reason: collision with root package name */
    public Notification f1173r;
    @Deprecated

    /* renamed from: s  reason: collision with root package name */
    public ArrayList<String> f1174s;

    @Deprecated
    public NotificationCompat0(Context context) {
        Notification notification = new Notification();
        this.f1173r = notification;
        this.a = context;
        this.f1169n = null;
        notification.when = System.currentTimeMillis();
        this.f1173r.audioStreamType = -1;
        this.g = 0;
        this.f1174s = new ArrayList<>();
        this.f1172q = true;
    }

    public NotificationCompat0 a(NotificationCompat1 notificationCompat1) {
        if (this.f1164i != notificationCompat1) {
            this.f1164i = notificationCompat1;
            if (!(notificationCompat1 == null || notificationCompat1.a == this)) {
                notificationCompat1.a = this;
                a(notificationCompat1);
            }
        }
        return this;
    }

    public static CharSequence a(CharSequence charSequence) {
        return (charSequence != null && charSequence.length() > 5120) ? charSequence.subSequence(0, 5120) : charSequence;
    }
}
