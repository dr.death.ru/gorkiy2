package i.h.d;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import i.h.l.KeyEventDispatcher;
import i.h.l.ViewCompat;
import i.o.Lifecycle;
import i.o.LifecycleOwner;
import i.o.LifecycleRegistry;
import i.o.ReportFragment;

public class ComponentActivity extends Activity implements LifecycleOwner, KeyEventDispatcher.a {
    public LifecycleRegistry b = new LifecycleRegistry(this);

    public boolean a(KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent);
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        View decorView = getWindow().getDecorView();
        if (decorView == null || !ViewCompat.b(decorView, keyEvent)) {
            return KeyEventDispatcher.a(this, decorView, this, keyEvent);
        }
        return true;
    }

    public boolean dispatchKeyShortcutEvent(KeyEvent keyEvent) {
        View decorView = getWindow().getDecorView();
        if (decorView == null || !ViewCompat.b(decorView, keyEvent)) {
            return super.dispatchKeyShortcutEvent(keyEvent);
        }
        return true;
    }

    @SuppressLint({"RestrictedApi"})
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ReportFragment.b(super);
    }

    public void onSaveInstanceState(Bundle bundle) {
        this.b.a(Lifecycle.b.CREATED);
        super.onSaveInstanceState(bundle);
    }
}
