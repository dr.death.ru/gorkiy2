package i.h.f.j;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;

public final class WrappedDrawableState extends Drawable.ConstantState {
    public int a;
    public Drawable.ConstantState b;
    public ColorStateList c = null;
    public PorterDuff.Mode d = WrappedDrawableApi14.h;

    public WrappedDrawableState(WrappedDrawableState wrappedDrawableState) {
        if (wrappedDrawableState != null) {
            this.a = wrappedDrawableState.a;
            this.b = wrappedDrawableState.b;
            this.c = wrappedDrawableState.c;
            this.d = wrappedDrawableState.d;
        }
    }

    public int getChangingConfigurations() {
        int i2 = this.a;
        Drawable.ConstantState constantState = this.b;
        return i2 | (constantState != null ? super.getChangingConfigurations() : 0);
    }

    public Drawable newDrawable() {
        return new WrappedDrawableApi21(this, null);
    }

    public Drawable newDrawable(Resources resources) {
        return new WrappedDrawableApi21(this, resources);
    }
}
