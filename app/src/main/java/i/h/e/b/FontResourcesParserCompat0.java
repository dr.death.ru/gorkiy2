package i.h.e.b;

/* compiled from: FontResourcesParserCompat */
public final class FontResourcesParserCompat0 {
    public final String a;
    public int b;
    public boolean c;
    public String d;

    /* renamed from: e  reason: collision with root package name */
    public int f1175e;

    /* renamed from: f  reason: collision with root package name */
    public int f1176f;

    public FontResourcesParserCompat0(String str, int i2, boolean z, String str2, int i3, int i4) {
        this.a = str;
        this.b = i2;
        this.c = z;
        this.d = str2;
        this.f1175e = i3;
        this.f1176f = i4;
    }
}
