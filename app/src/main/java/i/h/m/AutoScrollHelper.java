package i.h.m;

import android.content.res.Resources;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import i.h.l.ViewCompat;

public abstract class AutoScrollHelper implements View.OnTouchListener {

    /* renamed from: r  reason: collision with root package name */
    public static final int f1208r = ViewConfiguration.getTapTimeout();
    public final a b = new a();
    public final Interpolator c = new AccelerateInterpolator();
    public final View d;

    /* renamed from: e  reason: collision with root package name */
    public Runnable f1209e;

    /* renamed from: f  reason: collision with root package name */
    public float[] f1210f = {0.0f, 0.0f};
    public float[] g = {Float.MAX_VALUE, Float.MAX_VALUE};
    public int h;

    /* renamed from: i  reason: collision with root package name */
    public int f1211i;

    /* renamed from: j  reason: collision with root package name */
    public float[] f1212j = {0.0f, 0.0f};

    /* renamed from: k  reason: collision with root package name */
    public float[] f1213k = {0.0f, 0.0f};

    /* renamed from: l  reason: collision with root package name */
    public float[] f1214l = {Float.MAX_VALUE, Float.MAX_VALUE};

    /* renamed from: m  reason: collision with root package name */
    public boolean f1215m;

    /* renamed from: n  reason: collision with root package name */
    public boolean f1216n;

    /* renamed from: o  reason: collision with root package name */
    public boolean f1217o;

    /* renamed from: p  reason: collision with root package name */
    public boolean f1218p;

    /* renamed from: q  reason: collision with root package name */
    public boolean f1219q;

    public static class a {
        public int a;
        public int b;
        public float c;
        public float d;

        /* renamed from: e  reason: collision with root package name */
        public long f1220e = Long.MIN_VALUE;

        /* renamed from: f  reason: collision with root package name */
        public long f1221f = 0;
        public int g = 0;
        public int h = 0;

        /* renamed from: i  reason: collision with root package name */
        public long f1222i = -1;

        /* renamed from: j  reason: collision with root package name */
        public float f1223j;

        /* renamed from: k  reason: collision with root package name */
        public int f1224k;

        public final float a(long j2) {
            if (j2 < this.f1220e) {
                return 0.0f;
            }
            long j3 = this.f1222i;
            if (j3 < 0 || j2 < j3) {
                return AutoScrollHelper.a(((float) (j2 - this.f1220e)) / ((float) this.a), 0.0f, 1.0f) * 0.5f;
            }
            long j4 = j2 - j3;
            float f2 = this.f1223j;
            return (AutoScrollHelper.a(((float) j4) / ((float) this.f1224k), 0.0f, 1.0f) * f2) + (1.0f - f2);
        }
    }

    public class b implements Runnable {
        public b() {
        }

        public void run() {
            AutoScrollHelper autoScrollHelper = AutoScrollHelper.this;
            if (autoScrollHelper.f1218p) {
                if (autoScrollHelper.f1216n) {
                    autoScrollHelper.f1216n = false;
                    a aVar = autoScrollHelper.b;
                    if (aVar != null) {
                        long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
                        aVar.f1220e = currentAnimationTimeMillis;
                        aVar.f1222i = -1;
                        aVar.f1221f = currentAnimationTimeMillis;
                        aVar.f1223j = 0.5f;
                        aVar.g = 0;
                        aVar.h = 0;
                    } else {
                        throw null;
                    }
                }
                a aVar2 = AutoScrollHelper.this.b;
                if ((aVar2.f1222i > 0 && AnimationUtils.currentAnimationTimeMillis() > aVar2.f1222i + ((long) aVar2.f1224k)) || !AutoScrollHelper.this.b()) {
                    AutoScrollHelper.this.f1218p = false;
                    return;
                }
                AutoScrollHelper autoScrollHelper2 = AutoScrollHelper.this;
                if (autoScrollHelper2.f1217o) {
                    autoScrollHelper2.f1217o = false;
                    if (autoScrollHelper2 != null) {
                        long uptimeMillis = SystemClock.uptimeMillis();
                        MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
                        autoScrollHelper2.d.onTouchEvent(obtain);
                        obtain.recycle();
                    } else {
                        throw null;
                    }
                }
                if (aVar2.f1221f != 0) {
                    long currentAnimationTimeMillis2 = AnimationUtils.currentAnimationTimeMillis();
                    float a = aVar2.a(currentAnimationTimeMillis2);
                    aVar2.f1221f = currentAnimationTimeMillis2;
                    float f2 = ((float) (currentAnimationTimeMillis2 - aVar2.f1221f)) * ((a * 4.0f) + (-4.0f * a * a));
                    aVar2.g = (int) (aVar2.c * f2);
                    int i2 = (int) (f2 * aVar2.d);
                    aVar2.h = i2;
                    ((ListViewAutoScrollHelper) AutoScrollHelper.this).f1225s.scrollListBy(i2);
                    ViewCompat.a(AutoScrollHelper.this.d, this);
                    return;
                }
                throw new RuntimeException("Cannot compute scroll delta before calling start()");
            }
        }
    }

    public AutoScrollHelper(View view) {
        this.d = view;
        float f2 = Resources.getSystem().getDisplayMetrics().density;
        float[] fArr = this.f1214l;
        float f3 = ((float) ((int) ((1575.0f * f2) + 0.5f))) / 1000.0f;
        fArr[0] = f3;
        fArr[1] = f3;
        float[] fArr2 = this.f1213k;
        float f4 = ((float) ((int) ((f2 * 315.0f) + 0.5f))) / 1000.0f;
        fArr2[0] = f4;
        fArr2[1] = f4;
        this.h = 1;
        float[] fArr3 = this.g;
        fArr3[0] = Float.MAX_VALUE;
        fArr3[1] = Float.MAX_VALUE;
        float[] fArr4 = this.f1210f;
        fArr4[0] = 0.2f;
        fArr4[1] = 0.2f;
        float[] fArr5 = this.f1212j;
        fArr5[0] = 0.001f;
        fArr5[1] = 0.001f;
        this.f1211i = f1208r;
        a aVar = this.b;
        aVar.a = 500;
        aVar.b = 500;
    }

    public static float a(float f2, float f3, float f4) {
        return f2 > f4 ? f4 : f2 < f3 ? f3 : f2;
    }

    public final void a() {
        int i2 = 0;
        if (this.f1216n) {
            this.f1218p = false;
            return;
        }
        a aVar = this.b;
        if (aVar != null) {
            long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
            int i3 = (int) (currentAnimationTimeMillis - aVar.f1220e);
            int i4 = aVar.b;
            if (i3 > i4) {
                i2 = i4;
            } else if (i3 >= 0) {
                i2 = i3;
            }
            aVar.f1224k = i2;
            aVar.f1223j = aVar.a(currentAnimationTimeMillis);
            aVar.f1222i = currentAnimationTimeMillis;
            return;
        }
        throw null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean b() {
        /*
            r9 = this;
            i.h.m.AutoScrollHelper$a r0 = r9.b
            float r1 = r0.d
            float r2 = java.lang.Math.abs(r1)
            float r1 = r1 / r2
            int r1 = (int) r1
            float r0 = r0.c
            float r2 = java.lang.Math.abs(r0)
            float r0 = r0 / r2
            int r0 = (int) r0
            r2 = 1
            r3 = 0
            if (r1 == 0) goto L_0x0053
            r4 = r9
            i.h.m.ListViewAutoScrollHelper r4 = (i.h.m.ListViewAutoScrollHelper) r4
            android.widget.ListView r4 = r4.f1225s
            int r5 = r4.getCount()
            if (r5 != 0) goto L_0x0023
        L_0x0021:
            r1 = 0
            goto L_0x0051
        L_0x0023:
            int r6 = r4.getChildCount()
            int r7 = r4.getFirstVisiblePosition()
            int r8 = r7 + r6
            if (r1 <= 0) goto L_0x0041
            if (r8 < r5) goto L_0x0050
            int r6 = r6 - r2
            android.view.View r1 = r4.getChildAt(r6)
            int r1 = r1.getBottom()
            int r4 = r4.getHeight()
            if (r1 > r4) goto L_0x0050
            goto L_0x0021
        L_0x0041:
            if (r1 >= 0) goto L_0x0021
            if (r7 > 0) goto L_0x0050
            android.view.View r1 = r4.getChildAt(r3)
            int r1 = r1.getTop()
            if (r1 < 0) goto L_0x0050
            goto L_0x0021
        L_0x0050:
            r1 = 1
        L_0x0051:
            if (r1 != 0) goto L_0x0054
        L_0x0053:
            r2 = 0
        L_0x0054:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: i.h.m.AutoScrollHelper.b():boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0013, code lost:
        if (r0 != 3) goto L_0x007d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouch(android.view.View r6, android.view.MotionEvent r7) {
        /*
            r5 = this;
            boolean r0 = r5.f1219q
            r1 = 0
            if (r0 != 0) goto L_0x0006
            return r1
        L_0x0006:
            int r0 = r7.getActionMasked()
            r2 = 1
            if (r0 == 0) goto L_0x001a
            if (r0 == r2) goto L_0x0016
            r3 = 2
            if (r0 == r3) goto L_0x001e
            r6 = 3
            if (r0 == r6) goto L_0x0016
            goto L_0x007d
        L_0x0016:
            r5.a()
            goto L_0x007d
        L_0x001a:
            r5.f1217o = r2
            r5.f1215m = r1
        L_0x001e:
            float r0 = r7.getX()
            int r3 = r6.getWidth()
            float r3 = (float) r3
            android.view.View r4 = r5.d
            int r4 = r4.getWidth()
            float r4 = (float) r4
            float r0 = r5.a(r1, r0, r3, r4)
            float r7 = r7.getY()
            int r6 = r6.getHeight()
            float r6 = (float) r6
            android.view.View r3 = r5.d
            int r3 = r3.getHeight()
            float r3 = (float) r3
            float r6 = r5.a(r2, r7, r6, r3)
            i.h.m.AutoScrollHelper$a r7 = r5.b
            r7.c = r0
            r7.d = r6
            boolean r6 = r5.f1218p
            if (r6 != 0) goto L_0x007d
            boolean r6 = r5.b()
            if (r6 == 0) goto L_0x007d
            java.lang.Runnable r6 = r5.f1209e
            if (r6 != 0) goto L_0x0061
            i.h.m.AutoScrollHelper$b r6 = new i.h.m.AutoScrollHelper$b
            r6.<init>()
            r5.f1209e = r6
        L_0x0061:
            r5.f1218p = r2
            r5.f1216n = r2
            boolean r6 = r5.f1215m
            if (r6 != 0) goto L_0x0076
            int r6 = r5.f1211i
            if (r6 <= 0) goto L_0x0076
            android.view.View r7 = r5.d
            java.lang.Runnable r0 = r5.f1209e
            long r3 = (long) r6
            i.h.l.ViewCompat.a(r7, r0, r3)
            goto L_0x007b
        L_0x0076:
            java.lang.Runnable r6 = r5.f1209e
            r6.run()
        L_0x007b:
            r5.f1215m = r2
        L_0x007d:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: i.h.m.AutoScrollHelper.onTouch(android.view.View, android.view.MotionEvent):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x003e A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final float a(int r4, float r5, float r6, float r7) {
        /*
            r3 = this;
            float[] r0 = r3.f1210f
            r0 = r0[r4]
            float[] r1 = r3.g
            r1 = r1[r4]
            float r0 = r0 * r6
            r2 = 0
            float r0 = a(r0, r2, r1)
            float r1 = r3.a(r5, r0)
            float r6 = r6 - r5
            float r5 = r3.a(r6, r0)
            float r5 = r5 - r1
            int r6 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r6 >= 0) goto L_0x0026
            android.view.animation.Interpolator r6 = r3.c
            float r5 = -r5
            float r5 = r6.getInterpolation(r5)
            float r5 = -r5
            goto L_0x0030
        L_0x0026:
            int r6 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r6 <= 0) goto L_0x0039
            android.view.animation.Interpolator r6 = r3.c
            float r5 = r6.getInterpolation(r5)
        L_0x0030:
            r6 = -1082130432(0xffffffffbf800000, float:-1.0)
            r0 = 1065353216(0x3f800000, float:1.0)
            float r5 = a(r5, r6, r0)
            goto L_0x003a
        L_0x0039:
            r5 = 0
        L_0x003a:
            int r6 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r6 != 0) goto L_0x003f
            return r2
        L_0x003f:
            float[] r0 = r3.f1212j
            r0 = r0[r4]
            float[] r1 = r3.f1213k
            r1 = r1[r4]
            float[] r2 = r3.f1214l
            r4 = r2[r4]
            float r0 = r0 * r7
            if (r6 <= 0) goto L_0x0056
            float r5 = r5 * r0
            float r4 = a(r5, r1, r4)
            return r4
        L_0x0056:
            float r5 = -r5
            float r5 = r5 * r0
            float r4 = a(r5, r1, r4)
            float r4 = -r4
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: i.h.m.AutoScrollHelper.a(int, float, float, float):float");
    }

    public final float a(float f2, float f3) {
        if (f3 == 0.0f) {
            return 0.0f;
        }
        int i2 = this.h;
        if (i2 == 0 || i2 == 1) {
            if (f2 < f3) {
                if (f2 >= 0.0f) {
                    return 1.0f - (f2 / f3);
                }
                return (!this.f1218p || this.h != 1) ? 0.0f : 1.0f;
            }
        } else if (i2 == 2 && f2 < 0.0f) {
            return f2 / (-f3);
        }
    }
}
