package i.o;

import android.app.Activity;
import android.os.Bundle;
import i.o.Lifecycle;

/* compiled from: ProcessLifecycleOwner */
public class ProcessLifecycleOwner0 extends EmptyActivityLifecycleCallbacks {
    public final /* synthetic */ ProcessLifecycleOwner b;

    public ProcessLifecycleOwner0(ProcessLifecycleOwner processLifecycleOwner) {
        this.b = processLifecycleOwner;
    }

    public void onActivityCreated(Activity activity, Bundle bundle) {
        ReportFragment.a(activity).b = this.b.f1361i;
    }

    public void onActivityPaused(Activity activity) {
        ProcessLifecycleOwner processLifecycleOwner = this.b;
        int i2 = processLifecycleOwner.c - 1;
        processLifecycleOwner.c = i2;
        if (i2 == 0) {
            processLifecycleOwner.f1360f.postDelayed(processLifecycleOwner.h, 700);
        }
    }

    public void onActivityStopped(Activity activity) {
        ProcessLifecycleOwner processLifecycleOwner = this.b;
        int i2 = processLifecycleOwner.b - 1;
        processLifecycleOwner.b = i2;
        if (i2 == 0 && processLifecycleOwner.d) {
            processLifecycleOwner.g.a(Lifecycle.a.ON_STOP);
            processLifecycleOwner.f1359e = true;
        }
    }
}
