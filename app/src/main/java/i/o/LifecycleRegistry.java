package i.o;

import i.c.a.b.FastSafeIterableMap;
import i.c.a.b.SafeIterableMap;
import i.o.Lifecycle;
import i.o.g;
import i.o.l;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Map;

public class LifecycleRegistry extends Lifecycle {
    public FastSafeIterableMap<j, l.a> a = new FastSafeIterableMap<>();
    public Lifecycle.b b;
    public final WeakReference<k> c;
    public int d = 0;

    /* renamed from: e  reason: collision with root package name */
    public boolean f1356e = false;

    /* renamed from: f  reason: collision with root package name */
    public boolean f1357f = false;
    public ArrayList<g.b> g = new ArrayList<>();

    public static class a {
        public Lifecycle.b a;
        public LifecycleEventObserver b;

        public a(LifecycleObserver lifecycleObserver, Lifecycle.b bVar) {
            this.b = Lifecycling.a(lifecycleObserver);
            this.a = bVar;
        }

        public void a(LifecycleOwner lifecycleOwner, Lifecycle.a aVar) {
            Lifecycle.b b2 = LifecycleRegistry.b(aVar);
            this.a = LifecycleRegistry.a(this.a, b2);
            this.b.a(lifecycleOwner, aVar);
            this.a = b2;
        }
    }

    public LifecycleRegistry(LifecycleOwner lifecycleOwner) {
        this.c = new WeakReference<>(lifecycleOwner);
        this.b = Lifecycle.b.INITIALIZED;
    }

    public void a(Lifecycle.a aVar) {
        a(b(aVar));
    }

    /* JADX WARN: Type inference failed for: r0v5, types: [java.lang.Object] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final i.o.Lifecycle.b b(i.o.LifecycleObserver r4) {
        /*
            r3 = this;
            i.c.a.b.FastSafeIterableMap<i.o.j, i.o.l$a> r0 = r3.a
            java.util.HashMap<K, i.c.a.b.SafeIterableMap$c<K, V>> r1 = r0.f1049f
            boolean r1 = r1.containsKey(r4)
            r2 = 0
            if (r1 == 0) goto L_0x0016
            java.util.HashMap<K, i.c.a.b.SafeIterableMap$c<K, V>> r0 = r0.f1049f
            java.lang.Object r4 = r0.get(r4)
            i.c.a.b.SafeIterableMap$c r4 = (i.c.a.b.SafeIterableMap.c) r4
            i.c.a.b.SafeIterableMap$c<K, V> r4 = r4.f1051e
            goto L_0x0017
        L_0x0016:
            r4 = r2
        L_0x0017:
            if (r4 == 0) goto L_0x0022
            java.lang.Object r4 = r4.getValue()
            i.o.LifecycleRegistry$a r4 = (i.o.LifecycleRegistry.a) r4
            i.o.Lifecycle$b r4 = r4.a
            goto L_0x0023
        L_0x0022:
            r4 = r2
        L_0x0023:
            java.util.ArrayList<i.o.g$b> r0 = r3.g
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x003a
            java.util.ArrayList<i.o.g$b> r0 = r3.g
            int r1 = r0.size()
            int r1 = r1 + -1
            java.lang.Object r0 = r0.get(r1)
            r2 = r0
            i.o.Lifecycle$b r2 = (i.o.Lifecycle.b) r2
        L_0x003a:
            i.o.Lifecycle$b r0 = r3.b
            i.o.Lifecycle$b r4 = a(r0, r4)
            i.o.Lifecycle$b r4 = a(r4, r2)
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: i.o.LifecycleRegistry.b(i.o.LifecycleObserver):i.o.Lifecycle$b");
    }

    public final void a(Lifecycle.b bVar) {
        if (this.b != bVar) {
            this.b = bVar;
            if (this.f1356e || this.d != 0) {
                this.f1357f = true;
                return;
            }
            this.f1356e = true;
            b();
            this.f1356e = false;
        }
    }

    public static Lifecycle.b b(Lifecycle.a aVar) {
        int ordinal = aVar.ordinal();
        if (ordinal != 0) {
            if (ordinal != 1) {
                if (ordinal == 2) {
                    return Lifecycle.b.RESUMED;
                }
                if (ordinal != 3) {
                    if (ordinal != 4) {
                        if (ordinal == 5) {
                            return Lifecycle.b.DESTROYED;
                        }
                        throw new IllegalArgumentException("Unexpected event value " + aVar);
                    }
                }
            }
            return Lifecycle.b.STARTED;
        }
        return Lifecycle.b.CREATED;
    }

    public void a(LifecycleObserver lifecycleObserver) {
        LifecycleOwner lifecycleOwner;
        Lifecycle.b bVar = this.b;
        Lifecycle.b bVar2 = Lifecycle.b.DESTROYED;
        if (bVar != bVar2) {
            bVar2 = Lifecycle.b.INITIALIZED;
        }
        a aVar = new a(lifecycleObserver, bVar2);
        if (this.a.b(lifecycleObserver, aVar) == null && (lifecycleOwner = this.c.get()) != null) {
            boolean z = this.d != 0 || this.f1356e;
            Lifecycle.b b2 = b(lifecycleObserver);
            this.d++;
            while (aVar.a.compareTo((Enum) b2) < 0 && this.a.f1049f.containsKey(lifecycleObserver)) {
                this.g.add(aVar.a);
                aVar.a(lifecycleOwner, b(aVar.a));
                a();
                b2 = b(lifecycleObserver);
            }
            if (!z) {
                b();
            }
            this.d--;
        }
    }

    public static Lifecycle.a b(Lifecycle.b bVar) {
        int ordinal = bVar.ordinal();
        if (ordinal == 0 || ordinal == 1) {
            return Lifecycle.a.ON_CREATE;
        }
        if (ordinal == 2) {
            return Lifecycle.a.ON_START;
        }
        if (ordinal == 3) {
            return Lifecycle.a.ON_RESUME;
        }
        if (ordinal != 4) {
            throw new IllegalArgumentException("Unexpected state value " + bVar);
        }
        throw new IllegalArgumentException();
    }

    public final void b() {
        Lifecycle.a aVar;
        Lifecycle.b bVar;
        LifecycleOwner lifecycleOwner = this.c.get();
        if (lifecycleOwner != null) {
            while (true) {
                FastSafeIterableMap<j, l.a> fastSafeIterableMap = this.a;
                if (!(fastSafeIterableMap.f1050e == 0 || (((a) fastSafeIterableMap.b.getValue()).a == (bVar = ((a) this.a.c.getValue()).a) && this.b == bVar))) {
                    this.f1357f = false;
                    if (this.b.compareTo((Enum) ((a) this.a.b.getValue()).a) < 0) {
                        FastSafeIterableMap<j, l.a> fastSafeIterableMap2 = this.a;
                        SafeIterableMap.b bVar2 = new SafeIterableMap.b(fastSafeIterableMap2.c, fastSafeIterableMap2.b);
                        fastSafeIterableMap2.d.put(bVar2, false);
                        while (bVar2.hasNext() && !this.f1357f) {
                            Map.Entry entry = (Map.Entry) bVar2.next();
                            a aVar2 = (a) entry.getValue();
                            while (aVar2.a.compareTo((Enum) this.b) > 0 && !this.f1357f && this.a.contains(entry.getKey())) {
                                Lifecycle.b bVar3 = aVar2.a;
                                int ordinal = bVar3.ordinal();
                                if (ordinal == 0) {
                                    throw new IllegalArgumentException();
                                } else if (ordinal != 1) {
                                    if (ordinal == 2) {
                                        aVar = Lifecycle.a.ON_DESTROY;
                                    } else if (ordinal == 3) {
                                        aVar = Lifecycle.a.ON_STOP;
                                    } else if (ordinal == 4) {
                                        aVar = Lifecycle.a.ON_PAUSE;
                                    } else {
                                        throw new IllegalArgumentException("Unexpected state value " + bVar3);
                                    }
                                    this.g.add(b(aVar));
                                    aVar2.a(lifecycleOwner, aVar);
                                    a();
                                } else {
                                    throw new IllegalArgumentException();
                                }
                            }
                        }
                    }
                    SafeIterableMap.c<K, V> cVar = this.a.c;
                    if (!this.f1357f && cVar != null && this.b.compareTo((Enum) ((a) cVar.getValue()).a) > 0) {
                        SafeIterableMap<K, V>.d c2 = this.a.c();
                        while (c2.hasNext() && !this.f1357f) {
                            Map.Entry entry2 = (Map.Entry) c2.next();
                            a aVar3 = (a) entry2.getValue();
                            while (aVar3.a.compareTo((Enum) this.b) < 0 && !this.f1357f && this.a.contains(entry2.getKey())) {
                                this.g.add(aVar3.a);
                                aVar3.a(lifecycleOwner, b(aVar3.a));
                                a();
                            }
                        }
                    }
                } else {
                    this.f1357f = false;
                    return;
                }
            }
        } else {
            throw new IllegalStateException("LifecycleOwner of this LifecycleRegistry is alreadygarbage collected. It is too late to change lifecycle state.");
        }
    }

    public final void a() {
        ArrayList<g.b> arrayList = this.g;
        arrayList.remove(arrayList.size() - 1);
    }

    public static Lifecycle.b a(Lifecycle.b bVar, Lifecycle.b bVar2) {
        return (bVar2 == null || bVar2.compareTo(bVar) >= 0) ? bVar : bVar2;
    }
}
