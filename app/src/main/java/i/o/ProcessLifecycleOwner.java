package i.o;

import android.os.Handler;
import i.o.Lifecycle;
import i.o.ReportFragment;

public class ProcessLifecycleOwner implements LifecycleOwner {

    /* renamed from: j  reason: collision with root package name */
    public static final ProcessLifecycleOwner f1358j = new ProcessLifecycleOwner();
    public int b = 0;
    public int c = 0;
    public boolean d = true;

    /* renamed from: e  reason: collision with root package name */
    public boolean f1359e = true;

    /* renamed from: f  reason: collision with root package name */
    public Handler f1360f;
    public final LifecycleRegistry g = new LifecycleRegistry(this);
    public Runnable h = new a();

    /* renamed from: i  reason: collision with root package name */
    public ReportFragment.a f1361i = new b();

    public class a implements Runnable {
        public a() {
        }

        public void run() {
            ProcessLifecycleOwner processLifecycleOwner = ProcessLifecycleOwner.this;
            if (processLifecycleOwner.c == 0) {
                processLifecycleOwner.d = true;
                processLifecycleOwner.g.a(Lifecycle.a.ON_PAUSE);
            }
            ProcessLifecycleOwner processLifecycleOwner2 = ProcessLifecycleOwner.this;
            if (processLifecycleOwner2.b == 0 && processLifecycleOwner2.d) {
                processLifecycleOwner2.g.a(Lifecycle.a.ON_STOP);
                processLifecycleOwner2.f1359e = true;
            }
        }
    }

    public class b implements ReportFragment.a {
        public b() {
        }
    }

    public Lifecycle a() {
        return this.g;
    }
}
