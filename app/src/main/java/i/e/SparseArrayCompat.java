package i.e;

public class SparseArrayCompat<E> implements Cloneable {

    /* renamed from: f  reason: collision with root package name */
    public static final Object f1072f = new Object();
    public boolean b;
    public int[] c;
    public Object[] d;

    /* renamed from: e  reason: collision with root package name */
    public int f1073e;

    public SparseArrayCompat() {
        this(10);
    }

    public E a(int i2) {
        return b(i2, null);
    }

    public E b(int i2, E e2) {
        int a = ContainerHelpers.a(this.c, this.f1073e, i2);
        if (a >= 0) {
            E[] eArr = this.d;
            if (eArr[a] != f1072f) {
                return eArr[a];
            }
        }
        return e2;
    }

    public void c(int i2) {
        Object[] objArr;
        Object obj;
        int a = ContainerHelpers.a(this.c, this.f1073e, i2);
        if (a >= 0 && (objArr = this.d)[a] != (obj = f1072f)) {
            objArr[a] = obj;
            this.b = true;
        }
    }

    public E d(int i2) {
        if (this.b) {
            b();
        }
        return this.d[i2];
    }

    public String toString() {
        if (c() <= 0) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.f1073e * 28);
        sb.append('{');
        for (int i2 = 0; i2 < this.f1073e; i2++) {
            if (i2 > 0) {
                sb.append(", ");
            }
            sb.append(b(i2));
            sb.append('=');
            Object d2 = d(i2);
            if (d2 != this) {
                sb.append(d2);
            } else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }

    public SparseArrayCompat(int i2) {
        this.b = false;
        if (i2 == 0) {
            this.c = ContainerHelpers.a;
            this.d = ContainerHelpers.c;
            return;
        }
        int b2 = ContainerHelpers.b(i2);
        this.c = new int[b2];
        this.d = new Object[b2];
    }

    public void a(int i2, E e2) {
        int i3 = this.f1073e;
        if (i3 == 0 || i2 > this.c[i3 - 1]) {
            if (this.b && this.f1073e >= this.c.length) {
                b();
            }
            int i4 = this.f1073e;
            if (i4 >= this.c.length) {
                int b2 = ContainerHelpers.b(i4 + 1);
                int[] iArr = new int[b2];
                Object[] objArr = new Object[b2];
                int[] iArr2 = this.c;
                System.arraycopy(iArr2, 0, iArr, 0, iArr2.length);
                Object[] objArr2 = this.d;
                System.arraycopy(objArr2, 0, objArr, 0, objArr2.length);
                this.c = iArr;
                this.d = objArr;
            }
            this.c[i4] = i2;
            this.d[i4] = e2;
            this.f1073e = i4 + 1;
            return;
        }
        c(i2, e2);
    }

    public SparseArrayCompat<E> clone() {
        try {
            SparseArrayCompat<E> sparseArrayCompat = (SparseArrayCompat) super.clone();
            sparseArrayCompat.c = (int[]) this.c.clone();
            sparseArrayCompat.d = (Object[]) this.d.clone();
            return sparseArrayCompat;
        } catch (CloneNotSupportedException e2) {
            throw new AssertionError(e2);
        }
    }

    public final void b() {
        int i2 = this.f1073e;
        int[] iArr = this.c;
        Object[] objArr = this.d;
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            Object obj = objArr[i4];
            if (obj != f1072f) {
                if (i4 != i3) {
                    iArr[i3] = iArr[i4];
                    objArr[i3] = obj;
                    objArr[i4] = null;
                }
                i3++;
            }
        }
        this.b = false;
        this.f1073e = i3;
    }

    public void c(int i2, E e2) {
        int a = ContainerHelpers.a(this.c, this.f1073e, i2);
        if (a >= 0) {
            this.d[a] = e2;
            return;
        }
        int i3 = ~a;
        if (i3 < this.f1073e) {
            Object[] objArr = this.d;
            if (objArr[i3] == f1072f) {
                this.c[i3] = i2;
                objArr[i3] = e2;
                return;
            }
        }
        if (this.b && this.f1073e >= this.c.length) {
            b();
            i3 = ~ContainerHelpers.a(this.c, this.f1073e, i2);
        }
        int i4 = this.f1073e;
        if (i4 >= this.c.length) {
            int b2 = ContainerHelpers.b(i4 + 1);
            int[] iArr = new int[b2];
            Object[] objArr2 = new Object[b2];
            int[] iArr2 = this.c;
            System.arraycopy(iArr2, 0, iArr, 0, iArr2.length);
            Object[] objArr3 = this.d;
            System.arraycopy(objArr3, 0, objArr2, 0, objArr3.length);
            this.c = iArr;
            this.d = objArr2;
        }
        int i5 = this.f1073e;
        if (i5 - i3 != 0) {
            int[] iArr3 = this.c;
            int i6 = i3 + 1;
            System.arraycopy(iArr3, i3, iArr3, i6, i5 - i3);
            Object[] objArr4 = this.d;
            System.arraycopy(objArr4, i3, objArr4, i6, this.f1073e - i3);
        }
        this.c[i3] = i2;
        this.d[i3] = e2;
        this.f1073e++;
    }

    public int b(int i2) {
        if (this.b) {
            b();
        }
        return this.c[i2];
    }

    public int c() {
        if (this.b) {
            b();
        }
        return this.f1073e;
    }
}
