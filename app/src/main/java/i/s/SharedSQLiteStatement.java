package i.s;

import i.v.a.e.FrameworkSQLiteDatabase0;
import i.v.a.e.FrameworkSQLiteStatement;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class SharedSQLiteStatement {
    public final AtomicBoolean a = new AtomicBoolean(false);
    public final RoomDatabase b;
    public volatile FrameworkSQLiteStatement c;

    public SharedSQLiteStatement(RoomDatabase roomDatabase) {
        this.b = roomDatabase;
    }

    public final FrameworkSQLiteStatement a() {
        String b2 = b();
        RoomDatabase roomDatabase = this.b;
        roomDatabase.b();
        roomDatabase.c();
        return new FrameworkSQLiteStatement(((FrameworkSQLiteDatabase0) roomDatabase.c.a()).b.compileStatement(b2));
    }

    public abstract String b();
}
