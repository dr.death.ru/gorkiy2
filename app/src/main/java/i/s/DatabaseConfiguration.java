package i.s;

import android.content.Context;
import i.s.RoomDatabase;
import i.s.h;
import i.v.a.SupportSQLiteOpenHelper;
import i.v.a.c;
import java.io.File;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;

public class DatabaseConfiguration {
    public final SupportSQLiteOpenHelper.c a;
    public final Context b;
    public final String c;
    public final RoomDatabase.c d;

    /* renamed from: e  reason: collision with root package name */
    public final List<h.a> f1415e;

    /* renamed from: f  reason: collision with root package name */
    public final boolean f1416f;
    public final RoomDatabase.b g;
    public final Executor h;

    /* renamed from: i  reason: collision with root package name */
    public final Executor f1417i;

    /* renamed from: j  reason: collision with root package name */
    public final boolean f1418j;

    /* renamed from: k  reason: collision with root package name */
    public final boolean f1419k;

    /* renamed from: l  reason: collision with root package name */
    public final boolean f1420l;

    /* renamed from: m  reason: collision with root package name */
    public final Set<Integer> f1421m;

    public DatabaseConfiguration(Context context, String str, c.c cVar, h.c cVar2, List<h.a> list, boolean z, h.b bVar, Executor executor, Executor executor2, boolean z2, boolean z3, boolean z4, Set<Integer> set, String str2, File file) {
        this.a = cVar;
        this.b = context;
        this.c = str;
        this.d = cVar2;
        this.f1415e = list;
        this.f1416f = z;
        this.g = bVar;
        this.h = executor;
        this.f1417i = executor2;
        this.f1418j = z2;
        this.f1419k = z3;
        this.f1420l = z4;
        this.f1421m = set;
    }

    public boolean a(int i2, int i3) {
        Set<Integer> set;
        if ((!(i2 > i3) || !this.f1420l) && this.f1419k && ((set = this.f1421m) == null || !set.contains(Integer.valueOf(i2)))) {
            return true;
        }
        return false;
    }
}
