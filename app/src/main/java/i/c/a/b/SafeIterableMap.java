package i.c.a.b;

import j.a.a.a.outline;
import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;

public class SafeIterableMap<K, V> implements Iterable<Map.Entry<K, V>> {
    public c<K, V> b;
    public c<K, V> c;
    public WeakHashMap<f<K, V>, Boolean> d = new WeakHashMap<>();

    /* renamed from: e  reason: collision with root package name */
    public int f1050e = 0;

    public static class a<K, V> extends e<K, V> {
        public a(c<K, V> cVar, c<K, V> cVar2) {
            super(cVar, cVar2);
        }

        public c<K, V> b(c<K, V> cVar) {
            return cVar.f1051e;
        }

        public c<K, V> c(c<K, V> cVar) {
            return cVar.d;
        }
    }

    public static class b<K, V> extends e<K, V> {
        public b(c<K, V> cVar, c<K, V> cVar2) {
            super(cVar, cVar2);
        }

        public c<K, V> b(c<K, V> cVar) {
            return cVar.d;
        }

        public c<K, V> c(c<K, V> cVar) {
            return cVar.f1051e;
        }
    }

    public static class c<K, V> implements Map.Entry<K, V> {
        public final K b;
        public final V c;
        public c<K, V> d;

        /* renamed from: e  reason: collision with root package name */
        public c<K, V> f1051e;

        public c(K k2, V v) {
            this.b = k2;
            this.c = v;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof c)) {
                return false;
            }
            c cVar = (c) obj;
            if (!this.b.equals(cVar.b) || !this.c.equals(cVar.c)) {
                return false;
            }
            return true;
        }

        public K getKey() {
            return this.b;
        }

        public V getValue() {
            return this.c;
        }

        public int hashCode() {
            return this.b.hashCode() ^ this.c.hashCode();
        }

        public V setValue(V v) {
            throw new UnsupportedOperationException("An entry modification is not supported");
        }

        public String toString() {
            return ((Object) this.b) + "=" + ((Object) this.c);
        }
    }

    public class d implements Iterator<Map.Entry<K, V>>, f<K, V> {
        public c<K, V> b;
        public boolean c = true;

        public d() {
        }

        public void a(c<K, V> cVar) {
            c<K, V> cVar2 = this.b;
            if (cVar == cVar2) {
                c<K, V> cVar3 = cVar2.f1051e;
                this.b = cVar3;
                this.c = cVar3 == null;
            }
        }

        public boolean hasNext() {
            if (!this.c) {
                c<K, V> cVar = this.b;
                if (cVar == null || cVar.d == null) {
                    return false;
                }
                return true;
            } else if (SafeIterableMap.this.b != null) {
                return true;
            } else {
                return false;
            }
        }

        public Object next() {
            if (this.c) {
                this.c = false;
                this.b = SafeIterableMap.this.b;
            } else {
                c<K, V> cVar = this.b;
                this.b = cVar != null ? cVar.d : null;
            }
            return this.b;
        }
    }

    public static abstract class e<K, V> implements Iterator<Map.Entry<K, V>>, f<K, V> {
        public c<K, V> b;
        public c<K, V> c;

        public e(c<K, V> cVar, c<K, V> cVar2) {
            this.b = cVar2;
            this.c = cVar;
        }

        public void a(c<K, V> cVar) {
            c<K, V> cVar2 = null;
            if (this.b == cVar && cVar == this.c) {
                this.c = null;
                this.b = null;
            }
            c<K, V> cVar3 = this.b;
            if (cVar3 == cVar) {
                this.b = b(cVar3);
            }
            c<K, V> cVar4 = this.c;
            if (cVar4 == cVar) {
                c<K, V> cVar5 = this.b;
                if (!(cVar4 == cVar5 || cVar5 == null)) {
                    cVar2 = c(cVar4);
                }
                this.c = cVar2;
            }
        }

        public abstract c<K, V> b(c<K, V> cVar);

        public abstract c<K, V> c(c<K, V> cVar);

        public boolean hasNext() {
            return this.c != null;
        }

        public Object next() {
            c<K, V> cVar = this.c;
            c<K, V> cVar2 = this.b;
            this.c = (cVar == cVar2 || cVar2 == null) ? null : c(cVar);
            return cVar;
        }
    }

    public interface f<K, V> {
        void a(c<K, V> cVar);
    }

    public c<K, V> a(K k2) {
        c<K, V> cVar = this.b;
        while (cVar != null && !cVar.b.equals(k2)) {
            cVar = cVar.d;
        }
        return cVar;
    }

    public V b(K k2, V v) {
        c a2 = a(k2);
        if (a2 != null) {
            return a2.c;
        }
        a(k2, v);
        return null;
    }

    public SafeIterableMap<K, V>.defpackage.d c() {
        SafeIterableMap<K, V>.defpackage.d dVar = new d();
        this.d.put(dVar, false);
        return dVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0048, code lost:
        if (r3.hasNext() != false) goto L_0x0053;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0050, code lost:
        if (((i.c.a.b.SafeIterableMap.e) r7).hasNext() != false) goto L_0x0053;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0053, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r7) {
        /*
            r6 = this;
            r0 = 1
            if (r7 != r6) goto L_0x0004
            return r0
        L_0x0004:
            boolean r1 = r7 instanceof i.c.a.b.SafeIterableMap
            r2 = 0
            if (r1 != 0) goto L_0x000a
            return r2
        L_0x000a:
            i.c.a.b.SafeIterableMap r7 = (i.c.a.b.SafeIterableMap) r7
            int r1 = r6.f1050e
            int r3 = r7.f1050e
            if (r1 == r3) goto L_0x0013
            return r2
        L_0x0013:
            java.util.Iterator r1 = r6.iterator()
            java.util.Iterator r7 = r7.iterator()
        L_0x001b:
            r3 = r1
            i.c.a.b.SafeIterableMap$e r3 = (i.c.a.b.SafeIterableMap.e) r3
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x0044
            r4 = r7
            i.c.a.b.SafeIterableMap$e r4 = (i.c.a.b.SafeIterableMap.e) r4
            boolean r5 = r4.hasNext()
            if (r5 == 0) goto L_0x0044
            java.lang.Object r3 = r3.next()
            java.util.Map$Entry r3 = (java.util.Map.Entry) r3
            java.lang.Object r4 = r4.next()
            if (r3 != 0) goto L_0x003b
            if (r4 != 0) goto L_0x0043
        L_0x003b:
            if (r3 == 0) goto L_0x001b
            boolean r3 = r3.equals(r4)
            if (r3 != 0) goto L_0x001b
        L_0x0043:
            return r2
        L_0x0044:
            boolean r1 = r3.hasNext()
            if (r1 != 0) goto L_0x0053
            i.c.a.b.SafeIterableMap$e r7 = (i.c.a.b.SafeIterableMap.e) r7
            boolean r7 = r7.hasNext()
            if (r7 != 0) goto L_0x0053
            goto L_0x0054
        L_0x0053:
            r0 = 0
        L_0x0054:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: i.c.a.b.SafeIterableMap.equals(java.lang.Object):boolean");
    }

    public int hashCode() {
        Iterator it = iterator();
        int i2 = 0;
        while (true) {
            e eVar = (e) it;
            if (!eVar.hasNext()) {
                return i2;
            }
            i2 += ((Map.Entry) eVar.next()).hashCode();
        }
    }

    public Iterator<Map.Entry<K, V>> iterator() {
        a aVar = new a(this.b, this.c);
        this.d.put(aVar, false);
        return aVar;
    }

    public V remove(K k2) {
        c a2 = a(k2);
        if (a2 == null) {
            return null;
        }
        this.f1050e--;
        if (!this.d.isEmpty()) {
            for (f<K, V> a3 : this.d.keySet()) {
                a3.a(a2);
            }
        }
        c<K, V> cVar = a2.f1051e;
        if (cVar != null) {
            cVar.d = a2.d;
        } else {
            this.b = a2.d;
        }
        c<K, V> cVar2 = a2.d;
        if (cVar2 != null) {
            cVar2.f1051e = a2.f1051e;
        } else {
            this.c = a2.f1051e;
        }
        a2.d = null;
        a2.f1051e = null;
        return a2.c;
    }

    public String toString() {
        StringBuilder a2 = outline.a("[");
        Iterator it = iterator();
        while (true) {
            e eVar = (e) it;
            if (eVar.hasNext()) {
                a2.append(((Map.Entry) eVar.next()).toString());
                if (eVar.hasNext()) {
                    a2.append(", ");
                }
            } else {
                a2.append("]");
                return a2.toString();
            }
        }
    }

    public c<K, V> a(K k2, V v) {
        c<K, V> cVar = new c<>(k2, v);
        this.f1050e++;
        c<K, V> cVar2 = this.c;
        if (cVar2 == null) {
            this.b = cVar;
            this.c = cVar;
            return cVar;
        }
        cVar2.d = cVar;
        cVar.f1051e = cVar2;
        this.c = cVar;
        return cVar;
    }
}
