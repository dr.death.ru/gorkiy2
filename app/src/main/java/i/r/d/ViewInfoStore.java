package i.r.d;

import androidx.recyclerview.widget.RecyclerView;
import i.e.LongSparseArray;
import i.e.SimpleArrayMap;
import i.h.k.Pools;
import i.h.k.Pools0;
import i.r.d.z;

public class ViewInfoStore {
    public final SimpleArrayMap<RecyclerView.d0, z.a> a = new SimpleArrayMap<>();
    public final LongSparseArray<RecyclerView.d0> b = new LongSparseArray<>();

    public interface b {
    }

    public final RecyclerView.l.c a(RecyclerView.d0 d0Var, int i2) {
        a e2;
        RecyclerView.l.c cVar;
        int a2 = this.a.a(d0Var);
        if (a2 >= 0 && (e2 = this.a.e(a2)) != null) {
            int i3 = e2.a;
            if ((i3 & i2) != 0) {
                e2.a = (~i2) & i3;
                if (i2 == 4) {
                    cVar = e2.b;
                } else if (i2 == 8) {
                    cVar = e2.c;
                } else {
                    throw new IllegalArgumentException("Must provide flag PRE or POST");
                }
                if ((e2.a & 12) == 0) {
                    this.a.d(a2);
                    a.a(e2);
                }
                return cVar;
            }
        }
        return null;
    }

    public void b(RecyclerView.d0 d0Var, RecyclerView.l.c cVar) {
        a orDefault = this.a.getOrDefault(d0Var, null);
        if (orDefault == null) {
            orDefault = a.a();
            this.a.put(d0Var, orDefault);
        }
        orDefault.b = cVar;
        orDefault.a |= 4;
    }

    public void c(RecyclerView.d0 d0Var) {
        a orDefault = this.a.getOrDefault(d0Var, null);
        if (orDefault != null) {
            orDefault.a &= -2;
        }
    }

    public void d(RecyclerView.d0 d0Var) {
        int d = this.b.d() - 1;
        while (true) {
            if (d < 0) {
                break;
            } else if (d0Var == this.b.a(d)) {
                LongSparseArray<RecyclerView.d0> longSparseArray = this.b;
                Object[] objArr = longSparseArray.d;
                Object obj = objArr[d];
                Object obj2 = LongSparseArray.f1063f;
                if (obj != obj2) {
                    objArr[d] = obj2;
                    longSparseArray.b = true;
                }
            } else {
                d--;
            }
        }
        a remove = this.a.remove(d0Var);
        if (remove != null) {
            a.a(remove);
        }
    }

    public static class a {
        public static Pools<z.a> d = new Pools0(20);
        public int a;
        public RecyclerView.l.c b;
        public RecyclerView.l.c c;

        public static a a() {
            a a2 = d.a();
            return a2 == null ? new a() : a2;
        }

        public static void a(a aVar) {
            aVar.a = 0;
            aVar.b = null;
            aVar.c = null;
            d.a(aVar);
        }
    }

    public boolean b(RecyclerView.d0 d0Var) {
        a orDefault = this.a.getOrDefault(d0Var, null);
        if (orDefault == null || (orDefault.a & 1) == 0) {
            return false;
        }
        return true;
    }

    public void a(RecyclerView.d0 d0Var, RecyclerView.l.c cVar) {
        a orDefault = this.a.getOrDefault(d0Var, null);
        if (orDefault == null) {
            orDefault = a.a();
            this.a.put(d0Var, orDefault);
        }
        orDefault.c = cVar;
        orDefault.a |= 8;
    }

    public void a(RecyclerView.d0 d0Var) {
        a orDefault = this.a.getOrDefault(d0Var, null);
        if (orDefault == null) {
            orDefault = a.a();
            this.a.put(d0Var, orDefault);
        }
        orDefault.a |= 1;
    }
}
