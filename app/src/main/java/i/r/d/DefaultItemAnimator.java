package i.r.d;

import android.view.View;
import android.view.ViewPropertyAnimator;
import androidx.recyclerview.widget.RecyclerView;
import i.r.d.DefaultItemAnimator7;
import java.util.ArrayList;
import java.util.Iterator;

public class DefaultItemAnimator implements Runnable {
    public final /* synthetic */ ArrayList b;
    public final /* synthetic */ DefaultItemAnimator7 c;

    public DefaultItemAnimator(DefaultItemAnimator7 defaultItemAnimator7, ArrayList arrayList) {
        this.c = defaultItemAnimator7;
        this.b = arrayList;
    }

    public void run() {
        Iterator it = this.b.iterator();
        while (it.hasNext()) {
            DefaultItemAnimator7.b bVar = (DefaultItemAnimator7.b) it.next();
            DefaultItemAnimator7 defaultItemAnimator7 = this.c;
            RecyclerView.d0 d0Var = bVar.a;
            int i2 = bVar.b;
            int i3 = bVar.c;
            int i4 = bVar.d;
            int i5 = bVar.f1381e;
            if (defaultItemAnimator7 != null) {
                View view = d0Var.a;
                int i6 = i4 - i2;
                int i7 = i5 - i3;
                if (i6 != 0) {
                    view.animate().translationX(0.0f);
                }
                if (i7 != 0) {
                    view.animate().translationY(0.0f);
                }
                ViewPropertyAnimator animate = view.animate();
                defaultItemAnimator7.f1376p.add(d0Var);
                animate.setDuration(defaultItemAnimator7.f314e).setListener(new DefaultItemAnimator4(defaultItemAnimator7, d0Var, i6, view, i7, animate)).start();
            } else {
                throw null;
            }
        }
        this.b.clear();
        this.c.f1373m.remove(this.b);
    }
}
