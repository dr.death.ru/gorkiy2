package i.d.e;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

public class RoundRectDrawable extends Drawable {
    public float a;
    public final Paint b;
    public final RectF c;
    public final Rect d;

    /* renamed from: e  reason: collision with root package name */
    public float f1052e;

    /* renamed from: f  reason: collision with root package name */
    public boolean f1053f = false;
    public boolean g = true;
    public ColorStateList h;

    /* renamed from: i  reason: collision with root package name */
    public PorterDuffColorFilter f1054i;

    /* renamed from: j  reason: collision with root package name */
    public ColorStateList f1055j;

    /* renamed from: k  reason: collision with root package name */
    public PorterDuff.Mode f1056k = PorterDuff.Mode.SRC_IN;

    public RoundRectDrawable(ColorStateList colorStateList, float f2) {
        this.a = f2;
        this.b = new Paint(5);
        a(colorStateList);
        this.c = new RectF();
        this.d = new Rect();
    }

    public final void a(ColorStateList colorStateList) {
        if (colorStateList == null) {
            colorStateList = ColorStateList.valueOf(0);
        }
        this.h = colorStateList;
        this.b.setColor(colorStateList.getColorForState(getState(), this.h.getDefaultColor()));
    }

    public void draw(Canvas canvas) {
        boolean z;
        Paint paint = this.b;
        if (this.f1054i == null || paint.getColorFilter() != null) {
            z = false;
        } else {
            paint.setColorFilter(this.f1054i);
            z = true;
        }
        RectF rectF = this.c;
        float f2 = this.a;
        canvas.drawRoundRect(rectF, f2, f2, paint);
        if (z) {
            paint.setColorFilter(null);
        }
    }

    public int getOpacity() {
        return -3;
    }

    public void getOutline(Outline outline) {
        outline.setRoundRect(this.d, this.a);
    }

    public boolean isStateful() {
        ColorStateList colorStateList;
        ColorStateList colorStateList2 = this.f1055j;
        return (colorStateList2 != null && colorStateList2.isStateful()) || ((colorStateList = this.h) != null && colorStateList.isStateful()) || super.isStateful();
    }

    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        a(rect);
    }

    public boolean onStateChange(int[] iArr) {
        PorterDuff.Mode mode;
        ColorStateList colorStateList = this.h;
        int colorForState = colorStateList.getColorForState(iArr, colorStateList.getDefaultColor());
        boolean z = colorForState != this.b.getColor();
        if (z) {
            this.b.setColor(colorForState);
        }
        ColorStateList colorStateList2 = this.f1055j;
        if (colorStateList2 == null || (mode = this.f1056k) == null) {
            return z;
        }
        this.f1054i = a(colorStateList2, mode);
        return true;
    }

    public void setAlpha(int i2) {
        this.b.setAlpha(i2);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.b.setColorFilter(colorFilter);
    }

    public void setTintList(ColorStateList colorStateList) {
        this.f1055j = colorStateList;
        this.f1054i = a(colorStateList, this.f1056k);
        invalidateSelf();
    }

    public void setTintMode(PorterDuff.Mode mode) {
        this.f1056k = mode;
        this.f1054i = a(this.f1055j, mode);
        invalidateSelf();
    }

    public final void a(Rect rect) {
        if (rect == null) {
            rect = getBounds();
        }
        this.c.set((float) rect.left, (float) rect.top, (float) rect.right, (float) rect.bottom);
        this.d.set(rect);
        if (this.f1053f) {
            float b2 = RoundRectDrawableWithShadow.b(this.f1052e, this.a, this.g);
            this.d.inset((int) Math.ceil((double) RoundRectDrawableWithShadow.a(this.f1052e, this.a, this.g)), (int) Math.ceil((double) b2));
            this.c.set(this.d);
        }
    }

    public final PorterDuffColorFilter a(ColorStateList colorStateList, PorterDuff.Mode mode) {
        if (colorStateList == null || mode == null) {
            return null;
        }
        return new PorterDuffColorFilter(colorStateList.getColorForState(getState(), 0), mode);
    }
}
