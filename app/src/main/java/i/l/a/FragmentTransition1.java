package i.l.a;

import android.graphics.Rect;
import android.view.View;
import androidx.fragment.app.Fragment;
import i.e.ArrayMap;

/* compiled from: FragmentTransition */
public final class FragmentTransition1 implements Runnable {
    public final /* synthetic */ Fragment b;
    public final /* synthetic */ Fragment c;
    public final /* synthetic */ boolean d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ ArrayMap f1341e;

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ View f1342f;
    public final /* synthetic */ FragmentTransitionImpl0 g;
    public final /* synthetic */ Rect h;

    public FragmentTransition1(Fragment fragment, Fragment fragment2, boolean z, ArrayMap arrayMap, View view, FragmentTransitionImpl0 fragmentTransitionImpl0, Rect rect) {
        this.b = fragment;
        this.c = fragment2;
        this.d = z;
        this.f1341e = arrayMap;
        this.f1342f = view;
        this.g = fragmentTransitionImpl0;
        this.h = rect;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentTransition3.a(androidx.fragment.app.Fragment, androidx.fragment.app.Fragment, boolean, i.e.ArrayMap<java.lang.String, android.view.View>, boolean):void
     arg types: [androidx.fragment.app.Fragment, androidx.fragment.app.Fragment, boolean, i.e.ArrayMap, int]
     candidates:
      i.l.a.FragmentTransition3.a(i.l.a.b0, java.lang.Object, androidx.fragment.app.Fragment, java.util.ArrayList<android.view.View>, android.view.View):java.util.ArrayList<android.view.View>
      i.l.a.FragmentTransition3.a(i.l.a.a, i.l.a.r$a, android.util.SparseArray<i.l.a.w$a>, boolean, boolean):void
      i.l.a.FragmentTransition3.a(androidx.fragment.app.Fragment, androidx.fragment.app.Fragment, boolean, i.e.ArrayMap<java.lang.String, android.view.View>, boolean):void */
    public void run() {
        FragmentTransition3.a(this.b, this.c, this.d, (ArrayMap<String, View>) this.f1341e, false);
        View view = this.f1342f;
        if (view != null) {
            this.g.a(view, this.h);
        }
    }
}
