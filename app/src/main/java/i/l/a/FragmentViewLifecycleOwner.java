package i.l.a;

import i.o.Lifecycle;
import i.o.LifecycleOwner;
import i.o.LifecycleRegistry;

public class FragmentViewLifecycleOwner implements LifecycleOwner {
    public LifecycleRegistry b = null;

    public Lifecycle a() {
        if (this.b == null) {
            this.b = new LifecycleRegistry(this);
        }
        return this.b;
    }
}
