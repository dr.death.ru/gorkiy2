package i.l.a;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

@SuppressLint({"BanParcelableUsage"})
public final class FragmentManagerState implements Parcelable {
    public static final Parcelable.Creator<o> CREATOR = new a();
    public ArrayList<q> b;
    public ArrayList<String> c;
    public BackStackState[] d;

    /* renamed from: e  reason: collision with root package name */
    public String f1311e = null;

    /* renamed from: f  reason: collision with root package name */
    public int f1312f;

    public static class a implements Parcelable.Creator<o> {
        public Object createFromParcel(Parcel parcel) {
            return new FragmentManagerState(parcel);
        }

        public Object[] newArray(int i2) {
            return new FragmentManagerState[i2];
        }
    }

    public FragmentManagerState() {
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeTypedList(this.b);
        parcel.writeStringList(this.c);
        parcel.writeTypedArray(this.d, i2);
        parcel.writeString(this.f1311e);
        parcel.writeInt(this.f1312f);
    }

    public FragmentManagerState(Parcel parcel) {
        this.b = parcel.createTypedArrayList(FragmentState.CREATOR);
        this.c = parcel.createStringArrayList();
        this.d = (BackStackState[]) parcel.createTypedArray(BackStackState.CREATOR);
        this.f1311e = parcel.readString();
        this.f1312f = parcel.readInt();
    }
}
