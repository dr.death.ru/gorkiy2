package i.f.a.h;

import i.f.a.SolverVariable;

public class ConstraintAnchor {
    public ResolutionAnchor a = new ResolutionAnchor(this);
    public final ConstraintWidget b;
    public final c c;
    public ConstraintAnchor d;

    /* renamed from: e  reason: collision with root package name */
    public int f1106e = 0;

    /* renamed from: f  reason: collision with root package name */
    public int f1107f = -1;
    public b g = b.NONE;
    public int h;

    /* renamed from: i  reason: collision with root package name */
    public SolverVariable f1108i;

    public enum a {
        RELAXED,
        STRICT
    }

    public enum b {
        NONE,
        STRONG,
        WEAK
    }

    public enum c {
        NONE,
        LEFT,
        TOP,
        RIGHT,
        BOTTOM,
        BASELINE,
        CENTER,
        CENTER_X,
        CENTER_Y
    }

    public ConstraintAnchor(ConstraintWidget constraintWidget, c cVar) {
        a aVar = a.RELAXED;
        this.h = 0;
        this.b = constraintWidget;
        this.c = cVar;
    }

    public int a() {
        ConstraintAnchor constraintAnchor;
        if (this.b.Y == 8) {
            return 0;
        }
        int i2 = this.f1107f;
        if (i2 <= -1 || (constraintAnchor = this.d) == null || constraintAnchor.b.Y != 8) {
            return this.f1106e;
        }
        return i2;
    }

    public boolean b() {
        return this.d != null;
    }

    public void c() {
        this.d = null;
        this.f1106e = 0;
        this.f1107f = -1;
        this.g = b.STRONG;
        this.h = 0;
        a aVar = a.RELAXED;
        this.a.e();
    }

    public void d() {
        SolverVariable solverVariable = this.f1108i;
        if (solverVariable == null) {
            this.f1108i = new SolverVariable(SolverVariable.a.UNRESTRICTED);
        } else {
            solverVariable.a();
        }
    }

    public String toString() {
        return this.b.Z + ":" + this.c.toString();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0034, code lost:
        if ((r4.b.Q > 0) == false) goto L_0x0058;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0054, code lost:
        if (r10 != i.f.a.h.ConstraintAnchor.c.CENTER_Y) goto L_0x0056;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0070, code lost:
        if (r10 != i.f.a.h.ConstraintAnchor.c.CENTER_Y) goto L_0x0058;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x008b, code lost:
        if (r10 != i.f.a.h.ConstraintAnchor.c.CENTER_X) goto L_0x0058;
     */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0090 A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(i.f.a.h.ConstraintAnchor r5, int r6, int r7, i.f.a.h.ConstraintAnchor.b r8, int r9, boolean r10) {
        /*
            r4 = this;
            r0 = 0
            r1 = 1
            if (r5 != 0) goto L_0x0014
            r5 = 0
            r4.d = r5
            r4.f1106e = r0
            r5 = -1
            r4.f1107f = r5
            i.f.a.h.ConstraintAnchor$b r5 = i.f.a.h.ConstraintAnchor.b.NONE
            r4.g = r5
            r5 = 2
            r4.h = r5
            return r1
        L_0x0014:
            if (r10 != 0) goto L_0x0091
            i.f.a.h.ConstraintAnchor$c r10 = r5.c
            i.f.a.h.ConstraintAnchor$c r2 = r4.c
            if (r10 != r2) goto L_0x0037
            i.f.a.h.ConstraintAnchor$c r10 = i.f.a.h.ConstraintAnchor.c.BASELINE
            if (r2 != r10) goto L_0x0056
            i.f.a.h.ConstraintWidget r10 = r5.b
            int r10 = r10.Q
            if (r10 <= 0) goto L_0x0028
            r10 = 1
            goto L_0x0029
        L_0x0028:
            r10 = 0
        L_0x0029:
            if (r10 == 0) goto L_0x0058
            i.f.a.h.ConstraintWidget r10 = r4.b
            int r10 = r10.Q
            if (r10 <= 0) goto L_0x0033
            r10 = 1
            goto L_0x0034
        L_0x0033:
            r10 = 0
        L_0x0034:
            if (r10 != 0) goto L_0x0056
            goto L_0x0058
        L_0x0037:
            int r2 = r2.ordinal()
            switch(r2) {
                case 0: goto L_0x0058;
                case 1: goto L_0x0075;
                case 2: goto L_0x005a;
                case 3: goto L_0x0075;
                case 4: goto L_0x005a;
                case 5: goto L_0x0058;
                case 6: goto L_0x004a;
                case 7: goto L_0x0058;
                case 8: goto L_0x0058;
                default: goto L_0x003e;
            }
        L_0x003e:
            java.lang.AssertionError r5 = new java.lang.AssertionError
            i.f.a.h.ConstraintAnchor$c r6 = r4.c
            java.lang.String r6 = r6.name()
            r5.<init>(r6)
            throw r5
        L_0x004a:
            i.f.a.h.ConstraintAnchor$c r2 = i.f.a.h.ConstraintAnchor.c.BASELINE
            if (r10 == r2) goto L_0x0058
            i.f.a.h.ConstraintAnchor$c r2 = i.f.a.h.ConstraintAnchor.c.CENTER_X
            if (r10 == r2) goto L_0x0058
            i.f.a.h.ConstraintAnchor$c r2 = i.f.a.h.ConstraintAnchor.c.CENTER_Y
            if (r10 == r2) goto L_0x0058
        L_0x0056:
            r10 = 1
            goto L_0x008e
        L_0x0058:
            r10 = 0
            goto L_0x008e
        L_0x005a:
            i.f.a.h.ConstraintAnchor$c r2 = i.f.a.h.ConstraintAnchor.c.TOP
            if (r10 == r2) goto L_0x0065
            i.f.a.h.ConstraintAnchor$c r2 = i.f.a.h.ConstraintAnchor.c.BOTTOM
            if (r10 != r2) goto L_0x0063
            goto L_0x0065
        L_0x0063:
            r2 = 0
            goto L_0x0066
        L_0x0065:
            r2 = 1
        L_0x0066:
            i.f.a.h.ConstraintWidget r3 = r5.b
            boolean r3 = r3 instanceof i.f.a.h.Guideline
            if (r3 == 0) goto L_0x0073
            if (r2 != 0) goto L_0x0056
            i.f.a.h.ConstraintAnchor$c r2 = i.f.a.h.ConstraintAnchor.c.CENTER_Y
            if (r10 != r2) goto L_0x0058
            goto L_0x0056
        L_0x0073:
            r10 = r2
            goto L_0x008e
        L_0x0075:
            i.f.a.h.ConstraintAnchor$c r2 = i.f.a.h.ConstraintAnchor.c.LEFT
            if (r10 == r2) goto L_0x0080
            i.f.a.h.ConstraintAnchor$c r2 = i.f.a.h.ConstraintAnchor.c.RIGHT
            if (r10 != r2) goto L_0x007e
            goto L_0x0080
        L_0x007e:
            r2 = 0
            goto L_0x0081
        L_0x0080:
            r2 = 1
        L_0x0081:
            i.f.a.h.ConstraintWidget r3 = r5.b
            boolean r3 = r3 instanceof i.f.a.h.Guideline
            if (r3 == 0) goto L_0x0073
            if (r2 != 0) goto L_0x0056
            i.f.a.h.ConstraintAnchor$c r2 = i.f.a.h.ConstraintAnchor.c.CENTER_X
            if (r10 != r2) goto L_0x0058
            goto L_0x0056
        L_0x008e:
            if (r10 != 0) goto L_0x0091
            return r0
        L_0x0091:
            r4.d = r5
            if (r6 <= 0) goto L_0x0098
            r4.f1106e = r6
            goto L_0x009a
        L_0x0098:
            r4.f1106e = r0
        L_0x009a:
            r4.f1107f = r7
            r4.g = r8
            r4.h = r9
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: i.f.a.h.ConstraintAnchor.a(i.f.a.h.ConstraintAnchor, int, int, i.f.a.h.ConstraintAnchor$b, int, boolean):boolean");
    }
}
