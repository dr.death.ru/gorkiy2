package com.crashlytics.android.answers;

import android.content.Context;
import android.os.Build;
import java.util.Map;
import java.util.UUID;
import l.a.a.a.o.b.AdvertisingInfo;
import l.a.a.a.o.b.CommonUtils;
import l.a.a.a.o.b.IdManager;
import l.a.a.a.o.b.s;

public class SessionMetadataCollector {
    public final Context context;
    public final IdManager idManager;
    public final String versionCode;
    public final String versionName;

    public SessionMetadataCollector(Context context2, IdManager idManager2, String str, String str2) {
        this.context = context2;
        this.idManager = idManager2;
        this.versionCode = str;
        this.versionName = str2;
    }

    public SessionEventMetadata getMetadata() {
        AdvertisingInfo a;
        Map<s.a, String> c = this.idManager.c();
        IdManager idManager2 = this.idManager;
        String str = idManager2.f2635f;
        String b = idManager2.b();
        IdManager idManager3 = this.idManager;
        Boolean valueOf = (!(idManager3.c && !idManager3.f2639l.a(idManager3.f2634e)) || (a = idManager3.a()) == null) ? null : Boolean.valueOf(a.b);
        String str2 = c.get(IdManager.a.FONT_TOKEN);
        String j2 = CommonUtils.j(this.context);
        IdManager idManager4 = this.idManager;
        if (idManager4 != null) {
            return new SessionEventMetadata(str, UUID.randomUUID().toString(), b, valueOf, str2, j2, idManager4.a(Build.VERSION.RELEASE) + "/" + idManager4.a(Build.VERSION.INCREMENTAL), this.idManager.e(), this.versionCode, this.versionName);
        }
        throw null;
    }
}
