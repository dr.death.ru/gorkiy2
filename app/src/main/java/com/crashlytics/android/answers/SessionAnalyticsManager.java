package com.crashlytics.android.answers;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Looper;
import android.util.Log;
import com.crashlytics.android.answers.BackgroundManager;
import com.crashlytics.android.answers.SessionEvent;
import j.a.a.a.outline;
import j.c.a.a.c.n.c;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicLong;
import l.a.a.a.ActivityLifecycleManager0;
import l.a.a.a.DefaultLogger;
import l.a.a.a.Fabric;
import l.a.a.a.Kit;
import l.a.a.a.o.b.ExecutorUtils;
import l.a.a.a.o.b.IdManager;
import l.a.a.a.o.e.DefaultHttpRequestFactory;
import l.a.a.a.o.f.FileStoreImpl;
import l.a.a.a.o.g.AnalyticsSettingsData;

public class SessionAnalyticsManager implements BackgroundManager.Listener {
    public static final String EXECUTOR_SERVICE = "Answers Events Handler";
    public static final String ON_CRASH_ERROR_MSG = "onCrash called from main thread!!!";
    public final BackgroundManager backgroundManager;
    public final AnswersEventsHandler eventsHandler;
    public final long installedAt;
    public final ActivityLifecycleManager0 lifecycleManager;
    public final AnswersPreferenceManager preferenceManager;

    public SessionAnalyticsManager(AnswersEventsHandler answersEventsHandler, ActivityLifecycleManager0 activityLifecycleManager0, BackgroundManager backgroundManager2, AnswersPreferenceManager answersPreferenceManager, long j2) {
        this.eventsHandler = answersEventsHandler;
        this.lifecycleManager = activityLifecycleManager0;
        this.backgroundManager = backgroundManager2;
        this.preferenceManager = answersPreferenceManager;
        this.installedAt = j2;
    }

    public static SessionAnalyticsManager build(Kit kit, Context context, IdManager idManager, String str, String str2, long j2) {
        SessionMetadataCollector sessionMetadataCollector = new SessionMetadataCollector(context, idManager, str, str2);
        AnswersFilesManagerProvider answersFilesManagerProvider = new AnswersFilesManagerProvider(context, new FileStoreImpl(kit));
        DefaultHttpRequestFactory defaultHttpRequestFactory = new DefaultHttpRequestFactory(Fabric.a());
        ActivityLifecycleManager0 activityLifecycleManager0 = new ActivityLifecycleManager0(context);
        ScheduledExecutorService newSingleThreadScheduledExecutor = Executors.newSingleThreadScheduledExecutor(new ExecutorUtils(EXECUTOR_SERVICE, new AtomicLong(1)));
        c.a(EXECUTOR_SERVICE, newSingleThreadScheduledExecutor);
        BackgroundManager backgroundManager2 = new BackgroundManager(newSingleThreadScheduledExecutor);
        return new SessionAnalyticsManager(new AnswersEventsHandler(kit, context, answersFilesManagerProvider, sessionMetadataCollector, defaultHttpRequestFactory, newSingleThreadScheduledExecutor, new FirebaseAnalyticsApiAdapter(context)), activityLifecycleManager0, backgroundManager2, AnswersPreferenceManager.build(context), j2);
    }

    public void disable() {
        ActivityLifecycleManager0.a aVar = this.lifecycleManager.b;
        if (aVar != null) {
            for (Application.ActivityLifecycleCallbacks unregisterActivityLifecycleCallbacks : aVar.a) {
                aVar.b.unregisterActivityLifecycleCallbacks(unregisterActivityLifecycleCallbacks);
            }
        }
        this.eventsHandler.disable();
    }

    public void enable() {
        this.eventsHandler.enable();
        this.lifecycleManager.a(new AnswersLifecycleCallbacks(this, this.backgroundManager));
        this.backgroundManager.registerListener(this);
        if (isFirstLaunch()) {
            onInstall(this.installedAt);
            this.preferenceManager.setAnalyticsLaunched();
        }
    }

    public boolean isFirstLaunch() {
        return !this.preferenceManager.hasAnalyticsLaunched();
    }

    public void onBackground() {
        if (Fabric.a().a(Answers.TAG, 3)) {
            Log.d(Answers.TAG, "Flush events when app is backgrounded", null);
        }
        this.eventsHandler.flushEvents();
    }

    public void onCrash(String str, String str2) {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            if (Fabric.a().a(Answers.TAG, 3)) {
                Log.d(Answers.TAG, "Logged crash", null);
            }
            this.eventsHandler.processEventSync(SessionEvent.crashEventBuilder(str, str2));
            return;
        }
        throw new IllegalStateException(ON_CRASH_ERROR_MSG);
    }

    public void onCustom(CustomEvent customEvent) {
        String str = "Logged custom event: " + customEvent;
        if (Fabric.a().a(Answers.TAG, 3)) {
            Log.d(Answers.TAG, str, null);
        }
        this.eventsHandler.processEventAsync(SessionEvent.customEventBuilder(customEvent));
    }

    public void onError(String str) {
    }

    public void onInstall(long j2) {
        if (Fabric.a().a(Answers.TAG, 3)) {
            Log.d(Answers.TAG, "Logged install", null);
        }
        this.eventsHandler.processEventAsyncAndFlush(SessionEvent.installEventBuilder(j2));
    }

    public void onLifecycle(Activity activity, SessionEvent.Type type) {
        DefaultLogger a = Fabric.a();
        StringBuilder a2 = outline.a("Logged lifecycle event: ");
        a2.append(type.name());
        String sb = a2.toString();
        if (a.a(Answers.TAG, 3)) {
            Log.d(Answers.TAG, sb, null);
        }
        this.eventsHandler.processEventAsync(SessionEvent.lifecycleEventBuilder(type, activity));
    }

    public void onPredefined(PredefinedEvent predefinedEvent) {
        String str = "Logged predefined event: " + predefinedEvent;
        if (Fabric.a().a(Answers.TAG, 3)) {
            Log.d(Answers.TAG, str, null);
        }
        this.eventsHandler.processEventAsync(SessionEvent.predefinedEventBuilder(predefinedEvent));
    }

    public void setAnalyticsSettingsData(AnalyticsSettingsData analyticsSettingsData, String str) {
        this.backgroundManager.setFlushOnBackground(analyticsSettingsData.f2658i);
        this.eventsHandler.setAnalyticsSettingsData(analyticsSettingsData, str);
    }
}
