package com.crashlytics.android.answers;

import com.crashlytics.android.answers.PredefinedEvent;
import j.a.a.a.outline;
import java.util.Map;

public abstract class PredefinedEvent<T extends PredefinedEvent> extends AnswersEvent<T> {
    public final AnswersAttributes predefinedAttributes = new AnswersAttributes(super.validator);

    public Map<String, Object> getPredefinedAttributes() {
        return this.predefinedAttributes.attributes;
    }

    public abstract String getPredefinedType();

    public String toString() {
        StringBuilder a = outline.a("{type:\"");
        a.append(getPredefinedType());
        a.append('\"');
        a.append(", predefinedAttributes:");
        a.append(this.predefinedAttributes);
        a.append(", customAttributes:");
        a.append(super.customAttributes);
        a.append("}");
        return a.toString();
    }
}
