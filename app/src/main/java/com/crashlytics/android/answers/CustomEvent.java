package com.crashlytics.android.answers;

import j.a.a.a.outline;

public class CustomEvent extends AnswersEvent<CustomEvent> {
    public final String eventName;

    public CustomEvent(String str) {
        if (str != null) {
            this.eventName = super.validator.limitStringLength(str);
            return;
        }
        throw new NullPointerException("eventName must not be null");
    }

    public String getCustomType() {
        return this.eventName;
    }

    public String toString() {
        StringBuilder a = outline.a("{eventName:\"");
        a.append(this.eventName);
        a.append('\"');
        a.append(", customAttributes:");
        a.append(super.customAttributes);
        a.append("}");
        return a.toString();
    }
}
