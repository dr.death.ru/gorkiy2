package com.crashlytics.android.core;

import android.util.Log;
import j.a.a.a.outline;
import java.io.File;
import java.io.IOException;
import l.a.a.a.DefaultLogger;
import l.a.a.a.Fabric;
import l.a.a.a.o.f.FileStore;
import l.a.a.a.o.f.FileStoreImpl;

public class CrashlyticsFileMarker {
    public final FileStore fileStore;
    public final String markerName;

    public CrashlyticsFileMarker(String str, FileStore fileStore2) {
        this.markerName = str;
        this.fileStore = fileStore2;
    }

    private File getMarkerFile() {
        return new File(((FileStoreImpl) this.fileStore).a(), this.markerName);
    }

    public boolean create() {
        try {
            return getMarkerFile().createNewFile();
        } catch (IOException e2) {
            DefaultLogger a = Fabric.a();
            StringBuilder a2 = outline.a("Error creating marker: ");
            a2.append(this.markerName);
            String sb = a2.toString();
            if (a.a(CrashlyticsCore.TAG, 6)) {
                Log.e(CrashlyticsCore.TAG, sb, e2);
            }
            return false;
        }
    }

    public boolean isPresent() {
        return getMarkerFile().exists();
    }

    public boolean remove() {
        return getMarkerFile().delete();
    }
}
