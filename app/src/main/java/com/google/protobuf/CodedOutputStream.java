package com.google.protobuf;

import j.a.a.a.outline;
import j.c.e.ByteOutput;
import j.c.e.ByteString;
import j.c.e.Internal;
import j.c.e.MessageLite;
import j.c.e.UnsafeUtil;
import j.c.e.Utf8;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class CodedOutputStream extends ByteOutput {
    public static final Logger a = Logger.getLogger(CodedOutputStream.class.getName());
    public static final boolean b = UnsafeUtil.c;
    public static final long c = UnsafeUtil.d;

    public static class OutOfSpaceException extends IOException {
        public OutOfSpaceException() {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.");
        }

        public OutOfSpaceException(Throwable th) {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.", th);
        }

        public OutOfSpaceException(String str, Throwable th) {
            super(outline.a("CodedOutputStream was writing to a flat byte array and ran out of space.: ", str), th);
        }
    }

    public CodedOutputStream() {
    }

    public static CodedOutputStream a(byte[] bArr) {
        return new b(bArr, 0, bArr.length);
    }

    public static int b(int i2, String str) {
        int i3;
        int c2 = c(i2);
        try {
            i3 = Utf8.a(str);
        } catch (Utf8.c unused) {
            i3 = str.getBytes(Internal.a).length;
        }
        return c2 + b(i3);
    }

    public static int c(int i2, int i3) {
        return c(i2) + (i3 >= 0 ? d(i3) : 10);
    }

    public static int d(int i2) {
        if ((i2 & -128) == 0) {
            return 1;
        }
        if ((i2 & -16384) == 0) {
            return 2;
        }
        if ((-2097152 & i2) == 0) {
            return 3;
        }
        return (i2 & -268435456) == 0 ? 4 : 5;
    }

    public static int d(int i2, int i3) {
        return d(i3) + c(i2);
    }

    public abstract int a();

    public abstract void a(int i2);

    public abstract void a(int i2, ByteString byteString);

    public abstract void a(int i2, MessageLite messageLite);

    public abstract void a(int i2, String str);

    public abstract void b(int i2, int i3);

    public /* synthetic */ CodedOutputStream(a aVar) {
    }

    public static int c(int i2) {
        return d((i2 << 3) | 0);
    }

    public final void a(int i2, int i3) {
        b bVar = (b) this;
        bVar.a((i2 << 3) | 0);
        if (i3 >= 0) {
            bVar.a(i3);
            return;
        }
        long j2 = (long) i3;
        if (!b || bVar.a() < 10) {
            while ((j2 & -128) != 0) {
                byte[] bArr = bVar.d;
                int i4 = bVar.f561f;
                bVar.f561f = i4 + 1;
                bArr[i4] = (byte) ((((int) j2) & 127) | 128);
                j2 >>>= 7;
            }
            try {
                byte[] bArr2 = bVar.d;
                int i5 = bVar.f561f;
                bVar.f561f = i5 + 1;
                bArr2[i5] = (byte) ((int) j2);
            } catch (IndexOutOfBoundsException e2) {
                throw new OutOfSpaceException(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(bVar.f561f), Integer.valueOf(bVar.f560e), 1), e2);
            }
        } else {
            long j3 = c + ((long) bVar.f561f);
            while ((j2 & -128) != 0) {
                UnsafeUtil.a(bVar.d, j3, (byte) ((((int) j2) & 127) | 128));
                bVar.f561f++;
                j2 >>>= 7;
                j3 = 1 + j3;
            }
            UnsafeUtil.a(bVar.d, j3, (byte) ((int) j2));
            bVar.f561f++;
        }
    }

    public static int b(int i2, ByteString byteString) {
        return c(i2) + b(byteString.size());
    }

    public static int b(int i2, MessageLite messageLite) {
        return c(i2) + b(messageLite.c());
    }

    public static int b(int i2) {
        return d(i2) + i2;
    }

    public static class b extends CodedOutputStream {
        public final byte[] d;

        /* renamed from: e  reason: collision with root package name */
        public final int f560e;

        /* renamed from: f  reason: collision with root package name */
        public int f561f;

        public b(byte[] bArr, int i2, int i3) {
            super(null);
            if (bArr != null) {
                int i4 = i2 + i3;
                if ((i2 | i3 | (bArr.length - i4)) >= 0) {
                    this.d = bArr;
                    this.f561f = i2;
                    this.f560e = i4;
                    return;
                }
                throw new IllegalArgumentException(String.format("Array range is invalid. Buffer.length=%d, offset=%d, length=%d", Integer.valueOf(bArr.length), Integer.valueOf(i2), Integer.valueOf(i3)));
            }
            throw new NullPointerException("buffer");
        }

        public final void a(int i2) {
            if (!CodedOutputStream.b || a() < 10) {
                while ((i2 & -128) != 0) {
                    byte[] bArr = this.d;
                    int i3 = this.f561f;
                    this.f561f = i3 + 1;
                    bArr[i3] = (byte) ((i2 & 127) | 128);
                    i2 >>>= 7;
                }
                try {
                    byte[] bArr2 = this.d;
                    int i4 = this.f561f;
                    this.f561f = i4 + 1;
                    bArr2[i4] = (byte) i2;
                } catch (IndexOutOfBoundsException e2) {
                    throw new OutOfSpaceException(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f561f), Integer.valueOf(this.f560e), 1), e2);
                }
            } else {
                long j2 = CodedOutputStream.c + ((long) this.f561f);
                while ((i2 & -128) != 0) {
                    UnsafeUtil.a(this.d, j2, (byte) ((i2 & 127) | 128));
                    this.f561f++;
                    i2 >>>= 7;
                    j2 = 1 + j2;
                }
                UnsafeUtil.a(this.d, j2, (byte) i2);
                this.f561f++;
            }
        }

        public final void b(int i2, int i3) {
            a((i2 << 3) | 0);
            a(i3);
        }

        public final void a(int i2, ByteString byteString) {
            a((i2 << 3) | 2);
            a(byteString.size());
            ByteString.g gVar = (ByteString.g) byteString;
            a(gVar.f2586e, gVar.e(), gVar.size());
        }

        public final void a(int i2, MessageLite messageLite) {
            a((i2 << 3) | 2);
            a(messageLite.c());
            messageLite.a(super);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
         arg types: [java.util.logging.Level, java.lang.String, j.c.e.Utf8$c]
         candidates:
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
        public final void a(int i2, String str) {
            a((i2 << 3) | 2);
            int i3 = this.f561f;
            try {
                int d2 = CodedOutputStream.d(str.length() * 3);
                int d3 = CodedOutputStream.d(str.length());
                if (d3 == d2) {
                    int i4 = i3 + d3;
                    this.f561f = i4;
                    int a = Utf8.a.a(str, this.d, i4, a());
                    this.f561f = i3;
                    a((a - i3) - d3);
                    this.f561f = a;
                    return;
                }
                a(Utf8.a(str));
                this.f561f = Utf8.a.a(str, this.d, this.f561f, a());
            } catch (Utf8.c e2) {
                this.f561f = i3;
                CodedOutputStream.a.log(Level.WARNING, "Converting ill-formed UTF-16. Your Protocol Buffer will not round trip correctly!", (Throwable) e2);
                byte[] bytes = str.getBytes(Internal.a);
                try {
                    a(bytes.length);
                    a(bytes, 0, bytes.length);
                } catch (IndexOutOfBoundsException e3) {
                    throw new OutOfSpaceException(e3);
                } catch (OutOfSpaceException e4) {
                    throw e4;
                }
            } catch (IndexOutOfBoundsException e5) {
                throw new OutOfSpaceException(e5);
            }
        }

        public final void a(byte[] bArr, int i2, int i3) {
            try {
                System.arraycopy(bArr, i2, this.d, this.f561f, i3);
                this.f561f += i3;
            } catch (IndexOutOfBoundsException e2) {
                throw new OutOfSpaceException(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f561f), Integer.valueOf(this.f560e), Integer.valueOf(i3)), e2);
            }
        }

        public final int a() {
            return this.f560e - this.f561f;
        }
    }
}
