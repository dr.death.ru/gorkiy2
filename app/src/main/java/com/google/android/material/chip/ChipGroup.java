package com.google.android.material.chip;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.CompoundButton;
import i.h.l.ViewCompat;
import i.h.l.x.AccessibilityNodeInfoCompat;
import j.c.a.b.b0.FlowLayout;
import j.c.a.b.b0.ThemeEnforcement;
import j.c.a.b.k;
import j.c.a.b.l;
import j.c.a.b.m0.a.MaterialThemeOverlay;
import java.util.ArrayList;
import java.util.List;

public class ChipGroup extends FlowLayout {

    /* renamed from: o  reason: collision with root package name */
    public static final int f463o = k.Widget_MaterialComponents_ChipGroup;

    /* renamed from: f  reason: collision with root package name */
    public int f464f;
    public int g;
    public boolean h;

    /* renamed from: i  reason: collision with root package name */
    public boolean f465i;

    /* renamed from: j  reason: collision with root package name */
    public d f466j;

    /* renamed from: k  reason: collision with root package name */
    public final b f467k;

    /* renamed from: l  reason: collision with root package name */
    public e f468l;

    /* renamed from: m  reason: collision with root package name */
    public int f469m;

    /* renamed from: n  reason: collision with root package name */
    public boolean f470n;

    public class b implements CompoundButton.OnCheckedChangeListener {
        public /* synthetic */ b(a aVar) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.material.chip.ChipGroup.a(int, boolean):void
         arg types: [int, int]
         candidates:
          com.google.android.material.chip.ChipGroup.a(com.google.android.material.chip.ChipGroup, int):void
          j.c.a.b.b0.FlowLayout.a(android.content.Context, android.util.AttributeSet):void
          com.google.android.material.chip.ChipGroup.a(int, boolean):void */
        public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            ChipGroup chipGroup = ChipGroup.this;
            if (!chipGroup.f470n) {
                if (chipGroup.getCheckedChipIds().isEmpty()) {
                    ChipGroup chipGroup2 = ChipGroup.this;
                    if (chipGroup2.f465i) {
                        chipGroup2.a(compoundButton.getId(), true);
                        ChipGroup chipGroup3 = ChipGroup.this;
                        chipGroup3.f469m = compoundButton.getId();
                        d dVar = chipGroup3.f466j;
                        return;
                    }
                }
                int id = compoundButton.getId();
                if (z) {
                    ChipGroup chipGroup4 = ChipGroup.this;
                    int i2 = chipGroup4.f469m;
                    if (!(i2 == -1 || i2 == id || !chipGroup4.h)) {
                        chipGroup4.a(i2, false);
                    }
                    ChipGroup.this.setCheckedId(id);
                    return;
                }
                ChipGroup chipGroup5 = ChipGroup.this;
                if (chipGroup5.f469m == id) {
                    chipGroup5.setCheckedId(-1);
                }
            }
        }
    }

    public static class c extends ViewGroup.MarginLayoutParams {
        public c(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public c(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public c(int i2, int i3) {
            super(i2, i3);
        }
    }

    public interface d {
        void a(ChipGroup chipGroup, int i2);
    }

    public class e implements ViewGroup.OnHierarchyChangeListener {
        public ViewGroup.OnHierarchyChangeListener b;

        public /* synthetic */ e(a aVar) {
        }

        public void onChildViewAdded(View view, View view2) {
            if (view == ChipGroup.this && (view2 instanceof Chip)) {
                if (view2.getId() == -1) {
                    view2.setId(View.generateViewId());
                }
                ((Chip) view2).setOnCheckedChangeListenerInternal(ChipGroup.this.f467k);
            }
            ViewGroup.OnHierarchyChangeListener onHierarchyChangeListener = this.b;
            if (onHierarchyChangeListener != null) {
                onHierarchyChangeListener.onChildViewAdded(view, view2);
            }
        }

        public void onChildViewRemoved(View view, View view2) {
            if (view == ChipGroup.this && (view2 instanceof Chip)) {
                ((Chip) view2).setOnCheckedChangeListenerInternal(null);
            }
            ViewGroup.OnHierarchyChangeListener onHierarchyChangeListener = this.b;
            if (onHierarchyChangeListener != null) {
                onHierarchyChangeListener.onChildViewRemoved(view, view2);
            }
        }
    }

    public ChipGroup(Context context) {
        this(context, null);
    }

    private int getChipCount() {
        int i2 = 0;
        for (int i3 = 0; i3 < getChildCount(); i3++) {
            if (getChildAt(i3) instanceof Chip) {
                i2++;
            }
        }
        return i2;
    }

    /* access modifiers changed from: private */
    public void setCheckedId(int i2) {
        this.f469m = i2;
        d dVar = this.f466j;
        if (dVar != null && this.h) {
            dVar.a(this, i2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.material.chip.ChipGroup.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.google.android.material.chip.ChipGroup.a(com.google.android.material.chip.ChipGroup, int):void
      j.c.a.b.b0.FlowLayout.a(android.content.Context, android.util.AttributeSet):void
      com.google.android.material.chip.ChipGroup.a(int, boolean):void */
    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        if (view instanceof Chip) {
            Chip chip = (Chip) view;
            if (chip.isChecked()) {
                int i3 = this.f469m;
                if (i3 != -1 && this.h) {
                    a(i3, false);
                }
                setCheckedId(chip.getId());
            }
        }
        super.addView(view, i2, layoutParams);
    }

    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return super.checkLayoutParams(layoutParams) && (layoutParams instanceof c);
    }

    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new c(-2, -2);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new c(getContext(), attributeSet);
    }

    public int getCheckedChipId() {
        if (this.h) {
            return this.f469m;
        }
        return -1;
    }

    public List<Integer> getCheckedChipIds() {
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < getChildCount(); i2++) {
            View childAt = getChildAt(i2);
            if ((childAt instanceof Chip) && ((Chip) childAt).isChecked()) {
                arrayList.add(Integer.valueOf(childAt.getId()));
                if (this.h) {
                    return arrayList;
                }
            }
        }
        return arrayList;
    }

    public int getChipSpacingHorizontal() {
        return this.f464f;
    }

    public int getChipSpacingVertical() {
        return this.g;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.material.chip.ChipGroup.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.google.android.material.chip.ChipGroup.a(com.google.android.material.chip.ChipGroup, int):void
      j.c.a.b.b0.FlowLayout.a(android.content.Context, android.util.AttributeSet):void
      com.google.android.material.chip.ChipGroup.a(int, boolean):void */
    public void onFinishInflate() {
        super.onFinishInflate();
        int i2 = this.f469m;
        if (i2 != -1) {
            a(i2, true);
            setCheckedId(this.f469m);
        }
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        new AccessibilityNodeInfoCompat(accessibilityNodeInfo).a(AccessibilityNodeInfoCompat.b.a(getRowCount(), super.d ? getChipCount() : -1, false, this.h ? 1 : 2));
    }

    public void setChipSpacing(int i2) {
        setChipSpacingHorizontal(i2);
        setChipSpacingVertical(i2);
    }

    public void setChipSpacingHorizontal(int i2) {
        if (this.f464f != i2) {
            this.f464f = i2;
            setItemSpacing(i2);
            requestLayout();
        }
    }

    public void setChipSpacingHorizontalResource(int i2) {
        setChipSpacingHorizontal(getResources().getDimensionPixelOffset(i2));
    }

    public void setChipSpacingResource(int i2) {
        setChipSpacing(getResources().getDimensionPixelOffset(i2));
    }

    public void setChipSpacingVertical(int i2) {
        if (this.g != i2) {
            this.g = i2;
            setLineSpacing(i2);
            requestLayout();
        }
    }

    public void setChipSpacingVerticalResource(int i2) {
        setChipSpacingVertical(getResources().getDimensionPixelOffset(i2));
    }

    @Deprecated
    public void setDividerDrawableHorizontal(Drawable drawable) {
        throw new UnsupportedOperationException("Changing divider drawables have no effect. ChipGroup do not use divider drawables as spacing.");
    }

    @Deprecated
    public void setDividerDrawableVertical(Drawable drawable) {
        throw new UnsupportedOperationException("Changing divider drawables have no effect. ChipGroup do not use divider drawables as spacing.");
    }

    @Deprecated
    public void setFlexWrap(int i2) {
        throw new UnsupportedOperationException("Changing flex wrap not allowed. ChipGroup exposes a singleLine attribute instead.");
    }

    public void setOnCheckedChangeListener(d dVar) {
        this.f466j = dVar;
    }

    public void setOnHierarchyChangeListener(ViewGroup.OnHierarchyChangeListener onHierarchyChangeListener) {
        this.f468l.b = onHierarchyChangeListener;
    }

    public void setSelectionRequired(boolean z) {
        this.f465i = z;
    }

    @Deprecated
    public void setShowDividerHorizontal(int i2) {
        throw new UnsupportedOperationException("Changing divider modes has no effect. ChipGroup do not use divider drawables as spacing.");
    }

    @Deprecated
    public void setShowDividerVertical(int i2) {
        throw new UnsupportedOperationException("Changing divider modes has no effect. ChipGroup do not use divider drawables as spacing.");
    }

    public void setSingleLine(boolean z) {
        super.setSingleLine(z);
    }

    public void setSingleSelection(boolean z) {
        if (this.h != z) {
            this.h = z;
            this.f470n = true;
            for (int i2 = 0; i2 < getChildCount(); i2++) {
                View childAt = getChildAt(i2);
                if (childAt instanceof Chip) {
                    ((Chip) childAt).setChecked(false);
                }
            }
            this.f470n = false;
            setCheckedId(-1);
        }
    }

    public ChipGroup(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, j.c.a.b.b.chipGroupStyle);
    }

    public boolean a() {
        return super.d;
    }

    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new c(layoutParams);
    }

    public void setSingleLine(int i2) {
        setSingleLine(getResources().getBoolean(i2));
    }

    public ChipGroup(Context context, AttributeSet attributeSet, int i2) {
        super(MaterialThemeOverlay.a(context, attributeSet, i2, f463o), attributeSet, i2);
        this.f467k = new b(null);
        this.f468l = new e(null);
        this.f469m = -1;
        this.f470n = false;
        TypedArray b2 = ThemeEnforcement.b(getContext(), attributeSet, l.ChipGroup, i2, f463o, new int[0]);
        int dimensionPixelOffset = b2.getDimensionPixelOffset(l.ChipGroup_chipSpacing, 0);
        setChipSpacingHorizontal(b2.getDimensionPixelOffset(l.ChipGroup_chipSpacingHorizontal, dimensionPixelOffset));
        setChipSpacingVertical(b2.getDimensionPixelOffset(l.ChipGroup_chipSpacingVertical, dimensionPixelOffset));
        setSingleLine(b2.getBoolean(l.ChipGroup_singleLine, false));
        setSingleSelection(b2.getBoolean(l.ChipGroup_singleSelection, false));
        setSelectionRequired(b2.getBoolean(l.ChipGroup_selectionRequired, false));
        int resourceId = b2.getResourceId(l.ChipGroup_checkedChip, -1);
        if (resourceId != -1) {
            this.f469m = resourceId;
        }
        b2.recycle();
        super.setOnHierarchyChangeListener(this.f468l);
        ViewCompat.h(this, 1);
    }

    public final void a(int i2, boolean z) {
        View findViewById = findViewById(i2);
        if (findViewById instanceof Chip) {
            this.f470n = true;
            ((Chip) findViewById).setChecked(z);
            this.f470n = false;
        }
    }

    public void setSingleSelection(int i2) {
        setSingleSelection(getResources().getBoolean(i2));
    }
}
