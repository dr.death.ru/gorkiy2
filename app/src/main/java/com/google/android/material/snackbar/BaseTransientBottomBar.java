package com.google.android.material.snackbar;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.WindowInsets;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityManager;
import android.widget.FrameLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.behavior.SwipeDismissBehavior;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import i.b.k.ResourcesFlusher;
import i.h.l.AccessibilityDelegateCompat;
import i.h.l.OnApplyWindowInsetsListener;
import i.h.l.ViewCompat;
import i.h.l.WindowInsetsCompat;
import i.h.l.x.AccessibilityNodeInfoCompat;
import j.c.a.b.b0.ThemeEnforcement;
import j.c.a.b.h0.BaseTransientBottomBar0;
import j.c.a.b.h0.BaseTransientBottomBar1;
import j.c.a.b.h0.BaseTransientBottomBar10;
import j.c.a.b.h0.BaseTransientBottomBar11;
import j.c.a.b.h0.BaseTransientBottomBar5;
import j.c.a.b.h0.BaseTransientBottomBar6;
import j.c.a.b.h0.BaseTransientBottomBar7;
import j.c.a.b.h0.BaseTransientBottomBar8;
import j.c.a.b.h0.BaseTransientBottomBar9;
import j.c.a.b.h0.ContentViewCallback;
import j.c.a.b.h0.SnackbarManager;
import j.c.a.b.l;
import j.c.a.b.m.AnimationUtils;
import j.c.a.b.m0.a.MaterialThemeOverlay;
import java.util.List;

public abstract class BaseTransientBottomBar<B extends BaseTransientBottomBar<B>> {

    /* renamed from: o  reason: collision with root package name */
    public static final Handler f482o = new Handler(Looper.getMainLooper(), new a());

    /* renamed from: p  reason: collision with root package name */
    public static final int[] f483p = {j.c.a.b.b.snackbarStyle};
    public final ViewGroup a;
    public final Context b;
    public final i c;
    public final ContentViewCallback d;

    /* renamed from: e  reason: collision with root package name */
    public int f484e;

    /* renamed from: f  reason: collision with root package name */
    public final Runnable f485f = new b();
    public Rect g;
    public int h;

    /* renamed from: i  reason: collision with root package name */
    public int f486i;

    /* renamed from: j  reason: collision with root package name */
    public int f487j;

    /* renamed from: k  reason: collision with root package name */
    public int f488k;

    /* renamed from: l  reason: collision with root package name */
    public int f489l;

    /* renamed from: m  reason: collision with root package name */
    public final AccessibilityManager f490m;

    /* renamed from: n  reason: collision with root package name */
    public SnackbarManager.b f491n = new e();

    public static class a implements Handler.Callback {
        public boolean handleMessage(Message message) {
            int i2 = message.what;
            if (i2 == 0) {
                BaseTransientBottomBar baseTransientBottomBar = (BaseTransientBottomBar) message.obj;
                baseTransientBottomBar.c.setOnAttachStateChangeListener(new BaseTransientBottomBar9(baseTransientBottomBar));
                if (baseTransientBottomBar.c.getParent() == null) {
                    ViewGroup.LayoutParams layoutParams = baseTransientBottomBar.c.getLayoutParams();
                    if (layoutParams instanceof CoordinatorLayout.f) {
                        CoordinatorLayout.f fVar = (CoordinatorLayout.f) layoutParams;
                        Behavior behavior = new Behavior();
                        f fVar2 = behavior.f492j;
                        if (fVar2 != null) {
                            fVar2.a = baseTransientBottomBar.f491n;
                            behavior.b = new BaseTransientBottomBar11(baseTransientBottomBar);
                            fVar.a(behavior);
                            fVar.g = 80;
                        } else {
                            throw null;
                        }
                    }
                    baseTransientBottomBar.f489l = 0;
                    baseTransientBottomBar.e();
                    baseTransientBottomBar.c.setVisibility(4);
                    baseTransientBottomBar.a.addView(baseTransientBottomBar.c);
                }
                if (ViewCompat.w(baseTransientBottomBar.c)) {
                    baseTransientBottomBar.d();
                } else {
                    baseTransientBottomBar.c.setOnLayoutChangeListener(new BaseTransientBottomBar10(baseTransientBottomBar));
                }
                return true;
            } else if (i2 != 1) {
                return false;
            } else {
                BaseTransientBottomBar baseTransientBottomBar2 = (BaseTransientBottomBar) message.obj;
                int i3 = message.arg1;
                if (!baseTransientBottomBar2.c() || baseTransientBottomBar2.c.getVisibility() != 0) {
                    baseTransientBottomBar2.a(i3);
                } else if (baseTransientBottomBar2.c.getAnimationMode() == 1) {
                    ValueAnimator ofFloat = ValueAnimator.ofFloat(1.0f, 0.0f);
                    ofFloat.setInterpolator(AnimationUtils.a);
                    ofFloat.addUpdateListener(new BaseTransientBottomBar1(baseTransientBottomBar2));
                    ofFloat.setDuration(75L);
                    ofFloat.addListener(new BaseTransientBottomBar0(baseTransientBottomBar2, i3));
                    ofFloat.start();
                } else {
                    ValueAnimator valueAnimator = new ValueAnimator();
                    valueAnimator.setIntValues(0, baseTransientBottomBar2.a());
                    valueAnimator.setInterpolator(AnimationUtils.b);
                    valueAnimator.setDuration(250L);
                    valueAnimator.addListener(new BaseTransientBottomBar5(baseTransientBottomBar2, i3));
                    valueAnimator.addUpdateListener(new BaseTransientBottomBar6(baseTransientBottomBar2));
                    valueAnimator.start();
                }
                return true;
            }
        }
    }

    public class b implements Runnable {
        public b() {
        }

        public void run() {
            Context context;
            BaseTransientBottomBar baseTransientBottomBar = BaseTransientBottomBar.this;
            if (baseTransientBottomBar.c != null && (context = baseTransientBottomBar.b) != null) {
                DisplayMetrics displayMetrics = new DisplayMetrics();
                ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getRealMetrics(displayMetrics);
                int i2 = displayMetrics.heightPixels;
                BaseTransientBottomBar baseTransientBottomBar2 = BaseTransientBottomBar.this;
                int[] iArr = new int[2];
                baseTransientBottomBar2.c.getLocationOnScreen(iArr);
                int height = (i2 - (baseTransientBottomBar2.c.getHeight() + iArr[1])) + ((int) BaseTransientBottomBar.this.c.getTranslationY());
                BaseTransientBottomBar baseTransientBottomBar3 = BaseTransientBottomBar.this;
                if (height < baseTransientBottomBar3.f488k) {
                    ViewGroup.LayoutParams layoutParams = baseTransientBottomBar3.c.getLayoutParams();
                    if (!(layoutParams instanceof ViewGroup.MarginLayoutParams)) {
                        Log.w("BaseTransientBottomBar", "Unable to apply gesture inset because layout params are not MarginLayoutParams");
                        return;
                    }
                    ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
                    int i3 = marginLayoutParams.bottomMargin;
                    BaseTransientBottomBar baseTransientBottomBar4 = BaseTransientBottomBar.this;
                    marginLayoutParams.bottomMargin = (baseTransientBottomBar4.f488k - height) + i3;
                    baseTransientBottomBar4.c.requestLayout();
                }
            }
        }
    }

    public class c implements OnApplyWindowInsetsListener {
        public c() {
        }

        public WindowInsetsCompat a(View view, WindowInsetsCompat windowInsetsCompat) {
            BaseTransientBottomBar.this.h = windowInsetsCompat.a();
            BaseTransientBottomBar.this.f486i = windowInsetsCompat.b();
            BaseTransientBottomBar.this.f487j = windowInsetsCompat.c();
            BaseTransientBottomBar.this.e();
            return windowInsetsCompat;
        }
    }

    public class e implements SnackbarManager.b {
        public e() {
        }

        public void a() {
            Handler handler = BaseTransientBottomBar.f482o;
            handler.sendMessage(handler.obtainMessage(0, BaseTransientBottomBar.this));
        }

        public void a(int i2) {
            Handler handler = BaseTransientBottomBar.f482o;
            handler.sendMessage(handler.obtainMessage(1, i2, 0, BaseTransientBottomBar.this));
        }
    }

    public static class f {
        public SnackbarManager.b a;

        public f(SwipeDismissBehavior<?> swipeDismissBehavior) {
            if (swipeDismissBehavior != null) {
                swipeDismissBehavior.g = SwipeDismissBehavior.a(0.0f, 0.1f, 1.0f);
                swipeDismissBehavior.h = SwipeDismissBehavior.a(0.0f, 0.6f, 1.0f);
                swipeDismissBehavior.f404e = 0;
                return;
            }
            throw null;
        }
    }

    public interface g {
    }

    public interface h {
    }

    public static class i extends FrameLayout {

        /* renamed from: i  reason: collision with root package name */
        public static final View.OnTouchListener f493i = new a();
        public h b;
        public g c;
        public int d;

        /* renamed from: e  reason: collision with root package name */
        public final float f494e;

        /* renamed from: f  reason: collision with root package name */
        public final float f495f;
        public ColorStateList g;
        public PorterDuff.Mode h;

        public static class a implements View.OnTouchListener {
            @SuppressLint({"ClickableViewAccessibility"})
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        }

        public i(Context context) {
            this(context, null);
        }

        public float getActionTextColorAlpha() {
            return this.f495f;
        }

        public int getAnimationMode() {
            return this.d;
        }

        public float getBackgroundOverlayColorAlpha() {
            return this.f494e;
        }

        public void onAttachedToWindow() {
            WindowInsets rootWindowInsets;
            super.onAttachedToWindow();
            g gVar = this.c;
            if (gVar != null) {
                BaseTransientBottomBar9 baseTransientBottomBar9 = (BaseTransientBottomBar9) gVar;
                if (baseTransientBottomBar9 == null) {
                    throw null;
                } else if (Build.VERSION.SDK_INT >= 29 && (rootWindowInsets = baseTransientBottomBar9.a.c.getRootWindowInsets()) != null) {
                    baseTransientBottomBar9.a.f488k = rootWindowInsets.getMandatorySystemGestureInsets().bottom;
                    baseTransientBottomBar9.a.e();
                }
            }
            ViewCompat.B(this);
        }

        public void onDetachedFromWindow() {
            super.onDetachedFromWindow();
            g gVar = this.c;
            if (gVar != null) {
                BaseTransientBottomBar9 baseTransientBottomBar9 = (BaseTransientBottomBar9) gVar;
                BaseTransientBottomBar baseTransientBottomBar = baseTransientBottomBar9.a;
                if (baseTransientBottomBar == null) {
                    throw null;
                } else if (SnackbarManager.b().a(baseTransientBottomBar.f491n)) {
                    BaseTransientBottomBar.f482o.post(new BaseTransientBottomBar7(baseTransientBottomBar9));
                }
            }
        }

        public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
            super.onLayout(z, i2, i3, i4, i5);
            h hVar = this.b;
            if (hVar != null) {
                BaseTransientBottomBar10 baseTransientBottomBar10 = (BaseTransientBottomBar10) hVar;
                baseTransientBottomBar10.a.c.setOnLayoutChangeListener(null);
                baseTransientBottomBar10.a.d();
            }
        }

        public void setAnimationMode(int i2) {
            this.d = i2;
        }

        public void setBackground(Drawable drawable) {
            setBackgroundDrawable(drawable);
        }

        public void setBackgroundDrawable(Drawable drawable) {
            if (!(drawable == null || this.g == null)) {
                drawable = ResourcesFlusher.d(drawable.mutate());
                drawable.setTintList(this.g);
                drawable.setTintMode(this.h);
            }
            super.setBackgroundDrawable(drawable);
        }

        public void setBackgroundTintList(ColorStateList colorStateList) {
            this.g = colorStateList;
            if (getBackground() != null) {
                Drawable d2 = ResourcesFlusher.d(getBackground().mutate());
                d2.setTintList(colorStateList);
                d2.setTintMode(this.h);
                if (d2 != getBackground()) {
                    super.setBackgroundDrawable(d2);
                }
            }
        }

        public void setBackgroundTintMode(PorterDuff.Mode mode) {
            this.h = mode;
            if (getBackground() != null) {
                Drawable d2 = ResourcesFlusher.d(getBackground().mutate());
                d2.setTintMode(mode);
                if (d2 != getBackground()) {
                    super.setBackgroundDrawable(d2);
                }
            }
        }

        public void setOnAttachStateChangeListener(g gVar) {
            this.c = gVar;
        }

        public void setOnClickListener(View.OnClickListener onClickListener) {
            setOnTouchListener(onClickListener != null ? null : f493i);
            super.setOnClickListener(onClickListener);
        }

        public void setOnLayoutChangeListener(h hVar) {
            this.b = hVar;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: j.c.a.a.c.n.c.a(android.view.View, int):int
         arg types: [com.google.android.material.snackbar.BaseTransientBottomBar$i, int]
         candidates:
          j.c.a.a.c.n.c.a(android.content.Context, int):float
          j.c.a.a.c.n.c.a(byte[], int):long
          j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
          j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
          j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
          j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
          j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
          j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
          j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
          j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
          j.c.a.a.c.n.c.a(android.view.View, float):void
          j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
          j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
          j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
          j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
          j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
          j.c.a.a.c.n.c.a(android.view.View, int):int */
        public i(Context context, AttributeSet attributeSet) {
            super(MaterialThemeOverlay.a(context, attributeSet, 0, 0), attributeSet);
            Drawable drawable;
            Context context2 = getContext();
            TypedArray obtainStyledAttributes = context2.obtainStyledAttributes(attributeSet, l.SnackbarLayout);
            if (obtainStyledAttributes.hasValue(l.SnackbarLayout_elevation)) {
                ViewCompat.a(this, (float) obtainStyledAttributes.getDimensionPixelSize(l.SnackbarLayout_elevation, 0));
            }
            this.d = obtainStyledAttributes.getInt(l.SnackbarLayout_animationMode, 0);
            this.f494e = obtainStyledAttributes.getFloat(l.SnackbarLayout_backgroundOverlayColorAlpha, 1.0f);
            setBackgroundTintList(j.c.a.a.c.n.c.a(context2, obtainStyledAttributes, l.SnackbarLayout_backgroundTint));
            setBackgroundTintMode(j.c.a.a.c.n.c.a(obtainStyledAttributes.getInt(l.SnackbarLayout_backgroundTintMode, -1), PorterDuff.Mode.SRC_IN));
            this.f495f = obtainStyledAttributes.getFloat(l.SnackbarLayout_actionTextColorAlpha, 1.0f);
            obtainStyledAttributes.recycle();
            setOnTouchListener(f493i);
            setFocusable(true);
            if (getBackground() == null) {
                float dimension = getResources().getDimension(j.c.a.b.d.mtrl_snackbar_background_corner_radius);
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setShape(0);
                gradientDrawable.setCornerRadius(dimension);
                int i2 = j.c.a.b.b.colorSurface;
                int i3 = j.c.a.b.b.colorOnSurface;
                gradientDrawable.setColor(j.c.a.a.c.n.c.a(j.c.a.a.c.n.c.a((View) this, i2), j.c.a.a.c.n.c.a((View) this, i3), getBackgroundOverlayColorAlpha()));
                if (this.g != null) {
                    drawable = ResourcesFlusher.d((Drawable) gradientDrawable);
                    drawable.setTintList(this.g);
                } else {
                    drawable = ResourcesFlusher.d((Drawable) gradientDrawable);
                }
                ViewCompat.a(this, drawable);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(android.view.View, int):int
     arg types: [com.google.android.material.snackbar.SnackbarContentLayout, int]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(android.view.View, int):int */
    public BaseTransientBottomBar(ViewGroup viewGroup, View view, ContentViewCallback contentViewCallback) {
        if (viewGroup == null) {
            throw new IllegalArgumentException("Transient bottom bar must have non-null parent");
        } else if (view == null) {
            throw new IllegalArgumentException("Transient bottom bar must have non-null content");
        } else if (contentViewCallback != null) {
            this.a = viewGroup;
            this.d = contentViewCallback;
            Context context = viewGroup.getContext();
            this.b = context;
            ThemeEnforcement.a(context, ThemeEnforcement.a, "Theme.AppCompat");
            LayoutInflater from = LayoutInflater.from(this.b);
            TypedArray obtainStyledAttributes = this.b.obtainStyledAttributes(f483p);
            int resourceId = obtainStyledAttributes.getResourceId(0, -1);
            obtainStyledAttributes.recycle();
            i iVar = (i) from.inflate(resourceId != -1 ? j.c.a.b.h.mtrl_layout_snackbar : j.c.a.b.h.design_layout_snackbar, this.a, false);
            this.c = iVar;
            if (view instanceof SnackbarContentLayout) {
                SnackbarContentLayout snackbarContentLayout = (SnackbarContentLayout) view;
                float actionTextColorAlpha = iVar.getActionTextColorAlpha();
                if (actionTextColorAlpha != 1.0f) {
                    snackbarContentLayout.c.setTextColor(j.c.a.a.c.n.c.a(j.c.a.a.c.n.c.a((View) snackbarContentLayout, j.c.a.b.b.colorSurface), snackbarContentLayout.c.getCurrentTextColor(), actionTextColorAlpha));
                }
            }
            this.c.addView(view);
            ViewGroup.LayoutParams layoutParams = this.c.getLayoutParams();
            if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
                ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
                this.g = new Rect(marginLayoutParams.leftMargin, marginLayoutParams.topMargin, marginLayoutParams.rightMargin, marginLayoutParams.bottomMargin);
            }
            ViewCompat.g(this.c, 1);
            this.c.setImportantForAccessibility(1);
            this.c.setFitsSystemWindows(true);
            ViewCompat.a(this.c, new c());
            ViewCompat.a(this.c, new d());
            this.f490m = (AccessibilityManager) this.b.getSystemService("accessibility");
        } else {
            throw new IllegalArgumentException("Transient bottom bar must have non-null callback");
        }
    }

    public final int a() {
        int height = this.c.getHeight();
        ViewGroup.LayoutParams layoutParams = this.c.getLayoutParams();
        return layoutParams instanceof ViewGroup.MarginLayoutParams ? height + ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin : height;
    }

    public void b() {
        SnackbarManager.b().e(this.f491n);
    }

    public boolean c() {
        List<AccessibilityServiceInfo> enabledAccessibilityServiceList = this.f490m.getEnabledAccessibilityServiceList(1);
        if (enabledAccessibilityServiceList == null || !enabledAccessibilityServiceList.isEmpty()) {
            return false;
        }
        return true;
    }

    public final void d() {
        if (c()) {
            this.c.post(new BaseTransientBottomBar8(this));
            return;
        }
        this.c.setVisibility(0);
        b();
    }

    public final void e() {
        Rect rect;
        ViewGroup.LayoutParams layoutParams = this.c.getLayoutParams();
        if (!(layoutParams instanceof ViewGroup.MarginLayoutParams) || (rect = this.g) == null) {
            Log.w("BaseTransientBottomBar", "Unable to update margins because layout params are not MarginLayoutParams");
            return;
        }
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
        marginLayoutParams.bottomMargin = rect.bottom + this.h;
        marginLayoutParams.leftMargin = rect.left + this.f486i;
        marginLayoutParams.rightMargin = rect.right + this.f487j;
        this.c.requestLayout();
        if (Build.VERSION.SDK_INT >= 29) {
            boolean z = false;
            if (this.f488k > 0) {
                ViewGroup.LayoutParams layoutParams2 = this.c.getLayoutParams();
                if ((layoutParams2 instanceof CoordinatorLayout.f) && (((CoordinatorLayout.f) layoutParams2).a instanceof SwipeDismissBehavior)) {
                    z = true;
                }
            }
            if (z) {
                this.c.removeCallbacks(this.f485f);
                this.c.post(this.f485f);
            }
        }
    }

    public static class Behavior extends SwipeDismissBehavior<View> {

        /* renamed from: j  reason: collision with root package name */
        public final f f492j = new f(super);

        public boolean a(View view) {
            if (this.f492j != null) {
                return view instanceof i;
            }
            throw null;
        }

        public boolean a(CoordinatorLayout coordinatorLayout, View view, MotionEvent motionEvent) {
            f fVar = this.f492j;
            if (fVar != null) {
                int actionMasked = motionEvent.getActionMasked();
                if (actionMasked != 0) {
                    if (actionMasked == 1 || actionMasked == 3) {
                        SnackbarManager.b().g(fVar.a);
                    }
                } else if (coordinatorLayout.a(view, (int) motionEvent.getX(), (int) motionEvent.getY())) {
                    SnackbarManager.b().f(fVar.a);
                }
                return super.a(coordinatorLayout, view, motionEvent);
            }
            throw null;
        }
    }

    public class d extends AccessibilityDelegateCompat {
        public d() {
        }

        public void a(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            super.a.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfoCompat.a);
            accessibilityNodeInfoCompat.a.addAction(1048576);
            accessibilityNodeInfoCompat.a.setDismissable(true);
        }

        public boolean a(View view, int i2, Bundle bundle) {
            if (i2 != 1048576) {
                return super.a(view, i2, bundle);
            }
            Snackbar snackbar = (Snackbar) BaseTransientBottomBar.this;
            if (snackbar != null) {
                SnackbarManager.b().a(snackbar.f491n, 3);
                return true;
            }
            throw null;
        }
    }

    public void a(int i2) {
        SnackbarManager.b().d(this.f491n);
        ViewParent parent = this.c.getParent();
        if (parent instanceof ViewGroup) {
            ((ViewGroup) parent).removeView(this.c);
        }
    }
}
