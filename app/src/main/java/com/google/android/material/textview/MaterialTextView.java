package com.google.android.material.textview;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import i.b.q.AppCompatTextView;
import j.c.a.a.c.n.c;
import j.c.a.b.b;
import j.c.a.b.l;
import j.c.a.b.m0.a.MaterialThemeOverlay;

public class MaterialTextView extends AppCompatTextView {
    public MaterialTextView(Context context) {
        this(context, null);
    }

    public final void a(Resources.Theme theme, int i2) {
        TypedArray obtainStyledAttributes = theme.obtainStyledAttributes(i2, l.MaterialTextAppearance);
        int a = a(getContext(), obtainStyledAttributes, l.MaterialTextAppearance_android_lineHeight, l.MaterialTextAppearance_lineHeight);
        obtainStyledAttributes.recycle();
        if (a >= 0) {
            setLineHeight(a);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(android.content.Context, int, boolean):boolean
     arg types: [android.content.Context, int, int]
     candidates:
      j.c.a.a.c.n.c.a(float, float, float):float
      j.c.a.a.c.n.c.a(int, int, float):int
      j.c.a.a.c.n.c.a(android.content.Context, int, int):int
      j.c.a.a.c.n.c.a(android.content.Context, int, java.lang.String):int
      j.c.a.a.c.n.c.a(byte[], int, j.c.a.a.f.e.s2):int
      j.c.a.a.c.n.c.a(byte[], int, int):long
      j.c.a.a.c.n.c.a(android.content.Context, android.content.res.TypedArray, int):android.content.res.ColorStateList
      j.c.a.a.c.n.c.a(android.content.Context, i.b.q.TintTypedArray, int):android.content.res.ColorStateList
      j.c.a.a.c.n.c.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList, android.graphics.PorterDuff$Mode):android.graphics.PorterDuffColorFilter
      j.c.a.a.c.n.c.a(j.c.a.a.i.e, long, java.util.concurrent.TimeUnit):TResult
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String[], java.lang.String[]):java.lang.String
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[], byte[]):java.security.interfaces.ECPublicKey
      j.c.a.a.c.n.c.a(j.c.a.a.f.e.f5, java.lang.StringBuilder, int):void
      j.c.a.a.c.n.c.a(j.c.e.MessageLite, java.lang.StringBuilder, int):void
      j.c.a.a.c.n.c.a(byte[], long, int):void
      j.c.a.a.c.n.c.a(l.b.ObservableSource, l.b.Observer, l.b.t.Function):boolean
      j.c.a.a.c.n.c.a(android.content.Context, int, boolean):boolean */
    public void setTextAppearance(Context context, int i2) {
        super.setTextAppearance(context, i2);
        if (c.a(context, b.textAppearanceLineHeightEnabled, true)) {
            a(context.getTheme(), i2);
        }
    }

    public MaterialTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16842884);
    }

    public MaterialTextView(Context context, AttributeSet attributeSet, int i2) {
        this(context, attributeSet, i2, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(android.content.Context, int, boolean):boolean
     arg types: [android.content.Context, int, int]
     candidates:
      j.c.a.a.c.n.c.a(float, float, float):float
      j.c.a.a.c.n.c.a(int, int, float):int
      j.c.a.a.c.n.c.a(android.content.Context, int, int):int
      j.c.a.a.c.n.c.a(android.content.Context, int, java.lang.String):int
      j.c.a.a.c.n.c.a(byte[], int, j.c.a.a.f.e.s2):int
      j.c.a.a.c.n.c.a(byte[], int, int):long
      j.c.a.a.c.n.c.a(android.content.Context, android.content.res.TypedArray, int):android.content.res.ColorStateList
      j.c.a.a.c.n.c.a(android.content.Context, i.b.q.TintTypedArray, int):android.content.res.ColorStateList
      j.c.a.a.c.n.c.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList, android.graphics.PorterDuff$Mode):android.graphics.PorterDuffColorFilter
      j.c.a.a.c.n.c.a(j.c.a.a.i.e, long, java.util.concurrent.TimeUnit):TResult
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String[], java.lang.String[]):java.lang.String
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[], byte[]):java.security.interfaces.ECPublicKey
      j.c.a.a.c.n.c.a(j.c.a.a.f.e.f5, java.lang.StringBuilder, int):void
      j.c.a.a.c.n.c.a(j.c.e.MessageLite, java.lang.StringBuilder, int):void
      j.c.a.a.c.n.c.a(byte[], long, int):void
      j.c.a.a.c.n.c.a(l.b.ObservableSource, l.b.Observer, l.b.t.Function):boolean
      j.c.a.a.c.n.c.a(android.content.Context, int, boolean):boolean */
    public MaterialTextView(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(MaterialThemeOverlay.a(context, attributeSet, i2, i3), attributeSet, i2);
        Context context2 = getContext();
        boolean z = true;
        if (c.a(context2, b.textAppearanceLineHeightEnabled, true)) {
            Resources.Theme theme = context2.getTheme();
            TypedArray obtainStyledAttributes = theme.obtainStyledAttributes(attributeSet, l.MaterialTextView, i2, i3);
            int a = a(context2, obtainStyledAttributes, l.MaterialTextView_android_lineHeight, l.MaterialTextView_lineHeight);
            obtainStyledAttributes.recycle();
            if (!(a == -1 ? false : z)) {
                TypedArray obtainStyledAttributes2 = theme.obtainStyledAttributes(attributeSet, l.MaterialTextView, i2, i3);
                int resourceId = obtainStyledAttributes2.getResourceId(l.MaterialTextView_android_textAppearance, -1);
                obtainStyledAttributes2.recycle();
                if (resourceId != -1) {
                    a(theme, resourceId);
                }
            }
        }
    }

    public static int a(Context context, TypedArray typedArray, int... iArr) {
        int i2 = -1;
        for (int i3 = 0; i3 < iArr.length && i2 < 0; i3++) {
            int i4 = iArr[i3];
            TypedValue typedValue = new TypedValue();
            if (!typedArray.getValue(i4, typedValue) || typedValue.type != 2) {
                i2 = typedArray.getDimensionPixelSize(i4, -1);
            } else {
                TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(new int[]{typedValue.data});
                int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(0, -1);
                obtainStyledAttributes.recycle();
                i2 = dimensionPixelSize;
            }
        }
        return i2;
    }
}
