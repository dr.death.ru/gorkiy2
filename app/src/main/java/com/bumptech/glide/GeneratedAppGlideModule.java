package com.bumptech.glide;

import j.b.a.n.RequestManagerRetriever;
import j.b.a.o.AppGlideModule;
import java.util.Set;

public abstract class GeneratedAppGlideModule extends AppGlideModule {
    public abstract Set<Class<?>> b();

    public RequestManagerRetriever.b c() {
        return null;
    }
}
