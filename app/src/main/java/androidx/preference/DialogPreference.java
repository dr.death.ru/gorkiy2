package androidx.preference;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import i.b.k.ResourcesFlusher;
import i.q.a;
import i.q.d;

public abstract class DialogPreference extends Preference {

    /* renamed from: p  reason: collision with root package name */
    public CharSequence f252p;

    public DialogPreference(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(context, attributeSet, i2, i3);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, d.DialogPreference, i2, i3);
        String a = ResourcesFlusher.a(obtainStyledAttributes, d.DialogPreference_dialogTitle, d.DialogPreference_android_dialogTitle);
        this.f252p = a;
        if (a == null) {
            this.f252p = super.d;
        }
        int i4 = d.DialogPreference_dialogMessage;
        int i5 = d.DialogPreference_android_dialogMessage;
        if (obtainStyledAttributes.getString(i4) == null) {
            obtainStyledAttributes.getString(i5);
        }
        int i6 = d.DialogPreference_dialogIcon;
        int i7 = d.DialogPreference_android_dialogIcon;
        if (obtainStyledAttributes.getDrawable(i6) == null) {
            obtainStyledAttributes.getDrawable(i7);
        }
        int i8 = d.DialogPreference_positiveButtonText;
        int i9 = d.DialogPreference_android_positiveButtonText;
        if (obtainStyledAttributes.getString(i8) == null) {
            obtainStyledAttributes.getString(i9);
        }
        int i10 = d.DialogPreference_negativeButtonText;
        int i11 = d.DialogPreference_android_negativeButtonText;
        if (obtainStyledAttributes.getString(i10) == null) {
            obtainStyledAttributes.getString(i11);
        }
        obtainStyledAttributes.getResourceId(d.DialogPreference_dialogLayout, obtainStyledAttributes.getResourceId(d.DialogPreference_android_dialogLayout, 0));
        obtainStyledAttributes.recycle();
    }

    public DialogPreference(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, ResourcesFlusher.a(context, a.dialogPreferenceStyle, 16842897), 0);
    }
}
