package androidx.preference;

import android.content.res.TypedArray;
import android.text.TextUtils;
import androidx.preference.Preference;
import i.q.c;

public class EditTextPreference extends DialogPreference {

    public static final class a implements Preference.a<EditTextPreference> {
        public static a a;

        public CharSequence a(Preference preference) {
            EditTextPreference editTextPreference = (EditTextPreference) preference;
            if (editTextPreference == null) {
                throw null;
            } else if (TextUtils.isEmpty(null)) {
                return editTextPreference.b.getString(c.not_set);
            } else {
                return null;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, boolean):boolean
     arg types: [android.content.res.TypedArray, int, int, int]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int, boolean, int):int
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.content.res.Resources$Theme):android.content.res.ColorStateList
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, android.content.res.Resources$Theme, android.util.AttributeSet, int[]):android.content.res.TypedArray
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int):java.lang.String
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, java.lang.String, boolean):void
      i.b.k.ResourcesFlusher.a(int, android.graphics.Rect, android.graphics.Rect, android.graphics.Rect):boolean
      i.b.k.ResourcesFlusher.a(i.f.a.h.d, i.f.a.h.f, java.util.List<i.f.a.h.f>, boolean):boolean
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, boolean):boolean */
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public EditTextPreference(android.content.Context r4, android.util.AttributeSet r5) {
        /*
            r3 = this;
            int r0 = i.q.a.editTextPreferenceStyle
            r1 = 16842898(0x1010092, float:2.3693967E-38)
            int r0 = i.b.k.ResourcesFlusher.a(r4, r0, r1)
            r1 = 0
            r3.<init>(r4, r5, r0, r1)
            int[] r2 = i.q.d.EditTextPreference
            android.content.res.TypedArray r4 = r4.obtainStyledAttributes(r5, r2, r0, r1)
            int r5 = i.q.d.EditTextPreference_useSimpleSummaryProvider
            boolean r5 = i.b.k.ResourcesFlusher.a(r4, r5, r5, r1)
            if (r5 == 0) goto L_0x002a
            androidx.preference.EditTextPreference$a r5 = androidx.preference.EditTextPreference.a.a
            if (r5 != 0) goto L_0x0026
            androidx.preference.EditTextPreference$a r5 = new androidx.preference.EditTextPreference$a
            r5.<init>()
            androidx.preference.EditTextPreference.a.a = r5
        L_0x0026:
            androidx.preference.EditTextPreference$a r5 = androidx.preference.EditTextPreference.a.a
            r3.f265o = r5
        L_0x002a:
            r4.recycle()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.preference.EditTextPreference.<init>(android.content.Context, android.util.AttributeSet):void");
    }

    public Object a(TypedArray typedArray, int i2) {
        return typedArray.getString(i2);
    }
}
