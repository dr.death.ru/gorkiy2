package androidx.preference;

import android.content.Context;
import android.util.AttributeSet;
import i.b.k.ResourcesFlusher;
import i.q.a;

public class PreferenceCategory extends PreferenceGroup {
    public PreferenceCategory(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, ResourcesFlusher.a(context, a.preferenceCategoryStyle, 16842892), 0);
    }

    public boolean g() {
        return false;
    }
}
