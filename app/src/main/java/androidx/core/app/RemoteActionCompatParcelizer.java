package androidx.core.app;

import android.app.PendingIntent;
import android.text.TextUtils;
import androidx.core.graphics.drawable.IconCompat;
import androidx.versionedparcelable.VersionedParcel;
import i.y.VersionedParcelParcel;

public class RemoteActionCompatParcelizer {
    public static RemoteActionCompat read(VersionedParcel versionedParcel) {
        RemoteActionCompat remoteActionCompat = new RemoteActionCompat();
        Object obj = remoteActionCompat.a;
        if (versionedParcel.a(1)) {
            obj = versionedParcel.c();
        }
        remoteActionCompat.a = (IconCompat) obj;
        remoteActionCompat.b = versionedParcel.a(remoteActionCompat.b, 2);
        remoteActionCompat.c = versionedParcel.a(remoteActionCompat.c, 3);
        remoteActionCompat.d = (PendingIntent) versionedParcel.a(remoteActionCompat.d, 4);
        remoteActionCompat.f199e = versionedParcel.a(remoteActionCompat.f199e, 5);
        remoteActionCompat.f200f = versionedParcel.a(remoteActionCompat.f200f, 6);
        return remoteActionCompat;
    }

    public static void write(RemoteActionCompat remoteActionCompat, VersionedParcel versionedParcel) {
        if (versionedParcel != null) {
            IconCompat iconCompat = remoteActionCompat.a;
            versionedParcel.b(1);
            versionedParcel.a(iconCompat);
            CharSequence charSequence = remoteActionCompat.b;
            versionedParcel.b(2);
            VersionedParcelParcel versionedParcelParcel = (VersionedParcelParcel) versionedParcel;
            TextUtils.writeToParcel(charSequence, versionedParcelParcel.f1522e, 0);
            CharSequence charSequence2 = remoteActionCompat.c;
            versionedParcel.b(3);
            TextUtils.writeToParcel(charSequence2, versionedParcelParcel.f1522e, 0);
            versionedParcel.b(remoteActionCompat.d, 4);
            boolean z = remoteActionCompat.f199e;
            versionedParcel.b(5);
            versionedParcelParcel.f1522e.writeInt(z ? 1 : 0);
            boolean z2 = remoteActionCompat.f200f;
            versionedParcel.b(6);
            versionedParcelParcel.f1522e.writeInt(z2 ? 1 : 0);
            return;
        }
        throw null;
    }
}
