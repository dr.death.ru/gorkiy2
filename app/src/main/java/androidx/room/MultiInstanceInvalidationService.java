package androidx.room;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.util.Log;
import i.s.IMultiInstanceInvalidationCallback;
import i.s.IMultiInstanceInvalidationService;
import i.s.d;
import java.util.HashMap;

public class MultiInstanceInvalidationService extends Service {
    public int b = 0;
    public final HashMap<Integer, String> c = new HashMap<>();
    public final RemoteCallbackList<d> d = new a();

    /* renamed from: e  reason: collision with root package name */
    public final IMultiInstanceInvalidationService.a f348e = new b();

    public class a extends RemoteCallbackList<d> {
        public a() {
        }

        public void onCallbackDied(IInterface iInterface, Object obj) {
            IMultiInstanceInvalidationCallback iMultiInstanceInvalidationCallback = (IMultiInstanceInvalidationCallback) iInterface;
            MultiInstanceInvalidationService.this.c.remove(Integer.valueOf(((Integer) obj).intValue()));
        }
    }

    public IBinder onBind(Intent intent) {
        return this.f348e;
    }

    public class b extends IMultiInstanceInvalidationService.a {
        public b() {
        }

        public int a(IMultiInstanceInvalidationCallback iMultiInstanceInvalidationCallback, String str) {
            if (str == null) {
                return 0;
            }
            synchronized (MultiInstanceInvalidationService.this.d) {
                MultiInstanceInvalidationService multiInstanceInvalidationService = MultiInstanceInvalidationService.this;
                int i2 = multiInstanceInvalidationService.b + 1;
                multiInstanceInvalidationService.b = i2;
                if (MultiInstanceInvalidationService.this.d.register(iMultiInstanceInvalidationCallback, Integer.valueOf(i2))) {
                    MultiInstanceInvalidationService.this.c.put(Integer.valueOf(i2), str);
                    return i2;
                }
                MultiInstanceInvalidationService multiInstanceInvalidationService2 = MultiInstanceInvalidationService.this;
                multiInstanceInvalidationService2.b--;
                return 0;
            }
        }

        public void a(IMultiInstanceInvalidationCallback iMultiInstanceInvalidationCallback, int i2) {
            synchronized (MultiInstanceInvalidationService.this.d) {
                MultiInstanceInvalidationService.this.d.unregister(iMultiInstanceInvalidationCallback);
                MultiInstanceInvalidationService.this.c.remove(Integer.valueOf(i2));
            }
        }

        public void a(int i2, String[] strArr) {
            synchronized (MultiInstanceInvalidationService.this.d) {
                String str = MultiInstanceInvalidationService.this.c.get(Integer.valueOf(i2));
                if (str == null) {
                    Log.w("ROOM", "Remote invalidation client ID not registered");
                    return;
                }
                int beginBroadcast = MultiInstanceInvalidationService.this.d.beginBroadcast();
                for (int i3 = 0; i3 < beginBroadcast; i3++) {
                    try {
                        int intValue = ((Integer) MultiInstanceInvalidationService.this.d.getBroadcastCookie(i3)).intValue();
                        String str2 = MultiInstanceInvalidationService.this.c.get(Integer.valueOf(intValue));
                        if (i2 != intValue && str.equals(str2)) {
                            MultiInstanceInvalidationService.this.d.getBroadcastItem(i3).a(strArr);
                        }
                    } catch (RemoteException e2) {
                        Log.w("ROOM", "Error invoking a remote callback", e2);
                    } catch (Throwable th) {
                        MultiInstanceInvalidationService.this.d.finishBroadcast();
                        throw th;
                    }
                }
                MultiInstanceInvalidationService.this.d.finishBroadcast();
            }
        }
    }
}
