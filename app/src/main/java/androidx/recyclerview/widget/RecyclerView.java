package androidx.recyclerview.widget;

import android.animation.LayoutTransition;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.Observable;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.os.Trace;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.Display;
import android.view.FocusFinder;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewPropertyAnimator;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.animation.Interpolator;
import android.widget.EdgeEffect;
import android.widget.OverScroller;
import i.h.h.TraceCompat;
import i.h.l.AccessibilityDelegateCompat;
import i.h.l.NestedScrollingChild2;
import i.h.l.NestedScrollingChild3;
import i.h.l.NestedScrollingChildHelper;
import i.h.l.ViewCompat;
import i.h.l.ViewConfigurationCompat;
import i.h.l.x.AccessibilityNodeInfoCompat;
import i.j.a.AbsSavedState;
import i.r.d.AdapterHelper;
import i.r.d.ChildHelper;
import i.r.d.DefaultItemAnimator;
import i.r.d.DefaultItemAnimator0;
import i.r.d.DefaultItemAnimator1;
import i.r.d.DefaultItemAnimator2;
import i.r.d.DefaultItemAnimator7;
import i.r.d.FastScroller;
import i.r.d.GapWorker;
import i.r.d.LinearSmoothScroller;
import i.r.d.RecyclerViewAccessibilityDelegate;
import i.r.d.SimpleItemAnimator;
import i.r.d.ViewBoundsCheck;
import i.r.d.ViewInfoStore;
import j.a.a.a.outline;
import java.lang.ref.WeakReference;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import l.a.a.a.o.b.AbstractSpiCall;

public class RecyclerView extends ViewGroup implements NestedScrollingChild2, NestedScrollingChild3 {
    public static final boolean ALLOW_SIZE_IN_UNSPECIFIED_SPEC = (Build.VERSION.SDK_INT >= 23);
    public static final boolean ALLOW_THREAD_GAP_WORK = true;
    public static final boolean DEBUG = false;
    public static final int DEFAULT_ORIENTATION = 1;
    public static final boolean DISPATCH_TEMP_DETACH = false;
    public static final boolean FORCE_ABS_FOCUS_SEARCH_DIRECTION = false;
    public static final boolean FORCE_INVALIDATE_DISPLAY_LIST = false;
    public static final long FOREVER_NS = Long.MAX_VALUE;
    public static final int HORIZONTAL = 0;
    public static final boolean IGNORE_DETACHED_FOCUSED_CHILD = false;
    public static final int INVALID_POINTER = -1;
    public static final int INVALID_TYPE = -1;
    public static final Class<?>[] LAYOUT_MANAGER_CONSTRUCTOR_SIGNATURE;
    public static final int MAX_SCROLL_DURATION = 2000;
    public static final int[] NESTED_SCROLLING_ATTRS = {16843830};
    public static final long NO_ID = -1;
    public static final int NO_POSITION = -1;
    public static final boolean POST_UPDATES_ON_ANIMATION = true;
    public static final int SCROLL_STATE_DRAGGING = 1;
    public static final int SCROLL_STATE_IDLE = 0;
    public static final int SCROLL_STATE_SETTLING = 2;
    public static final String TAG = "RecyclerView";
    public static final int TOUCH_SLOP_DEFAULT = 0;
    public static final int TOUCH_SLOP_PAGING = 1;
    public static final String TRACE_BIND_VIEW_TAG = "RV OnBindView";
    public static final String TRACE_CREATE_VIEW_TAG = "RV CreateView";
    public static final String TRACE_HANDLE_ADAPTER_UPDATES_TAG = "RV PartialInvalidate";
    public static final String TRACE_NESTED_PREFETCH_TAG = "RV Nested Prefetch";
    public static final String TRACE_ON_DATA_SET_CHANGE_LAYOUT_TAG = "RV FullInvalidate";
    public static final String TRACE_ON_LAYOUT_TAG = "RV OnLayout";
    public static final String TRACE_PREFETCH_TAG = "RV Prefetch";
    public static final String TRACE_SCROLL_TAG = "RV Scroll";
    public static final int UNDEFINED_DURATION = Integer.MIN_VALUE;
    public static final boolean VERBOSE_TRACING = false;
    public static final int VERTICAL = 1;
    public static final Interpolator sQuinticInterpolator = new c();
    public RecyclerViewAccessibilityDelegate mAccessibilityDelegate;
    public final AccessibilityManager mAccessibilityManager;
    public g mAdapter;
    public AdapterHelper mAdapterHelper;
    public boolean mAdapterUpdateDuringMeasure;
    public EdgeEffect mBottomGlow;
    public j mChildDrawingOrderCallback;
    public ChildHelper mChildHelper;
    public boolean mClipToPadding;
    public boolean mDataSetHasChangedAfterLayout;
    public boolean mDispatchItemsChangedEvent;
    public int mDispatchScrollCounter;
    public int mEatenAccessibilityChangeFlags;
    public k mEdgeEffectFactory;
    public boolean mEnableFastScroller;
    public boolean mFirstLayoutComplete;
    public GapWorker mGapWorker;
    public boolean mHasFixedSize;
    public boolean mIgnoreMotionEventTillDown;
    public int mInitialTouchX;
    public int mInitialTouchY;
    public int mInterceptRequestLayoutDepth;
    public s mInterceptingOnItemTouchListener;
    public boolean mIsAttached;
    public l mItemAnimator;
    public l.b mItemAnimatorListener;
    public Runnable mItemAnimatorRunner;
    public final ArrayList<n> mItemDecorations;
    public boolean mItemsAddedOrRemoved;
    public boolean mItemsChanged;
    public int mLastTouchX;
    public int mLastTouchY;
    public o mLayout;
    public int mLayoutOrScrollCounter;
    public boolean mLayoutSuppressed;
    public boolean mLayoutWasDefered;
    public EdgeEffect mLeftGlow;
    public final int mMaxFlingVelocity;
    public final int mMinFlingVelocity;
    public final int[] mMinMaxLayoutPositions;
    public final int[] mNestedOffsets;
    public final x mObserver;
    public List<q> mOnChildAttachStateListeners;
    public r mOnFlingListener;
    public final ArrayList<s> mOnItemTouchListeners;
    public final List<d0> mPendingAccessibilityImportanceChange;
    public y mPendingSavedState;
    public boolean mPostedAnimatorRunner;
    public GapWorker.b mPrefetchRegistry;
    public boolean mPreserveFocusAfterLayout;
    public final v mRecycler;
    public w mRecyclerListener;
    public final int[] mReusableIntPair;
    public EdgeEffect mRightGlow;
    public float mScaledHorizontalScrollFactor;
    public float mScaledVerticalScrollFactor;
    public t mScrollListener;
    public List<t> mScrollListeners;
    public final int[] mScrollOffset;
    public int mScrollPointerId;
    public int mScrollState;
    public NestedScrollingChildHelper mScrollingChildHelper;
    public final a0 mState;
    public final Rect mTempRect;
    public final Rect mTempRect2;
    public final RectF mTempRectF;
    public EdgeEffect mTopGlow;
    public int mTouchSlop;
    public final Runnable mUpdateChildViewsRunnable;
    public VelocityTracker mVelocityTracker;
    public final c0 mViewFlinger;
    public final ViewInfoStore.b mViewInfoProcessCallback;
    public final ViewInfoStore mViewInfoStore;

    public class a implements Runnable {
        public a() {
        }

        public void run() {
            RecyclerView recyclerView = RecyclerView.this;
            if (recyclerView.mFirstLayoutComplete && !recyclerView.isLayoutRequested()) {
                RecyclerView recyclerView2 = RecyclerView.this;
                if (!recyclerView2.mIsAttached) {
                    recyclerView2.requestLayout();
                } else if (recyclerView2.mLayoutSuppressed) {
                    recyclerView2.mLayoutWasDefered = true;
                } else {
                    recyclerView2.consumePendingUpdateOperations();
                }
            }
        }
    }

    public class b implements Runnable {
        public b() {
        }

        public void run() {
            l lVar = RecyclerView.this.mItemAnimator;
            if (lVar != null) {
                DefaultItemAnimator7 defaultItemAnimator7 = (DefaultItemAnimator7) lVar;
                boolean z = !defaultItemAnimator7.h.isEmpty();
                boolean z2 = !defaultItemAnimator7.f1370j.isEmpty();
                boolean z3 = !defaultItemAnimator7.f1371k.isEmpty();
                boolean z4 = !defaultItemAnimator7.f1369i.isEmpty();
                if (z || z2 || z4 || z3) {
                    Iterator<d0> it = defaultItemAnimator7.h.iterator();
                    while (it.hasNext()) {
                        d0 next = it.next();
                        View view = next.a;
                        ViewPropertyAnimator animate = view.animate();
                        defaultItemAnimator7.f1377q.add(next);
                        animate.setDuration(defaultItemAnimator7.d).alpha(0.0f).setListener(new DefaultItemAnimator2(defaultItemAnimator7, next, animate, view)).start();
                    }
                    defaultItemAnimator7.h.clear();
                    if (z2) {
                        ArrayList arrayList = new ArrayList();
                        arrayList.addAll(defaultItemAnimator7.f1370j);
                        defaultItemAnimator7.f1373m.add(arrayList);
                        defaultItemAnimator7.f1370j.clear();
                        DefaultItemAnimator defaultItemAnimator = new DefaultItemAnimator(defaultItemAnimator7, arrayList);
                        if (z) {
                            ViewCompat.a(((DefaultItemAnimator7.b) arrayList.get(0)).a.a, defaultItemAnimator, defaultItemAnimator7.d);
                        } else {
                            defaultItemAnimator.run();
                        }
                    }
                    if (z3) {
                        ArrayList arrayList2 = new ArrayList();
                        arrayList2.addAll(defaultItemAnimator7.f1371k);
                        defaultItemAnimator7.f1374n.add(arrayList2);
                        defaultItemAnimator7.f1371k.clear();
                        DefaultItemAnimator0 defaultItemAnimator0 = new DefaultItemAnimator0(defaultItemAnimator7, arrayList2);
                        if (z) {
                            ViewCompat.a(((DefaultItemAnimator7.a) arrayList2.get(0)).a.a, defaultItemAnimator0, defaultItemAnimator7.d);
                        } else {
                            defaultItemAnimator0.run();
                        }
                    }
                    if (z4) {
                        ArrayList arrayList3 = new ArrayList();
                        arrayList3.addAll(defaultItemAnimator7.f1369i);
                        defaultItemAnimator7.f1372l.add(arrayList3);
                        defaultItemAnimator7.f1369i.clear();
                        DefaultItemAnimator1 defaultItemAnimator1 = new DefaultItemAnimator1(defaultItemAnimator7, arrayList3);
                        if (z || z2 || z3) {
                            long j2 = 0;
                            long j3 = z ? defaultItemAnimator7.d : 0;
                            long j4 = z2 ? defaultItemAnimator7.f314e : 0;
                            if (z3) {
                                j2 = defaultItemAnimator7.f315f;
                            }
                            ViewCompat.a(((d0) arrayList3.get(0)).a, defaultItemAnimator1, Math.max(j4, j2) + j3);
                        } else {
                            defaultItemAnimator1.run();
                        }
                    }
                }
            }
            RecyclerView.this.mPostedAnimatorRunner = false;
        }
    }

    public static abstract class b0 {
    }

    public static class c implements Interpolator {
        public float getInterpolation(float f2) {
            float f3 = f2 - 1.0f;
            return (f3 * f3 * f3 * f3 * f3) + 1.0f;
        }
    }

    public class d implements ViewInfoStore.b {
        public d() {
        }
    }

    public static abstract class d0 {

        /* renamed from: s  reason: collision with root package name */
        public static final List<Object> f301s = Collections.emptyList();
        public final View a;
        public WeakReference<RecyclerView> b;
        public int c = -1;
        public int d = -1;

        /* renamed from: e  reason: collision with root package name */
        public long f302e = -1;

        /* renamed from: f  reason: collision with root package name */
        public int f303f = -1;
        public int g = -1;
        public d0 h = null;

        /* renamed from: i  reason: collision with root package name */
        public d0 f304i = null;

        /* renamed from: j  reason: collision with root package name */
        public int f305j;

        /* renamed from: k  reason: collision with root package name */
        public List<Object> f306k = null;

        /* renamed from: l  reason: collision with root package name */
        public List<Object> f307l = null;

        /* renamed from: m  reason: collision with root package name */
        public int f308m = 0;

        /* renamed from: n  reason: collision with root package name */
        public v f309n = null;

        /* renamed from: o  reason: collision with root package name */
        public boolean f310o = false;

        /* renamed from: p  reason: collision with root package name */
        public int f311p = 0;

        /* renamed from: q  reason: collision with root package name */
        public int f312q = -1;

        /* renamed from: r  reason: collision with root package name */
        public RecyclerView f313r;

        public d0(View view) {
            if (view != null) {
                this.a = view;
                return;
            }
            throw new IllegalArgumentException("itemView may not be null");
        }

        public void a(int i2, boolean z) {
            if (this.d == -1) {
                this.d = this.c;
            }
            if (this.g == -1) {
                this.g = this.c;
            }
            if (z) {
                this.g += i2;
            }
            this.c += i2;
            if (this.a.getLayoutParams() != null) {
                ((p) this.a.getLayoutParams()).c = true;
            }
        }

        public void b() {
            this.f305j &= -33;
        }

        public final int c() {
            RecyclerView recyclerView = this.f313r;
            if (recyclerView == null) {
                return -1;
            }
            return recyclerView.getAdapterPositionFor(this);
        }

        public final int d() {
            int i2 = this.g;
            return i2 == -1 ? this.c : i2;
        }

        public List<Object> e() {
            if ((this.f305j & 1024) != 0) {
                return f301s;
            }
            List<Object> list = this.f306k;
            if (list == null || list.size() == 0) {
                return f301s;
            }
            return this.f307l;
        }

        public boolean f() {
            return (this.a.getParent() == null || this.a.getParent() == this.f313r) ? false : true;
        }

        public boolean g() {
            return (this.f305j & 1) != 0;
        }

        public boolean h() {
            return (this.f305j & 4) != 0;
        }

        public final boolean i() {
            return (this.f305j & 16) == 0 && !ViewCompat.u(this.a);
        }

        public boolean j() {
            return (this.f305j & 8) != 0;
        }

        public boolean k() {
            return this.f309n != null;
        }

        public boolean l() {
            return (this.f305j & 256) != 0;
        }

        public boolean m() {
            return (this.f305j & 2) != 0;
        }

        public void n() {
            this.f305j = 0;
            this.c = -1;
            this.d = -1;
            this.f302e = -1;
            this.g = -1;
            this.f308m = 0;
            this.h = null;
            this.f304i = null;
            List<Object> list = this.f306k;
            if (list != null) {
                list.clear();
            }
            this.f305j &= -1025;
            this.f311p = 0;
            this.f312q = -1;
            RecyclerView.clearNestedRecyclerViewIfNotNested(this);
        }

        public boolean o() {
            return (this.f305j & 128) != 0;
        }

        public boolean p() {
            return (this.f305j & 32) != 0;
        }

        public String toString() {
            StringBuilder b2 = outline.b(getClass().isAnonymousClass() ? "ViewHolder" : getClass().getSimpleName(), "{");
            b2.append(Integer.toHexString(hashCode()));
            b2.append(" position=");
            b2.append(this.c);
            b2.append(" id=");
            b2.append(this.f302e);
            b2.append(", oldPos=");
            b2.append(this.d);
            b2.append(", pLpos:");
            b2.append(this.g);
            StringBuilder sb = new StringBuilder(b2.toString());
            if (k()) {
                sb.append(" scrap ");
                sb.append(this.f310o ? "[changeScrap]" : "[attachedScrap]");
            }
            if (h()) {
                sb.append(" invalid");
            }
            if (!g()) {
                sb.append(" unbound");
            }
            boolean z = false;
            if ((this.f305j & 2) != 0) {
                sb.append(" update");
            }
            if (j()) {
                sb.append(" removed");
            }
            if (o()) {
                sb.append(" ignored");
            }
            if (l()) {
                sb.append(" tmpDetached");
            }
            if (!i()) {
                StringBuilder a2 = outline.a(" not recyclable(");
                a2.append(this.f308m);
                a2.append(")");
                sb.append(a2.toString());
            }
            if ((this.f305j & 512) != 0 || h()) {
                z = true;
            }
            if (z) {
                sb.append(" undefined adapter position");
            }
            if (this.a.getParent() == null) {
                sb.append(" no parent");
            }
            sb.append("}");
            return sb.toString();
        }

        public boolean b(int i2) {
            return (i2 & this.f305j) != 0;
        }

        public void a() {
            this.d = -1;
            this.g = -1;
        }

        public void a(int i2, int i3) {
            this.f305j = (i2 & i3) | (this.f305j & (~i3));
        }

        public void a(int i2) {
            this.f305j = i2 | this.f305j;
        }

        public void a(Object obj) {
            if (obj == null) {
                a(1024);
            } else if ((1024 & this.f305j) == 0) {
                if (this.f306k == null) {
                    ArrayList arrayList = new ArrayList();
                    this.f306k = arrayList;
                    this.f307l = Collections.unmodifiableList(arrayList);
                }
                this.f306k.add(obj);
            }
        }

        public final void a(boolean z) {
            int i2 = this.f308m;
            int i3 = z ? i2 - 1 : i2 + 1;
            this.f308m = i3;
            if (i3 < 0) {
                this.f308m = 0;
                Log.e("View", "isRecyclable decremented below 0: unmatched pair of setIsRecyable() calls for " + this);
            } else if (!z && i3 == 1) {
                this.f305j |= 16;
            } else if (z && this.f308m == 0) {
                this.f305j &= -17;
            }
        }
    }

    public class e implements ChildHelper.b {
        public e() {
        }

        public int a() {
            return RecyclerView.this.getChildCount();
        }

        public void b(int i2) {
            View childAt = RecyclerView.this.getChildAt(i2);
            if (childAt != null) {
                RecyclerView.this.dispatchChildDetached(childAt);
                childAt.clearAnimation();
            }
            RecyclerView.this.removeViewAt(i2);
        }

        public View a(int i2) {
            return RecyclerView.this.getChildAt(i2);
        }
    }

    public static abstract class g<VH extends d0> {
        public final h a = new h();
        public boolean b = false;

        public abstract int a();

        public long a(int i2) {
            return -1;
        }

        public abstract VH a(ViewGroup viewGroup, int i2);

        public abstract void a(VH vh, int i2);
    }

    public static class h extends Observable<i> {
        public boolean a() {
            return !super.mObservers.isEmpty();
        }

        public void b() {
            for (int size = super.mObservers.size() - 1; size >= 0; size--) {
                x xVar = (x) ((i) super.mObservers.get(size));
                RecyclerView.this.assertNotInLayoutOrScroll(null);
                RecyclerView recyclerView = RecyclerView.this;
                recyclerView.mState.f291f = true;
                recyclerView.processDataSetCompletelyChanged(true);
                if (!RecyclerView.this.mAdapterHelper.c()) {
                    RecyclerView.this.requestLayout();
                }
            }
        }
    }

    public static abstract class i {
    }

    public interface j {
        int a(int i2, int i3);
    }

    public static class k {
        public EdgeEffect a(RecyclerView recyclerView) {
            return new EdgeEffect(recyclerView.getContext());
        }
    }

    public class m implements l.b {
        public m() {
        }
    }

    public static abstract class n {
        public void a(Canvas canvas, RecyclerView recyclerView, a0 a0Var) {
        }

        public void b(Canvas canvas, RecyclerView recyclerView, a0 a0Var) {
        }
    }

    public static abstract class o {
        public ChildHelper a;
        public RecyclerView b;
        public final ViewBoundsCheck.b c = new a();
        public final ViewBoundsCheck.b d = new b();

        /* renamed from: e  reason: collision with root package name */
        public ViewBoundsCheck f316e = new ViewBoundsCheck(this.c);

        /* renamed from: f  reason: collision with root package name */
        public ViewBoundsCheck f317f = new ViewBoundsCheck(this.d);
        public z g;
        public boolean h = false;

        /* renamed from: i  reason: collision with root package name */
        public boolean f318i = false;

        /* renamed from: j  reason: collision with root package name */
        public boolean f319j = false;

        /* renamed from: k  reason: collision with root package name */
        public boolean f320k = true;

        /* renamed from: l  reason: collision with root package name */
        public boolean f321l = true;

        /* renamed from: m  reason: collision with root package name */
        public int f322m;

        /* renamed from: n  reason: collision with root package name */
        public boolean f323n;

        /* renamed from: o  reason: collision with root package name */
        public int f324o;

        /* renamed from: p  reason: collision with root package name */
        public int f325p;

        /* renamed from: q  reason: collision with root package name */
        public int f326q;

        /* renamed from: r  reason: collision with root package name */
        public int f327r;

        public class a implements ViewBoundsCheck.b {
            public a() {
            }

            public View a(int i2) {
                return o.this.c(i2);
            }

            public int b() {
                return o.this.j();
            }

            public int a() {
                o oVar = o.this;
                return oVar.f326q - oVar.k();
            }

            public int b(View view) {
                return o.this.d(view) - ((p) view.getLayoutParams()).leftMargin;
            }

            public int a(View view) {
                return o.this.g(view) + ((p) view.getLayoutParams()).rightMargin;
            }
        }

        public class b implements ViewBoundsCheck.b {
            public b() {
            }

            public View a(int i2) {
                return o.this.c(i2);
            }

            public int b() {
                return o.this.l();
            }

            public int a() {
                o oVar = o.this;
                return oVar.f327r - oVar.i();
            }

            public int b(View view) {
                return o.this.h(view) - ((p) view.getLayoutParams()).topMargin;
            }

            public int a(View view) {
                return o.this.c(view) + ((p) view.getLayoutParams()).bottomMargin;
            }
        }

        public interface c {
        }

        public static class d {
            public int a;
            public int b;
            public boolean c;
            public boolean d;
        }

        public int a(int i2, v vVar, a0 a0Var) {
            return 0;
        }

        public int a(a0 a0Var) {
            return 0;
        }

        public View a(View view, int i2, v vVar, a0 a0Var) {
            return null;
        }

        public void a(int i2, int i3) {
            this.f326q = View.MeasureSpec.getSize(i2);
            int mode = View.MeasureSpec.getMode(i2);
            this.f324o = mode;
            if (mode == 0 && !RecyclerView.ALLOW_SIZE_IN_UNSPECIFIED_SPEC) {
                this.f326q = 0;
            }
            this.f327r = View.MeasureSpec.getSize(i3);
            int mode2 = View.MeasureSpec.getMode(i3);
            this.f325p = mode2;
            if (mode2 == 0 && !RecyclerView.ALLOW_SIZE_IN_UNSPECIFIED_SPEC) {
                this.f327r = 0;
            }
        }

        public void a(int i2, int i3, a0 a0Var, c cVar) {
        }

        public void a(int i2, c cVar) {
        }

        public void a(Parcelable parcelable) {
        }

        public void a(RecyclerView recyclerView) {
        }

        public void a(RecyclerView recyclerView, int i2, int i3) {
        }

        public void a(RecyclerView recyclerView, int i2, int i3, int i4) {
        }

        public void a(RecyclerView recyclerView, int i2, int i3, Object obj) {
        }

        public void a(RecyclerView recyclerView, v vVar) {
        }

        public boolean a() {
            return false;
        }

        public boolean a(p pVar) {
            return pVar != null;
        }

        public int b(int i2, v vVar, a0 a0Var) {
            return 0;
        }

        public int b(a0 a0Var) {
            return 0;
        }

        public void b(int i2, int i3) {
            int d2 = d();
            if (d2 == 0) {
                this.b.defaultOnMeasure(i2, i3);
                return;
            }
            int i4 = Integer.MAX_VALUE;
            int i5 = Integer.MAX_VALUE;
            int i6 = RecyclerView.UNDEFINED_DURATION;
            int i7 = RecyclerView.UNDEFINED_DURATION;
            for (int i8 = 0; i8 < d2; i8++) {
                View c2 = c(i8);
                Rect rect = this.b.mTempRect;
                RecyclerView.getDecoratedBoundsWithMarginsInt(c2, rect);
                int i9 = rect.left;
                if (i9 < i4) {
                    i4 = i9;
                }
                int i10 = rect.right;
                if (i10 > i6) {
                    i6 = i10;
                }
                int i11 = rect.top;
                if (i11 < i5) {
                    i5 = i11;
                }
                int i12 = rect.bottom;
                if (i12 > i7) {
                    i7 = i12;
                }
            }
            this.b.mTempRect.set(i4, i5, i6, i7);
            a(this.b.mTempRect, i2, i3);
        }

        public void b(RecyclerView recyclerView, int i2, int i3) {
        }

        public boolean b() {
            return false;
        }

        public int c(a0 a0Var) {
            return 0;
        }

        public abstract p c();

        public void c(v vVar) {
            int size = vVar.a.size();
            for (int i2 = size - 1; i2 >= 0; i2--) {
                View view = vVar.a.get(i2).a;
                d0 childViewHolderInt = RecyclerView.getChildViewHolderInt(view);
                if (!childViewHolderInt.o()) {
                    childViewHolderInt.a(false);
                    if (childViewHolderInt.l()) {
                        this.b.removeDetachedView(view, false);
                    }
                    l lVar = this.b.mItemAnimator;
                    if (lVar != null) {
                        lVar.b(childViewHolderInt);
                    }
                    childViewHolderInt.a(true);
                    d0 childViewHolderInt2 = RecyclerView.getChildViewHolderInt(view);
                    childViewHolderInt2.f309n = null;
                    childViewHolderInt2.f310o = false;
                    childViewHolderInt2.b();
                    vVar.a(childViewHolderInt2);
                }
            }
            vVar.a.clear();
            ArrayList<d0> arrayList = vVar.b;
            if (arrayList != null) {
                arrayList.clear();
            }
            if (size > 0) {
                this.b.invalidate();
            }
        }

        public int d() {
            ChildHelper childHelper = this.a;
            if (childHelper != null) {
                return childHelper.a();
            }
            return 0;
        }

        public int d(a0 a0Var) {
            return 0;
        }

        public int e(a0 a0Var) {
            return 0;
        }

        public View e() {
            View focusedChild;
            RecyclerView recyclerView = this.b;
            if (recyclerView == null || (focusedChild = recyclerView.getFocusedChild()) == null || this.a.c.contains(focusedChild)) {
                return null;
            }
            return focusedChild;
        }

        public int f() {
            return ViewCompat.k(this.b);
        }

        public int f(a0 a0Var) {
            return 0;
        }

        public void f(int i2) {
        }

        public void g(int i2) {
            ChildHelper childHelper;
            int c2;
            View a2;
            if (c(i2) != null && (a2 = ((e) childHelper.a).a((c2 = (childHelper = this.a).c(i2)))) != null) {
                if (childHelper.b.d(c2)) {
                    childHelper.d(a2);
                }
                ((e) childHelper.a).b(c2);
            }
        }

        public void g(a0 a0Var) {
        }

        public int h(View view) {
            return view.getTop() - ((p) view.getLayoutParams()).b.top;
        }

        public void h(int i2) {
        }

        public int i(View view) {
            return ((p) view.getLayoutParams()).a();
        }

        public int j() {
            RecyclerView recyclerView = this.b;
            if (recyclerView != null) {
                return recyclerView.getPaddingLeft();
            }
            return 0;
        }

        public int k() {
            RecyclerView recyclerView = this.b;
            if (recyclerView != null) {
                return recyclerView.getPaddingRight();
            }
            return 0;
        }

        public int l() {
            RecyclerView recyclerView = this.b;
            if (recyclerView != null) {
                return recyclerView.getPaddingTop();
            }
            return 0;
        }

        public boolean m() {
            return this.f319j;
        }

        public Parcelable n() {
            return null;
        }

        public void o() {
            RecyclerView recyclerView = this.b;
            if (recyclerView != null) {
                recyclerView.requestLayout();
            }
        }

        public boolean p() {
            return false;
        }

        public boolean q() {
            return false;
        }

        public void d(int i2) {
            RecyclerView recyclerView = this.b;
            if (recyclerView != null) {
                recyclerView.offsetChildrenHorizontal(i2);
            }
        }

        public int f(View view) {
            Rect rect = ((p) view.getLayoutParams()).b;
            return view.getMeasuredWidth() + rect.left + rect.right;
        }

        public int i() {
            RecyclerView recyclerView = this.b;
            if (recyclerView != null) {
                return recyclerView.getPaddingBottom();
            }
            return 0;
        }

        public int h() {
            return ViewCompat.m(this.b);
        }

        public int d(View view) {
            return view.getLeft() - ((p) view.getLayoutParams()).b.left;
        }

        public void e(int i2) {
            RecyclerView recyclerView = this.b;
            if (recyclerView != null) {
                recyclerView.offsetChildrenVertical(i2);
            }
        }

        public int e(View view) {
            Rect rect = ((p) view.getLayoutParams()).b;
            return view.getMeasuredHeight() + rect.top + rect.bottom;
        }

        public int g(View view) {
            return view.getRight() + ((p) view.getLayoutParams()).b.right;
        }

        public void a(Rect rect, int i2, int i3) {
            int k2 = k() + j() + rect.width();
            int i4 = i() + l() + rect.height();
            this.b.setMeasuredDimension(a(i2, k2, h()), a(i3, i4, g()));
        }

        public int g() {
            return ViewCompat.l(this.b);
        }

        public View b(View view) {
            View findContainingItemView;
            RecyclerView recyclerView = this.b;
            if (recyclerView == null || (findContainingItemView = recyclerView.findContainingItemView(view)) == null || this.a.c.contains(findContainingItemView)) {
                return null;
            }
            return findContainingItemView;
        }

        public static int a(int i2, int i3, int i4) {
            int mode = View.MeasureSpec.getMode(i2);
            int size = View.MeasureSpec.getSize(i2);
            if (mode != Integer.MIN_VALUE) {
                return mode != 1073741824 ? Math.max(i3, i4) : size;
            }
            return Math.min(size, Math.max(i3, i4));
        }

        public View b(int i2) {
            int d2 = d();
            for (int i3 = 0; i3 < d2; i3++) {
                View c2 = c(i3);
                d0 childViewHolderInt = RecyclerView.getChildViewHolderInt(c2);
                if (childViewHolderInt != null && childViewHolderInt.d() == i2 && !childViewHolderInt.o() && (this.b.mState.g || !childViewHolderInt.j())) {
                    return c2;
                }
            }
            return null;
        }

        public void a(String str) {
            RecyclerView recyclerView = this.b;
            if (recyclerView != null) {
                recyclerView.assertNotInLayoutOrScroll(str);
            }
        }

        public p a(ViewGroup.LayoutParams layoutParams) {
            if (layoutParams instanceof p) {
                return new p((p) layoutParams);
            }
            if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
                return new p((ViewGroup.MarginLayoutParams) layoutParams);
            }
            return new p(layoutParams);
        }

        public void c(RecyclerView recyclerView) {
            if (recyclerView == null) {
                this.b = null;
                this.a = null;
                this.f326q = 0;
                this.f327r = 0;
            } else {
                this.b = recyclerView;
                this.a = recyclerView.mChildHelper;
                this.f326q = recyclerView.getWidth();
                this.f327r = recyclerView.getHeight();
            }
            this.f324o = 1073741824;
            this.f325p = 1073741824;
        }

        public boolean b(View view, int i2, int i3, p pVar) {
            return !this.f320k || !b(view.getMeasuredWidth(), i2, pVar.width) || !b(view.getMeasuredHeight(), i3, pVar.height);
        }

        public static boolean b(int i2, int i3, int i4) {
            int mode = View.MeasureSpec.getMode(i3);
            int size = View.MeasureSpec.getSize(i3);
            if (i4 > 0 && i2 != i4) {
                return false;
            }
            if (mode == Integer.MIN_VALUE) {
                return size >= i2;
            }
            if (mode != 0) {
                return mode == 1073741824 && size == i2;
            }
            return true;
        }

        public p a(Context context, AttributeSet attributeSet) {
            return new p(context, attributeSet);
        }

        public void a(RecyclerView recyclerView, a0 a0Var, int i2) {
            Log.e(RecyclerView.TAG, "You must override smoothScrollToPosition to support smooth scrolling");
        }

        public void a(z zVar) {
            z zVar2 = this.g;
            if (!(zVar2 == null || zVar == zVar2 || !zVar2.f330e)) {
                zVar2.a();
            }
            this.g = zVar;
            RecyclerView recyclerView = this.b;
            if (zVar != null) {
                recyclerView.mViewFlinger.b();
                if (zVar.h) {
                    StringBuilder a2 = outline.a("An instance of ");
                    a2.append(zVar.getClass().getSimpleName());
                    a2.append(" was started more than once. Each instance of");
                    a2.append(zVar.getClass().getSimpleName());
                    a2.append(" is intended to only be used once. You should create a new instance for each use.");
                    Log.w(RecyclerView.TAG, a2.toString());
                }
                zVar.b = recyclerView;
                zVar.c = this;
                int i2 = zVar.a;
                if (i2 != -1) {
                    recyclerView.mState.a = i2;
                    zVar.f330e = true;
                    zVar.d = true;
                    zVar.f331f = recyclerView.mLayout.b(i2);
                    zVar.b.mViewFlinger.a();
                    zVar.h = true;
                    return;
                }
                throw new IllegalArgumentException("Invalid target position");
            }
            throw null;
        }

        public void b(v vVar) {
            for (int d2 = d() - 1; d2 >= 0; d2--) {
                if (!RecyclerView.getChildViewHolderInt(c(d2)).o()) {
                    a(d2, vVar);
                }
            }
        }

        public void c(v vVar, a0 a0Var) {
            Log.e(RecyclerView.TAG, "You must override onLayoutChildren(Recycler recycler, State state) ");
        }

        public int b(v vVar, a0 a0Var) {
            RecyclerView recyclerView = this.b;
            if (recyclerView == null || recyclerView.mAdapter == null || !b()) {
                return 1;
            }
            return this.b.mAdapter.a();
        }

        public View c(int i2) {
            ChildHelper childHelper = this.a;
            if (childHelper == null) {
                return null;
            }
            return ((e) childHelper.a).a(childHelper.c(i2));
        }

        public void b(RecyclerView recyclerView) {
            a(View.MeasureSpec.makeMeasureSpec(recyclerView.getWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(recyclerView.getHeight(), 1073741824));
        }

        public int c(View view) {
            return view.getBottom() + ((p) view.getLayoutParams()).b.bottom;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, boolean):void
         arg types: [android.view.View, int, int]
         candidates:
          androidx.recyclerview.widget.RecyclerView.o.a(int, int, int):int
          androidx.recyclerview.widget.RecyclerView.o.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$a0):int
          androidx.recyclerview.widget.RecyclerView.o.a(android.graphics.Rect, int, int):void
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, android.graphics.Rect):void
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int):void
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$a0, int):void
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, boolean):void */
        public void a(View view) {
            a(view, -1, false);
        }

        public final void a(View view, int i2, boolean z) {
            d0 childViewHolderInt = RecyclerView.getChildViewHolderInt(view);
            if (z || childViewHolderInt.j()) {
                this.b.mViewInfoStore.a(childViewHolderInt);
            } else {
                this.b.mViewInfoStore.c(childViewHolderInt);
            }
            p pVar = (p) view.getLayoutParams();
            if (childViewHolderInt.p() || childViewHolderInt.k()) {
                if (childViewHolderInt.k()) {
                    childViewHolderInt.f309n.b(childViewHolderInt);
                } else {
                    childViewHolderInt.b();
                }
                this.a.a(view, i2, view.getLayoutParams(), false);
            } else if (view.getParent() == this.b) {
                int b2 = this.a.b(view);
                if (i2 == -1) {
                    i2 = this.a.a();
                }
                if (b2 == -1) {
                    StringBuilder a2 = outline.a("Added View has RecyclerView as parent but view is not a real child. Unfiltered index:");
                    a2.append(this.b.indexOfChild(view));
                    throw new IllegalStateException(outline.a(this.b, a2));
                } else if (b2 != i2) {
                    o oVar = this.b.mLayout;
                    View c2 = oVar.c(b2);
                    if (c2 != null) {
                        oVar.c(b2);
                        oVar.a.a(b2);
                        p pVar2 = (p) c2.getLayoutParams();
                        d0 childViewHolderInt2 = RecyclerView.getChildViewHolderInt(c2);
                        if (childViewHolderInt2.j()) {
                            oVar.b.mViewInfoStore.a(childViewHolderInt2);
                        } else {
                            oVar.b.mViewInfoStore.c(childViewHolderInt2);
                        }
                        oVar.a.a(c2, i2, pVar2, childViewHolderInt2.j());
                    } else {
                        throw new IllegalArgumentException("Cannot move a child from non-existing index:" + b2 + oVar.b.toString());
                    }
                }
            } else {
                this.a.a(view, i2, false);
                pVar.c = true;
                z zVar = this.g;
                if (zVar != null && zVar.f330e && zVar.b.getChildLayoutPosition(view) == zVar.a) {
                    zVar.f331f = view;
                }
            }
            if (pVar.d) {
                childViewHolderInt.a.invalidate();
                pVar.d = false;
            }
        }

        public void a(View view, v vVar) {
            ChildHelper childHelper = this.a;
            int indexOfChild = RecyclerView.this.indexOfChild(view);
            if (indexOfChild >= 0) {
                if (childHelper.b.d(indexOfChild)) {
                    childHelper.d(view);
                }
                ((e) childHelper.a).b(indexOfChild);
            }
            vVar.a(view);
        }

        public void a(int i2, v vVar) {
            View c2 = c(i2);
            g(i2);
            vVar.a(c2);
        }

        public void a(v vVar) {
            for (int d2 = d() - 1; d2 >= 0; d2--) {
                View c2 = c(d2);
                d0 childViewHolderInt = RecyclerView.getChildViewHolderInt(c2);
                if (!childViewHolderInt.o()) {
                    if (!childViewHolderInt.h() || childViewHolderInt.j() || this.b.mAdapter.b) {
                        c(d2);
                        this.a.a(d2);
                        vVar.b(c2);
                        this.b.mViewInfoStore.c(childViewHolderInt);
                    } else {
                        g(d2);
                        vVar.a(childViewHolderInt);
                    }
                }
            }
        }

        public boolean a(View view, int i2, int i3, p pVar) {
            return view.isLayoutRequested() || !this.f320k || !b(view.getWidth(), i2, pVar.width) || !b(view.getHeight(), i3, pVar.height);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:6:0x0017, code lost:
            if (r5 == 1073741824) goto L_0x0021;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public static int a(int r4, int r5, int r6, int r7, boolean r8) {
            /*
                int r4 = r4 - r6
                r6 = 0
                int r4 = java.lang.Math.max(r6, r4)
                r0 = -2
                r1 = -1
                r2 = -2147483648(0xffffffff80000000, float:-0.0)
                r3 = 1073741824(0x40000000, float:2.0)
                if (r8 == 0) goto L_0x001a
                if (r7 < 0) goto L_0x0011
                goto L_0x001c
            L_0x0011:
                if (r7 != r1) goto L_0x002f
                if (r5 == r2) goto L_0x0021
                if (r5 == 0) goto L_0x002f
                if (r5 == r3) goto L_0x0021
                goto L_0x002f
            L_0x001a:
                if (r7 < 0) goto L_0x001f
            L_0x001c:
                r5 = 1073741824(0x40000000, float:2.0)
                goto L_0x0031
            L_0x001f:
                if (r7 != r1) goto L_0x0023
            L_0x0021:
                r7 = r4
                goto L_0x0031
            L_0x0023:
                if (r7 != r0) goto L_0x002f
                if (r5 == r2) goto L_0x002c
                if (r5 != r3) goto L_0x002a
                goto L_0x002c
            L_0x002a:
                r5 = 0
                goto L_0x0021
            L_0x002c:
                r5 = -2147483648(0xffffffff80000000, float:-0.0)
                goto L_0x0021
            L_0x002f:
                r5 = 0
                r7 = 0
            L_0x0031:
                int r4 = android.view.View.MeasureSpec.makeMeasureSpec(r7, r5)
                return r4
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.o.a(int, int, int, int, boolean):int");
        }

        public void a(View view, int i2, int i3, int i4, int i5) {
            p pVar = (p) view.getLayoutParams();
            Rect rect = pVar.b;
            view.layout(i2 + rect.left + pVar.leftMargin, i3 + rect.top + pVar.topMargin, (i4 - rect.right) - pVar.rightMargin, (i5 - rect.bottom) - pVar.bottomMargin);
        }

        public void a(View view, boolean z, Rect rect) {
            Matrix matrix;
            if (z) {
                Rect rect2 = ((p) view.getLayoutParams()).b;
                rect.set(-rect2.left, -rect2.top, view.getWidth() + rect2.right, view.getHeight() + rect2.bottom);
            } else {
                rect.set(0, 0, view.getWidth(), view.getHeight());
            }
            if (!(this.b == null || (matrix = view.getMatrix()) == null || matrix.isIdentity())) {
                RectF rectF = this.b.mTempRectF;
                rectF.set(rect);
                matrix.mapRect(rectF);
                rect.set((int) Math.floor((double) rectF.left), (int) Math.floor((double) rectF.top), (int) Math.ceil((double) rectF.right), (int) Math.ceil((double) rectF.bottom));
            }
            rect.offset(view.getLeft(), view.getTop());
        }

        /* JADX WARNING: Code restructure failed: missing block: B:23:0x00b3, code lost:
            if (r14 == false) goto L_0x00ba;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean a(androidx.recyclerview.widget.RecyclerView r10, android.view.View r11, android.graphics.Rect r12, boolean r13, boolean r14) {
            /*
                r9 = this;
                r0 = 2
                int[] r0 = new int[r0]
                int r1 = r9.j()
                int r2 = r9.l()
                int r3 = r9.f326q
                int r4 = r9.k()
                int r3 = r3 - r4
                int r4 = r9.f327r
                int r5 = r9.i()
                int r4 = r4 - r5
                int r5 = r11.getLeft()
                int r6 = r12.left
                int r5 = r5 + r6
                int r6 = r11.getScrollX()
                int r5 = r5 - r6
                int r6 = r11.getTop()
                int r7 = r12.top
                int r6 = r6 + r7
                int r11 = r11.getScrollY()
                int r6 = r6 - r11
                int r11 = r12.width()
                int r11 = r11 + r5
                int r12 = r12.height()
                int r12 = r12 + r6
                int r5 = r5 - r1
                r1 = 0
                int r7 = java.lang.Math.min(r1, r5)
                int r6 = r6 - r2
                int r2 = java.lang.Math.min(r1, r6)
                int r11 = r11 - r3
                int r3 = java.lang.Math.max(r1, r11)
                int r12 = r12 - r4
                int r12 = java.lang.Math.max(r1, r12)
                int r4 = r9.f()
                r8 = 1
                if (r4 != r8) goto L_0x005f
                if (r3 == 0) goto L_0x005a
                goto L_0x0067
            L_0x005a:
                int r3 = java.lang.Math.max(r7, r11)
                goto L_0x0067
            L_0x005f:
                if (r7 == 0) goto L_0x0062
                goto L_0x0066
            L_0x0062:
                int r7 = java.lang.Math.min(r5, r3)
            L_0x0066:
                r3 = r7
            L_0x0067:
                if (r2 == 0) goto L_0x006a
                goto L_0x006e
            L_0x006a:
                int r2 = java.lang.Math.min(r6, r12)
            L_0x006e:
                r0[r1] = r3
                r0[r8] = r2
                r11 = r0[r1]
                r12 = r0[r8]
                if (r14 == 0) goto L_0x00b5
                android.view.View r14 = r10.getFocusedChild()
                if (r14 != 0) goto L_0x0080
            L_0x007e:
                r14 = 0
                goto L_0x00b3
            L_0x0080:
                int r0 = r9.j()
                int r2 = r9.l()
                int r3 = r9.f326q
                int r4 = r9.k()
                int r3 = r3 - r4
                int r4 = r9.f327r
                int r5 = r9.i()
                int r4 = r4 - r5
                androidx.recyclerview.widget.RecyclerView r5 = r9.b
                android.graphics.Rect r5 = r5.mTempRect
                androidx.recyclerview.widget.RecyclerView.getDecoratedBoundsWithMarginsInt(r14, r5)
                int r14 = r5.left
                int r14 = r14 - r11
                if (r14 >= r3) goto L_0x007e
                int r14 = r5.right
                int r14 = r14 - r11
                if (r14 <= r0) goto L_0x007e
                int r14 = r5.top
                int r14 = r14 - r12
                if (r14 >= r4) goto L_0x007e
                int r14 = r5.bottom
                int r14 = r14 - r12
                if (r14 > r2) goto L_0x00b2
                goto L_0x007e
            L_0x00b2:
                r14 = 1
            L_0x00b3:
                if (r14 == 0) goto L_0x00ba
            L_0x00b5:
                if (r11 != 0) goto L_0x00bb
                if (r12 == 0) goto L_0x00ba
                goto L_0x00bb
            L_0x00ba:
                return r1
            L_0x00bb:
                if (r13 == 0) goto L_0x00c1
                r10.scrollBy(r11, r12)
                goto L_0x00c4
            L_0x00c1:
                r10.smoothScrollBy(r11, r12)
            L_0x00c4:
                return r8
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean, boolean):boolean");
        }

        public void a(AccessibilityEvent accessibilityEvent) {
            RecyclerView recyclerView = this.b;
            v vVar = recyclerView.mRecycler;
            a0 a0Var = recyclerView.mState;
            if (recyclerView != null && accessibilityEvent != null) {
                boolean z = true;
                if (!recyclerView.canScrollVertically(1) && !this.b.canScrollVertically(-1) && !this.b.canScrollHorizontally(-1) && !this.b.canScrollHorizontally(1)) {
                    z = false;
                }
                accessibilityEvent.setScrollable(z);
                g gVar = this.b.mAdapter;
                if (gVar != null) {
                    accessibilityEvent.setItemCount(gVar.a());
                }
            }
        }

        public void a(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            d0 childViewHolderInt = RecyclerView.getChildViewHolderInt(view);
            if (childViewHolderInt != null && !childViewHolderInt.j() && !this.a.c(childViewHolderInt.a)) {
                RecyclerView recyclerView = this.b;
                a(recyclerView.mRecycler, recyclerView.mState, view, accessibilityNodeInfoCompat);
            }
        }

        public void a(v vVar, a0 a0Var, View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            accessibilityNodeInfoCompat.b(AccessibilityNodeInfoCompat.c.a(b() ? i(view) : 0, 1, a() ? i(view) : 0, 1, false, false));
        }

        public int a(v vVar, a0 a0Var) {
            RecyclerView recyclerView = this.b;
            if (recyclerView == null || recyclerView.mAdapter == null || !a()) {
                return 1;
            }
            return this.b.mAdapter.a();
        }

        public static d a(Context context, AttributeSet attributeSet, int i2, int i3) {
            d dVar = new d();
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, i.r.c.RecyclerView, i2, i3);
            dVar.a = obtainStyledAttributes.getInt(i.r.c.RecyclerView_android_orientation, 1);
            dVar.b = obtainStyledAttributes.getInt(i.r.c.RecyclerView_spanCount, 1);
            dVar.c = obtainStyledAttributes.getBoolean(i.r.c.RecyclerView_reverseLayout, false);
            dVar.d = obtainStyledAttributes.getBoolean(i.r.c.RecyclerView_stackFromEnd, false);
            obtainStyledAttributes.recycle();
            return dVar;
        }
    }

    public interface q {
        void a(View view);

        void b(View view);
    }

    public static abstract class r {
    }

    public interface s {
        void a(boolean z);

        boolean a(RecyclerView recyclerView, MotionEvent motionEvent);

        void b(RecyclerView recyclerView, MotionEvent motionEvent);
    }

    public static abstract class t {
        public void a(RecyclerView recyclerView, int i2) {
        }

        public void a(RecyclerView recyclerView, int i2, int i3) {
        }
    }

    public static class u {
        public SparseArray<a> a = new SparseArray<>();
        public int b = 0;

        public static class a {
            public final ArrayList<d0> a = new ArrayList<>();
            public int b = 5;
            public long c = 0;
            public long d = 0;
        }

        public long a(long j2, long j3) {
            if (j2 == 0) {
                return j3;
            }
            return (j3 / 4) + ((j2 / 4) * 3);
        }

        public final a a(int i2) {
            a aVar = this.a.get(i2);
            if (aVar != null) {
                return aVar;
            }
            a aVar2 = new a();
            this.a.put(i2, aVar2);
            return aVar2;
        }
    }

    public interface w {
        void a(d0 d0Var);
    }

    public class x extends i {
        public x() {
        }
    }

    static {
        Class<?> cls = Integer.TYPE;
        LAYOUT_MANAGER_CONSTRUCTOR_SIGNATURE = new Class[]{Context.class, AttributeSet.class, cls, cls};
    }

    public RecyclerView(Context context) {
        this(context, null);
    }

    private void addAnimatingView(d0 d0Var) {
        View view = d0Var.a;
        boolean z2 = view.getParent() == this;
        this.mRecycler.b(getChildViewHolder(view));
        if (d0Var.l()) {
            this.mChildHelper.a(view, -1, view.getLayoutParams(), true);
        } else if (!z2) {
            this.mChildHelper.a(view, -1, true);
        } else {
            ChildHelper childHelper = this.mChildHelper;
            int indexOfChild = RecyclerView.this.indexOfChild(view);
            if (indexOfChild >= 0) {
                childHelper.b.e(indexOfChild);
                childHelper.a(view);
                return;
            }
            throw new IllegalArgumentException("view is not a child, cannot hide " + view);
        }
    }

    private void animateChange(d0 d0Var, d0 d0Var2, l.c cVar, l.c cVar2, boolean z2, boolean z3) {
        d0Var.a(false);
        if (z2) {
            addAnimatingView(d0Var);
        }
        if (d0Var != d0Var2) {
            if (z3) {
                addAnimatingView(d0Var2);
            }
            d0Var.h = d0Var2;
            addAnimatingView(d0Var);
            this.mRecycler.b(d0Var);
            d0Var2.a(false);
            d0Var2.f304i = d0Var;
        }
        if (this.mItemAnimator.a(d0Var, d0Var2, cVar, cVar2)) {
            postAnimationRunner();
        }
    }

    private void cancelScroll() {
        resetScroll();
        setScrollState(0);
    }

    public static void clearNestedRecyclerViewIfNotNested(d0 d0Var) {
        WeakReference<RecyclerView> weakReference = d0Var.b;
        if (weakReference != null) {
            View view = weakReference.get();
            while (view != null) {
                if (view != d0Var.a) {
                    ViewParent parent = view.getParent();
                    view = parent instanceof View ? (View) parent : null;
                } else {
                    return;
                }
            }
            d0Var.b = null;
        }
    }

    private void createLayoutManager(Context context, String str, AttributeSet attributeSet, int i2, int i3) {
        ClassLoader classLoader;
        Constructor<? extends U> constructor;
        if (str != null) {
            String trim = str.trim();
            if (!trim.isEmpty()) {
                String fullClassName = getFullClassName(context, trim);
                try {
                    if (isInEditMode()) {
                        classLoader = getClass().getClassLoader();
                    } else {
                        classLoader = context.getClassLoader();
                    }
                    Class<? extends U> asSubclass = Class.forName(fullClassName, false, classLoader).asSubclass(o.class);
                    Object[] objArr = null;
                    try {
                        constructor = asSubclass.getConstructor(LAYOUT_MANAGER_CONSTRUCTOR_SIGNATURE);
                        objArr = new Object[]{context, attributeSet, Integer.valueOf(i2), Integer.valueOf(i3)};
                    } catch (NoSuchMethodException e2) {
                        constructor = asSubclass.getConstructor(new Class[0]);
                    }
                    constructor.setAccessible(true);
                    setLayoutManager((o) constructor.newInstance(objArr));
                } catch (NoSuchMethodException e3) {
                    e3.initCause(e2);
                    throw new IllegalStateException(attributeSet.getPositionDescription() + ": Error creating LayoutManager " + fullClassName, e3);
                } catch (ClassNotFoundException e4) {
                    throw new IllegalStateException(attributeSet.getPositionDescription() + ": Unable to find LayoutManager " + fullClassName, e4);
                } catch (InvocationTargetException e5) {
                    throw new IllegalStateException(attributeSet.getPositionDescription() + ": Could not instantiate the LayoutManager: " + fullClassName, e5);
                } catch (InstantiationException e6) {
                    throw new IllegalStateException(attributeSet.getPositionDescription() + ": Could not instantiate the LayoutManager: " + fullClassName, e6);
                } catch (IllegalAccessException e7) {
                    throw new IllegalStateException(attributeSet.getPositionDescription() + ": Cannot access non-public constructor " + fullClassName, e7);
                } catch (ClassCastException e8) {
                    throw new IllegalStateException(attributeSet.getPositionDescription() + ": Class is not a LayoutManager " + fullClassName, e8);
                }
            }
        }
    }

    private boolean didChildRangeChange(int i2, int i3) {
        findMinMaxChildLayoutPositions(this.mMinMaxLayoutPositions);
        int[] iArr = this.mMinMaxLayoutPositions;
        return (iArr[0] == i2 && iArr[1] == i3) ? false : true;
    }

    private void dispatchContentChangedIfNecessary() {
        int i2 = this.mEatenAccessibilityChangeFlags;
        this.mEatenAccessibilityChangeFlags = 0;
        if (i2 != 0 && isAccessibilityEnabled()) {
            AccessibilityEvent obtain = AccessibilityEvent.obtain();
            obtain.setEventType(2048);
            obtain.setContentChangeTypes(i2);
            sendAccessibilityEventUnchecked(obtain);
        }
    }

    private void dispatchLayoutStep1() {
        this.mState.a(1);
        fillRemainingScrollValues(this.mState);
        this.mState.f292i = false;
        startInterceptRequestLayout();
        ViewInfoStore viewInfoStore = this.mViewInfoStore;
        viewInfoStore.a.clear();
        viewInfoStore.b.b();
        onEnterLayoutOrScroll();
        processAdapterUpdatesAndSetAnimationFlags();
        saveFocusInfo();
        a0 a0Var = this.mState;
        a0Var.h = a0Var.f293j && this.mItemsChanged;
        this.mItemsChanged = false;
        this.mItemsAddedOrRemoved = false;
        a0 a0Var2 = this.mState;
        a0Var2.g = a0Var2.f294k;
        a0Var2.f290e = this.mAdapter.a();
        findMinMaxChildLayoutPositions(this.mMinMaxLayoutPositions);
        if (this.mState.f293j) {
            int a2 = this.mChildHelper.a();
            for (int i2 = 0; i2 < a2; i2++) {
                d0 childViewHolderInt = getChildViewHolderInt(this.mChildHelper.b(i2));
                if (!childViewHolderInt.o() && (!childViewHolderInt.h() || this.mAdapter.b)) {
                    l lVar = this.mItemAnimator;
                    l.d(childViewHolderInt);
                    childViewHolderInt.e();
                    this.mViewInfoStore.b(childViewHolderInt, lVar.c(childViewHolderInt));
                    if (this.mState.h && childViewHolderInt.m() && !childViewHolderInt.j() && !childViewHolderInt.o() && !childViewHolderInt.h()) {
                        this.mViewInfoStore.b.c(getChangedHolderKey(childViewHolderInt), childViewHolderInt);
                    }
                }
            }
        }
        if (this.mState.f294k) {
            saveOldPositions();
            a0 a0Var3 = this.mState;
            boolean z2 = a0Var3.f291f;
            a0Var3.f291f = false;
            this.mLayout.c(this.mRecycler, a0Var3);
            this.mState.f291f = z2;
            for (int i3 = 0; i3 < this.mChildHelper.a(); i3++) {
                d0 childViewHolderInt2 = getChildViewHolderInt(this.mChildHelper.b(i3));
                if (!childViewHolderInt2.o()) {
                    ViewInfoStore.a orDefault = this.mViewInfoStore.a.getOrDefault(childViewHolderInt2, null);
                    if (!((orDefault == null || (orDefault.a & 4) == 0) ? false : true)) {
                        l.d(childViewHolderInt2);
                        boolean b2 = childViewHolderInt2.b(8192);
                        l lVar2 = this.mItemAnimator;
                        childViewHolderInt2.e();
                        l.c c2 = lVar2.c(childViewHolderInt2);
                        if (b2) {
                            recordAnimationInfoIfBouncedHiddenView(childViewHolderInt2, c2);
                        } else {
                            ViewInfoStore viewInfoStore2 = this.mViewInfoStore;
                            ViewInfoStore.a orDefault2 = viewInfoStore2.a.getOrDefault(childViewHolderInt2, null);
                            if (orDefault2 == null) {
                                orDefault2 = ViewInfoStore.a.a();
                                viewInfoStore2.a.put(childViewHolderInt2, orDefault2);
                            }
                            orDefault2.a |= 2;
                            orDefault2.b = c2;
                        }
                    }
                }
            }
            clearOldPositions();
        } else {
            clearOldPositions();
        }
        onExitLayoutOrScroll();
        stopInterceptRequestLayout(false);
        this.mState.d = 2;
    }

    private void dispatchLayoutStep2() {
        startInterceptRequestLayout();
        onEnterLayoutOrScroll();
        this.mState.a(6);
        this.mAdapterHelper.b();
        this.mState.f290e = this.mAdapter.a();
        a0 a0Var = this.mState;
        a0Var.c = 0;
        a0Var.g = false;
        this.mLayout.c(this.mRecycler, a0Var);
        a0 a0Var2 = this.mState;
        a0Var2.f291f = false;
        this.mPendingSavedState = null;
        a0Var2.f293j = a0Var2.f293j && this.mItemAnimator != null;
        this.mState.d = 4;
        onExitLayoutOrScroll();
        stopInterceptRequestLayout(false);
    }

    private void dispatchLayoutStep3() {
        boolean z2;
        this.mState.a(4);
        startInterceptRequestLayout();
        onEnterLayoutOrScroll();
        a0 a0Var = this.mState;
        a0Var.d = 1;
        if (a0Var.f293j) {
            for (int a2 = this.mChildHelper.a() - 1; a2 >= 0; a2--) {
                d0 childViewHolderInt = getChildViewHolderInt(this.mChildHelper.b(a2));
                if (!childViewHolderInt.o()) {
                    long changedHolderKey = getChangedHolderKey(childViewHolderInt);
                    if (this.mItemAnimator != null) {
                        l.c cVar = new l.c();
                        View view = childViewHolderInt.a;
                        cVar.a = view.getLeft();
                        cVar.b = view.getTop();
                        view.getRight();
                        view.getBottom();
                        d0 b2 = this.mViewInfoStore.b.b(changedHolderKey, null);
                        if (b2 == null || b2.o()) {
                            this.mViewInfoStore.a(childViewHolderInt, cVar);
                        } else {
                            boolean b3 = this.mViewInfoStore.b(b2);
                            boolean b4 = this.mViewInfoStore.b(childViewHolderInt);
                            if (!b3 || b2 != childViewHolderInt) {
                                l.c a3 = this.mViewInfoStore.a(b2, 4);
                                this.mViewInfoStore.a(childViewHolderInt, cVar);
                                l.c a4 = this.mViewInfoStore.a(childViewHolderInt, 8);
                                if (a3 == null) {
                                    handleMissingPreInfoForChangeError(changedHolderKey, childViewHolderInt, b2);
                                } else {
                                    animateChange(b2, childViewHolderInt, a3, a4, b3, b4);
                                }
                            } else {
                                this.mViewInfoStore.a(childViewHolderInt, cVar);
                            }
                        }
                    } else {
                        throw null;
                    }
                }
            }
            ViewInfoStore viewInfoStore = this.mViewInfoStore;
            ViewInfoStore.b bVar = this.mViewInfoProcessCallback;
            int i2 = viewInfoStore.a.d;
            while (true) {
                i2--;
                if (i2 < 0) {
                    break;
                }
                d0 c2 = viewInfoStore.a.c(i2);
                ViewInfoStore.a d2 = viewInfoStore.a.d(i2);
                int i3 = d2.a;
                if ((i3 & 3) == 3) {
                    RecyclerView recyclerView = RecyclerView.this;
                    recyclerView.mLayout.a(c2.a, recyclerView.mRecycler);
                } else if ((i3 & 1) != 0) {
                    l.c cVar2 = d2.b;
                    if (cVar2 == null) {
                        RecyclerView recyclerView2 = RecyclerView.this;
                        recyclerView2.mLayout.a(c2.a, recyclerView2.mRecycler);
                    } else {
                        l.c cVar3 = d2.c;
                        d dVar = (d) bVar;
                        RecyclerView.this.mRecycler.b(c2);
                        RecyclerView.this.animateDisappearance(c2, cVar2, cVar3);
                    }
                } else if ((i3 & 14) == 14) {
                    RecyclerView.this.animateAppearance(c2, d2.b, d2.c);
                } else if ((i3 & 12) == 12) {
                    l.c cVar4 = d2.b;
                    l.c cVar5 = d2.c;
                    d dVar2 = (d) bVar;
                    if (dVar2 != null) {
                        c2.a(false);
                        RecyclerView recyclerView3 = RecyclerView.this;
                        if (!recyclerView3.mDataSetHasChangedAfterLayout) {
                            SimpleItemAnimator simpleItemAnimator = (SimpleItemAnimator) recyclerView3.mItemAnimator;
                            if (simpleItemAnimator != null) {
                                if (cVar4.a == cVar5.a && cVar4.b == cVar5.b) {
                                    simpleItemAnimator.a(c2);
                                    z2 = false;
                                } else {
                                    z2 = simpleItemAnimator.a(c2, cVar4.a, cVar4.b, cVar5.a, cVar5.b);
                                }
                                if (z2) {
                                    RecyclerView.this.postAnimationRunner();
                                }
                            } else {
                                throw null;
                            }
                        } else if (recyclerView3.mItemAnimator.a(c2, c2, cVar4, cVar5)) {
                            RecyclerView.this.postAnimationRunner();
                        }
                    } else {
                        throw null;
                    }
                } else if ((i3 & 4) != 0) {
                    l.c cVar6 = d2.b;
                    d dVar3 = (d) bVar;
                    RecyclerView.this.mRecycler.b(c2);
                    RecyclerView.this.animateDisappearance(c2, cVar6, null);
                } else if ((i3 & 8) != 0) {
                    RecyclerView.this.animateAppearance(c2, d2.b, d2.c);
                }
                ViewInfoStore.a.a(d2);
            }
        }
        this.mLayout.c(this.mRecycler);
        a0 a0Var2 = this.mState;
        a0Var2.b = a0Var2.f290e;
        this.mDataSetHasChangedAfterLayout = false;
        this.mDispatchItemsChangedEvent = false;
        a0Var2.f293j = false;
        a0Var2.f294k = false;
        this.mLayout.h = false;
        ArrayList<d0> arrayList = this.mRecycler.b;
        if (arrayList != null) {
            arrayList.clear();
        }
        o oVar = this.mLayout;
        if (oVar.f323n) {
            oVar.f322m = 0;
            oVar.f323n = false;
            this.mRecycler.d();
        }
        this.mLayout.g(this.mState);
        onExitLayoutOrScroll();
        stopInterceptRequestLayout(false);
        ViewInfoStore viewInfoStore2 = this.mViewInfoStore;
        viewInfoStore2.a.clear();
        viewInfoStore2.b.b();
        int[] iArr = this.mMinMaxLayoutPositions;
        if (didChildRangeChange(iArr[0], iArr[1])) {
            dispatchOnScrolled(0, 0);
        }
        recoverFocusFromState();
        resetFocusInfo();
    }

    private boolean dispatchToOnItemTouchListeners(MotionEvent motionEvent) {
        s sVar = this.mInterceptingOnItemTouchListener;
        if (sVar != null) {
            sVar.b(this, motionEvent);
            int action = motionEvent.getAction();
            if (action == 3 || action == 1) {
                this.mInterceptingOnItemTouchListener = null;
            }
            return true;
        } else if (motionEvent.getAction() == 0) {
            return false;
        } else {
            return findInterceptingOnItemTouchListener(motionEvent);
        }
    }

    private boolean findInterceptingOnItemTouchListener(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        int size = this.mOnItemTouchListeners.size();
        int i2 = 0;
        while (i2 < size) {
            s sVar = this.mOnItemTouchListeners.get(i2);
            if (!sVar.a(this, motionEvent) || action == 3) {
                i2++;
            } else {
                this.mInterceptingOnItemTouchListener = sVar;
                return true;
            }
        }
        return false;
    }

    private void findMinMaxChildLayoutPositions(int[] iArr) {
        int a2 = this.mChildHelper.a();
        if (a2 == 0) {
            iArr[0] = -1;
            iArr[1] = -1;
            return;
        }
        int i2 = Integer.MAX_VALUE;
        int i3 = UNDEFINED_DURATION;
        for (int i4 = 0; i4 < a2; i4++) {
            d0 childViewHolderInt = getChildViewHolderInt(this.mChildHelper.b(i4));
            if (!childViewHolderInt.o()) {
                int d2 = childViewHolderInt.d();
                if (d2 < i2) {
                    i2 = d2;
                }
                if (d2 > i3) {
                    i3 = d2;
                }
            }
        }
        iArr[0] = i2;
        iArr[1] = i3;
    }

    public static RecyclerView findNestedRecyclerView(View view) {
        if (!(view instanceof ViewGroup)) {
            return null;
        }
        if (view instanceof RecyclerView) {
            return (RecyclerView) view;
        }
        ViewGroup viewGroup = (ViewGroup) view;
        int childCount = super.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            RecyclerView findNestedRecyclerView = findNestedRecyclerView(super.getChildAt(i2));
            if (findNestedRecyclerView != null) {
                return findNestedRecyclerView;
            }
        }
        return null;
    }

    private View findNextViewToFocus() {
        d0 findViewHolderForAdapterPosition;
        int i2 = this.mState.f295l;
        if (i2 == -1) {
            i2 = 0;
        }
        int a2 = this.mState.a();
        int i3 = i2;
        while (i3 < a2) {
            d0 findViewHolderForAdapterPosition2 = findViewHolderForAdapterPosition(i3);
            if (findViewHolderForAdapterPosition2 == null) {
                break;
            } else if (findViewHolderForAdapterPosition2.a.hasFocusable()) {
                return findViewHolderForAdapterPosition2.a;
            } else {
                i3++;
            }
        }
        int min = Math.min(a2, i2);
        while (true) {
            min--;
            if (min < 0 || (findViewHolderForAdapterPosition = findViewHolderForAdapterPosition(min)) == null) {
                return null;
            }
            if (findViewHolderForAdapterPosition.a.hasFocusable()) {
                return findViewHolderForAdapterPosition.a;
            }
        }
    }

    public static d0 getChildViewHolderInt(View view) {
        if (view == null) {
            return null;
        }
        return ((p) view.getLayoutParams()).a;
    }

    public static void getDecoratedBoundsWithMarginsInt(View view, Rect rect) {
        p pVar = (p) view.getLayoutParams();
        Rect rect2 = pVar.b;
        rect.set((view.getLeft() - rect2.left) - pVar.leftMargin, (view.getTop() - rect2.top) - pVar.topMargin, view.getRight() + rect2.right + pVar.rightMargin, view.getBottom() + rect2.bottom + pVar.bottomMargin);
    }

    private int getDeepestFocusedViewWithId(View view) {
        int id = view.getId();
        while (!view.isFocused() && (view instanceof ViewGroup) && view.hasFocus()) {
            view = ((ViewGroup) view).getFocusedChild();
            if (view.getId() != -1) {
                id = view.getId();
            }
        }
        return id;
    }

    private String getFullClassName(Context context, String str) {
        if (str.charAt(0) == '.') {
            return context.getPackageName() + str;
        } else if (str.contains(".")) {
            return str;
        } else {
            return RecyclerView.class.getPackage().getName() + '.' + str;
        }
    }

    private NestedScrollingChildHelper getScrollingChildHelper() {
        if (this.mScrollingChildHelper == null) {
            this.mScrollingChildHelper = new NestedScrollingChildHelper(this);
        }
        return this.mScrollingChildHelper;
    }

    private void handleMissingPreInfoForChangeError(long j2, d0 d0Var, d0 d0Var2) {
        int a2 = this.mChildHelper.a();
        for (int i2 = 0; i2 < a2; i2++) {
            d0 childViewHolderInt = getChildViewHolderInt(this.mChildHelper.b(i2));
            if (childViewHolderInt != d0Var && getChangedHolderKey(childViewHolderInt) == j2) {
                g gVar = this.mAdapter;
                if (gVar == null || !gVar.b) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Two different ViewHolders have the same change ID. This might happen due to inconsistent Adapter update events or if the LayoutManager lays out the same View multiple times.\n ViewHolder 1:");
                    sb.append(childViewHolderInt);
                    sb.append(" \n View Holder 2:");
                    sb.append(d0Var);
                    throw new IllegalStateException(outline.a(this, sb));
                }
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Two different ViewHolders have the same stable ID. Stable IDs in your adapter MUST BE unique and SHOULD NOT change.\n ViewHolder 1:");
                sb2.append(childViewHolderInt);
                sb2.append(" \n View Holder 2:");
                sb2.append(d0Var);
                throw new IllegalStateException(outline.a(this, sb2));
            }
        }
        Log.e(TAG, "Problem while matching changed view holders with the newones. The pre-layout information for the change holder " + d0Var2 + " cannot be found but it is necessary for " + d0Var + exceptionLabel());
    }

    private boolean hasUpdatedView() {
        int a2 = this.mChildHelper.a();
        for (int i2 = 0; i2 < a2; i2++) {
            d0 childViewHolderInt = getChildViewHolderInt(this.mChildHelper.b(i2));
            if (childViewHolderInt != null && !childViewHolderInt.o() && childViewHolderInt.m()) {
                return true;
            }
        }
        return false;
    }

    @SuppressLint({"InlinedApi"})
    private void initAutofill() {
        if (ViewCompat.j(this) == 0 && Build.VERSION.SDK_INT >= 26) {
            setImportantForAutofill(8);
        }
    }

    private void initChildrenHelper() {
        this.mChildHelper = new ChildHelper(new e());
    }

    private boolean isPreferredNextFocus(View view, View view2, int i2) {
        int i3;
        if (view2 == null || view2 == this || findContainingItemView(view2) == null) {
            return false;
        }
        if (view == null || findContainingItemView(view) == null) {
            return true;
        }
        this.mTempRect.set(0, 0, view.getWidth(), view.getHeight());
        this.mTempRect2.set(0, 0, view2.getWidth(), view2.getHeight());
        offsetDescendantRectToMyCoords(view, this.mTempRect);
        offsetDescendantRectToMyCoords(view2, this.mTempRect2);
        char c2 = 65535;
        int i4 = this.mLayout.f() == 1 ? -1 : 1;
        Rect rect = this.mTempRect;
        int i5 = rect.left;
        int i6 = this.mTempRect2.left;
        if ((i5 < i6 || rect.right <= i6) && this.mTempRect.right < this.mTempRect2.right) {
            i3 = 1;
        } else {
            Rect rect2 = this.mTempRect;
            int i7 = rect2.right;
            int i8 = this.mTempRect2.right;
            i3 = ((i7 > i8 || rect2.left >= i8) && this.mTempRect.left > this.mTempRect2.left) ? -1 : 0;
        }
        Rect rect3 = this.mTempRect;
        int i9 = rect3.top;
        int i10 = this.mTempRect2.top;
        if ((i9 < i10 || rect3.bottom <= i10) && this.mTempRect.bottom < this.mTempRect2.bottom) {
            c2 = 1;
        } else {
            Rect rect4 = this.mTempRect;
            int i11 = rect4.bottom;
            int i12 = this.mTempRect2.bottom;
            if ((i11 <= i12 && rect4.top < i12) || this.mTempRect.top <= this.mTempRect2.top) {
                c2 = 0;
            }
        }
        if (i2 != 1) {
            if (i2 != 2) {
                if (i2 != 17) {
                    if (i2 != 33) {
                        if (i2 != 66) {
                            if (i2 != 130) {
                                StringBuilder sb = new StringBuilder();
                                sb.append("Invalid direction: ");
                                sb.append(i2);
                                throw new IllegalArgumentException(outline.a(this, sb));
                            } else if (c2 > 0) {
                                return true;
                            } else {
                                return false;
                            }
                        } else if (i3 > 0) {
                            return true;
                        } else {
                            return false;
                        }
                    } else if (c2 < 0) {
                        return true;
                    } else {
                        return false;
                    }
                } else if (i3 < 0) {
                    return true;
                } else {
                    return false;
                }
            } else if (c2 > 0 || (c2 == 0 && i3 * i4 >= 0)) {
                return true;
            } else {
                return false;
            }
        } else if (c2 < 0 || (c2 == 0 && i3 * i4 <= 0)) {
            return true;
        } else {
            return false;
        }
    }

    private void onPointerUp(MotionEvent motionEvent) {
        int actionIndex = motionEvent.getActionIndex();
        if (motionEvent.getPointerId(actionIndex) == this.mScrollPointerId) {
            int i2 = actionIndex == 0 ? 1 : 0;
            this.mScrollPointerId = motionEvent.getPointerId(i2);
            int x2 = (int) (motionEvent.getX(i2) + 0.5f);
            this.mLastTouchX = x2;
            this.mInitialTouchX = x2;
            int y2 = (int) (motionEvent.getY(i2) + 0.5f);
            this.mLastTouchY = y2;
            this.mInitialTouchY = y2;
        }
    }

    private boolean predictiveItemAnimationsEnabled() {
        return this.mItemAnimator != null && this.mLayout.q();
    }

    private void processAdapterUpdatesAndSetAnimationFlags() {
        boolean z2 = false;
        if (this.mDataSetHasChangedAfterLayout) {
            AdapterHelper adapterHelper = this.mAdapterHelper;
            adapterHelper.a(adapterHelper.b);
            adapterHelper.a(adapterHelper.c);
            adapterHelper.g = 0;
            if (this.mDispatchItemsChangedEvent) {
                this.mLayout.a(this);
            }
        }
        if (predictiveItemAnimationsEnabled()) {
            this.mAdapterHelper.d();
        } else {
            this.mAdapterHelper.b();
        }
        boolean z3 = this.mItemsAddedOrRemoved || this.mItemsChanged;
        this.mState.f293j = this.mFirstLayoutComplete && this.mItemAnimator != null && (this.mDataSetHasChangedAfterLayout || z3 || this.mLayout.h) && (!this.mDataSetHasChangedAfterLayout || this.mAdapter.b);
        a0 a0Var = this.mState;
        if (a0Var.f293j && z3 && !this.mDataSetHasChangedAfterLayout && predictiveItemAnimationsEnabled()) {
            z2 = true;
        }
        a0Var.f294k = z2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0056  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void pullGlows(float r7, float r8, float r9, float r10) {
        /*
            r6 = this;
            r0 = 1065353216(0x3f800000, float:1.0)
            r1 = 1
            r2 = 0
            int r3 = (r8 > r2 ? 1 : (r8 == r2 ? 0 : -1))
            if (r3 >= 0) goto L_0x0021
            r6.ensureLeftGlow()
            android.widget.EdgeEffect r3 = r6.mLeftGlow
            float r4 = -r8
            int r5 = r6.getWidth()
            float r5 = (float) r5
            float r4 = r4 / r5
            int r5 = r6.getHeight()
            float r5 = (float) r5
            float r9 = r9 / r5
            float r9 = r0 - r9
            r3.onPull(r4, r9)
        L_0x001f:
            r9 = 1
            goto L_0x003c
        L_0x0021:
            int r3 = (r8 > r2 ? 1 : (r8 == r2 ? 0 : -1))
            if (r3 <= 0) goto L_0x003b
            r6.ensureRightGlow()
            android.widget.EdgeEffect r3 = r6.mRightGlow
            int r4 = r6.getWidth()
            float r4 = (float) r4
            float r4 = r8 / r4
            int r5 = r6.getHeight()
            float r5 = (float) r5
            float r9 = r9 / r5
            r3.onPull(r4, r9)
            goto L_0x001f
        L_0x003b:
            r9 = 0
        L_0x003c:
            int r3 = (r10 > r2 ? 1 : (r10 == r2 ? 0 : -1))
            if (r3 >= 0) goto L_0x0056
            r6.ensureTopGlow()
            android.widget.EdgeEffect r9 = r6.mTopGlow
            float r0 = -r10
            int r3 = r6.getHeight()
            float r3 = (float) r3
            float r0 = r0 / r3
            int r3 = r6.getWidth()
            float r3 = (float) r3
            float r7 = r7 / r3
            r9.onPull(r0, r7)
            goto L_0x0072
        L_0x0056:
            int r3 = (r10 > r2 ? 1 : (r10 == r2 ? 0 : -1))
            if (r3 <= 0) goto L_0x0071
            r6.ensureBottomGlow()
            android.widget.EdgeEffect r9 = r6.mBottomGlow
            int r3 = r6.getHeight()
            float r3 = (float) r3
            float r3 = r10 / r3
            int r4 = r6.getWidth()
            float r4 = (float) r4
            float r7 = r7 / r4
            float r0 = r0 - r7
            r9.onPull(r3, r0)
            goto L_0x0072
        L_0x0071:
            r1 = r9
        L_0x0072:
            if (r1 != 0) goto L_0x007c
            int r7 = (r8 > r2 ? 1 : (r8 == r2 ? 0 : -1))
            if (r7 != 0) goto L_0x007c
            int r7 = (r10 > r2 ? 1 : (r10 == r2 ? 0 : -1))
            if (r7 == 0) goto L_0x007f
        L_0x007c:
            i.h.l.ViewCompat.A(r6)
        L_0x007f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.pullGlows(float, float, float, float):void");
    }

    private void recoverFocusFromState() {
        View findViewById;
        if (this.mPreserveFocusAfterLayout && this.mAdapter != null && hasFocus() && getDescendantFocusability() != 393216) {
            if (getDescendantFocusability() != 131072 || !isFocused()) {
                if (!isFocused()) {
                    View focusedChild = getFocusedChild();
                    if (!IGNORE_DETACHED_FOCUSED_CHILD || (focusedChild.getParent() != null && focusedChild.hasFocus())) {
                        if (!this.mChildHelper.c(focusedChild)) {
                            return;
                        }
                    } else if (this.mChildHelper.a() == 0) {
                        requestFocus();
                        return;
                    }
                }
                long j2 = this.mState.f296m;
                View view = null;
                d0 findViewHolderForItemId = (j2 == -1 || !this.mAdapter.b) ? null : findViewHolderForItemId(j2);
                if (findViewHolderForItemId != null && !this.mChildHelper.c(findViewHolderForItemId.a) && findViewHolderForItemId.a.hasFocusable()) {
                    view = findViewHolderForItemId.a;
                } else if (this.mChildHelper.a() > 0) {
                    view = findNextViewToFocus();
                }
                if (view != null) {
                    int i2 = this.mState.f297n;
                    if (!(((long) i2) == -1 || (findViewById = view.findViewById(i2)) == null || !findViewById.isFocusable())) {
                        view = findViewById;
                    }
                    view.requestFocus();
                }
            }
        }
    }

    private void releaseGlows() {
        boolean z2;
        EdgeEffect edgeEffect = this.mLeftGlow;
        if (edgeEffect != null) {
            edgeEffect.onRelease();
            z2 = this.mLeftGlow.isFinished();
        } else {
            z2 = false;
        }
        EdgeEffect edgeEffect2 = this.mTopGlow;
        if (edgeEffect2 != null) {
            edgeEffect2.onRelease();
            z2 |= this.mTopGlow.isFinished();
        }
        EdgeEffect edgeEffect3 = this.mRightGlow;
        if (edgeEffect3 != null) {
            edgeEffect3.onRelease();
            z2 |= this.mRightGlow.isFinished();
        }
        EdgeEffect edgeEffect4 = this.mBottomGlow;
        if (edgeEffect4 != null) {
            edgeEffect4.onRelease();
            z2 |= this.mBottomGlow.isFinished();
        }
        if (z2) {
            ViewCompat.A(this);
        }
    }

    private void requestChildOnScreen(View view, View view2) {
        View view3 = view2 != null ? view2 : view;
        this.mTempRect.set(0, 0, view3.getWidth(), view3.getHeight());
        ViewGroup.LayoutParams layoutParams = view3.getLayoutParams();
        if (layoutParams instanceof p) {
            p pVar = (p) layoutParams;
            if (!pVar.c) {
                Rect rect = pVar.b;
                Rect rect2 = this.mTempRect;
                rect2.left -= rect.left;
                rect2.right += rect.right;
                rect2.top -= rect.top;
                rect2.bottom += rect.bottom;
            }
        }
        if (view2 != null) {
            offsetDescendantRectToMyCoords(view2, this.mTempRect);
            offsetRectIntoDescendantCoords(view, this.mTempRect);
        }
        this.mLayout.a(this, view, this.mTempRect, !this.mFirstLayoutComplete, view2 == null);
    }

    private void resetFocusInfo() {
        a0 a0Var = this.mState;
        a0Var.f296m = -1;
        a0Var.f295l = -1;
        a0Var.f297n = -1;
    }

    private void resetScroll() {
        VelocityTracker velocityTracker = this.mVelocityTracker;
        if (velocityTracker != null) {
            velocityTracker.clear();
        }
        stopNestedScroll(0);
        releaseGlows();
    }

    private void saveFocusInfo() {
        int i2;
        d0 d0Var = null;
        View focusedChild = (!this.mPreserveFocusAfterLayout || !hasFocus() || this.mAdapter == null) ? null : getFocusedChild();
        if (focusedChild != null) {
            d0Var = findContainingViewHolder(focusedChild);
        }
        if (d0Var == null) {
            resetFocusInfo();
            return;
        }
        this.mState.f296m = this.mAdapter.b ? d0Var.f302e : -1;
        a0 a0Var = this.mState;
        if (this.mDataSetHasChangedAfterLayout) {
            i2 = -1;
        } else if (d0Var.j()) {
            i2 = d0Var.d;
        } else {
            i2 = d0Var.c();
        }
        a0Var.f295l = i2;
        this.mState.f297n = getDeepestFocusedViewWithId(d0Var.a);
    }

    private void setAdapterInternal(g gVar, boolean z2, boolean z3) {
        g gVar2 = this.mAdapter;
        if (gVar2 != null) {
            gVar2.a.unregisterObserver(this.mObserver);
            if (this.mAdapter == null) {
                throw null;
            }
        }
        if (!z2 || z3) {
            removeAndRecycleViews();
        }
        AdapterHelper adapterHelper = this.mAdapterHelper;
        adapterHelper.a(adapterHelper.b);
        adapterHelper.a(adapterHelper.c);
        adapterHelper.g = 0;
        g gVar3 = this.mAdapter;
        this.mAdapter = gVar;
        if (gVar != null) {
            gVar.a.registerObserver(this.mObserver);
        }
        o oVar = this.mLayout;
        v vVar = this.mRecycler;
        g gVar4 = this.mAdapter;
        vVar.a();
        u b2 = vVar.b();
        if (b2 != null) {
            if (gVar3 != null) {
                b2.b--;
            }
            if (!z2 && b2.b == 0) {
                for (int i2 = 0; i2 < b2.a.size(); i2++) {
                    b2.a.valueAt(i2).a.clear();
                }
            }
            if (gVar4 != null) {
                b2.b++;
            }
            this.mState.f291f = true;
            return;
        }
        throw null;
    }

    private void stopScrollersInternal() {
        z zVar;
        this.mViewFlinger.b();
        o oVar = this.mLayout;
        if (oVar != null && (zVar = oVar.g) != null) {
            zVar.a();
        }
    }

    public void absorbGlows(int i2, int i3) {
        if (i2 < 0) {
            ensureLeftGlow();
            if (this.mLeftGlow.isFinished()) {
                this.mLeftGlow.onAbsorb(-i2);
            }
        } else if (i2 > 0) {
            ensureRightGlow();
            if (this.mRightGlow.isFinished()) {
                this.mRightGlow.onAbsorb(i2);
            }
        }
        if (i3 < 0) {
            ensureTopGlow();
            if (this.mTopGlow.isFinished()) {
                this.mTopGlow.onAbsorb(-i3);
            }
        } else if (i3 > 0) {
            ensureBottomGlow();
            if (this.mBottomGlow.isFinished()) {
                this.mBottomGlow.onAbsorb(i3);
            }
        }
        if (i2 != 0 || i3 != 0) {
            ViewCompat.A(this);
        }
    }

    public void addFocusables(ArrayList<View> arrayList, int i2, int i3) {
        o oVar = this.mLayout;
        if (oVar == null || oVar != null) {
            super.addFocusables(arrayList, i2, i3);
            return;
        }
        throw null;
    }

    public void addItemDecoration(n nVar, int i2) {
        o oVar = this.mLayout;
        if (oVar != null) {
            oVar.a("Cannot add item decoration during a scroll  or layout");
        }
        if (this.mItemDecorations.isEmpty()) {
            setWillNotDraw(false);
        }
        if (i2 < 0) {
            this.mItemDecorations.add(nVar);
        } else {
            this.mItemDecorations.add(i2, nVar);
        }
        markItemDecorInsetsDirty();
        requestLayout();
    }

    public void addOnChildAttachStateChangeListener(q qVar) {
        if (this.mOnChildAttachStateListeners == null) {
            this.mOnChildAttachStateListeners = new ArrayList();
        }
        this.mOnChildAttachStateListeners.add(qVar);
    }

    public void addOnItemTouchListener(s sVar) {
        this.mOnItemTouchListeners.add(sVar);
    }

    public void addOnScrollListener(t tVar) {
        if (this.mScrollListeners == null) {
            this.mScrollListeners = new ArrayList();
        }
        this.mScrollListeners.add(tVar);
    }

    public void animateAppearance(d0 d0Var, l.c cVar, l.c cVar2) {
        boolean z2;
        d0Var.a(false);
        SimpleItemAnimator simpleItemAnimator = (SimpleItemAnimator) this.mItemAnimator;
        if (simpleItemAnimator != null) {
            if (cVar == null || (cVar.a == cVar2.a && cVar.b == cVar2.b)) {
                DefaultItemAnimator7 defaultItemAnimator7 = (DefaultItemAnimator7) simpleItemAnimator;
                defaultItemAnimator7.e(d0Var);
                d0Var.a.setAlpha(0.0f);
                defaultItemAnimator7.f1369i.add(d0Var);
                z2 = true;
            } else {
                z2 = simpleItemAnimator.a(d0Var, cVar.a, cVar.b, cVar2.a, cVar2.b);
            }
            if (z2) {
                postAnimationRunner();
                return;
            }
            return;
        }
        throw null;
    }

    public void animateDisappearance(d0 d0Var, l.c cVar, l.c cVar2) {
        boolean z2;
        addAnimatingView(d0Var);
        d0Var.a(false);
        SimpleItemAnimator simpleItemAnimator = (SimpleItemAnimator) this.mItemAnimator;
        if (simpleItemAnimator != null) {
            int i2 = cVar.a;
            int i3 = cVar.b;
            View view = d0Var.a;
            int left = cVar2 == null ? view.getLeft() : cVar2.a;
            int top = cVar2 == null ? view.getTop() : cVar2.b;
            if (d0Var.j() || (i2 == left && i3 == top)) {
                DefaultItemAnimator7 defaultItemAnimator7 = (DefaultItemAnimator7) simpleItemAnimator;
                defaultItemAnimator7.e(d0Var);
                defaultItemAnimator7.h.add(d0Var);
                z2 = true;
            } else {
                view.layout(left, top, view.getWidth() + left, view.getHeight() + top);
                z2 = simpleItemAnimator.a(d0Var, i2, i3, left, top);
            }
            if (z2) {
                postAnimationRunner();
                return;
            }
            return;
        }
        throw null;
    }

    public void assertInLayoutOrScroll(String str) {
        if (isComputingLayout()) {
            return;
        }
        if (str == null) {
            throw new IllegalStateException(outline.a(this, outline.a("Cannot call this method unless RecyclerView is computing a layout or scrolling")));
        }
        throw new IllegalStateException(outline.a(this, outline.a(str)));
    }

    public void assertNotInLayoutOrScroll(String str) {
        if (isComputingLayout()) {
            if (str == null) {
                throw new IllegalStateException(outline.a(this, outline.a("Cannot call this method while RecyclerView is computing a layout or scrolling")));
            }
            throw new IllegalStateException(str);
        } else if (this.mDispatchScrollCounter > 0) {
            Log.w(TAG, "Cannot call this method in a scroll callback. Scroll callbacks mightbe run during a measure & layout pass where you cannot change theRecyclerView data. Any method call that might change the structureof the RecyclerView or the adapter contents should be postponed tothe next frame.", new IllegalStateException(outline.a(this, outline.a(""))));
        }
    }

    public boolean canReuseUpdatedViewHolder(d0 d0Var) {
        l lVar = this.mItemAnimator;
        return lVar == null || lVar.a(d0Var, d0Var.e());
    }

    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof p) && this.mLayout.a((p) layoutParams);
    }

    public void clearOldPositions() {
        int b2 = this.mChildHelper.b();
        for (int i2 = 0; i2 < b2; i2++) {
            d0 childViewHolderInt = getChildViewHolderInt(this.mChildHelper.d(i2));
            if (!childViewHolderInt.o()) {
                childViewHolderInt.a();
            }
        }
        v vVar = this.mRecycler;
        int size = vVar.c.size();
        for (int i3 = 0; i3 < size; i3++) {
            vVar.c.get(i3).a();
        }
        int size2 = vVar.a.size();
        for (int i4 = 0; i4 < size2; i4++) {
            vVar.a.get(i4).a();
        }
        ArrayList<d0> arrayList = vVar.b;
        if (arrayList != null) {
            int size3 = arrayList.size();
            for (int i5 = 0; i5 < size3; i5++) {
                vVar.b.get(i5).a();
            }
        }
    }

    public void clearOnChildAttachStateChangeListeners() {
        List<q> list = this.mOnChildAttachStateListeners;
        if (list != null) {
            list.clear();
        }
    }

    public void clearOnScrollListeners() {
        List<t> list = this.mScrollListeners;
        if (list != null) {
            list.clear();
        }
    }

    public int computeHorizontalScrollExtent() {
        o oVar = this.mLayout;
        if (oVar != null && oVar.a()) {
            return this.mLayout.a(this.mState);
        }
        return 0;
    }

    public int computeHorizontalScrollOffset() {
        o oVar = this.mLayout;
        if (oVar != null && oVar.a()) {
            return this.mLayout.b(this.mState);
        }
        return 0;
    }

    public int computeHorizontalScrollRange() {
        o oVar = this.mLayout;
        if (oVar != null && oVar.a()) {
            return this.mLayout.c(this.mState);
        }
        return 0;
    }

    public int computeVerticalScrollExtent() {
        o oVar = this.mLayout;
        if (oVar != null && oVar.b()) {
            return this.mLayout.d(this.mState);
        }
        return 0;
    }

    public int computeVerticalScrollOffset() {
        o oVar = this.mLayout;
        if (oVar != null && oVar.b()) {
            return this.mLayout.e(this.mState);
        }
        return 0;
    }

    public int computeVerticalScrollRange() {
        o oVar = this.mLayout;
        if (oVar != null && oVar.b()) {
            return this.mLayout.f(this.mState);
        }
        return 0;
    }

    public void considerReleasingGlowsOnScroll(int i2, int i3) {
        boolean z2;
        EdgeEffect edgeEffect = this.mLeftGlow;
        if (edgeEffect == null || edgeEffect.isFinished() || i2 <= 0) {
            z2 = false;
        } else {
            this.mLeftGlow.onRelease();
            z2 = this.mLeftGlow.isFinished();
        }
        EdgeEffect edgeEffect2 = this.mRightGlow;
        if (edgeEffect2 != null && !edgeEffect2.isFinished() && i2 < 0) {
            this.mRightGlow.onRelease();
            z2 |= this.mRightGlow.isFinished();
        }
        EdgeEffect edgeEffect3 = this.mTopGlow;
        if (edgeEffect3 != null && !edgeEffect3.isFinished() && i3 > 0) {
            this.mTopGlow.onRelease();
            z2 |= this.mTopGlow.isFinished();
        }
        EdgeEffect edgeEffect4 = this.mBottomGlow;
        if (edgeEffect4 != null && !edgeEffect4.isFinished() && i3 < 0) {
            this.mBottomGlow.onRelease();
            z2 |= this.mBottomGlow.isFinished();
        }
        if (z2) {
            ViewCompat.A(this);
        }
    }

    public void consumePendingUpdateOperations() {
        if (!this.mFirstLayoutComplete || this.mDataSetHasChangedAfterLayout) {
            TraceCompat.a(TRACE_ON_DATA_SET_CHANGE_LAYOUT_TAG);
            dispatchLayout();
            Trace.endSection();
        } else if (this.mAdapterHelper.c()) {
            boolean z2 = false;
            if ((this.mAdapterHelper.g & 4) != 0) {
                if ((this.mAdapterHelper.g & 11) != 0) {
                    z2 = true;
                }
                if (!z2) {
                    TraceCompat.a(TRACE_HANDLE_ADAPTER_UPDATES_TAG);
                    startInterceptRequestLayout();
                    onEnterLayoutOrScroll();
                    this.mAdapterHelper.d();
                    if (!this.mLayoutWasDefered) {
                        if (hasUpdatedView()) {
                            dispatchLayout();
                        } else {
                            this.mAdapterHelper.a();
                        }
                    }
                    stopInterceptRequestLayout(true);
                    onExitLayoutOrScroll();
                    Trace.endSection();
                    return;
                }
            }
            if (this.mAdapterHelper.c()) {
                TraceCompat.a(TRACE_ON_DATA_SET_CHANGE_LAYOUT_TAG);
                dispatchLayout();
                Trace.endSection();
            }
        }
    }

    public void defaultOnMeasure(int i2, int i3) {
        setMeasuredDimension(o.a(i2, getPaddingRight() + getPaddingLeft(), ViewCompat.m(this)), o.a(i3, getPaddingBottom() + getPaddingTop(), getMinimumHeight()));
    }

    public void dispatchChildAttached(View view) {
        d0 childViewHolderInt = getChildViewHolderInt(view);
        onChildAttachedToWindow(view);
        g gVar = this.mAdapter;
        if (gVar == null || childViewHolderInt == null || gVar != null) {
            List<q> list = this.mOnChildAttachStateListeners;
            if (list != null) {
                for (int size = list.size() - 1; size >= 0; size--) {
                    this.mOnChildAttachStateListeners.get(size).a(view);
                }
                return;
            }
            return;
        }
        throw null;
    }

    public void dispatchChildDetached(View view) {
        d0 childViewHolderInt = getChildViewHolderInt(view);
        onChildDetachedFromWindow(view);
        g gVar = this.mAdapter;
        if (gVar == null || childViewHolderInt == null || gVar != null) {
            List<q> list = this.mOnChildAttachStateListeners;
            if (list != null) {
                for (int size = list.size() - 1; size >= 0; size--) {
                    this.mOnChildAttachStateListeners.get(size).b(view);
                }
                return;
            }
            return;
        }
        throw null;
    }

    public void dispatchLayout() {
        if (this.mAdapter == null) {
            Log.e(TAG, "No adapter attached; skipping layout");
        } else if (this.mLayout == null) {
            Log.e(TAG, "No layout manager attached; skipping layout");
        } else {
            a0 a0Var = this.mState;
            boolean z2 = false;
            a0Var.f292i = false;
            if (a0Var.d == 1) {
                dispatchLayoutStep1();
                this.mLayout.b(this);
                dispatchLayoutStep2();
            } else {
                AdapterHelper adapterHelper = this.mAdapterHelper;
                if (!adapterHelper.c.isEmpty() && !adapterHelper.b.isEmpty()) {
                    z2 = true;
                }
                if (!z2 && this.mLayout.f326q == getWidth() && this.mLayout.f327r == getHeight()) {
                    this.mLayout.b(this);
                } else {
                    this.mLayout.b(this);
                    dispatchLayoutStep2();
                }
            }
            dispatchLayoutStep3();
        }
    }

    public boolean dispatchNestedFling(float f2, float f3, boolean z2) {
        return getScrollingChildHelper().a(f2, f3, z2);
    }

    public boolean dispatchNestedPreFling(float f2, float f3) {
        return getScrollingChildHelper().a(f2, f3);
    }

    public boolean dispatchNestedPreScroll(int i2, int i3, int[] iArr, int[] iArr2) {
        return getScrollingChildHelper().a(i2, i3, iArr, iArr2, 0);
    }

    public boolean dispatchNestedScroll(int i2, int i3, int i4, int i5, int[] iArr) {
        return getScrollingChildHelper().a(i2, i3, i4, i5, iArr);
    }

    public void dispatchOnScrollStateChanged(int i2) {
        o oVar = this.mLayout;
        if (oVar != null) {
            oVar.f(i2);
        }
        onScrollStateChanged(i2);
        t tVar = this.mScrollListener;
        if (tVar != null) {
            tVar.a(this, i2);
        }
        List<t> list = this.mScrollListeners;
        if (list != null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                this.mScrollListeners.get(size).a(this, i2);
            }
        }
    }

    public void dispatchOnScrolled(int i2, int i3) {
        this.mDispatchScrollCounter++;
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        onScrollChanged(scrollX, scrollY, scrollX - i2, scrollY - i3);
        onScrolled(i2, i3);
        t tVar = this.mScrollListener;
        if (tVar != null) {
            tVar.a(this, i2, i3);
        }
        List<t> list = this.mScrollListeners;
        if (list != null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                this.mScrollListeners.get(size).a(this, i2, i3);
            }
        }
        this.mDispatchScrollCounter--;
    }

    public void dispatchPendingImportantForAccessibilityChanges() {
        int i2;
        for (int size = this.mPendingAccessibilityImportanceChange.size() - 1; size >= 0; size--) {
            d0 d0Var = this.mPendingAccessibilityImportanceChange.get(size);
            if (d0Var.a.getParent() == this && !d0Var.o() && (i2 = d0Var.f312q) != -1) {
                ViewCompat.h(d0Var.a, i2);
                d0Var.f312q = -1;
            }
        }
        this.mPendingAccessibilityImportanceChange.clear();
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        onPopulateAccessibilityEvent(accessibilityEvent);
        return true;
    }

    public void dispatchRestoreInstanceState(SparseArray<Parcelable> sparseArray) {
        dispatchThawSelfOnly(sparseArray);
    }

    public void dispatchSaveInstanceState(SparseArray<Parcelable> sparseArray) {
        dispatchFreezeSelfOnly(sparseArray);
    }

    public void draw(Canvas canvas) {
        boolean z2;
        super.draw(canvas);
        int size = this.mItemDecorations.size();
        boolean z3 = false;
        for (int i2 = 0; i2 < size; i2++) {
            this.mItemDecorations.get(i2).b(canvas, this, this.mState);
        }
        EdgeEffect edgeEffect = this.mLeftGlow;
        boolean z4 = true;
        if (edgeEffect == null || edgeEffect.isFinished()) {
            z2 = false;
        } else {
            int save = canvas.save();
            int paddingBottom = this.mClipToPadding ? getPaddingBottom() : 0;
            canvas.rotate(270.0f);
            canvas.translate((float) ((-getHeight()) + paddingBottom), 0.0f);
            EdgeEffect edgeEffect2 = this.mLeftGlow;
            z2 = edgeEffect2 != null && edgeEffect2.draw(canvas);
            canvas.restoreToCount(save);
        }
        EdgeEffect edgeEffect3 = this.mTopGlow;
        if (edgeEffect3 != null && !edgeEffect3.isFinished()) {
            int save2 = canvas.save();
            if (this.mClipToPadding) {
                canvas.translate((float) getPaddingLeft(), (float) getPaddingTop());
            }
            EdgeEffect edgeEffect4 = this.mTopGlow;
            z2 |= edgeEffect4 != null && edgeEffect4.draw(canvas);
            canvas.restoreToCount(save2);
        }
        EdgeEffect edgeEffect5 = this.mRightGlow;
        if (edgeEffect5 != null && !edgeEffect5.isFinished()) {
            int save3 = canvas.save();
            int width = getWidth();
            int paddingTop = this.mClipToPadding ? getPaddingTop() : 0;
            canvas.rotate(90.0f);
            canvas.translate((float) (-paddingTop), (float) (-width));
            EdgeEffect edgeEffect6 = this.mRightGlow;
            z2 |= edgeEffect6 != null && edgeEffect6.draw(canvas);
            canvas.restoreToCount(save3);
        }
        EdgeEffect edgeEffect7 = this.mBottomGlow;
        if (edgeEffect7 != null && !edgeEffect7.isFinished()) {
            int save4 = canvas.save();
            canvas.rotate(180.0f);
            if (this.mClipToPadding) {
                canvas.translate((float) (getPaddingRight() + (-getWidth())), (float) (getPaddingBottom() + (-getHeight())));
            } else {
                canvas.translate((float) (-getWidth()), (float) (-getHeight()));
            }
            EdgeEffect edgeEffect8 = this.mBottomGlow;
            if (edgeEffect8 != null && edgeEffect8.draw(canvas)) {
                z3 = true;
            }
            z2 |= z3;
            canvas.restoreToCount(save4);
        }
        if (z2 || this.mItemAnimator == null || this.mItemDecorations.size() <= 0 || !this.mItemAnimator.c()) {
            z4 = z2;
        }
        if (z4) {
            ViewCompat.A(this);
        }
    }

    public boolean drawChild(Canvas canvas, View view, long j2) {
        return super.drawChild(canvas, view, j2);
    }

    public void ensureBottomGlow() {
        if (this.mBottomGlow == null) {
            EdgeEffect a2 = this.mEdgeEffectFactory.a(this);
            this.mBottomGlow = a2;
            if (this.mClipToPadding) {
                a2.setSize((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight(), (getMeasuredHeight() - getPaddingTop()) - getPaddingBottom());
            } else {
                a2.setSize(getMeasuredWidth(), getMeasuredHeight());
            }
        }
    }

    public void ensureLeftGlow() {
        if (this.mLeftGlow == null) {
            EdgeEffect a2 = this.mEdgeEffectFactory.a(this);
            this.mLeftGlow = a2;
            if (this.mClipToPadding) {
                a2.setSize((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom(), (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight());
            } else {
                a2.setSize(getMeasuredHeight(), getMeasuredWidth());
            }
        }
    }

    public void ensureRightGlow() {
        if (this.mRightGlow == null) {
            EdgeEffect a2 = this.mEdgeEffectFactory.a(this);
            this.mRightGlow = a2;
            if (this.mClipToPadding) {
                a2.setSize((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom(), (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight());
            } else {
                a2.setSize(getMeasuredHeight(), getMeasuredWidth());
            }
        }
    }

    public void ensureTopGlow() {
        if (this.mTopGlow == null) {
            EdgeEffect a2 = this.mEdgeEffectFactory.a(this);
            this.mTopGlow = a2;
            if (this.mClipToPadding) {
                a2.setSize((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight(), (getMeasuredHeight() - getPaddingTop()) - getPaddingBottom());
            } else {
                a2.setSize(getMeasuredWidth(), getMeasuredHeight());
            }
        }
    }

    public String exceptionLabel() {
        StringBuilder a2 = outline.a(" ");
        a2.append(super.toString());
        a2.append(", adapter:");
        a2.append(this.mAdapter);
        a2.append(", layout:");
        a2.append(this.mLayout);
        a2.append(", context:");
        a2.append(getContext());
        return a2.toString();
    }

    public final void fillRemainingScrollValues(a0 a0Var) {
        if (getScrollState() == 2) {
            OverScroller overScroller = this.mViewFlinger.d;
            a0Var.f298o = overScroller.getFinalX() - overScroller.getCurrX();
            overScroller.getFinalY();
            overScroller.getCurrY();
            return;
        }
        a0Var.f298o = 0;
    }

    public View findChildViewUnder(float f2, float f3) {
        for (int a2 = this.mChildHelper.a() - 1; a2 >= 0; a2--) {
            View b2 = this.mChildHelper.b(a2);
            float translationX = b2.getTranslationX();
            float translationY = b2.getTranslationY();
            if (f2 >= ((float) b2.getLeft()) + translationX && f2 <= ((float) b2.getRight()) + translationX && f3 >= ((float) b2.getTop()) + translationY && f3 <= ((float) b2.getBottom()) + translationY) {
                return b2;
            }
        }
        return null;
    }

    public View findContainingItemView(View view) {
        ViewParent parent = view.getParent();
        while (parent != null && parent != this && (parent instanceof View)) {
            view = (View) parent;
            parent = view.getParent();
        }
        if (parent == this) {
            return view;
        }
        return null;
    }

    public d0 findContainingViewHolder(View view) {
        View findContainingItemView = findContainingItemView(view);
        if (findContainingItemView == null) {
            return null;
        }
        return getChildViewHolder(findContainingItemView);
    }

    public d0 findViewHolderForAdapterPosition(int i2) {
        d0 d0Var = null;
        if (this.mDataSetHasChangedAfterLayout) {
            return null;
        }
        int b2 = this.mChildHelper.b();
        for (int i3 = 0; i3 < b2; i3++) {
            d0 childViewHolderInt = getChildViewHolderInt(this.mChildHelper.d(i3));
            if (childViewHolderInt != null && !childViewHolderInt.j() && getAdapterPositionFor(childViewHolderInt) == i2) {
                if (!this.mChildHelper.c(childViewHolderInt.a)) {
                    return childViewHolderInt;
                }
                d0Var = childViewHolderInt;
            }
        }
        return d0Var;
    }

    public d0 findViewHolderForItemId(long j2) {
        g gVar = this.mAdapter;
        d0 d0Var = null;
        if (gVar != null && gVar.b) {
            int b2 = this.mChildHelper.b();
            for (int i2 = 0; i2 < b2; i2++) {
                d0 childViewHolderInt = getChildViewHolderInt(this.mChildHelper.d(i2));
                if (childViewHolderInt != null && !childViewHolderInt.j() && childViewHolderInt.f302e == j2) {
                    if (!this.mChildHelper.c(childViewHolderInt.a)) {
                        return childViewHolderInt;
                    }
                    d0Var = childViewHolderInt;
                }
            }
        }
        return d0Var;
    }

    public d0 findViewHolderForLayoutPosition(int i2) {
        return findViewHolderForPosition(i2, false);
    }

    @Deprecated
    public d0 findViewHolderForPosition(int i2) {
        return findViewHolderForPosition(i2, false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:84:0x0117  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x011f  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x0124 A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean fling(int r19, int r20) {
        /*
            r18 = this;
            r0 = r18
            androidx.recyclerview.widget.RecyclerView$o r1 = r0.mLayout
            r2 = 0
            if (r1 != 0) goto L_0x000f
            java.lang.String r1 = "RecyclerView"
            java.lang.String r3 = "Cannot fling without a LayoutManager set. Call setLayoutManager with a non-null argument."
            android.util.Log.e(r1, r3)
            return r2
        L_0x000f:
            boolean r3 = r0.mLayoutSuppressed
            if (r3 == 0) goto L_0x0014
            return r2
        L_0x0014:
            boolean r1 = r1.a()
            androidx.recyclerview.widget.RecyclerView$o r3 = r0.mLayout
            boolean r3 = r3.b()
            if (r1 == 0) goto L_0x002c
            int r4 = java.lang.Math.abs(r19)
            int r5 = r0.mMinFlingVelocity
            if (r4 >= r5) goto L_0x0029
            goto L_0x002c
        L_0x0029:
            r4 = r19
            goto L_0x002d
        L_0x002c:
            r4 = 0
        L_0x002d:
            if (r3 == 0) goto L_0x003b
            int r5 = java.lang.Math.abs(r20)
            int r6 = r0.mMinFlingVelocity
            if (r5 >= r6) goto L_0x0038
            goto L_0x003b
        L_0x0038:
            r5 = r20
            goto L_0x003c
        L_0x003b:
            r5 = 0
        L_0x003c:
            if (r4 != 0) goto L_0x0041
            if (r5 != 0) goto L_0x0041
            return r2
        L_0x0041:
            float r6 = (float) r4
            float r7 = (float) r5
            boolean r8 = r0.dispatchNestedPreFling(r6, r7)
            if (r8 != 0) goto L_0x0184
            if (r1 != 0) goto L_0x0050
            if (r3 == 0) goto L_0x004e
            goto L_0x0050
        L_0x004e:
            r9 = 0
            goto L_0x0051
        L_0x0050:
            r9 = 1
        L_0x0051:
            r0.dispatchNestedFling(r6, r7, r9)
            androidx.recyclerview.widget.RecyclerView$r r6 = r0.mOnFlingListener
            if (r6 == 0) goto L_0x0126
            i.r.d.SnapHelper r6 = (i.r.d.SnapHelper) r6
            androidx.recyclerview.widget.RecyclerView r7 = r6.a
            androidx.recyclerview.widget.RecyclerView$o r7 = r7.getLayoutManager()
            if (r7 != 0) goto L_0x0064
            goto L_0x0121
        L_0x0064:
            androidx.recyclerview.widget.RecyclerView r10 = r6.a
            androidx.recyclerview.widget.RecyclerView$g r10 = r10.getAdapter()
            if (r10 != 0) goto L_0x006e
            goto L_0x0121
        L_0x006e:
            androidx.recyclerview.widget.RecyclerView r10 = r6.a
            int r10 = r10.getMinFlingVelocity()
            int r11 = java.lang.Math.abs(r5)
            if (r11 > r10) goto L_0x0080
            int r11 = java.lang.Math.abs(r4)
            if (r11 <= r10) goto L_0x0121
        L_0x0080:
            boolean r10 = r7 instanceof androidx.recyclerview.widget.RecyclerView.z.b
            if (r10 != 0) goto L_0x0086
            goto L_0x0115
        L_0x0086:
            r11 = 0
            if (r10 != 0) goto L_0x008b
            r12 = r11
            goto L_0x0096
        L_0x008b:
            i.r.d.SnapHelper0 r12 = new i.r.d.SnapHelper0
            androidx.recyclerview.widget.RecyclerView r13 = r6.a
            android.content.Context r13 = r13.getContext()
            r12.<init>(r6, r13)
        L_0x0096:
            if (r12 != 0) goto L_0x009a
            goto L_0x0115
        L_0x009a:
            i.r.d.LinearSnapHelper r6 = (i.r.d.LinearSnapHelper) r6
            r13 = -1
            if (r10 != 0) goto L_0x00a1
            goto L_0x0107
        L_0x00a1:
            androidx.recyclerview.widget.RecyclerView r10 = r7.b
            if (r10 == 0) goto L_0x00a9
            androidx.recyclerview.widget.RecyclerView$g r11 = r10.getAdapter()
        L_0x00a9:
            if (r11 == 0) goto L_0x00b0
            int r10 = r11.a()
            goto L_0x00b1
        L_0x00b0:
            r10 = 0
        L_0x00b1:
            if (r10 != 0) goto L_0x00b4
            goto L_0x0107
        L_0x00b4:
            android.view.View r11 = r6.a(r7)
            if (r11 != 0) goto L_0x00bb
            goto L_0x0107
        L_0x00bb:
            int r11 = r7.i(r11)
            if (r11 != r13) goto L_0x00c2
            goto L_0x0107
        L_0x00c2:
            r14 = r7
            androidx.recyclerview.widget.RecyclerView$z$b r14 = (androidx.recyclerview.widget.RecyclerView.z.b) r14
            int r15 = r10 + -1
            android.graphics.PointF r14 = r14.a(r15)
            if (r14 != 0) goto L_0x00ce
            goto L_0x0107
        L_0x00ce:
            boolean r16 = r7.a()
            r17 = 0
            if (r16 == 0) goto L_0x00e6
            i.r.d.OrientationHelper r8 = r6.b(r7)
            int r8 = r6.a(r7, r8, r4, r2)
            float r13 = r14.x
            int r13 = (r13 > r17 ? 1 : (r13 == r17 ? 0 : -1))
            if (r13 >= 0) goto L_0x00e7
            int r8 = -r8
            goto L_0x00e7
        L_0x00e6:
            r8 = 0
        L_0x00e7:
            boolean r13 = r7.b()
            if (r13 == 0) goto L_0x00fd
            i.r.d.OrientationHelper r13 = r6.c(r7)
            int r6 = r6.a(r7, r13, r2, r5)
            float r13 = r14.y
            int r13 = (r13 > r17 ? 1 : (r13 == r17 ? 0 : -1))
            if (r13 >= 0) goto L_0x00fe
            int r6 = -r6
            goto L_0x00fe
        L_0x00fd:
            r6 = 0
        L_0x00fe:
            boolean r13 = r7.b()
            if (r13 == 0) goto L_0x0105
            r8 = r6
        L_0x0105:
            if (r8 != 0) goto L_0x010a
        L_0x0107:
            r6 = -1
            r15 = -1
            goto L_0x0113
        L_0x010a:
            int r11 = r11 + r8
            if (r11 >= 0) goto L_0x010e
            r11 = 0
        L_0x010e:
            if (r11 < r10) goto L_0x0111
            goto L_0x0112
        L_0x0111:
            r15 = r11
        L_0x0112:
            r6 = -1
        L_0x0113:
            if (r15 != r6) goto L_0x0117
        L_0x0115:
            r6 = 0
            goto L_0x011d
        L_0x0117:
            r12.a = r15
            r7.a(r12)
            r6 = 1
        L_0x011d:
            if (r6 == 0) goto L_0x0121
            r6 = 1
            goto L_0x0122
        L_0x0121:
            r6 = 0
        L_0x0122:
            if (r6 == 0) goto L_0x0126
            r6 = 1
            return r6
        L_0x0126:
            if (r9 == 0) goto L_0x0184
            if (r1 == 0) goto L_0x012c
            r1 = 1
            goto L_0x012d
        L_0x012c:
            r1 = 0
        L_0x012d:
            if (r3 == 0) goto L_0x0131
            r1 = r1 | 2
        L_0x0131:
            r3 = 1
            r0.startNestedScroll(r1, r3)
            int r1 = r0.mMaxFlingVelocity
            int r3 = -r1
            int r1 = java.lang.Math.min(r4, r1)
            int r9 = java.lang.Math.max(r3, r1)
            int r1 = r0.mMaxFlingVelocity
            int r3 = -r1
            int r1 = java.lang.Math.min(r5, r1)
            int r10 = java.lang.Math.max(r3, r1)
            androidx.recyclerview.widget.RecyclerView$c0 r1 = r0.mViewFlinger
            androidx.recyclerview.widget.RecyclerView r3 = androidx.recyclerview.widget.RecyclerView.this
            r4 = 2
            r3.setScrollState(r4)
            r1.c = r2
            r1.b = r2
            android.view.animation.Interpolator r2 = r1.f299e
            android.view.animation.Interpolator r3 = androidx.recyclerview.widget.RecyclerView.sQuinticInterpolator
            if (r2 == r3) goto L_0x016e
            r1.f299e = r3
            android.widget.OverScroller r2 = new android.widget.OverScroller
            androidx.recyclerview.widget.RecyclerView r3 = androidx.recyclerview.widget.RecyclerView.this
            android.content.Context r3 = r3.getContext()
            android.view.animation.Interpolator r4 = androidx.recyclerview.widget.RecyclerView.sQuinticInterpolator
            r2.<init>(r3, r4)
            r1.d = r2
        L_0x016e:
            android.widget.OverScroller r6 = r1.d
            r7 = 0
            r8 = 0
            r11 = -2147483648(0xffffffff80000000, float:-0.0)
            r12 = 2147483647(0x7fffffff, float:NaN)
            r13 = -2147483648(0xffffffff80000000, float:-0.0)
            r14 = 2147483647(0x7fffffff, float:NaN)
            r6.fling(r7, r8, r9, r10, r11, r12, r13, r14)
            r1.a()
            r1 = 1
            return r1
        L_0x0184:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.fling(int, int):boolean");
    }

    public View focusSearch(View view, int i2) {
        View view2;
        boolean z2;
        o oVar = this.mLayout;
        if (oVar != null) {
            boolean z3 = true;
            boolean z4 = this.mAdapter != null && oVar != null && !isComputingLayout() && !this.mLayoutSuppressed;
            FocusFinder instance = FocusFinder.getInstance();
            if (!z4 || !(i2 == 2 || i2 == 1)) {
                View findNextFocus = instance.findNextFocus(super, view, i2);
                if (findNextFocus != null || !z4) {
                    view2 = findNextFocus;
                } else {
                    consumePendingUpdateOperations();
                    if (findContainingItemView(view) == null) {
                        return null;
                    }
                    startInterceptRequestLayout();
                    view2 = this.mLayout.a(view, i2, this.mRecycler, this.mState);
                    stopInterceptRequestLayout(false);
                }
            } else {
                if (this.mLayout.b()) {
                    int i3 = i2 == 2 ? 130 : 33;
                    z2 = instance.findNextFocus(super, view, i3) == null;
                    if (FORCE_ABS_FOCUS_SEARCH_DIRECTION) {
                        i2 = i3;
                    }
                } else {
                    z2 = false;
                }
                if (!z2 && this.mLayout.a()) {
                    int i4 = (this.mLayout.f() == 1) ^ (i2 == 2) ? 66 : 17;
                    if (instance.findNextFocus(super, view, i4) != null) {
                        z3 = false;
                    }
                    if (FORCE_ABS_FOCUS_SEARCH_DIRECTION) {
                        i2 = i4;
                    }
                    z2 = z3;
                }
                if (z2) {
                    consumePendingUpdateOperations();
                    if (findContainingItemView(view) == null) {
                        return null;
                    }
                    startInterceptRequestLayout();
                    this.mLayout.a(view, i2, this.mRecycler, this.mState);
                    stopInterceptRequestLayout(false);
                }
                view2 = instance.findNextFocus(super, view, i2);
            }
            if (view2 == null || view2.hasFocusable()) {
                return isPreferredNextFocus(view, view2, i2) ? view2 : super.focusSearch(view, i2);
            }
            if (getFocusedChild() == null) {
                return super.focusSearch(view, i2);
            }
            requestChildOnScreen(view2, null);
            return view;
        }
        throw null;
    }

    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        o oVar = this.mLayout;
        if (oVar != null) {
            return oVar.c();
        }
        throw new IllegalStateException(outline.a(this, outline.a("RecyclerView has no LayoutManager")));
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        o oVar = this.mLayout;
        if (oVar != null) {
            return oVar.a(getContext(), attributeSet);
        }
        throw new IllegalStateException(outline.a(this, outline.a("RecyclerView has no LayoutManager")));
    }

    public CharSequence getAccessibilityClassName() {
        return "androidx.recyclerview.widget.RecyclerView";
    }

    public g getAdapter() {
        return this.mAdapter;
    }

    public int getAdapterPositionFor(d0 d0Var) {
        if (d0Var.b(524) || !d0Var.g()) {
            return -1;
        }
        AdapterHelper adapterHelper = this.mAdapterHelper;
        int i2 = d0Var.c;
        int size = adapterHelper.b.size();
        for (int i3 = 0; i3 < size; i3++) {
            AdapterHelper.b bVar = adapterHelper.b.get(i3);
            int i4 = bVar.a;
            if (i4 != 1) {
                if (i4 == 2) {
                    int i5 = bVar.b;
                    if (i5 <= i2) {
                        int i6 = bVar.d;
                        if (i5 + i6 > i2) {
                            return -1;
                        }
                        i2 -= i6;
                    } else {
                        continue;
                    }
                } else if (i4 == 8) {
                    int i7 = bVar.b;
                    if (i7 == i2) {
                        i2 = bVar.d;
                    } else {
                        if (i7 < i2) {
                            i2--;
                        }
                        if (bVar.d <= i2) {
                            i2++;
                        }
                    }
                }
            } else if (bVar.b <= i2) {
                i2 += bVar.d;
            }
        }
        return i2;
    }

    public int getBaseline() {
        o oVar = this.mLayout;
        if (oVar == null) {
            return super.getBaseline();
        }
        if (oVar != null) {
            return -1;
        }
        throw null;
    }

    public long getChangedHolderKey(d0 d0Var) {
        if (this.mAdapter.b) {
            return d0Var.f302e;
        }
        return (long) d0Var.c;
    }

    public int getChildAdapterPosition(View view) {
        d0 childViewHolderInt = getChildViewHolderInt(view);
        if (childViewHolderInt != null) {
            return childViewHolderInt.c();
        }
        return -1;
    }

    public int getChildDrawingOrder(int i2, int i3) {
        j jVar = this.mChildDrawingOrderCallback;
        if (jVar == null) {
            return super.getChildDrawingOrder(i2, i3);
        }
        return jVar.a(i2, i3);
    }

    public long getChildItemId(View view) {
        d0 childViewHolderInt;
        g gVar = this.mAdapter;
        if (gVar == null || !gVar.b || (childViewHolderInt = getChildViewHolderInt(view)) == null) {
            return -1;
        }
        return childViewHolderInt.f302e;
    }

    public int getChildLayoutPosition(View view) {
        d0 childViewHolderInt = getChildViewHolderInt(view);
        if (childViewHolderInt != null) {
            return childViewHolderInt.d();
        }
        return -1;
    }

    @Deprecated
    public int getChildPosition(View view) {
        return getChildAdapterPosition(view);
    }

    public d0 getChildViewHolder(View view) {
        ViewParent parent = view.getParent();
        if (parent == null || parent == this) {
            return getChildViewHolderInt(view);
        }
        throw new IllegalArgumentException("View " + view + " is not a direct child of " + this);
    }

    public boolean getClipToPadding() {
        return this.mClipToPadding;
    }

    public RecyclerViewAccessibilityDelegate getCompatAccessibilityDelegate() {
        return this.mAccessibilityDelegate;
    }

    public void getDecoratedBoundsWithMargins(View view, Rect rect) {
        getDecoratedBoundsWithMarginsInt(view, rect);
    }

    public k getEdgeEffectFactory() {
        return this.mEdgeEffectFactory;
    }

    public l getItemAnimator() {
        return this.mItemAnimator;
    }

    public Rect getItemDecorInsetsForChild(View view) {
        p pVar = (p) view.getLayoutParams();
        if (!pVar.c) {
            return pVar.b;
        }
        if (this.mState.g && (pVar.b() || pVar.a.h())) {
            return pVar.b;
        }
        Rect rect = pVar.b;
        rect.set(0, 0, 0, 0);
        int size = this.mItemDecorations.size();
        int i2 = 0;
        while (i2 < size) {
            this.mTempRect.set(0, 0, 0, 0);
            n nVar = this.mItemDecorations.get(i2);
            Rect rect2 = this.mTempRect;
            if (nVar != null) {
                ((p) view.getLayoutParams()).a();
                rect2.set(0, 0, 0, 0);
                int i3 = rect.left;
                Rect rect3 = this.mTempRect;
                rect.left = i3 + rect3.left;
                rect.top += rect3.top;
                rect.right += rect3.right;
                rect.bottom += rect3.bottom;
                i2++;
            } else {
                throw null;
            }
        }
        pVar.c = false;
        return rect;
    }

    public n getItemDecorationAt(int i2) {
        int itemDecorationCount = getItemDecorationCount();
        if (i2 >= 0 && i2 < itemDecorationCount) {
            return this.mItemDecorations.get(i2);
        }
        throw new IndexOutOfBoundsException(i2 + " is an invalid index for size " + itemDecorationCount);
    }

    public int getItemDecorationCount() {
        return this.mItemDecorations.size();
    }

    public o getLayoutManager() {
        return this.mLayout;
    }

    public int getMaxFlingVelocity() {
        return this.mMaxFlingVelocity;
    }

    public int getMinFlingVelocity() {
        return this.mMinFlingVelocity;
    }

    public long getNanoTime() {
        if (ALLOW_THREAD_GAP_WORK) {
            return System.nanoTime();
        }
        return 0;
    }

    public r getOnFlingListener() {
        return this.mOnFlingListener;
    }

    public boolean getPreserveFocusAfterLayout() {
        return this.mPreserveFocusAfterLayout;
    }

    public u getRecycledViewPool() {
        return this.mRecycler.b();
    }

    public int getScrollState() {
        return this.mScrollState;
    }

    public boolean hasFixedSize() {
        return this.mHasFixedSize;
    }

    public boolean hasNestedScrollingParent() {
        return getScrollingChildHelper().b(0);
    }

    public boolean hasPendingAdapterUpdates() {
        return !this.mFirstLayoutComplete || this.mDataSetHasChangedAfterLayout || this.mAdapterHelper.c();
    }

    public void initAdapterManager() {
        this.mAdapterHelper = new AdapterHelper(new f());
    }

    public void initFastScroller(StateListDrawable stateListDrawable, Drawable drawable, StateListDrawable stateListDrawable2, Drawable drawable2) {
        if (stateListDrawable == null || drawable == null || stateListDrawable2 == null || drawable2 == null) {
            throw new IllegalArgumentException(outline.a(this, outline.a("Trying to set fast scroller without both required drawables.")));
        }
        Resources resources = getContext().getResources();
        new FastScroller(this, stateListDrawable, drawable, stateListDrawable2, drawable2, resources.getDimensionPixelSize(i.r.b.fastscroll_default_thickness), resources.getDimensionPixelSize(i.r.b.fastscroll_minimum_range), resources.getDimensionPixelOffset(i.r.b.fastscroll_margin));
    }

    public void invalidateGlows() {
        this.mBottomGlow = null;
        this.mTopGlow = null;
        this.mRightGlow = null;
        this.mLeftGlow = null;
    }

    public void invalidateItemDecorations() {
        if (this.mItemDecorations.size() != 0) {
            o oVar = this.mLayout;
            if (oVar != null) {
                oVar.a("Cannot invalidate item decorations during a scroll or layout");
            }
            markItemDecorInsetsDirty();
            requestLayout();
        }
    }

    public boolean isAccessibilityEnabled() {
        AccessibilityManager accessibilityManager = this.mAccessibilityManager;
        return accessibilityManager != null && accessibilityManager.isEnabled();
    }

    public boolean isAnimating() {
        l lVar = this.mItemAnimator;
        return lVar != null && lVar.c();
    }

    public boolean isAttachedToWindow() {
        return this.mIsAttached;
    }

    public boolean isComputingLayout() {
        return this.mLayoutOrScrollCounter > 0;
    }

    @Deprecated
    public boolean isLayoutFrozen() {
        return isLayoutSuppressed();
    }

    public final boolean isLayoutSuppressed() {
        return this.mLayoutSuppressed;
    }

    public boolean isNestedScrollingEnabled() {
        return getScrollingChildHelper().d;
    }

    public void jumpToPositionForSmoothScroller(int i2) {
        if (this.mLayout != null) {
            setScrollState(2);
            this.mLayout.h(i2);
            awakenScrollBars();
        }
    }

    public void markItemDecorInsetsDirty() {
        int b2 = this.mChildHelper.b();
        for (int i2 = 0; i2 < b2; i2++) {
            ((p) this.mChildHelper.d(i2).getLayoutParams()).c = true;
        }
        v vVar = this.mRecycler;
        int size = vVar.c.size();
        for (int i3 = 0; i3 < size; i3++) {
            p pVar = (p) vVar.c.get(i3).a.getLayoutParams();
            if (pVar != null) {
                pVar.c = true;
            }
        }
    }

    public void markKnownViewsInvalid() {
        int b2 = this.mChildHelper.b();
        for (int i2 = 0; i2 < b2; i2++) {
            d0 childViewHolderInt = getChildViewHolderInt(this.mChildHelper.d(i2));
            if (childViewHolderInt != null && !childViewHolderInt.o()) {
                childViewHolderInt.a(6);
            }
        }
        markItemDecorInsetsDirty();
        v vVar = this.mRecycler;
        int size = vVar.c.size();
        for (int i3 = 0; i3 < size; i3++) {
            d0 d0Var = vVar.c.get(i3);
            if (d0Var != null) {
                d0Var.a(6);
                d0Var.a((Object) null);
            }
        }
        g gVar = RecyclerView.this.mAdapter;
        if (gVar == null || !gVar.b) {
            vVar.c();
        }
    }

    public void offsetChildrenHorizontal(int i2) {
        int a2 = this.mChildHelper.a();
        for (int i3 = 0; i3 < a2; i3++) {
            this.mChildHelper.b(i3).offsetLeftAndRight(i2);
        }
    }

    public void offsetChildrenVertical(int i2) {
        int a2 = this.mChildHelper.a();
        for (int i3 = 0; i3 < a2; i3++) {
            this.mChildHelper.b(i3).offsetTopAndBottom(i2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.RecyclerView.d0.a(int, boolean):void
     arg types: [int, int]
     candidates:
      androidx.recyclerview.widget.RecyclerView.d0.a(int, int):void
      androidx.recyclerview.widget.RecyclerView.d0.a(int, boolean):void */
    public void offsetPositionRecordsForInsert(int i2, int i3) {
        int b2 = this.mChildHelper.b();
        for (int i4 = 0; i4 < b2; i4++) {
            d0 childViewHolderInt = getChildViewHolderInt(this.mChildHelper.d(i4));
            if (childViewHolderInt != null && !childViewHolderInt.o() && childViewHolderInt.c >= i2) {
                childViewHolderInt.a(i3, false);
                this.mState.f291f = true;
            }
        }
        v vVar = this.mRecycler;
        int size = vVar.c.size();
        for (int i5 = 0; i5 < size; i5++) {
            d0 d0Var = vVar.c.get(i5);
            if (d0Var != null && d0Var.c >= i2) {
                d0Var.a(i3, true);
            }
        }
        requestLayout();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.RecyclerView.d0.a(int, boolean):void
     arg types: [int, int]
     candidates:
      androidx.recyclerview.widget.RecyclerView.d0.a(int, int):void
      androidx.recyclerview.widget.RecyclerView.d0.a(int, boolean):void */
    public void offsetPositionRecordsForMove(int i2, int i3) {
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int b2 = this.mChildHelper.b();
        int i11 = -1;
        if (i2 < i3) {
            i6 = i2;
            i5 = i3;
            i4 = -1;
        } else {
            i5 = i2;
            i6 = i3;
            i4 = 1;
        }
        for (int i12 = 0; i12 < b2; i12++) {
            d0 childViewHolderInt = getChildViewHolderInt(this.mChildHelper.d(i12));
            if (childViewHolderInt != null && (i10 = childViewHolderInt.c) >= i6 && i10 <= i5) {
                if (i10 == i2) {
                    childViewHolderInt.a(i3 - i2, false);
                } else {
                    childViewHolderInt.a(i4, false);
                }
                this.mState.f291f = true;
            }
        }
        v vVar = this.mRecycler;
        if (i2 < i3) {
            i8 = i2;
            i7 = i3;
        } else {
            i7 = i2;
            i8 = i3;
            i11 = 1;
        }
        int size = vVar.c.size();
        for (int i13 = 0; i13 < size; i13++) {
            d0 d0Var = vVar.c.get(i13);
            if (d0Var != null && (i9 = d0Var.c) >= i8 && i9 <= i7) {
                if (i9 == i2) {
                    d0Var.a(i3 - i2, false);
                } else {
                    d0Var.a(i11, false);
                }
            }
        }
        requestLayout();
    }

    public void offsetPositionRecordsForRemove(int i2, int i3, boolean z2) {
        int i4 = i2 + i3;
        int b2 = this.mChildHelper.b();
        for (int i5 = 0; i5 < b2; i5++) {
            d0 childViewHolderInt = getChildViewHolderInt(this.mChildHelper.d(i5));
            if (childViewHolderInt != null && !childViewHolderInt.o()) {
                int i6 = childViewHolderInt.c;
                if (i6 >= i4) {
                    childViewHolderInt.a(-i3, z2);
                    this.mState.f291f = true;
                } else if (i6 >= i2) {
                    childViewHolderInt.a(8);
                    childViewHolderInt.a(-i3, z2);
                    childViewHolderInt.c = i2 - 1;
                    this.mState.f291f = true;
                }
            }
        }
        v vVar = this.mRecycler;
        int size = vVar.c.size();
        while (true) {
            size--;
            if (size >= 0) {
                d0 d0Var = vVar.c.get(size);
                if (d0Var != null) {
                    int i7 = d0Var.c;
                    if (i7 >= i4) {
                        d0Var.a(-i3, z2);
                    } else if (i7 >= i2) {
                        d0Var.a(8);
                        vVar.b(size);
                    }
                }
            } else {
                requestLayout();
                return;
            }
        }
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.mLayoutOrScrollCounter = 0;
        this.mIsAttached = true;
        this.mFirstLayoutComplete = this.mFirstLayoutComplete && !isLayoutRequested();
        o oVar = this.mLayout;
        if (oVar != null) {
            oVar.f318i = true;
        }
        this.mPostedAnimatorRunner = false;
        if (ALLOW_THREAD_GAP_WORK) {
            GapWorker gapWorker = GapWorker.f1396f.get();
            this.mGapWorker = gapWorker;
            if (gapWorker == null) {
                this.mGapWorker = new GapWorker();
                Display f2 = ViewCompat.f(this);
                float f3 = 60.0f;
                if (!isInEditMode() && f2 != null) {
                    float refreshRate = f2.getRefreshRate();
                    if (refreshRate >= 30.0f) {
                        f3 = refreshRate;
                    }
                }
                GapWorker gapWorker2 = this.mGapWorker;
                gapWorker2.d = (long) (1.0E9f / f3);
                GapWorker.f1396f.set(gapWorker2);
            }
            this.mGapWorker.b.add(this);
        }
    }

    public void onChildAttachedToWindow(View view) {
    }

    public void onChildDetachedFromWindow(View view) {
    }

    public void onDetachedFromWindow() {
        GapWorker gapWorker;
        super.onDetachedFromWindow();
        l lVar = this.mItemAnimator;
        if (lVar != null) {
            lVar.b();
        }
        stopScroll();
        this.mIsAttached = false;
        o oVar = this.mLayout;
        if (oVar != null) {
            v vVar = this.mRecycler;
            oVar.f318i = false;
            oVar.a(this, vVar);
        }
        this.mPendingAccessibilityImportanceChange.clear();
        removeCallbacks(this.mItemAnimatorRunner);
        if (this.mViewInfoStore != null) {
            do {
            } while (ViewInfoStore.a.d.a() != null);
            if (ALLOW_THREAD_GAP_WORK && (gapWorker = this.mGapWorker) != null) {
                gapWorker.b.remove(this);
                this.mGapWorker = null;
                return;
            }
            return;
        }
        throw null;
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int size = this.mItemDecorations.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.mItemDecorations.get(i2).a(canvas, this, this.mState);
        }
    }

    public void onEnterLayoutOrScroll() {
        this.mLayoutOrScrollCounter++;
    }

    public void onExitLayoutOrScroll() {
        onExitLayoutOrScroll(true);
    }

    public boolean onGenericMotionEvent(MotionEvent motionEvent) {
        float f2;
        float f3;
        if (this.mLayout != null && !this.mLayoutSuppressed && motionEvent.getAction() == 8) {
            if ((motionEvent.getSource() & 2) != 0) {
                f3 = this.mLayout.b() ? -motionEvent.getAxisValue(9) : 0.0f;
                if (this.mLayout.a()) {
                    f2 = motionEvent.getAxisValue(10);
                    if (!(f3 == 0.0f && f2 == 0.0f)) {
                        scrollByInternal((int) (f2 * this.mScaledHorizontalScrollFactor), (int) (f3 * this.mScaledVerticalScrollFactor), motionEvent);
                    }
                }
            } else {
                if ((motionEvent.getSource() & 4194304) != 0) {
                    float axisValue = motionEvent.getAxisValue(26);
                    if (this.mLayout.b()) {
                        f3 = -axisValue;
                    } else if (this.mLayout.a()) {
                        f2 = axisValue;
                        f3 = 0.0f;
                        scrollByInternal((int) (f2 * this.mScaledHorizontalScrollFactor), (int) (f3 * this.mScaledVerticalScrollFactor), motionEvent);
                    }
                }
                f3 = 0.0f;
            }
            f2 = 0.0f;
            scrollByInternal((int) (f2 * this.mScaledHorizontalScrollFactor), (int) (f3 * this.mScaledVerticalScrollFactor), motionEvent);
        }
        return false;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z2;
        if (this.mLayoutSuppressed) {
            return false;
        }
        this.mInterceptingOnItemTouchListener = null;
        if (findInterceptingOnItemTouchListener(motionEvent)) {
            cancelScroll();
            return true;
        }
        o oVar = this.mLayout;
        if (oVar == null) {
            return false;
        }
        boolean a2 = oVar.a();
        boolean b2 = this.mLayout.b();
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
        }
        this.mVelocityTracker.addMovement(motionEvent);
        int actionMasked = motionEvent.getActionMasked();
        int actionIndex = motionEvent.getActionIndex();
        if (actionMasked == 0) {
            if (this.mIgnoreMotionEventTillDown) {
                this.mIgnoreMotionEventTillDown = false;
            }
            this.mScrollPointerId = motionEvent.getPointerId(0);
            int x2 = (int) (motionEvent.getX() + 0.5f);
            this.mLastTouchX = x2;
            this.mInitialTouchX = x2;
            int y2 = (int) (motionEvent.getY() + 0.5f);
            this.mLastTouchY = y2;
            this.mInitialTouchY = y2;
            if (this.mScrollState == 2) {
                getParent().requestDisallowInterceptTouchEvent(true);
                setScrollState(1);
                stopNestedScroll(1);
            }
            int[] iArr = this.mNestedOffsets;
            iArr[1] = 0;
            iArr[0] = 0;
            int i2 = a2 ? 1 : 0;
            if (b2) {
                i2 |= 2;
            }
            startNestedScroll(i2, 0);
        } else if (actionMasked == 1) {
            this.mVelocityTracker.clear();
            stopNestedScroll(0);
        } else if (actionMasked == 2) {
            int findPointerIndex = motionEvent.findPointerIndex(this.mScrollPointerId);
            if (findPointerIndex < 0) {
                StringBuilder a3 = outline.a("Error processing scroll; pointer index for id ");
                a3.append(this.mScrollPointerId);
                a3.append(" not found. Did any MotionEvents get skipped?");
                Log.e(TAG, a3.toString());
                return false;
            }
            int x3 = (int) (motionEvent.getX(findPointerIndex) + 0.5f);
            int y3 = (int) (motionEvent.getY(findPointerIndex) + 0.5f);
            if (this.mScrollState != 1) {
                int i3 = x3 - this.mInitialTouchX;
                int i4 = y3 - this.mInitialTouchY;
                if (!a2 || Math.abs(i3) <= this.mTouchSlop) {
                    z2 = false;
                } else {
                    this.mLastTouchX = x3;
                    z2 = true;
                }
                if (b2 && Math.abs(i4) > this.mTouchSlop) {
                    this.mLastTouchY = y3;
                    z2 = true;
                }
                if (z2) {
                    setScrollState(1);
                }
            }
        } else if (actionMasked == 3) {
            cancelScroll();
        } else if (actionMasked == 5) {
            this.mScrollPointerId = motionEvent.getPointerId(actionIndex);
            int x4 = (int) (motionEvent.getX(actionIndex) + 0.5f);
            this.mLastTouchX = x4;
            this.mInitialTouchX = x4;
            int y4 = (int) (motionEvent.getY(actionIndex) + 0.5f);
            this.mLastTouchY = y4;
            this.mInitialTouchY = y4;
        } else if (actionMasked == 6) {
            onPointerUp(motionEvent);
        }
        if (this.mScrollState == 1) {
            return true;
        }
        return false;
    }

    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        TraceCompat.a(TRACE_ON_LAYOUT_TAG);
        dispatchLayout();
        Trace.endSection();
        this.mFirstLayoutComplete = true;
    }

    public void onMeasure(int i2, int i3) {
        o oVar = this.mLayout;
        if (oVar == null) {
            defaultOnMeasure(i2, i3);
            return;
        }
        boolean z2 = false;
        if (oVar.m()) {
            int mode = View.MeasureSpec.getMode(i2);
            int mode2 = View.MeasureSpec.getMode(i3);
            this.mLayout.b.defaultOnMeasure(i2, i3);
            if (mode == 1073741824 && mode2 == 1073741824) {
                z2 = true;
            }
            if (!z2 && this.mAdapter != null) {
                if (this.mState.d == 1) {
                    dispatchLayoutStep1();
                }
                this.mLayout.a(i2, i3);
                this.mState.f292i = true;
                dispatchLayoutStep2();
                this.mLayout.b(i2, i3);
                if (this.mLayout.p()) {
                    this.mLayout.a(View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(getMeasuredHeight(), 1073741824));
                    this.mState.f292i = true;
                    dispatchLayoutStep2();
                    this.mLayout.b(i2, i3);
                }
            }
        } else if (this.mHasFixedSize) {
            this.mLayout.b.defaultOnMeasure(i2, i3);
        } else {
            if (this.mAdapterUpdateDuringMeasure) {
                startInterceptRequestLayout();
                onEnterLayoutOrScroll();
                processAdapterUpdatesAndSetAnimationFlags();
                onExitLayoutOrScroll();
                a0 a0Var = this.mState;
                if (a0Var.f294k) {
                    a0Var.g = true;
                } else {
                    this.mAdapterHelper.b();
                    this.mState.g = false;
                }
                this.mAdapterUpdateDuringMeasure = false;
                stopInterceptRequestLayout(false);
            } else if (this.mState.f294k) {
                setMeasuredDimension(getMeasuredWidth(), getMeasuredHeight());
                return;
            }
            g gVar = this.mAdapter;
            if (gVar != null) {
                this.mState.f290e = gVar.a();
            } else {
                this.mState.f290e = 0;
            }
            startInterceptRequestLayout();
            this.mLayout.b.defaultOnMeasure(i2, i3);
            stopInterceptRequestLayout(false);
            this.mState.g = false;
        }
    }

    public boolean onRequestFocusInDescendants(int i2, Rect rect) {
        if (isComputingLayout()) {
            return false;
        }
        return super.onRequestFocusInDescendants(i2, rect);
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        Parcelable parcelable2;
        if (!(parcelable instanceof y)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        y yVar = (y) parcelable;
        this.mPendingSavedState = yVar;
        super.onRestoreInstanceState(yVar.b);
        o oVar = this.mLayout;
        if (oVar != null && (parcelable2 = this.mPendingSavedState.d) != null) {
            oVar.a(parcelable2);
        }
    }

    public Parcelable onSaveInstanceState() {
        y yVar = new y(super.onSaveInstanceState());
        y yVar2 = this.mPendingSavedState;
        if (yVar2 != null) {
            yVar.d = yVar2.d;
        } else {
            o oVar = this.mLayout;
            if (oVar != null) {
                yVar.d = oVar.n();
            } else {
                yVar.d = null;
            }
        }
        return yVar;
    }

    public void onScrollStateChanged(int i2) {
    }

    public void onScrolled(int i2, int i3) {
    }

    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (i2 != i4 || i3 != i5) {
            invalidateGlows();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:45:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00f4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r18) {
        /*
            r17 = this;
            r6 = r17
            r7 = r18
            boolean r0 = r6.mLayoutSuppressed
            r8 = 0
            if (r0 != 0) goto L_0x01e6
            boolean r0 = r6.mIgnoreMotionEventTillDown
            if (r0 == 0) goto L_0x000f
            goto L_0x01e6
        L_0x000f:
            boolean r0 = r17.dispatchToOnItemTouchListeners(r18)
            r9 = 1
            if (r0 == 0) goto L_0x001a
            r17.cancelScroll()
            return r9
        L_0x001a:
            androidx.recyclerview.widget.RecyclerView$o r0 = r6.mLayout
            if (r0 != 0) goto L_0x001f
            return r8
        L_0x001f:
            boolean r10 = r0.a()
            androidx.recyclerview.widget.RecyclerView$o r0 = r6.mLayout
            boolean r11 = r0.b()
            android.view.VelocityTracker r0 = r6.mVelocityTracker
            if (r0 != 0) goto L_0x0033
            android.view.VelocityTracker r0 = android.view.VelocityTracker.obtain()
            r6.mVelocityTracker = r0
        L_0x0033:
            int r0 = r18.getActionMasked()
            int r1 = r18.getActionIndex()
            if (r0 != 0) goto L_0x0043
            int[] r2 = r6.mNestedOffsets
            r2[r9] = r8
            r2[r8] = r8
        L_0x0043:
            android.view.MotionEvent r12 = android.view.MotionEvent.obtain(r18)
            int[] r2 = r6.mNestedOffsets
            r3 = r2[r8]
            float r3 = (float) r3
            r2 = r2[r9]
            float r2 = (float) r2
            r12.offsetLocation(r3, r2)
            r2 = 1056964608(0x3f000000, float:0.5)
            if (r0 == 0) goto L_0x01b5
            if (r0 == r9) goto L_0x0173
            r3 = 2
            if (r0 == r3) goto L_0x008c
            r3 = 3
            if (r0 == r3) goto L_0x0087
            r3 = 5
            if (r0 == r3) goto L_0x006b
            r1 = 6
            if (r0 == r1) goto L_0x0066
            goto L_0x01db
        L_0x0066:
            r17.onPointerUp(r18)
            goto L_0x01db
        L_0x006b:
            int r0 = r7.getPointerId(r1)
            r6.mScrollPointerId = r0
            float r0 = r7.getX(r1)
            float r0 = r0 + r2
            int r0 = (int) r0
            r6.mLastTouchX = r0
            r6.mInitialTouchX = r0
            float r0 = r7.getY(r1)
            float r0 = r0 + r2
            int r0 = (int) r0
            r6.mLastTouchY = r0
            r6.mInitialTouchY = r0
            goto L_0x01db
        L_0x0087:
            r17.cancelScroll()
            goto L_0x01db
        L_0x008c:
            int r0 = r6.mScrollPointerId
            int r0 = r7.findPointerIndex(r0)
            if (r0 >= 0) goto L_0x00ae
            java.lang.String r0 = "Error processing scroll; pointer index for id "
            java.lang.StringBuilder r0 = j.a.a.a.outline.a(r0)
            int r1 = r6.mScrollPointerId
            r0.append(r1)
            java.lang.String r1 = " not found. Did any MotionEvents get skipped?"
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.lang.String r1 = "RecyclerView"
            android.util.Log.e(r1, r0)
            return r8
        L_0x00ae:
            float r1 = r7.getX(r0)
            float r1 = r1 + r2
            int r13 = (int) r1
            float r0 = r7.getY(r0)
            float r0 = r0 + r2
            int r14 = (int) r0
            int r0 = r6.mLastTouchX
            int r0 = r0 - r13
            int r1 = r6.mLastTouchY
            int r1 = r1 - r14
            int r2 = r6.mScrollState
            if (r2 == r9) goto L_0x00f7
            if (r10 == 0) goto L_0x00db
            if (r0 <= 0) goto L_0x00d0
            int r2 = r6.mTouchSlop
            int r0 = r0 - r2
            int r0 = java.lang.Math.max(r8, r0)
            goto L_0x00d7
        L_0x00d0:
            int r2 = r6.mTouchSlop
            int r0 = r0 + r2
            int r0 = java.lang.Math.min(r8, r0)
        L_0x00d7:
            if (r0 == 0) goto L_0x00db
            r2 = 1
            goto L_0x00dc
        L_0x00db:
            r2 = 0
        L_0x00dc:
            if (r11 == 0) goto L_0x00f2
            if (r1 <= 0) goto L_0x00e8
            int r3 = r6.mTouchSlop
            int r1 = r1 - r3
            int r1 = java.lang.Math.max(r8, r1)
            goto L_0x00ef
        L_0x00e8:
            int r3 = r6.mTouchSlop
            int r1 = r1 + r3
            int r1 = java.lang.Math.min(r8, r1)
        L_0x00ef:
            if (r1 == 0) goto L_0x00f2
            r2 = 1
        L_0x00f2:
            if (r2 == 0) goto L_0x00f7
            r6.setScrollState(r9)
        L_0x00f7:
            r15 = r0
            r16 = r1
            int r0 = r6.mScrollState
            if (r0 != r9) goto L_0x01db
            int[] r0 = r6.mReusableIntPair
            r0[r8] = r8
            r0[r9] = r8
            if (r10 == 0) goto L_0x0108
            r1 = r15
            goto L_0x010a
        L_0x0108:
            r0 = 0
            r1 = 0
        L_0x010a:
            if (r11 == 0) goto L_0x010f
            r2 = r16
            goto L_0x0111
        L_0x010f:
            r0 = 0
            r2 = 0
        L_0x0111:
            int[] r3 = r6.mReusableIntPair
            int[] r4 = r6.mScrollOffset
            r5 = 0
            r0 = r17
            boolean r0 = r0.dispatchNestedPreScroll(r1, r2, r3, r4, r5)
            if (r0 == 0) goto L_0x0140
            int[] r0 = r6.mReusableIntPair
            r1 = r0[r8]
            int r15 = r15 - r1
            r0 = r0[r9]
            int r16 = r16 - r0
            int[] r0 = r6.mNestedOffsets
            r1 = r0[r8]
            int[] r2 = r6.mScrollOffset
            r3 = r2[r8]
            int r1 = r1 + r3
            r0[r8] = r1
            r1 = r0[r9]
            r2 = r2[r9]
            int r1 = r1 + r2
            r0[r9] = r1
            android.view.ViewParent r0 = r17.getParent()
            r0.requestDisallowInterceptTouchEvent(r9)
        L_0x0140:
            r0 = r16
            int[] r1 = r6.mScrollOffset
            r2 = r1[r8]
            int r13 = r13 - r2
            r6.mLastTouchX = r13
            r1 = r1[r9]
            int r14 = r14 - r1
            r6.mLastTouchY = r14
            if (r10 == 0) goto L_0x0152
            r1 = r15
            goto L_0x0153
        L_0x0152:
            r1 = 0
        L_0x0153:
            if (r11 == 0) goto L_0x0157
            r2 = r0
            goto L_0x0158
        L_0x0157:
            r2 = 0
        L_0x0158:
            boolean r1 = r6.scrollByInternal(r1, r2, r7)
            if (r1 == 0) goto L_0x0165
            android.view.ViewParent r1 = r17.getParent()
            r1.requestDisallowInterceptTouchEvent(r9)
        L_0x0165:
            i.r.d.GapWorker r1 = r6.mGapWorker
            if (r1 == 0) goto L_0x01db
            if (r15 != 0) goto L_0x016d
            if (r0 == 0) goto L_0x01db
        L_0x016d:
            i.r.d.GapWorker r1 = r6.mGapWorker
            r1.a(r6, r15, r0)
            goto L_0x01db
        L_0x0173:
            android.view.VelocityTracker r0 = r6.mVelocityTracker
            r0.addMovement(r12)
            android.view.VelocityTracker r0 = r6.mVelocityTracker
            r1 = 1000(0x3e8, float:1.401E-42)
            int r2 = r6.mMaxFlingVelocity
            float r2 = (float) r2
            r0.computeCurrentVelocity(r1, r2)
            r0 = 0
            if (r10 == 0) goto L_0x018f
            android.view.VelocityTracker r1 = r6.mVelocityTracker
            int r2 = r6.mScrollPointerId
            float r1 = r1.getXVelocity(r2)
            float r1 = -r1
            goto L_0x0190
        L_0x018f:
            r1 = 0
        L_0x0190:
            if (r11 == 0) goto L_0x019c
            android.view.VelocityTracker r2 = r6.mVelocityTracker
            int r3 = r6.mScrollPointerId
            float r2 = r2.getYVelocity(r3)
            float r2 = -r2
            goto L_0x019d
        L_0x019c:
            r2 = 0
        L_0x019d:
            int r3 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r3 != 0) goto L_0x01a5
            int r0 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r0 == 0) goto L_0x01ad
        L_0x01a5:
            int r0 = (int) r1
            int r1 = (int) r2
            boolean r0 = r6.fling(r0, r1)
            if (r0 != 0) goto L_0x01b0
        L_0x01ad:
            r6.setScrollState(r8)
        L_0x01b0:
            r17.resetScroll()
            r8 = 1
            goto L_0x01db
        L_0x01b5:
            int r0 = r7.getPointerId(r8)
            r6.mScrollPointerId = r0
            float r0 = r18.getX()
            float r0 = r0 + r2
            int r0 = (int) r0
            r6.mLastTouchX = r0
            r6.mInitialTouchX = r0
            float r0 = r18.getY()
            float r0 = r0 + r2
            int r0 = (int) r0
            r6.mLastTouchY = r0
            r6.mInitialTouchY = r0
            if (r10 == 0) goto L_0x01d3
            r0 = 1
            goto L_0x01d4
        L_0x01d3:
            r0 = 0
        L_0x01d4:
            if (r11 == 0) goto L_0x01d8
            r0 = r0 | 2
        L_0x01d8:
            r6.startNestedScroll(r0, r8)
        L_0x01db:
            if (r8 != 0) goto L_0x01e2
            android.view.VelocityTracker r0 = r6.mVelocityTracker
            r0.addMovement(r12)
        L_0x01e2:
            r12.recycle()
            return r9
        L_0x01e6:
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public void postAnimationRunner() {
        if (!this.mPostedAnimatorRunner && this.mIsAttached) {
            ViewCompat.a(this, this.mItemAnimatorRunner);
            this.mPostedAnimatorRunner = true;
        }
    }

    public void processDataSetCompletelyChanged(boolean z2) {
        this.mDispatchItemsChangedEvent = z2 | this.mDispatchItemsChangedEvent;
        this.mDataSetHasChangedAfterLayout = true;
        markKnownViewsInvalid();
    }

    public void recordAnimationInfoIfBouncedHiddenView(d0 d0Var, l.c cVar) {
        d0Var.a(0, 8192);
        if (this.mState.h && d0Var.m() && !d0Var.j() && !d0Var.o()) {
            this.mViewInfoStore.b.c(getChangedHolderKey(d0Var), d0Var);
        }
        this.mViewInfoStore.b(d0Var, cVar);
    }

    public void removeAndRecycleViews() {
        l lVar = this.mItemAnimator;
        if (lVar != null) {
            lVar.b();
        }
        o oVar = this.mLayout;
        if (oVar != null) {
            oVar.b(this.mRecycler);
            this.mLayout.c(this.mRecycler);
        }
        this.mRecycler.a();
    }

    public boolean removeAnimatingView(View view) {
        startInterceptRequestLayout();
        ChildHelper childHelper = this.mChildHelper;
        int indexOfChild = RecyclerView.this.indexOfChild(view);
        boolean z2 = true;
        if (indexOfChild == -1) {
            childHelper.d(view);
        } else if (childHelper.b.c(indexOfChild)) {
            childHelper.b.d(indexOfChild);
            childHelper.d(view);
            ((e) childHelper.a).b(indexOfChild);
        } else {
            z2 = false;
        }
        if (z2) {
            d0 childViewHolderInt = getChildViewHolderInt(view);
            this.mRecycler.b(childViewHolderInt);
            this.mRecycler.a(childViewHolderInt);
        }
        stopInterceptRequestLayout(!z2);
        return z2;
    }

    public void removeDetachedView(View view, boolean z2) {
        d0 childViewHolderInt = getChildViewHolderInt(view);
        if (childViewHolderInt != null) {
            if (childViewHolderInt.l()) {
                childViewHolderInt.f305j &= -257;
            } else if (!childViewHolderInt.o()) {
                StringBuilder sb = new StringBuilder();
                sb.append("Called removeDetachedView with a view which is not flagged as tmp detached.");
                sb.append(childViewHolderInt);
                throw new IllegalArgumentException(outline.a(this, sb));
            }
        }
        view.clearAnimation();
        dispatchChildDetached(view);
        super.removeDetachedView(view, z2);
    }

    public void removeItemDecoration(n nVar) {
        o oVar = this.mLayout;
        if (oVar != null) {
            oVar.a("Cannot remove item decoration during a scroll  or layout");
        }
        this.mItemDecorations.remove(nVar);
        if (this.mItemDecorations.isEmpty()) {
            setWillNotDraw(getOverScrollMode() == 2);
        }
        markItemDecorInsetsDirty();
        requestLayout();
    }

    public void removeItemDecorationAt(int i2) {
        int itemDecorationCount = getItemDecorationCount();
        if (i2 < 0 || i2 >= itemDecorationCount) {
            throw new IndexOutOfBoundsException(i2 + " is an invalid index for size " + itemDecorationCount);
        }
        removeItemDecoration(getItemDecorationAt(i2));
    }

    public void removeOnChildAttachStateChangeListener(q qVar) {
        List<q> list = this.mOnChildAttachStateListeners;
        if (list != null) {
            list.remove(qVar);
        }
    }

    public void removeOnItemTouchListener(s sVar) {
        this.mOnItemTouchListeners.remove(sVar);
        if (this.mInterceptingOnItemTouchListener == sVar) {
            this.mInterceptingOnItemTouchListener = null;
        }
    }

    public void removeOnScrollListener(t tVar) {
        List<t> list = this.mScrollListeners;
        if (list != null) {
            list.remove(tVar);
        }
    }

    public void repositionShadowingViews() {
        d0 d0Var;
        int a2 = this.mChildHelper.a();
        for (int i2 = 0; i2 < a2; i2++) {
            View b2 = this.mChildHelper.b(i2);
            d0 childViewHolder = getChildViewHolder(b2);
            if (!(childViewHolder == null || (d0Var = childViewHolder.f304i) == null)) {
                View view = d0Var.a;
                int left = b2.getLeft();
                int top = b2.getTop();
                if (left != view.getLeft() || top != view.getTop()) {
                    view.layout(left, top, view.getWidth() + left, view.getHeight() + top);
                }
            }
        }
    }

    public void requestChildFocus(View view, View view2) {
        z zVar = this.mLayout.g;
        boolean z2 = true;
        if (!(zVar != null && zVar.f330e) && !isComputingLayout()) {
            z2 = false;
        }
        if (!z2 && view2 != null) {
            requestChildOnScreen(view, view2);
        }
        super.requestChildFocus(view, view2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean, boolean):boolean
     arg types: [androidx.recyclerview.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean, int]
     candidates:
      androidx.recyclerview.widget.RecyclerView.o.a(int, int, int, int, boolean):int
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, int, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean, boolean):boolean */
    public boolean requestChildRectangleOnScreen(View view, Rect rect, boolean z2) {
        return this.mLayout.a(this, view, rect, z2, false);
    }

    public void requestDisallowInterceptTouchEvent(boolean z2) {
        int size = this.mOnItemTouchListeners.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.mOnItemTouchListeners.get(i2).a(z2);
        }
        super.requestDisallowInterceptTouchEvent(z2);
    }

    public void requestLayout() {
        if (this.mInterceptRequestLayoutDepth != 0 || this.mLayoutSuppressed) {
            this.mLayoutWasDefered = true;
        } else {
            super.requestLayout();
        }
    }

    public void saveOldPositions() {
        int b2 = this.mChildHelper.b();
        for (int i2 = 0; i2 < b2; i2++) {
            d0 childViewHolderInt = getChildViewHolderInt(this.mChildHelper.d(i2));
            if (!childViewHolderInt.o() && childViewHolderInt.d == -1) {
                childViewHolderInt.d = childViewHolderInt.c;
            }
        }
    }

    public void scrollBy(int i2, int i3) {
        o oVar = this.mLayout;
        if (oVar == null) {
            Log.e(TAG, "Cannot scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
        } else if (!this.mLayoutSuppressed) {
            boolean a2 = oVar.a();
            boolean b2 = this.mLayout.b();
            if (a2 || b2) {
                if (!a2) {
                    i2 = 0;
                }
                if (!b2) {
                    i3 = 0;
                }
                scrollByInternal(i2, i3, null);
            }
        }
    }

    public boolean scrollByInternal(int i2, int i3, MotionEvent motionEvent) {
        int i4;
        int i5;
        int i6;
        int i7;
        int i8 = i2;
        int i9 = i3;
        consumePendingUpdateOperations();
        if (this.mAdapter != null) {
            int[] iArr = this.mReusableIntPair;
            iArr[0] = 0;
            iArr[1] = 0;
            scrollStep(i8, i9, iArr);
            int[] iArr2 = this.mReusableIntPair;
            int i10 = iArr2[0];
            int i11 = iArr2[1];
            i7 = i11;
            i6 = i10;
            i5 = i8 - i10;
            i4 = i9 - i11;
        } else {
            i7 = 0;
            i6 = 0;
            i5 = 0;
            i4 = 0;
        }
        if (!this.mItemDecorations.isEmpty()) {
            invalidate();
        }
        int[] iArr3 = this.mReusableIntPair;
        iArr3[0] = 0;
        iArr3[1] = 0;
        dispatchNestedScroll(i6, i7, i5, i4, this.mScrollOffset, 0, iArr3);
        int[] iArr4 = this.mReusableIntPair;
        int i12 = i5 - iArr4[0];
        int i13 = i4 - iArr4[1];
        boolean z2 = (iArr4[0] == 0 && iArr4[1] == 0) ? false : true;
        int i14 = this.mLastTouchX;
        int[] iArr5 = this.mScrollOffset;
        this.mLastTouchX = i14 - iArr5[0];
        this.mLastTouchY -= iArr5[1];
        int[] iArr6 = this.mNestedOffsets;
        iArr6[0] = iArr6[0] + iArr5[0];
        iArr6[1] = iArr6[1] + iArr5[1];
        if (getOverScrollMode() != 2) {
            if (motionEvent != null) {
                if (!((motionEvent.getSource() & 8194) == 8194)) {
                    pullGlows(motionEvent.getX(), (float) i12, motionEvent.getY(), (float) i13);
                }
            }
            considerReleasingGlowsOnScroll(i2, i3);
        }
        if (!(i6 == 0 && i7 == 0)) {
            dispatchOnScrolled(i6, i7);
        }
        if (!awakenScrollBars()) {
            invalidate();
        }
        if (!z2 && i6 == 0 && i7 == 0) {
            return false;
        }
        return true;
    }

    public void scrollStep(int i2, int i3, int[] iArr) {
        startInterceptRequestLayout();
        onEnterLayoutOrScroll();
        TraceCompat.a(TRACE_SCROLL_TAG);
        fillRemainingScrollValues(this.mState);
        int a2 = i2 != 0 ? this.mLayout.a(i2, this.mRecycler, this.mState) : 0;
        int b2 = i3 != 0 ? this.mLayout.b(i3, this.mRecycler, this.mState) : 0;
        Trace.endSection();
        repositionShadowingViews();
        onExitLayoutOrScroll();
        stopInterceptRequestLayout(false);
        if (iArr != null) {
            iArr[0] = a2;
            iArr[1] = b2;
        }
    }

    public void scrollTo(int i2, int i3) {
        Log.w(TAG, "RecyclerView does not support scrolling to an absolute position. Use scrollToPosition instead");
    }

    public void scrollToPosition(int i2) {
        if (!this.mLayoutSuppressed) {
            stopScroll();
            o oVar = this.mLayout;
            if (oVar == null) {
                Log.e(TAG, "Cannot scroll to position a LayoutManager set. Call setLayoutManager with a non-null argument.");
                return;
            }
            oVar.h(i2);
            awakenScrollBars();
        }
    }

    public void sendAccessibilityEventUnchecked(AccessibilityEvent accessibilityEvent) {
        if (!shouldDeferAccessibilityEvent(accessibilityEvent)) {
            super.sendAccessibilityEventUnchecked(accessibilityEvent);
        }
    }

    public void setAccessibilityDelegateCompat(RecyclerViewAccessibilityDelegate recyclerViewAccessibilityDelegate) {
        this.mAccessibilityDelegate = recyclerViewAccessibilityDelegate;
        ViewCompat.a(this, recyclerViewAccessibilityDelegate);
    }

    public void setAdapter(g gVar) {
        setLayoutFrozen(false);
        setAdapterInternal(gVar, false, true);
        processDataSetCompletelyChanged(false);
        requestLayout();
    }

    public void setChildDrawingOrderCallback(j jVar) {
        if (jVar != this.mChildDrawingOrderCallback) {
            this.mChildDrawingOrderCallback = jVar;
            setChildrenDrawingOrderEnabled(jVar != null);
        }
    }

    public boolean setChildImportantForAccessibilityInternal(d0 d0Var, int i2) {
        if (isComputingLayout()) {
            d0Var.f312q = i2;
            this.mPendingAccessibilityImportanceChange.add(d0Var);
            return false;
        }
        ViewCompat.h(d0Var.a, i2);
        return true;
    }

    public void setClipToPadding(boolean z2) {
        if (z2 != this.mClipToPadding) {
            invalidateGlows();
        }
        this.mClipToPadding = z2;
        super.setClipToPadding(z2);
        if (this.mFirstLayoutComplete) {
            requestLayout();
        }
    }

    public void setEdgeEffectFactory(k kVar) {
        if (kVar != null) {
            this.mEdgeEffectFactory = kVar;
            invalidateGlows();
            return;
        }
        throw null;
    }

    public void setHasFixedSize(boolean z2) {
        this.mHasFixedSize = z2;
    }

    public void setItemAnimator(l lVar) {
        l lVar2 = this.mItemAnimator;
        if (lVar2 != null) {
            lVar2.b();
            this.mItemAnimator.a = null;
        }
        this.mItemAnimator = lVar;
        if (lVar != null) {
            lVar.a = this.mItemAnimatorListener;
        }
    }

    public void setItemViewCacheSize(int i2) {
        v vVar = this.mRecycler;
        vVar.f328e = i2;
        vVar.d();
    }

    @Deprecated
    public void setLayoutFrozen(boolean z2) {
        suppressLayout(z2);
    }

    public void setLayoutManager(o oVar) {
        if (oVar != this.mLayout) {
            stopScroll();
            if (this.mLayout != null) {
                l lVar = this.mItemAnimator;
                if (lVar != null) {
                    lVar.b();
                }
                this.mLayout.b(this.mRecycler);
                this.mLayout.c(this.mRecycler);
                this.mRecycler.a();
                if (this.mIsAttached) {
                    o oVar2 = this.mLayout;
                    v vVar = this.mRecycler;
                    oVar2.f318i = false;
                    oVar2.a(this, vVar);
                }
                this.mLayout.c((RecyclerView) null);
                this.mLayout = null;
            } else {
                this.mRecycler.a();
            }
            ChildHelper childHelper = this.mChildHelper;
            ChildHelper.a aVar = childHelper.b;
            aVar.a = 0;
            ChildHelper.a aVar2 = aVar.b;
            if (aVar2 != null) {
                aVar2.b();
            }
            int size = childHelper.c.size();
            while (true) {
                size--;
                if (size >= 0) {
                    ChildHelper.b bVar = childHelper.a;
                    View view = childHelper.c.get(size);
                    e eVar = (e) bVar;
                    if (eVar != null) {
                        d0 childViewHolderInt = getChildViewHolderInt(view);
                        if (childViewHolderInt != null) {
                            RecyclerView.this.setChildImportantForAccessibilityInternal(childViewHolderInt, childViewHolderInt.f311p);
                            childViewHolderInt.f311p = 0;
                        }
                        childHelper.c.remove(size);
                    } else {
                        throw null;
                    }
                } else {
                    e eVar2 = (e) childHelper.a;
                    int a2 = eVar2.a();
                    for (int i2 = 0; i2 < a2; i2++) {
                        View a3 = eVar2.a(i2);
                        RecyclerView.this.dispatchChildDetached(a3);
                        a3.clearAnimation();
                    }
                    RecyclerView.this.removeAllViews();
                    this.mLayout = oVar;
                    if (oVar != null) {
                        if (oVar.b == null) {
                            oVar.c(this);
                            if (this.mIsAttached) {
                                this.mLayout.f318i = true;
                            }
                        } else {
                            StringBuilder sb = new StringBuilder();
                            sb.append("LayoutManager ");
                            sb.append(oVar);
                            sb.append(" is already attached to a RecyclerView:");
                            throw new IllegalArgumentException(outline.a(oVar.b, sb));
                        }
                    }
                    this.mRecycler.d();
                    requestLayout();
                    return;
                }
            }
        }
    }

    @Deprecated
    public void setLayoutTransition(LayoutTransition layoutTransition) {
        if (layoutTransition == null) {
            super.setLayoutTransition(null);
            return;
        }
        throw new IllegalArgumentException("Providing a LayoutTransition into RecyclerView is not supported. Please use setItemAnimator() instead for animating changes to the items in this RecyclerView");
    }

    public void setNestedScrollingEnabled(boolean z2) {
        NestedScrollingChildHelper scrollingChildHelper = getScrollingChildHelper();
        if (scrollingChildHelper.d) {
            ViewCompat.C(scrollingChildHelper.c);
        }
        scrollingChildHelper.d = z2;
    }

    public void setOnFlingListener(r rVar) {
        this.mOnFlingListener = rVar;
    }

    @Deprecated
    public void setOnScrollListener(t tVar) {
        this.mScrollListener = tVar;
    }

    public void setPreserveFocusAfterLayout(boolean z2) {
        this.mPreserveFocusAfterLayout = z2;
    }

    public void setRecycledViewPool(u uVar) {
        v vVar = this.mRecycler;
        u uVar2 = vVar.g;
        if (uVar2 != null) {
            uVar2.b--;
        }
        vVar.g = uVar;
        if (uVar != null && RecyclerView.this.getAdapter() != null) {
            vVar.g.b++;
        }
    }

    public void setRecyclerListener(w wVar) {
        this.mRecyclerListener = wVar;
    }

    public void setScrollState(int i2) {
        if (i2 != this.mScrollState) {
            this.mScrollState = i2;
            if (i2 != 2) {
                stopScrollersInternal();
            }
            dispatchOnScrollStateChanged(i2);
        }
    }

    public void setScrollingTouchSlop(int i2) {
        ViewConfiguration viewConfiguration = ViewConfiguration.get(getContext());
        if (i2 != 0) {
            if (i2 != 1) {
                Log.w(TAG, "setScrollingTouchSlop(): bad argument constant " + i2 + "; using default value");
            } else {
                this.mTouchSlop = viewConfiguration.getScaledPagingTouchSlop();
                return;
            }
        }
        this.mTouchSlop = viewConfiguration.getScaledTouchSlop();
    }

    public void setViewCacheExtension(b0 b0Var) {
    }

    public boolean shouldDeferAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        int i2 = 0;
        if (!isComputingLayout()) {
            return false;
        }
        int contentChangeTypes = accessibilityEvent != null ? accessibilityEvent.getContentChangeTypes() : 0;
        if (contentChangeTypes != 0) {
            i2 = contentChangeTypes;
        }
        this.mEatenAccessibilityChangeFlags |= i2;
        return true;
    }

    public void smoothScrollBy(int i2, int i3) {
        smoothScrollBy(i2, i3, null);
    }

    public void smoothScrollToPosition(int i2) {
        if (!this.mLayoutSuppressed) {
            o oVar = this.mLayout;
            if (oVar == null) {
                Log.e(TAG, "Cannot smooth scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
            } else {
                oVar.a(this, this.mState, i2);
            }
        }
    }

    public void startInterceptRequestLayout() {
        int i2 = this.mInterceptRequestLayoutDepth + 1;
        this.mInterceptRequestLayoutDepth = i2;
        if (i2 == 1 && !this.mLayoutSuppressed) {
            this.mLayoutWasDefered = false;
        }
    }

    public boolean startNestedScroll(int i2) {
        return getScrollingChildHelper().a(i2, 0);
    }

    public void stopInterceptRequestLayout(boolean z2) {
        if (this.mInterceptRequestLayoutDepth < 1) {
            this.mInterceptRequestLayoutDepth = 1;
        }
        if (!z2 && !this.mLayoutSuppressed) {
            this.mLayoutWasDefered = false;
        }
        if (this.mInterceptRequestLayoutDepth == 1) {
            if (z2 && this.mLayoutWasDefered && !this.mLayoutSuppressed && this.mLayout != null && this.mAdapter != null) {
                dispatchLayout();
            }
            if (!this.mLayoutSuppressed) {
                this.mLayoutWasDefered = false;
            }
        }
        this.mInterceptRequestLayoutDepth--;
    }

    public void stopNestedScroll() {
        getScrollingChildHelper().c(0);
    }

    public void stopScroll() {
        setScrollState(0);
        stopScrollersInternal();
    }

    public final void suppressLayout(boolean z2) {
        if (z2 != this.mLayoutSuppressed) {
            assertNotInLayoutOrScroll("Do not suppressLayout in layout or scroll");
            if (!z2) {
                this.mLayoutSuppressed = false;
                if (!(!this.mLayoutWasDefered || this.mLayout == null || this.mAdapter == null)) {
                    requestLayout();
                }
                this.mLayoutWasDefered = false;
                return;
            }
            long uptimeMillis = SystemClock.uptimeMillis();
            onTouchEvent(MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0));
            this.mLayoutSuppressed = true;
            this.mIgnoreMotionEventTillDown = true;
            stopScroll();
        }
    }

    public void swapAdapter(g gVar, boolean z2) {
        setLayoutFrozen(false);
        setAdapterInternal(gVar, true, z2);
        processDataSetCompletelyChanged(true);
        requestLayout();
    }

    public void viewRangeUpdate(int i2, int i3, Object obj) {
        int i4;
        int i5;
        int b2 = this.mChildHelper.b();
        int i6 = i3 + i2;
        for (int i7 = 0; i7 < b2; i7++) {
            View d2 = this.mChildHelper.d(i7);
            d0 childViewHolderInt = getChildViewHolderInt(d2);
            if (childViewHolderInt != null && !childViewHolderInt.o() && (i5 = childViewHolderInt.c) >= i2 && i5 < i6) {
                childViewHolderInt.a(2);
                childViewHolderInt.a(obj);
                ((p) d2.getLayoutParams()).c = true;
            }
        }
        v vVar = this.mRecycler;
        int size = vVar.c.size();
        while (true) {
            size--;
            if (size >= 0) {
                d0 d0Var = vVar.c.get(size);
                if (d0Var != null && (i4 = d0Var.c) >= i2 && i4 < i6) {
                    d0Var.a(2);
                    vVar.b(size);
                }
            } else {
                return;
            }
        }
    }

    public class f implements AdapterHelper.a {
        public f() {
        }

        public d0 a(int i2) {
            d0 findViewHolderForPosition = RecyclerView.this.findViewHolderForPosition(i2, true);
            if (findViewHolderForPosition != null && !RecyclerView.this.mChildHelper.c(findViewHolderForPosition.a)) {
                return findViewHolderForPosition;
            }
            return null;
        }

        public void a(int i2, int i3, Object obj) {
            RecyclerView.this.viewRangeUpdate(i2, i3, obj);
            RecyclerView.this.mItemsChanged = true;
        }

        public void a(AdapterHelper.b bVar) {
            int i2 = bVar.a;
            if (i2 == 1) {
                RecyclerView recyclerView = RecyclerView.this;
                recyclerView.mLayout.a(recyclerView, bVar.b, bVar.d);
            } else if (i2 == 2) {
                RecyclerView recyclerView2 = RecyclerView.this;
                recyclerView2.mLayout.b(recyclerView2, bVar.b, bVar.d);
            } else if (i2 == 4) {
                RecyclerView recyclerView3 = RecyclerView.this;
                recyclerView3.mLayout.a(recyclerView3, bVar.b, bVar.d, bVar.c);
            } else if (i2 == 8) {
                RecyclerView recyclerView4 = RecyclerView.this;
                recyclerView4.mLayout.a(recyclerView4, bVar.b, bVar.d, 1);
            }
        }
    }

    public final class v {
        public final ArrayList<d0> a = new ArrayList<>();
        public ArrayList<d0> b = null;
        public final ArrayList<d0> c = new ArrayList<>();
        public final List<d0> d = Collections.unmodifiableList(this.a);

        /* renamed from: e  reason: collision with root package name */
        public int f328e = 2;

        /* renamed from: f  reason: collision with root package name */
        public int f329f = 2;
        public u g;

        public v() {
        }

        public void a() {
            this.a.clear();
            c();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.recyclerview.widget.RecyclerView.v.a(androidx.recyclerview.widget.RecyclerView$d0, boolean):void
         arg types: [androidx.recyclerview.widget.RecyclerView$d0, int]
         candidates:
          androidx.recyclerview.widget.RecyclerView.v.a(android.view.ViewGroup, boolean):void
          androidx.recyclerview.widget.RecyclerView.v.a(androidx.recyclerview.widget.RecyclerView$d0, boolean):void */
        public void b(int i2) {
            a(this.c.get(i2), true);
            this.c.remove(i2);
        }

        public void c() {
            for (int size = this.c.size() - 1; size >= 0; size--) {
                b(size);
            }
            this.c.clear();
            if (RecyclerView.ALLOW_THREAD_GAP_WORK) {
                GapWorker.b bVar = RecyclerView.this.mPrefetchRegistry;
                int[] iArr = bVar.c;
                if (iArr != null) {
                    Arrays.fill(iArr, -1);
                }
                bVar.d = 0;
            }
        }

        public void d() {
            o oVar = RecyclerView.this.mLayout;
            this.f329f = this.f328e + (oVar != null ? oVar.f322m : 0);
            for (int size = this.c.size() - 1; size >= 0 && this.c.size() > this.f329f; size--) {
                b(size);
            }
        }

        public int a(int i2) {
            if (i2 < 0 || i2 >= RecyclerView.this.mState.a()) {
                StringBuilder a2 = outline.a("invalid position ", i2, ". State item count is ");
                a2.append(RecyclerView.this.mState.a());
                throw new IndexOutOfBoundsException(outline.a(RecyclerView.this, a2));
            }
            RecyclerView recyclerView = RecyclerView.this;
            if (!recyclerView.mState.g) {
                return i2;
            }
            return recyclerView.mAdapterHelper.a(i2, 0);
        }

        public void b(View view) {
            d0 childViewHolderInt = RecyclerView.getChildViewHolderInt(view);
            if (!childViewHolderInt.b(12) && childViewHolderInt.m() && !RecyclerView.this.canReuseUpdatedViewHolder(childViewHolderInt)) {
                if (this.b == null) {
                    this.b = new ArrayList<>();
                }
                childViewHolderInt.f309n = this;
                childViewHolderInt.f310o = true;
                this.b.add(childViewHolderInt);
            } else if (!childViewHolderInt.h() || childViewHolderInt.j() || RecyclerView.this.mAdapter.b) {
                childViewHolderInt.f309n = this;
                childViewHolderInt.f310o = false;
                this.a.add(childViewHolderInt);
            } else {
                throw new IllegalArgumentException(outline.a(RecyclerView.this, outline.a("Called scrap view with an invalid view. Invalid views cannot be reused from scrap, they should rebound from recycler pool.")));
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.recyclerview.widget.RecyclerView.v.a(android.view.ViewGroup, boolean):void
         arg types: [android.view.ViewGroup, int]
         candidates:
          androidx.recyclerview.widget.RecyclerView.v.a(androidx.recyclerview.widget.RecyclerView$d0, boolean):void
          androidx.recyclerview.widget.RecyclerView.v.a(android.view.ViewGroup, boolean):void */
        /* JADX WARNING: Code restructure failed: missing block: B:107:0x01e5, code lost:
            if (r7.f303f != 0) goto L_0x01fe;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:245:0x044f, code lost:
            if (r7.h() == false) goto L_0x0485;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:254:0x0483, code lost:
            if ((r11 == 0 || r11 + r9 < r19) == false) goto L_0x0485;
         */
        /* JADX WARNING: Removed duplicated region for block: B:130:0x0248  */
        /* JADX WARNING: Removed duplicated region for block: B:231:0x0417  */
        /* JADX WARNING: Removed duplicated region for block: B:239:0x0441  */
        /* JADX WARNING: Removed duplicated region for block: B:248:0x046c  */
        /* JADX WARNING: Removed duplicated region for block: B:258:0x048e  */
        /* JADX WARNING: Removed duplicated region for block: B:289:0x0529  */
        /* JADX WARNING: Removed duplicated region for block: B:290:0x0537  */
        /* JADX WARNING: Removed duplicated region for block: B:300:0x0559  */
        /* JADX WARNING: Removed duplicated region for block: B:34:0x0089  */
        /* JADX WARNING: Removed duplicated region for block: B:39:0x0090  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public androidx.recyclerview.widget.RecyclerView.d0 a(int r17, boolean r18, long r19) {
            /*
                r16 = this;
                r1 = r16
                r0 = r17
                if (r0 < 0) goto L_0x055a
                androidx.recyclerview.widget.RecyclerView r2 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$a0 r2 = r2.mState
                int r2 = r2.a()
                if (r0 >= r2) goto L_0x055a
                androidx.recyclerview.widget.RecyclerView r2 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$a0 r2 = r2.mState
                boolean r2 = r2.g
                r3 = 32
                r4 = 0
                r5 = 0
                if (r2 == 0) goto L_0x008b
                java.util.ArrayList<androidx.recyclerview.widget.RecyclerView$d0> r2 = r1.b
                if (r2 == 0) goto L_0x0086
                int r2 = r2.size()
                if (r2 != 0) goto L_0x0027
                goto L_0x0086
            L_0x0027:
                r6 = 0
            L_0x0028:
                if (r6 >= r2) goto L_0x0045
                java.util.ArrayList<androidx.recyclerview.widget.RecyclerView$d0> r7 = r1.b
                java.lang.Object r7 = r7.get(r6)
                androidx.recyclerview.widget.RecyclerView$d0 r7 = (androidx.recyclerview.widget.RecyclerView.d0) r7
                boolean r8 = r7.p()
                if (r8 != 0) goto L_0x0042
                int r8 = r7.d()
                if (r8 != r0) goto L_0x0042
                r7.a(r3)
                goto L_0x0087
            L_0x0042:
                int r6 = r6 + 1
                goto L_0x0028
            L_0x0045:
                androidx.recyclerview.widget.RecyclerView r6 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$g r7 = r6.mAdapter
                boolean r7 = r7.b
                if (r7 == 0) goto L_0x0086
                i.r.d.AdapterHelper r6 = r6.mAdapterHelper
                int r6 = r6.a(r0, r5)
                if (r6 <= 0) goto L_0x0086
                androidx.recyclerview.widget.RecyclerView r7 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$g r7 = r7.mAdapter
                int r7 = r7.a()
                if (r6 >= r7) goto L_0x0086
                androidx.recyclerview.widget.RecyclerView r7 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$g r7 = r7.mAdapter
                long r6 = r7.a(r6)
                r8 = 0
            L_0x0068:
                if (r8 >= r2) goto L_0x0086
                java.util.ArrayList<androidx.recyclerview.widget.RecyclerView$d0> r9 = r1.b
                java.lang.Object r9 = r9.get(r8)
                androidx.recyclerview.widget.RecyclerView$d0 r9 = (androidx.recyclerview.widget.RecyclerView.d0) r9
                boolean r10 = r9.p()
                if (r10 != 0) goto L_0x0083
                long r10 = r9.f302e
                int r12 = (r10 > r6 ? 1 : (r10 == r6 ? 0 : -1))
                if (r12 != 0) goto L_0x0083
                r9.a(r3)
                r7 = r9
                goto L_0x0087
            L_0x0083:
                int r8 = r8 + 1
                goto L_0x0068
            L_0x0086:
                r7 = r4
            L_0x0087:
                if (r7 == 0) goto L_0x008c
                r2 = 1
                goto L_0x008d
            L_0x008b:
                r7 = r4
            L_0x008c:
                r2 = 0
            L_0x008d:
                r6 = -1
                if (r7 != 0) goto L_0x0245
                java.util.ArrayList<androidx.recyclerview.widget.RecyclerView$d0> r7 = r1.a
                int r7 = r7.size()
                r8 = 0
            L_0x0097:
                if (r8 >= r7) goto L_0x00c9
                java.util.ArrayList<androidx.recyclerview.widget.RecyclerView$d0> r9 = r1.a
                java.lang.Object r9 = r9.get(r8)
                androidx.recyclerview.widget.RecyclerView$d0 r9 = (androidx.recyclerview.widget.RecyclerView.d0) r9
                boolean r10 = r9.p()
                if (r10 != 0) goto L_0x00c6
                int r10 = r9.d()
                if (r10 != r0) goto L_0x00c6
                boolean r10 = r9.h()
                if (r10 != 0) goto L_0x00c6
                androidx.recyclerview.widget.RecyclerView r10 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$a0 r10 = r10.mState
                boolean r10 = r10.g
                if (r10 != 0) goto L_0x00c1
                boolean r10 = r9.j()
                if (r10 != 0) goto L_0x00c6
            L_0x00c1:
                r9.a(r3)
                goto L_0x01b4
            L_0x00c6:
                int r8 = r8 + 1
                goto L_0x0097
            L_0x00c9:
                if (r18 != 0) goto L_0x018a
                androidx.recyclerview.widget.RecyclerView r7 = androidx.recyclerview.widget.RecyclerView.this
                i.r.d.ChildHelper r7 = r7.mChildHelper
                java.util.List<android.view.View> r8 = r7.c
                int r8 = r8.size()
                r9 = 0
            L_0x00d6:
                if (r9 >= r8) goto L_0x0101
                java.util.List<android.view.View> r10 = r7.c
                java.lang.Object r10 = r10.get(r9)
                android.view.View r10 = (android.view.View) r10
                i.r.d.ChildHelper$b r11 = r7.a
                androidx.recyclerview.widget.RecyclerView$e r11 = (androidx.recyclerview.widget.RecyclerView.e) r11
                if (r11 == 0) goto L_0x0100
                androidx.recyclerview.widget.RecyclerView$d0 r11 = androidx.recyclerview.widget.RecyclerView.getChildViewHolderInt(r10)
                int r12 = r11.d()
                if (r12 != r0) goto L_0x00fd
                boolean r12 = r11.h()
                if (r12 != 0) goto L_0x00fd
                boolean r11 = r11.j()
                if (r11 != 0) goto L_0x00fd
                goto L_0x0102
            L_0x00fd:
                int r9 = r9 + 1
                goto L_0x00d6
            L_0x0100:
                throw r4
            L_0x0101:
                r10 = r4
            L_0x0102:
                if (r10 == 0) goto L_0x018a
                androidx.recyclerview.widget.RecyclerView$d0 r7 = androidx.recyclerview.widget.RecyclerView.getChildViewHolderInt(r10)
                androidx.recyclerview.widget.RecyclerView r8 = androidx.recyclerview.widget.RecyclerView.this
                i.r.d.ChildHelper r8 = r8.mChildHelper
                i.r.d.ChildHelper$b r9 = r8.a
                androidx.recyclerview.widget.RecyclerView$e r9 = (androidx.recyclerview.widget.RecyclerView.e) r9
                androidx.recyclerview.widget.RecyclerView r9 = androidx.recyclerview.widget.RecyclerView.this
                int r9 = r9.indexOfChild(r10)
                if (r9 < 0) goto L_0x0173
                i.r.d.ChildHelper$a r11 = r8.b
                boolean r11 = r11.c(r9)
                if (r11 == 0) goto L_0x015c
                i.r.d.ChildHelper$a r11 = r8.b
                r11.a(r9)
                r8.d(r10)
                androidx.recyclerview.widget.RecyclerView r8 = androidx.recyclerview.widget.RecyclerView.this
                i.r.d.ChildHelper r8 = r8.mChildHelper
                int r8 = r8.b(r10)
                if (r8 == r6) goto L_0x0143
                androidx.recyclerview.widget.RecyclerView r9 = androidx.recyclerview.widget.RecyclerView.this
                i.r.d.ChildHelper r9 = r9.mChildHelper
                r9.a(r8)
                r1.b(r10)
                r8 = 8224(0x2020, float:1.1524E-41)
                r7.a(r8)
                goto L_0x01ba
            L_0x0143:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r3 = "layout index should not be -1 after unhiding a view:"
                r2.append(r3)
                r2.append(r7)
                androidx.recyclerview.widget.RecyclerView r3 = androidx.recyclerview.widget.RecyclerView.this
                java.lang.String r2 = j.a.a.a.outline.a(r3, r2)
                r0.<init>(r2)
                throw r0
            L_0x015c:
                java.lang.RuntimeException r0 = new java.lang.RuntimeException
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r3 = "trying to unhide a view that was not hidden"
                r2.append(r3)
                r2.append(r10)
                java.lang.String r2 = r2.toString()
                r0.<init>(r2)
                throw r0
            L_0x0173:
                java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r3 = "view is not a child, cannot hide "
                r2.append(r3)
                r2.append(r10)
                java.lang.String r2 = r2.toString()
                r0.<init>(r2)
                throw r0
            L_0x018a:
                java.util.ArrayList<androidx.recyclerview.widget.RecyclerView$d0> r7 = r1.c
                int r7 = r7.size()
                r8 = 0
            L_0x0191:
                if (r8 >= r7) goto L_0x01b9
                java.util.ArrayList<androidx.recyclerview.widget.RecyclerView$d0> r9 = r1.c
                java.lang.Object r9 = r9.get(r8)
                androidx.recyclerview.widget.RecyclerView$d0 r9 = (androidx.recyclerview.widget.RecyclerView.d0) r9
                boolean r10 = r9.h()
                if (r10 != 0) goto L_0x01b6
                int r10 = r9.d()
                if (r10 != r0) goto L_0x01b6
                boolean r10 = r9.f()
                if (r10 != 0) goto L_0x01b6
                if (r18 != 0) goto L_0x01b4
                java.util.ArrayList<androidx.recyclerview.widget.RecyclerView$d0> r7 = r1.c
                r7.remove(r8)
            L_0x01b4:
                r7 = r9
                goto L_0x01ba
            L_0x01b6:
                int r8 = r8 + 1
                goto L_0x0191
            L_0x01b9:
                r7 = r4
            L_0x01ba:
                if (r7 == 0) goto L_0x0245
                boolean r8 = r7.j()
                if (r8 == 0) goto L_0x01c9
                androidx.recyclerview.widget.RecyclerView r8 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$a0 r8 = r8.mState
                boolean r8 = r8.g
                goto L_0x0201
            L_0x01c9:
                int r8 = r7.c
                if (r8 < 0) goto L_0x022c
                androidx.recyclerview.widget.RecyclerView r9 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$g r9 = r9.mAdapter
                int r9 = r9.a()
                if (r8 >= r9) goto L_0x022c
                androidx.recyclerview.widget.RecyclerView r8 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$a0 r9 = r8.mState
                boolean r9 = r9.g
                if (r9 != 0) goto L_0x01e9
                androidx.recyclerview.widget.RecyclerView$g r8 = r8.mAdapter
                if (r8 == 0) goto L_0x01e8
                int r8 = r7.f303f
                if (r8 == 0) goto L_0x01e9
                goto L_0x01fe
            L_0x01e8:
                throw r4
            L_0x01e9:
                androidx.recyclerview.widget.RecyclerView r8 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$g r8 = r8.mAdapter
                boolean r9 = r8.b
                if (r9 == 0) goto L_0x0200
                long r9 = r7.f302e
                int r11 = r7.c
                long r11 = r8.a(r11)
                int r8 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
                if (r8 != 0) goto L_0x01fe
                goto L_0x0200
            L_0x01fe:
                r8 = 0
                goto L_0x0201
            L_0x0200:
                r8 = 1
            L_0x0201:
                if (r8 != 0) goto L_0x022a
                if (r18 != 0) goto L_0x0228
                r8 = 4
                r7.a(r8)
                boolean r8 = r7.k()
                if (r8 == 0) goto L_0x021c
                androidx.recyclerview.widget.RecyclerView r8 = androidx.recyclerview.widget.RecyclerView.this
                android.view.View r9 = r7.a
                r8.removeDetachedView(r9, r5)
                androidx.recyclerview.widget.RecyclerView$v r8 = r7.f309n
                r8.b(r7)
                goto L_0x0225
            L_0x021c:
                boolean r8 = r7.p()
                if (r8 == 0) goto L_0x0225
                r7.b()
            L_0x0225:
                r1.a(r7)
            L_0x0228:
                r7 = r4
                goto L_0x0245
            L_0x022a:
                r2 = 1
                goto L_0x0245
            L_0x022c:
                java.lang.IndexOutOfBoundsException r0 = new java.lang.IndexOutOfBoundsException
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r3 = "Inconsistency detected. Invalid view holder adapter position"
                r2.append(r3)
                r2.append(r7)
                androidx.recyclerview.widget.RecyclerView r3 = androidx.recyclerview.widget.RecyclerView.this
                java.lang.String r2 = j.a.a.a.outline.a(r3, r2)
                r0.<init>(r2)
                throw r0
            L_0x0245:
                r8 = 2
                if (r7 != 0) goto L_0x03fa
                androidx.recyclerview.widget.RecyclerView r11 = androidx.recyclerview.widget.RecyclerView.this
                i.r.d.AdapterHelper r11 = r11.mAdapterHelper
                int r11 = r11.a(r0, r5)
                if (r11 < 0) goto L_0x03c9
                androidx.recyclerview.widget.RecyclerView r12 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$g r12 = r12.mAdapter
                int r12 = r12.a()
                if (r11 >= r12) goto L_0x03c9
                androidx.recyclerview.widget.RecyclerView r12 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$g r12 = r12.mAdapter
                if (r12 == 0) goto L_0x03c8
                boolean r13 = r12.b
                if (r13 == 0) goto L_0x02fc
                long r12 = r12.a(r11)
                java.util.ArrayList<androidx.recyclerview.widget.RecyclerView$d0> r7 = r1.a
                int r7 = r7.size()
                int r7 = r7 + r6
            L_0x0271:
                if (r7 < 0) goto L_0x02c4
                java.util.ArrayList<androidx.recyclerview.widget.RecyclerView$d0> r14 = r1.a
                java.lang.Object r14 = r14.get(r7)
                androidx.recyclerview.widget.RecyclerView$d0 r14 = (androidx.recyclerview.widget.RecyclerView.d0) r14
                long r9 = r14.f302e
                int r15 = (r9 > r12 ? 1 : (r9 == r12 ? 0 : -1))
                if (r15 != 0) goto L_0x02c1
                boolean r9 = r14.p()
                if (r9 != 0) goto L_0x02c1
                int r9 = r14.f303f
                if (r9 != 0) goto L_0x02a3
                r14.a(r3)
                boolean r3 = r14.j()
                if (r3 == 0) goto L_0x02a1
                androidx.recyclerview.widget.RecyclerView r3 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$a0 r3 = r3.mState
                boolean r3 = r3.g
                if (r3 != 0) goto L_0x02a1
                r3 = 14
                r14.a(r8, r3)
            L_0x02a1:
                r7 = r14
                goto L_0x02f7
            L_0x02a3:
                if (r18 != 0) goto L_0x02c1
                java.util.ArrayList<androidx.recyclerview.widget.RecyclerView$d0> r9 = r1.a
                r9.remove(r7)
                androidx.recyclerview.widget.RecyclerView r9 = androidx.recyclerview.widget.RecyclerView.this
                android.view.View r10 = r14.a
                r9.removeDetachedView(r10, r5)
                android.view.View r9 = r14.a
                androidx.recyclerview.widget.RecyclerView$d0 r9 = androidx.recyclerview.widget.RecyclerView.getChildViewHolderInt(r9)
                r9.f309n = r4
                r9.f310o = r5
                r9.b()
                r1.a(r9)
            L_0x02c1:
                int r7 = r7 + -1
                goto L_0x0271
            L_0x02c4:
                java.util.ArrayList<androidx.recyclerview.widget.RecyclerView$d0> r3 = r1.c
                int r3 = r3.size()
                int r3 = r3 + r6
            L_0x02cb:
                if (r3 < 0) goto L_0x02f6
                java.util.ArrayList<androidx.recyclerview.widget.RecyclerView$d0> r7 = r1.c
                java.lang.Object r7 = r7.get(r3)
                androidx.recyclerview.widget.RecyclerView$d0 r7 = (androidx.recyclerview.widget.RecyclerView.d0) r7
                long r9 = r7.f302e
                int r14 = (r9 > r12 ? 1 : (r9 == r12 ? 0 : -1))
                if (r14 != 0) goto L_0x02f3
                boolean r9 = r7.f()
                if (r9 != 0) goto L_0x02f3
                int r9 = r7.f303f
                if (r9 != 0) goto L_0x02ed
                if (r18 != 0) goto L_0x02f7
                java.util.ArrayList<androidx.recyclerview.widget.RecyclerView$d0> r9 = r1.c
                r9.remove(r3)
                goto L_0x02f7
            L_0x02ed:
                if (r18 != 0) goto L_0x02f3
                r1.b(r3)
                goto L_0x02f6
            L_0x02f3:
                int r3 = r3 + -1
                goto L_0x02cb
            L_0x02f6:
                r7 = r4
            L_0x02f7:
                if (r7 == 0) goto L_0x02fc
                r7.c = r11
                r2 = 1
            L_0x02fc:
                if (r7 != 0) goto L_0x0349
                androidx.recyclerview.widget.RecyclerView$u r3 = r16.b()
                android.util.SparseArray<androidx.recyclerview.widget.RecyclerView$u$a> r3 = r3.a
                java.lang.Object r3 = r3.get(r5)
                androidx.recyclerview.widget.RecyclerView$u$a r3 = (androidx.recyclerview.widget.RecyclerView.u.a) r3
                if (r3 == 0) goto L_0x0333
                java.util.ArrayList<androidx.recyclerview.widget.RecyclerView$d0> r7 = r3.a
                boolean r7 = r7.isEmpty()
                if (r7 != 0) goto L_0x0333
                java.util.ArrayList<androidx.recyclerview.widget.RecyclerView$d0> r3 = r3.a
                int r7 = r3.size()
                int r7 = r7 + r6
            L_0x031b:
                if (r7 < 0) goto L_0x0333
                java.lang.Object r6 = r3.get(r7)
                androidx.recyclerview.widget.RecyclerView$d0 r6 = (androidx.recyclerview.widget.RecyclerView.d0) r6
                boolean r6 = r6.f()
                if (r6 != 0) goto L_0x0330
                java.lang.Object r3 = r3.remove(r7)
                androidx.recyclerview.widget.RecyclerView$d0 r3 = (androidx.recyclerview.widget.RecyclerView.d0) r3
                goto L_0x0334
            L_0x0330:
                int r7 = r7 + -1
                goto L_0x031b
            L_0x0333:
                r3 = r4
            L_0x0334:
                if (r3 == 0) goto L_0x0348
                r3.n()
                boolean r6 = androidx.recyclerview.widget.RecyclerView.FORCE_INVALIDATE_DISPLAY_LIST
                if (r6 == 0) goto L_0x0348
                android.view.View r6 = r3.a
                boolean r7 = r6 instanceof android.view.ViewGroup
                if (r7 == 0) goto L_0x0348
                android.view.ViewGroup r6 = (android.view.ViewGroup) r6
                r1.a(r6, r5)
            L_0x0348:
                r7 = r3
            L_0x0349:
                if (r7 != 0) goto L_0x03fa
                androidx.recyclerview.widget.RecyclerView r3 = androidx.recyclerview.widget.RecyclerView.this
                long r6 = r3.getNanoTime()
                r9 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
                int r3 = (r19 > r9 ? 1 : (r19 == r9 ? 0 : -1))
                if (r3 == 0) goto L_0x0374
                androidx.recyclerview.widget.RecyclerView$u r3 = r1.g
                androidx.recyclerview.widget.RecyclerView$u$a r3 = r3.a(r5)
                long r9 = r3.c
                r11 = 0
                int r3 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
                if (r3 == 0) goto L_0x0370
                long r9 = r9 + r6
                int r3 = (r9 > r19 ? 1 : (r9 == r19 ? 0 : -1))
                if (r3 >= 0) goto L_0x036e
                goto L_0x0370
            L_0x036e:
                r3 = 0
                goto L_0x0371
            L_0x0370:
                r3 = 1
            L_0x0371:
                if (r3 != 0) goto L_0x0374
                return r4
            L_0x0374:
                androidx.recyclerview.widget.RecyclerView r3 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$g r9 = r3.mAdapter
                if (r9 == 0) goto L_0x03c7
                java.lang.String r10 = "RV CreateView"
                i.h.h.TraceCompat.a(r10)     // Catch:{ all -> 0x03c2 }
                androidx.recyclerview.widget.RecyclerView$d0 r3 = r9.a(r3, r5)     // Catch:{ all -> 0x03c2 }
                android.view.View r9 = r3.a     // Catch:{ all -> 0x03c2 }
                android.view.ViewParent r9 = r9.getParent()     // Catch:{ all -> 0x03c2 }
                if (r9 != 0) goto L_0x03ba
                r3.f303f = r5     // Catch:{ all -> 0x03c2 }
                android.os.Trace.endSection()
                boolean r9 = androidx.recyclerview.widget.RecyclerView.ALLOW_THREAD_GAP_WORK
                if (r9 == 0) goto L_0x03a3
                android.view.View r9 = r3.a
                androidx.recyclerview.widget.RecyclerView r9 = androidx.recyclerview.widget.RecyclerView.findNestedRecyclerView(r9)
                if (r9 == 0) goto L_0x03a3
                java.lang.ref.WeakReference r10 = new java.lang.ref.WeakReference
                r10.<init>(r9)
                r3.b = r10
            L_0x03a3:
                androidx.recyclerview.widget.RecyclerView r9 = androidx.recyclerview.widget.RecyclerView.this
                long r9 = r9.getNanoTime()
                androidx.recyclerview.widget.RecyclerView$u r11 = r1.g
                long r9 = r9 - r6
                androidx.recyclerview.widget.RecyclerView$u$a r6 = r11.a(r5)
                long r12 = r6.c
                long r9 = r11.a(r12, r9)
                r6.c = r9
                r7 = r3
                goto L_0x03fa
            L_0x03ba:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ all -> 0x03c2 }
                java.lang.String r2 = "ViewHolder views must not be attached when created. Ensure that you are not passing 'true' to the attachToRoot parameter of LayoutInflater.inflate(..., boolean attachToRoot)"
                r0.<init>(r2)     // Catch:{ all -> 0x03c2 }
                throw r0     // Catch:{ all -> 0x03c2 }
            L_0x03c2:
                r0 = move-exception
                i.h.h.TraceCompat.a()
                throw r0
            L_0x03c7:
                throw r4
            L_0x03c8:
                throw r4
            L_0x03c9:
                java.lang.IndexOutOfBoundsException r2 = new java.lang.IndexOutOfBoundsException
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r4 = "Inconsistency detected. Invalid item position "
                r3.append(r4)
                r3.append(r0)
                java.lang.String r0 = "(offset:"
                r3.append(r0)
                r3.append(r11)
                java.lang.String r0 = ").state:"
                r3.append(r0)
                androidx.recyclerview.widget.RecyclerView r0 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$a0 r0 = r0.mState
                int r0 = r0.a()
                r3.append(r0)
                androidx.recyclerview.widget.RecyclerView r0 = androidx.recyclerview.widget.RecyclerView.this
                java.lang.String r0 = j.a.a.a.outline.a(r0, r3)
                r2.<init>(r0)
                throw r2
            L_0x03fa:
                if (r2 == 0) goto L_0x042a
                androidx.recyclerview.widget.RecyclerView r3 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$a0 r3 = r3.mState
                boolean r3 = r3.g
                if (r3 != 0) goto L_0x042a
                r3 = 8192(0x2000, float:1.14794E-41)
                boolean r6 = r7.b(r3)
                if (r6 == 0) goto L_0x042a
                r7.a(r5, r3)
                androidx.recyclerview.widget.RecyclerView r3 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$a0 r3 = r3.mState
                boolean r3 = r3.f293j
                if (r3 == 0) goto L_0x042a
                androidx.recyclerview.widget.RecyclerView.l.d(r7)
                androidx.recyclerview.widget.RecyclerView r3 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$l r3 = r3.mItemAnimator
                r7.e()
                androidx.recyclerview.widget.RecyclerView$l$c r3 = r3.c(r7)
                androidx.recyclerview.widget.RecyclerView r6 = androidx.recyclerview.widget.RecyclerView.this
                r6.recordAnimationInfoIfBouncedHiddenView(r7, r3)
            L_0x042a:
                androidx.recyclerview.widget.RecyclerView r3 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$a0 r3 = r3.mState
                boolean r3 = r3.g
                if (r3 == 0) goto L_0x043b
                boolean r3 = r7.g()
                if (r3 == 0) goto L_0x043b
                r7.g = r0
                goto L_0x0485
            L_0x043b:
                boolean r3 = r7.g()
                if (r3 == 0) goto L_0x0451
                int r3 = r7.f305j
                r3 = r3 & r8
                if (r3 == 0) goto L_0x0448
                r3 = 1
                goto L_0x0449
            L_0x0448:
                r3 = 0
            L_0x0449:
                if (r3 != 0) goto L_0x0451
                boolean r3 = r7.h()
                if (r3 == 0) goto L_0x0485
            L_0x0451:
                androidx.recyclerview.widget.RecyclerView r3 = androidx.recyclerview.widget.RecyclerView.this
                i.r.d.AdapterHelper r3 = r3.mAdapterHelper
                int r3 = r3.a(r0, r5)
                androidx.recyclerview.widget.RecyclerView r6 = androidx.recyclerview.widget.RecyclerView.this
                r7.f313r = r6
                int r8 = r7.f303f
                long r9 = r6.getNanoTime()
                r11 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
                int r6 = (r19 > r11 ? 1 : (r19 == r11 ? 0 : -1))
                if (r6 == 0) goto L_0x0488
                androidx.recyclerview.widget.RecyclerView$u r6 = r1.g
                androidx.recyclerview.widget.RecyclerView$u$a r6 = r6.a(r8)
                long r11 = r6.d
                r13 = 0
                int r6 = (r11 > r13 ? 1 : (r11 == r13 ? 0 : -1))
                if (r6 == 0) goto L_0x0482
                long r11 = r11 + r9
                int r6 = (r11 > r19 ? 1 : (r11 == r19 ? 0 : -1))
                if (r6 >= 0) goto L_0x0480
                goto L_0x0482
            L_0x0480:
                r6 = 0
                goto L_0x0483
            L_0x0482:
                r6 = 1
            L_0x0483:
                if (r6 != 0) goto L_0x0488
            L_0x0485:
                r0 = 0
                goto L_0x0521
            L_0x0488:
                androidx.recyclerview.widget.RecyclerView r6 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$g r6 = r6.mAdapter
                if (r6 == 0) goto L_0x0559
                r7.c = r3
                boolean r8 = r6.b
                if (r8 == 0) goto L_0x049a
                long r11 = r6.a(r3)
                r7.f302e = r11
            L_0x049a:
                r8 = 519(0x207, float:7.27E-43)
                r11 = 1
                r7.a(r11, r8)
                java.lang.String r8 = "RV OnBindView"
                i.h.h.TraceCompat.a(r8)
                r7.e()
                r6.a(r7, r3)
                java.util.List<java.lang.Object> r3 = r7.f306k
                if (r3 == 0) goto L_0x04b2
                r3.clear()
            L_0x04b2:
                int r3 = r7.f305j
                r3 = r3 & -1025(0xfffffffffffffbff, float:NaN)
                r7.f305j = r3
                android.view.View r3 = r7.a
                android.view.ViewGroup$LayoutParams r3 = r3.getLayoutParams()
                boolean r6 = r3 instanceof androidx.recyclerview.widget.RecyclerView.p
                if (r6 == 0) goto L_0x04c7
                androidx.recyclerview.widget.RecyclerView$p r3 = (androidx.recyclerview.widget.RecyclerView.p) r3
                r6 = 1
                r3.c = r6
            L_0x04c7:
                android.os.Trace.endSection()
                androidx.recyclerview.widget.RecyclerView r3 = androidx.recyclerview.widget.RecyclerView.this
                long r11 = r3.getNanoTime()
                androidx.recyclerview.widget.RecyclerView$u r3 = r1.g
                int r6 = r7.f303f
                long r11 = r11 - r9
                androidx.recyclerview.widget.RecyclerView$u$a r6 = r3.a(r6)
                long r8 = r6.d
                long r8 = r3.a(r8, r11)
                r6.d = r8
                androidx.recyclerview.widget.RecyclerView r3 = androidx.recyclerview.widget.RecyclerView.this
                boolean r3 = r3.isAccessibilityEnabled()
                if (r3 == 0) goto L_0x0516
                android.view.View r3 = r7.a
                int r6 = i.h.l.ViewCompat.i(r3)
                r8 = 1
                if (r6 != 0) goto L_0x04f5
                r3.setImportantForAccessibility(r8)
            L_0x04f5:
                androidx.recyclerview.widget.RecyclerView r6 = androidx.recyclerview.widget.RecyclerView.this
                i.r.d.RecyclerViewAccessibilityDelegate r6 = r6.mAccessibilityDelegate
                if (r6 != 0) goto L_0x04fc
                goto L_0x0516
            L_0x04fc:
                i.r.d.RecyclerViewAccessibilityDelegate$a r6 = r6.f1411e
                boolean r8 = r6 instanceof i.r.d.RecyclerViewAccessibilityDelegate.a
                if (r8 == 0) goto L_0x0513
                if (r6 == 0) goto L_0x0512
                i.h.l.AccessibilityDelegateCompat r4 = i.h.l.ViewCompat.b(r3)
                if (r4 == 0) goto L_0x0513
                if (r4 == r6) goto L_0x0513
                java.util.Map<android.view.View, i.h.l.a> r8 = r6.f1412e
                r8.put(r3, r4)
                goto L_0x0513
            L_0x0512:
                throw r4
            L_0x0513:
                i.h.l.ViewCompat.a(r3, r6)
            L_0x0516:
                androidx.recyclerview.widget.RecyclerView r3 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$a0 r3 = r3.mState
                boolean r3 = r3.g
                if (r3 == 0) goto L_0x0520
                r7.g = r0
            L_0x0520:
                r0 = 1
            L_0x0521:
                android.view.View r3 = r7.a
                android.view.ViewGroup$LayoutParams r3 = r3.getLayoutParams()
                if (r3 != 0) goto L_0x0537
                androidx.recyclerview.widget.RecyclerView r3 = androidx.recyclerview.widget.RecyclerView.this
                android.view.ViewGroup$LayoutParams r3 = r3.generateDefaultLayoutParams()
                androidx.recyclerview.widget.RecyclerView$p r3 = (androidx.recyclerview.widget.RecyclerView.p) r3
                android.view.View r4 = r7.a
                r4.setLayoutParams(r3)
                goto L_0x054f
            L_0x0537:
                androidx.recyclerview.widget.RecyclerView r4 = androidx.recyclerview.widget.RecyclerView.this
                boolean r4 = r4.checkLayoutParams(r3)
                if (r4 != 0) goto L_0x054d
                androidx.recyclerview.widget.RecyclerView r4 = androidx.recyclerview.widget.RecyclerView.this
                android.view.ViewGroup$LayoutParams r3 = r4.generateLayoutParams(r3)
                androidx.recyclerview.widget.RecyclerView$p r3 = (androidx.recyclerview.widget.RecyclerView.p) r3
                android.view.View r4 = r7.a
                r4.setLayoutParams(r3)
                goto L_0x054f
            L_0x054d:
                androidx.recyclerview.widget.RecyclerView$p r3 = (androidx.recyclerview.widget.RecyclerView.p) r3
            L_0x054f:
                r3.a = r7
                if (r2 == 0) goto L_0x0556
                if (r0 == 0) goto L_0x0556
                r5 = 1
            L_0x0556:
                r3.d = r5
                return r7
            L_0x0559:
                throw r4
            L_0x055a:
                java.lang.IndexOutOfBoundsException r2 = new java.lang.IndexOutOfBoundsException
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r4 = "Invalid item position "
                r3.append(r4)
                r3.append(r0)
                java.lang.String r4 = "("
                r3.append(r4)
                r3.append(r0)
                java.lang.String r0 = "). Item count:"
                r3.append(r0)
                androidx.recyclerview.widget.RecyclerView r0 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$a0 r0 = r0.mState
                int r0 = r0.a()
                r3.append(r0)
                androidx.recyclerview.widget.RecyclerView r0 = androidx.recyclerview.widget.RecyclerView.this
                java.lang.String r0 = j.a.a.a.outline.a(r0, r3)
                r2.<init>(r0)
                throw r2
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.v.a(int, boolean, long):androidx.recyclerview.widget.RecyclerView$d0");
        }

        public void b(d0 d0Var) {
            if (d0Var.f310o) {
                this.b.remove(d0Var);
            } else {
                this.a.remove(d0Var);
            }
            d0Var.f309n = null;
            d0Var.f310o = false;
            d0Var.b();
        }

        public u b() {
            if (this.g == null) {
                this.g = new u();
            }
            return this.g;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.recyclerview.widget.RecyclerView.v.a(android.view.ViewGroup, boolean):void
         arg types: [android.view.ViewGroup, int]
         candidates:
          androidx.recyclerview.widget.RecyclerView.v.a(androidx.recyclerview.widget.RecyclerView$d0, boolean):void
          androidx.recyclerview.widget.RecyclerView.v.a(android.view.ViewGroup, boolean):void */
        public final void a(ViewGroup viewGroup, boolean z) {
            for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = viewGroup.getChildAt(childCount);
                if (childAt instanceof ViewGroup) {
                    a((ViewGroup) childAt, true);
                }
            }
            if (z) {
                if (viewGroup.getVisibility() == 4) {
                    viewGroup.setVisibility(0);
                    viewGroup.setVisibility(4);
                    return;
                }
                int visibility = viewGroup.getVisibility();
                viewGroup.setVisibility(4);
                viewGroup.setVisibility(visibility);
            }
        }

        public void a(View view) {
            d0 childViewHolderInt = RecyclerView.getChildViewHolderInt(view);
            if (childViewHolderInt.l()) {
                RecyclerView.this.removeDetachedView(view, false);
            }
            if (childViewHolderInt.k()) {
                childViewHolderInt.f309n.b(childViewHolderInt);
            } else if (childViewHolderInt.p()) {
                childViewHolderInt.b();
            }
            a(childViewHolderInt);
            if (RecyclerView.this.mItemAnimator != null && !childViewHolderInt.i()) {
                RecyclerView.this.mItemAnimator.b(childViewHolderInt);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.recyclerview.widget.RecyclerView.v.a(androidx.recyclerview.widget.RecyclerView$d0, boolean):void
         arg types: [androidx.recyclerview.widget.RecyclerView$d0, int]
         candidates:
          androidx.recyclerview.widget.RecyclerView.v.a(android.view.ViewGroup, boolean):void
          androidx.recyclerview.widget.RecyclerView.v.a(androidx.recyclerview.widget.RecyclerView$d0, boolean):void */
        public void a(d0 d0Var) {
            boolean z = false;
            boolean z2 = true;
            if (d0Var.k() || d0Var.a.getParent() != null) {
                StringBuilder a2 = outline.a("Scrapped or attached views may not be recycled. isScrap:");
                a2.append(d0Var.k());
                a2.append(" isAttached:");
                if (d0Var.a.getParent() != null) {
                    z = true;
                }
                a2.append(z);
                throw new IllegalArgumentException(outline.a(RecyclerView.this, a2));
            } else if (d0Var.l()) {
                StringBuilder sb = new StringBuilder();
                sb.append("Tmp detached view should be removed from RecyclerView before it can be recycled: ");
                sb.append(d0Var);
                throw new IllegalArgumentException(outline.a(RecyclerView.this, sb));
            } else if (!d0Var.o()) {
                boolean z3 = (d0Var.f305j & 16) == 0 && ViewCompat.u(d0Var.a);
                g gVar = RecyclerView.this.mAdapter;
                if (gVar == null || !z3 || gVar != null) {
                    if (d0Var.i()) {
                        if (this.f329f <= 0 || d0Var.b(526)) {
                            z = false;
                        } else {
                            int size = this.c.size();
                            if (size >= this.f329f && size > 0) {
                                b(0);
                                size--;
                            }
                            if (RecyclerView.ALLOW_THREAD_GAP_WORK && size > 0 && !RecyclerView.this.mPrefetchRegistry.a(d0Var.c)) {
                                do {
                                    size--;
                                    if (size < 0) {
                                        break;
                                    }
                                } while (RecyclerView.this.mPrefetchRegistry.a(this.c.get(size).c));
                                size++;
                            }
                            this.c.add(size, d0Var);
                            z = true;
                        }
                        if (!z) {
                            a(d0Var, true);
                            RecyclerView.this.mViewInfoStore.d(d0Var);
                            if (!z && !z2 && z3) {
                                d0Var.f313r = null;
                                return;
                            }
                            return;
                        }
                    }
                    z2 = false;
                    RecyclerView.this.mViewInfoStore.d(d0Var);
                    if (!z) {
                        return;
                    }
                    return;
                }
                throw null;
            } else {
                throw new IllegalArgumentException(outline.a(RecyclerView.this, outline.a("Trying to recycle an ignored view holder. You should first call stopIgnoringView(view) before calling recycle.")));
            }
        }

        public void a(d0 d0Var, boolean z) {
            RecyclerView.clearNestedRecyclerViewIfNotNested(d0Var);
            View view = d0Var.a;
            RecyclerViewAccessibilityDelegate recyclerViewAccessibilityDelegate = RecyclerView.this.mAccessibilityDelegate;
            if (recyclerViewAccessibilityDelegate != null) {
                RecyclerViewAccessibilityDelegate.a aVar = recyclerViewAccessibilityDelegate.f1411e;
                ViewCompat.a(view, aVar instanceof RecyclerViewAccessibilityDelegate.a ? (AccessibilityDelegateCompat) aVar.f1412e.remove(view) : null);
            }
            if (z) {
                w wVar = RecyclerView.this.mRecyclerListener;
                if (wVar != null) {
                    wVar.a(d0Var);
                }
                g gVar = RecyclerView.this.mAdapter;
                RecyclerView recyclerView = RecyclerView.this;
                if (recyclerView.mState != null) {
                    recyclerView.mViewInfoStore.d(d0Var);
                }
            }
            d0Var.f313r = null;
            u b2 = b();
            if (b2 != null) {
                int i2 = d0Var.f303f;
                ArrayList<d0> arrayList = b2.a(i2).a;
                if (b2.a.get(i2).b > arrayList.size()) {
                    d0Var.n();
                    arrayList.add(d0Var);
                    return;
                }
                return;
            }
            throw null;
        }
    }

    public RecyclerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, i.r.a.recyclerViewStyle);
    }

    public boolean dispatchNestedScroll(int i2, int i3, int i4, int i5, int[] iArr, int i6) {
        return getScrollingChildHelper().b(i2, i3, i4, i5, iArr, i6, null);
    }

    public d0 findViewHolderForPosition(int i2, boolean z2) {
        int b2 = this.mChildHelper.b();
        d0 d0Var = null;
        for (int i3 = 0; i3 < b2; i3++) {
            d0 childViewHolderInt = getChildViewHolderInt(this.mChildHelper.d(i3));
            if (childViewHolderInt != null && !childViewHolderInt.j()) {
                if (z2) {
                    if (childViewHolderInt.c != i2) {
                        continue;
                    }
                } else if (childViewHolderInt.d() != i2) {
                    continue;
                }
                if (!this.mChildHelper.c(childViewHolderInt.a)) {
                    return childViewHolderInt;
                }
                d0Var = childViewHolderInt;
            }
        }
        return d0Var;
    }

    public void onExitLayoutOrScroll(boolean z2) {
        int i2 = this.mLayoutOrScrollCounter - 1;
        this.mLayoutOrScrollCounter = i2;
        if (i2 < 1) {
            this.mLayoutOrScrollCounter = 0;
            if (z2) {
                dispatchContentChangedIfNecessary();
                dispatchPendingImportantForAccessibilityChanges();
            }
        }
    }

    public void smoothScrollBy(int i2, int i3, Interpolator interpolator) {
        smoothScrollBy(i2, i3, interpolator, UNDEFINED_DURATION);
    }

    public RecyclerView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        float f2;
        this.mObserver = new x();
        this.mRecycler = new v();
        this.mViewInfoStore = new ViewInfoStore();
        this.mUpdateChildViewsRunnable = new a();
        this.mTempRect = new Rect();
        this.mTempRect2 = new Rect();
        this.mTempRectF = new RectF();
        this.mItemDecorations = new ArrayList<>();
        this.mOnItemTouchListeners = new ArrayList<>();
        this.mInterceptRequestLayoutDepth = 0;
        this.mDataSetHasChangedAfterLayout = false;
        this.mDispatchItemsChangedEvent = false;
        this.mLayoutOrScrollCounter = 0;
        this.mDispatchScrollCounter = 0;
        this.mEdgeEffectFactory = new k();
        this.mItemAnimator = new DefaultItemAnimator7();
        this.mScrollState = 0;
        this.mScrollPointerId = -1;
        this.mScaledHorizontalScrollFactor = Float.MIN_VALUE;
        this.mScaledVerticalScrollFactor = Float.MIN_VALUE;
        this.mPreserveFocusAfterLayout = true;
        this.mViewFlinger = new c0();
        this.mPrefetchRegistry = ALLOW_THREAD_GAP_WORK ? new GapWorker.b() : null;
        this.mState = new a0();
        this.mItemsAddedOrRemoved = false;
        this.mItemsChanged = false;
        this.mItemAnimatorListener = new m();
        this.mPostedAnimatorRunner = false;
        this.mMinMaxLayoutPositions = new int[2];
        this.mScrollOffset = new int[2];
        this.mNestedOffsets = new int[2];
        this.mReusableIntPair = new int[2];
        this.mPendingAccessibilityImportanceChange = new ArrayList();
        this.mItemAnimatorRunner = new b();
        this.mViewInfoProcessCallback = new d();
        setScrollContainer(true);
        setFocusableInTouchMode(true);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        this.mTouchSlop = viewConfiguration.getScaledTouchSlop();
        this.mScaledHorizontalScrollFactor = ViewConfigurationCompat.b(viewConfiguration, context);
        if (Build.VERSION.SDK_INT >= 26) {
            f2 = viewConfiguration.getScaledVerticalScrollFactor();
        } else {
            f2 = ViewConfigurationCompat.a(viewConfiguration, context);
        }
        this.mScaledVerticalScrollFactor = f2;
        this.mMinFlingVelocity = viewConfiguration.getScaledMinimumFlingVelocity();
        this.mMaxFlingVelocity = viewConfiguration.getScaledMaximumFlingVelocity();
        setWillNotDraw(getOverScrollMode() == 2);
        this.mItemAnimator.a = this.mItemAnimatorListener;
        initAdapterManager();
        initChildrenHelper();
        initAutofill();
        if (ViewCompat.i(this) == 0) {
            setImportantForAccessibility(1);
        }
        this.mAccessibilityManager = (AccessibilityManager) getContext().getSystemService("accessibility");
        setAccessibilityDelegateCompat(new RecyclerViewAccessibilityDelegate(this));
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, i.r.c.RecyclerView, i2, 0);
        if (Build.VERSION.SDK_INT >= 29) {
            saveAttributeDataForStyleable(context, i.r.c.RecyclerView, attributeSet, obtainStyledAttributes, i2, 0);
        }
        String string = obtainStyledAttributes.getString(i.r.c.RecyclerView_layoutManager);
        if (obtainStyledAttributes.getInt(i.r.c.RecyclerView_android_descendantFocusability, -1) == -1) {
            setDescendantFocusability(262144);
        }
        this.mClipToPadding = obtainStyledAttributes.getBoolean(i.r.c.RecyclerView_android_clipToPadding, true);
        boolean z2 = obtainStyledAttributes.getBoolean(i.r.c.RecyclerView_fastScrollEnabled, false);
        this.mEnableFastScroller = z2;
        if (z2) {
            initFastScroller((StateListDrawable) obtainStyledAttributes.getDrawable(i.r.c.RecyclerView_fastScrollVerticalThumbDrawable), obtainStyledAttributes.getDrawable(i.r.c.RecyclerView_fastScrollVerticalTrackDrawable), (StateListDrawable) obtainStyledAttributes.getDrawable(i.r.c.RecyclerView_fastScrollHorizontalThumbDrawable), obtainStyledAttributes.getDrawable(i.r.c.RecyclerView_fastScrollHorizontalTrackDrawable));
        }
        obtainStyledAttributes.recycle();
        createLayoutManager(context, string, attributeSet, i2, 0);
        TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(attributeSet, NESTED_SCROLLING_ATTRS, i2, 0);
        if (Build.VERSION.SDK_INT >= 29) {
            saveAttributeDataForStyleable(context, NESTED_SCROLLING_ATTRS, attributeSet, obtainStyledAttributes2, i2, 0);
        }
        boolean z3 = obtainStyledAttributes2.getBoolean(0, true);
        obtainStyledAttributes2.recycle();
        setNestedScrollingEnabled(z3);
    }

    public boolean dispatchNestedPreScroll(int i2, int i3, int[] iArr, int[] iArr2, int i4) {
        return getScrollingChildHelper().a(i2, i3, iArr, iArr2, i4);
    }

    public boolean hasNestedScrollingParent(int i2) {
        return getScrollingChildHelper().a(i2) != null;
    }

    public void smoothScrollBy(int i2, int i3, Interpolator interpolator, int i4) {
        smoothScrollBy(i2, i3, interpolator, i4, false);
    }

    public boolean startNestedScroll(int i2, int i3) {
        return getScrollingChildHelper().a(i2, i3);
    }

    public void stopNestedScroll(int i2) {
        getScrollingChildHelper().c(i2);
    }

    public static class a0 {
        public int a = -1;
        public int b = 0;
        public int c = 0;
        public int d = 1;

        /* renamed from: e  reason: collision with root package name */
        public int f290e = 0;

        /* renamed from: f  reason: collision with root package name */
        public boolean f291f = false;
        public boolean g = false;
        public boolean h = false;

        /* renamed from: i  reason: collision with root package name */
        public boolean f292i = false;

        /* renamed from: j  reason: collision with root package name */
        public boolean f293j = false;

        /* renamed from: k  reason: collision with root package name */
        public boolean f294k = false;

        /* renamed from: l  reason: collision with root package name */
        public int f295l;

        /* renamed from: m  reason: collision with root package name */
        public long f296m;

        /* renamed from: n  reason: collision with root package name */
        public int f297n;

        /* renamed from: o  reason: collision with root package name */
        public int f298o;

        public void a(int i2) {
            if ((this.d & i2) == 0) {
                StringBuilder a2 = outline.a("Layout state should be one of ");
                a2.append(Integer.toBinaryString(i2));
                a2.append(" but it is ");
                a2.append(Integer.toBinaryString(this.d));
                throw new IllegalStateException(a2.toString());
            }
        }

        public String toString() {
            StringBuilder a2 = outline.a("State{mTargetPosition=");
            a2.append(this.a);
            a2.append(", mData=");
            a2.append((Object) null);
            a2.append(", mItemCount=");
            a2.append(this.f290e);
            a2.append(", mIsMeasuring=");
            a2.append(this.f292i);
            a2.append(", mPreviousLayoutItemCount=");
            a2.append(this.b);
            a2.append(", mDeletedInvisibleItemCountSincePreviousLayout=");
            a2.append(this.c);
            a2.append(", mStructureChanged=");
            a2.append(this.f291f);
            a2.append(", mInPreLayout=");
            a2.append(this.g);
            a2.append(", mRunSimpleAnimations=");
            a2.append(this.f293j);
            a2.append(", mRunPredictiveAnimations=");
            a2.append(this.f294k);
            a2.append('}');
            return a2.toString();
        }

        public int a() {
            return this.g ? this.b - this.c : this.f290e;
        }
    }

    public class c0 implements Runnable {
        public int b;
        public int c;
        public OverScroller d;

        /* renamed from: e  reason: collision with root package name */
        public Interpolator f299e = RecyclerView.sQuinticInterpolator;

        /* renamed from: f  reason: collision with root package name */
        public boolean f300f = false;
        public boolean g = false;

        public c0() {
            this.d = new OverScroller(RecyclerView.this.getContext(), RecyclerView.sQuinticInterpolator);
        }

        public void a() {
            if (this.f300f) {
                this.g = true;
                return;
            }
            RecyclerView.this.removeCallbacks(this);
            ViewCompat.a(RecyclerView.this, this);
        }

        public void b() {
            RecyclerView.this.removeCallbacks(this);
            this.d.abortAnimation();
        }

        public void run() {
            int i2;
            int i3;
            RecyclerView recyclerView = RecyclerView.this;
            if (recyclerView.mLayout == null) {
                b();
                return;
            }
            this.g = false;
            this.f300f = true;
            recyclerView.consumePendingUpdateOperations();
            OverScroller overScroller = this.d;
            if (overScroller.computeScrollOffset()) {
                int currX = overScroller.getCurrX();
                int currY = overScroller.getCurrY();
                int i4 = currX - this.b;
                int i5 = currY - this.c;
                this.b = currX;
                this.c = currY;
                RecyclerView recyclerView2 = RecyclerView.this;
                int[] iArr = recyclerView2.mReusableIntPair;
                iArr[0] = 0;
                iArr[1] = 0;
                if (recyclerView2.dispatchNestedPreScroll(i4, i5, iArr, null, 1)) {
                    int[] iArr2 = RecyclerView.this.mReusableIntPair;
                    i4 -= iArr2[0];
                    i5 -= iArr2[1];
                }
                if (RecyclerView.this.getOverScrollMode() != 2) {
                    RecyclerView.this.considerReleasingGlowsOnScroll(i4, i5);
                }
                RecyclerView recyclerView3 = RecyclerView.this;
                if (recyclerView3.mAdapter != null) {
                    int[] iArr3 = recyclerView3.mReusableIntPair;
                    iArr3[0] = 0;
                    iArr3[1] = 0;
                    recyclerView3.scrollStep(i4, i5, iArr3);
                    RecyclerView recyclerView4 = RecyclerView.this;
                    int[] iArr4 = recyclerView4.mReusableIntPair;
                    i2 = iArr4[0];
                    i3 = iArr4[1];
                    i4 -= i2;
                    i5 -= i3;
                    z zVar = recyclerView4.mLayout.g;
                    if (zVar != null && !zVar.d && zVar.f330e) {
                        int a = recyclerView4.mState.a();
                        if (a == 0) {
                            zVar.a();
                        } else if (zVar.a >= a) {
                            zVar.a = a - 1;
                            zVar.a(i2, i3);
                        } else {
                            zVar.a(i2, i3);
                        }
                    }
                } else {
                    i3 = 0;
                    i2 = 0;
                }
                if (!RecyclerView.this.mItemDecorations.isEmpty()) {
                    RecyclerView.this.invalidate();
                }
                RecyclerView recyclerView5 = RecyclerView.this;
                int[] iArr5 = recyclerView5.mReusableIntPair;
                iArr5[0] = 0;
                iArr5[1] = 0;
                recyclerView5.dispatchNestedScroll(i2, i3, i4, i5, null, 1, iArr5);
                int[] iArr6 = RecyclerView.this.mReusableIntPair;
                int i6 = i4 - iArr6[0];
                int i7 = i5 - iArr6[1];
                if (!(i2 == 0 && i3 == 0)) {
                    RecyclerView.this.dispatchOnScrolled(i2, i3);
                }
                if (!RecyclerView.this.awakenScrollBars()) {
                    RecyclerView.this.invalidate();
                }
                boolean z = overScroller.isFinished() || (((overScroller.getCurrX() == overScroller.getFinalX()) || i6 != 0) && ((overScroller.getCurrY() == overScroller.getFinalY()) || i7 != 0));
                z zVar2 = RecyclerView.this.mLayout.g;
                if ((zVar2 != null && zVar2.d) || !z) {
                    a();
                    RecyclerView recyclerView6 = RecyclerView.this;
                    GapWorker gapWorker = recyclerView6.mGapWorker;
                    if (gapWorker != null) {
                        gapWorker.a(recyclerView6, i2, i3);
                    }
                } else {
                    if (RecyclerView.this.getOverScrollMode() != 2) {
                        int currVelocity = (int) overScroller.getCurrVelocity();
                        int i8 = i6 < 0 ? -currVelocity : i6 > 0 ? currVelocity : 0;
                        if (i7 < 0) {
                            currVelocity = -currVelocity;
                        } else if (i7 <= 0) {
                            currVelocity = 0;
                        }
                        RecyclerView.this.absorbGlows(i8, currVelocity);
                    }
                    if (RecyclerView.ALLOW_THREAD_GAP_WORK) {
                        GapWorker.b bVar = RecyclerView.this.mPrefetchRegistry;
                        int[] iArr7 = bVar.c;
                        if (iArr7 != null) {
                            Arrays.fill(iArr7, -1);
                        }
                        bVar.d = 0;
                    }
                }
            }
            z zVar3 = RecyclerView.this.mLayout.g;
            if (zVar3 != null && zVar3.d) {
                zVar3.a(0, 0);
            }
            this.f300f = false;
            if (this.g) {
                RecyclerView.this.removeCallbacks(this);
                ViewCompat.a(RecyclerView.this, this);
                return;
            }
            RecyclerView.this.setScrollState(0);
            RecyclerView.this.stopNestedScroll(1);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.min(float, float):float}
         arg types: [int, float]
         candidates:
          ClspMth{java.lang.Math.min(double, double):double}
          ClspMth{java.lang.Math.min(long, long):long}
          ClspMth{java.lang.Math.min(int, int):int}
          ClspMth{java.lang.Math.min(float, float):float} */
        public void a(int i2, int i3, int i4, Interpolator interpolator) {
            int i5;
            if (i4 == Integer.MIN_VALUE) {
                int abs = Math.abs(i2);
                int abs2 = Math.abs(i3);
                boolean z = abs > abs2;
                int sqrt = (int) Math.sqrt((double) 0);
                int sqrt2 = (int) Math.sqrt((double) ((i3 * i3) + (i2 * i2)));
                RecyclerView recyclerView = RecyclerView.this;
                int width = z ? recyclerView.getWidth() : recyclerView.getHeight();
                int i6 = width / 2;
                float f2 = (float) width;
                float f3 = (float) i6;
                float sin = (((float) Math.sin((double) ((Math.min(1.0f, (((float) sqrt2) * 1.0f) / f2) - 0.5f) * 0.47123894f))) * f3) + f3;
                if (sqrt > 0) {
                    i5 = Math.round(Math.abs(sin / ((float) sqrt)) * 1000.0f) * 4;
                } else {
                    if (!z) {
                        abs = abs2;
                    }
                    i5 = (int) (((((float) abs) / f2) + 1.0f) * 300.0f);
                }
                i4 = Math.min(i5, (int) RecyclerView.MAX_SCROLL_DURATION);
            }
            int i7 = i4;
            if (interpolator == null) {
                interpolator = RecyclerView.sQuinticInterpolator;
            }
            if (this.f299e != interpolator) {
                this.f299e = interpolator;
                this.d = new OverScroller(RecyclerView.this.getContext(), interpolator);
            }
            this.c = 0;
            this.b = 0;
            RecyclerView.this.setScrollState(2);
            this.d.startScroll(0, 0, i2, i3, i7);
            if (Build.VERSION.SDK_INT < 23) {
                this.d.computeScrollOffset();
            }
            a();
        }
    }

    public static class p extends ViewGroup.MarginLayoutParams {
        public d0 a;
        public final Rect b = new Rect();
        public boolean c = true;
        public boolean d = false;

        public p(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public int a() {
            return this.a.d();
        }

        public boolean b() {
            return this.a.m();
        }

        public boolean c() {
            return this.a.j();
        }

        public p(int i2, int i3) {
            super(i2, i3);
        }

        public p(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(super);
        }

        public p(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public p(p pVar) {
            super((ViewGroup.LayoutParams) pVar);
        }
    }

    public static class y extends AbsSavedState {
        public static final Parcelable.Creator<y> CREATOR = new a();
        public Parcelable d;

        public static class a implements Parcelable.ClassLoaderCreator<y> {
            public Object createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new y(parcel, classLoader);
            }

            public Object[] newArray(int i2) {
                return new y[i2];
            }

            public Object createFromParcel(Parcel parcel) {
                return new y(parcel, null);
            }
        }

        public y(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.d = parcel.readParcelable(classLoader == null ? o.class.getClassLoader() : classLoader);
        }

        public void writeToParcel(Parcel parcel, int i2) {
            parcel.writeParcelable(super.b, i2);
            parcel.writeParcelable(this.d, 0);
        }

        public y(Parcelable parcelable) {
            super(parcelable);
        }
    }

    public final void dispatchNestedScroll(int i2, int i3, int i4, int i5, int[] iArr, int i6, int[] iArr2) {
        getScrollingChildHelper().b(i2, i3, i4, i5, iArr, i6, iArr2);
    }

    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        o oVar = this.mLayout;
        if (oVar != null) {
            return oVar.a(layoutParams);
        }
        throw new IllegalStateException(outline.a(this, outline.a("RecyclerView has no LayoutManager")));
    }

    public void smoothScrollBy(int i2, int i3, Interpolator interpolator, int i4, boolean z2) {
        o oVar = this.mLayout;
        if (oVar == null) {
            Log.e(TAG, "Cannot smooth scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
        } else if (!this.mLayoutSuppressed) {
            int i5 = 0;
            if (!oVar.a()) {
                i2 = 0;
            }
            if (!this.mLayout.b()) {
                i3 = 0;
            }
            if (i2 != 0 || i3 != 0) {
                if (i4 == Integer.MIN_VALUE || i4 > 0) {
                    if (z2) {
                        if (i2 != 0) {
                            i5 = 1;
                        }
                        if (i3 != 0) {
                            i5 |= 2;
                        }
                        startNestedScroll(i5, 1);
                    }
                    this.mViewFlinger.a(i2, i3, i4, interpolator);
                    return;
                }
                scrollBy(i2, i3);
            }
        }
    }

    public static abstract class z {
        public int a = -1;
        public RecyclerView b;
        public o c;
        public boolean d;

        /* renamed from: e  reason: collision with root package name */
        public boolean f330e;

        /* renamed from: f  reason: collision with root package name */
        public View f331f;
        public final a g = new a(0, 0);
        public boolean h;

        public interface b {
            PointF a(int i2);
        }

        public PointF a(int i2) {
            o oVar = this.c;
            if (oVar instanceof b) {
                return ((b) oVar).a(i2);
            }
            StringBuilder a2 = outline.a("You should override computeScrollVectorForPosition when the LayoutManager does not implement ");
            a2.append(b.class.getCanonicalName());
            Log.w(RecyclerView.TAG, a2.toString());
            return null;
        }

        public abstract void a(View view, a0 a0Var, a aVar);

        public final void a() {
            if (this.f330e) {
                this.f330e = false;
                LinearSmoothScroller linearSmoothScroller = (LinearSmoothScroller) this;
                linearSmoothScroller.f1409p = 0;
                linearSmoothScroller.f1408o = 0;
                linearSmoothScroller.f1404k = null;
                this.b.mState.a = -1;
                this.f331f = null;
                this.a = -1;
                this.d = false;
                o oVar = this.c;
                if (oVar.g == this) {
                    oVar.g = null;
                }
                this.c = null;
                this.b = null;
            }
        }

        public static class a {
            public int a;
            public int b;
            public int c;
            public int d = -1;

            /* renamed from: e  reason: collision with root package name */
            public Interpolator f332e;

            /* renamed from: f  reason: collision with root package name */
            public boolean f333f = false;
            public int g = 0;

            public a(int i2, int i3) {
                this.a = i2;
                this.b = i3;
                this.c = RecyclerView.UNDEFINED_DURATION;
                this.f332e = null;
            }

            public void a(RecyclerView recyclerView) {
                int i2 = this.d;
                if (i2 >= 0) {
                    this.d = -1;
                    recyclerView.jumpToPositionForSmoothScroller(i2);
                    this.f333f = false;
                } else if (!this.f333f) {
                    this.g = 0;
                } else if (this.f332e == null || this.c >= 1) {
                    int i3 = this.c;
                    if (i3 >= 1) {
                        recyclerView.mViewFlinger.a(this.a, this.b, i3, this.f332e);
                        int i4 = this.g + 1;
                        this.g = i4;
                        if (i4 > 10) {
                            Log.e(RecyclerView.TAG, "Smooth Scroll action is being updated too frequently. Make sure you are not changing it unless necessary");
                        }
                        this.f333f = false;
                        return;
                    }
                    throw new IllegalStateException("Scroll duration must be a positive number");
                } else {
                    throw new IllegalStateException("If you provide an interpolator, you must set a positive duration");
                }
            }

            public void a(int i2, int i3, int i4, Interpolator interpolator) {
                this.a = i2;
                this.b = i3;
                this.c = i4;
                this.f332e = interpolator;
                this.f333f = true;
            }
        }

        public void a(int i2, int i3) {
            PointF a2;
            RecyclerView recyclerView = this.b;
            if (this.a == -1 || recyclerView == null) {
                a();
            }
            if (!(!this.d || this.f331f != null || this.c == null || (a2 = a(this.a)) == null || (a2.x == 0.0f && a2.y == 0.0f))) {
                recyclerView.scrollStep((int) Math.signum(a2.x), (int) Math.signum(a2.y), null);
            }
            boolean z = false;
            this.d = false;
            View view = this.f331f;
            if (view != null) {
                if (this.b.getChildLayoutPosition(view) == this.a) {
                    a(this.f331f, recyclerView.mState, this.g);
                    this.g.a(recyclerView);
                    a();
                } else {
                    Log.e(RecyclerView.TAG, "Passed over target position while smooth scrolling.");
                    this.f331f = null;
                }
            }
            if (this.f330e) {
                a0 a0Var = recyclerView.mState;
                a aVar = this.g;
                LinearSmoothScroller linearSmoothScroller = (LinearSmoothScroller) this;
                if (linearSmoothScroller.b.mLayout.d() == 0) {
                    linearSmoothScroller.a();
                } else {
                    int i4 = linearSmoothScroller.f1408o;
                    int i5 = i4 - i2;
                    if (i4 * i5 <= 0) {
                        i5 = 0;
                    }
                    linearSmoothScroller.f1408o = i5;
                    int i6 = linearSmoothScroller.f1409p;
                    int i7 = i6 - i3;
                    if (i6 * i7 <= 0) {
                        i7 = 0;
                    }
                    linearSmoothScroller.f1409p = i7;
                    if (linearSmoothScroller.f1408o == 0 && i7 == 0) {
                        PointF a3 = linearSmoothScroller.a(linearSmoothScroller.a);
                        if (a3 == null || (a3.x == 0.0f && a3.y == 0.0f)) {
                            aVar.d = linearSmoothScroller.a;
                            linearSmoothScroller.a();
                        } else {
                            float f2 = a3.x;
                            float f3 = a3.y;
                            float sqrt = (float) Math.sqrt((double) ((f3 * f3) + (f2 * f2)));
                            float f4 = a3.x / sqrt;
                            a3.x = f4;
                            float f5 = a3.y / sqrt;
                            a3.y = f5;
                            linearSmoothScroller.f1404k = a3;
                            linearSmoothScroller.f1408o = (int) (f4 * 10000.0f);
                            linearSmoothScroller.f1409p = (int) (f5 * 10000.0f);
                            aVar.a((int) (((float) linearSmoothScroller.f1408o) * 1.2f), (int) (((float) linearSmoothScroller.f1409p) * 1.2f), (int) (((float) linearSmoothScroller.b(AbstractSpiCall.DEFAULT_TIMEOUT)) * 1.2f), linearSmoothScroller.f1402i);
                        }
                    }
                }
                if (this.g.d >= 0) {
                    z = true;
                }
                this.g.a(recyclerView);
                if (z && this.f330e) {
                    this.d = true;
                    recyclerView.mViewFlinger.a();
                }
            }
        }
    }

    public void addItemDecoration(n nVar) {
        addItemDecoration(nVar, -1);
    }

    public static abstract class l {
        public b a = null;
        public ArrayList<a> b = new ArrayList<>();
        public long c = 120;
        public long d = 120;

        /* renamed from: e  reason: collision with root package name */
        public long f314e = 250;

        /* renamed from: f  reason: collision with root package name */
        public long f315f = 250;

        public interface a {
            void a();
        }

        public interface b {
        }

        public static class c {
            public int a;
            public int b;
        }

        public static int d(d0 d0Var) {
            int i2 = d0Var.f305j & 14;
            if (d0Var.h()) {
                return 4;
            }
            if ((i2 & 4) != 0) {
                return i2;
            }
            int i3 = d0Var.d;
            int c2 = d0Var.c();
            return (i3 == -1 || c2 == -1 || i3 == c2) ? i2 : i2 | 2048;
        }

        public final void a(d0 d0Var) {
            b bVar = this.a;
            if (bVar != null) {
                m mVar = (m) bVar;
                if (mVar != null) {
                    boolean z = true;
                    d0Var.a(true);
                    if (d0Var.h != null && d0Var.f304i == null) {
                        d0Var.h = null;
                    }
                    d0Var.f304i = null;
                    if ((d0Var.f305j & 16) == 0) {
                        z = false;
                    }
                    if (!z && !RecyclerView.this.removeAnimatingView(d0Var.a) && d0Var.l()) {
                        RecyclerView.this.removeDetachedView(d0Var.a, false);
                        return;
                    }
                    return;
                }
                throw null;
            }
        }

        public abstract boolean a(d0 d0Var, d0 d0Var2, c cVar, c cVar2);

        public abstract void b();

        public abstract void b(d0 d0Var);

        public c c(d0 d0Var) {
            c cVar = new c();
            View view = d0Var.a;
            cVar.a = view.getLeft();
            cVar.b = view.getTop();
            view.getRight();
            view.getBottom();
            return cVar;
        }

        public abstract boolean c();

        public boolean a(d0 d0Var, List<Object> list) {
            return !((SimpleItemAnimator) this).g || d0Var.h();
        }

        public final void a() {
            int size = this.b.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.b.get(i2).a();
            }
            this.b.clear();
        }
    }
}
