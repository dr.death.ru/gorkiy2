package androidx.savedstate;

import i.o.GenericLifecycleObserver;
import i.o.Lifecycle;
import i.o.LifecycleOwner;
import i.t.SavedStateRegistry;

public class SavedStateRegistry$1 implements GenericLifecycleObserver {
    public final /* synthetic */ SavedStateRegistry a;

    public SavedStateRegistry$1(SavedStateRegistry savedStateRegistry) {
        this.a = savedStateRegistry;
    }

    public void a(LifecycleOwner lifecycleOwner, Lifecycle.a aVar) {
        if (aVar == Lifecycle.a.ON_START) {
            this.a.d = true;
        } else if (aVar == Lifecycle.a.ON_STOP) {
            this.a.d = false;
        }
    }
}
