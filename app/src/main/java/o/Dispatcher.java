package o;

import java.io.InterruptedIOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import n.AssertionsJVM;
import n.n.c.Intrinsics;
import o.RealCall;
import o.d0;
import o.m0.Util;
import o.m0.d.Transmitter;
import r.OkHttpCall;
import r.Utils;

/* compiled from: Dispatcher.kt */
public final class Dispatcher {
    public int a = 64;
    public int b = 5;
    public ExecutorService c;
    public final ArrayDeque<d0.a> d = new ArrayDeque<>();

    /* renamed from: e  reason: collision with root package name */
    public final ArrayDeque<d0.a> f2845e = new ArrayDeque<>();

    /* renamed from: f  reason: collision with root package name */
    public final ArrayDeque<d0> f2846f = new ArrayDeque<>();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: o.m0.Util.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
     arg types: [java.lang.String, int]
     candidates:
      o.m0.Util.a(byte, int):int
      o.m0.Util.a(java.lang.String, int):int
      o.m0.Util.a(java.lang.String, long):long
      o.m0.Util.a(java.lang.String, java.lang.Object[]):java.lang.String
      o.m0.Util.a(o.HttpUrl, boolean):java.lang.String
      o.m0.Util.a(p.BufferedSource, java.nio.charset.Charset):java.nio.charset.Charset
      o.m0.Util.a(java.lang.Object, long):void
      o.m0.Util.a(p.BufferedSink, int):void
      o.m0.Util.a(o.HttpUrl, o.HttpUrl):boolean
      o.m0.Util.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory */
    public final synchronized ExecutorService a() {
        ExecutorService executorService;
        if (this.c == null) {
            this.c = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), Util.a("OkHttp Dispatcher", false));
        }
        executorService = this.c;
        if (executorService == null) {
            Intrinsics.a();
            throw null;
        }
        return executorService;
    }

    public final void b(RealCall.a aVar) {
        if (aVar != null) {
            aVar.b.decrementAndGet();
            a(this.f2845e, aVar);
            return;
        }
        Intrinsics.a("call");
        throw null;
    }

    public final synchronized int c() {
        return this.f2845e.size() + this.f2846f.size();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Iterator<o.d0$a>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [o.RealCall$a, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final boolean b() {
        int i2;
        boolean z;
        RealCall.a aVar;
        boolean z2 = !Thread.holdsLock(this);
        if (!AssertionsJVM.a || z2) {
            ArrayList arrayList = new ArrayList();
            synchronized (this) {
                Iterator<d0.a> it = this.d.iterator();
                Intrinsics.a((Object) it, "readyAsyncCalls.iterator()");
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    RealCall.a next = it.next();
                    if (this.f2845e.size() >= this.a) {
                        break;
                    } else if (next.b.get() < this.b) {
                        it.remove();
                        next.b.incrementAndGet();
                        Intrinsics.a((Object) next, "asyncCall");
                        arrayList.add(next);
                        this.f2845e.add(next);
                    }
                }
                i2 = 0;
                z = c() > 0;
            }
            int size = arrayList.size();
            while (i2 < size) {
                aVar = (RealCall.a) arrayList.get(i2);
                ExecutorService a2 = a();
                if (a2 != null) {
                    boolean z3 = !Thread.holdsLock(aVar.d.d.b);
                    if (!AssertionsJVM.a || z3) {
                        try {
                            a2.execute(aVar);
                        } catch (RejectedExecutionException e2) {
                            InterruptedIOException interruptedIOException = new InterruptedIOException("executor rejected");
                            interruptedIOException.initCause(e2);
                            Transmitter transmitter = aVar.d.b;
                            if (transmitter != null) {
                                transmitter.a(interruptedIOException);
                                OkHttpCall.a aVar2 = (OkHttpCall.a) aVar.c;
                                if (aVar2 != null) {
                                    aVar2.a.a(OkHttpCall.this, interruptedIOException);
                                } else {
                                    throw null;
                                }
                            } else {
                                Intrinsics.b("transmitter");
                                throw null;
                            }
                        } catch (Throwable th) {
                            Utils.a(th);
                            th.printStackTrace();
                        }
                        i2++;
                    } else {
                        throw new AssertionError("Assertion failed");
                    }
                } else {
                    Intrinsics.a("executorService");
                    throw null;
                }
            }
            return z;
        }
        throw new AssertionError("Assertion failed");
        aVar.d.d.b.b(aVar);
        i2++;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002f, code lost:
        r0 = r3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(o.RealCall.a r6) {
        /*
            r5 = this;
            r0 = 0
            if (r6 == 0) goto L_0x005c
            monitor-enter(r5)
            java.util.ArrayDeque<o.d0$a> r1 = r5.d     // Catch:{ all -> 0x0059 }
            r1.add(r6)     // Catch:{ all -> 0x0059 }
            o.RealCall r1 = r6.d     // Catch:{ all -> 0x0059 }
            boolean r1 = r1.f2895f     // Catch:{ all -> 0x0059 }
            if (r1 != 0) goto L_0x0054
            java.lang.String r1 = r6.a()     // Catch:{ all -> 0x0059 }
            java.util.ArrayDeque<o.d0$a> r2 = r5.f2845e     // Catch:{ all -> 0x0059 }
            java.util.Iterator r2 = r2.iterator()     // Catch:{ all -> 0x0059 }
        L_0x0019:
            boolean r3 = r2.hasNext()     // Catch:{ all -> 0x0059 }
            if (r3 == 0) goto L_0x0031
            java.lang.Object r3 = r2.next()     // Catch:{ all -> 0x0059 }
            o.RealCall$a r3 = (o.RealCall.a) r3     // Catch:{ all -> 0x0059 }
            java.lang.String r4 = r3.a()     // Catch:{ all -> 0x0059 }
            boolean r4 = n.n.c.Intrinsics.a(r4, r1)     // Catch:{ all -> 0x0059 }
            if (r4 == 0) goto L_0x0019
        L_0x002f:
            r0 = r3
            goto L_0x004e
        L_0x0031:
            java.util.ArrayDeque<o.d0$a> r2 = r5.d     // Catch:{ all -> 0x0059 }
            java.util.Iterator r2 = r2.iterator()     // Catch:{ all -> 0x0059 }
        L_0x0037:
            boolean r3 = r2.hasNext()     // Catch:{ all -> 0x0059 }
            if (r3 == 0) goto L_0x004e
            java.lang.Object r3 = r2.next()     // Catch:{ all -> 0x0059 }
            o.RealCall$a r3 = (o.RealCall.a) r3     // Catch:{ all -> 0x0059 }
            java.lang.String r4 = r3.a()     // Catch:{ all -> 0x0059 }
            boolean r4 = n.n.c.Intrinsics.a(r4, r1)     // Catch:{ all -> 0x0059 }
            if (r4 == 0) goto L_0x0037
            goto L_0x002f
        L_0x004e:
            if (r0 == 0) goto L_0x0054
            java.util.concurrent.atomic.AtomicInteger r0 = r0.b     // Catch:{ all -> 0x0059 }
            r6.b = r0     // Catch:{ all -> 0x0059 }
        L_0x0054:
            monitor-exit(r5)
            r5.b()
            return
        L_0x0059:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        L_0x005c:
            java.lang.String r6 = "call"
            n.n.c.Intrinsics.a(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: o.Dispatcher.a(o.RealCall$a):void");
    }

    public final synchronized void a(RealCall realCall) {
        if (realCall != null) {
            this.f2846f.add(realCall);
        } else {
            Intrinsics.a("call");
            throw null;
        }
    }

    public final <T> void a(Deque<T> deque, T t2) {
        synchronized (this) {
            if (!deque.remove(t2)) {
                throw new AssertionError("Call wasn't in-flight!");
            }
        }
        boolean b2 = b();
    }
}
