package o.m0.d;

import androidx.recyclerview.widget.RecyclerView;
import j.a.a.a.outline;
import java.io.IOException;
import java.lang.ref.Reference;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Socket;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlin.TypeCastException;
import l.a.a.a.o.b.AbstractSpiCall;
import n.AssertionsJVM;
import n.i.Collections;
import n.n.c.Intrinsics;
import n.r.Indent;
import o.Address;
import o.Call;
import o.Connection;
import o.EventListener;
import o.Handshake;
import o.HttpUrl;
import o.Interceptor;
import o.OkHttpClient;
import o.Protocol;
import o.Request;
import o.RequestBody;
import o.Response;
import o.Route;
import o.m0.Util;
import o.m0.e.ExchangeCode;
import o.m0.f.Http1ExchangeCodec;
import o.m0.g.ErrorCode;
import o.m0.g.Http2Connection;
import o.m0.g.Http2ExchangeCodec;
import o.m0.g.Http2Stream;
import o.m0.i.Platform;
import o.m0.k.OkHostnameVerifier;
import okhttp3.internal.http2.ConnectionShutdownException;
import okhttp3.internal.http2.StreamResetException;
import p.BufferedSink;
import p.BufferedSource;
import p.Source;

/* compiled from: RealConnection.kt */
public final class RealConnection extends Http2Connection.c implements Connection {
    public Socket b;
    public Socket c;
    public Handshake d;

    /* renamed from: e  reason: collision with root package name */
    public Protocol f2949e;

    /* renamed from: f  reason: collision with root package name */
    public Http2Connection f2950f;
    public BufferedSource g;
    public BufferedSink h;

    /* renamed from: i  reason: collision with root package name */
    public boolean f2951i;

    /* renamed from: j  reason: collision with root package name */
    public int f2952j;

    /* renamed from: k  reason: collision with root package name */
    public int f2953k;

    /* renamed from: l  reason: collision with root package name */
    public int f2954l;

    /* renamed from: m  reason: collision with root package name */
    public int f2955m;

    /* renamed from: n  reason: collision with root package name */
    public final List<Reference<l>> f2956n;

    /* renamed from: o  reason: collision with root package name */
    public long f2957o;

    /* renamed from: p  reason: collision with root package name */
    public final RealConnectionPool f2958p;

    /* renamed from: q  reason: collision with root package name */
    public final Route f2959q;

    public RealConnection(RealConnectionPool realConnectionPool, Route route) {
        if (realConnectionPool == null) {
            Intrinsics.a("connectionPool");
            throw null;
        } else if (route != null) {
            this.f2958p = realConnectionPool;
            this.f2959q = route;
            this.f2955m = 1;
            this.f2956n = new ArrayList();
            this.f2957o = RecyclerView.FOREVER_NS;
        } else {
            Intrinsics.a("route");
            throw null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:101:0x0145 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:105:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00dd  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00e4  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00fd  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(int r15, int r16, int r17, int r18, boolean r19, o.Call r20, o.EventListener r21) {
        /*
            r14 = this;
            r7 = r14
            r8 = r20
            r9 = r21
            r10 = 0
            if (r8 == 0) goto L_0x016a
            if (r9 == 0) goto L_0x0164
            o.Protocol r0 = r7.f2949e
            r11 = 1
            if (r0 != 0) goto L_0x0011
            r0 = 1
            goto L_0x0012
        L_0x0011:
            r0 = 0
        L_0x0012:
            if (r0 == 0) goto L_0x0158
            o.Route r0 = r7.f2959q
            o.Address r0 = r0.a
            java.util.List<o.m> r0 = r0.c
            o.m0.d.ConnectionSpecSelector r12 = new o.m0.d.ConnectionSpecSelector
            r12.<init>(r0)
            o.Route r1 = r7.f2959q
            o.Address r1 = r1.a
            javax.net.ssl.SSLSocketFactory r2 = r1.f2797f
            if (r2 != 0) goto L_0x0062
            o.ConnectionSpec r1 = o.ConnectionSpec.h
            boolean r0 = r0.contains(r1)
            if (r0 == 0) goto L_0x0055
            o.Route r0 = r7.f2959q
            o.Address r0 = r0.a
            o.HttpUrl r0 = r0.a
            java.lang.String r0 = r0.f2851e
            o.m0.i.Platform$a r1 = o.m0.i.Platform.c
            o.m0.i.Platform r1 = o.m0.i.Platform.a
            boolean r1 = r1.b(r0)
            if (r1 == 0) goto L_0x0042
            goto L_0x006c
        L_0x0042:
            okhttp3.internal.connection.RouteException r1 = new okhttp3.internal.connection.RouteException
            java.net.UnknownServiceException r2 = new java.net.UnknownServiceException
            java.lang.String r3 = "CLEARTEXT communication to "
            java.lang.String r4 = " not permitted by network security policy"
            java.lang.String r0 = j.a.a.a.outline.a(r3, r0, r4)
            r2.<init>(r0)
            r1.<init>(r2)
            throw r1
        L_0x0055:
            okhttp3.internal.connection.RouteException r0 = new okhttp3.internal.connection.RouteException
            java.net.UnknownServiceException r1 = new java.net.UnknownServiceException
            java.lang.String r2 = "CLEARTEXT communication not enabled for client"
            r1.<init>(r2)
            r0.<init>(r1)
            throw r0
        L_0x0062:
            java.util.List<o.c0> r0 = r1.b
            o.Protocol r1 = o.Protocol.H2_PRIOR_KNOWLEDGE
            boolean r0 = r0.contains(r1)
            if (r0 != 0) goto L_0x014b
        L_0x006c:
            r13 = r10
        L_0x006d:
            o.Route r0 = r7.f2959q     // Catch:{ IOException -> 0x00d4 }
            boolean r0 = r0.a()     // Catch:{ IOException -> 0x00d4 }
            if (r0 == 0) goto L_0x008b
            r1 = r14
            r2 = r15
            r3 = r16
            r4 = r17
            r5 = r20
            r6 = r21
            r1.a(r2, r3, r4, r5, r6)     // Catch:{ IOException -> 0x00d4 }
            java.net.Socket r0 = r7.b     // Catch:{ IOException -> 0x00d4 }
            if (r0 != 0) goto L_0x0087
            goto L_0x00a1
        L_0x0087:
            r1 = r15
            r2 = r16
            goto L_0x0091
        L_0x008b:
            r1 = r15
            r2 = r16
            r14.a(r15, r2, r8, r9)     // Catch:{ IOException -> 0x00d0 }
        L_0x0091:
            r3 = r18
            r14.a(r12, r3, r8, r9)     // Catch:{ IOException -> 0x00ce }
            o.Route r0 = r7.f2959q     // Catch:{ IOException -> 0x00ce }
            java.net.InetSocketAddress r0 = r0.c     // Catch:{ IOException -> 0x00ce }
            o.Route r4 = r7.f2959q     // Catch:{ IOException -> 0x00ce }
            java.net.Proxy r4 = r4.b     // Catch:{ IOException -> 0x00ce }
            r9.a(r8, r0, r4)     // Catch:{ IOException -> 0x00ce }
        L_0x00a1:
            o.Route r0 = r7.f2959q
            boolean r0 = r0.a()
            if (r0 == 0) goto L_0x00bb
            java.net.Socket r0 = r7.b
            if (r0 == 0) goto L_0x00ae
            goto L_0x00bb
        L_0x00ae:
            okhttp3.internal.connection.RouteException r0 = new okhttp3.internal.connection.RouteException
            java.net.ProtocolException r1 = new java.net.ProtocolException
            java.lang.String r2 = "Too many tunnel connections attempted: 21"
            r1.<init>(r2)
            r0.<init>(r1)
            throw r0
        L_0x00bb:
            o.m0.g.Http2Connection r0 = r7.f2950f
            if (r0 == 0) goto L_0x00cd
            o.m0.d.RealConnectionPool r1 = r7.f2958p
            monitor-enter(r1)
            int r0 = r0.f()     // Catch:{ all -> 0x00ca }
            r7.f2955m = r0     // Catch:{ all -> 0x00ca }
            monitor-exit(r1)
            goto L_0x00cd
        L_0x00ca:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x00cd:
            return
        L_0x00ce:
            r0 = move-exception
            goto L_0x00d9
        L_0x00d0:
            r0 = move-exception
        L_0x00d1:
            r3 = r18
            goto L_0x00d9
        L_0x00d4:
            r0 = move-exception
            r1 = r15
            r2 = r16
            goto L_0x00d1
        L_0x00d9:
            java.net.Socket r4 = r7.c
            if (r4 == 0) goto L_0x00e0
            o.m0.Util.a(r4)
        L_0x00e0:
            java.net.Socket r4 = r7.b
            if (r4 == 0) goto L_0x00e7
            o.m0.Util.a(r4)
        L_0x00e7:
            r7.c = r10
            r7.b = r10
            r7.g = r10
            r7.h = r10
            r7.d = r10
            r7.f2949e = r10
            r7.f2950f = r10
            o.Route r4 = r7.f2959q
            java.net.InetSocketAddress r5 = r4.c
            java.net.Proxy r4 = r4.b
            if (r5 == 0) goto L_0x0145
            if (r4 == 0) goto L_0x013f
            if (r13 != 0) goto L_0x0107
            okhttp3.internal.connection.RouteException r13 = new okhttp3.internal.connection.RouteException
            r13.<init>(r0)
            goto L_0x010e
        L_0x0107:
            java.io.IOException r4 = r13.c
            r4.addSuppressed(r0)
            r13.b = r0
        L_0x010e:
            if (r19 == 0) goto L_0x013e
            r12.c = r11
            boolean r4 = r12.b
            if (r4 != 0) goto L_0x0117
            goto L_0x0139
        L_0x0117:
            boolean r4 = r0 instanceof java.net.ProtocolException
            if (r4 == 0) goto L_0x011c
            goto L_0x0139
        L_0x011c:
            boolean r4 = r0 instanceof java.io.InterruptedIOException
            if (r4 == 0) goto L_0x0121
            goto L_0x0139
        L_0x0121:
            boolean r4 = r0 instanceof javax.net.ssl.SSLHandshakeException
            if (r4 == 0) goto L_0x012e
            java.lang.Throwable r4 = r0.getCause()
            boolean r4 = r4 instanceof java.security.cert.CertificateException
            if (r4 == 0) goto L_0x012e
            goto L_0x0139
        L_0x012e:
            boolean r4 = r0 instanceof javax.net.ssl.SSLPeerUnverifiedException
            if (r4 == 0) goto L_0x0133
            goto L_0x0139
        L_0x0133:
            boolean r0 = r0 instanceof javax.net.ssl.SSLException
            if (r0 == 0) goto L_0x0139
            r0 = 1
            goto L_0x013a
        L_0x0139:
            r0 = 0
        L_0x013a:
            if (r0 == 0) goto L_0x013e
            goto L_0x006d
        L_0x013e:
            throw r13
        L_0x013f:
            java.lang.String r0 = "proxy"
            n.n.c.Intrinsics.a(r0)
            throw r10
        L_0x0145:
            java.lang.String r0 = "inetSocketAddress"
            n.n.c.Intrinsics.a(r0)
            throw r10
        L_0x014b:
            okhttp3.internal.connection.RouteException r0 = new okhttp3.internal.connection.RouteException
            java.net.UnknownServiceException r1 = new java.net.UnknownServiceException
            java.lang.String r2 = "H2_PRIOR_KNOWLEDGE cannot be used with HTTPS"
            r1.<init>(r2)
            r0.<init>(r1)
            throw r0
        L_0x0158:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "already connected"
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0164:
            java.lang.String r0 = "eventListener"
            n.n.c.Intrinsics.a(r0)
            throw r10
        L_0x016a:
            java.lang.String r0 = "call"
            n.n.c.Intrinsics.a(r0)
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.d.RealConnection.a(int, int, int, int, boolean, o.Call, o.EventListener):void");
    }

    public final boolean b() {
        return this.f2950f != null;
    }

    public final void c() {
        boolean z = !Thread.holdsLock(this.f2958p);
        if (!AssertionsJVM.a || z) {
            synchronized (this.f2958p) {
                this.f2951i = true;
            }
            return;
        }
        throw new AssertionError("Assertion failed");
    }

    public Socket d() {
        Socket socket = this.c;
        if (socket != null) {
            return socket;
        }
        Intrinsics.a();
        throw null;
    }

    public String toString() {
        Object obj;
        StringBuilder a = outline.a("Connection{");
        a.append(this.f2959q.a.a.f2851e);
        a.append(':');
        a.append(this.f2959q.a.a.f2852f);
        a.append(',');
        a.append(" proxy=");
        a.append(this.f2959q.b);
        a.append(" hostAddress=");
        a.append(this.f2959q.c);
        a.append(" cipherSuite=");
        Handshake handshake = this.d;
        if (handshake == null || (obj = handshake.c) == null) {
            obj = "none";
        }
        a.append(obj);
        a.append(" protocol=");
        a.append(this.f2949e);
        a.append('}');
        return a.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
    public final void a(int i2, int i3, Call call, EventListener eventListener) {
        Socket socket;
        int i4;
        Route route = this.f2959q;
        Proxy proxy = route.b;
        Address address = route.a;
        Proxy.Type type = proxy.type();
        if (type != null && ((i4 = e.a[type.ordinal()]) == 1 || i4 == 2)) {
            socket = address.f2796e.createSocket();
            if (socket == null) {
                Intrinsics.a();
                throw null;
            }
        } else {
            socket = new Socket(proxy);
        }
        this.b = socket;
        InetSocketAddress inetSocketAddress = this.f2959q.c;
        if (eventListener == null) {
            throw null;
        } else if (call == null) {
            Intrinsics.a("call");
            throw null;
        } else if (inetSocketAddress != null) {
            socket.setSoTimeout(i3);
            try {
                Platform.a aVar = Platform.c;
                Platform.a.a(socket, this.f2959q.c, i2);
                try {
                    this.g = Collections.a(Collections.b(socket));
                    this.h = Collections.a(Collections.a(socket));
                } catch (NullPointerException e2) {
                    if (Intrinsics.a((Object) e2.getMessage(), (Object) "throw with null exception")) {
                        throw new IOException(e2);
                    }
                }
            } catch (ConnectException e3) {
                StringBuilder a = outline.a("Failed to connect to ");
                a.append(this.f2959q.c);
                ConnectException connectException = new ConnectException(a.toString());
                connectException.initCause(e3);
                throw connectException;
            }
        } else {
            Intrinsics.a("inetSocketAddress");
            throw null;
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: javax.net.ssl.SSLSocket} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v3, resolved type: java.lang.String} */
    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v2 */
    /* JADX WARN: Type inference failed for: r2v5 */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException}
     arg types: [java.net.Socket, java.lang.String, int, int]
     candidates:
      ClspMth{javax.net.SocketFactory.createSocket(java.lang.String, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException, java.net.UnknownHostException}
      ClspMth{javax.net.SocketFactory.createSocket(java.net.InetAddress, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException}
      ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [javax.net.ssl.SSLSession, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.security.Principal, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i._Arrays.a(java.util.Collection, java.lang.Iterable):java.util.List<T>
     arg types: [java.util.List<java.lang.String>, java.util.List<java.lang.String>]
     candidates:
      n.i._Arrays.a(java.lang.Iterable, java.util.Collection):C
      n.i._Arrays.a(java.util.Collection, java.lang.Iterable):java.util.List<T> */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x018d  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0196  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(o.m0.d.ConnectionSpecSelector r9, int r10, o.Call r11, o.EventListener r12) {
        /*
            r8 = this;
            o.Route r0 = r8.f2959q
            o.Address r0 = r0.a
            javax.net.ssl.SSLSocketFactory r1 = r0.f2797f
            if (r1 != 0) goto L_0x0027
            java.util.List<o.c0> r9 = r0.b
            o.Protocol r11 = o.Protocol.H2_PRIOR_KNOWLEDGE
            boolean r9 = r9.contains(r11)
            if (r9 == 0) goto L_0x001e
            java.net.Socket r9 = r8.b
            r8.c = r9
            o.Protocol r9 = o.Protocol.H2_PRIOR_KNOWLEDGE
            r8.f2949e = r9
            r8.a(r10)
            return
        L_0x001e:
            java.net.Socket r9 = r8.b
            r8.c = r9
            o.Protocol r9 = o.Protocol.HTTP_1_1
            r8.f2949e = r9
            return
        L_0x0027:
            r2 = 0
            if (r12 == 0) goto L_0x01a0
            if (r11 == 0) goto L_0x019a
            if (r1 == 0) goto L_0x0186
            java.net.Socket r11 = r8.b     // Catch:{ all -> 0x018a }
            o.HttpUrl r12 = r0.a     // Catch:{ all -> 0x018a }
            java.lang.String r12 = r12.f2851e     // Catch:{ all -> 0x018a }
            o.HttpUrl r3 = r0.a     // Catch:{ all -> 0x018a }
            int r3 = r3.f2852f     // Catch:{ all -> 0x018a }
            r4 = 1
            java.net.Socket r11 = r1.createSocket(r11, r12, r3, r4)     // Catch:{ all -> 0x018a }
            if (r11 == 0) goto L_0x017e
            javax.net.ssl.SSLSocket r11 = (javax.net.ssl.SSLSocket) r11     // Catch:{ all -> 0x018a }
            o.ConnectionSpec r9 = r9.a(r11)     // Catch:{ all -> 0x017b }
            boolean r12 = r9.b     // Catch:{ all -> 0x017b }
            if (r12 == 0) goto L_0x0056
            o.m0.i.Platform$a r12 = o.m0.i.Platform.c     // Catch:{ all -> 0x017b }
            o.m0.i.Platform r12 = o.m0.i.Platform.a     // Catch:{ all -> 0x017b }
            o.HttpUrl r1 = r0.a     // Catch:{ all -> 0x017b }
            java.lang.String r1 = r1.f2851e     // Catch:{ all -> 0x017b }
            java.util.List<o.c0> r3 = r0.b     // Catch:{ all -> 0x017b }
            r12.a(r11, r1, r3)     // Catch:{ all -> 0x017b }
        L_0x0056:
            r11.startHandshake()     // Catch:{ all -> 0x017b }
            javax.net.ssl.SSLSession r12 = r11.getSession()     // Catch:{ all -> 0x017b }
            o.Handshake$a r1 = o.Handshake.f2848f     // Catch:{ all -> 0x017b }
            java.lang.String r3 = "sslSocketSession"
            n.n.c.Intrinsics.a(r12, r3)     // Catch:{ all -> 0x017b }
            o.Handshake r1 = r1.a(r12)     // Catch:{ all -> 0x017b }
            javax.net.ssl.HostnameVerifier r3 = r0.g     // Catch:{ all -> 0x017b }
            if (r3 == 0) goto L_0x0177
            o.HttpUrl r5 = r0.a     // Catch:{ all -> 0x017b }
            java.lang.String r5 = r5.f2851e     // Catch:{ all -> 0x017b }
            boolean r12 = r3.verify(r5, r12)     // Catch:{ all -> 0x017b }
            if (r12 != 0) goto L_0x0111
            java.util.List r9 = r1.a()     // Catch:{ all -> 0x017b }
            boolean r10 = r9.isEmpty()     // Catch:{ all -> 0x017b }
            r10 = r10 ^ r4
            if (r10 == 0) goto L_0x00f1
            r10 = 0
            java.lang.Object r9 = r9.get(r10)     // Catch:{ all -> 0x017b }
            if (r9 == 0) goto L_0x00e9
            java.security.cert.X509Certificate r9 = (java.security.cert.X509Certificate) r9     // Catch:{ all -> 0x017b }
            javax.net.ssl.SSLPeerUnverifiedException r10 = new javax.net.ssl.SSLPeerUnverifiedException     // Catch:{ all -> 0x017b }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ all -> 0x017b }
            r12.<init>()     // Catch:{ all -> 0x017b }
            java.lang.String r1 = "\n              |Hostname "
            r12.append(r1)     // Catch:{ all -> 0x017b }
            o.HttpUrl r0 = r0.a     // Catch:{ all -> 0x017b }
            java.lang.String r0 = r0.f2851e     // Catch:{ all -> 0x017b }
            r12.append(r0)     // Catch:{ all -> 0x017b }
            java.lang.String r0 = " not verified:\n              |    certificate: "
            r12.append(r0)     // Catch:{ all -> 0x017b }
            o.CertificatePinner$a r0 = o.CertificatePinner.d     // Catch:{ all -> 0x017b }
            java.lang.String r0 = r0.a(r9)     // Catch:{ all -> 0x017b }
            r12.append(r0)     // Catch:{ all -> 0x017b }
            java.lang.String r0 = "\n              |    DN: "
            r12.append(r0)     // Catch:{ all -> 0x017b }
            java.security.Principal r0 = r9.getSubjectDN()     // Catch:{ all -> 0x017b }
            java.lang.String r1 = "cert.subjectDN"
            n.n.c.Intrinsics.a(r0, r1)     // Catch:{ all -> 0x017b }
            java.lang.String r0 = r0.getName()     // Catch:{ all -> 0x017b }
            r12.append(r0)     // Catch:{ all -> 0x017b }
            java.lang.String r0 = "\n              |    subjectAltNames: "
            r12.append(r0)     // Catch:{ all -> 0x017b }
            o.m0.k.OkHostnameVerifier r0 = o.m0.k.OkHostnameVerifier.a     // Catch:{ all -> 0x017b }
            r1 = 7
            java.util.List r1 = r0.a(r9, r1)     // Catch:{ all -> 0x017b }
            r3 = 2
            java.util.List r9 = r0.a(r9, r3)     // Catch:{ all -> 0x017b }
            java.util.List r9 = n.i._Arrays.a(r1, r9)     // Catch:{ all -> 0x017b }
            r12.append(r9)     // Catch:{ all -> 0x017b }
            java.lang.String r9 = "\n              "
            r12.append(r9)     // Catch:{ all -> 0x017b }
            java.lang.String r9 = r12.toString()     // Catch:{ all -> 0x017b }
            java.lang.String r9 = n.r.Indent.a(r9, r2, r4)     // Catch:{ all -> 0x017b }
            r10.<init>(r9)     // Catch:{ all -> 0x017b }
            throw r10     // Catch:{ all -> 0x017b }
        L_0x00e9:
            kotlin.TypeCastException r9 = new kotlin.TypeCastException     // Catch:{ all -> 0x017b }
            java.lang.String r10 = "null cannot be cast to non-null type java.security.cert.X509Certificate"
            r9.<init>(r10)     // Catch:{ all -> 0x017b }
            throw r9     // Catch:{ all -> 0x017b }
        L_0x00f1:
            javax.net.ssl.SSLPeerUnverifiedException r9 = new javax.net.ssl.SSLPeerUnverifiedException     // Catch:{ all -> 0x017b }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ all -> 0x017b }
            r10.<init>()     // Catch:{ all -> 0x017b }
            java.lang.String r12 = "Hostname "
            r10.append(r12)     // Catch:{ all -> 0x017b }
            o.HttpUrl r12 = r0.a     // Catch:{ all -> 0x017b }
            java.lang.String r12 = r12.f2851e     // Catch:{ all -> 0x017b }
            r10.append(r12)     // Catch:{ all -> 0x017b }
            java.lang.String r12 = " not verified (no certificates)"
            r10.append(r12)     // Catch:{ all -> 0x017b }
            java.lang.String r10 = r10.toString()     // Catch:{ all -> 0x017b }
            r9.<init>(r10)     // Catch:{ all -> 0x017b }
            throw r9     // Catch:{ all -> 0x017b }
        L_0x0111:
            o.CertificatePinner r12 = r0.h     // Catch:{ all -> 0x017b }
            if (r12 == 0) goto L_0x0173
            o.Handshake r3 = new o.Handshake     // Catch:{ all -> 0x017b }
            o.TlsVersion r4 = r1.b     // Catch:{ all -> 0x017b }
            o.CipherSuite r5 = r1.c     // Catch:{ all -> 0x017b }
            java.util.List<java.security.cert.Certificate> r6 = r1.d     // Catch:{ all -> 0x017b }
            o.m0.d.RealConnection0 r7 = new o.m0.d.RealConnection0     // Catch:{ all -> 0x017b }
            r7.<init>(r12, r1, r0)     // Catch:{ all -> 0x017b }
            r3.<init>(r4, r5, r6, r7)     // Catch:{ all -> 0x017b }
            r8.d = r3     // Catch:{ all -> 0x017b }
            o.HttpUrl r0 = r0.a     // Catch:{ all -> 0x017b }
            java.lang.String r0 = r0.f2851e     // Catch:{ all -> 0x017b }
            o.m0.d.RealConnection1 r1 = new o.m0.d.RealConnection1     // Catch:{ all -> 0x017b }
            r1.<init>(r8)     // Catch:{ all -> 0x017b }
            r12.a(r0, r1)     // Catch:{ all -> 0x017b }
            boolean r9 = r9.b     // Catch:{ all -> 0x017b }
            if (r9 == 0) goto L_0x013f
            o.m0.i.Platform$a r9 = o.m0.i.Platform.c     // Catch:{ all -> 0x017b }
            o.m0.i.Platform r9 = o.m0.i.Platform.a     // Catch:{ all -> 0x017b }
            java.lang.String r2 = r9.b(r11)     // Catch:{ all -> 0x017b }
        L_0x013f:
            r8.c = r11     // Catch:{ all -> 0x017b }
            p.Source r9 = n.i.Collections.b(r11)     // Catch:{ all -> 0x017b }
            p.BufferedSource r9 = n.i.Collections.a(r9)     // Catch:{ all -> 0x017b }
            r8.g = r9     // Catch:{ all -> 0x017b }
            p.Sink r9 = n.i.Collections.a(r11)     // Catch:{ all -> 0x017b }
            p.BufferedSink r9 = n.i.Collections.a(r9)     // Catch:{ all -> 0x017b }
            r8.h = r9     // Catch:{ all -> 0x017b }
            if (r2 == 0) goto L_0x015e
            o.Protocol$a r9 = o.Protocol.Companion     // Catch:{ all -> 0x017b }
            o.Protocol r9 = r9.a(r2)     // Catch:{ all -> 0x017b }
            goto L_0x0160
        L_0x015e:
            o.Protocol r9 = o.Protocol.HTTP_1_1     // Catch:{ all -> 0x017b }
        L_0x0160:
            r8.f2949e = r9     // Catch:{ all -> 0x017b }
            o.m0.i.Platform$a r9 = o.m0.i.Platform.c
            o.m0.i.Platform r9 = o.m0.i.Platform.a
            r9.a(r11)
            o.Protocol r9 = r8.f2949e
            o.Protocol r11 = o.Protocol.HTTP_2
            if (r9 != r11) goto L_0x0172
            r8.a(r10)
        L_0x0172:
            return
        L_0x0173:
            n.n.c.Intrinsics.a()     // Catch:{ all -> 0x017b }
            throw r2
        L_0x0177:
            n.n.c.Intrinsics.a()     // Catch:{ all -> 0x017b }
            throw r2
        L_0x017b:
            r9 = move-exception
            r2 = r11
            goto L_0x018b
        L_0x017e:
            kotlin.TypeCastException r9 = new kotlin.TypeCastException     // Catch:{ all -> 0x018a }
            java.lang.String r10 = "null cannot be cast to non-null type javax.net.ssl.SSLSocket"
            r9.<init>(r10)     // Catch:{ all -> 0x018a }
            throw r9     // Catch:{ all -> 0x018a }
        L_0x0186:
            n.n.c.Intrinsics.a()     // Catch:{ all -> 0x018a }
            throw r2
        L_0x018a:
            r9 = move-exception
        L_0x018b:
            if (r2 == 0) goto L_0x0194
            o.m0.i.Platform$a r10 = o.m0.i.Platform.c
            o.m0.i.Platform r10 = o.m0.i.Platform.a
            r10.a(r2)
        L_0x0194:
            if (r2 == 0) goto L_0x0199
            o.m0.Util.a(r2)
        L_0x0199:
            throw r9
        L_0x019a:
            java.lang.String r9 = "call"
            n.n.c.Intrinsics.a(r9)
            throw r2
        L_0x01a0:
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.d.RealConnection.a(o.m0.d.ConnectionSpecSelector, int, o.Call, o.EventListener):void");
    }

    public final void a(int i2) {
        Socket socket = this.c;
        if (socket != null) {
            BufferedSource bufferedSource = this.g;
            if (bufferedSource != null) {
                BufferedSink bufferedSink = this.h;
                if (bufferedSink != null) {
                    socket.setSoTimeout(0);
                    Http2Connection.b bVar = new Http2Connection.b(true);
                    String str = this.f2959q.a.a.f2851e;
                    if (str == null) {
                        Intrinsics.a("connectionName");
                        throw null;
                    } else if (bufferedSource == null) {
                        Intrinsics.a("source");
                        throw null;
                    } else if (bufferedSink != null) {
                        bVar.a = socket;
                        bVar.b = str;
                        bVar.c = bufferedSource;
                        bVar.d = bufferedSink;
                        bVar.f3012e = super;
                        bVar.g = i2;
                        Http2Connection http2Connection = new Http2Connection(bVar);
                        this.f2950f = http2Connection;
                        http2Connection.f3011t.a();
                        http2Connection.f3011t.b(http2Connection.f3004m);
                        int a = http2Connection.f3004m.a();
                        if (a != 65535) {
                            http2Connection.f3011t.a(0, (long) (a - 65535));
                        }
                        Http2Connection.d dVar = http2Connection.u;
                        StringBuilder a2 = outline.a("OkHttp ");
                        a2.append(http2Connection.f2998e);
                        new Thread(dVar, a2.toString()).start();
                    } else {
                        Intrinsics.a("sink");
                        throw null;
                    }
                } else {
                    Intrinsics.a();
                    throw null;
                }
            } else {
                Intrinsics.a();
                throw null;
            }
        } else {
            Intrinsics.a();
            throw null;
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r5v5, types: [java.lang.Throwable, o.m0.d.RealConnection, o.OkHttpClient] */
    /* JADX WARN: Type inference failed for: r5v16 */
    /* JADX WARN: Type inference failed for: r5v17 */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: o.m0.Util.a(o.HttpUrl, boolean):java.lang.String
     arg types: [o.HttpUrl, int]
     candidates:
      o.m0.Util.a(byte, int):int
      o.m0.Util.a(java.lang.String, int):int
      o.m0.Util.a(java.lang.String, long):long
      o.m0.Util.a(java.lang.String, java.lang.Object[]):java.lang.String
      o.m0.Util.a(p.BufferedSource, java.nio.charset.Charset):java.nio.charset.Charset
      o.m0.Util.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
      o.m0.Util.a(java.lang.Object, long):void
      o.m0.Util.a(p.BufferedSink, int):void
      o.m0.Util.a(o.HttpUrl, o.HttpUrl):boolean
      o.m0.Util.a(o.HttpUrl, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      n.r.Indent.a(java.lang.String, java.lang.String, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean */
    public final void a(int i2, int i3, int i4, Call call, EventListener eventListener) {
        int i5 = i3;
        Call call2 = call;
        EventListener eventListener2 = eventListener;
        Request.a aVar = new Request.a();
        aVar.a(this.f2959q.a.a);
        boolean z = false;
        aVar.a("CONNECT", (RequestBody) null);
        boolean z2 = true;
        aVar.a("Host", Util.a(this.f2959q.a.a, true));
        aVar.a("Proxy-Connection", "Keep-Alive");
        aVar.a(AbstractSpiCall.HEADER_USER_AGENT, "okhttp/4.2.1");
        Request a = aVar.a();
        Response.a aVar2 = new Response.a();
        aVar2.a = a;
        aVar2.a(Protocol.HTTP_1_1);
        aVar2.c = 407;
        aVar2.d = "Preemptive Authenticate";
        aVar2.g = Util.c;
        aVar2.f2913k = -1;
        aVar2.f2914l = -1;
        aVar2.f2910f.c("Proxy-Authenticate", "OkHttp-Preemptive");
        Response a2 = aVar2.a();
        Route route = this.f2959q;
        Request a3 = route.a.f2798i.a(route, a2);
        if (a3 != null) {
            a = a3;
        }
        HttpUrl httpUrl = a.b;
        int i6 = 21;
        int i7 = 0;
        while (i7 < i6) {
            a(i2, i5, call2, eventListener2);
            String str = "CONNECT " + Util.a(httpUrl, z2) + " HTTP/1.1";
            ? r5 = z;
            while (true) {
                BufferedSource bufferedSource = this.g;
                if (bufferedSource != null) {
                    BufferedSink bufferedSink = this.h;
                    if (bufferedSink != null) {
                        Http1ExchangeCodec http1ExchangeCodec = new Http1ExchangeCodec(r5, r5, bufferedSource, bufferedSink);
                        bufferedSource.b().a((long) i5, TimeUnit.MILLISECONDS);
                        bufferedSink.b().a((long) i4, TimeUnit.MILLISECONDS);
                        http1ExchangeCodec.a(a.d, str);
                        http1ExchangeCodec.g.flush();
                        Response.a a4 = http1ExchangeCodec.a(false);
                        if (a4 != null) {
                            a4.a = a;
                            Response a5 = a4.a();
                            long a6 = Util.a(a5);
                            if (a6 != -1) {
                                Source a7 = http1ExchangeCodec.a(a6);
                                Util.b(a7, Integer.MAX_VALUE, TimeUnit.MILLISECONDS);
                                a7.close();
                            }
                            int i8 = a5.f2901f;
                            if (i8 != 200) {
                                if (i8 == 407) {
                                    Route route2 = this.f2959q;
                                    a = route2.a.f2798i.a(route2, a5);
                                    if (a == null) {
                                        throw new IOException("Failed to authenticate with proxy");
                                    } else if (Indent.a("close", Response.a(a5, "Connection", null, 2), true)) {
                                        break;
                                    } else {
                                        r5 = 0;
                                        i5 = i3;
                                    }
                                } else {
                                    StringBuilder a8 = outline.a("Unexpected response code for CONNECT: ");
                                    a8.append(a5.f2901f);
                                    throw new IOException(a8.toString());
                                }
                            } else if (!bufferedSource.getBuffer().j() || !bufferedSink.getBuffer().j()) {
                                throw new IOException("TLS tunnel buffered too many bytes!");
                            } else {
                                a = null;
                            }
                        } else {
                            Intrinsics.a();
                            throw null;
                        }
                    } else {
                        Intrinsics.a();
                        throw r5;
                    }
                } else {
                    Intrinsics.a();
                    throw r5;
                }
            }
            if (a != null) {
                Socket socket = this.b;
                if (socket != null) {
                    Util.a(socket);
                }
                this.b = null;
                this.h = null;
                this.g = null;
                Route route3 = this.f2959q;
                eventListener2.a(call2, route3.c, route3.b);
                i7++;
                z = false;
                z2 = true;
                i5 = i3;
                i6 = 21;
            } else {
                return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
    public final boolean a(HttpUrl httpUrl) {
        if (httpUrl != null) {
            HttpUrl httpUrl2 = this.f2959q.a.a;
            if (httpUrl.f2852f != httpUrl2.f2852f) {
                return false;
            }
            if (Intrinsics.a((Object) httpUrl.f2851e, (Object) httpUrl2.f2851e)) {
                return true;
            }
            Handshake handshake = this.d;
            if (handshake == null) {
                return false;
            }
            OkHostnameVerifier okHostnameVerifier = OkHostnameVerifier.a;
            String str = httpUrl.f2851e;
            if (handshake != null) {
                Certificate certificate = handshake.a().get(0);
                if (certificate == null) {
                    throw new TypeCastException("null cannot be cast to non-null type java.security.cert.X509Certificate");
                } else if (okHostnameVerifier.a(str, (X509Certificate) certificate)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                Intrinsics.a();
                throw null;
            }
        } else {
            Intrinsics.a("url");
            throw null;
        }
    }

    public final ExchangeCode a(OkHttpClient okHttpClient, Interceptor.a aVar) {
        if (okHttpClient == null) {
            Intrinsics.a("client");
            throw null;
        } else if (aVar != null) {
            Socket socket = this.c;
            if (socket != null) {
                BufferedSource bufferedSource = this.g;
                if (bufferedSource != null) {
                    BufferedSink bufferedSink = this.h;
                    if (bufferedSink != null) {
                        Http2Connection http2Connection = this.f2950f;
                        if (http2Connection != null) {
                            return new Http2ExchangeCodec(okHttpClient, this, aVar, http2Connection);
                        }
                        socket.setSoTimeout(aVar.d());
                        bufferedSource.b().a((long) aVar.d(), TimeUnit.MILLISECONDS);
                        bufferedSink.b().a((long) aVar.b(), TimeUnit.MILLISECONDS);
                        return new Http1ExchangeCodec(okHttpClient, this, bufferedSource, bufferedSink);
                    }
                    Intrinsics.a();
                    throw null;
                }
                Intrinsics.a();
                throw null;
            }
            Intrinsics.a();
            throw null;
        } else {
            Intrinsics.a("chain");
            throw null;
        }
    }

    public void a(Http2Stream http2Stream) {
        if (http2Stream != null) {
            http2Stream.a(ErrorCode.REFUSED_STREAM, (IOException) null);
        } else {
            Intrinsics.a("stream");
            throw null;
        }
    }

    public void a(Http2Connection http2Connection) {
        if (http2Connection != null) {
            synchronized (this.f2958p) {
                this.f2955m = http2Connection.f();
            }
            return;
        }
        Intrinsics.a("connection");
        throw null;
    }

    public final void a(IOException iOException) {
        boolean z = !Thread.holdsLock(this.f2958p);
        if (!AssertionsJVM.a || z) {
            synchronized (this.f2958p) {
                if (iOException instanceof StreamResetException) {
                    int ordinal = ((StreamResetException) iOException).b.ordinal();
                    if (ordinal == 4) {
                        int i2 = this.f2954l + 1;
                        this.f2954l = i2;
                        if (i2 > 1) {
                            this.f2951i = true;
                            this.f2952j++;
                        }
                    } else if (ordinal != 5) {
                        this.f2951i = true;
                        this.f2952j++;
                    }
                } else if (!b() || (iOException instanceof ConnectionShutdownException)) {
                    this.f2951i = true;
                    if (this.f2953k == 0) {
                        if (iOException != null) {
                            this.f2958p.a(this.f2959q, iOException);
                        }
                        this.f2952j++;
                    }
                }
            }
            return;
        }
        throw new AssertionError("Assertion failed");
    }

    public Protocol a() {
        Protocol protocol = this.f2949e;
        if (protocol != null) {
            return protocol;
        }
        Intrinsics.a();
        throw null;
    }
}
