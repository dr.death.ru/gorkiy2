package o.m0.i.g;

import java.util.List;
import javax.net.ssl.SSLSocket;
import n.n.c.Intrinsics;
import n.r.Indent;
import o.c0;
import o.m0.i.Platform;

/* compiled from: DeferredSocketAdapter.kt */
public final class DeferredSocketAdapter implements SocketAdapter {
    public boolean a;
    public SocketAdapter b;
    public final String c;

    public DeferredSocketAdapter(String str) {
        if (str != null) {
            this.c = str;
        } else {
            Intrinsics.a("socketPackage");
            throw null;
        }
    }

    public void a(SSLSocket sSLSocket, String str, List<? extends c0> list) {
        if (sSLSocket == null) {
            Intrinsics.a("sslSocket");
            throw null;
        } else if (list != null) {
            SocketAdapter c2 = c(sSLSocket);
            if (c2 != null) {
                c2.a(sSLSocket, str, list);
            }
        } else {
            Intrinsics.a("protocols");
            throw null;
        }
    }

    public boolean a() {
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public boolean b(SSLSocket sSLSocket) {
        if (sSLSocket != null) {
            String name = sSLSocket.getClass().getName();
            Intrinsics.a((Object) name, "sslSocket.javaClass.name");
            return Indent.b(name, this.c, false, 2);
        }
        Intrinsics.a("sslSocket");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.Class<?>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final synchronized SocketAdapter c(SSLSocket sSLSocket) {
        if (!this.a) {
            try {
                Class<?> cls = sSLSocket.getClass();
                while (true) {
                    String name = cls.getName();
                    if (!(!Intrinsics.a((Object) name, (Object) (this.c + ".OpenSSLSocketImpl")))) {
                        break;
                    }
                    cls = cls.getSuperclass();
                    Intrinsics.a((Object) cls, "possibleClass.superclass");
                }
                this.b = new AndroidSocketAdapter(cls);
            } catch (Exception e2) {
                Platform.a aVar = Platform.c;
                Platform platform = Platform.a;
                platform.a(5, "Failed to initialize DeferredSocketAdapter " + this.c, e2);
            }
            this.a = true;
        }
        return this.b;
    }

    public String a(SSLSocket sSLSocket) {
        if (sSLSocket != null) {
            SocketAdapter c2 = c(sSLSocket);
            if (c2 != null) {
                return c2.a(sSLSocket);
            }
            return null;
        }
        Intrinsics.a("sslSocket");
        throw null;
    }
}
