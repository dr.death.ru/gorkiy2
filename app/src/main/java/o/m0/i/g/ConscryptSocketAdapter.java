package o.m0.i.g;

import java.util.List;
import javax.net.ssl.SSLSocket;
import kotlin.TypeCastException;
import n.n.c.Intrinsics;
import o.c0;
import o.m0.i.ConscryptPlatform;
import o.m0.i.Platform;
import org.conscrypt.Conscrypt;

/* compiled from: ConscryptSocketAdapter.kt */
public final class ConscryptSocketAdapter implements SocketAdapter {
    public static final ConscryptSocketAdapter a = new ConscryptSocketAdapter();

    public boolean a() {
        ConscryptPlatform.a aVar = ConscryptPlatform.f3047f;
        return ConscryptPlatform.f3046e;
    }

    public boolean b(SSLSocket sSLSocket) {
        if (sSLSocket != null) {
            return Conscrypt.isConscrypt(sSLSocket);
        }
        Intrinsics.a("sslSocket");
        throw null;
    }

    public String a(SSLSocket sSLSocket) {
        if (sSLSocket == null) {
            Intrinsics.a("sslSocket");
            throw null;
        } else if (b(sSLSocket)) {
            return Conscrypt.getApplicationProtocol(sSLSocket);
        } else {
            return null;
        }
    }

    public void a(SSLSocket sSLSocket, String str, List<? extends c0> list) {
        if (sSLSocket == null) {
            Intrinsics.a("sslSocket");
            throw null;
        } else if (list == null) {
            Intrinsics.a("protocols");
            throw null;
        } else if (b(sSLSocket)) {
            if (str != null) {
                Conscrypt.setUseSessionTickets(sSLSocket, true);
                Conscrypt.setHostname(sSLSocket, str);
            }
            Object[] array = Platform.c.a(list).toArray(new String[0]);
            if (array != null) {
                Conscrypt.setApplicationProtocols(sSLSocket, (String[]) array);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }
}
