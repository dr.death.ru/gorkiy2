package o.m0.i.g;

import java.util.List;
import javax.net.ssl.SSLSocket;
import o.c0;

/* compiled from: SocketAdapter.kt */
public interface SocketAdapter {
    String a(SSLSocket sSLSocket);

    void a(SSLSocket sSLSocket, String str, List<? extends c0> list);

    boolean a();

    boolean b(SSLSocket sSLSocket);
}
