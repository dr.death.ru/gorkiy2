package o.m0.e;

import n.n.c.Intrinsics;

/* compiled from: HttpMethod.kt */
public final class HttpMethod {
    public static final boolean a(String str) {
        if (str != null) {
            return !Intrinsics.a(str, "GET") && !Intrinsics.a(str, "HEAD");
        }
        Intrinsics.a("method");
        throw null;
    }

    public static final boolean b(String str) {
        if (str != null) {
            return Intrinsics.a(str, "POST") || Intrinsics.a(str, "PUT") || Intrinsics.a(str, "PATCH") || Intrinsics.a(str, "PROPPATCH") || Intrinsics.a(str, "REPORT");
        }
        Intrinsics.a("method");
        throw null;
    }
}
