package o.m0.e;

import java.util.List;
import l.a.a.a.o.b.AbstractSpiCall;
import n.i.Collections;
import n.n.c.Intrinsics;
import n.r.Indent;
import o.Cookie;
import o.CookieJar;
import o.Headers;
import o.Interceptor;
import o.MediaType;
import o.Request;
import o.RequestBody;
import o.Response;
import o.ResponseBody;
import o.m0.Util;
import o.n;
import p.GzipSource;
import p.Source;

/* compiled from: BridgeInterceptor.kt */
public final class BridgeInterceptor implements Interceptor {
    public final CookieJar b;

    public BridgeInterceptor(CookieJar cookieJar) {
        if (cookieJar != null) {
            this.b = cookieJar;
        } else {
            Intrinsics.a("cookieJar");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: o.m0.Util.a(o.HttpUrl, boolean):java.lang.String
     arg types: [o.HttpUrl, int]
     candidates:
      o.m0.Util.a(byte, int):int
      o.m0.Util.a(java.lang.String, int):int
      o.m0.Util.a(java.lang.String, long):long
      o.m0.Util.a(java.lang.String, java.lang.Object[]):java.lang.String
      o.m0.Util.a(p.BufferedSource, java.nio.charset.Charset):java.nio.charset.Charset
      o.m0.Util.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
      o.m0.Util.a(java.lang.Object, long):void
      o.m0.Util.a(p.BufferedSink, int):void
      o.m0.Util.a(o.HttpUrl, o.HttpUrl):boolean
      o.m0.Util.a(o.HttpUrl, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      n.r.Indent.a(java.lang.String, java.lang.String, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean */
    public Response a(Interceptor.a aVar) {
        boolean z;
        ResponseBody responseBody;
        Interceptor.a aVar2 = aVar;
        if (aVar2 != null) {
            Request f2 = aVar.f();
            if (f2 != null) {
                Request.a aVar3 = new Request.a(f2);
                RequestBody requestBody = f2.f2896e;
                if (requestBody != null) {
                    MediaType b2 = requestBody.b();
                    if (b2 != null) {
                        aVar3.a("Content-Type", b2.a);
                    }
                    long a = requestBody.a();
                    if (a != -1) {
                        aVar3.a("Content-Length", String.valueOf(a));
                        aVar3.a("Transfer-Encoding");
                    } else {
                        aVar3.a("Transfer-Encoding", "chunked");
                        aVar3.a("Content-Length");
                    }
                }
                int i2 = 0;
                if (f2.a("Host") == null) {
                    aVar3.a("Host", Util.a(f2.b, false));
                }
                if (f2.a("Connection") == null) {
                    aVar3.a("Connection", "Keep-Alive");
                }
                if (f2.a("Accept-Encoding") == null && f2.a("Range") == null) {
                    aVar3.a("Accept-Encoding", "gzip");
                    z = true;
                } else {
                    z = false;
                }
                List<n> a2 = this.b.a(f2.b);
                if (!a2.isEmpty()) {
                    StringBuilder sb = new StringBuilder();
                    for (T next : a2) {
                        int i3 = i2 + 1;
                        if (i2 >= 0) {
                            Cookie cookie = (Cookie) next;
                            if (i2 > 0) {
                                sb.append("; ");
                            }
                            sb.append(cookie.a);
                            sb.append('=');
                            sb.append(cookie.b);
                            i2 = i3;
                        } else {
                            Collections.a();
                            throw null;
                        }
                    }
                    String sb2 = sb.toString();
                    Intrinsics.a((Object) sb2, "StringBuilder().apply(builderAction).toString()");
                    aVar3.a("Cookie", sb2);
                }
                if (f2.a(AbstractSpiCall.HEADER_USER_AGENT) == null) {
                    aVar3.a(AbstractSpiCall.HEADER_USER_AGENT, "okhttp/4.2.1");
                }
                Response a3 = aVar2.a(aVar3.a());
                HttpHeaders.a(this.b, f2.b, a3.h);
                Response.a aVar4 = new Response.a(a3);
                aVar4.a = f2;
                if (z && Indent.a("gzip", Response.a(a3, "Content-Encoding", null, 2), true) && HttpHeaders.a(a3) && (responseBody = a3.f2902i) != null) {
                    GzipSource gzipSource = new GzipSource(responseBody.g());
                    Headers.a c = a3.h.c();
                    c.c("Content-Encoding");
                    c.c("Content-Length");
                    aVar4.a(c.a());
                    aVar4.g = new RealResponseBody(Response.a(a3, "Content-Type", null, 2), -1, Collections.a((Source) gzipSource));
                }
                return aVar4.a();
            }
            throw null;
        }
        Intrinsics.a("chain");
        throw null;
    }
}
