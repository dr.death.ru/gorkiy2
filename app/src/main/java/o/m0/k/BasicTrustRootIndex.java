package o.m0.k;

import java.security.cert.X509Certificate;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import javax.security.auth.x500.X500Principal;
import n.n.c.Intrinsics;

/* compiled from: BasicTrustRootIndex.kt */
public final class BasicTrustRootIndex implements TrustRootIndex {
    public final Map<X500Principal, Set<X509Certificate>> a;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [javax.security.auth.x500.X500Principal, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public BasicTrustRootIndex(X509Certificate... x509CertificateArr) {
        if (x509CertificateArr != null) {
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (X509Certificate x509Certificate : x509CertificateArr) {
                X500Principal subjectX500Principal = x509Certificate.getSubjectX500Principal();
                Intrinsics.a((Object) subjectX500Principal, "caCert.subjectX500Principal");
                Object obj = linkedHashMap.get(subjectX500Principal);
                if (obj == null) {
                    obj = new LinkedHashSet();
                    linkedHashMap.put(subjectX500Principal, obj);
                }
                ((Set) obj).add(x509Certificate);
            }
            this.a = linkedHashMap;
            return;
        }
        Intrinsics.a("caCerts");
        throw null;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: java.security.cert.X509Certificate} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.security.cert.X509Certificate} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v0, resolved type: java.security.cert.X509Certificate} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v3, resolved type: java.security.cert.X509Certificate} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.security.cert.X509Certificate a(java.security.cert.X509Certificate r5) {
        /*
            r4 = this;
            r0 = 0
            if (r5 == 0) goto L_0x0032
            javax.security.auth.x500.X500Principal r1 = r5.getIssuerX500Principal()
            java.util.Map<javax.security.auth.x500.X500Principal, java.util.Set<java.security.cert.X509Certificate>> r2 = r4.a
            java.lang.Object r1 = r2.get(r1)
            java.util.Set r1 = (java.util.Set) r1
            if (r1 == 0) goto L_0x0031
            java.util.Iterator r1 = r1.iterator()
        L_0x0015:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x002f
            java.lang.Object r2 = r1.next()
            r3 = r2
            java.security.cert.X509Certificate r3 = (java.security.cert.X509Certificate) r3
            java.security.PublicKey r3 = r3.getPublicKey()     // Catch:{ Exception -> 0x002b }
            r5.verify(r3)     // Catch:{ Exception -> 0x002b }
            r3 = 1
            goto L_0x002c
        L_0x002b:
            r3 = 0
        L_0x002c:
            if (r3 == 0) goto L_0x0015
            r0 = r2
        L_0x002f:
            java.security.cert.X509Certificate r0 = (java.security.cert.X509Certificate) r0
        L_0x0031:
            return r0
        L_0x0032:
            java.lang.String r5 = "cert"
            n.n.c.Intrinsics.a(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.k.BasicTrustRootIndex.a(java.security.cert.X509Certificate):java.security.cert.X509Certificate");
    }

    public boolean equals(Object obj) {
        return obj == this || ((obj instanceof BasicTrustRootIndex) && Intrinsics.a(((BasicTrustRootIndex) obj).a, this.a));
    }

    public int hashCode() {
        return this.a.hashCode();
    }
}
