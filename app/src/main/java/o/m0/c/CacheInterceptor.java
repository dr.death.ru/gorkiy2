package o.m0.c;

import n.n.c.DefaultConstructorMarker;
import n.r.Indent;
import o.Cache;
import o.Interceptor;
import o.Response;

/* compiled from: CacheInterceptor.kt */
public final class CacheInterceptor implements Interceptor {
    public static final a c = new a(null);
    public final Cache b;

    public CacheInterceptor(Cache cache) {
        this.b = cache;
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARN: Type inference failed for: r2v2, types: [o.Response, o.Request] */
    /* JADX WARN: Type inference failed for: r2v37 */
    /* JADX WARN: Type inference failed for: r2v39 */
    /* JADX WARN: Type inference failed for: r2v45 */
    /* JADX WARN: Type inference failed for: r2v47 */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      n.r.Indent.a(java.lang.String, java.lang.String, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x028c, code lost:
        if (r4 > 0) goto L_0x02c6;
     */
    /* JADX WARNING: Removed duplicated region for block: B:193:0x03ef  */
    /* JADX WARNING: Removed duplicated region for block: B:200:0x03ff A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:204:0x042f  */
    /* JADX WARNING: Removed duplicated region for block: B:209:0x0449  */
    /* JADX WARNING: Removed duplicated region for block: B:322:0x0608  */
    /* JADX WARNING: Removed duplicated region for block: B:360:0x0690  */
    /* JADX WARNING: Removed duplicated region for block: B:378:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x011f  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x012a  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x014b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public o.Response a(o.Interceptor.a r28) {
        /*
            r27 = this;
            r1 = r27
            r0 = r28
            r2 = 0
            if (r0 == 0) goto L_0x0696
            o.Cache r3 = r1.b
            java.lang.String r4 = "Content-Type"
            java.lang.String r5 = "request"
            r6 = 0
            r7 = 1
            if (r3 == 0) goto L_0x0140
            o.Request r8 = r28.f()
            if (r8 == 0) goto L_0x013c
            o.HttpUrl r9 = r8.b
            java.lang.String r9 = o.Cache.a(r9)
            o.m0.c.DiskLruCache r3 = r3.b     // Catch:{ IOException -> 0x0140 }
            o.m0.c.DiskLruCache$c r3 = r3.b(r9)     // Catch:{ IOException -> 0x0140 }
            if (r3 == 0) goto L_0x0140
            o.Cache$b r9 = new o.Cache$b     // Catch:{ IOException -> 0x0138 }
            java.util.List<p.z> r10 = r3.d     // Catch:{ IOException -> 0x0138 }
            java.lang.Object r6 = r10.get(r6)     // Catch:{ IOException -> 0x0138 }
            p.Source r6 = (p.Source) r6     // Catch:{ IOException -> 0x0138 }
            r9.<init>(r6)     // Catch:{ IOException -> 0x0138 }
            o.Headers r6 = r9.g
            java.lang.String r6 = r6.a(r4)
            o.Headers r10 = r9.g
            java.lang.String r11 = "Content-Length"
            java.lang.String r10 = r10.a(r11)
            o.Request$a r11 = new o.Request$a
            r11.<init>()
            java.lang.String r12 = r9.a
            if (r12 == 0) goto L_0x0132
            java.lang.String r13 = "ws:"
            boolean r13 = n.r.Indent.b(r12, r13, r7)
            java.lang.String r14 = "(this as java.lang.String).substring(startIndex)"
            if (r13 == 0) goto L_0x0069
            java.lang.String r13 = "http:"
            java.lang.StringBuilder r13 = j.a.a.a.outline.a(r13)
            r15 = 3
            java.lang.String r12 = r12.substring(r15)
            n.n.c.Intrinsics.a(r12, r14)
            r13.append(r12)
            java.lang.String r12 = r13.toString()
            goto L_0x0086
        L_0x0069:
            java.lang.String r13 = "wss:"
            boolean r13 = n.r.Indent.b(r12, r13, r7)
            if (r13 == 0) goto L_0x0086
            java.lang.String r13 = "https:"
            java.lang.StringBuilder r13 = j.a.a.a.outline.a(r13)
            r15 = 4
            java.lang.String r12 = r12.substring(r15)
            n.n.c.Intrinsics.a(r12, r14)
            r13.append(r12)
            java.lang.String r12 = r13.toString()
        L_0x0086:
            o.HttpUrl$b r13 = o.HttpUrl.f2850l
            o.HttpUrl r12 = r13.b(r12)
            r11.a(r12)
            java.lang.String r12 = r9.c
            r11.a(r12, r2)
            o.Headers r12 = r9.b
            r11.a(r12)
            o.Request r11 = r11.a()
            o.Response$a r12 = new o.Response$a
            r12.<init>()
            r12.a = r11
            o.Protocol r11 = r9.d
            r12.a(r11)
            int r11 = r9.f2807e
            r12.c = r11
            java.lang.String r11 = r9.f2808f
            r12.a(r11)
            o.Headers r11 = r9.g
            r12.a(r11)
            o.Cache$a r11 = new o.Cache$a
            r11.<init>(r3, r6, r10)
            r12.g = r11
            o.Handshake r3 = r9.h
            r12.f2909e = r3
            long r10 = r9.f2809i
            r12.f2913k = r10
            long r10 = r9.f2810j
            r12.f2914l = r10
            o.Response r3 = r12.a()
            java.lang.String r6 = r9.a
            o.HttpUrl r10 = r8.b
            java.lang.String r10 = r10.f2854j
            boolean r6 = n.n.c.Intrinsics.a(r6, r10)
            if (r6 == 0) goto L_0x0127
            java.lang.String r6 = r9.c
            java.lang.String r10 = r8.c
            boolean r6 = n.n.c.Intrinsics.a(r6, r10)
            if (r6 == 0) goto L_0x0127
            o.Headers r6 = r9.b
            if (r6 == 0) goto L_0x0121
            o.Headers r9 = r3.h
            java.util.Set r9 = o.Cache.a(r9)
            boolean r10 = r9 instanceof java.util.Collection
            if (r10 == 0) goto L_0x00f9
            boolean r10 = r9.isEmpty()
            if (r10 == 0) goto L_0x00f9
            goto L_0x011c
        L_0x00f9:
            java.util.Iterator r9 = r9.iterator()
        L_0x00fd:
            boolean r10 = r9.hasNext()
            if (r10 == 0) goto L_0x011c
            java.lang.Object r10 = r9.next()
            java.lang.String r10 = (java.lang.String) r10
            java.util.List r11 = r6.b(r10)
            o.Headers r12 = r8.d
            java.util.List r10 = r12.b(r10)
            boolean r10 = n.n.c.Intrinsics.a(r11, r10)
            r10 = r10 ^ r7
            if (r10 == 0) goto L_0x00fd
            r6 = 0
            goto L_0x011d
        L_0x011c:
            r6 = 1
        L_0x011d:
            if (r6 == 0) goto L_0x0127
            r6 = 1
            goto L_0x0128
        L_0x0121:
            java.lang.String r0 = "cachedRequest"
            n.n.c.Intrinsics.a(r0)
            throw r2
        L_0x0127:
            r6 = 0
        L_0x0128:
            if (r6 != 0) goto L_0x0141
            o.ResponseBody r3 = r3.f2902i
            if (r3 == 0) goto L_0x0140
            o.m0.Util.a(r3)
            goto L_0x0140
        L_0x0132:
            java.lang.String r0 = "url"
            n.n.c.Intrinsics.a(r0)
            throw r2
        L_0x0138:
            o.m0.Util.a(r3)
            goto L_0x0140
        L_0x013c:
            n.n.c.Intrinsics.a(r5)
            throw r2
        L_0x0140:
            r3 = r2
        L_0x0141:
            long r8 = java.lang.System.currentTimeMillis()
            o.Request r6 = r28.f()
            if (r6 == 0) goto L_0x0690
            if (r3 == 0) goto L_0x01d0
            long r10 = r3.f2906m
            long r12 = r3.f2907n
            o.Headers r5 = r3.h
            int r14 = r5.size()
            r15 = 0
            r16 = -1
            r15 = r2
            r16 = r15
            r17 = r16
            r18 = r17
            r19 = r18
            r7 = 0
            r21 = -1
        L_0x0166:
            if (r7 >= r14) goto L_0x01c9
            r22 = r2
            java.lang.String r2 = r5.c(r7)
            r23 = r10
            java.lang.String r10 = r5.d(r7)
            java.lang.String r11 = "Date"
            r25 = r5
            r5 = 1
            boolean r11 = n.r.Indent.a(r2, r11, r5)
            if (r11 == 0) goto L_0x0186
            java.util.Date r2 = o.m0.e.dates.a(r10)
            r19 = r10
            goto L_0x01c2
        L_0x0186:
            java.lang.String r11 = "Expires"
            boolean r11 = n.r.Indent.a(r2, r11, r5)
            if (r11 == 0) goto L_0x0196
            java.util.Date r2 = o.m0.e.dates.a(r10)
            r15 = r2
        L_0x0193:
            r2 = r22
            goto L_0x01c2
        L_0x0196:
            java.lang.String r11 = "Last-Modified"
            boolean r11 = n.r.Indent.a(r2, r11, r5)
            if (r11 == 0) goto L_0x01a7
            java.util.Date r2 = o.m0.e.dates.a(r10)
            r16 = r2
            r18 = r10
            goto L_0x0193
        L_0x01a7:
            java.lang.String r11 = "ETag"
            boolean r11 = n.r.Indent.a(r2, r11, r5)
            if (r11 == 0) goto L_0x01b2
            r17 = r10
            goto L_0x0193
        L_0x01b2:
            java.lang.String r11 = "Age"
            boolean r2 = n.r.Indent.a(r2, r11, r5)
            if (r2 == 0) goto L_0x0193
            r2 = -1
            int r2 = o.m0.Util.b(r10, r2)
            r21 = r2
            goto L_0x0193
        L_0x01c2:
            int r7 = r7 + 1
            r10 = r23
            r5 = r25
            goto L_0x0166
        L_0x01c9:
            r22 = r2
            r23 = r10
            r5 = r21
            goto L_0x01e1
        L_0x01d0:
            r21 = -1
            r10 = 0
            r12 = 0
            r2 = 0
            r15 = 0
            r16 = 0
            r17 = 0
            r18 = 0
            r19 = 0
            r5 = -1
        L_0x01e1:
            java.lang.String r7 = "Warning"
            if (r3 != 0) goto L_0x01ec
            o.m0.c.CacheStrategy r2 = new o.m0.c.CacheStrategy
            r5 = 0
            r2.<init>(r6, r5)
            goto L_0x0209
        L_0x01ec:
            o.HttpUrl r14 = r6.b
            boolean r14 = r14.a
            if (r14 == 0) goto L_0x01fd
            o.Handshake r14 = r3.g
            if (r14 != 0) goto L_0x01fd
            o.m0.c.CacheStrategy r2 = new o.m0.c.CacheStrategy
            r14 = 0
            r2.<init>(r6, r14)
            goto L_0x0209
        L_0x01fd:
            r14 = 0
            boolean r20 = o.m0.c.CacheStrategy.a(r3, r6)
            if (r20 != 0) goto L_0x020f
            o.m0.c.CacheStrategy r2 = new o.m0.c.CacheStrategy
            r2.<init>(r6, r14)
        L_0x0209:
            r20 = r4
            r1 = r6
            r5 = r7
            goto L_0x03a4
        L_0x020f:
            o.CacheControl r14 = r6.a()
            r20 = r4
            boolean r4 = r14.a
            if (r4 != 0) goto L_0x03cc
            java.lang.String r4 = "If-Modified-Since"
            java.lang.String r21 = r6.a(r4)
            r22 = r4
            java.lang.String r4 = "If-None-Match"
            if (r21 != 0) goto L_0x022f
            java.lang.String r21 = r6.a(r4)
            if (r21 == 0) goto L_0x022c
            goto L_0x022f
        L_0x022c:
            r21 = 0
            goto L_0x0231
        L_0x022f:
            r21 = 1
        L_0x0231:
            if (r21 == 0) goto L_0x0235
            goto L_0x03cc
        L_0x0235:
            r21 = r4
            o.CacheControl r4 = r3.a()
            if (r2 == 0) goto L_0x024e
            long r23 = r2.getTime()
            long r0 = r12 - r23
            r23 = r6
            r24 = r7
            r6 = 0
            long r0 = java.lang.Math.max(r6, r0)
            goto L_0x0254
        L_0x024e:
            r23 = r6
            r24 = r7
            r0 = 0
        L_0x0254:
            r6 = -1
            if (r5 == r6) goto L_0x0264
            java.util.concurrent.TimeUnit r6 = java.util.concurrent.TimeUnit.SECONDS
            r7 = r4
            long r4 = (long) r5
            long r4 = r6.toMillis(r4)
            long r0 = java.lang.Math.max(r0, r4)
            goto L_0x0265
        L_0x0264:
            r7 = r4
        L_0x0265:
            long r4 = r12 - r10
            long r8 = r8 - r12
            long r0 = r0 + r4
            long r0 = r0 + r8
            o.CacheControl r4 = r3.a()
            int r4 = r4.c
            r5 = -1
            if (r4 == r5) goto L_0x027b
            java.util.concurrent.TimeUnit r5 = java.util.concurrent.TimeUnit.SECONDS
            long r8 = (long) r4
            long r4 = r5.toMillis(r8)
            goto L_0x02c6
        L_0x027b:
            if (r15 == 0) goto L_0x028f
            if (r2 == 0) goto L_0x0283
            long r12 = r2.getTime()
        L_0x0283:
            long r4 = r15.getTime()
            long r4 = r4 - r12
            r8 = 0
            int r6 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r6 <= 0) goto L_0x02c4
            goto L_0x02c6
        L_0x028f:
            if (r16 == 0) goto L_0x02c4
            o.Request r4 = r3.c
            o.HttpUrl r4 = r4.b
            java.util.List<java.lang.String> r5 = r4.h
            if (r5 != 0) goto L_0x029b
            r4 = 0
            goto L_0x02ab
        L_0x029b:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            o.HttpUrl$b r6 = o.HttpUrl.f2850l
            java.util.List<java.lang.String> r4 = r4.h
            r6.a(r4, r5)
            java.lang.String r4 = r5.toString()
        L_0x02ab:
            if (r4 != 0) goto L_0x02c4
            if (r2 == 0) goto L_0x02b3
            long r10 = r2.getTime()
        L_0x02b3:
            long r4 = r16.getTime()
            long r10 = r10 - r4
            r4 = 0
            int r6 = (r10 > r4 ? 1 : (r10 == r4 ? 0 : -1))
            if (r6 <= 0) goto L_0x02c4
            r4 = 10
            long r4 = (long) r4
            long r4 = r10 / r4
            goto L_0x02c6
        L_0x02c4:
            r4 = 0
        L_0x02c6:
            int r6 = r14.c
            r8 = -1
            if (r6 == r8) goto L_0x02d6
            java.util.concurrent.TimeUnit r9 = java.util.concurrent.TimeUnit.SECONDS
            long r10 = (long) r6
            long r9 = r9.toMillis(r10)
            long r4 = java.lang.Math.min(r4, r9)
        L_0x02d6:
            int r6 = r14.f2815i
            if (r6 == r8) goto L_0x02e2
            java.util.concurrent.TimeUnit r9 = java.util.concurrent.TimeUnit.SECONDS
            long r10 = (long) r6
            long r9 = r9.toMillis(r10)
            goto L_0x02e4
        L_0x02e2:
            r9 = 0
        L_0x02e4:
            boolean r6 = r7.g
            if (r6 != 0) goto L_0x02f4
            int r6 = r14.h
            if (r6 == r8) goto L_0x02f4
            java.util.concurrent.TimeUnit r8 = java.util.concurrent.TimeUnit.SECONDS
            long r11 = (long) r6
            long r11 = r8.toMillis(r11)
            goto L_0x02f6
        L_0x02f4:
            r11 = 0
        L_0x02f6:
            boolean r6 = r7.a
            if (r6 != 0) goto L_0x033d
            long r9 = r9 + r0
            long r11 = r11 + r4
            int r6 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
            if (r6 >= 0) goto L_0x033d
            o.Response$a r2 = new o.Response$a
            r2.<init>(r3)
            int r6 = (r9 > r4 ? 1 : (r9 == r4 ? 0 : -1))
            if (r6 < 0) goto L_0x0311
            java.lang.String r4 = "110 HttpURLConnection \"Response is stale\""
            r5 = r24
            r2.a(r5, r4)
            goto L_0x0313
        L_0x0311:
            r5 = r24
        L_0x0313:
            r6 = 86400000(0x5265c00, double:4.2687272E-316)
            int r4 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r4 <= 0) goto L_0x032f
            o.CacheControl r0 = r3.a()
            int r0 = r0.c
            r1 = -1
            if (r0 != r1) goto L_0x0327
            if (r15 != 0) goto L_0x0327
            r0 = 1
            goto L_0x0328
        L_0x0327:
            r0 = 0
        L_0x0328:
            if (r0 == 0) goto L_0x032f
            java.lang.String r0 = "113 HttpURLConnection \"Heuristic expiration\""
            r2.a(r5, r0)
        L_0x032f:
            o.m0.c.CacheStrategy r0 = new o.m0.c.CacheStrategy
            o.Response r1 = r2.a()
            r2 = 0
            r0.<init>(r2, r1)
            r1 = r23
            goto L_0x03d4
        L_0x033d:
            r5 = r24
            if (r17 == 0) goto L_0x0346
            r0 = r17
            r4 = r21
            goto L_0x0352
        L_0x0346:
            if (r16 == 0) goto L_0x034d
            r0 = r18
        L_0x034a:
            r4 = r22
            goto L_0x0352
        L_0x034d:
            if (r2 == 0) goto L_0x03c3
            r0 = r19
            goto L_0x034a
        L_0x0352:
            r1 = r23
            o.Headers r2 = r1.d
            o.Headers$a r2 = r2.c()
            if (r0 == 0) goto L_0x03be
            r2.b(r4, r0)
            java.util.LinkedHashMap r0 = new java.util.LinkedHashMap
            r0.<init>()
            o.HttpUrl r7 = r1.b
            java.lang.String r8 = r1.c
            o.RequestBody r10 = r1.f2896e
            java.util.Map<java.lang.Class<?>, java.lang.Object> r0 = r1.f2897f
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0378
            java.util.LinkedHashMap r0 = new java.util.LinkedHashMap
            r0.<init>()
            goto L_0x0382
        L_0x0378:
            java.util.Map<java.lang.Class<?>, java.lang.Object> r0 = r1.f2897f
            if (r0 == 0) goto L_0x03b7
            java.util.LinkedHashMap r4 = new java.util.LinkedHashMap
            r4.<init>(r0)
            r0 = r4
        L_0x0382:
            o.Headers r4 = r1.d
            r4.c()
            o.Headers r2 = r2.a()
            o.Headers$a r2 = r2.c()
            if (r7 == 0) goto L_0x03ab
            o.Headers r9 = r2.a()
            java.util.Map r11 = o.m0.Util.a(r0)
            o.Request r0 = new o.Request
            r6 = r0
            r6.<init>(r7, r8, r9, r10, r11)
            o.m0.c.CacheStrategy r2 = new o.m0.c.CacheStrategy
            r2.<init>(r0, r3)
        L_0x03a4:
            r0 = 0
            r26 = r2
            r2 = r0
            r0 = r26
            goto L_0x03d4
        L_0x03ab:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "url == null"
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x03b7:
            java.lang.String r0 = "$this$toMutableMap"
            n.n.c.Intrinsics.a(r0)
            r0 = 0
            throw r0
        L_0x03be:
            r0 = 0
            n.n.c.Intrinsics.a()
            throw r0
        L_0x03c3:
            r1 = r23
            r2 = 0
            o.m0.c.CacheStrategy r0 = new o.m0.c.CacheStrategy
            r0.<init>(r1, r2)
            goto L_0x03d4
        L_0x03cc:
            r1 = r6
            r5 = r7
            r2 = 0
            o.m0.c.CacheStrategy r0 = new o.m0.c.CacheStrategy
            r0.<init>(r1, r2)
        L_0x03d4:
            o.Request r4 = r0.a
            if (r4 == 0) goto L_0x03e5
            o.CacheControl r1 = r1.a()
            boolean r1 = r1.f2816j
            if (r1 == 0) goto L_0x03e5
            o.m0.c.CacheStrategy r0 = new o.m0.c.CacheStrategy
            r0.<init>(r2, r2)
        L_0x03e5:
            o.Request r1 = r0.a
            o.Response r2 = r0.b
            r4 = r27
            o.Cache r6 = r4.b
            if (r6 == 0) goto L_0x03f2
            r6.a(r0)
        L_0x03f2:
            if (r3 == 0) goto L_0x03fd
            if (r2 != 0) goto L_0x03fd
            o.ResponseBody r0 = r3.f2902i
            if (r0 == 0) goto L_0x03fd
            o.m0.Util.a(r0)
        L_0x03fd:
            if (r1 != 0) goto L_0x042d
            if (r2 != 0) goto L_0x042d
            o.Response$a r0 = new o.Response$a
            r0.<init>()
            o.Request r1 = r28.f()
            r0.a(r1)
            o.Protocol r1 = o.Protocol.HTTP_1_1
            r0.a(r1)
            r1 = 504(0x1f8, float:7.06E-43)
            r0.c = r1
            java.lang.String r1 = "Unsatisfiable Request (only-if-cached)"
            r0.d = r1
            o.ResponseBody r1 = o.m0.Util.c
            r0.g = r1
            r1 = -1
            r0.f2913k = r1
            long r1 = java.lang.System.currentTimeMillis()
            r0.f2914l = r1
            o.Response r0 = r0.a()
            return r0
        L_0x042d:
            if (r1 != 0) goto L_0x0449
            if (r2 == 0) goto L_0x0444
            o.Response$a r0 = new o.Response$a
            r0.<init>(r2)
            o.m0.c.CacheInterceptor$a r1 = o.m0.c.CacheInterceptor.c
            o.Response r1 = o.m0.c.CacheInterceptor.a.a(r1, r2)
            r0.a(r1)
            o.Response r0 = r0.a()
            return r0
        L_0x0444:
            n.n.c.Intrinsics.a()
            r0 = 0
            throw r0
        L_0x0449:
            r0 = r28
            o.Response r0 = r0.a(r1)     // Catch:{ all -> 0x0684 }
            if (r0 != 0) goto L_0x045a
            if (r3 == 0) goto L_0x045a
            o.ResponseBody r3 = r3.f2902i
            if (r3 == 0) goto L_0x045a
            o.m0.Util.a(r3)
        L_0x045a:
            java.lang.String r3 = "networkResponse"
            r6 = 2
            if (r2 == 0) goto L_0x0555
            if (r0 == 0) goto L_0x054c
            int r7 = r0.f2901f
            r8 = 304(0x130, float:4.26E-43)
            if (r7 != r8) goto L_0x054c
            o.Response$a r1 = new o.Response$a
            r1.<init>(r2)
            o.m0.c.CacheInterceptor$a r7 = o.m0.c.CacheInterceptor.c
            o.Headers r8 = r2.h
            o.Headers r9 = r0.h
            o.Headers$a r10 = new o.Headers$a
            r10.<init>()
            int r11 = r8.size()
            r12 = 0
        L_0x047c:
            if (r12 >= r11) goto L_0x04b5
            java.lang.String r13 = r8.c(r12)
            java.lang.String r14 = r8.d(r12)
            r15 = 1
            boolean r15 = n.r.Indent.a(r5, r13, r15)
            if (r15 == 0) goto L_0x0499
            java.lang.String r15 = "1"
            r24 = r5
            r5 = 0
            boolean r5 = n.r.Indent.b(r14, r15, r5, r6)
            if (r5 == 0) goto L_0x049b
            goto L_0x04b0
        L_0x0499:
            r24 = r5
        L_0x049b:
            boolean r5 = r7.a(r13)
            if (r5 != 0) goto L_0x04ad
            boolean r5 = r7.b(r13)
            if (r5 == 0) goto L_0x04ad
            java.lang.String r5 = r9.a(r13)
            if (r5 != 0) goto L_0x04b0
        L_0x04ad:
            r10.b(r13, r14)
        L_0x04b0:
            int r12 = r12 + 1
            r5 = r24
            goto L_0x047c
        L_0x04b5:
            r5 = 0
            int r6 = r9.size()
        L_0x04ba:
            if (r5 >= r6) goto L_0x04d6
            java.lang.String r8 = r9.c(r5)
            boolean r11 = r7.a(r8)
            if (r11 != 0) goto L_0x04d3
            boolean r11 = r7.b(r8)
            if (r11 == 0) goto L_0x04d3
            java.lang.String r11 = r9.d(r5)
            r10.b(r8, r11)
        L_0x04d3:
            int r5 = r5 + 1
            goto L_0x04ba
        L_0x04d6:
            o.Headers r5 = r10.a()
            r1.a(r5)
            long r5 = r0.f2906m
            r1.f2913k = r5
            long r5 = r0.f2907n
            r1.f2914l = r5
            o.m0.c.CacheInterceptor$a r5 = o.m0.c.CacheInterceptor.c
            o.Response r5 = o.m0.c.CacheInterceptor.a.a(r5, r2)
            r1.a(r5)
            o.m0.c.CacheInterceptor$a r5 = o.m0.c.CacheInterceptor.c
            o.Response r5 = o.m0.c.CacheInterceptor.a.a(r5, r0)
            r1.a(r3, r5)
            r1.h = r5
            o.Response r1 = r1.a()
            o.ResponseBody r0 = r0.f2902i
            if (r0 == 0) goto L_0x0547
            r0.close()
            o.Cache r0 = r4.b
            if (r0 == 0) goto L_0x0542
            r0.a()
            o.Cache r0 = r4.b
            if (r0 == 0) goto L_0x0540
            o.Cache$b r0 = new o.Cache$b
            r0.<init>(r1)
            o.ResponseBody r2 = r2.f2902i
            if (r2 == 0) goto L_0x0538
            o.Cache$a r2 = (o.Cache.a) r2
            o.m0.c.DiskLruCache$c r2 = r2.f2803e
            o.m0.c.DiskLruCache r3 = r2.f2938e     // Catch:{ IOException -> 0x0531 }
            java.lang.String r5 = r2.b     // Catch:{ IOException -> 0x0531 }
            long r6 = r2.c     // Catch:{ IOException -> 0x0531 }
            o.m0.c.DiskLruCache$a r2 = r3.a(r5, r6)     // Catch:{ IOException -> 0x0531 }
            if (r2 == 0) goto L_0x0537
            r0.a(r2)     // Catch:{ IOException -> 0x052f }
            r2.b()     // Catch:{ IOException -> 0x052f }
            goto L_0x0537
        L_0x052f:
            goto L_0x0532
        L_0x0531:
            r2 = 0
        L_0x0532:
            if (r2 == 0) goto L_0x0537
            r2.a()     // Catch:{ IOException -> 0x0537 }
        L_0x0537:
            return r1
        L_0x0538:
            kotlin.TypeCastException r0 = new kotlin.TypeCastException
            java.lang.String r1 = "null cannot be cast to non-null type okhttp3.Cache.CacheResponseBody"
            r0.<init>(r1)
            throw r0
        L_0x0540:
            r0 = 0
            throw r0
        L_0x0542:
            r0 = 0
            n.n.c.Intrinsics.a()
            throw r0
        L_0x0547:
            r0 = 0
            n.n.c.Intrinsics.a()
            throw r0
        L_0x054c:
            r5 = 0
            o.ResponseBody r7 = r2.f2902i
            if (r7 == 0) goto L_0x0556
            o.m0.Util.a(r7)
            goto L_0x0556
        L_0x0555:
            r5 = 0
        L_0x0556:
            if (r0 == 0) goto L_0x067f
            o.Response$a r7 = new o.Response$a
            r7.<init>(r0)
            o.m0.c.CacheInterceptor$a r8 = o.m0.c.CacheInterceptor.c
            o.Response r2 = o.m0.c.CacheInterceptor.a.a(r8, r2)
            r7.a(r2)
            o.m0.c.CacheInterceptor$a r2 = o.m0.c.CacheInterceptor.c
            o.Response r0 = o.m0.c.CacheInterceptor.a.a(r2, r0)
            r7.a(r3, r0)
            r7.h = r0
            o.Response r0 = r7.a()
            o.Cache r2 = r4.b
            if (r2 == 0) goto L_0x067e
            boolean r2 = o.m0.e.HttpHeaders.a(r0)
            java.lang.String r3 = "MOVE"
            java.lang.String r7 = "DELETE"
            java.lang.String r8 = "PUT"
            java.lang.String r9 = "PATCH"
            java.lang.String r10 = "POST"
            java.lang.String r11 = "method"
            if (r2 == 0) goto L_0x064b
            boolean r2 = o.m0.c.CacheStrategy.a(r0, r1)
            if (r2 == 0) goto L_0x064b
            o.Cache r1 = r4.b
            if (r1 == 0) goto L_0x0649
            o.Request r2 = r0.c
            java.lang.String r2 = r2.c
            if (r2 == 0) goto L_0x0644
            boolean r10 = n.n.c.Intrinsics.a(r2, r10)
            if (r10 != 0) goto L_0x05b9
            boolean r9 = n.n.c.Intrinsics.a(r2, r9)
            if (r9 != 0) goto L_0x05b9
            boolean r8 = n.n.c.Intrinsics.a(r2, r8)
            if (r8 != 0) goto L_0x05b9
            boolean r7 = n.n.c.Intrinsics.a(r2, r7)
            if (r7 != 0) goto L_0x05b9
            boolean r3 = n.n.c.Intrinsics.a(r2, r3)
            if (r3 == 0) goto L_0x05ba
        L_0x05b9:
            r5 = 1
        L_0x05ba:
            if (r5 == 0) goto L_0x05c2
            o.Request r2 = r0.c     // Catch:{ IOException -> 0x0604 }
            r1.a(r2)     // Catch:{ IOException -> 0x0604 }
            goto L_0x0604
        L_0x05c2:
            java.lang.String r3 = "GET"
            boolean r2 = n.n.c.Intrinsics.a(r2, r3)
            r2 = r2 ^ 1
            if (r2 == 0) goto L_0x05cd
            goto L_0x0604
        L_0x05cd:
            o.Headers r2 = r0.h
            java.util.Set r2 = o.Cache.a(r2)
            java.lang.String r3 = "*"
            boolean r2 = r2.contains(r3)
            if (r2 == 0) goto L_0x05dc
            goto L_0x0604
        L_0x05dc:
            o.Cache$b r2 = new o.Cache$b
            r2.<init>(r0)
            o.m0.c.DiskLruCache r3 = r1.b     // Catch:{ IOException -> 0x05fe }
            o.Request r5 = r0.c     // Catch:{ IOException -> 0x05fe }
            o.HttpUrl r5 = r5.b     // Catch:{ IOException -> 0x05fe }
            java.lang.String r5 = o.Cache.a(r5)     // Catch:{ IOException -> 0x05fe }
            r7 = 0
            o.m0.c.DiskLruCache$a r3 = o.m0.c.DiskLruCache.a(r3, r5, r7, r6)     // Catch:{ IOException -> 0x05fe }
            if (r3 == 0) goto L_0x0604
            r2.a(r3)     // Catch:{ IOException -> 0x05fc }
            o.Cache$c r2 = new o.Cache$c     // Catch:{ IOException -> 0x05fc }
            r2.<init>(r1, r3)     // Catch:{ IOException -> 0x05fc }
            goto L_0x0605
        L_0x05fc:
            goto L_0x05ff
        L_0x05fe:
            r3 = 0
        L_0x05ff:
            if (r3 == 0) goto L_0x0604
            r3.a()     // Catch:{ IOException -> 0x0604 }
        L_0x0604:
            r2 = 0
        L_0x0605:
            if (r2 != 0) goto L_0x0608
            goto L_0x063e
        L_0x0608:
            p.Sink r1 = r2.b()
            o.ResponseBody r3 = r0.f2902i
            if (r3 == 0) goto L_0x063f
            p.BufferedSource r3 = r3.g()
            p.BufferedSink r1 = n.i.Collections.a(r1)
            o.m0.c.CacheInterceptor0 r5 = new o.m0.c.CacheInterceptor0
            r5.<init>(r3, r2, r1)
            r1 = 0
            r2 = r20
            java.lang.String r1 = o.Response.a(r0, r2, r1, r6)
            o.ResponseBody r2 = r0.f2902i
            long r2 = r2.a()
            o.Response$a r6 = new o.Response$a
            r6.<init>(r0)
            o.m0.e.RealResponseBody r0 = new o.m0.e.RealResponseBody
            p.BufferedSource r5 = n.i.Collections.a(r5)
            r0.<init>(r1, r2, r5)
            r6.g = r0
            o.Response r0 = r6.a()
        L_0x063e:
            return r0
        L_0x063f:
            n.n.c.Intrinsics.a()
            r0 = 0
            throw r0
        L_0x0644:
            r0 = 0
            n.n.c.Intrinsics.a(r11)
            throw r0
        L_0x0649:
            r0 = 0
            throw r0
        L_0x064b:
            java.lang.String r2 = r1.c
            if (r2 == 0) goto L_0x0679
            boolean r5 = n.n.c.Intrinsics.a(r2, r10)
            if (r5 != 0) goto L_0x0670
            boolean r5 = n.n.c.Intrinsics.a(r2, r9)
            if (r5 != 0) goto L_0x0670
            boolean r5 = n.n.c.Intrinsics.a(r2, r8)
            if (r5 != 0) goto L_0x0670
            boolean r5 = n.n.c.Intrinsics.a(r2, r7)
            if (r5 != 0) goto L_0x0670
            boolean r2 = n.n.c.Intrinsics.a(r2, r3)
            if (r2 == 0) goto L_0x066e
            goto L_0x0670
        L_0x066e:
            r2 = 0
            goto L_0x0671
        L_0x0670:
            r2 = 1
        L_0x0671:
            if (r2 == 0) goto L_0x067e
            o.Cache r2 = r4.b     // Catch:{ IOException -> 0x067e }
            r2.a(r1)     // Catch:{ IOException -> 0x067e }
            goto L_0x067e
        L_0x0679:
            n.n.c.Intrinsics.a(r11)
            r0 = 0
            throw r0
        L_0x067e:
            return r0
        L_0x067f:
            r0 = 0
            n.n.c.Intrinsics.a()
            throw r0
        L_0x0684:
            r0 = move-exception
            r1 = r0
            if (r3 == 0) goto L_0x068f
            o.ResponseBody r0 = r3.f2902i
            if (r0 == 0) goto L_0x068f
            o.m0.Util.a(r0)
        L_0x068f:
            throw r1
        L_0x0690:
            r4 = r1
            n.n.c.Intrinsics.a(r5)
            r0 = 0
            throw r0
        L_0x0696:
            r4 = r1
            java.lang.String r0 = "chain"
            n.n.c.Intrinsics.a(r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.c.CacheInterceptor.a(o.Interceptor$a):o.Response");
    }

    /* compiled from: CacheInterceptor.kt */
    public static final class a {
        public /* synthetic */ a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public static final /* synthetic */ Response a(a aVar, Response response) {
            if (aVar != null) {
                if ((response != null ? response.f2902i : null) == null) {
                    return response;
                }
                if (response != null) {
                    Response.a aVar2 = new Response.a(response);
                    aVar2.g = null;
                    return aVar2.a();
                }
                throw null;
            }
            throw null;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean
         arg types: [java.lang.String, java.lang.String, int]
         candidates:
          n.r.Indent.a(java.lang.String, java.lang.String, int):java.lang.String
          n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean
          n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean */
        public final boolean b(String str) {
            if (Indent.a("Connection", str, true) || Indent.a("Keep-Alive", str, true) || Indent.a("Proxy-Authenticate", str, true) || Indent.a("Proxy-Authorization", str, true) || Indent.a("TE", str, true) || Indent.a("Trailers", str, true) || Indent.a("Transfer-Encoding", str, true) || Indent.a("Upgrade", str, true)) {
                return false;
            }
            return true;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean
         arg types: [java.lang.String, java.lang.String, int]
         candidates:
          n.r.Indent.a(java.lang.String, java.lang.String, int):java.lang.String
          n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean
          n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean */
        public final boolean a(String str) {
            if (Indent.a("Content-Length", str, true) || Indent.a("Content-Encoding", str, true) || Indent.a("Content-Type", str, true)) {
                return true;
            }
            return false;
        }
    }
}
