package o.m0.g;

import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import kotlin.TypeCastException;
import n.n.c.Intrinsics;
import n.r.Indent;
import o.Headers;
import o.HttpUrl;
import o.Interceptor;
import o.OkHttpClient;
import o.Protocol;
import o.Request;
import o.Response;
import o.m0.Util;
import o.m0.d.RealConnection;
import o.m0.e.ExchangeCode;
import o.m0.e.StatusLine;
import p.ByteString;
import p.Sink;
import p.Source;

/* compiled from: Http2ExchangeCodec.kt */
public final class Http2ExchangeCodec implements ExchangeCode {
    public static final List<String> g = Util.a("connection", "host", "keep-alive", "proxy-connection", "te", "transfer-encoding", "encoding", "upgrade", ":method", ":path", ":scheme", ":authority");
    public static final List<String> h = Util.a("connection", "host", "keep-alive", "proxy-connection", "te", "transfer-encoding", "encoding", "upgrade");
    public volatile Http2Stream a;
    public final Protocol b;
    public volatile boolean c;
    public final RealConnection d;

    /* renamed from: e  reason: collision with root package name */
    public final Interceptor.a f3019e;

    /* renamed from: f  reason: collision with root package name */
    public final Http2Connection f3020f;

    public Http2ExchangeCodec(OkHttpClient okHttpClient, RealConnection realConnection, Interceptor.a aVar, Http2Connection http2Connection) {
        Protocol protocol;
        if (okHttpClient == null) {
            Intrinsics.a("client");
            throw null;
        } else if (realConnection == null) {
            Intrinsics.a("realConnection");
            throw null;
        } else if (aVar == null) {
            Intrinsics.a("chain");
            throw null;
        } else if (http2Connection != null) {
            this.d = realConnection;
            this.f3019e = aVar;
            this.f3020f = http2Connection;
            if (okHttpClient.u.contains(Protocol.H2_PRIOR_KNOWLEDGE)) {
                protocol = Protocol.H2_PRIOR_KNOWLEDGE;
            } else {
                protocol = Protocol.HTTP_2;
            }
            this.b = protocol;
        } else {
            Intrinsics.a("connection");
            throw null;
        }
    }

    public RealConnection a() {
        return this.d;
    }

    public void b() {
        Http2Stream http2Stream = this.a;
        if (http2Stream != null) {
            http2Stream.d().close();
        } else {
            Intrinsics.a();
            throw null;
        }
    }

    public void c() {
        this.f3020f.f3011t.flush();
    }

    public void cancel() {
        this.c = true;
        Http2Stream http2Stream = this.a;
        if (http2Stream != null) {
            http2Stream.a(ErrorCode.CANCEL);
        }
    }

    public Sink a(Request request, long j2) {
        if (request != null) {
            Http2Stream http2Stream = this.a;
            if (http2Stream != null) {
                return http2Stream.d();
            }
            Intrinsics.a();
            throw null;
        }
        Intrinsics.a("request");
        throw null;
    }

    public Source b(Response response) {
        if (response != null) {
            Http2Stream http2Stream = this.a;
            if (http2Stream != null) {
                return http2Stream.g;
            }
            Intrinsics.a();
            throw null;
        }
        Intrinsics.a("response");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Locale, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
    public void a(Request request) {
        if (request == null) {
            Intrinsics.a("request");
            throw null;
        } else if (this.a == null) {
            boolean z = request.f2896e != null;
            Headers headers = request.d;
            ArrayList arrayList = new ArrayList(headers.size() + 4);
            arrayList.add(new Header(Header.f2989f, request.c));
            ByteString byteString = Header.g;
            HttpUrl httpUrl = request.b;
            if (httpUrl != null) {
                String b2 = httpUrl.b();
                String d2 = httpUrl.d();
                if (d2 != null) {
                    b2 = b2 + '?' + d2;
                }
                arrayList.add(new Header(byteString, b2));
                String a2 = request.a("Host");
                if (a2 != null) {
                    arrayList.add(new Header(Header.f2990i, a2));
                }
                arrayList.add(new Header(Header.h, request.b.b));
                int size = headers.size();
                int i2 = 0;
                while (i2 < size) {
                    String c2 = headers.c(i2);
                    Locale locale = Locale.US;
                    Intrinsics.a((Object) locale, "Locale.US");
                    if (c2 != null) {
                        String lowerCase = c2.toLowerCase(locale);
                        Intrinsics.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                        if (!g.contains(lowerCase) || (Intrinsics.a((Object) lowerCase, (Object) "te") && Intrinsics.a((Object) headers.d(i2), (Object) "trailers"))) {
                            arrayList.add(new Header(lowerCase, headers.d(i2)));
                        }
                        i2++;
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                    }
                }
                this.a = this.f3020f.a(0, arrayList, z);
                if (this.c) {
                    Http2Stream http2Stream = this.a;
                    if (http2Stream == null) {
                        Intrinsics.a();
                        throw null;
                    } else {
                        http2Stream.a(ErrorCode.CANCEL);
                        throw new IOException("Canceled");
                    }
                } else {
                    Http2Stream http2Stream2 = this.a;
                    if (http2Stream2 != null) {
                        http2Stream2.f3027i.a((long) this.f3019e.d(), TimeUnit.MILLISECONDS);
                        Http2Stream http2Stream3 = this.a;
                        if (http2Stream3 != null) {
                            http2Stream3.f3028j.a((long) this.f3019e.b(), TimeUnit.MILLISECONDS);
                        } else {
                            Intrinsics.a();
                            throw null;
                        }
                    } else {
                        Intrinsics.a();
                        throw null;
                    }
                }
            } else {
                Intrinsics.a("url");
                throw null;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
    public Response.a a(boolean z) {
        Http2Stream http2Stream = this.a;
        if (http2Stream != null) {
            Headers g2 = http2Stream.g();
            Protocol protocol = this.b;
            if (g2 == null) {
                Intrinsics.a("headerBlock");
                throw null;
            } else if (protocol != null) {
                ArrayList arrayList = new ArrayList(20);
                int size = g2.size();
                StatusLine statusLine = null;
                for (int i2 = 0; i2 < size; i2++) {
                    String c2 = g2.c(i2);
                    String d2 = g2.d(i2);
                    if (Intrinsics.a((Object) c2, (Object) ":status")) {
                        statusLine = StatusLine.a("HTTP/1.1 " + d2);
                    } else if (h.contains(c2)) {
                        continue;
                    } else if (c2 == null) {
                        Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
                        throw null;
                    } else if (d2 != null) {
                        arrayList.add(c2);
                        arrayList.add(Indent.c(d2).toString());
                    } else {
                        Intrinsics.a("value");
                        throw null;
                    }
                }
                if (statusLine != null) {
                    Response.a aVar = new Response.a();
                    aVar.b = protocol;
                    aVar.c = statusLine.b;
                    aVar.a(statusLine.c);
                    Object[] array = arrayList.toArray(new String[0]);
                    if (array != null) {
                        aVar.a(new Headers((String[]) array, null));
                        if (!z || aVar.c != 100) {
                            return aVar;
                        }
                        return null;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                }
                throw new ProtocolException("Expected ':status' header not present");
            } else {
                Intrinsics.a("protocol");
                throw null;
            }
        } else {
            Intrinsics.a();
            throw null;
        }
    }

    public long a(Response response) {
        if (response != null) {
            return Util.a(response);
        }
        Intrinsics.a("response");
        throw null;
    }
}
