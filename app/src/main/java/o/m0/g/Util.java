package o.m0.g;

import p.Buffer;

/* compiled from: Util.kt */
public final class Util implements Runnable {
    public final /* synthetic */ String b;
    public final /* synthetic */ Http2Connection c;
    public final /* synthetic */ int d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ Buffer f3039e;

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ int f3040f;
    public final /* synthetic */ boolean g;

    public Util(String str, Http2Connection http2Connection, int i2, Buffer buffer, int i3, boolean z) {
        this.b = str;
        this.c = http2Connection;
        this.d = i2;
        this.f3039e = buffer;
        this.f3040f = i3;
        this.g = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.Thread, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004b, code lost:
        r1.setName(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004e, code lost:
        throw r0;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r7 = this;
            java.lang.String r0 = r7.b
            java.lang.Thread r1 = java.lang.Thread.currentThread()
            java.lang.String r2 = "currentThread"
            n.n.c.Intrinsics.a(r1, r2)
            java.lang.String r2 = r1.getName()
            r1.setName(r0)
            o.m0.g.Http2Connection r0 = r7.c     // Catch:{ IOException -> 0x004f, all -> 0x004a }
            o.m0.g.PushObserver r0 = r0.f3002k     // Catch:{ IOException -> 0x004f, all -> 0x004a }
            int r3 = r7.d     // Catch:{ IOException -> 0x004f, all -> 0x004a }
            p.Buffer r4 = r7.f3039e     // Catch:{ IOException -> 0x004f, all -> 0x004a }
            int r5 = r7.f3040f     // Catch:{ IOException -> 0x004f, all -> 0x004a }
            boolean r6 = r7.g     // Catch:{ IOException -> 0x004f, all -> 0x004a }
            boolean r0 = r0.a(r3, r4, r5, r6)     // Catch:{ IOException -> 0x004f, all -> 0x004a }
            if (r0 == 0) goto L_0x002f
            o.m0.g.Http2Connection r3 = r7.c     // Catch:{ IOException -> 0x004f, all -> 0x004a }
            o.m0.g.Http2Writer r3 = r3.f3011t     // Catch:{ IOException -> 0x004f, all -> 0x004a }
            int r4 = r7.d     // Catch:{ IOException -> 0x004f, all -> 0x004a }
            o.m0.g.ErrorCode r5 = o.m0.g.ErrorCode.CANCEL     // Catch:{ IOException -> 0x004f, all -> 0x004a }
            r3.a(r4, r5)     // Catch:{ IOException -> 0x004f, all -> 0x004a }
        L_0x002f:
            if (r0 != 0) goto L_0x0035
            boolean r0 = r7.g     // Catch:{ IOException -> 0x004f, all -> 0x004a }
            if (r0 == 0) goto L_0x004f
        L_0x0035:
            o.m0.g.Http2Connection r0 = r7.c     // Catch:{ IOException -> 0x004f, all -> 0x004a }
            monitor-enter(r0)     // Catch:{ IOException -> 0x004f, all -> 0x004a }
            o.m0.g.Http2Connection r3 = r7.c     // Catch:{ all -> 0x0047 }
            java.util.Set<java.lang.Integer> r3 = r3.v     // Catch:{ all -> 0x0047 }
            int r4 = r7.d     // Catch:{ all -> 0x0047 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x0047 }
            r3.remove(r4)     // Catch:{ all -> 0x0047 }
            monitor-exit(r0)     // Catch:{ IOException -> 0x004f, all -> 0x004a }
            goto L_0x004f
        L_0x0047:
            r3 = move-exception
            monitor-exit(r0)     // Catch:{ IOException -> 0x004f, all -> 0x004a }
            throw r3     // Catch:{ IOException -> 0x004f, all -> 0x004a }
        L_0x004a:
            r0 = move-exception
            r1.setName(r2)
            throw r0
        L_0x004f:
            r1.setName(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.g.Util.run():void");
    }
}
