package o.n0;

import androidx.recyclerview.widget.RecyclerView;
import j.a.a.a.outline;
import java.io.Closeable;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import n.i.Collections;
import n.i.Sets;
import n.n.c.Intrinsics;
import n.r.Indent;
import o.Connection;
import o.Headers;
import o.Interceptor;
import o.MediaType;
import o.Request;
import o.RequestBody;
import o.Response;
import o.ResponseBody;
import o.m0.e.HttpHeaders;
import p.Buffer;
import p.BufferedSource;
import p.GzipSource;

/* compiled from: HttpLoggingInterceptor.kt */
public final class HttpLoggingInterceptor implements Interceptor {
    public volatile Set<String> b;
    public volatile a c;
    public final b d;

    /* compiled from: HttpLoggingInterceptor.kt */
    public enum a {
        NONE,
        BASIC,
        HEADERS,
        BODY
    }

    /* compiled from: HttpLoggingInterceptor.kt */
    public interface b {
        public static final b a = new b$a();

        void a(String str);
    }

    public /* synthetic */ HttpLoggingInterceptor(b bVar, int i2) {
        bVar = (i2 & 1) != 0 ? b.a : bVar;
        if (bVar != null) {
            this.d = bVar;
            this.b = Sets.b;
            this.c = a.NONE;
            return;
        }
        Intrinsics.a("logger");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.nio.charset.Charset, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      n.r.Indent.a(java.lang.String, java.lang.String, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
     arg types: [p.GzipSource, ?[OBJECT, ARRAY]]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(java.lang.Object, java.lang.String):T
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
     arg types: [p.GzipSource, java.lang.Throwable]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(java.lang.Object, java.lang.String):T
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void */
    public Response a(Interceptor.a aVar) {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        Long l2;
        Charset charset;
        Throwable th;
        Charset charset2;
        Interceptor.a aVar2 = aVar;
        if (aVar2 != null) {
            a aVar3 = this.c;
            Request f2 = aVar.f();
            if (aVar3 == a.NONE) {
                return aVar2.a(f2);
            }
            boolean z = aVar3 == a.BODY;
            boolean z2 = z || aVar3 == a.HEADERS;
            RequestBody requestBody = f2.f2896e;
            Connection a2 = aVar.a();
            StringBuilder a3 = outline.a("--> ");
            a3.append(f2.c);
            a3.append(' ');
            a3.append(f2.b);
            if (a2 != null) {
                StringBuilder a4 = outline.a(" ");
                a4.append(a2.a());
                str = a4.toString();
            } else {
                str = "";
            }
            a3.append(str);
            String sb = a3.toString();
            if (!z2 && requestBody != null) {
                StringBuilder b2 = outline.b(sb, " (");
                b2.append(requestBody.a());
                b2.append("-byte body)");
                sb = b2.toString();
            }
            this.d.a(sb);
            if (z2) {
                Headers headers = f2.d;
                if (requestBody != null) {
                    MediaType b3 = requestBody.b();
                    if (b3 != null && headers.a("Content-Type") == null) {
                        this.d.a("Content-Type: " + b3);
                    }
                    if (requestBody.a() != -1 && headers.a("Content-Length") == null) {
                        b bVar = this.d;
                        StringBuilder a5 = outline.a("Content-Length: ");
                        a5.append(requestBody.a());
                        bVar.a(a5.toString());
                    }
                }
                int size = headers.size();
                for (int i2 = 0; i2 < size; i2++) {
                    a(headers, i2);
                }
                if (!z || requestBody == null) {
                    b bVar2 = this.d;
                    StringBuilder a6 = outline.a("--> END ");
                    a6.append(f2.c);
                    bVar2.a(a6.toString());
                } else if (a(f2.d)) {
                    b bVar3 = this.d;
                    StringBuilder a7 = outline.a("--> END ");
                    a7.append(f2.c);
                    a7.append(" (encoded body omitted)");
                    bVar3.a(a7.toString());
                } else {
                    Buffer buffer = new Buffer();
                    requestBody.a(buffer);
                    MediaType b4 = requestBody.b();
                    if (b4 == null || (charset2 = b4.a(StandardCharsets.UTF_8)) == null) {
                        charset2 = StandardCharsets.UTF_8;
                        Intrinsics.a((Object) charset2, "UTF_8");
                    }
                    this.d.a("");
                    if (Collections.a(buffer)) {
                        this.d.a(buffer.a(charset2));
                        b bVar4 = this.d;
                        StringBuilder a8 = outline.a("--> END ");
                        a8.append(f2.c);
                        a8.append(" (");
                        a8.append(requestBody.a());
                        a8.append("-byte body)");
                        bVar4.a(a8.toString());
                    } else {
                        b bVar5 = this.d;
                        StringBuilder a9 = outline.a("--> END ");
                        a9.append(f2.c);
                        a9.append(" (binary ");
                        a9.append(requestBody.a());
                        a9.append("-byte body omitted)");
                        bVar5.a(a9.toString());
                    }
                }
            }
            long nanoTime = System.nanoTime();
            try {
                Response a10 = aVar2.a(f2);
                long millis = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - nanoTime);
                ResponseBody responseBody = a10.f2902i;
                if (responseBody != null) {
                    long a11 = responseBody.a();
                    if (a11 != -1) {
                        str2 = a11 + "-byte";
                    } else {
                        str2 = "unknown-length";
                    }
                    b bVar6 = this.d;
                    String str6 = "-byte body)";
                    StringBuilder a12 = outline.a("<-- ");
                    long j2 = a11;
                    a12.append(a10.f2901f);
                    if (a10.f2900e.length() == 0) {
                        str3 = "-byte body omitted)";
                        str4 = "";
                    } else {
                        String str7 = a10.f2900e;
                        StringBuilder sb2 = new StringBuilder();
                        str3 = "-byte body omitted)";
                        sb2.append(String.valueOf(' '));
                        sb2.append(str7);
                        str4 = sb2.toString();
                    }
                    a12.append(str4);
                    a12.append(' ');
                    a12.append(a10.c.b);
                    a12.append(" (");
                    a12.append(millis);
                    a12.append("ms");
                    if (!z2) {
                        str5 = outline.a(", ", str2, " body");
                    } else {
                        str5 = "";
                    }
                    a12.append(str5);
                    a12.append(')');
                    bVar6.a(a12.toString());
                    if (z2) {
                        Headers headers2 = a10.h;
                        int size2 = headers2.size();
                        for (int i3 = 0; i3 < size2; i3++) {
                            a(headers2, i3);
                        }
                        if (!z || !HttpHeaders.a(a10)) {
                            this.d.a("<-- END HTTP");
                        } else if (a(a10.h)) {
                            this.d.a("<-- END HTTP (encoded body omitted)");
                        } else {
                            BufferedSource g = responseBody.g();
                            g.d(RecyclerView.FOREVER_NS);
                            Buffer buffer2 = g.getBuffer();
                            if (Indent.a("gzip", headers2.a("Content-Encoding"), true)) {
                                l2 = Long.valueOf(buffer2.c);
                                GzipSource gzipSource = new GzipSource(buffer2.clone());
                                try {
                                    buffer2 = new Buffer();
                                    buffer2.a(gzipSource);
                                    Collections.a((Closeable) gzipSource, (Throwable) null);
                                } catch (Throwable th2) {
                                    Throwable th3 = th2;
                                    Collections.a((Closeable) gzipSource, th);
                                    throw th3;
                                }
                            } else {
                                l2 = null;
                            }
                            MediaType f3 = responseBody.f();
                            if (f3 == null || (charset = f3.a(StandardCharsets.UTF_8)) == null) {
                                charset = StandardCharsets.UTF_8;
                                Intrinsics.a((Object) charset, "UTF_8");
                            }
                            if (!Collections.a(buffer2)) {
                                this.d.a("");
                                b bVar7 = this.d;
                                StringBuilder a13 = outline.a("<-- END HTTP (binary ");
                                a13.append(buffer2.c);
                                a13.append(str3);
                                bVar7.a(a13.toString());
                                return a10;
                            }
                            if (j2 != 0) {
                                this.d.a("");
                                this.d.a(buffer2.clone().a(charset));
                            }
                            if (l2 != null) {
                                b bVar8 = this.d;
                                StringBuilder a14 = outline.a("<-- END HTTP (");
                                a14.append(buffer2.c);
                                a14.append("-byte, ");
                                a14.append(l2);
                                a14.append("-gzipped-byte body)");
                                bVar8.a(a14.toString());
                            } else {
                                b bVar9 = this.d;
                                StringBuilder a15 = outline.a("<-- END HTTP (");
                                a15.append(buffer2.c);
                                a15.append(str6);
                                bVar9.a(a15.toString());
                            }
                        }
                    }
                    return a10;
                }
                Intrinsics.a();
                throw null;
            } catch (Exception e2) {
                Exception exc = e2;
                this.d.a("<-- HTTP FAILED: " + exc);
                throw exc;
            }
        } else {
            Intrinsics.a("chain");
            throw null;
        }
    }

    public final void a(Headers headers, int i2) {
        String str;
        int i3 = i2 * 2;
        if (this.b.contains(headers.b[i3])) {
            str = "██";
        } else {
            str = headers.b[i3 + 1];
        }
        b bVar = this.d;
        bVar.a(headers.b[i3] + ": " + str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      n.r.Indent.a(java.lang.String, java.lang.String, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean */
    public final boolean a(Headers headers) {
        String a2 = headers.a("Content-Encoding");
        if (a2 == null || Indent.a(a2, "identity", true) || Indent.a(a2, "gzip", true)) {
            return false;
        }
        return true;
    }
}
