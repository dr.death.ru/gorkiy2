package p;

import java.security.MessageDigest;
import java.util.Arrays;
import n.i.Collections;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;
import p.b0.ByteString;

/* compiled from: SegmentedByteString.kt */
public final class SegmentedByteString extends ByteString {
    public final transient byte[][] g;
    public final transient int[] h;

    public /* synthetic */ SegmentedByteString(byte[][] bArr, int[] iArr, DefaultConstructorMarker defaultConstructorMarker) {
        super(ByteString.f3062e.d);
        this.g = bArr;
        this.h = iArr;
    }

    public void a(Buffer buffer) {
        if (buffer != null) {
            int length = this.g.length;
            int i2 = 0;
            int i3 = 0;
            while (i2 < length) {
                int[] iArr = this.h;
                int i4 = iArr[length + i2];
                int i5 = iArr[i2];
                Segment segment = new Segment(this.g[i2], i4, i4 + (i5 - i3), true, false);
                Segment segment2 = buffer.b;
                if (segment2 == null) {
                    segment.g = segment;
                    segment.f3071f = segment;
                    buffer.b = segment;
                } else if (segment2 != null) {
                    Segment segment3 = segment2.g;
                    if (segment3 != null) {
                        segment3.a(segment);
                    } else {
                        Intrinsics.a();
                        throw null;
                    }
                } else {
                    Intrinsics.a();
                    throw null;
                }
                i2++;
                i3 = i5;
            }
            buffer.c += (long) g();
            return;
        }
        Intrinsics.a("buffer");
        throw null;
    }

    public final int b(int i2) {
        int binarySearch = Arrays.binarySearch(this.h, 0, this.g.length, i2 + 1);
        return binarySearch >= 0 ? binarySearch : ~binarySearch;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ByteString) {
            ByteString byteString = (ByteString) obj;
            if (super.g() != g() || !a(0, super, 0, g())) {
                return false;
            }
            return true;
        }
        return false;
    }

    public String f() {
        return ByteString.a(o());
    }

    public int g() {
        return this.h[this.g.length - 1];
    }

    public String h() {
        return ByteString.d(o());
    }

    public int hashCode() {
        int i2 = super.b;
        if (i2 != 0) {
            return i2;
        }
        int length = this.g.length;
        int i3 = 0;
        int i4 = 0;
        int i5 = 1;
        while (i3 < length) {
            int[] iArr = this.h;
            int i6 = iArr[length + i3];
            int i7 = iArr[i3];
            byte[] bArr = this.g[i3];
            int i8 = (i7 - i4) + i6;
            while (i6 < i8) {
                i5 = (i5 * 31) + bArr[i6];
                i6++;
            }
            i3++;
            i4 = i7;
        }
        super.b = i5;
        return i5;
    }

    public byte[] i() {
        return l();
    }

    public ByteString j() {
        return ByteString.f(o());
    }

    public byte[] l() {
        byte[] bArr = new byte[g()];
        int length = this.g.length;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        while (i2 < length) {
            int[] iArr = this.h;
            int i5 = iArr[length + i2];
            int i6 = iArr[i2];
            int i7 = i6 - i3;
            Collections.b(this.g[i2], i5, bArr, i4, i7);
            i4 += i7;
            i2++;
            i3 = i6;
        }
        return bArr;
    }

    public final ByteString o() {
        return new ByteString(l());
    }

    public String toString() {
        return ByteString.g(o());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [byte[], java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public ByteString a(String str) {
        if (str != null) {
            MessageDigest instance = MessageDigest.getInstance(str);
            int length = this.g.length;
            int i2 = 0;
            int i3 = 0;
            while (i2 < length) {
                int[] iArr = this.h;
                int i4 = iArr[length + i2];
                int i5 = iArr[i2];
                instance.update(this.g[i2], i4, i5 - i3);
                i2++;
                i3 = i5;
            }
            byte[] digest = instance.digest();
            Intrinsics.a((Object) digest, "digest.digest()");
            return new ByteString(digest);
        }
        Intrinsics.a("algorithm");
        throw null;
    }

    public byte a(int i2) {
        int i3;
        Collections.a((long) this.h[this.g.length - 1], (long) i2, 1);
        int b = b(i2);
        if (b == 0) {
            i3 = 0;
        } else {
            i3 = this.h[b - 1];
        }
        int[] iArr = this.h;
        byte[][] bArr = this.g;
        return bArr[b][(i2 - i3) + iArr[bArr.length + b]];
    }

    public boolean a(int i2, ByteString byteString, int i3, int i4) {
        int i5;
        if (byteString == null) {
            Intrinsics.a("other");
            throw null;
        } else if (i2 < 0 || i2 > g() - i4) {
            return false;
        } else {
            int i6 = i4 + i2;
            int b = b(i2);
            while (i2 < i6) {
                if (b == 0) {
                    i5 = 0;
                } else {
                    i5 = this.h[b - 1];
                }
                int[] iArr = this.h;
                int i7 = iArr[this.g.length + b];
                int min = Math.min(i6, (iArr[b] - i5) + i5) - i2;
                if (!super.a(i3, this.g[b], (i2 - i5) + i7, min)) {
                    return false;
                }
                i3 += min;
                i2 += min;
                b++;
            }
            return true;
        }
    }

    public boolean a(int i2, byte[] bArr, int i3, int i4) {
        int i5;
        if (bArr == null) {
            Intrinsics.a("other");
            throw null;
        } else if (i2 < 0 || i2 > g() - i4 || i3 < 0 || i3 > bArr.length - i4) {
            return false;
        } else {
            int i6 = i4 + i2;
            int b = b(i2);
            while (i2 < i6) {
                if (b == 0) {
                    i5 = 0;
                } else {
                    i5 = this.h[b - 1];
                }
                int[] iArr = this.h;
                int i7 = iArr[this.g.length + b];
                int min = Math.min(i6, (iArr[b] - i5) + i5) - i2;
                if (!Collections.a(this.g[b], (i2 - i5) + i7, bArr, i3, min)) {
                    return false;
                }
                i3 += min;
                i2 += min;
                b++;
            }
            return true;
        }
    }
}
