package p;

import androidx.recyclerview.widget.RecyclerView;
import java.io.EOFException;
import java.io.IOException;
import java.util.Arrays;
import java.util.zip.CRC32;
import java.util.zip.Inflater;
import n.n.c.Intrinsics;

/* compiled from: GzipSource.kt */
public final class GzipSource implements Source {
    public byte b;
    public final RealBufferedSource c;
    public final Inflater d;

    /* renamed from: e  reason: collision with root package name */
    public final InflaterSource f3065e;

    /* renamed from: f  reason: collision with root package name */
    public final CRC32 f3066f;

    public GzipSource(Source source) {
        if (source != null) {
            this.c = new RealBufferedSource(source);
            Inflater inflater = new Inflater(true);
            this.d = inflater;
            this.f3065e = new InflaterSource(this.c, inflater);
            this.f3066f = new CRC32();
            return;
        }
        Intrinsics.a("source");
        throw null;
    }

    public final void a(Buffer buffer, long j2, long j3) {
        Segment segment = buffer.b;
        if (segment != null) {
            do {
                int i2 = segment.c;
                int i3 = segment.b;
                if (j2 >= ((long) (i2 - i3))) {
                    j2 -= (long) (i2 - i3);
                    segment = segment.f3071f;
                } else {
                    while (j3 > 0) {
                        int i4 = (int) (((long) segment.b) + j2);
                        int min = (int) Math.min((long) (segment.c - i4), j3);
                        this.f3066f.update(segment.a, i4, min);
                        j3 -= (long) min;
                        segment = segment.f3071f;
                        if (segment != null) {
                            j2 = 0;
                        } else {
                            Intrinsics.a();
                            throw null;
                        }
                    }
                    return;
                }
            } while (segment != null);
            Intrinsics.a();
            throw null;
        }
        Intrinsics.a();
        throw null;
    }

    public long b(Buffer buffer, long j2) {
        long j3;
        Buffer buffer2 = buffer;
        long j4 = j2;
        if (buffer2 != null) {
            boolean z = false;
            int i2 = (j4 > 0 ? 1 : (j4 == 0 ? 0 : -1));
            if (!(i2 >= 0)) {
                throw new IllegalArgumentException(("byteCount < 0: " + j4).toString());
            } else if (i2 == 0) {
                return 0;
            } else {
                if (this.b == 0) {
                    this.c.g(10);
                    byte e2 = this.c.b.e(3);
                    boolean z2 = ((e2 >> 1) & 1) == 1;
                    if (z2) {
                        a(this.c.b, 0, 10);
                    }
                    RealBufferedSource realBufferedSource = this.c;
                    realBufferedSource.g(2);
                    a("ID1ID2", 8075, realBufferedSource.b.readShort());
                    this.c.skip(8);
                    if (((e2 >> 2) & 1) == 1) {
                        this.c.g(2);
                        if (z2) {
                            a(this.c.b, 0, 2);
                        }
                        long n2 = (long) this.c.b.n();
                        this.c.g(n2);
                        if (z2) {
                            j3 = n2;
                            a(this.c.b, 0, n2);
                        } else {
                            j3 = n2;
                        }
                        this.c.skip(j3);
                    }
                    if (((e2 >> 3) & 1) == 1) {
                        long a = this.c.a((byte) 0, 0, RecyclerView.FOREVER_NS);
                        if (a != -1) {
                            if (z2) {
                                a(this.c.b, 0, a + 1);
                            }
                            this.c.skip(a + 1);
                        } else {
                            throw new EOFException();
                        }
                    }
                    if (((e2 >> 4) & 1) == 1) {
                        z = true;
                    }
                    if (z) {
                        long a2 = this.c.a((byte) 0, 0, RecyclerView.FOREVER_NS);
                        if (a2 != -1) {
                            if (z2) {
                                a(this.c.b, 0, a2 + 1);
                            }
                            this.c.skip(a2 + 1);
                        } else {
                            throw new EOFException();
                        }
                    }
                    if (z2) {
                        RealBufferedSource realBufferedSource2 = this.c;
                        realBufferedSource2.g(2);
                        a("FHCRC", realBufferedSource2.b.n(), (short) ((int) this.f3066f.getValue()));
                        this.f3066f.reset();
                    }
                    this.b = 1;
                }
                if (this.b == 1) {
                    long j5 = buffer2.c;
                    long b2 = this.f3065e.b(buffer2, j4);
                    if (b2 != -1) {
                        a(buffer, j5, b2);
                        return b2;
                    }
                    this.b = 2;
                }
                if (this.b == 2) {
                    a("CRC", this.c.a(), (int) this.f3066f.getValue());
                    a("ISIZE", this.c.a(), (int) this.d.getBytesWritten());
                    this.b = 3;
                    if (!this.c.j()) {
                        throw new IOException("gzip finished without exhausting source");
                    }
                }
                return -1;
            }
        } else {
            Intrinsics.a("sink");
            throw null;
        }
    }

    public void close() {
        this.f3065e.close();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final void a(String str, int i2, int i3) {
        if (i3 != i2) {
            String format = String.format("%s: actual 0x%08x != expected 0x%08x", Arrays.copyOf(new Object[]{str, Integer.valueOf(i3), Integer.valueOf(i2)}, 3));
            Intrinsics.a((Object) format, "java.lang.String.format(this, *args)");
            throw new IOException(format);
        }
    }

    public Timeout b() {
        return this.c.b();
    }
}
