package okhttp3.internal.publicsuffix;

import j.c.a.a.c.n.c;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;
import o.m0.Util;

/* compiled from: PublicSuffixDatabase.kt */
public final class PublicSuffixDatabase {

    /* renamed from: e  reason: collision with root package name */
    public static final byte[] f3055e = {(byte) 42};

    /* renamed from: f  reason: collision with root package name */
    public static final List<String> f3056f = c.c((Object) "*");
    public static final PublicSuffixDatabase g = new PublicSuffixDatabase();
    public static final a h = new a(null);
    public final AtomicBoolean a = new AtomicBoolean(false);
    public final CountDownLatch b = new CountDownLatch(1);
    public byte[] c;
    public byte[] d;

    /* compiled from: PublicSuffixDatabase.kt */
    public static final class a {
        public /* synthetic */ a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.nio.charset.Charset, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public static final /* synthetic */ String a(a aVar, byte[] bArr, byte[][] bArr2, int i2) {
            int i3;
            int i4;
            boolean z;
            int a;
            byte[] bArr3 = bArr;
            byte[][] bArr4 = bArr2;
            if (aVar != null) {
                int length = bArr3.length;
                int i5 = 0;
                while (i5 < length) {
                    int i6 = (i5 + length) / 2;
                    while (i6 > -1 && bArr3[i6] != ((byte) 10)) {
                        i6--;
                    }
                    int i7 = i6 + 1;
                    int i8 = 1;
                    while (true) {
                        i3 = i7 + i8;
                        if (bArr3[i3] == ((byte) 10)) {
                            break;
                        }
                        i8++;
                    }
                    int i9 = i3 - i7;
                    int i10 = i2;
                    boolean z2 = false;
                    int i11 = 0;
                    int i12 = 0;
                    while (true) {
                        if (z2) {
                            i4 = 46;
                            z = false;
                        } else {
                            boolean z3 = z2;
                            i4 = Util.a(bArr4[i10][i11], 255);
                            z = z3;
                        }
                        a = i4 - Util.a(bArr3[i7 + i12], 255);
                        if (a == 0) {
                            i12++;
                            i11++;
                            if (i12 == i9) {
                                break;
                            } else if (bArr4[i10].length != i11) {
                                z2 = z;
                            } else if (i10 == bArr4.length - 1) {
                                break;
                            } else {
                                i10++;
                                z2 = true;
                                i11 = -1;
                            }
                        } else {
                            break;
                        }
                    }
                    if (a >= 0) {
                        if (a <= 0) {
                            int i13 = i9 - i12;
                            int length2 = bArr4[i10].length - i11;
                            int length3 = bArr4.length;
                            for (int i14 = i10 + 1; i14 < length3; i14++) {
                                length2 += bArr4[i14].length;
                            }
                            if (length2 >= i13) {
                                if (length2 <= i13) {
                                    Charset charset = StandardCharsets.UTF_8;
                                    Intrinsics.a((Object) charset, "UTF_8");
                                    return new String(bArr3, i7, i9, charset);
                                }
                            }
                        }
                        i5 = i3 + 1;
                    }
                    length = i7 - 1;
                }
                return null;
            }
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.nio.charset.Charset, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [byte[], java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.CharSequence, char[], boolean, int, int):java.util.List
     arg types: [java.lang.String, char[], int, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int
      n.r.Indent.a(java.lang.String, java.lang.String, java.lang.String, boolean, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, java.lang.String[], boolean, int, int):n.q.Sequence
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean, int):boolean
      n.r.Indent.a(java.lang.CharSequence, char[], boolean, int, int):java.util.List */
    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x0037 */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x00ed  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0107  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<java.lang.String> a(java.util.List<java.lang.String> r12) {
        /*
            r11 = this;
            java.util.concurrent.atomic.AtomicBoolean r0 = r11.a
            boolean r0 = r0.get()
            r1 = 1
            r2 = 0
            if (r0 != 0) goto L_0x0046
            java.util.concurrent.atomic.AtomicBoolean r0 = r11.a
            boolean r0 = r0.compareAndSet(r2, r1)
            if (r0 == 0) goto L_0x0046
            r0 = 0
        L_0x0013:
            r11.a()     // Catch:{  }
            if (r0 == 0) goto L_0x0053
            java.lang.Thread r3 = java.lang.Thread.currentThread()     // Catch:{ InterruptedIOException -> 0x0037, IOException -> 0x0022 }
            r3.interrupt()     // Catch:{ InterruptedIOException -> 0x0037, IOException -> 0x0022 }
            goto L_0x0053
        L_0x0020:
            r12 = move-exception
            goto L_0x003c
        L_0x0022:
            r3 = move-exception
            o.m0.i.Platform$a r4 = o.m0.i.Platform.c     // Catch:{ all -> 0x0020 }
            o.m0.i.Platform r4 = o.m0.i.Platform.a     // Catch:{ all -> 0x0020 }
            r5 = 5
            java.lang.String r6 = "Failed to read public suffix list"
            r4.a(r5, r6, r3)     // Catch:{ all -> 0x0020 }
            if (r0 == 0) goto L_0x0053
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            r0.interrupt()
            goto L_0x0053
        L_0x0037:
            java.lang.Thread.interrupted()     // Catch:{ all -> 0x0020 }
            r0 = 1
            goto L_0x0013
        L_0x003c:
            if (r0 == 0) goto L_0x0045
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            r0.interrupt()
        L_0x0045:
            throw r12
        L_0x0046:
            java.util.concurrent.CountDownLatch r0 = r11.b     // Catch:{ InterruptedException -> 0x004c }
            r0.await()     // Catch:{ InterruptedException -> 0x004c }
            goto L_0x0053
        L_0x004c:
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            r0.interrupt()
        L_0x0053:
            byte[] r0 = r11.c
            if (r0 == 0) goto L_0x0059
            r0 = 1
            goto L_0x005a
        L_0x0059:
            r0 = 0
        L_0x005a:
            if (r0 == 0) goto L_0x0139
            int r0 = r12.size()
            byte[][] r3 = new byte[r0][]
            r4 = 0
        L_0x0063:
            if (r4 >= r0) goto L_0x008a
            java.lang.Object r5 = r12.get(r4)
            java.lang.String r5 = (java.lang.String) r5
            java.nio.charset.Charset r6 = java.nio.charset.StandardCharsets.UTF_8
            java.lang.String r7 = "UTF_8"
            n.n.c.Intrinsics.a(r6, r7)
            if (r5 == 0) goto L_0x0082
            byte[] r5 = r5.getBytes(r6)
            java.lang.String r6 = "(this as java.lang.String).getBytes(charset)"
            n.n.c.Intrinsics.a(r5, r6)
            r3[r4] = r5
            int r4 = r4 + 1
            goto L_0x0063
        L_0x0082:
            kotlin.TypeCastException r12 = new kotlin.TypeCastException
            java.lang.String r0 = "null cannot be cast to non-null type java.lang.String"
            r12.<init>(r0)
            throw r12
        L_0x008a:
            r12 = 0
        L_0x008b:
            java.lang.String r4 = "publicSuffixListBytes"
            r5 = 0
            if (r12 >= r0) goto L_0x00a4
            okhttp3.internal.publicsuffix.PublicSuffixDatabase$a r6 = okhttp3.internal.publicsuffix.PublicSuffixDatabase.h
            byte[] r7 = r11.c
            if (r7 == 0) goto L_0x00a0
            java.lang.String r6 = okhttp3.internal.publicsuffix.PublicSuffixDatabase.a.a(r6, r7, r3, r12)
            if (r6 == 0) goto L_0x009d
            goto L_0x00a5
        L_0x009d:
            int r12 = r12 + 1
            goto L_0x008b
        L_0x00a0:
            n.n.c.Intrinsics.b(r4)
            throw r5
        L_0x00a4:
            r6 = r5
        L_0x00a5:
            if (r0 <= r1) goto L_0x00ca
            java.lang.Object r12 = r3.clone()
            byte[][] r12 = (byte[][]) r12
            int r7 = r12.length
            int r7 = r7 - r1
            r8 = 0
        L_0x00b0:
            if (r8 >= r7) goto L_0x00ca
            byte[] r9 = okhttp3.internal.publicsuffix.PublicSuffixDatabase.f3055e
            r12[r8] = r9
            okhttp3.internal.publicsuffix.PublicSuffixDatabase$a r9 = okhttp3.internal.publicsuffix.PublicSuffixDatabase.h
            byte[] r10 = r11.c
            if (r10 == 0) goto L_0x00c6
            java.lang.String r9 = okhttp3.internal.publicsuffix.PublicSuffixDatabase.a.a(r9, r10, r12, r8)
            if (r9 == 0) goto L_0x00c3
            goto L_0x00cb
        L_0x00c3:
            int r8 = r8 + 1
            goto L_0x00b0
        L_0x00c6:
            n.n.c.Intrinsics.b(r4)
            throw r5
        L_0x00ca:
            r9 = r5
        L_0x00cb:
            if (r9 == 0) goto L_0x00e8
            int r0 = r0 - r1
            r12 = 0
        L_0x00cf:
            if (r12 >= r0) goto L_0x00e8
            okhttp3.internal.publicsuffix.PublicSuffixDatabase$a r4 = okhttp3.internal.publicsuffix.PublicSuffixDatabase.h
            byte[] r7 = r11.d
            if (r7 == 0) goto L_0x00e2
            java.lang.String r4 = okhttp3.internal.publicsuffix.PublicSuffixDatabase.a.a(r4, r7, r3, r12)
            if (r4 == 0) goto L_0x00df
            r5 = r4
            goto L_0x00e8
        L_0x00df:
            int r12 = r12 + 1
            goto L_0x00cf
        L_0x00e2:
            java.lang.String r12 = "publicSuffixExceptionListBytes"
            n.n.c.Intrinsics.b(r12)
            throw r5
        L_0x00e8:
            r12 = 6
            r0 = 46
            if (r5 == 0) goto L_0x0107
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r4 = 33
            r3.append(r4)
            r3.append(r5)
            java.lang.String r3 = r3.toString()
            char[] r1 = new char[r1]
            r1[r2] = r0
            java.util.List r12 = n.r.Indent.a(r3, r1, r2, r2, r12)
            return r12
        L_0x0107:
            if (r6 != 0) goto L_0x010e
            if (r9 != 0) goto L_0x010e
            java.util.List<java.lang.String> r12 = okhttp3.internal.publicsuffix.PublicSuffixDatabase.f3056f
            return r12
        L_0x010e:
            if (r6 == 0) goto L_0x011b
            char[] r3 = new char[r1]
            r3[r2] = r0
            java.util.List r3 = n.r.Indent.a(r6, r3, r2, r2, r12)
            if (r3 == 0) goto L_0x011b
            goto L_0x011d
        L_0x011b:
            n.i.Collections2 r3 = n.i.Collections2.b
        L_0x011d:
            if (r9 == 0) goto L_0x012a
            char[] r1 = new char[r1]
            r1[r2] = r0
            java.util.List r12 = n.r.Indent.a(r9, r1, r2, r2, r12)
            if (r12 == 0) goto L_0x012a
            goto L_0x012c
        L_0x012a:
            n.i.Collections2 r12 = n.i.Collections2.b
        L_0x012c:
            int r0 = r3.size()
            int r1 = r12.size()
            if (r0 <= r1) goto L_0x0137
            goto L_0x0138
        L_0x0137:
            r3 = r12
        L_0x0138:
            return r3
        L_0x0139:
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            java.lang.String r0 = "Unable to load publicsuffixes.gz resource from the classpath."
            java.lang.String r0 = r0.toString()
            r12.<init>(r0)
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.publicsuffix.PublicSuffixDatabase.a(java.util.List):java.util.List");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
     arg types: [p.BufferedSource, ?[OBJECT, ARRAY]]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(java.lang.Object, java.lang.String):T
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
     arg types: [p.BufferedSource, java.lang.Throwable]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(java.lang.Object, java.lang.String):T
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0050, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0051, code lost:
        n.i.Collections.a((java.io.Closeable) r0, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0054, code lost:
        throw r2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a() {
        /*
            r4 = this;
            java.lang.Class<okhttp3.internal.publicsuffix.PublicSuffixDatabase> r0 = okhttp3.internal.publicsuffix.PublicSuffixDatabase.class
            java.lang.String r1 = "publicsuffixes.gz"
            java.io.InputStream r0 = r0.getResourceAsStream(r1)
            if (r0 == 0) goto L_0x0055
            p.GzipSource r1 = new p.GzipSource
            p.Okio0 r2 = new p.Okio0
            p.Timeout r3 = new p.Timeout
            r3.<init>()
            r2.<init>(r0, r3)
            r1.<init>(r2)
            p.BufferedSource r0 = n.i.Collections.a(r1)
            int r1 = r0.readInt()     // Catch:{ all -> 0x004e }
            long r1 = (long) r1     // Catch:{ all -> 0x004e }
            byte[] r1 = r0.i(r1)     // Catch:{ all -> 0x004e }
            int r2 = r0.readInt()     // Catch:{ all -> 0x004e }
            long r2 = (long) r2     // Catch:{ all -> 0x004e }
            byte[] r2 = r0.i(r2)     // Catch:{ all -> 0x004e }
            r3 = 0
            n.i.Collections.a(r0, r3)
            monitor-enter(r4)
            if (r1 == 0) goto L_0x0047
            r4.c = r1     // Catch:{ all -> 0x004b }
            if (r2 == 0) goto L_0x0043
            r4.d = r2     // Catch:{ all -> 0x004b }
            monitor-exit(r4)
            java.util.concurrent.CountDownLatch r0 = r4.b
            r0.countDown()
            return
        L_0x0043:
            n.n.c.Intrinsics.a()     // Catch:{ all -> 0x004b }
            throw r3
        L_0x0047:
            n.n.c.Intrinsics.a()     // Catch:{ all -> 0x004b }
            throw r3
        L_0x004b:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x004e:
            r1 = move-exception
            throw r1     // Catch:{ all -> 0x0050 }
        L_0x0050:
            r2 = move-exception
            n.i.Collections.a(r0, r1)
            throw r2
        L_0x0055:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.publicsuffix.PublicSuffixDatabase.a():void");
    }
}
