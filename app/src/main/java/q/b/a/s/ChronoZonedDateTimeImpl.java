package q.b.a.s;

import java.io.InvalidObjectException;
import java.io.Serializable;
import n.i.Collections;
import q.b.a.Instant;
import q.b.a.LocalDateTime;
import q.b.a.ZoneId;
import q.b.a.ZoneOffset;
import q.b.a.c;
import q.b.a.o;
import q.b.a.p;
import q.b.a.s.b;
import q.b.a.v.ChronoField;
import q.b.a.v.ChronoUnit;
import q.b.a.v.TemporalField;
import q.b.a.v.d;
import q.b.a.v.j;
import q.b.a.v.m;

public final class ChronoZonedDateTimeImpl<D extends b> extends ChronoZonedDateTime<D> implements Serializable {
    public final ChronoLocalDateTimeImpl<D> b;
    public final ZoneOffset c;
    public final ZoneId d;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.s.ChronoLocalDateTimeImpl<D>, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.p, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.o, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public ChronoZonedDateTimeImpl(ChronoLocalDateTimeImpl<D> chronoLocalDateTimeImpl, p pVar, o oVar) {
        Collections.a((Object) chronoLocalDateTimeImpl, "dateTime");
        this.b = chronoLocalDateTimeImpl;
        Collections.a((Object) pVar, "offset");
        this.c = pVar;
        Collections.a((Object) oVar, "zone");
        this.d = oVar;
    }

    private Object readResolve() {
        throw new InvalidObjectException("Deserialization via serialization delegate");
    }

    private Object writeReplace() {
        return new Ser((byte) 13, this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.v.TemporalAccessor, q.b.a.s.ChronoZonedDateTimeImpl] */
    public boolean c(TemporalField temporalField) {
        return (temporalField instanceof ChronoField) || (temporalField != null && temporalField.a(this));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ChronoZonedDateTime) || compareTo((ChronoZonedDateTime) obj) != 0) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (this.b.hashCode() ^ this.c.c) ^ Integer.rotateLeft(this.d.hashCode(), 3);
    }

    public ZoneOffset i() {
        return this.c;
    }

    public ZoneId j() {
        return this.d;
    }

    public ChronoLocalDateTime<D> p() {
        return this.b;
    }

    public String toString() {
        String str = this.b.toString() + this.c.d;
        if (this.c == this.d) {
            return str;
        }
        return str + '[' + this.d.toString() + ']';
    }

    /* JADX INFO: additional move instructions added (3) to help type inference */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r11v0, types: [q.b.a.s.ChronoLocalDateTimeImpl, q.b.a.v.TemporalAccessor, java.lang.Object] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.o, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.p, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0062, code lost:
        if (r2.contains(r13) != false) goto L_0x006b;
     */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static <R extends q.b.a.s.b> q.b.a.s.ChronoZonedDateTime<R> a(q.b.a.s.ChronoLocalDateTimeImpl r11, q.b.a.o r12, q.b.a.p r13) {
        /*
            java.lang.String r0 = "localDateTime"
            n.i.Collections.a(r11, r0)
            java.lang.String r0 = "zone"
            n.i.Collections.a(r12, r0)
            boolean r0 = r12 instanceof q.b.a.ZoneOffset
            if (r0 == 0) goto L_0x0017
            q.b.a.s.ChronoZonedDateTimeImpl r13 = new q.b.a.s.ChronoZonedDateTimeImpl
            r0 = r12
            q.b.a.ZoneOffset r0 = (q.b.a.ZoneOffset) r0
            r13.<init>(r11, r0, r12)
            return r13
        L_0x0017:
            q.b.a.w.ZoneRules r0 = r12.g()
            q.b.a.LocalDateTime r1 = q.b.a.LocalDateTime.a(r11)
            java.util.List r2 = r0.b(r1)
            int r3 = r2.size()
            r4 = 1
            r5 = 0
            if (r3 != r4) goto L_0x0032
            java.lang.Object r13 = r2.get(r5)
            q.b.a.ZoneOffset r13 = (q.b.a.ZoneOffset) r13
            goto L_0x006b
        L_0x0032:
            int r3 = r2.size()
            if (r3 != 0) goto L_0x005c
            q.b.a.w.ZoneOffsetTransition r13 = r0.a(r1)
            q.b.a.ZoneOffset r0 = r13.d
            int r0 = r0.c
            q.b.a.ZoneOffset r1 = r13.c
            int r1 = r1.c
            int r0 = r0 - r1
            long r0 = (long) r0
            q.b.a.Duration r0 = q.b.a.Duration.b(r0)
            long r7 = r0.b
            D r2 = r11.b
            r3 = 0
            r5 = 0
            r9 = 0
            r1 = r11
            q.b.a.s.ChronoLocalDateTimeImpl r11 = r1.a(r2, r3, r5, r7, r9)
            q.b.a.ZoneOffset r13 = r13.d
            goto L_0x006b
        L_0x005c:
            if (r13 == 0) goto L_0x0065
            boolean r0 = r2.contains(r13)
            if (r0 == 0) goto L_0x0065
            goto L_0x006b
        L_0x0065:
            java.lang.Object r13 = r2.get(r5)
            q.b.a.ZoneOffset r13 = (q.b.a.ZoneOffset) r13
        L_0x006b:
            java.lang.String r0 = "offset"
            n.i.Collections.a(r13, r0)
            q.b.a.s.ChronoZonedDateTimeImpl r0 = new q.b.a.s.ChronoZonedDateTimeImpl
            r0.<init>(r11, r13, r12)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: q.b.a.s.ChronoZonedDateTimeImpl.a(q.b.a.s.ChronoLocalDateTimeImpl, q.b.a.ZoneId, q.b.a.ZoneOffset):q.b.a.s.ChronoZonedDateTime");
    }

    /* JADX WARN: Type inference failed for: r2v3, types: [q.b.a.s.ChronoLocalDateTimeImpl, q.b.a.v.TemporalAdjuster] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoLocalDateTimeImpl.b(long, q.b.a.v.m):q.b.a.s.ChronoLocalDateTimeImpl<D>
     arg types: [long, q.b.a.v.m]
     candidates:
      q.b.a.s.ChronoLocalDateTimeImpl.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDateTime
      q.b.a.s.ChronoLocalDateTimeImpl.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTime.b(long, q.b.a.v.m):q.b.a.s.ChronoLocalDateTime<D>
      q.b.a.s.ChronoLocalDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTimeImpl.b(long, q.b.a.v.m):q.b.a.s.ChronoLocalDateTimeImpl<D> */
    public ChronoZonedDateTime<D> b(long j2, m mVar) {
        if (!(mVar instanceof ChronoUnit)) {
            return o().i().c((d) mVar.a(this, j2));
        }
        return o().i().c((d) this.b.b(j2, mVar).a(this));
    }

    /* JADX WARN: Type inference failed for: r4v2, types: [q.b.a.v.TemporalAccessor, q.b.a.LocalDateTime] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.ZoneOffset, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public static <R extends b> ChronoZonedDateTimeImpl<R> a(h hVar, c cVar, o oVar) {
        ZoneOffset a = oVar.g().a((Instant) cVar);
        Collections.a((Object) a, "offset");
        return new ChronoZonedDateTimeImpl<>((ChronoLocalDateTimeImpl) hVar.b(LocalDateTime.a(cVar.b, cVar.c, a)), a, oVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoZonedDateTimeImpl.a(q.b.a.s.ChronoLocalDateTimeImpl, q.b.a.o, q.b.a.p):q.b.a.s.ChronoZonedDateTime<R>
     arg types: [q.b.a.s.ChronoLocalDateTimeImpl, q.b.a.o, q.b.a.ZoneOffset]
     candidates:
      q.b.a.s.ChronoZonedDateTimeImpl.a(q.b.a.s.h, q.b.a.c, q.b.a.o):q.b.a.s.ChronoZonedDateTimeImpl<R>
      q.b.a.s.ChronoZonedDateTimeImpl.a(q.b.a.s.ChronoLocalDateTimeImpl, q.b.a.o, q.b.a.p):q.b.a.s.ChronoZonedDateTime<R> */
    public ChronoZonedDateTime<D> a(o oVar) {
        return a(this.b, oVar, (p) this.c);
    }

    /* JADX WARN: Type inference failed for: r0v4, types: [java.lang.Enum, q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r4v3, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoZonedDateTimeImpl.a(q.b.a.s.ChronoLocalDateTimeImpl, q.b.a.o, q.b.a.p):q.b.a.s.ChronoZonedDateTime<R>
     arg types: [q.b.a.s.ChronoLocalDateTimeImpl, q.b.a.ZoneId, q.b.a.ZoneOffset]
     candidates:
      q.b.a.s.ChronoZonedDateTimeImpl.a(q.b.a.s.h, q.b.a.c, q.b.a.o):q.b.a.s.ChronoZonedDateTimeImpl<R>
      q.b.a.s.ChronoZonedDateTimeImpl.a(q.b.a.s.ChronoLocalDateTimeImpl, q.b.a.o, q.b.a.p):q.b.a.s.ChronoZonedDateTime<R> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.j, long):q.b.a.s.ChronoLocalDateTimeImpl<D>
     arg types: [q.b.a.v.j, long]
     candidates:
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.TemporalField, long):q.b.a.s.ChronoLocalDateTime
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.d, q.b.a.f):q.b.a.s.ChronoLocalDateTimeImpl<D>
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTime.a(long, q.b.a.v.m):q.b.a.s.ChronoLocalDateTime<D>
      q.b.a.s.ChronoLocalDateTime.a(q.b.a.v.j, long):q.b.a.s.ChronoLocalDateTime<D>
      q.b.a.s.ChronoLocalDateTime.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTime.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.j, long):q.b.a.s.ChronoLocalDateTimeImpl<D> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoZonedDateTimeImpl.a(q.b.a.s.h, q.b.a.c, q.b.a.o):q.b.a.s.ChronoZonedDateTimeImpl<R>
     arg types: [q.b.a.s.Chronology, q.b.a.Instant, q.b.a.ZoneId]
     candidates:
      q.b.a.s.ChronoZonedDateTimeImpl.a(q.b.a.s.ChronoLocalDateTimeImpl, q.b.a.o, q.b.a.p):q.b.a.s.ChronoZonedDateTime<R>
      q.b.a.s.ChronoZonedDateTimeImpl.a(q.b.a.s.h, q.b.a.c, q.b.a.o):q.b.a.s.ChronoZonedDateTimeImpl<R> */
    public ChronoZonedDateTime<D> a(j jVar, long j2) {
        if (!(jVar instanceof ChronoField)) {
            return o().i().c((d) jVar.a(this, j2));
        }
        ? r0 = (ChronoField) jVar;
        int ordinal = r0.ordinal();
        if (ordinal == 28) {
            return b(j2 - l(), (m) ChronoUnit.SECONDS);
        }
        if (ordinal != 29) {
            return a(this.b.a(jVar, j2), (o) this.d, (p) this.c);
        }
        ZoneOffset a = ZoneOffset.a(r0.range.a(j2, (TemporalField) r0));
        ChronoLocalDateTimeImpl<D> chronoLocalDateTimeImpl = this.b;
        return a((h) o().i(), (c) Instant.b(chronoLocalDateTimeImpl.a(a), (long) chronoLocalDateTimeImpl.c.f3094e), (o) this.d);
    }
}
