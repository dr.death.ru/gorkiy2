package q.b.a.s;

import q.b.a.v.TemporalAccessor;
import q.b.a.v.TemporalAdjuster;

public interface Era extends TemporalAccessor, TemporalAdjuster {
    int getValue();
}
