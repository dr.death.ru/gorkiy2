package q.b.a.s;

import n.i.Collections;
import q.b.a.LocalDate;
import q.b.a.u.b;
import q.b.a.v.ChronoField;
import q.b.a.v.ChronoUnit;
import q.b.a.v.Temporal;
import q.b.a.v.TemporalAdjuster;
import q.b.a.v.TemporalAmount;
import q.b.a.v.TemporalField;
import q.b.a.v.TemporalQueries;
import q.b.a.v.TemporalQuery;
import q.b.a.v.TemporalUnit;
import q.b.a.v.d;
import q.b.a.v.f;

public abstract class ChronoLocalDate extends b implements d, f, Comparable<b> {
    public abstract ChronoLocalDate a(TemporalField temporalField, long j2);

    public ChronoLocalDateTime<?> a(q.b.a.f fVar) {
        return new ChronoLocalDateTimeImpl(this, fVar);
    }

    public abstract ChronoLocalDate b(long j2, TemporalUnit temporalUnit);

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.v.TemporalAccessor, q.b.a.s.ChronoLocalDate] */
    public boolean c(TemporalField temporalField) {
        if (temporalField instanceof ChronoField) {
            return temporalField.f();
        }
        return temporalField != null && temporalField.a(this);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ChronoLocalDate) || compareTo((ChronoLocalDate) obj) != 0) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        long l2 = l();
        return i().hashCode() ^ ((int) (l2 ^ (l2 >>> 32)));
    }

    public abstract Chronology i();

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public Era j() {
        return i().a(b(ChronoField.ERA));
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public long l() {
        return d(ChronoField.EPOCH_DAY);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r4v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public String toString() {
        long d = d(ChronoField.YEAR_OF_ERA);
        long d2 = d(ChronoField.MONTH_OF_YEAR);
        long d3 = d(ChronoField.DAY_OF_MONTH);
        StringBuilder sb = new StringBuilder(30);
        sb.append(i().g());
        sb.append(" ");
        sb.append(j());
        sb.append(" ");
        sb.append(d);
        String str = "-0";
        sb.append(d2 < 10 ? str : "-");
        sb.append(d2);
        if (d3 >= 10) {
            str = "-";
        }
        sb.append(str);
        sb.append(d3);
        return sb.toString();
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.s.ChronoLocalDate, q.b.a.u.DefaultInterfaceTemporalAccessor] */
    public <R> R a(TemporalQuery temporalQuery) {
        if (temporalQuery == TemporalQueries.b) {
            return i();
        }
        if (temporalQuery == TemporalQueries.c) {
            return ChronoUnit.DAYS;
        }
        if (temporalQuery == TemporalQueries.f3151f) {
            return LocalDate.e(l());
        }
        if (temporalQuery == TemporalQueries.g || temporalQuery == TemporalQueries.d || temporalQuery == TemporalQueries.a || temporalQuery == TemporalQueries.f3150e) {
            return null;
        }
        return ChronoLocalDate.super.a(temporalQuery);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.s.ChronoLocalDate, q.b.a.v.Temporal] */
    public ChronoLocalDate a(TemporalAdjuster temporalAdjuster) {
        return i().a((d) temporalAdjuster.a(this));
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.s.ChronoLocalDate, q.b.a.v.Temporal] */
    public ChronoLocalDate a(TemporalAmount temporalAmount) {
        return i().a((d) temporalAmount.a(this));
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.s.ChronoLocalDate, q.b.a.u.DefaultInterfaceTemporal] */
    public ChronoLocalDate a(long j2, TemporalUnit temporalUnit) {
        return i().a((d) ChronoLocalDate.super.a(j2, temporalUnit));
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public Temporal a(Temporal temporal) {
        return temporal.a((TemporalField) ChronoField.EPOCH_DAY, l());
    }

    /* renamed from: a */
    public int compareTo(ChronoLocalDate chronoLocalDate) {
        int a = Collections.a(l(), chronoLocalDate.l());
        return a == 0 ? i().compareTo(chronoLocalDate.i()) : a;
    }
}
