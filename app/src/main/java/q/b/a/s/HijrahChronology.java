package q.b.a.s;

import java.io.Serializable;
import java.util.HashMap;
import org.threeten.bp.DateTimeException;
import q.b.a.c;
import q.b.a.o;
import q.b.a.v.ChronoField;
import q.b.a.v.TemporalAccessor;
import q.b.a.v.e;

public final class HijrahChronology extends Chronology implements Serializable {
    public static final HijrahChronology d = new HijrahChronology();

    /* renamed from: e  reason: collision with root package name */
    public static final HashMap<String, String[]> f3100e = new HashMap<>();

    /* renamed from: f  reason: collision with root package name */
    public static final HashMap<String, String[]> f3101f = new HashMap<>();
    public static final HashMap<String, String[]> g = new HashMap<>();

    static {
        f3100e.put("en", new String[]{"BH", "HE"});
        f3101f.put("en", new String[]{"B.H.", "H.E."});
        g.put("en", new String[]{"Before Hijrah", "Hijrah Era"});
    }

    private Object readResolve() {
        return d;
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [q.b.a.s.ChronoLocalDate, q.b.a.s.HijrahDate] */
    public ChronoLocalDate a(int i2, int i3, int i4) {
        return HijrahDate.d(i2, i3, i4);
    }

    public ChronoLocalDateTime<k> b(e eVar) {
        return super.b(eVar);
    }

    public String f() {
        return "islamic-umalqura";
    }

    public String g() {
        return "Hijrah-umalqura";
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r3v1, types: [q.b.a.s.ChronoLocalDate, q.b.a.s.HijrahDate] */
    /* JADX WARN: Type inference failed for: r3v2, types: [q.b.a.s.ChronoLocalDate, q.b.a.s.HijrahDate] */
    public ChronoLocalDate a(TemporalAccessor temporalAccessor) {
        if (temporalAccessor instanceof HijrahDate) {
            return (HijrahDate) temporalAccessor;
        }
        return new HijrahDate(temporalAccessor.d(ChronoField.EPOCH_DAY));
    }

    /* JADX WARN: Type inference failed for: r2v1, types: [q.b.a.s.Era, q.b.a.s.HijrahEra] */
    /* JADX WARN: Type inference failed for: r2v3, types: [q.b.a.s.Era, q.b.a.s.HijrahEra] */
    public Era a(int i2) {
        if (i2 == 0) {
            return HijrahEra.BEFORE_AH;
        }
        if (i2 == 1) {
            return HijrahEra.AH;
        }
        throw new DateTimeException("invalid Hijrah era");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoZonedDateTimeImpl.a(q.b.a.s.h, q.b.a.c, q.b.a.o):q.b.a.s.ChronoZonedDateTimeImpl<R>
     arg types: [q.b.a.s.HijrahChronology, q.b.a.c, q.b.a.o]
     candidates:
      q.b.a.s.ChronoZonedDateTimeImpl.a(q.b.a.s.ChronoLocalDateTimeImpl, q.b.a.o, q.b.a.p):q.b.a.s.ChronoZonedDateTime<R>
      q.b.a.s.ChronoZonedDateTimeImpl.a(q.b.a.s.h, q.b.a.c, q.b.a.o):q.b.a.s.ChronoZonedDateTimeImpl<R> */
    public ChronoZonedDateTime<k> a(c cVar, o oVar) {
        return ChronoZonedDateTimeImpl.a((h) super, cVar, oVar);
    }
}
