package q.b.a.s;

import com.crashlytics.android.answers.AnswersRetryFilesSender;
import java.io.Serializable;
import n.i.Collections;
import org.threeten.bp.DateTimeException;
import q.b.a.s.b;
import q.b.a.v.ChronoUnit;
import q.b.a.v.d;
import q.b.a.v.f;
import q.b.a.v.m;

public abstract class ChronoDateImpl<D extends b> extends b implements d, f, Serializable {
    public abstract ChronoDateImpl<D> a(long j2);

    public ChronoLocalDateTime<?> a(q.b.a.f fVar) {
        return new ChronoLocalDateTimeImpl(this, fVar);
    }

    public abstract ChronoDateImpl<D> b(long j2);

    public abstract ChronoDateImpl<D> c(long j2);

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.s.ChronoLocalDate, q.b.a.v.Temporal, q.b.a.s.ChronoDateImpl] */
    public ChronoDateImpl<D> b(long j2, m mVar) {
        if (!(mVar instanceof ChronoUnit)) {
            return (ChronoDateImpl) i().a((d) mVar.a(this, j2));
        }
        switch (((ChronoUnit) mVar).ordinal()) {
            case 7:
                return a(j2);
            case 8:
                return a(Collections.b(j2, 7));
            case 9:
                return b(j2);
            case 10:
                return c(j2);
            case 11:
                return c(Collections.b(j2, 10));
            case 12:
                return c(Collections.b(j2, 100));
            case 13:
                return c(Collections.b(j2, (int) AnswersRetryFilesSender.BACKOFF_MS));
            default:
                throw new DateTimeException(mVar + " not valid for chronology " + i().g());
        }
    }
}
