package q.b.a;

import androidx.recyclerview.widget.RecyclerView;
import com.crashlytics.android.answers.AnswersRetryFilesSender;
import j.a.a.a.outline;
import java.io.DataInput;
import java.io.InvalidObjectException;
import java.io.Serializable;
import l.a.a.a.o.b.AbstractSpiCall;
import n.i.Collections;
import org.threeten.bp.DateTimeException;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import q.b.a.s.Chronology;
import q.b.a.s.IsoChronology;
import q.b.a.t.DateTimeFormatterBuilder;
import q.b.a.t.SignStyle;
import q.b.a.u.c;
import q.b.a.v.ChronoField;
import q.b.a.v.ChronoUnit;
import q.b.a.v.Temporal;
import q.b.a.v.TemporalAdjuster;
import q.b.a.v.TemporalField;
import q.b.a.v.TemporalQueries;
import q.b.a.v.TemporalQuery;
import q.b.a.v.TemporalUnit;
import q.b.a.v.ValueRange;
import q.b.a.v.d;
import q.b.a.v.f;

public final class YearMonth extends c implements d, f, Comparable<n>, Serializable {
    public final int b;
    public final int c;

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r1v2, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    static {
        DateTimeFormatterBuilder dateTimeFormatterBuilder = new DateTimeFormatterBuilder();
        dateTimeFormatterBuilder.a((TemporalField) ChronoField.YEAR, 4, 10, SignStyle.EXCEEDS_PAD);
        dateTimeFormatterBuilder.a('-');
        dateTimeFormatterBuilder.a((TemporalField) ChronoField.MONTH_OF_YEAR, 2);
        dateTimeFormatterBuilder.c();
    }

    public YearMonth(int i2, int i3) {
        this.b = i2;
        this.c = i3;
    }

    private Object readResolve() {
        throw new InvalidObjectException("Deserialization via serialization delegate");
    }

    private Object writeReplace() {
        return new Ser((byte) 68, this);
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [q.b.a.v.TemporalAccessor, q.b.a.YearMonth] */
    public boolean c(TemporalField temporalField) {
        if (temporalField instanceof ChronoField) {
            if (temporalField == ChronoField.YEAR || temporalField == ChronoField.MONTH_OF_YEAR || temporalField == ChronoField.PROLEPTIC_MONTH || temporalField == ChronoField.YEAR_OF_ERA || temporalField == ChronoField.ERA) {
                return true;
            }
            return false;
        } else if (temporalField == null || !temporalField.a(this)) {
            return false;
        } else {
            return true;
        }
    }

    public int compareTo(Object obj) {
        YearMonth yearMonth = (YearMonth) obj;
        int i2 = this.b - yearMonth.b;
        return i2 == 0 ? this.c - yearMonth.c : i2;
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [q.b.a.v.TemporalAccessor, q.b.a.YearMonth] */
    public long d(TemporalField temporalField) {
        int i2;
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.b(this);
        }
        int i3 = 1;
        switch (((ChronoField) temporalField).ordinal()) {
            case 23:
                i2 = this.c;
                break;
            case 24:
                return (((long) this.b) * 12) + ((long) (this.c - 1));
            case 25:
                int i4 = this.b;
                if (i4 < 1) {
                    i4 = 1 - i4;
                }
                return (long) i4;
            case 26:
                i2 = this.b;
                break;
            case 27:
                if (this.b < 1) {
                    i3 = 0;
                }
                return (long) i3;
            default:
                throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
        }
        return (long) i2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof YearMonth)) {
            return false;
        }
        YearMonth yearMonth = (YearMonth) obj;
        if (this.b == yearMonth.b && this.c == yearMonth.c) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return this.b ^ (this.c << 27);
    }

    public String toString() {
        int abs = Math.abs(this.b);
        StringBuilder sb = new StringBuilder(9);
        if (abs < 1000) {
            int i2 = this.b;
            if (i2 < 0) {
                sb.append(i2 - 10000);
                sb.deleteCharAt(1);
            } else {
                sb.append(i2 + AbstractSpiCall.DEFAULT_TIMEOUT);
                sb.deleteCharAt(0);
            }
        } else {
            sb.append(this.b);
        }
        sb.append(this.c < 10 ? "-0" : "-");
        sb.append(this.c);
        return sb.toString();
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v1, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public static YearMonth b(int i2, int i3) {
        ? r0 = ChronoField.YEAR;
        r0.range.b((long) i2, r0);
        ? r02 = ChronoField.MONTH_OF_YEAR;
        r02.range.b((long) i3, r02);
        return new YearMonth(i2, i3);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.YearMonth, q.b.a.u.DefaultInterfaceTemporalAccessor] */
    public <R> R a(TemporalQuery temporalQuery) {
        if (temporalQuery == TemporalQueries.b) {
            return IsoChronology.d;
        }
        if (temporalQuery == TemporalQueries.c) {
            return ChronoUnit.MONTHS;
        }
        if (temporalQuery == TemporalQueries.f3151f || temporalQuery == TemporalQueries.g || temporalQuery == TemporalQueries.d || temporalQuery == TemporalQueries.a || temporalQuery == TemporalQueries.f3150e) {
            return null;
        }
        return YearMonth.super.a(temporalQuery);
    }

    public int b(TemporalField temporalField) {
        return a(temporalField).a(d(temporalField), temporalField);
    }

    /* JADX WARN: Type inference failed for: r5v4, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.YearMonth.a(q.b.a.v.TemporalField, long):q.b.a.YearMonth
     arg types: [?, long]
     candidates:
      q.b.a.YearMonth.a(int, int):q.b.a.YearMonth
      q.b.a.YearMonth.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.YearMonth.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.YearMonth.a(q.b.a.v.TemporalField, long):q.b.a.YearMonth */
    public YearMonth b(long j2, TemporalUnit temporalUnit) {
        if (!(temporalUnit instanceof ChronoUnit)) {
            return (YearMonth) temporalUnit.a(this, j2);
        }
        switch (((ChronoUnit) temporalUnit).ordinal()) {
            case 9:
                return a(j2);
            case 10:
                return b(j2);
            case 11:
                return b(Collections.b(j2, 10));
            case 12:
                return b(Collections.b(j2, 100));
            case 13:
                return b(Collections.b(j2, (int) AnswersRetryFilesSender.BACKOFF_MS));
            case 14:
                ? r5 = ChronoField.ERA;
                return a((TemporalField) r5, Collections.d(d(r5), j2));
            default:
                throw new UnsupportedTemporalTypeException("Unsupported unit: " + temporalUnit);
        }
    }

    public final YearMonth a(int i2, int i3) {
        if (this.b == i2 && this.c == i3) {
            return this;
        }
        return new YearMonth(i2, i3);
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [q.b.a.YearMonth, q.b.a.u.DefaultInterfaceTemporalAccessor] */
    public ValueRange a(TemporalField temporalField) {
        if (temporalField != ChronoField.YEAR_OF_ERA) {
            return YearMonth.super.a(temporalField);
        }
        return ValueRange.a(1, this.b <= 0 ? 1000000000 : 999999999);
    }

    /* JADX WARN: Type inference failed for: r1v2, types: [q.b.a.YearMonth, q.b.a.v.Temporal] */
    public Temporal a(TemporalAdjuster temporalAdjuster) {
        return (YearMonth) temporalAdjuster.a(this);
    }

    /* JADX WARN: Type inference failed for: r0v2, types: [java.lang.Enum, q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r6v1, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r5v5, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r5v12, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public YearMonth a(TemporalField temporalField, long j2) {
        if (!(temporalField instanceof ChronoField)) {
            return (YearMonth) temporalField.a(this, j2);
        }
        ? r0 = (ChronoField) temporalField;
        r0.range.b(j2, r0);
        switch (r0.ordinal()) {
            case 23:
                int i2 = (int) j2;
                ? r6 = ChronoField.MONTH_OF_YEAR;
                r6.range.b((long) i2, r6);
                return a(this.b, i2);
            case 24:
                return a(j2 - d(ChronoField.PROLEPTIC_MONTH));
            case 25:
                if (this.b < 1) {
                    j2 = 1 - j2;
                }
                return a((int) j2);
            case 26:
                return a((int) j2);
            case 27:
                return d(ChronoField.ERA) == j2 ? this : a(1 - this.b);
            default:
                throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
        }
    }

    public YearMonth b(long j2) {
        if (j2 == 0) {
            return this;
        }
        return a(ChronoField.YEAR.a(((long) this.b) + j2), this.c);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public YearMonth a(int i2) {
        ? r0 = ChronoField.YEAR;
        r0.range.b((long) i2, r0);
        return a(i2, this.c);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.b(long, long):long
     arg types: [long, int]
     candidates:
      n.i.Collections.b(int, int):int
      n.i.Collections.b(long, int):long
      n.i.Collections.b(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.b(long, long):long */
    public YearMonth a(long j2) {
        if (j2 == 0) {
            return this;
        }
        long j3 = (((long) this.b) * 12) + ((long) (this.c - 1)) + j2;
        return a(ChronoField.YEAR.a(Collections.b(j3, 12L)), Collections.a(j3, 12) + 1);
    }

    /* JADX WARN: Type inference failed for: r4v2, types: [q.b.a.YearMonth, q.b.a.v.Temporal] */
    /* JADX WARN: Type inference failed for: r4v5, types: [q.b.a.YearMonth, q.b.a.v.Temporal] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.YearMonth.b(long, q.b.a.v.TemporalUnit):q.b.a.YearMonth
     arg types: [int, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.YearMonth.b(int, int):q.b.a.YearMonth
      q.b.a.YearMonth.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.YearMonth.b(long, q.b.a.v.TemporalUnit):q.b.a.YearMonth */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.YearMonth.b(long, q.b.a.v.TemporalUnit):q.b.a.YearMonth
     arg types: [?, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.YearMonth.b(int, int):q.b.a.YearMonth
      q.b.a.YearMonth.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.YearMonth.b(long, q.b.a.v.TemporalUnit):q.b.a.YearMonth */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.YearMonth.b(long, q.b.a.v.TemporalUnit):q.b.a.YearMonth
     arg types: [long, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.YearMonth.b(int, int):q.b.a.YearMonth
      q.b.a.YearMonth.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.YearMonth.b(long, q.b.a.v.TemporalUnit):q.b.a.YearMonth */
    public Temporal a(long j2, TemporalUnit temporalUnit) {
        return j2 == Long.MIN_VALUE ? b((long) RecyclerView.FOREVER_NS, temporalUnit).b(1L, temporalUnit) : b(-j2, temporalUnit);
    }

    /* JADX WARN: Type inference failed for: r0v3, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public Temporal a(Temporal temporal) {
        if (Chronology.c(temporal).equals(IsoChronology.d)) {
            return temporal.a((TemporalField) ChronoField.PROLEPTIC_MONTH, (((long) this.b) * 12) + ((long) (this.c - 1)));
        }
        throw new DateTimeException("Adjustment only supported on ISO date-time");
    }

    public static YearMonth a(DataInput dataInput) {
        return b(dataInput.readInt(), dataInput.readByte());
    }
}
