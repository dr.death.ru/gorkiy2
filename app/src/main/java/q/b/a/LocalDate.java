package q.b.a;

import androidx.recyclerview.widget.RecyclerView;
import com.crashlytics.android.answers.AnswersRetryFilesSender;
import j.a.a.a.outline;
import java.io.DataInput;
import java.io.InvalidObjectException;
import java.io.Serializable;
import l.a.a.a.o.b.AbstractSpiCall;
import n.i.Collections;
import org.threeten.bp.DateTimeException;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import q.b.a.s.ChronoLocalDate;
import q.b.a.s.ChronoLocalDateTime;
import q.b.a.s.Chronology;
import q.b.a.s.Era;
import q.b.a.s.IsoChronology;
import q.b.a.t.DateTimeFormatter;
import q.b.a.v.ChronoField;
import q.b.a.v.ChronoUnit;
import q.b.a.v.Temporal;
import q.b.a.v.TemporalAccessor;
import q.b.a.v.TemporalAdjuster;
import q.b.a.v.TemporalAmount;
import q.b.a.v.TemporalField;
import q.b.a.v.TemporalQueries;
import q.b.a.v.TemporalQuery;
import q.b.a.v.TemporalUnit;
import q.b.a.v.ValueRange;

public final class LocalDate extends ChronoLocalDate implements Temporal, TemporalAdjuster, Serializable {

    /* renamed from: e  reason: collision with root package name */
    public static final LocalDate f3088e = a(-999999999, 1, 1);

    /* renamed from: f  reason: collision with root package name */
    public static final LocalDate f3089f = a(999999999, 12, 31);
    public static final TemporalQuery<d> g = new a();
    public final int b;
    public final short c;
    public final short d;

    public class a implements TemporalQuery<d> {
        public Object a(TemporalAccessor temporalAccessor) {
            return LocalDate.a(temporalAccessor);
        }
    }

    public LocalDate(int i2, int i3, int i4) {
        this.b = i2;
        this.c = (short) i3;
        this.d = (short) i4;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public static LocalDate e(long j2) {
        long j3;
        long j4 = j2;
        ? r2 = ChronoField.EPOCH_DAY;
        r2.range.b(j4, r2);
        long j5 = (j4 + 719528) - 60;
        if (j5 < 0) {
            long j6 = ((j5 + 1) / 146097) - 1;
            j3 = j6 * 400;
            j5 += (-j6) * 146097;
        } else {
            j3 = 0;
        }
        long j7 = ((j5 * 400) + 591) / 146097;
        long j8 = j5 - ((j7 / 400) + (((j7 / 4) + (j7 * 365)) - (j7 / 100)));
        if (j8 < 0) {
            j7--;
            j8 = j5 - ((j7 / 400) + (((j7 / 4) + (365 * j7)) - (j7 / 100)));
        }
        int i2 = (int) j8;
        int i3 = ((i2 * 5) + 2) / 153;
        return new LocalDate(ChronoField.YEAR.a(j7 + j3 + ((long) (i3 / 10))), ((i3 + 2) % 12) + 1, (i2 - (((i3 * 306) + 5) / 10)) + 1);
    }

    private Object readResolve() {
        throw new InvalidObjectException("Deserialization via serialization delegate");
    }

    private Object writeReplace() {
        return new Ser((byte) 3, this);
    }

    public boolean c(TemporalField temporalField) {
        return super.c(temporalField);
    }

    public long d(TemporalField temporalField) {
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.b(this);
        }
        if (temporalField == ChronoField.EPOCH_DAY) {
            return l();
        }
        if (temporalField == ChronoField.PROLEPTIC_MONTH) {
            return (((long) this.b) * 12) + ((long) (this.c - 1));
        }
        return (long) e(temporalField);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LocalDate) || a((LocalDate) obj) != 0) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i2 = this.b;
        return (((i2 << 11) + (this.c << 6)) + this.d) ^ (i2 & -2048);
    }

    public Chronology i() {
        return IsoChronology.d;
    }

    public Era j() {
        return super.j();
    }

    public long l() {
        long j2;
        long j3 = (long) this.b;
        long j4 = (long) this.c;
        long j5 = (365 * j3) + 0;
        if (j3 >= 0) {
            j2 = ((j3 + 399) / 400) + (((3 + j3) / 4) - ((99 + j3) / 100)) + j5;
        } else {
            j2 = j5 - ((j3 / -400) + ((j3 / -4) - (j3 / -100)));
        }
        long j6 = (((367 * j4) - 362) / 12) + j2 + ((long) (this.d - 1));
        if (j4 > 2) {
            j6--;
            if (!q()) {
                j6--;
            }
        }
        return j6 - 719528;
    }

    public DayOfWeek o() {
        return DayOfWeek.a(Collections.a(l() + 3, 7) + 1);
    }

    public int p() {
        return (Month.a(this.c).a(q()) + this.d) - 1;
    }

    public boolean q() {
        return IsoChronology.d.a((long) this.b);
    }

    public String toString() {
        int i2 = this.b;
        short s2 = this.c;
        short s3 = this.d;
        int abs = Math.abs(i2);
        StringBuilder sb = new StringBuilder(10);
        if (abs >= 1000) {
            if (i2 > 9999) {
                sb.append('+');
            }
            sb.append(i2);
        } else if (i2 < 0) {
            sb.append(i2 - 10000);
            sb.deleteCharAt(1);
        } else {
            sb.append(i2 + AbstractSpiCall.DEFAULT_TIMEOUT);
            sb.deleteCharAt(0);
        }
        String str = "-0";
        sb.append(s2 < 10 ? str : "-");
        sb.append((int) s2);
        if (s3 >= 10) {
            str = "-";
        }
        sb.append(str);
        sb.append((int) s3);
        return sb.toString();
    }

    public LocalDate c(long j2) {
        return a(Collections.b(j2, 7));
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v2, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.Month, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public static LocalDate b(int i2, Month month, int i3) {
        ? r0 = ChronoField.YEAR;
        r0.range.b((long) i2, r0);
        Collections.a((Object) month, "month");
        ? r02 = ChronoField.DAY_OF_MONTH;
        r02.range.b((long) i3, r02);
        return a(i2, month, i3);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v1, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v2, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public static LocalDate a(int i2, int i3, int i4) {
        ? r0 = ChronoField.YEAR;
        r0.range.b((long) i2, r0);
        ? r02 = ChronoField.MONTH_OF_YEAR;
        r02.range.b((long) i3, r02);
        ? r03 = ChronoField.DAY_OF_MONTH;
        r03.range.b((long) i4, r03);
        return a(i2, Month.a(i3), i4);
    }

    public LocalDate d(long j2) {
        if (j2 == 0) {
            return this;
        }
        return b(ChronoField.YEAR.a(((long) this.b) + j2), this.c, this.d);
    }

    public static LocalDate b(int i2, int i3, int i4) {
        if (i3 == 2) {
            i4 = Math.min(i4, IsoChronology.d.a((long) i2) ? 29 : 28);
        } else if (i3 == 4 || i3 == 6 || i3 == 9 || i3 == 11) {
            i4 = Math.min(i4, 30);
        }
        return a(i2, i3, i4);
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [q.b.a.v.TemporalAccessor, q.b.a.LocalDate, q.b.a.u.DefaultInterfaceTemporalAccessor] */
    public int b(TemporalField temporalField) {
        if (temporalField instanceof ChronoField) {
            return e(temporalField);
        }
        return a(temporalField).a(d(temporalField), temporalField);
    }

    public final int e(TemporalField temporalField) {
        switch (((ChronoField) temporalField).ordinal()) {
            case 15:
                return o().getValue();
            case 16:
                return ((this.d - 1) % 7) + 1;
            case 17:
                return ((p() - 1) % 7) + 1;
            case 18:
                return this.d;
            case 19:
                return p();
            case 20:
                throw new DateTimeException(outline.a("Field too large for an int: ", temporalField));
            case 21:
                return ((this.d - 1) / 7) + 1;
            case 22:
                return ((p() - 1) / 7) + 1;
            case 23:
                return this.c;
            case 24:
                throw new DateTimeException(outline.a("Field too large for an int: ", temporalField));
            case 25:
                int i2 = this.b;
                return i2 >= 1 ? i2 : 1 - i2;
            case 26:
                return this.b;
            case 27:
                if (this.b >= 1) {
                    return 1;
                }
                return 0;
            default:
                throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
        }
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v1, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public static LocalDate a(int i2, int i3) {
        ? r0 = ChronoField.YEAR;
        long j2 = (long) i2;
        r0.range.b(j2, r0);
        ? r02 = ChronoField.DAY_OF_YEAR;
        r02.range.b((long) i3, r02);
        boolean a2 = IsoChronology.d.a(j2);
        if (i3 != 366 || a2) {
            Month a3 = Month.a(((i3 - 1) / 31) + 1);
            if (i3 > (a3.b(a2) + a3.a(a2)) - 1) {
                a3 = Month.ENUMS[((((int) 1) + 12) + a3.ordinal()) % 12];
            }
            return a(i2, a3, (i3 - a3.a(a2)) + 1);
        }
        throw new DateTimeException(outline.b("Invalid date 'DayOfYear 366' as '", i2, "' is not a leap year"));
    }

    /* JADX WARN: Type inference failed for: r5v4, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.LocalDate.a(q.b.a.v.TemporalField, long):q.b.a.LocalDate
     arg types: [?, long]
     candidates:
      q.b.a.LocalDate.a(int, int):q.b.a.LocalDate
      q.b.a.LocalDate.a(java.lang.CharSequence, q.b.a.t.DateTimeFormatter):q.b.a.LocalDate
      q.b.a.LocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.LocalDate
      q.b.a.LocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.LocalDate.a(q.b.a.v.TemporalField, long):q.b.a.s.ChronoLocalDate
      q.b.a.LocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.LocalDate.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.s.ChronoLocalDate.a(q.b.a.v.TemporalField, long):q.b.a.s.ChronoLocalDate
      q.b.a.s.ChronoLocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDate.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.v.Temporal.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.v.Temporal.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.LocalDate.a(q.b.a.v.TemporalField, long):q.b.a.LocalDate */
    public LocalDate b(long j2, TemporalUnit temporalUnit) {
        if (!(temporalUnit instanceof ChronoUnit)) {
            return (LocalDate) temporalUnit.a(this, j2);
        }
        switch (((ChronoUnit) temporalUnit).ordinal()) {
            case 7:
                return a(j2);
            case 8:
                return c(j2);
            case 9:
                return b(j2);
            case 10:
                return d(j2);
            case 11:
                return d(Collections.b(j2, 10));
            case 12:
                return d(Collections.b(j2, 100));
            case 13:
                return d(Collections.b(j2, (int) AnswersRetryFilesSender.BACKOFF_MS));
            case 14:
                ? r5 = ChronoField.ERA;
                return a((TemporalField) r5, Collections.d(d((TemporalField) r5), j2));
            default:
                throw new UnsupportedTemporalTypeException("Unsupported unit: " + temporalUnit);
        }
    }

    public static LocalDate a(TemporalAccessor temporalAccessor) {
        LocalDate localDate = (LocalDate) temporalAccessor.a(TemporalQueries.f3151f);
        if (localDate != null) {
            return localDate;
        }
        throw new DateTimeException(outline.a(temporalAccessor, outline.a("Unable to obtain LocalDate from TemporalAccessor: ", temporalAccessor, ", type ")));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.b(long, long):long
     arg types: [long, int]
     candidates:
      n.i.Collections.b(int, int):int
      n.i.Collections.b(long, int):long
      n.i.Collections.b(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.b(long, long):long */
    public LocalDate b(long j2) {
        if (j2 == 0) {
            return this;
        }
        long j3 = (((long) this.b) * 12) + ((long) (this.c - 1)) + j2;
        return b(ChronoField.YEAR.a(Collections.b(j3, 12L)), Collections.a(j3, 12) + 1, this.d);
    }

    public <R> R a(TemporalQuery temporalQuery) {
        if (temporalQuery == TemporalQueries.f3151f) {
            return this;
        }
        return super.a(temporalQuery);
    }

    public boolean b(ChronoLocalDate chronoLocalDate) {
        if (chronoLocalDate instanceof LocalDate) {
            if (a((LocalDate) chronoLocalDate) < 0) {
                return true;
            }
            return false;
        } else if (l() < super.l()) {
            return true;
        } else {
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.t.DateTimeFormatter, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public static LocalDate a(CharSequence charSequence, DateTimeFormatter dateTimeFormatter) {
        Collections.a((Object) dateTimeFormatter, "formatter");
        return (LocalDate) dateTimeFormatter.a(charSequence, g);
    }

    public static LocalDate a(int i2, Month month, int i3) {
        if (i3 <= 28 || i3 <= month.b(IsoChronology.d.a((long) i2))) {
            return new LocalDate(i2, month.getValue(), i3);
        }
        if (i3 == 29) {
            throw new DateTimeException(outline.b("Invalid date 'February 29' as '", i2, "' is not a leap year"));
        }
        StringBuilder a2 = outline.a("Invalid date '");
        a2.append(month.name());
        a2.append(" ");
        a2.append(i3);
        a2.append("'");
        throw new DateTimeException(a2.toString());
    }

    public ValueRange a(TemporalField temporalField) {
        int i2;
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.c(this);
        }
        ChronoField chronoField = (ChronoField) temporalField;
        if (chronoField.f()) {
            int ordinal = chronoField.ordinal();
            if (ordinal == 18) {
                short s2 = this.c;
                if (s2 != 2) {
                    i2 = (s2 == 4 || s2 == 6 || s2 == 9 || s2 == 11) ? 30 : 31;
                } else {
                    i2 = q() ? 29 : 28;
                }
                return ValueRange.a(1, (long) i2);
            } else if (ordinal == 19) {
                return ValueRange.a(1, (long) (q() ? 366 : 365));
            } else if (ordinal == 21) {
                return ValueRange.a(1, (Month.a(this.c) != Month.FEBRUARY || q()) ? 5 : 4);
            } else if (ordinal != 25) {
                return temporalField.g();
            } else {
                return ValueRange.a(1, this.b <= 0 ? 1000000000 : 999999999);
            }
        } else {
            throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
        }
    }

    public LocalDate a(TemporalAdjuster temporalAdjuster) {
        if (temporalAdjuster instanceof LocalDate) {
            return (LocalDate) temporalAdjuster;
        }
        return (LocalDate) temporalAdjuster.a(this);
    }

    /* JADX WARN: Type inference failed for: r0v2, types: [java.lang.Enum, q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r5v6, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r5v8, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r5v17, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r5v19, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r6v11, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r5v24, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r5v31, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public LocalDate a(TemporalField temporalField, long j2) {
        if (!(temporalField instanceof ChronoField)) {
            return (LocalDate) temporalField.a(this, j2);
        }
        ? r0 = (ChronoField) temporalField;
        r0.range.b(j2, r0);
        switch (r0.ordinal()) {
            case 15:
                return a(j2 - ((long) o().getValue()));
            case 16:
                return a(j2 - d((TemporalField) ChronoField.ALIGNED_DAY_OF_WEEK_IN_MONTH));
            case 17:
                return a(j2 - d((TemporalField) ChronoField.ALIGNED_DAY_OF_WEEK_IN_YEAR));
            case 18:
                int i2 = (int) j2;
                return this.d == i2 ? this : a(this.b, this.c, i2);
            case 19:
                int i3 = (int) j2;
                return p() == i3 ? this : a(this.b, i3);
            case 20:
                return e(j2);
            case 21:
                return c(j2 - d((TemporalField) ChronoField.ALIGNED_WEEK_OF_MONTH));
            case 22:
                return c(j2 - d((TemporalField) ChronoField.ALIGNED_WEEK_OF_YEAR));
            case 23:
                int i4 = (int) j2;
                if (this.c == i4) {
                    return this;
                }
                ? r6 = ChronoField.MONTH_OF_YEAR;
                r6.range.b((long) i4, r6);
                return b(this.b, i4, this.d);
            case 24:
                return b(j2 - d((TemporalField) ChronoField.PROLEPTIC_MONTH));
            case 25:
                if (this.b < 1) {
                    j2 = 1 - j2;
                }
                return a((int) j2);
            case 26:
                return a((int) j2);
            case 27:
                return d(ChronoField.ERA) == j2 ? this : a(1 - this.b);
            default:
                throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
        }
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public LocalDate a(int i2) {
        if (this.b == i2) {
            return this;
        }
        ? r0 = ChronoField.YEAR;
        r0.range.b((long) i2, r0);
        return b(i2, this.c, this.d);
    }

    public ChronoLocalDate a(TemporalAmount temporalAmount) {
        return (LocalDate) temporalAmount.a(this);
    }

    public LocalDate a(long j2) {
        if (j2 == 0) {
            return this;
        }
        return e(Collections.d(l(), j2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.LocalDate.b(long, q.b.a.v.TemporalUnit):q.b.a.LocalDate
     arg types: [int, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.LocalDate.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.LocalDate.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDate.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.s.ChronoLocalDate.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.v.Temporal.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.LocalDate.b(long, q.b.a.v.TemporalUnit):q.b.a.LocalDate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.LocalDate.b(long, q.b.a.v.TemporalUnit):q.b.a.LocalDate
     arg types: [?, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.LocalDate.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.LocalDate.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDate.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.s.ChronoLocalDate.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.v.Temporal.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.LocalDate.b(long, q.b.a.v.TemporalUnit):q.b.a.LocalDate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.LocalDate.b(long, q.b.a.v.TemporalUnit):q.b.a.LocalDate
     arg types: [long, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.LocalDate.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.LocalDate.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDate.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.s.ChronoLocalDate.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.v.Temporal.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.LocalDate.b(long, q.b.a.v.TemporalUnit):q.b.a.LocalDate */
    public LocalDate a(long j2, TemporalUnit temporalUnit) {
        return j2 == Long.MIN_VALUE ? b((long) RecyclerView.FOREVER_NS, temporalUnit).b(1L, temporalUnit) : b(-j2, temporalUnit);
    }

    public Temporal a(Temporal temporal) {
        return super.a(temporal);
    }

    public ChronoLocalDateTime a(LocalTime localTime) {
        return LocalDateTime.b(this, localTime);
    }

    /* renamed from: a */
    public int compareTo(ChronoLocalDate chronoLocalDate) {
        if (chronoLocalDate instanceof LocalDate) {
            return a((LocalDate) chronoLocalDate);
        }
        return super.compareTo(super);
    }

    public int a(LocalDate localDate) {
        int i2 = this.b - localDate.b;
        if (i2 != 0) {
            return i2;
        }
        int i3 = this.c - localDate.c;
        return i3 == 0 ? this.d - localDate.d : i3;
    }

    public static LocalDate a(DataInput dataInput) {
        return a(dataInput.readInt(), dataInput.readByte(), dataInput.readByte());
    }
}
