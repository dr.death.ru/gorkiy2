package q.b.a.u;

import androidx.recyclerview.widget.RecyclerView;
import q.b.a.v.Temporal;
import q.b.a.v.TemporalUnit;

public abstract class DefaultInterfaceTemporal extends DefaultInterfaceTemporalAccessor implements Temporal {
    public Temporal a(long j2, TemporalUnit temporalUnit) {
        return j2 == Long.MIN_VALUE ? b(RecyclerView.FOREVER_NS, temporalUnit).b(1, temporalUnit) : b(-j2, temporalUnit);
    }
}
