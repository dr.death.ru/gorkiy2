package q.b.a.v;

import java.io.Serializable;
import org.threeten.bp.DateTimeException;

public final class ValueRange implements Serializable {
    public final long b;
    public final long c;
    public final long d;

    /* renamed from: e  reason: collision with root package name */
    public final long f3152e;

    public ValueRange(long j2, long j3, long j4, long j5) {
        this.b = j2;
        this.c = j3;
        this.d = j4;
        this.f3152e = j5;
    }

    public static ValueRange a(long j2, long j3) {
        if (j2 <= j3) {
            return new ValueRange(j2, j2, j3, j3);
        }
        throw new IllegalArgumentException("Minimum value must be less than maximum value");
    }

    public long b(long j2, TemporalField temporalField) {
        if (a(j2)) {
            return j2;
        }
        if (temporalField != null) {
            throw new DateTimeException("Invalid value for " + temporalField + " (valid values " + this + "): " + j2);
        }
        throw new DateTimeException("Invalid value (valid values " + this + "): " + j2);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ValueRange)) {
            return false;
        }
        ValueRange valueRange = (ValueRange) obj;
        if (this.b == valueRange.b && this.c == valueRange.c && this.d == valueRange.d && this.f3152e == valueRange.f3152e) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        long j2 = this.b;
        long j3 = this.c;
        long j4 = this.d;
        long j5 = this.f3152e;
        long j6 = ((((((j2 + j3) << ((int) (j3 + 16))) >> ((int) (j4 + 48))) << ((int) (j4 + 32))) >> ((int) (32 + j5))) << ((int) (j5 + 48))) >> 16;
        return (int) (j6 ^ (j6 >>> 32));
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.b);
        if (this.b != this.c) {
            sb.append('/');
            sb.append(this.c);
        }
        sb.append(" - ");
        sb.append(this.d);
        if (this.d != this.f3152e) {
            sb.append('/');
            sb.append(this.f3152e);
        }
        return sb.toString();
    }

    public static ValueRange a(long j2, long j3, long j4) {
        return a(j2, j2, j3, j4);
    }

    public static ValueRange a(long j2, long j3, long j4, long j5) {
        if (j2 > j3) {
            throw new IllegalArgumentException("Smallest minimum value must be less than largest minimum value");
        } else if (j4 > j5) {
            throw new IllegalArgumentException("Smallest maximum value must be less than largest maximum value");
        } else if (j3 <= j5) {
            return new ValueRange(j2, j3, j4, j5);
        } else {
            throw new IllegalArgumentException("Minimum value must be less than maximum value");
        }
    }

    public int a(long j2, TemporalField temporalField) {
        boolean z = false;
        if ((this.b >= -2147483648L && this.f3152e <= 2147483647L) && a(j2)) {
            z = true;
        }
        if (z) {
            return (int) j2;
        }
        throw new DateTimeException("Invalid int value for " + temporalField + ": " + j2);
    }

    public boolean a(long j2) {
        return j2 >= this.b && j2 <= this.f3152e;
    }
}
