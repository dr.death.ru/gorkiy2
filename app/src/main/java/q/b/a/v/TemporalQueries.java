package q.b.a.v;

import q.b.a.LocalDate;
import q.b.a.LocalTime;
import q.b.a.ZoneId;
import q.b.a.ZoneOffset;
import q.b.a.o;
import q.b.a.p;
import q.b.a.s.Chronology;
import q.b.a.s.h;

public final class TemporalQueries {
    public static final TemporalQuery<o> a = new a();
    public static final TemporalQuery<h> b = new b();
    public static final TemporalQuery<m> c = new c();
    public static final TemporalQuery<o> d = new d();

    /* renamed from: e  reason: collision with root package name */
    public static final TemporalQuery<p> f3150e = new e();

    /* renamed from: f  reason: collision with root package name */
    public static final TemporalQuery<q.b.a.d> f3151f = new f();
    public static final TemporalQuery<q.b.a.f> g = new g();

    public class a implements TemporalQuery<o> {
        public Object a(TemporalAccessor temporalAccessor) {
            return (ZoneId) temporalAccessor.a(this);
        }
    }

    public class b implements TemporalQuery<h> {
        public Object a(TemporalAccessor temporalAccessor) {
            return (Chronology) temporalAccessor.a(this);
        }
    }

    public class c implements TemporalQuery<m> {
        public Object a(TemporalAccessor temporalAccessor) {
            return (TemporalUnit) temporalAccessor.a(this);
        }
    }

    public class d implements TemporalQuery<o> {
        public Object a(TemporalAccessor temporalAccessor) {
            ZoneId zoneId = (ZoneId) temporalAccessor.a(TemporalQueries.a);
            return zoneId != null ? zoneId : (ZoneId) temporalAccessor.a(TemporalQueries.f3150e);
        }
    }

    public class e implements TemporalQuery<p> {
        /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r0v2, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        public Object a(TemporalAccessor temporalAccessor) {
            if (temporalAccessor.c(ChronoField.OFFSET_SECONDS)) {
                return ZoneOffset.a(temporalAccessor.b(ChronoField.OFFSET_SECONDS));
            }
            return null;
        }
    }

    public class f implements TemporalQuery<q.b.a.d> {
        /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r0v2, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        public Object a(TemporalAccessor temporalAccessor) {
            if (temporalAccessor.c(ChronoField.EPOCH_DAY)) {
                return LocalDate.e(temporalAccessor.d(ChronoField.EPOCH_DAY));
            }
            return null;
        }
    }

    public class g implements TemporalQuery<q.b.a.f> {
        /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r0v2, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        public Object a(TemporalAccessor temporalAccessor) {
            if (temporalAccessor.c(ChronoField.NANO_OF_DAY)) {
                return LocalTime.e(temporalAccessor.d(ChronoField.NANO_OF_DAY));
            }
            return null;
        }
    }
}
