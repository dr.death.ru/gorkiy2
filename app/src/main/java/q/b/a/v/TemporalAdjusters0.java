package q.b.a.v;

import n.i.Collections;
import q.b.a.DayOfWeek;

/* compiled from: TemporalAdjusters */
public final class TemporalAdjusters0 implements TemporalAdjuster {
    public final int b;
    public final int c;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.DayOfWeek, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public /* synthetic */ TemporalAdjusters0(int i2, DayOfWeek dayOfWeek, TemporalAdjusters temporalAdjusters) {
        Collections.a((Object) dayOfWeek, "dayOfWeek");
        this.b = i2;
        this.c = dayOfWeek.getValue();
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r2v1, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
    /* JADX WARN: Type inference failed for: r2v2, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
    public Temporal a(Temporal temporal) {
        int b2 = temporal.b(ChronoField.DAY_OF_WEEK);
        if (this.b < 2 && b2 == this.c) {
            return temporal;
        }
        if ((this.b & 1) == 0) {
            int i2 = b2 - this.c;
            return temporal.b((long) (i2 >= 0 ? 7 - i2 : -i2), ChronoUnit.DAYS);
        }
        int i3 = this.c - b2;
        return temporal.a((long) (i3 >= 0 ? 7 - i3 : -i3), (TemporalUnit) ChronoUnit.DAYS);
    }
}
