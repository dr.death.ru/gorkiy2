package q.b.a.v;

import j.a.a.a.outline;
import java.io.InvalidObjectException;
import java.io.Serializable;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import n.i.Collections;
import org.threeten.bp.DateTimeException;
import q.b.a.DayOfWeek;
import q.b.a.Year;
import q.b.a.s.ChronoLocalDate;
import q.b.a.s.Chronology;
import q.b.a.t.ResolverStyle;
import q.b.a.t.i;

public final class WeekFields implements Serializable {
    public static final ConcurrentMap<String, o> h = new ConcurrentHashMap(4, 0.75f, 2);
    public final DayOfWeek b;
    public final int c;
    public final transient TemporalField d = new a("DayOfWeek", this, ChronoUnit.DAYS, ChronoUnit.WEEKS, a.g);

    /* renamed from: e  reason: collision with root package name */
    public final transient TemporalField f3153e = new a("WeekOfMonth", this, ChronoUnit.WEEKS, ChronoUnit.MONTHS, a.h);

    /* renamed from: f  reason: collision with root package name */
    public final transient TemporalField f3154f;
    public final transient TemporalField g;

    static {
        new WeekFields(DayOfWeek.MONDAY, 4);
        a(DayOfWeek.SUNDAY, 1);
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
    /* JADX WARN: Type inference failed for: r4v0, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
    /* JADX WARN: Type inference failed for: r10v0, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
    /* JADX WARN: Type inference failed for: r11v0, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
    /* JADX WARN: Type inference failed for: r4v1, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
    /* JADX WARN: Type inference failed for: r5v1, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
    /* JADX WARN: Type inference failed for: r10v1, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
    /* JADX WARN: Type inference failed for: r5v2, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.DayOfWeek, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public WeekFields(DayOfWeek dayOfWeek, int i2) {
        new a("WeekOfYear", this, ChronoUnit.WEEKS, ChronoUnit.YEARS, a.f3155i);
        this.f3154f = new a("WeekOfWeekBasedYear", this, ChronoUnit.WEEKS, IsoFields.d, a.f3156j);
        this.g = new a("WeekBasedYear", this, IsoFields.d, ChronoUnit.FOREVER, a.f3157k);
        Collections.a((Object) dayOfWeek, "firstDayOfWeek");
        if (i2 < 1 || i2 > 7) {
            throw new IllegalArgumentException("Minimal number of days is invalid");
        }
        this.b = dayOfWeek;
        this.c = i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [java.util.Locale, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public static WeekFields a(Locale locale) {
        Collections.a((Object) locale, "locale");
        GregorianCalendar gregorianCalendar = new GregorianCalendar(new Locale(locale.getLanguage(), locale.getCountry()));
        long firstDayOfWeek = (long) (gregorianCalendar.getFirstDayOfWeek() - 1);
        if (DayOfWeek.SUNDAY != null) {
            return a(DayOfWeek.ENUMS[((((int) (firstDayOfWeek % 7)) + 7) + 6) % 7], gregorianCalendar.getMinimalDaysInFirstWeek());
        }
        throw null;
    }

    private Object readResolve() {
        try {
            return a(this.b, this.c);
        } catch (IllegalArgumentException e2) {
            StringBuilder a2 = outline.a("Invalid WeekFields");
            a2.append(e2.getMessage());
            throw new InvalidObjectException(a2.toString());
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WeekFields) || hashCode() != obj.hashCode()) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (this.b.ordinal() * 7) + this.c;
    }

    public String toString() {
        StringBuilder a2 = outline.a("WeekFields[");
        a2.append(this.b);
        a2.append(',');
        a2.append(this.c);
        a2.append(']');
        return a2.toString();
    }

    public static class a implements TemporalField {
        public static final ValueRange g = ValueRange.a(1, 7);
        public static final ValueRange h = ValueRange.a(0, 1, 4, 6);

        /* renamed from: i  reason: collision with root package name */
        public static final ValueRange f3155i = ValueRange.a(0, 1, 52, 54);

        /* renamed from: j  reason: collision with root package name */
        public static final ValueRange f3156j = ValueRange.a(1, 52, 53);

        /* renamed from: k  reason: collision with root package name */
        public static final ValueRange f3157k = ChronoField.YEAR.range;
        public final String b;
        public final WeekFields c;
        public final TemporalUnit d;

        /* renamed from: e  reason: collision with root package name */
        public final TemporalUnit f3158e;

        /* renamed from: f  reason: collision with root package name */
        public final ValueRange f3159f;

        public a(String str, WeekFields weekFields, TemporalUnit temporalUnit, TemporalUnit temporalUnit2, ValueRange valueRange) {
            this.b = str;
            this.c = weekFields;
            this.d = temporalUnit;
            this.f3158e = temporalUnit2;
            this.f3159f = valueRange;
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        public final int a(TemporalAccessor temporalAccessor, int i2) {
            return Collections.b(temporalAccessor.b(ChronoField.DAY_OF_WEEK) - i2, 7) + 1;
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r2v1, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r1v10, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r2v6, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r2v8, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r5v2, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r5v4, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r3v12, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
        /* JADX WARN: Type inference failed for: r12v15, types: [q.b.a.v.TemporalAccessor, q.b.a.s.ChronoLocalDate] */
        /* JADX WARN: Type inference failed for: r0v17, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r0v20, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: q.b.a.s.ChronoLocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
         arg types: [int, ?]
         candidates:
          q.b.a.s.ChronoLocalDate.a(q.b.a.v.TemporalField, long):q.b.a.s.ChronoLocalDate
          q.b.a.s.ChronoLocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
          q.b.a.s.ChronoLocalDate.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
          q.b.a.s.ChronoLocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate */
        public long b(TemporalAccessor temporalAccessor) {
            int i2;
            int a;
            int b2 = Collections.b(temporalAccessor.b(ChronoField.DAY_OF_WEEK) - this.c.b.getValue(), 7) + 1;
            TemporalUnit temporalUnit = this.f3158e;
            if (temporalUnit == ChronoUnit.WEEKS) {
                return (long) b2;
            }
            if (temporalUnit == ChronoUnit.MONTHS) {
                int b3 = temporalAccessor.b(ChronoField.DAY_OF_MONTH);
                a = a(b(b3, b2), b3);
            } else if (temporalUnit == ChronoUnit.YEARS) {
                int b4 = temporalAccessor.b(ChronoField.DAY_OF_YEAR);
                a = a(b(b4, b2), b4);
            } else {
                int i3 = 366;
                if (temporalUnit == IsoFields.d) {
                    int b5 = Collections.b(temporalAccessor.b(ChronoField.DAY_OF_WEEK) - this.c.b.getValue(), 7) + 1;
                    long b6 = b(temporalAccessor, b5);
                    if (b6 == 0) {
                        i2 = ((int) b((TemporalAccessor) Chronology.c(temporalAccessor).a(temporalAccessor).a(1L, (TemporalUnit) ChronoUnit.WEEKS), b5)) + 1;
                    } else {
                        if (b6 >= 53) {
                            int b7 = b(temporalAccessor.b(ChronoField.DAY_OF_YEAR), b5);
                            if (!Year.b((long) temporalAccessor.b(ChronoField.YEAR))) {
                                i3 = 365;
                            }
                            int a2 = a(b7, i3 + this.c.c);
                            if (b6 >= ((long) a2)) {
                                b6 -= (long) (a2 - 1);
                            }
                        }
                        i2 = (int) b6;
                    }
                    return (long) i2;
                } else if (temporalUnit == ChronoUnit.FOREVER) {
                    int b8 = Collections.b(temporalAccessor.b(ChronoField.DAY_OF_WEEK) - this.c.b.getValue(), 7) + 1;
                    int b9 = temporalAccessor.b(ChronoField.YEAR);
                    long b10 = b(temporalAccessor, b8);
                    if (b10 == 0) {
                        b9--;
                    } else if (b10 >= 53) {
                        int b11 = b(temporalAccessor.b(ChronoField.DAY_OF_YEAR), b8);
                        if (!Year.b((long) b9)) {
                            i3 = 365;
                        }
                        if (b10 >= ((long) a(b11, i3 + this.c.c))) {
                            b9++;
                        }
                    }
                    return (long) b9;
                } else {
                    throw new IllegalStateException("unreachable");
                }
            }
            return (long) a;
        }

        /* JADX INFO: additional move instructions added (1) to help type inference */
        /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r0v6, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r0v7, types: [q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r0v8, types: [q.b.a.v.ChronoField] */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public q.b.a.v.ValueRange c(q.b.a.v.TemporalAccessor r7) {
            /*
                r6 = this;
                q.b.a.v.TemporalUnit r0 = r6.f3158e
                q.b.a.v.ChronoUnit r1 = q.b.a.v.ChronoUnit.WEEKS
                if (r0 != r1) goto L_0x0009
                q.b.a.v.ValueRange r7 = r6.f3159f
                return r7
            L_0x0009:
                q.b.a.v.ChronoUnit r1 = q.b.a.v.ChronoUnit.MONTHS
                if (r0 != r1) goto L_0x0010
                q.b.a.v.ChronoField r0 = q.b.a.v.ChronoField.DAY_OF_MONTH
                goto L_0x0016
            L_0x0010:
                q.b.a.v.ChronoUnit r1 = q.b.a.v.ChronoUnit.YEARS
                if (r0 != r1) goto L_0x004d
                q.b.a.v.ChronoField r0 = q.b.a.v.ChronoField.DAY_OF_YEAR
            L_0x0016:
                q.b.a.v.WeekFields r1 = r6.c
                q.b.a.DayOfWeek r1 = r1.b
                int r1 = r1.getValue()
                q.b.a.v.ChronoField r2 = q.b.a.v.ChronoField.DAY_OF_WEEK
                int r2 = r7.b(r2)
                int r2 = r2 - r1
                r1 = 7
                int r1 = n.i.Collections.b(r2, r1)
                int r1 = r1 + 1
                int r2 = r7.b(r0)
                int r1 = r6.b(r2, r1)
                q.b.a.v.ValueRange r7 = r7.a(r0)
                long r2 = r7.b
                int r0 = (int) r2
                int r0 = r6.a(r1, r0)
                long r2 = (long) r0
                long r4 = r7.f3152e
                int r7 = (int) r4
                int r7 = r6.a(r1, r7)
                long r0 = (long) r7
                q.b.a.v.ValueRange r7 = q.b.a.v.ValueRange.a(r2, r0)
                return r7
            L_0x004d:
                q.b.a.v.TemporalUnit r1 = q.b.a.v.IsoFields.d
                if (r0 != r1) goto L_0x0056
                q.b.a.v.ValueRange r7 = r6.d(r7)
                return r7
            L_0x0056:
                q.b.a.v.ChronoUnit r1 = q.b.a.v.ChronoUnit.FOREVER
                if (r0 != r1) goto L_0x0061
                q.b.a.v.ChronoField r0 = q.b.a.v.ChronoField.YEAR
                q.b.a.v.ValueRange r7 = r7.a(r0)
                return r7
            L_0x0061:
                java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
                java.lang.String r0 = "unreachable"
                r7.<init>(r0)
                throw r7
            */
            throw new UnsupportedOperationException("Method not decompiled: q.b.a.v.WeekFields.a.c(q.b.a.v.TemporalAccessor):q.b.a.v.ValueRange");
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r3v1, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r3v3, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r0v10, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
        /* JADX WARN: Type inference failed for: r9v3, types: [q.b.a.v.TemporalAccessor, q.b.a.s.ChronoLocalDate] */
        /* JADX WARN: Type inference failed for: r0v12, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
        /* JADX WARN: Type inference failed for: r9v6, types: [q.b.a.v.TemporalAccessor, q.b.a.s.ChronoLocalDate] */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: q.b.a.s.ChronoLocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
         arg types: [int, ?]
         candidates:
          q.b.a.s.ChronoLocalDate.a(q.b.a.v.TemporalField, long):q.b.a.s.ChronoLocalDate
          q.b.a.s.ChronoLocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
          q.b.a.s.ChronoLocalDate.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
          q.b.a.s.ChronoLocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: q.b.a.s.ChronoLocalDate.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
         arg types: [int, ?]
         candidates:
          q.b.a.s.ChronoLocalDate.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
          q.b.a.s.ChronoLocalDate.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate */
        public final ValueRange d(TemporalAccessor temporalAccessor) {
            int b2 = Collections.b(temporalAccessor.b(ChronoField.DAY_OF_WEEK) - this.c.b.getValue(), 7) + 1;
            long b3 = b(temporalAccessor, b2);
            if (b3 == 0) {
                return d(Chronology.c(temporalAccessor).a(temporalAccessor).a(2L, (TemporalUnit) ChronoUnit.WEEKS));
            }
            int a = a(b(temporalAccessor.b(ChronoField.DAY_OF_YEAR), b2), (Year.b((long) temporalAccessor.b(ChronoField.YEAR)) ? 366 : 365) + this.c.c);
            if (b3 >= ((long) a)) {
                return d(Chronology.c(temporalAccessor).a(temporalAccessor).b(2L, (TemporalUnit) ChronoUnit.WEEKS));
            }
            return ValueRange.a(1, (long) (a - 1));
        }

        public boolean f() {
            return true;
        }

        public ValueRange g() {
            return this.f3159f;
        }

        public boolean h() {
            return false;
        }

        public String toString() {
            return this.b + "[" + this.c.toString() + "]";
        }

        public final int a(int i2, int i3) {
            return ((i3 - 1) + (i2 + 7)) / 7;
        }

        /* JADX WARN: Type inference failed for: r1v1, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
        /* JADX WARN: Type inference failed for: r1v2, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
        /* JADX WARN: Type inference failed for: r0v4, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
        /* JADX WARN: Type inference failed for: r1v3, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
        /* JADX WARN: Type inference failed for: r0v5, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
        public <R extends d> R a(R r2, long j2) {
            int a = this.f3159f.a(j2, this);
            int b2 = r2.b(this);
            if (a == b2) {
                return r2;
            }
            if (this.f3158e != ChronoUnit.FOREVER) {
                return r2.b((long) (a - b2), this.d);
            }
            int b3 = r2.b(this.c.f3154f);
            Temporal b4 = r2.b((long) (((double) (j2 - ((long) b2))) * 52.1775d), ChronoUnit.WEEKS);
            if (b4.b(this) > a) {
                return b4.a((long) b4.b(this.c.f3154f), (TemporalUnit) ChronoUnit.WEEKS);
            }
            if (b4.b(this) < a) {
                b4 = b4.b(2, ChronoUnit.WEEKS);
            }
            R b5 = b4.b((long) (b3 - b4.b(this.c.f3154f)), ChronoUnit.WEEKS);
            return b5.b(this) > a ? b5.a(1, ChronoUnit.WEEKS) : b5;
        }

        /* JADX INFO: additional move instructions added (1) to help type inference */
        /* JADX WARN: Type inference failed for: r13v3, types: [q.b.a.v.TemporalAccessor, q.b.a.s.ChronoLocalDate] */
        /* JADX WARN: Type inference failed for: r0v3, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
        /* JADX WARN: Type inference failed for: r14v3, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r0v10, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
        /* JADX WARN: Type inference failed for: r14v11, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r13v8, types: [q.b.a.v.TemporalAccessor, q.b.a.s.ChronoLocalDate] */
        /* JADX WARN: Type inference failed for: r4v3, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r2v19, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
        /* JADX WARN: Type inference failed for: r13v10, types: [q.b.a.v.TemporalAccessor, q.b.a.s.ChronoLocalDate] */
        /* JADX WARN: Type inference failed for: r2v20, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r0v19, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
        /* JADX WARN: Type inference failed for: r13v14, types: [q.b.a.v.TemporalAccessor, q.b.a.s.ChronoLocalDate] */
        /* JADX WARN: Type inference failed for: r13v15, types: [q.b.a.v.TemporalAccessor, q.b.a.s.ChronoLocalDate] */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: q.b.a.s.ChronoLocalDate.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
         arg types: [long, ?]
         candidates:
          q.b.a.s.ChronoLocalDate.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
          q.b.a.s.ChronoLocalDate.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate */
        public e a(Map<j, Long> map, e eVar, i iVar) {
            long j2;
            ChronoLocalDate chronoLocalDate;
            long j3;
            ChronoLocalDate chronoLocalDate2;
            long j4;
            long j5;
            int i2;
            int value = this.c.b.getValue();
            if (this.f3158e == ChronoUnit.WEEKS) {
                map.put(ChronoField.DAY_OF_WEEK, Long.valueOf((long) (Collections.b((this.f3159f.a(map.remove(this).longValue(), this) - 1) + (value - 1), 7) + 1)));
                return null;
            } else if (!map.containsKey(ChronoField.DAY_OF_WEEK)) {
                return null;
            } else {
                if (this.f3158e == ChronoUnit.FOREVER) {
                    if (!map.containsKey(this.c.f3154f)) {
                        return null;
                    }
                    Chronology c2 = Chronology.c((TemporalAccessor) eVar);
                    ChronoField chronoField = ChronoField.DAY_OF_WEEK;
                    int b2 = Collections.b(chronoField.a(map.get(chronoField).longValue()) - value, 7) + 1;
                    int a = this.f3159f.a(map.get(this).longValue(), this);
                    if (iVar == ResolverStyle.LENIENT) {
                        ? a2 = c2.a(a, 1, this.c.c);
                        j5 = map.get(this.c.f3154f).longValue();
                        i2 = a((TemporalAccessor) a2, value);
                        j4 = b((TemporalAccessor) a2, i2);
                        chronoLocalDate2 = a2;
                    } else {
                        ? a3 = c2.a(a, 1, this.c.c);
                        j5 = (long) this.c.f3154f.g().a(map.get(this.c.f3154f).longValue(), this.c.f3154f);
                        i2 = a((TemporalAccessor) a3, value);
                        j4 = b((TemporalAccessor) a3, i2);
                        chronoLocalDate2 = a3;
                    }
                    e b3 = chronoLocalDate2.b(((j5 - j4) * 7) + ((long) (b2 - i2)), (TemporalUnit) ChronoUnit.DAYS);
                    if (iVar != ResolverStyle.STRICT || b3.d(this) == map.get(this).longValue()) {
                        map.remove(this);
                        map.remove(this.c.f3154f);
                        map.remove(ChronoField.DAY_OF_WEEK);
                        return b3;
                    }
                    throw new DateTimeException("Strict mode rejected date parsed to a different year");
                } else if (!map.containsKey(ChronoField.YEAR)) {
                    return null;
                } else {
                    ChronoField chronoField2 = ChronoField.DAY_OF_WEEK;
                    int b4 = Collections.b(chronoField2.a(map.get(chronoField2).longValue()) - value, 7) + 1;
                    ChronoField chronoField3 = ChronoField.YEAR;
                    int a4 = chronoField3.a(map.get(chronoField3).longValue());
                    Chronology c3 = Chronology.c((TemporalAccessor) eVar);
                    TemporalUnit temporalUnit = this.f3158e;
                    if (temporalUnit == ChronoUnit.MONTHS) {
                        if (!map.containsKey(ChronoField.MONTH_OF_YEAR)) {
                            return null;
                        }
                        long longValue = map.remove(this).longValue();
                        if (iVar == ResolverStyle.LENIENT) {
                            ? b5 = c3.a(a4, 1, 1).b(map.get(ChronoField.MONTH_OF_YEAR).longValue() - 1, (TemporalUnit) ChronoUnit.MONTHS);
                            int a5 = a((TemporalAccessor) b5, value);
                            int b6 = b5.b(ChronoField.DAY_OF_MONTH);
                            j3 = ((longValue - ((long) a(b(b6, a5), b6))) * 7) + ((long) (b4 - a5));
                            chronoLocalDate = b5;
                        } else {
                            ChronoField chronoField4 = ChronoField.MONTH_OF_YEAR;
                            ? a6 = c3.a(a4, chronoField4.a(map.get(chronoField4).longValue()), 8);
                            int a7 = a((TemporalAccessor) a6, value);
                            int b7 = a6.b(ChronoField.DAY_OF_MONTH);
                            j3 = ((((long) this.f3159f.a(longValue, this)) - ((long) a(b(b7, a7), b7))) * 7) + ((long) (b4 - a7));
                            chronoLocalDate = a6;
                        }
                        e b8 = chronoLocalDate.b(j3, (TemporalUnit) ChronoUnit.DAYS);
                        if (iVar != ResolverStyle.STRICT || b8.d(ChronoField.MONTH_OF_YEAR) == map.get(ChronoField.MONTH_OF_YEAR).longValue()) {
                            map.remove(this);
                            map.remove(ChronoField.YEAR);
                            map.remove(ChronoField.MONTH_OF_YEAR);
                            map.remove(ChronoField.DAY_OF_WEEK);
                            return b8;
                        }
                        throw new DateTimeException("Strict mode rejected date parsed to a different month");
                    } else if (temporalUnit == ChronoUnit.YEARS) {
                        long longValue2 = map.remove(this).longValue();
                        ? a8 = c3.a(a4, 1, 1);
                        if (iVar == ResolverStyle.LENIENT) {
                            int a9 = a((TemporalAccessor) a8, value);
                            j2 = ((longValue2 - b((TemporalAccessor) a8, a9)) * 7) + ((long) (b4 - a9));
                        } else {
                            int a10 = a((TemporalAccessor) a8, value);
                            j2 = ((((long) this.f3159f.a(longValue2, this)) - b((TemporalAccessor) a8, a10)) * 7) + ((long) (b4 - a10));
                        }
                        e b9 = a8.b(j2, ChronoUnit.DAYS);
                        if (iVar != ResolverStyle.STRICT || b9.d(ChronoField.YEAR) == map.get(ChronoField.YEAR).longValue()) {
                            map.remove(this);
                            map.remove(ChronoField.YEAR);
                            map.remove(ChronoField.DAY_OF_WEEK);
                            return b9;
                        }
                        throw new DateTimeException("Strict mode rejected date parsed to a different year");
                    } else {
                        throw new IllegalStateException("unreachable");
                    }
                }
            }
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        public final long b(TemporalAccessor temporalAccessor, int i2) {
            int b2 = temporalAccessor.b(ChronoField.DAY_OF_YEAR);
            return (long) a(b(b2, i2), b2);
        }

        public final int b(int i2, int i3) {
            int b2 = Collections.b(i2 - i3, 7);
            return b2 + 1 > this.c.c ? 7 - b2 : -b2;
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r0v3, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r0v4, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r0v5, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r0v6, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        public boolean a(TemporalAccessor temporalAccessor) {
            if (!temporalAccessor.c(ChronoField.DAY_OF_WEEK)) {
                return false;
            }
            TemporalUnit temporalUnit = this.f3158e;
            if (temporalUnit == ChronoUnit.WEEKS) {
                return true;
            }
            if (temporalUnit == ChronoUnit.MONTHS) {
                return temporalAccessor.c(ChronoField.DAY_OF_MONTH);
            }
            if (temporalUnit == ChronoUnit.YEARS) {
                return temporalAccessor.c(ChronoField.DAY_OF_YEAR);
            }
            if (temporalUnit == IsoFields.d) {
                return temporalAccessor.c(ChronoField.EPOCH_DAY);
            }
            if (temporalUnit == ChronoUnit.FOREVER) {
                return temporalAccessor.c(ChronoField.EPOCH_DAY);
            }
            return false;
        }
    }

    public static WeekFields a(DayOfWeek dayOfWeek, int i2) {
        String str = dayOfWeek.toString() + i2;
        WeekFields weekFields = h.get(str);
        if (weekFields != null) {
            return weekFields;
        }
        h.putIfAbsent(str, new WeekFields(dayOfWeek, i2));
        return h.get(str);
    }
}
