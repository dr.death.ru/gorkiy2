package q.b.a.w;

import j.a.a.a.outline;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.StreamCorruptedException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicReferenceArray;
import org.threeten.bp.zone.ZoneRulesException;
import q.b.a.w.c;

public final class TzdbZoneRulesProvider extends ZoneRulesProvider {
    public List<String> c;
    public final ConcurrentNavigableMap<String, c.a> d = new ConcurrentSkipListMap();

    public static class a {
        public final String a;
        public final String[] b;
        public final short[] c;
        public final AtomicReferenceArray<Object> d;

        public a(String str, String[] strArr, short[] sArr, AtomicReferenceArray<Object> atomicReferenceArray) {
            this.d = atomicReferenceArray;
            this.a = str;
            this.b = strArr;
            this.c = sArr;
        }

        public String toString() {
            return this.a;
        }
    }

    public TzdbZoneRulesProvider(InputStream inputStream) {
        new CopyOnWriteArraySet();
        try {
            a(inputStream);
        } catch (Exception e2) {
            throw new ZoneRulesException("Unable to load TZDB time-zone rules", e2);
        }
    }

    public final boolean a(InputStream inputStream) {
        DataInputStream dataInputStream = new DataInputStream(inputStream);
        if (dataInputStream.readByte() != 1) {
            throw new StreamCorruptedException("File format not recognised");
        } else if ("TZDB".equals(dataInputStream.readUTF())) {
            int readShort = dataInputStream.readShort();
            String[] strArr = new String[readShort];
            boolean z = false;
            for (int i2 = 0; i2 < readShort; i2++) {
                strArr[i2] = dataInputStream.readUTF();
            }
            int readShort2 = dataInputStream.readShort();
            String[] strArr2 = new String[readShort2];
            for (int i3 = 0; i3 < readShort2; i3++) {
                strArr2[i3] = dataInputStream.readUTF();
            }
            this.c = Arrays.asList(strArr2);
            int readShort3 = dataInputStream.readShort();
            Object[] objArr = new Object[readShort3];
            for (int i4 = 0; i4 < readShort3; i4++) {
                byte[] bArr = new byte[dataInputStream.readShort()];
                dataInputStream.readFully(bArr);
                objArr[i4] = bArr;
            }
            AtomicReferenceArray atomicReferenceArray = new AtomicReferenceArray(objArr);
            HashSet hashSet = new HashSet(readShort);
            for (int i5 = 0; i5 < readShort; i5++) {
                int readShort4 = dataInputStream.readShort();
                String[] strArr3 = new String[readShort4];
                short[] sArr = new short[readShort4];
                for (int i6 = 0; i6 < readShort4; i6++) {
                    strArr3[i6] = strArr2[dataInputStream.readShort()];
                    sArr[i6] = dataInputStream.readShort();
                }
                hashSet.add(new a(strArr[i5], strArr3, sArr, atomicReferenceArray));
            }
            Iterator it = hashSet.iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                a putIfAbsent = this.d.putIfAbsent(aVar.a, aVar);
                if (putIfAbsent == null || putIfAbsent.a.equals(aVar.a)) {
                    z = true;
                } else {
                    StringBuilder a2 = outline.a("Data already loaded for TZDB time-zone rules version: ");
                    a2.append(aVar.a);
                    throw new ZoneRulesException(a2.toString());
                }
            }
            return z;
        } else {
            throw new StreamCorruptedException("File format not recognised");
        }
    }

    public String toString() {
        return "TZDB";
    }
}
