package q.a.a.i;

import java.security.SecureRandom;
import org.bouncycastle.crypto.InvalidCipherTextException;

public class b implements a {
    public int a(byte[] bArr) {
        byte b = bArr[bArr.length - 1] & 255;
        byte b2 = (byte) b;
        boolean z = (b > bArr.length) | (b == 0);
        for (int i2 = 0; i2 < bArr.length; i2++) {
            z |= (bArr.length - i2 <= b) & (bArr[i2] != b2);
        }
        if (!z) {
            return b;
        }
        throw new InvalidCipherTextException("pad block corrupted");
    }

    public int a(byte[] bArr, int i2) {
        byte length = (byte) (bArr.length - i2);
        while (i2 < bArr.length) {
            bArr[i2] = length;
            i2++;
        }
        return length;
    }

    public void a(SecureRandom secureRandom) {
    }
}
