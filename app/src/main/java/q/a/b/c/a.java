package q.a.b.c;

import j.a.a.a.outline;
import java.io.ByteArrayOutputStream;
import org.bouncycastle.util.encoders.DecoderException;
import org.bouncycastle.util.encoders.EncoderException;
import q.a.b.b;

public class a {
    public static final b a = new b();

    public static String a(byte[] bArr) {
        int length = bArr.length;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(((length + 2) / 3) * 4);
        try {
            a.a(bArr, 0, length, byteArrayOutputStream);
            return b.a(byteArrayOutputStream.toByteArray());
        } catch (Exception e2) {
            StringBuilder a2 = outline.a("exception encoding base64 string: ");
            a2.append(e2.getMessage());
            throw new EncoderException(a2.toString(), e2);
        }
    }

    public static byte[] a(String str) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream((str.length() / 4) * 3);
        try {
            a.a(str, byteArrayOutputStream);
            return byteArrayOutputStream.toByteArray();
        } catch (Exception e2) {
            StringBuilder a2 = outline.a("unable to decode base64 string: ");
            a2.append(e2.getMessage());
            throw new DecoderException(a2.toString(), e2);
        }
    }
}
